/****************************************************************************
 Copyright (c) 2011-2012 cocos2d-x.org
 Copyright (c) 2013-2014 Chukong Technologies Inc.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/
/**
 * <p>cc.LoaderScene is a scene that you can load it when you loading files</p>
 * <p>cc.LoaderScene can present thedownload progress </p>
 * @class
 * @extends cc.Scene
 * @example
 * var lc = new cc.LoaderScene();
 */
cc.LoaderScene = cc.Scene.extend({
    _interval : null,
    _label : null,
    _loadr : null,//29 March
    _loadrBG : null,//29 March
    _loadingLabelText : null, //change 18 May
    _className:"LoaderScene",
    cb: null,
    target: null,
    /**
     * Contructor of cc.LoaderScene
     * @returns {boolean}
     */
    init : function(){
        var self = this;

        //logo
        var logoWidth = 160;
        var logoHeight = 200;

        // bg
        var bgLayer = self._bgLayer = new cc.LayerColor(cc.color(32, 32, 32, 0));
        self.addChild(bgLayer, 0);

        //image move to CCSceneFile.js
        var fontSize = 24, lblHeight =  -logoHeight / 2 + 100;
        if(cc._loaderImage){
            //loading logo
            cc.loader.loadImg(cc._loaderImage, {isCrossOrigin : false }, function(err, img){
                logoWidth = img.width;
                logoHeight = img.height;
                self._initStage(img, cc.visibleRect.center);
            });
            fontSize = 50;//14;
            lblHeight = -logoHeight / 2 - 10;
        }
        //loading percent
        var label = self._label = new cc.LabelTTF("Loading... 0%", "Arial", fontSize);
        label.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.25 ) );
        //label.setColor(cc.color(180, 180, 180));
        label.enableShadow( cc.color( 0, 0, 0), 10, 10 );
        bgLayer.addChild(this._label, 100);

        cc.loader.loadImg("res/loaderBG.png", {isCrossOrigin : false }, function(err, img){
            self._initLoadrBG(img);
        });

        if( !appLaunchWatcher )
        {
            var label = self._loadingLabelText = new cc.LabelTTF("Get ready for an explosive fun.", "Arial", fontSize);
            label.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.15 ) );
            //label.setColor(cc.color(180, 180, 180));
            label.enableShadow( cc.color( 0, 0, 0), 10, 10 );
            bgLayer.addChild(this._loadingLabelText, 100);
        }

        return true;
    },

    _initLoadrBG : function( img )
    {
        var self = this;
        var texture2d = self._texture2d = new cc.Texture2D();
        texture2d.initWithElement(img);
        texture2d.handleLoadedTexture();
        var logo = self._loadrBG = new cc.Sprite(texture2d);
        logo.setScale(cc.contentScaleFactor());
        logo.x = cc.winSize.width * 0.5;
        logo.y = cc.winSize.height * 0.25;
        self._bgLayer.addChild(logo, 10);

        cc.loader.loadImg("res/loaderFill.png", {isCrossOrigin : false }, function(err, img){
            //var self2 = this;
            self._initLoaderFiller(img);
        });
    },

    _initLoaderFiller : function( img )
    {
        var self = this;
        var texture2d = self._texture2d = new cc.Texture2D();
        texture2d.initWithElement(img);
        texture2d.handleLoadedTexture();
        var logo = new cc.Sprite(texture2d);

        self._loadr = new ccui.LoadingBar( "res/loaderFill.png" );
//self._loadr.setPercent( 0 );
        self._loadr.setScale(cc.contentScaleFactor());
        self._loadr.x = self._loadrBG.getContentSize().width * 0.5;
        self._loadr.y = self._loadrBG.getContentSize().height * 0.5;
        self._loadrBG.addChild(self._loadr, 10);
    },

    _initStage: function (img, centerPos) {
        var self = this;
        var texture2d = self._texture2d = new cc.Texture2D();
        texture2d.initWithElement(img);
        texture2d.handleLoadedTexture();
        var logo = self._logo = new cc.Sprite(texture2d);
        logo.setScale(cc.contentScaleFactor());
        logo.x = centerPos.x;
        logo.y = centerPos.y;
        self._bgLayer.addChild(logo, 10);
    },
    /**
     * custom onEnter
     */
    onEnter: function () {
        var self = this;
        cc.Node.prototype.onEnter.call(self);
        self.schedule(self._startLoading, 0.3);
    },
    /**
     * custom onExit
     */
    onExit: function () {
        cc.Node.prototype.onExit.call(this);
        var tmpStr = "Loading... 0%";
        this._label.setString(tmpStr);
    },

    /**
     * init with resources
     * @param {Array} resources
     * @param {Function|String} cb
     * @param {Object} target
     */
    initWithResources: function (resources, cb, target) {
        if(cc.isString(resources))
            resources = [resources];
        this.resources = resources || [];
        this.cb = cb;
        this.target = target;
    },

    _startLoading: function () {
        var self = this;
        self.unschedule(self._startLoading);
        var res = self.resources;
        cc.loader.load(res,
            function (result, count, loadedCount) {
                var percent = (loadedCount / count * 100) | 0;
                percent = Math.min(percent, 100);
                self._label.setString("Loading... " + percent + "%");

                if( self._loadr )
                {
                    self._loadr.setPercent( percent );
                }

                if( self._loadingLabelText )
                {
                    if( percent <= 25 )
                    {
                        if( self._loadingLabelText.getString() != "Get ready for an explosive fun." )
                        {
                            self._loadingLabelText.setString( "Get ready for an explosive fun." );
                        }
                    }
                    else if( percent <= 50 )
                    {
                        if( self._loadingLabelText.getString() != "Join people who are spinning and winning." )
                        {
                            self._loadingLabelText.setString( "Join people who are spinning and winning." );
                        }
                    }
                    else if( percent <= 75 )
                    {
                        if( self._loadingLabelText.getString() != "You are about to enter the fun zone of Double Jackpot Slots." )
                        {
                            self._loadingLabelText.setString( "You are about to enter the fun zone of Double Jackpot Slots." );
                        }
                    }
                    else
                    {
                        if( self._loadingLabelText.getString() != "Just a moment! You are entering into the best of slots fun." )
                        {
                            self._loadingLabelText.setString( "Just a moment! You are entering into the best of slots fun." );
                        }
                    }
                }
            }, function () {
                if (self.cb)
                {
                    self.cb.call(self.target);
                    if( self._loadingLabelText )
                    {
                        self._loadingLabelText.removeFromParent( true );
                        self._loadingLabelText = null;
                    }
                }

            });
    },

    _updateTransform: function(){
        this._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
        this._bgLayer._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
        this._label._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
        //this._logo._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
    }
});
/**
 * <p>cc.LoaderScene.preload can present a loaderScene with download progress.</p>
 * <p>when all the resource are downloaded it will invoke call function</p>
 * @param resources
 * @param cb
 * @param target
 * @returns {cc.LoaderScene|*}
 * @example
 * //Example
 * cc.LoaderScene.preload(g_resources, function () {
        cc.director.runScene(new HelloWorldScene());
    }, this);
 */
cc.LoaderScene.preload = function(resources, cb, target){
    var _cc = cc;
    if(!_cc.loaderScene) {
        _cc.loaderScene = new cc.LoaderScene();
        _cc.loaderScene.init();
        cc.eventManager.addCustomListener(cc.Director.EVENT_PROJECTION_CHANGED, function(){
            _cc.loaderScene._updateTransform();
        });
    }
    _cc.loaderScene.initWithResources(resources, cb, target);

    cc.director.runScene(_cc.loaderScene);
    return _cc.loaderScene;
};