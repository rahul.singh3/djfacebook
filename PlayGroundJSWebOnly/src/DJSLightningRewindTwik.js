/**
 * Created by RNF-Mac11 on 2/13/17.
 */



var reelsRandomizationArrayDJSLightningRewindTwik =
    [
        [// 85% payout
            20, 80, 20, 40, 20, 50,
            5, 110, 5, 110, 5, 120,
            22, 6, 10, 50, 5, 50,
            10, 35, 10, 50, 5, 50,
            10, 60, 10, 50, 5,60,
            10, 40, 2, 30,2 , 50
        ],
        [
            20, 95, 20, 50, 20, 70,
            5, 110, 5, 110, 5, 120,
            22, 6, 10, 50, 5, 50,
            10, 35, 10, 50, 5, 50,
            10, 60, 10, 50, 5,60,
            10, 40, 2, 30,2 , 50
        ],
        [// 97 %
            20, 105, 20, 65, 20, 80,
            5, 110, 5, 110, 5, 120,
            22, 6, 10, 50, 5, 50,
            10, 35, 10, 50, 5, 50,
            10, 60, 10, 50, 5,60,
            10, 40, 2, 30,2 , 50
        ],
        [// 140% payout

            10, 200, 10, 190, 10, 170,
            3, 250, 3, 200, 3, 200,
            5, 10, 5, 90, 3, 90,
            5, 60, 5, 80, 3, 80,
            5, 90, 5, 80, 3,90,
            5, 70, 1, 60,1, 80
        ],
        [// 300% payout

            10, 200, 10, 190, 10, 170,
            3, 250, 3, 200, 3, 200,
            5, 10, 5, 90, 3, 90,
            5, 60, 5, 80, 3, 80,
            5, 90, 5, 80, 3,90,
            5, 70, 1, 60,1, 80
        ]
    ];

var DJSLightningRewindTwik = GameScene.extend({

    /*haveExcitement : false,

    reverseCount : 0,*/
    reelsArrayDJSLightningRewindTwik:[],
    ctor:function ( level_index ) {
        this.reelsArrayDJSLightningRewindTwik = [
            /*1*/ELEMENT_EMPTY,/*2*/ELEMENT_7_PINK,/*3*/ ELEMENT_EMPTY,/*4*/ ELEMENT_7_BLUE,/*5*/ ELEMENT_EMPTY,
            /*6*/ ELEMENT_7_RED,/*7*/ELEMENT_EMPTY,/*8*/ELEMENT_BAR_3,/*9*/ ELEMENT_EMPTY,/*10*/ ELEMENT_BAR_2,
            /*11*/ELEMENT_EMPTY,/*12*/ELEMENT_BAR_1,/*13*/ELEMENT_EMPTY,/*14*/ELEMENT_2X,/*15*/ ELEMENT_EMPTY,
            /*16*/ELEMENT_7_RED,/*17*/ELEMENT_EMPTY,/*18*/ ELEMENT_BAR_1,/*19*/ELEMENT_EMPTY,/*20*/ELEMENT_7_BLUE,
            /*21*/ ELEMENT_EMPTY,/*22*/ ELEMENT_7_PINK,/*23*/ ELEMENT_EMPTY,/*24*/ELEMENT_BAR_2,/*25*/ELEMENT_EMPTY,
            /*26*/ELEMENT_BAR_3,/*27*/ ELEMENT_EMPTY,/*28*/ ELEMENT_7_BLUE,/*29*/ ELEMENT_EMPTY,/*30*/ELEMENT_BAR_1,
            /*31*/ ELEMENT_EMPTY,/*32*/ELEMENT_7_RED,/*33*/ELEMENT_EMPTY,/*34*/ ELEMENT_BAR_1,/*35*/ ELEMENT_EMPTY,
            /*36*/ ELEMENT_BAR_2
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSLightningRewindTwik;
        this.physicalArrayCheck= this.reelsArrayDJSLightningRewindTwik;
        this._super( level_index );

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;


    },

    calculatePayment : function( doAnimate )
    {
        var payMentArray = [
        /*0*/    500, // 2x 2x 2x

        /*1*/    20,  // 7 7 7 Blue

        /*2*/    15,  // 7 7 7 Pink

        /*3*/    12,  // 7 7 7 Red

        /*4*/    10,   // 3Bar 3Bar 3Bar

        /*5*/    5,   // 2Bar 2Bar 2Bar

        /*6*/    3,   // Bar Bar Bar

        /*7*/    2,   // Any Bar 7Bar Combo

        /*8*/    10   // Any 7 Combo
    ];

        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        for ( var i = 0; i < this.slots[0].resultReel.length; i++ )
        {
            tmpVector = this.slots[0].resultReel[i];
            tmpVector2.push( tmpVector[1] );
        }

        this.lineVector.push( tmpVector2 );


        var pay = 0;
        var tmpPay = 1;
        var count2X = 0;
        var countWild = 0;
        var lineLength = 3;
        var countGeneral = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var i, j, k;

        var e = null;
        var pivotElm = null;
        var pivotElm2 = null;
        var tmpVec;

        for ( i = 0; i < this.totalLines; i++ )
        {
            tmpVec = this.lineVector[i];
            for ( k = 0; k < 9; k++ )
            {
                var breakLoop = false;
                countGeneral = 0;
                switch ( k )
                {
                    case 0://for all combination with wild
                        for ( j = 0; j < lineLength; j++ )
                        {
                            if ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[i][j]] != -1 && this.reelsArrayDJSLightningRewindTwik[this.resultArray[i][j]] != ELEMENT_EMPTY )
                            {
                                switch ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[i][j]] )
                                {
                                    case ELEMENT_2X:
                                        count2X++;
                                        tmpPay*=2;
                                        countWild++;
                                        if ( j < tmpVec.length )
                                        {
                                            e = tmpVec[j];
                                        }

                                        if ( e )
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        switch ( pivotElement )
                                        {
                                            case -1:
                                                pivotElement = this.reelsArrayDJSLightningRewindTwik[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( j < tmpVec.length )
                                                {
                                                    e = tmpVec[j];
                                                }
                                                if ( e )
                                                {
                                                    pivotElm = e;
                                                }
                                                break;

                                            default:
                                                pivotElement2 = this.reelsArrayDJSLightningRewindTwik[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( j < tmpVec.length )
                                                {
                                                    e = tmpVec[j];
                                                }
                                                if ( e )
                                                {
                                                    pivotElm2 = e;
                                                }
                                                break;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }

                        if ( j == 3 )
                        {
                            if ( count2X == 3 )
                            {
                                pay = payMentArray[0];
                                breakLoop = true;
                            }
                            else if( countWild >= 2 )
                            {
                                breakLoop = true;

                                switch ( pivotElement )
                                {
                                    case ELEMENT_7_BLUE:
                                        pay = tmpPay * payMentArray[1];
                                        if ( pivotElm )
                                        {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_7_PINK:
                                        pay = tmpPay * payMentArray[2];
                                        if ( pivotElm )
                                        {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_7_RED:
                                        pay = tmpPay * payMentArray[3];
                                        if ( pivotElm )
                                        {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_3:
                                        pay = tmpPay * payMentArray[4];
                                        if ( pivotElm )
                                        {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_2:
                                        pay = tmpPay * payMentArray[5];
                                        if ( pivotElm )
                                        {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_1:
                                        pay = tmpPay * payMentArray[6];
                                        if ( pivotElm )
                                        {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        pay = tmpPay;
                                        if ( pivotElm )
                                        {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;
                                }
                            }
                            else if( countWild == 1 )
                            {
                                breakLoop = true;

                                if ( pivotElement == pivotElement2 )
                                {
                                    switch ( pivotElement )
                                    {
                                        case ELEMENT_7_BLUE:
                                            pay = tmpPay * payMentArray[1];
                                            if ( pivotElm )
                                            {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_7_PINK:
                                            pay = tmpPay * payMentArray[2];
                                            if ( pivotElm )
                                            {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_7_RED:
                                            pay = tmpPay * payMentArray[3];
                                            if ( pivotElm )
                                            {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_3:
                                            pay = tmpPay * payMentArray[4];
                                            if ( pivotElm )
                                            {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_2:
                                            pay = tmpPay * payMentArray[5];
                                            if ( pivotElm )
                                            {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_1:
                                            pay = tmpPay * payMentArray[6];
                                            if ( pivotElm )
                                            {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        default:
                                            break;
                                    }
                                }
                                else
                                {
                                    if( ( pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_BAR_3 ) &&
                                        ( pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_BAR_3 ) )
                                    {
                                        pay = tmpPay * payMentArray[7];
                                        if ( pivotElm )
                                        {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if( ( pivotElement >= ELEMENT_7_BLUE && pivotElement <= ELEMENT_7_RED ) &&
                                        ( pivotElement2 >= ELEMENT_7_BLUE && pivotElement2 <= ELEMENT_7_RED ) )
                                    {
                                        pay = tmpPay * payMentArray[8];
                                        if ( pivotElm )
                                        {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                }
                            }
                            else// no wild element calculation
                            {
                            }
                        }
                        break;

                    case 1: // 7 7 7 BLUe
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_BLUE:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[1];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( l < tmpVec.length ) {
                                    e = tmpVec[l];
                                }

                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 2: // 7 7 7 Pink
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_PINK:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[2];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( l < tmpVec.length ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }

                        break;

                    case 3: // 7 7 7 Red
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[3];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( l < tmpVec.length ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 4: // 3Bar 3Bar 3Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[4];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( l < tmpVec.length ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 5: // 2Bar 2Bar 2Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[5];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( l < tmpVec.length ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 6: // Bar Bar Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[6];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( l < tmpVec.length ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 7: // any Bar combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[7];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( l < tmpVec.length ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 8: // any 7 combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_RED:
                                case ELEMENT_7_PINK:
                                case ELEMENT_7_BLUE:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[8];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( l < tmpVec.length ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

                if ( breakLoop )
                {
                    break;
                }
            }
        }


        if ( doAnimate )
        {
            this.currentWin = this.currentBet * pay;
        }
        else
        {
            this.currentWinForcast = this.currentBet * pay;
        }

        if ( doAnimate )
        {
            if ( this.currentWin > 0 )
            {
                this.addWin = this.currentWin / 10;
                this.setCurrentWinAnimation();
            }
            this.checkAndSetAPopUp();
        }
    },
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSLightningRewindTwik[i]=this.physicalReelElements[idx];
                i++;
            }
        }
    },
    changeTotalProbabilities : function()
    {
        this.totalProbalities = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            //this.extractArrayFromSring();
            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[this.easyModeCode][i];
            }
        }
        else
        {
            for ( var i = 0; i < 36; i++ )
            {
                this.probabilityArray[this.easyModeCode][i] = reelsRandomizationArrayDJSLightningRewindTwik[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSLightningRewindTwik[this.easyModeCode][i];
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
        if( ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][0]] == ELEMENT_7_PINK && this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][1]] == ELEMENT_2X ) ||
            ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][1]] == ELEMENT_7_PINK && this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][0]] == ELEMENT_2X ) ||
            ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][0]] == ELEMENT_7_PINK && this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][1]] == ELEMENT_7_PINK ) ||
            ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][0]] == ELEMENT_2X && this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][1]] == ELEMENT_2X ) ||
            ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][0]] == ELEMENT_BAR_3 && this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][1]] == ELEMENT_2X ) ||
            ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][1]] == ELEMENT_BAR_3 && this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][0]] == ELEMENT_2X ) ||
            ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][0]] == ELEMENT_7_RED && this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][1]] == ELEMENT_2X ) ||
            ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][1]] == ELEMENT_7_RED && this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][0]] == ELEMENT_2X ) ||
            ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][0]] == ELEMENT_7_BLUE && this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][1]] == ELEMENT_2X ) ||
            ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][1]] == ELEMENT_7_BLUE && this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][0]] == ELEMENT_2X ) ||
            ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][0]] == ELEMENT_7_BLUE && this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][1]] == ELEMENT_7_BLUE ) ||
            ( this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][0]] == ELEMENT_7_RED && this.reelsArrayDJSLightningRewindTwik[this.resultArray[0][1]] == ELEMENT_7_RED ) )
        {
            if ( !this.slots[0].isForcedStop )
            {
                this.schedule( this.reelStopper, 4.8 );

                this.delegate.jukeBox( S_EXCITEMENT );

                var glowSprite = new cc.Sprite( glowStrings[0] );

                if ( glowSprite )
                {
                    var r = this.slots[0].physicalReels[ this.slots[0].physicalReels.length - 1 ];

                    glowSprite.setPosition( r.getParent().getPosition().x + this.slots[0].getPosition().x, r.getParent().getPosition().y + this.slots[0].getPosition().y );

                    //this.scaleAccordingToSlotScreen( glowSprite );

                    this.addChild( glowSprite, 5 );
                    this.animNodeVec.push( glowSprite );

                    glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ) ) );
                }
            }
        }
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
    },

    generateResult : function( scrIndx )
    {
        for ( var i = 0; i < this.slots[scrIndx].displayedReels.length; i++ )
        {
            //generate:
            var num = parseInt( this.prevReelIndex[i] );

            var tmpnum = 0;

            while ( num == parseInt( this.prevReelIndex[i] ) )
            {
                num = Math.floor( Math.random() * this.totalProbalities );

            }
            this.prevReelIndex[i] = num;
            this.resultArray[0][i] = num;

            for (var j = 0; j < 36; j++)
            {
                tmpnum+=this.probabilityArray[this.easyModeCode][j];

                if (tmpnum >= this.resultArray[0][i])
                {
                    this.resultArray[0][i] = j;
                    break;
                }
            }
            //console.log( " rand =  " + num + " index = " + this.resultArray[0][i] );
        }

        /*
         int highPay[][3] = {
         {15, 21, 15}, //2x 5x 2x
         {15, 19, 15}, //2x 4x 2x
         {15, 17, 15}, //2x 3x 2x
         {7, 7, 7},    //Triple 7 Red
         {5, 5, 5},    //Triple 7 Yellow
         {29, 29, 29}, //Triple 7 Green
         {1, 1, 1},    //Triple 7 Bar
         {31, 31, 31}, //Triple 3Bar
         {27, 27, 27}, //Triple 2Bar
         {23, 23, 23}, //Triple Bar
         };*/
        /*/HACK
         this.resultArray[0][0] = 0;
         this.resultArray[0][1] = 1;
         this.resultArray[0][2] = 2;
         //*///HACK

        this.calculatePayment( false );
        this.checkForFalseWin();

    },

        reversalAnimCB : function( obj )
{
    this.slots[0].setReelsDirection( -1 );
    this.spin();

    obj.removeFromParent( true );

    this.removeChildByTag( D_NODE_TAG );
},

    rollUpdator : function(dt)
    {
        var i;
        for (i = 0; i < this.totalSlots; i++)
        {
            this.slots[i].updateSlot(dt);
        }

        for (i = 0; i < this.totalSlots; i++)
        {
            if ( this.slots[i].isSlotRunning() )
            {
                break;
            }
        }

        if (i >= this.totalSlots)
        {
            this.isRolling = false;
            this.unschedule( this.rollUpdator );
            this.calculatePayment( true );

            if (this.isAutoSpinning)
            {
                if (this.currentWin > 0)
                {
                    this.scheduleOnce( this.autoSpinCB, 1.5);
                }
                else
                {
                    this.scheduleOnce( this.autoSpinCB, 1.0);
                }
            }
        }
    },

    setFalseWin : function()
    {
        var highWinIndices = [ 13, 1, 3, 5 ];
        var missIndices = [ 13, 1, 3, 5 ];

        var missedIndx = Math.floor( Math.random() * 3 );

        if( missedIndx == 2 )
        {
            if( Math.floor( Math.random() * 10 ) == 3 )
            {
            }
            else
            {
                missedIndx = Math.floor( Math.random() * 2 );
            }
        }

        var sameIndex = Math.floor( Math.random( ) * highWinIndices.length );

        for ( var i = 0; i < 3; i++ )
        {
            if ( i == missedIndx )
            {
                this.resultArray[0][i] = this.getRandomIndex( missIndices[ Math.floor( Math.random( ) * missIndices.length ) ], true );
            }
            else
            {
                this.resultArray[0][i] = this.getRandomIndex( highWinIndices[ sameIndex ], false );
            }
        }
    },

    setResult : function( indx, lane, sSlot )
    {
        var gap = 387;

        var j = 0;
        var animOriginY;
        var direction = 1;

        if ( this.haveReversal )
        {
            direction = -1;
        }


        var r = this.slots[0].physicalReels[lane];
        var arrSize = r.elementsVec.length;
        var spr;
        var spriteVec = new Array();

        animOriginY = gap * direction;
        spr = r.elementsVec[indx];
        if ( spr.eCode == ELEMENT_EMPTY )
        {
            gap = 285 * spr.getScale();
        }

        if ( indx == 0 )
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push( spr );

            spr.setPositionY( animOriginY );

            for ( var i = 0; i < 2; i++ )
            {
                spr = r.elementsVec[i];
                spriteVec.push( spr );
                spr.setPositionY( animOriginY + gap * ( i + 1 ) * direction );
            }
        }
        else if ( indx == arrSize - 1 )
        {
            spr = r.elementsVec[0];
            spriteVec.push( spr );

            spr.setPositionY( animOriginY );
            for ( var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i-- )
            {
                spr = r.elementsVec[i];

                spriteVec.push( spr );
                spr.setPositionY( animOriginY + gap * ( j + 1 ) * direction );
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push( spr );
            spr.setPositionY( animOriginY );

            for ( var i = indx; i < indx + 2; i++ )
            {
                spr = r.elementsVec[i];

                spriteVec.push( spr );
                spr.setPositionY( animOriginY + gap * ( j + 1 ) * direction );
                j++;
            }
        }


        for ( var i = 0; i < spriteVec.length; i++ )
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            gap = -773.5;

            var eee;
            if ( i < 2 )
            {
                eee = spriteVec[i + 1];
            }
            if ( i == 0 )
            {
                var ee = spriteVec[i + 1];
                if ( Math.abs( e.getPositionY() - ee.getPositionY() ) != 387 )
                {
                    gap = -( Math.abs( e.getPositionY() - ee.getPositionY() ) * 2 * ee.getScale() );
                }
            }


            gap*=direction;


            var ease;

            if ( this.slots[sSlot].isForcedStop )
            {
                ease = new cc.MoveTo(0.15, cc.p(0, e.getPositionY() + gap)).easing( cc.easeBackOut() );
            }
            else
            {
                ease = new cc.MoveTo(0.3, cc.p(0, e.getPositionY() + gap)).easing( cc.easeBackOut() );
            }
            ease.setTag( MOVING_TAG );
            e.runAction( ease );
        }

        return spriteVec;
    },

    setReversalAnim : function()
    {
        this.delegate.jukeBoxStop( this.S_COIN );
        this.delegate.jukeBoxStop( this.S_COIN_MORE );
        this.delegate.jukeBox( this.S_REVERSAL );

        var sprite = new cc.Sprite( "res/DJSLightningRewind/black_bg.png" );
        sprite.setTag( D_NODE_TAG );
        sprite.setOpacity( 150 );
        sprite.setScale( 2.1 );
        sprite.setPosition( cc.p( this.vSize.width - sprite.getContentSize().width / 2, this.vSize.height / 2 + 60 ) );
        this.addChild( sprite, 4 );

        var r = this.slots[0].physicalReels[1];
        var node = r.getParent();

        var revSprite = new cc.Sprite( "#rev.png" );
        revSprite.setTag( REVERSE_TAG );
        revSprite.setPosition( node.getPosition().x + this.slots[0].getPosition().x, node.getPosition().y + this.slots[0].getPosition().y );
        this.addChild( revSprite, 5 );
        var rBy = new cc.RotateBy( 1, 720 );
        var rByRev = rBy.reverse();
        revSprite.setScale( 0.25 );
        revSprite.runAction( new cc.Sequence( new cc.ScaleTo( 0.5, 1 ), rBy.easing( cc.easeBackOut() ), rByRev.easing( cc.easeBackOut() ), new cc.ScaleBy( 0.5, 0.25 ), new cc.callFunc(this.reversalAnimCB, this ) ) );

        var particle = new cc.ParticleSun();
        particle.texture = cc.textureCache.addImage( res.FireSnow_png );

        particle.setPosition( revSprite.getContentSize().width * 0.75, revSprite.getContentSize().height * 0.75 );
        revSprite.addChild( particle );
    },

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSLightningRewindTwik;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    setWinAnimation : function( animCode, screen )
{
    var tI = 0.3;

    var fire;
    for ( var i = 0; i < this.animNodeVec; i++ )
    {
        fire = animNodeVec[i];
        fire.removeFromParent( true );
    }
    animNodeVec = [];


    switch ( animCode )
    {
        case WIN_TAG:

            for ( var i = 0; i < this.slots[0].blurReels.length; i++ )
        {
            var r = this.slots[0].blurReels[i];

            var node = r.getParent();

            var moveW = this.slots[0].reelDimentions.x * this.slots[0].getScale();
            var moveH = this.slots[0].reelDimentions.y * this.slots[0].getScale();

            for ( var j = 0; j < 4; j++ )
            {

                var fire = new cc.ParticleSun();
                fire.texture = cc.textureCache.addImage( res.FireStar_png );

                fire.setStartColor( cc.color( 0, 255, 0 ) );

                var pos;

                this.addChild( fire, 5 );
                this.animNodeVec.push( fire );

                var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                diff*=( 2 - i );
                var moveBy = null;

                switch ( j % 4 )
                {
                    case 0:
                        pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY());
                        fire.setPosition( pos );
                        moveBy = new cc.MoveBy( tI, cc.p( moveW, 0 ) );
                        break;

                    case 1:
                        pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY());
                        fire.setPosition( pos );
                        moveBy = new cc.MoveBy( tI, cc.p( 0, moveH ) );
                        break;

                    case 2:
                        pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                        fire.setPosition( pos );
                        moveBy = new cc.MoveBy( tI, cc.p( -moveW, 0 ) );
                        break;

                    case 3:
                        pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                        fire.setPosition( pos );
                        moveBy = new cc.MoveBy( tI, cc.p( 0, -moveH ) );
                        break;

                    default:
                        break;
                }
                fire.runAction( new cc.RepeatForever( new cc.Sequence( moveBy, moveBy.reverse() ) ) );
            }
        }

            break;

        case BIG_WIN_TAG:
            for( var i = 0; i < this.slots[0].blurReels.length; i++ )
        {
            var r = this.slots[0].blurReels[i];

            var node = r.getParent();

            var glowSprite = new cc.Sprite( glowStrings[4] );
            var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
            diff*=( 2 - i );

            if ( glowSprite )
            {
                glowSprite.setPosition( node.getPositionX() + this.slots[0].getPositionX() - diff, node.getPositionY() + this.slots[0].getPositionY() );

                this.addChild( glowSprite, 5 );
                this.animNodeVec.push( glowSprite );
                this.scaleAccordingToSlotScreen( glowSprite );
                glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ) ) );
            }
        }
            break;

        default:
            break;
    }

    if ( !this.haveExcitement )
    {
        var tmpNo = Math.floor(Math.random() * 100);
        if ( this.reverseCount > 0 )
        {
            this.reverseCount--;
            if ( this.reverseCount <= 0 )
            {
                this.haveReversal = false;
                for ( var i = 0; i < this.slots[0].blurReels.length; i++ )
                {
                    this.slots[0].blurReels[i].resetPosition( 1 );
                }

                this.slots[0].setReelsDirection( 1 );
            }
            else
            {
                this.setReversalAnim();
            }
        }
        else if ( tmpNo % 9 == 0 )
        {
            this.reverseCount = ( Math.floor(Math.random() * 3 ) ) + 1;
            this.haveReversal = true;
            this.setReversalAnim();

            this.touchPressed = false;

            for ( var i = 0; i < this.slots[0].blurReels.length; i++ )
            {
                this.slots[0].blurReels[i].resetPosition( -1 );
            }
        }
    }
},

    spin : function()
    {

        this.unschedule( this.updateWinCoin );
        if ( !this.haveReversal )
        {
            this.deductBet( this.currentBet );
        }

        if ( this.displayedScore != this.totalScore )
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;
        this.haveExcitement = false;

        this.currentWin = 0;
        this.currentWinForcast = 0;
        this.startRolling();


        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    }

});