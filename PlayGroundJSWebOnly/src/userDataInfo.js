var userDataInfo = (function () {
// Instance stores a reference to the Singleton
var instance;

function init() {
    // Singleton
    // Private methods and variables
    function privateMethod() {
        //console.log( "I am private" );
    }
      return {
             totalChips:0,
             bonusChips:0,
             maxBetPlaced:0,
             countryCode:"IN",
             user_name:"",
             user_id:"",
             device_token:"",
             session_end_date_time:"",
             days_since_user_created:"",
             piggy_bank_id:"",
             piggy_bank_coins:"",
             piggy_bank_max_coins:"",
             is_free_deal_no_deal_active:false,
             consecutive_days_count:0,
             seconds_since_last_session:0,
             session_count:0,
             deal_remaining_seconds:0,
             deal_no_deal_consecutive_days_count:0,
             deal_reflush_remaining_seconds:0,
             isInAppVerified:false,
             _hasInAppResponded:false,
             lastPlayMachineId:0,
             userdef:null,
             favourite_machines:[],
          seconds_since_user_created:0,
             setUserDefaultConfigureData:function(user_info){
                  this.userdef = UserDef.getInstance();
                  this.user_name = user_info["name"];
                  this.user_id = user_info["user_id"];
                  this.countryCode =user_info["country"];
                  if (DPUtils.getInstance().strcasecmp(this.countryCode, "N/A" ) == 0){
                      this.countryCode = "IN";
                  }
                  this.userdef.setValueForKey("user_id" ,  this.user_id );
                  this.device_token = user_info["device_token"];
                  this.session_end_date_time = user_info["session_end_date_time"];
                  this.days_since_user_created = user_info["days_since_user_created"];
                  this.seconds_since_last_session = user_info["seconds_since_last_session"];
                  this.user_type = user_info["user_type"];
                  this.device_type = user_info["device_type"];
                  this.session_count = user_info["session_count"];
                  DPUtils.getInstance().assignDataFromJSON(user_info['custom_data']);
                 this.consecutive_days_count = parseInt(user_info["consecutive_days_count"]);
                 this.deal_no_deal_consecutive_days_count = parseInt(user_info["deal_no_deal_consecutive_days_count"]);
                 this.deal_remaining_seconds = parseInt(user_info["deal_remaining_seconds"]?user_info["deal_remaining_seconds"]:0);
                 this.deal_reflush_remaining_seconds = parseInt(user_info["deal_reflush_remaining_seconds"]?user_info["deal_reflush_remaining_seconds"]:0);
                 this.isInAppVerified = user_info["is_inapp_verified"] == 1 ?true:false;
                // console.log("Server Level "+user_info["level"]);
                    this.userdef.setValueForKey(LEVEL , user_info["level"] );
                    this.userdef.setValueForKey(VIP_POINTS , parseInt(user_info["vip_points"]) );
                    this.userdef.setValueForKey(IS_BUYER , user_info["is_buyer"] );
                    this.userdef.setValueForKey(DOLLARS_SPENT , user_info["dollar_spent"] );
                    this.userdef.setValueForKey(IAP_COUNT , user_info["transaction_count"] );
                    this.userdef.setValueForKey(MACHINE_TUNNING, user_info["machine_tuning"] );
                    this.userdef.setValueForKey(BONUS_HOUR , user_info["hourly_bonus_remaining_seconds"] );
                    this.userdef.setValueForKey(BONUS_DAY , user_info["daily_bonus_remaining_seconds"] );
                    this.userdef.setValueForKey("daily_challenge_time" , user_info["daily_challenges_remaining_seconds"] );
                    this.userdef.setValueForKey("buyer_compensate_hour" , user_info["daily_bell_bonus_remaining_seconds"] );
                    this.userdef.setValueForKey("bet" , user_info["total_bet_amount"] );
                    this.userdef.setValueForKey("last_played_bet" , user_info["last_bet_placed"] );
                    this.userdef.setValueForKey("maxbetplaced" , user_info["max_bet_placed"] );
                    this.userdef.setValueForKey("maximumcoinsreached" , user_info["maximum_coins_reached"] );
                    this.userdef.setValueForKey("maximumwinmachine" , user_info["maximum_win_machine"] );
                    this.userdef.setValueForKey("maximumwin" , user_info["maximum_win"] );
                    this.totalChips = user_info["coins"];
                    this.userdef.setValueForKey("chips" , this.totalChips );
                    var areaBonus = user_info["bonus_coins"];
                    if(areaBonus>0){
                    this.userdef.setValueForKey(AREARS ,areaBonus );
                    }
                 this.progressive_jackpot = parseInt(user_info["progressive_jackpot"]);
                 this.progressive_jackpot_winning_count = parseInt(user_info["progressive_jackpot_winning_count"]);
                    var piggy_bank_map =  user_info["piggy_bank"];
                    if ( piggy_bank_map!=null) {
                        this.piggy_bank_id = piggy_bank_map["product_id"];
                        this.piggy_bank_coins = piggy_bank_map["coins"];
                        this.piggy_bank_max_coins = piggy_bank_map["max_coins"];
                    }
                    this.is_free_deal_no_deal_active = user_info["is_free_deal_no_deal_active"];
                    this.userdef.setValueForKey("media_source" , user_info["media_source"] );
                    this.userdef.setValueForKey("original_media_source" , user_info["original_media_source"] );
                    // deal Timer
                 //totalChips
                    //console.log(user_info["coins"]);
                   // console.log(user_info["level"]);
                 this.seconds_since_user_created = user_info["seconds_since_user_created"];
                 var daily_challangeStr = user_info["daily_challenges_string"];
                 var jObj = null;
                 if (daily_challangeStr && daily_challangeStr.length>0){
                     jObj = JSON.parse(daily_challangeStr);
                 }
                 VSDailyChallenge.getInstance().refreshDailyChallengesRecord(false , jObj);
                 var favrotie = user_info["favourite_machines"];
                 for ( var i = 0; i < favrotie.length; i++ )
                 {
                     var curChar = favrotie[i];
                     this.favourite_machines.push(parseInt( curChar ));
                 }
                 var defValueData = CSUserdefauts.getInstance();
                 var realmoneyClickCount = defValueData.getValueForKey(REAL_MONEY_ADV_CLICK_COUNT,0);
                 if (realmoneyClickCount ){
                     defValueData.setValueForKey(REAL_MONEY_ADV_CLICK_COUNT,parseInt(realmoneyClickCount));
                 }
                 var realmoneyClickedCount = defValueData.getValueForKey(REAL_MONEY_ADV_CLICKED,0);
                 if (realmoneyClickedCount ){
                     defValueData.setValueForKey(REAL_MONEY_ADV_CLICKED,parseInt(realmoneyClickedCount));
                 }
                 realmoneyClickedCount = defValueData.getValueForKey(REAL_MONEY_ADV_CLICKED,0);
                 if (realmoneyClickedCount>0){
                     if (realmoneyClickedCount>=11){
                         defValueData.setValueForKey(REAL_MONEY_ADV_CLICKED,1);

                     }
                     realmoneyClickedCount = defValueData.getValueForKey(REAL_MONEY_ADV_CLICKED,0);

                     defValueData.setValueForKey(REAL_MONEY_ADV_CLICKED,realmoneyClickedCount+1);

                 }

             },
                updateWheelControlData:function(){
                    this.userdef = UserDef.getInstance();
                    var wheelspinCount =  CSUserdefauts.getInstance().getValueForKey( GRAND_WHEEL_SPIN_COUNT,0 );
                    var currentCount = wheelspinCount / MINIMUM_SPIN_COUNT;
                    var wheel_control = ServerData.getInstance().server_inner_wheel_control;
                    if( !currentCount ){
                
                        if(wheel_control >0 )
                        {
                            if( wheel_control < MINIMUM_SPIN_COUNT )
                                CSUserdefauts.getInstance().setValueForKey(GRAND_WHEEL_SPIN_TO_MAKE , MINIMUM_SPIN_COUNT );
                            else
                                CSUserdefauts.getInstance().setValueForKey( GRAND_WHEEL_SPIN_TO_MAKE, wheel_control );
                        }
                    }
                    
                }
                    
                    
                    

        };
                 
    };
   return {
        // Get the Singleton instance if one exists
       // or create one if it doesn't
       getInstance: function () {
                                if ( !instance ) {
                                    instance = init();
                                }
                                return instance;
                            }
                        };
                    })();
