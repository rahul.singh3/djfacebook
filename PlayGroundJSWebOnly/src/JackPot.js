

var JackPot = (function () {
    // Instance stores a reference to the Singleton

    function init() {

        return {

            mCode : 0,

        startTime : 0,

        ackPotInitialValue : 0,

         jackPotFinalValue : 0,

         incrementFactor : 0,

            initVariables : function ( code )
            {
                this.mCode = code;

                this.startTime = UserDef.getInstance().getValueForKey( JACKPOT_START_TIME + "_" +  machinesString[ code ] );

                this.jackPotInitialValue = UserDef.getInstance().getValueForKey( JACKPOT_INITIAL_VALUE + "_" + machinesString[ code ] );

                this.jackPotFinalValue = UserDef.getInstance().getValueForKey( JACKPOT_FINAL_VALUE + "_" + machinesString[ code ] );

                this.incrementFactor = UserDef.getInstance().getValueForKey( JACKPOT_INCREMENT_FACTOR + "_" + machinesString[ code ] );

                //log( "current time = %ld", time( 0 ) );
                this.jackPotInitialValue = this.jackPotInitialValue + ( ( ServerData.getInstance().current_time_seconds - this.startTime ) * this.incrementFactor * 20 );

            },
            resetJackpot :function( )
            {
                var def = UserDef.getInstance();
                def.deleteValueForKey( JACKPOT_START_TIME + "_" + machinesString[ this.mCode ] );

                def.deleteValueForKey( JACKPOT_INITIAL_VALUE + "_" + machinesString[ this.mCode ] );

                def.deleteValueForKey( JACKPOT_FINAL_VALUE + "_" + machinesString[ this.mCode ] );

                def.deleteValueForKey( JACKPOT_INCREMENT_FACTOR + "_" + machinesString[ this.mCode ] );

                JackPotManager.getInstance().setJackPotWithMachineCode( this.mCode );
                this.initVariables( this.mCode );
            }
            ,
            updateJackPot : function( node )
        {
            if( node )
            {
                this.jackPotInitialValue+=this.incrementFactor;
            }

            if( this.jackPotInitialValue >= this.jackPotFinalValue || this.jackPotInitialValue < 14000000 )
            {
                this.resetJackpot();
            }

            return this.jackPotInitialValue;
        },
            setInitialValueAfterForeGround : function()
            {
                this.jackPotInitialValue = UserDef.getInstance.getValueForKey( JACKPOT_INCREMENT_FACTOR + "_" + machinesString[ this.mCode ] ) +
                    ( ( ServerData.getInstance().current_time_seconds - this.startTime ) * this.incrementFactor * 20 );
            }
        };
    };
    return {
        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function ( cd ) {

                var instance = init();
                instance.initVariables( cd );

            return instance;
        }
    };
})();