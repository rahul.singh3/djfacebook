/**
 * Created by RNF-Mac11 on 1/19/17.
 */

CustomLabel = cc.LabelTTF.extend({


    ctor:function ( text, fontName, fontSize, parSize, dimensions, hAlignment, vAlignment ) {
        this.parentSize = parSize;
        this._minFontSize = 15;
        this.originalFontSize = fontSize;

        this._super( text, fontName, fontSize, dimensions, hAlignment, vAlignment );

        if (fontName && fontName instanceof cc.FontDefinition) {
            this.initWithStringAndTextDefinition(text, fontName);
        } else {
            cc.LabelTTF.prototype.initWithString.call(this, text, fontName, fontSize, dimensions, hAlignment, vAlignment);
        }

        return true;
    },
    originalFontSize : 0,
    _minFontSize : 0,
    parentSize : cc.size( 0, 0 ),

    setString: function (text) {
        text = String(text);
        if (this._originalText !== text) {
            this._originalText = text + "";

            this._updateString();

            // Force update
            this._setUpdateTextureDirty();
            this._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
        }


        if ( this.getFontSize() != this.originalFontSize )
        {
            this.setFontSize( this.originalFontSize );
        }

        while ( this.getContentSize().width >= this.parentSize.width )
        {
            this.setFontSize( this.getFontSize() - 1);
            if ( this.getFontSize() < this._minFontSize )
            {
                break;
            }
            else
            {
                this._updateString();

                // Force update
                this._setUpdateTextureDirty();
                this._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
            }
        }
    }
});