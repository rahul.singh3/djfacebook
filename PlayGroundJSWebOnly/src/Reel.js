/**
 * Created by RNF-Mac11 on 1/23/17.
 */

var TOTAL_REEL_ELEMNTS = 36;

var highestPayment =
    [
        [19, 19, 19],//1
        [13, 13, 13],//2
        [15, 21, 15],//3
        [31, 31, 31],//4
        [13, 13, 13],//5
        [7, 7, 7],//6
        [1, 13, 1],//7
        [35, 35, 35],//8
        [1, 33, 1],//9
        [16, 16, 16],//10
        [3, 3, 3],//11
        [3, 3, 3],//12
        [19, 7, 19],//13
        [21, 21, 21],//14
        [19, 7, 19],//15
        [13, 13, 13],//16
        [14, 14, 14],//17
        [14, 14, 14],//18
        [1, 1, 1],//19
        [34, 34, 22],//20
        [21, 21, 21],//21
        [14, 14, 14],//22
        [31, 31, 31],//23
        [19, 19, 19],//24
        [27, 27, 27],//25
    ];

Reel = cc.Node.extend({

    ctor:function ( machine_index, lIndex, typ, pArr ) {
        this._super();

        this.setReel( machine_index, lIndex, typ, pArr );

        var resizer = DJSResizer.getInstance();
        resizer.setFinalScaling( this );

        return true;
    },
    mCode : 0,

    type : 0,

    elementsShown : 0,

    timeElapsed : 0.0,

    parentScale : 0.0,

    vSize : cc.size( 0, 0 ),

    origin : cc.p( 0, 0 ),

    haveWild : false,

    rolling : false,

//variable added for reverse movement
//1 for normal rotation
//-1 for reverse rotation
    direction : 1,

    laneCode : 0,

    speed : 0,

    elementsVec : null,

    dispElementsVec : null,

    elementsPosVec : null,

    setReel : function( lvl, lIndx, typ, pArr )
    {
        this.elementsVec = new Array();

        this.dispElementsVec = new Array();

        this.elementsPosVec = new Array();

        this.haveWild = false;

        this.rolling = false;

        this.mCode = lvl;

        this.laneCode = lIndx;

        this.type = typ;

        this.elementsShown = 1;

        this.timeElapsed = 0;

        this.direction = 1;

        this.speed = 15000;

        this.vSize = cc.winSize;

        //elementsVec;

        switch ( lvl )
        {
            case DJS_X_MULTIPLIERS:
                this.parentScale = 0.5;
                break;

            default:
                this.parentScale = 1;
                break;
        }

        switch ( typ )
        {
            case reelTypes.PHYSICAL_REEL_TAG:
                this.setPhysicalReel( pArr );
                break;

            case reelTypes.BLUR_REEL_TAG:
                this.setBlurReel();
                break;

            default:
                break;
        }
    },

    setPhysicalReel : function( pArr )
    {
        var elm = TOTAL_REEL_ELEMNTS;

        if ( this.mCode == DJS_RESPIN )
        {
            elm = 45;
        }

        for ( var i = 0; i < elm; i++ )
        {
            var e;
            if( this.laneCode == 3 )
            {
                if( i > 5 )
                    break;
                var multiplierArr = [
                    ELEMENT_2X, ELEMENT_3X, ELEMENT_5X, ELEMENT_10X, ELEMENT_20X, ELEMENT_40X
                ];
                e = new CSElemnt( this.mCode, multiplierArr[i], i, this.laneCode );
            }
            else if( this.mCode == DJS_CRYSTAL_SLOT && this.laneCode == 2 )
            {

                e = new CSElemnt( this.mCode, reelsArrayDJSCrystalSlotWild1[i], i, this.laneCode );
            }
            else
            {
                e = new CSElemnt( this.mCode, pArr[i], i, this.laneCode );
            }

            this.elementsVec.push( e );
            if ( this.laneCode < 3 && i != highestPayment[this.mCode - DJS_QUINTUPLE_5X][this.laneCode] )
            {
                e.setOpacity(0);
            }
            else if( this.laneCode == 3 && this.mCode == DJS_RNF_STAR )
            {
                if (i < 5)
                {
                    e.setOpacity(0);
                }
            }
            e.setScale(this.parentScale);
            this.addChild(e);
        }
    },

    setBlurReel : function()
    {
        var totalBlurs = 0;
        switch ( this.mCode )
        {
            case DJS_CRAZY_CHERRY:
                totalBlurs = 5;
                break;

            case DJS_DIAMOND:
            case DJS_WILD_X:
            case DJS_FREEZE:
            case DJS_DOUBLE_DIAMOND:
                totalBlurs = 6;
                break;

            case DJS_RNF_STAR:
            case DJS_FIRE:
            case DJS_GOLDEN_SLOT:
            case DJS_HALLOWEEN_SLOT:
                totalBlurs = 7;
                break;

            case DJS_LIGHTNING_REWIND:
            case DJS_TRIPLE_PAY:
            case DJS_FREE_SPIN:
            case DJS_BONUS_MACHINE:
                totalBlurs = 8;
                break;

            case DJS_DIAMOND_3X:
            case DJS_RESPIN:
            case DJS_SPIN_TILL_YOU_WIN:
            case DJS_FREE_SPINNER:
            case DJS_SPIN_TILL_YOU_WIN_TWIK:
            case DJS_LIGHTNING_REWIND_TWIK:
            case DJS_MAGNET_SLIDE:
                totalBlurs = 9;
                break;

            case DJS_CRYSTAL_SLOT:
                totalBlurs = 10;
                break;

            case DJS_QUINTUPLE_5X:
            case DJS_X_MULTIPLIERS:
            case DJS_NEON_SLOT:
                totalBlurs = 11;
                break;

            case DJS_DIAMONDS_FOREVER:
                totalBlurs = 12;
                break;

            default:
                break;
        }

        var num = 0;
        var prevNum = 0;

        for (var i = 1; i <= totalBlurs; i++ )
        {
            while (num == prevNum)
            {
                if( this.laneCode == 3 )
                {
                    num = Math.floor( Math.random() * 6 ) + 8;
                }
                else
                {
                    num = Math.floor( Math.random() * totalBlurs ) + 1;
                    if ( this.mCode == DJS_DIAMOND_3X && this.laneCode != 2 )
                    {
                        while ( num == 3 && num == prevNum )
                        {
                            num = Math.floor( Math.random() * totalBlurs)+ 1;
                        }
                    }
                }
            }
            prevNum = num;

            var string = "#blur_" + parseInt( num ) + ".png";
            var e = new cc.Sprite( string );
            e.setPosition( cc.p( 0, e.getContentSize().height * 0.75 * ( i - 1 ) ) );
            this.elementsPosVec.push( e.getPositionY() );
            this.elementsVec.push( e );
            e.setOpacity( 0 );
            e.setScale( this.parentScale );
            this.addChild( e );
        }
    },

    clearWildReel : function()
    {
        for (var i = 0; i < this.elementsVec.length; i++ )
        {
            var s = this.elementsVec[ i ];
            s.removeFromParent( true );
        }
        this.elementsVec = [];
        this.elementsPosVec = [];
        this.dispElementsVec = [];

        this.setWildReel(wildArr);
    },

    getMString : function()
    {
        var mString = null;
        var delegate = AppDelegate.getInstance();
        switch ( this.mCode )
        {
            case DJS_X_MULTIPLIERS:
                mString = "DJSQuintuple5x/";
                break;

            default:
                mString = machinesString[this.mCode - DJS_QUINTUPLE_5X] + "/";
                break;
        }

        return mString;
    },

    reSetElements : function()
    {
        for (var i = 0; i < this.elementsVec.length; i++)
        {
            var s = this.elementsVec[i];
            s.setScale( this.parentScale );
        }
    },

    resetPosition : function( dir )
    {
        var spr;

        var pos = 0.0;

        this.elementsPosVec = [];

        for (var i = 0; i < this.elementsVec.length; i++ )
        {
            spr = this.elementsVec[i];
            pos = spr.getContentSize().height * 1.5 * i * dir;

            spr.setPositionY(pos);
            this.elementsPosVec.push(pos);
            //cc.log( "in resetPosition posY = " + pos + ", at i = " + i );
        }
    },

    setFadeIn : function()
    {

        for (var i = 0; i < this.elementsVec.length; i++)
        {
            var s = this.elementsVec[i];
            s.runAction( new cc.FadeIn( 0.2 ) );
        }
    },

    setFadeOut : function( isForcedStop )//change 6 Sept
    {
        var time = 0.2;

        if ( isForcedStop )
        {
            time = 0.1;
        }

        for ( var i = 0; i < this.elementsVec.length; i++ )
        {
            var s = this.elementsVec[i];
            s.runAction( new cc.FadeOut( time ) );
        }
    },

    setParentScale : function()
    {
        for (var i = 0; i < this.elementsVec.length; i++)
        {
            var s = this.elementsVec[i];
            s.setScale( this.parentScale );

            var gafObject = s.getChildByTag( GAF_OBJECT_TAG );
            if ( gafObject )
            {
                if( this.mCode == DJS_HALLOWEEN_SLOT && gafObject.isVisible() )//#Dp change 29 Aug
                {
                    var e = s;
                    if( e.eCode === ELEMENT_2X || e.eCode === ELEMENT_5X )//2122, 2125
                    {
                        log( "e.eCode = " + e.eCode );
                        e.setOpacity( 255 );
                    }
                }

                gafObject.stop();
                gafObject.setVisible( false );
            }
        }
    },

    setWildReel : function( pArr )
    {
        var totalWilds = 36;
        if (this.mCode == DJS_WILD_X)
        {
            totalWilds = 7;
        }
        for (var i = 0; i < totalWilds; i++)
        {
            var e = new CSElemnt( this.mCode, pArr[i], i, this.laneCode );
            e.setPosition( 0, e.getContentSize().height * (i * 1.5) );
            this.elementsVec.push( e );
            this.elementsPosVec.push( e.getPositionY() );
            e.setOpacity( 0 );
            e.setScale( this.parentScale );
            this.addChild( e );
        }
    },

    setWildBlurReel : function(  pArr )
    {
        var totalWilds = 36;

        for (var i = 0; i < totalWilds; i++ )
        {
            var e = new CSElemnt(this.mCode, pArr[i], i, this.laneCode);
            e.setPosition(0, e.getContentSize().height * (i));
            this.elementsVec.push(e);
            this.elementsPosVec.push(e.getPositionY());
            e.setOpacity(0);
            e.setScale(this.parentScale);
            this.addChild(e);
        }
    },

    startRolling : function()
    {
        this.timeElapsed = 0;
        this.rolling = true;
    },

    stopRolling : function( isForced )
    {

        this.rolling = false;
        this.setFadeOut( isForced );
    },

    updateRoll : function(dt)
    {
        if (this.mCode == DJS_FREE_SPIN)
        {
            var game = this.getParent().getParent().getParent();
            if (game.wildLaneCode[this.laneCode] > 0)
            {
                this.rolling = false;
                return;
            }
        }

        //timeElapsed+=dt;
        var spr = null;
        var pos;
        this.timeElapsed+=dt;
        this.speed = 4000;

        for (var i = 0; i < this.elementsVec.length; i++)
        {
            spr = this.elementsVec[i];
            pos = this.elementsPosVec[i];
            spr.setPositionY( pos - (this.speed * this.timeElapsed * this.direction) );
        }

        if (this.direction == 1)
        {
            spr = this.elementsVec[0];
            if( cc.rectGetMaxY( spr.getBoundingBox() ) <= -spr.getContentSize().height)
            {
                this.elementsVec.splice( 0, 1 );
                this.elementsPosVec.splice( 0, 1 );
                var sprite = this.elementsVec[this.elementsVec.length - 1];
                spr.setPositionY(sprite.getPositionY() + sprite.getContentSize().height * 1.5);
                this.elementsVec.push(spr);
                this.elementsPosVec.push(spr.getPositionY());

                this.timeElapsed = 0;
                this.elementsPosVec = [];
                for (var i = 0; i < this.elementsVec.length; i++)
                {
                    spr = this.elementsVec[i];

                    this.elementsPosVec.push(spr.getPositionY());
                }
            }
        }
        else
        {
            spr = this.elementsVec[0];
            //log( "minY = %f", spr.getBoundingBox().getMinY() );
            if( cc.rectGetMinY(spr.getBoundingBox()) >= this.vSize.height )
            {
                this.elementsVec.splice(0, 1);
                this.elementsPosVec.splice(0, 1);
                var sprite = this.elementsVec[this.elementsVec.length - 1];
                spr.setPositionY(sprite.getPositionY() - sprite.getContentSize().height * 1.5);
                this.elementsVec.push(spr);
                this.elementsPosVec.push(spr.getPositionY());

                this.timeElapsed = 0;
                this.elementsPosVec = [];
                for (var i = 0; i < this.elementsVec.length; i++)
                {
                    spr = this.elementsVec[i];

                    this.elementsPosVec.push(spr.getPositionY());
                }
            }
        }
    },

    updateWildRoll : function( dt )
    {
        var game = this.getParent().getParent().getParent();
        var spr = null;
        var pos;
        game.timeElapsedWild+=dt;

        var sped = 1000;
        if (this.mCode == DJS_FREE_SPIN)
        {
            sped = 4000;
        }

        for (var i = 0; i < this.elementsVec.length; i++)
        {
            spr = this.elementsVec[i];
            pos = this.elementsPosVec[i];
            spr.setPositionY( pos - (sped * game.timeElapsedWild) );
        }

        spr = this.elementsVec[0];
        if( cc.rectGetMaxY( spr.getBoundingBox() ) <= - spr.getContentSize().height)
        {
            this.elementsVec.splice( 0, 1 );
            this.elementsPosVec.splice( 0, 1 );
            var sprite = this.elementsVec[this.elementsVec.length - 1];
            spr.setPositionY(sprite.getPositionY() + sprite.getContentSize().height * 1.5);
            this.elementsVec.push(spr);
            this.elementsPosVec.push(spr.getPositionY());

            game.timeElapsedWild = 0;
            this.elementsPosVec = [];
            for ( var i = 0; i < this.elementsVec.length; i++ )
            {
                spr = this.elementsVec[i];

                this.elementsPosVec.push( spr.getPositionY() );
            }
            game.wildRotationCount++;
        }

        if (game.wildRotationCount == game.wildIndx[game.currentWildIndx] + 7)
        {

            game.removeChildByTag( WILD_ANIM_TAG);
            var e = game.wildElms[game.currentWildIndx];
            e.aCode = SCALE_TAG;
            e.animateNow();

            var multiplier = 1;
            switch (wildArr[game.wildIndx[game.currentWildIndx]])
            {
                case ELEMENT_2X:
                    multiplier = 2;
                    break;

                case ELEMENT_3X:
                    multiplier = 3;
                    break;

                case ELEMENT_5X:
                    multiplier = 5;
                    break;

                case ELEMENT_10X:
                    multiplier = 10;
                    break;

                case ELEMENT_15X:
                    multiplier = 15;
                    break;

                case ELEMENT_20X:
                    multiplier = 20;
                    break;

                case ELEMENT_25X:
                    multiplier = 25;
                    break;

                default:
                    break;
            }


            var currentWin = game.currentWin;
            currentWin*=multiplier;

            var i;
            for (i = 0; i < 3; i++)
            {
                if (wildArr[game.wildIndx[i]] != ELEMENT_25X)
                {
                    break;
                }
            }
            if (i == 3)
            {
                currentWin = game.payOutVec[0] * game.currentBet;//23 Feb
            }
            game.currentWin = currentWin;


            game.unschedule( game.rollUpdatorWild );

            game.isScheduled = false;

            game.startWildRolling();

            if (!game.isScheduled/*( DJSWildX.rollUpdatorWild )*/ )
            {
                game.addWin = currentWin / 10;
                game.setCurrentWinAnimation();
                game.isRolling = false;
                game.haveWild = false;//27 May
                game.checkAndSetAPopUp();

                if ( game.isAutoSpinning )
                {
                    if (currentWin > 0)
                    {
                        game.scheduleOnce( game.autoSpinCB, 1.5 );
                    }
                    else
                    {
                        game.scheduleOnce( game.autoSpinCB, 1.0 );
                    }
                }
            }
        }
    }


});