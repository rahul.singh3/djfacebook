/**
 * Created by RNF-Mac11 on 6/20/17.
 */

GiftPage = cc.Layer.extend({



    ctor:function ( code ) {
        this._super( );
        //var pRet = new CustomButton();
        //if ( pRet && pRet.init() ){
        this.create( code );
        return true;
        //}
    },

    create:function ( code ) {

        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        this.addChild( darkBG );

        var vSize = cc.winSize;

        var bgSprite = new cc.Sprite( "#giftPop/bg.png" );
        bgSprite.setPosition( vSize.width / 2, vSize.height / 2 );
        bgSprite.setVisible( false );

        var giftPop = new FBGiftPopUp();

        this.addChild( giftPop, 0, 1 );
        this.addChild( new FBGiftReceptionPopUp(), 0, 2 );

        if( FacebookObj.receivedGiftsDetails )
        {
            this.getChildByTag( 2 ).setLocalZOrder( 1 );
        }
        else
        {
            this.getChildByTag( 1 ).setLocalZOrder( 1 );
        }

        var closeItem = new cc.MenuItemImage(res.Close_Buttpn_png, res.Close_Buttpn_png, this.menuCB, this );

        closeItem.setPosition( cc.rectGetMaxX( bgSprite.getBoundingBox() ) - closeItem.getContentSize().width / 4, cc.rectGetMaxY( bgSprite.getBoundingBox() ) - closeItem.getContentSize().height / 4 );
        closeItem.setTag( CLOSE_BTN_TAG );


        var buyMenu = new cc.Menu();

        buyMenu.setPosition( cc.p( 0, 0 ) );
        this.addChild( buyMenu, 5 );

        buyMenu.addChild( closeItem );

        DPUtils.getInstance().setTouchSwallowing( 100, this );
    },
    menuCB : function( sender )
    {
        var itm = sender;

        var game = this.getParent();

        var data = ServerData.getInstance();

        var deal = AppDelegate.getInstance().getDealInstance();//DJSDeal.getInstance();

        var def = UserDef.getInstance();

        var delegate = AppDelegate.getInstance();

        switch ( itm.getTag() )
        {
            case SEE_MORE_BTN_TAG:
                switch ( data.server_buy_page )
                {
                    default:
                        game.setBuyPage(BuyPageTags.BUY_CREDITS);
                        break;

                    case 2:
                        if (this.def.getValueForKey(IS_BUYER, false)) {
                            game.setBuyPage(BuyPageTags.BUY_CREDITS);
                        }
                        else {
                            game.setBuyPage(BuyPageTags.FIRST_TIME_CREDITS);
                        }
                        break;
                    case 3:
                    {
                        game.setBuyPage(BuyPageTags.DOUBLE_CREDITS);
                    }
                        break;
                    case 4:
                    {
                        game.setBuyPage(BuyPageTags.OFFER_CREDITS);

                    }
                        break;
                }
                delegate.showingScreen=false;
                this.removeFromParent( true );
                def.setValueForKey( BUY_BUTTON_CLICK_COUNT, def.getValueForKey( BUY_BUTTON_CLICK_COUNT, 0 ) + 1 );
                break;

            case CLOSE_BTN_TAG:
                delegate.showingScreen=false;
                this.getParent().showingScreen = false;
                this.removeFromParent( true );
                //ServerCommunicator.getInstance().SCUpdateData();
                //game.keyListner.setEnabled( true );//change 1 Sept
                break;

            default:
                //if ( this.getParent().getParent() && !this.getParent().getParent().getChildByTag( LOADER_TAG ) )
            {
                IAPHelper.getInstance().purchase( itm.getTag(), this.offerCode );
            }
                break;
        }
        delegate.jukeBox( S_BTN_CLICK );
    },

    handleScroll : function( event )
    {
        if( this.getChildByTag( 1 ).getLocalZOrder() == 1 )
        {
            this.getChildByTag( 1 ).handleScroll( event );
        }
        else
        {
            this.getChildByTag( 2 ).handleScroll( event );
        }
    },

    handleGiftSendingResponse : function( response )
    {
        if( this.getChildByTag( 1 ).getLocalZOrder() == 1 )
        {
            this.getChildByTag( 1 ).makeEntriesOfSentGifts( response );
            this.getChildByTag( 1 ).sendInvitations();
        }
        else
        {
            this.getChildByTag( 2 ).giftCollectionRewards();
        }
    }

});