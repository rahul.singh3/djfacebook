BonusTimer = (function () {
    // Instance stores a reference to the Singleton
    var instance;

    function init() {
        return{
             hourlyBonusTime:0,
             dailyBonusTime:0,
             offerExpireTime:0,
            _percentValue:0,
             daliyChanllengeTime:0,
            _haveDailyBonus:false,
            _haveHourBonus:false,
            _havedaliyChanllengeEnd:false,
            intializeVar:function() {
                var defData = UserDef.getInstance();
                this.hourlyBonusTime = defData.getValueForKey("bonus_hour");
                this.dailyBonusTime = defData.getValueForKey("bonus_day");
                this.daliyChanllengeTime = defData.getValueForKey("daily_challenge_time");
                 this.updateTimeForHourlyBonus();
                 this.updateTimeForDaliyBonus();
                 this.updateTimeForDaliyChanllenge();
               // cc.director.getScheduler().unschedule("manageTimePerSecond",this);
                //cc.director.getScheduler().schedule(this.manageTimePerSecond,this,1,false,"manageTimePerSecond");
            },
            updateTimeForHourlyBonus:function () {
                var totalTime = this.hourlyBonusTime;
                this._haveHourBonus = this.hourlyBonusTime <= 0 ? true : false;
                if ( totalTime < 3600 )
                {
                    this._percentValue = ( ( ( 3600.0 - totalTime ) / 3600.0 ) * 100.0 );
                }
                else
                {
                    this._percentValue = 100 - ( ( ( totalTime - 3600.0 ) / totalTime ) * 100.0 );
                }
                return this.getTimeAccToTwentyFourHours(totalTime,totalTime);
            },

            updateTimeForDaliyBonus:function () {
                var totalTime = this.dailyBonusTime;
                this._haveDailyBonus = this.dailyBonusTime <= 0 ? true : false;
                 return this.getTimeAccToTwentyFourHours(totalTime,totalTime);
            },
            _percentDailyBonus:0,
            updateTimeForDaliyChanllenge:function () {
                var totalTime = this.daliyChanllengeTime;
                this._havedaliyChanllengeEnd = this.daliyChanllengeTime <= 0 ? true : false;
                var  cTime = totalTime;
                var string = this.getTimeAccToTwentyFourHours(totalTime,totalTime);
                if ( cTime < 3600 )
                {
                    this._percentDailyBonus = ( ( ( 86400.0 - cTime ) / 86400.0 ) * 100.0 );
                }
                else
                {
                    this._percentDailyBonus = 100 - ( ( ( cTime - 86400.0 ) / cTime ) * 100.0 );
                }
                return string;
            },
            getTimeAccToTwentyFourHours:function(totalTime,cTime){

                var hour = parseInt(totalTime / 3600);
                totalTime%=3600;
                var min = parseInt(totalTime / 60);
                var sec = totalTime % 60;

                var secStr = null;
                var minStr = null;
                var hourStr = null;

                sec < 10 ? secStr = "0" + sec : secStr = "" + sec;


                min < 10 ? minStr = "0" + min : minStr = "" + min;


                hour < 10 ? hourStr = "0" + hour : hourStr = "" + hour;

                var string = hourStr + ":" + minStr + ":" + secStr;
                return string;
            },
            manageTimePerSecond:function () {
                BonusTimer.getInstance().backgroundTimerHandler(1);
            },
            setTime:function(key,t){
                UserDef.getInstance().setValueForKey( key, t );
                if (key === "bonus_hour"){
                   this.hourlyBonusTime = t;
                }else if (key === "bonus_day"){
                    this.dailyBonusTime = t;

                }else if (key === "daily_challenge_time"){
                    this.daliyChanllengeTime = t;

                }
            },
            getTime : function( key )
            {
                var time = 0;
                var def = UserDef.getInstance();
                var t = def.getValueForKey( key, 0 );
                time = t;
                if ( key == HAPPY_HOURS )
                {
                    if ( time <= 0 )
                    {
                        time = -1;
                    }
                }
                return time;
            },
            backgroundTimerHandler:function (dt) {
                 if (ServerData.getInstance().configCode==0){
                     return;
                 }
                var bonusObj = BonusTimer.getInstance();
                if (bonusObj.hourlyBonusTime > 0)
                    bonusObj.hourlyBonusTime = (bonusObj.hourlyBonusTime - dt) > 0 ? bonusObj.hourlyBonusTime - dt : 0;

                if (bonusObj.dailyBonusTime > 0)
                    bonusObj.dailyBonusTime = (bonusObj.dailyBonusTime - dt) > 0 ? bonusObj.dailyBonusTime - dt : 0;
                if (bonusObj.daliyChanllengeTime > 0) {
                    bonusObj.daliyChanllengeTime = (bonusObj.daliyChanllengeTime - dt) > 0 ? bonusObj.daliyChanllengeTime - dt : 0;
                }else {
                    bonusObj.daliyChanllengeTime = 24*60*60;
                    var scene = cc.director.getRunningScene();
                    var layer = scene.getChildByTag( sceneTags.LOBBY_SCENE_TAG );
                    if ( layer )
                    {
                        var lobby = layer;
                         lobby.removeDailyChanges();
                    } else {
                        layer = scene;//.getChildByTag( sceneTags.GAME_SCENE_TAG );
                        if (layer.getTag() == sceneTags.GAME_SCENE_TAG) {
                            var game = layer;
                            game.removeDC();
                        }
                    }
                    VSDailyChallenge.getInstance().refreshDailyChallengesRecord(true,null);
                }
                bonusObj._haveHourBonus = bonusObj.hourlyBonusTime > 0 ? false : true;
                bonusObj._haveDailyBonus = bonusObj.dailyBonusTime > 0 ? false : true;
                bonusObj._havedaliyChanllengeEnd = bonusObj.daliyChanllengeTime > 0 ? false : true;
               var userDataObj = userDataInfo.getInstance();
                if(userDataObj.deal_remaining_seconds > 0){
                    userDataObj.deal_remaining_seconds =  (userDataObj.deal_remaining_seconds-dt) > 0  ?  userDataObj.deal_remaining_seconds-dt : 0;
                    userDataObj.deal_reflush_remaining_seconds = -1; // for buyer deal show only one time
                }
                ServerData.getInstance().timeTicker();
            }
        };
    };
    return {
        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function () {
            if ( !instance ) {
                instance = init();
                instance.intializeVar();
            }
            return instance;
        }
    };
})();