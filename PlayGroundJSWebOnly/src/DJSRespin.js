/**
 * Created by RNF-Mac11 on 2/10/17.
 */



var reelsRandomizationArrayDJSRespin =
    [
        [// 85% payout
            35, 4, 30, 7, 60, 2,
            40, 6, 20, 60, 2, 70,
            2 , 70 ,1, 80, 1, 10,
            35, 5, 60, 50, 5, 5,
            45, 5, 30, 5, 55, 40,
            10, 3, 50, 3, 45,3 ,
            40, 3 ,10, 15 ,40, 15,
            60, 50 ,10
        ],
        [//92 %
            32, 4, 30, 8, 60, 3,

            40, 6, 20, 60, 2, 70,

            2 , 70 ,1, 80, 1, 10,

            35, 5, 65, 50, 5, 5,

            45, 5, 35, 5, 55, 40,

            10, 3, 50, 3, 45,3 ,

            40, 3 ,15, 15 ,40, 15,

            60, 40 ,10
        ],
        [
            //95 - 97% payour
            35, 5, 40, 8, 60, 3,
            40, 7, 10, 60, 2, 70,
            2 , 70 ,1, 70, 1, 10,
            30, 5, 65, 50, 5, 5,
            40, 5, 40, 5, 50, 40,
            10, 3, 50, 3, 45,3 ,
            50, 3 ,25, 15 ,40, 15,
            60, 50 ,10
        ],
        [// 140+ % payout
            50, 6, 75, 10, 70, 5,
            75, 8, 40, 40, 4, 50,
            2 , 55 ,2, 55, 2, 10,
            40, 3, 75, 40, 5, 3,
            50, 3, 50, 3, 60, 40,
            2, 1, 60, 1, 55,1 ,
            40, 1 ,50, 3 ,50, 3,
            70, 100 ,2
        ],
        [// 300+ % payout
            50, 6, 75, 10, 70, 5,
            75, 8, 40, 40, 4, 50,
            2 , 55 ,2, 55, 2, 10,
            40, 3, 75, 40, 5, 3,
            50, 3, 50, 3, 60, 40,
            2, 1, 60, 1, 55,1 ,
            40, 1 ,50, 3 ,50, 3,
            70, 100 ,2
        ]
    ];

var DJSRespin = GameScene.extend({
    calcCount : 0,

    dummyWildElemnt : null,

    wildElemnt : null,
    reelsArrayDJSRespin:[],
    ctor:function ( level_index ) {
        this.reelsArrayDJSRespin = [
            //no xx element near empty elements or other xx element
            /*1*/ELEMENT_BAR_2,/*2*/ ELEMENT_4XX,/*3*/ ELEMENT_7_BAR,/*4*/ ELEMENT_2XX,/*5*/ ELEMENT_7_RED,
            /*6*/ELEMENT_5XX,/*7*/ELEMENT_BAR_3,/*8*/ ELEMENT_3XX,/*9*/ ELEMENT_BAR_1,/*10*/ ELEMENT_EMPTY,
            /*11*/ELEMENT_2X,/*12*/ELEMENT_EMPTY,/*13*/ELEMENT_3X,/*14*/ ELEMENT_EMPTY,/*15*/ELEMENT_4X,
            /*16*/ELEMENT_EMPTY,/*17*/ ELEMENT_5X,/*18*/ ELEMENT_EMPTY,/*19*/ELEMENT_BAR_3,/*20*/ ELEMENT_EMPTY,
            /*21*/ELEMENT_7_BAR,/*22*/ ELEMENT_EMPTY,/*23*/ELEMENT_BAR_1,/*24*/ELEMENT_EMPTY,/*25*/ELEMENT_BAR_2,
            /*26*/ ELEMENT_EMPTY,/*27*/ ELEMENT_7_RED,/*28*/ ELEMENT_EMPTY,/*29*/ELEMENT_BAR_3,/*30*/ ELEMENT_EMPTY,
            /*31*/ELEMENT_BAR_1,/*32*/ELEMENT_EMPTY,/*33*/ ELEMENT_BAR_2,/*34*/ ELEMENT_EMPTY,/*35*/ ELEMENT_BAR_1,
            /*36*/ ELEMENT_EMPTY,/*37*/ELEMENT_7_BAR,/*38*/ELEMENT_EMPTY,/*39*/ELEMENT_7_RED,/*40*/ELEMENT_EMPTY,
            /*41*/ELEMENT_BAR_3,/*42*/ELEMENT_EMPTY,/*43*/ELEMENT_BAR_2,/*44*/ELEMENT_EMPTY,/*45*/ELEMENT_BAR_1
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSRespin;
        this.physicalArrayCheck= this.reelsArrayDJSRespin;
        this._super( level_index );

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;


    },

    animCBR : function( obj)
    {
        obj.removeFromParent(true);

        this.isRolling = true;
        this.startRolling(true);
    },

    calculatePayment : function( doAnimate )
    {
        var payMentArray = [

            /*0*/    1000,// 2x 5x 2x

            /*1*/    40,// 2x 4x 2x

            /*2*/    30,// 2x 3x 2x

            /*3*/    45, // 7 7 7 Red

            /*4*/    25,  // 7Bar 7Bar 7Bar

            /*5*/    15,  // Triple Bar

            /*6*/    10,  // Double Bar

            /*7*/    5,   // single Bar

            /*8*/    3   // any bar

            ///*9*/    5,   // Single Bar 7Bar combo

        ];

        this.calcCount++;

        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        for (var i = 0; i < this.slots[0].resultReel.length; i++)
        {
            tmpVector = this.slots[0].resultReel[i];
            tmpVector2.push(tmpVector[1]);
            if (this.wildAnimating && i == 0 && this.calcCount > 1)
            {
                tmpVector2.push(this.dummyWildElemnt);
            }

        }

        this.lineVector.push(tmpVector2);
        var winScenario = 7;
        var lineLength = 3;
        var pay = 0;
        var tmpPay = 1;
        var count2X = 0;
        var count3X = 0;
        var count4X = 0;
        var count5X = 0;
        var countWild = 0;
        var countGeneral = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var i, j, k;
        var e = null;
        var pivotElm = null;
        var pivotElm2 = null;
        var tmpVec = new Array();


        for (i = 0; i < this.totalLines; i++)
        {
            tmpVec = this.lineVector[i];
            for (k = 0; k < winScenario; k++)
            {
                var breakLoop = false;
                countGeneral = 0;
                switch (k)
                {
                    case 0://for all combination with wild
                        for (j = 0; j < tmpVec.length; j++)
                        {
                            if (this.reelsArrayDJSRespin[this.resultArray[i][j]] != -1 )
                            {
                                switch (this.reelsArrayDJSRespin[this.resultArray[i][j]])
                                {
                                    case ELEMENT_2X:
                                    case ELEMENT_2XX:
                                        count2X++;
                                        tmpPay*=2;
                                        countWild++;
                                        if ( doAnimate) {
                                            e = tmpVec[j];
                                        }

                                        if (e)
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_3X:
                                    case ELEMENT_3XX:
                                        count3X++;
                                        tmpPay*=3;
                                        countWild++;
                                        if ( doAnimate) {
                                            e = tmpVec[j];
                                        }
                                        if (e)
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_4X:
                                    case ELEMENT_4XX:
                                        count4X++;
                                        tmpPay*=4;
                                        countWild++;
                                        if ( doAnimate) {
                                            e = tmpVec[j];
                                        }
                                        if (e)
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_5X:
                                    case ELEMENT_5XX:
                                        count5X++;
                                        tmpPay*=5;
                                        countWild++;
                                        if ( doAnimate) {
                                            e = tmpVec[j];
                                        }
                                        if (e)
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        switch (pivotElement)
                                        {
                                            case -1:
                                                pivotElement = this.reelsArrayDJSRespin[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate) {
                                                    e = tmpVec[j];
                                                }
                                                if (e)
                                                {
                                                    pivotElm = e;
                                                }
                                                break;

                                            default:
                                                pivotElement2 = this.reelsArrayDJSRespin[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate) {
                                                    e = tmpVec[j];
                                                }
                                                if (e)
                                                {
                                                    pivotElm2 = e;
                                                }
                                                break;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }

                        if (j == 3)
                        {
                            if (count2X == 2 && count5X == 1)
                            {
                                pay = payMentArray[0];
                                breakLoop = true;
                            }
                            else if(countWild >= 2)
                            {
                                breakLoop = true;

                                switch (pivotElement)
                                {
                                    case ELEMENT_7_RED:
                                        pay = tmpPay * payMentArray[3];
                                        if ( doAnimate) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_7_BAR:
                                        pay = tmpPay * payMentArray[4];
                                        if ( doAnimate) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_3:
                                        pay = tmpPay * payMentArray[5];
                                        if ( doAnimate) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_2:
                                        pay = tmpPay * payMentArray[6];
                                        if ( doAnimate) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_1:
                                        pay = tmpPay * payMentArray[7];
                                        if ( doAnimate) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        pay = tmpPay;
                                        if (pivotElm)
                                        {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;
                                }
                            }
                            else if(countWild == 1)
                            {
                                breakLoop = true;

                                if (pivotElement == pivotElement2)
                                {
                                    switch (pivotElement)
                                    {
                                        case ELEMENT_7_RED:
                                            pay = tmpPay * payMentArray[3];
                                            if ( doAnimate) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }

                                            break;

                                        case ELEMENT_7_BAR:
                                            pay = tmpPay * payMentArray[4];
                                            if ( doAnimate) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_3:
                                            pay = tmpPay * payMentArray[5];
                                            if ( doAnimate) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_2:
                                            pay = tmpPay * payMentArray[6];
                                            if ( doAnimate) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_1:
                                            pay = tmpPay * payMentArray[7];
                                            if ( doAnimate) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        default:
                                            pay = tmpPay;
                                            break;
                                    }
                                }
                                else
                                {
                                    if((pivotElement == ELEMENT_BAR_1 || pivotElement == ELEMENT_7_BAR) &&
                                        (pivotElement2 == ELEMENT_BAR_1 || pivotElement2 == ELEMENT_7_BAR))
                                    {
                                        pay = tmpPay * payMentArray[7];
                                        if ( doAnimate) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if ( ( ( pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_BAR_3 ) || pivotElement == ELEMENT_7_BAR ) &&
                                        ( ( pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_BAR_3 ) || pivotElement2 == ELEMENT_7_BAR ) )
                                    {
                                        pay = tmpPay * payMentArray[8];
                                        if ( doAnimate) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else
                                    {
                                        pay = tmpPay;
                                    }
                                }
                            }
                        }
                        break;

                    case 1: // 7 7 7 red
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSRespin[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[3];
                            breakLoop = true;

                            for (var l = 0; l < countGeneral; l++) {
                                if ( doAnimate) {
                                    e = tmpVec[l];
                                }

                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 2: // 7 7 7 bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSRespin[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[4];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                                if ( doAnimate) {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 3: // 3Bar 3Bar 3Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSRespin[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[5];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                                if ( doAnimate) {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 4: // 2Bar 2Bar 2Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSRespin[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[6];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                                if ( doAnimate) {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 5: // Bar Bar Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSRespin[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[7];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                                if ( doAnimate) {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 6: // any Bar and 7Bar combo
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSRespin[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[8];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++)
                            {
                                if ( doAnimate) {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

                if (breakLoop)
                {
                    break;
                }
            }
        }


        if ( doAnimate )
        {
            this.currentWin = this.currentBet * pay;
            if (this.currentWin > 0)
            {
                this.addWin = this.currentWin / 10;
                this.setCurrentWinAnimation();
            }
            if (this.calcCount > 1)
            {
                this.wildAnimating = false;
                this.isRolling = false;
                this.resultArray[0][1] = 0;//@Dp TO be changed after biasing

                if (this.isAutoSpinning)
                {
                    if (this.currentWin > 0)
                    {
                        this.scheduleOnce(this.autoSpinCB, 1.5);
                    }
                    else
                    {
                        this.scheduleOnce(this.autoSpinCB, 1.0);
                    }
                }
            }
            switch (this.reelsArrayDJSRespin[this.resultArray[0][1]])
            {
                case ELEMENT_2XX:
                case ELEMENT_3XX:
                case ELEMENT_4XX:
                case ELEMENT_5XX:
                    if (this.slots[0].isForcedStop && !this.wildAnimating)
                    {
                        this.setDummyWild();
                    }
                    break;
            }

            this.checkAndSetAPopUp();
        }
        else
        {
            this.currentWinForcast = this.currentBet * pay;
        }
    },
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSRespin[i]=this.physicalReelElements[idx];
                i++;
            }
        }
    },
    changeTotalProbabilities : function()
    {
        this.totalProbalities = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            //this.extractArrayFromSring();
            for ( var i = 0; i < 45; i++ )
            {
                this.totalProbalities+=this.probabilityArray[this.easyModeCode][i];
            }
        }
        else
        {
            for ( var i = 0; i < 45; i++ )
            {
                this.probabilityArray[this.easyModeCode][i] = reelsRandomizationArrayDJSRespin[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSRespin[this.easyModeCode][i];
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
        if ( this.slots[0].isForcedStop || this.wildAnimating )
        {
            return;
        }

        switch (this.reelsArrayDJSRespin[this.resultArray[0][1]])
        {
            case ELEMENT_2XX:
            case ELEMENT_3XX:
            case ELEMENT_4XX:
            case ELEMENT_5XX:
                this.setWildAnimation(this.wildElemnt);
                this.isRolling = true;
                this.unschedule(this.reelStopper);
                break;
        }
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
    },

    generateResult : function( scrIndx )
    {
        if ( this.isWildGeneration )
        {
            this.generateResultWild();
        }
        else
        {
            var havePayment = false;
            for ( var i = 0; i < this.slots[scrIndx].displayedReels.length; i++ )
            {
                //generate:
                for( ; ; ) {
                    var num = this.prevReelIndex[i];

                    var tmpnum = 0;

                    while (num == this.prevReelIndex[i]) {
                        num = Math.floor(Math.random() * this.totalProbalities);
                    }
                    this.prevReelIndex[i] = num;
                    this.resultArray[0][i] = num;

                    for (var j = 0; j < 45; j++) {
                        tmpnum += this.probabilityArray[this.easyModeCode][j];

                        if (tmpnum >= this.resultArray[0][i]) {
                            this.resultArray[0][i] = j;
                            break;
                        }
                    }

                    /*/HACK
                     if (i == 0)
                     {
                     resultArray[0][i] = 2;//2;
                     }
                     else if (i == 1)
                     {
                     resultArray[0][i] = 3;//1357
                     }
                     else if(i == 2)
                     {
                     resultArray[0][i] = 2;
                     }
                     else
                     {
                     resultArray[0][i] = 0;
                     }//*///HACK

                    if (this.reelsArrayDJSRespin[this.resultArray[0][i]] == ELEMENT_2XX || this.reelsArrayDJSRespin[this.resultArray[0][i]] == ELEMENT_3XX ||
                        this.reelsArrayDJSRespin[this.resultArray[0][i]] == ELEMENT_4XX || this.reelsArrayDJSRespin[this.resultArray[0][i]] == ELEMENT_5XX) {
                        if (i != 1) {
                            //goto
                            //generate;
                        }
                        else {
                            havePayment = true;
                            break;
                        }
                    }
                    else if (this.reelsArrayDJSRespin[this.resultArray[0][i]] == ELEMENT_2X || this.reelsArrayDJSRespin[this.resultArray[0][i]] == ELEMENT_3X ||
                        this.reelsArrayDJSRespin[this.resultArray[0][i]] == ELEMENT_4X || this.reelsArrayDJSRespin[this.resultArray[0][i]] == ELEMENT_5X) {
                        if (i == 1) {
                            //goto
                            //generate;
                        }
                        else {
                            havePayment = true;
                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
    },

    generateResultWild : function()
    {
        for ( var i = 0; i < 3; i++ )
        {
            if ( i != 1 )
            {
                for( ; ; ) {
                    //generate:
                    var num = this.prevReelIndex[i];

                    var tmpnum = 0;

                    while (num == this.prevReelIndex[i]) {
                        num = Math.floor(Math.random() * this.totalProbalities);
                    }
                    this.prevReelIndex[i] = num;
                    this.resultArray[0][i] = num;

                    for (var j = 0; j < 45; j++) {
                        tmpnum += reelsRandomizationArrayDJSRespin[this.easyModeCode][j];

                        if (tmpnum >= this.resultArray[0][i]) {
                            this.resultArray[0][i] = j;
                            break;
                        }
                    }
                    if (this.reelsArrayDJSRespin[this.resultArray[0][i]] == ELEMENT_2XX || this.reelsArrayDJSRespin[this.resultArray[0][i]] == ELEMENT_3XX ||
                        this.reelsArrayDJSRespin[this.resultArray[0][i]] == ELEMENT_4XX || this.reelsArrayDJSRespin[this.resultArray[0][i]] == ELEMENT_5XX) {
                        //goto generate;
                    }
                    else
                    {
                        break;
                    }
                }
            }


            /*/HACK
             if (i == 0)
             {
             resultArray[0][i] = 6;//2;
             }
             else if (i == 1)
             {
             resultArray[0][i] = 1;//2, 4, 20, 30
             }
             else if(i == 2)
             {
             resultArray[0][i] = 21;
             }
             else
             {
             resultArray[0][i] = 0;
             }//*///HACK
        }
    },

    reelStopper : function ( dt ) {
        this.activeScreens-=this.slots[this.totalSlots - this.activeScreens].stopARoll( dt );
        if ( this.activeScreens <= 0 )
        {
            this.unschedule(  this.reelStopper );

            if ( this.wildAnimating )
            {
                this.calculatePayment( true );
            }
        }
    },

    rollUpdator : function(dt)
    {
        var i;
        for ( i = 0; i < this.totalSlots; i++ )
        {
            this.slots[i].updateSlot( dt );
        }

        for ( i = 0; i < this.totalSlots; i++ )
        {
            if ( this.slots[i].isSlotRunning() )
            {
                break;
            }
        }

        if ( i >= this.totalSlots )
        {
            this.isRolling = false;
            this.unschedule( this.rollUpdator );
            if ( !this.wildAnimating )
            {
                this.calculatePayment( true );

                if ( this.isAutoSpinning )
                {
                    if ( this.currentWin > 0 )
                    {
                        this.scheduleOnce(  this.autoSpinCB, 1.5 );
                    }
                    else
                    {
                        this.scheduleOnce(  this.autoSpinCB, 1.0 );
                    }
                }
            }
        }
    },

    setDummyWild : function()
    {
        this.wildAnimating = true;
        this.removeChildByTag( XX_GLOW_TAG );

        var r = this.slots[0].physicalReels[1];
        switch ( this.wildElemnt.eCode )
        {
            case ELEMENT_2XX:
                this.dummyWildElemnt = r.elementsVec[10];
                break;

            case ELEMENT_3XX:
                this.dummyWildElemnt = r.elementsVec[12];
                break;

            case ELEMENT_4XX:
                this.dummyWildElemnt = r.elementsVec[14];
                break;

            case ELEMENT_5XX:
                this.dummyWildElemnt = r.elementsVec[16];
                break;

            default:
                break;
        }

        if ( this.slots[0].isForcedStop )
        {
            this.dummyWildElemnt.setPositionY( 0.999725/* + slots[0].getPositionY()*/ );
        }
        else
        {
            switch ( this.wildElemnt.eCode )
            {
                case ELEMENT_2XX:
                    this.dummyWildElemnt.setPositionY(this.wildElemnt.getPositionY() + this.wildElemnt.getContentSize().height / 3);
                    break;

                case ELEMENT_3XX:
                    this.dummyWildElemnt.setPositionY(this.wildElemnt.getPositionY() - this.wildElemnt.getContentSize().height / 3);
                    break;

                default:
                    this.dummyWildElemnt.setPositionY(this.wildElemnt.getPositionY());
                    break;
            }
        }

        this.wildElemnt.runAction( new cc.FadeOut( 0.2 ) );
        this.dummyWildElemnt.runAction( new cc.FadeIn( 0.2 ) );
        this.dummyWildElemnt.runAction( new cc.RepeatForever( new cc.Sequence( new cc.ScaleTo( 0.5, 1.1 ), new cc.ScaleTo( 0.5, 1 ) ) ) );

        var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;

        var node = new cc.Sprite( "#re_spin.png" );
        node.setPosition( this.wildElemnt.getParent().getParent().getPosition() + this.slots[0].getPosition() );
        node.setPositionX( node.getPositionX() - diff );
        node.setTag( HOLD_ANIM_TAG );
        node.setScale( 0 );
        node.setOpacity( 150 );
        node.runAction( new cc.Sequence( new cc.ScaleTo( 0.5, 1 ), new cc.RotateBy( 0.5, 360 ), new cc.RotateBy( 0.5, -360 ),
            new cc.FadeOut( 0.5 ), new cc.DelayTime( 1.0 ), cc.callFunc( this.animCBR, this  ) ) );
        this.addChild( node, 5 );
    },

    setFalseWin : function()
    {
    },

    setResult : function( indx, lane, sSlot )
    {
        var gap = 387.0;

        var j = 0;
        var animOriginY;

        var r = this.slots[0].physicalReels[lane];
        var arrSize = r.elementsVec.length;
        var spr;

        var spriteVec = new Array();

        animOriginY = gap * r.direction;
        spr = r.elementsVec[indx];
        if ( spr.eCode == ELEMENT_EMPTY )
        {
            gap = 285 * spr.getScale();
        }

        if ( indx == 0 )
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push(spr);

            spr.setPositionY( animOriginY );

            for (var i = 0; i < 2; i++)
            {
                spr = r.elementsVec[i];
                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (i + 1) * r.direction);
            }
        }
        else if (indx == arrSize - 1)
        {
            spr = r.elementsVec[0];
            spriteVec.push(spr);

            spr.setPositionY(animOriginY);
            for (var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i--)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction );
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push(spr);
            spr.setPositionY(animOriginY);

            for (var i = indx; i < indx + 2; i++)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }

        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            gap = -773.5;

            var eee;
            if (i < 2)
            {
                eee = spriteVec[i + 1];
            }
            if (i == 0)
            {
                var ee = spriteVec[i + 1];
                if ( Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) ) != 387)
                {
                    gap = -Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) * 2 * ee.getScale() );
                }
            }

            gap*=r.direction;
            var ease;

            if ( this.slots[sSlot].isForcedStop )
            {
                ease =  new cc.MoveTo( 0.15, cc.p( 0, e.getPositionY() + gap) ).easing( cc.easeBackOut() );
            }
            else
            {
                ease = new cc.MoveTo( 0.3, cc.p( 0, e.getPositionY() + gap ) ).easing( cc.easeBackOut() );
            }

            ease.setTag( MOVING_TAG );
            e.runAction(ease);

            if (i == 1 && (e.eCode == ELEMENT_2XX || e.eCode == ELEMENT_3XX ||
                e.eCode == ELEMENT_4XX || e.eCode == ELEMENT_5XX) )
            {
                this.wildElemnt = e;
            }
        }
        return spriteVec;
    },

    setWildAnimation : function(e)
    {
        this.wildAnimating = true;
        var moveBy = new cc.MoveBy( 0.2, cc.p( 0, e.getContentSize().height / 3 ) );

        var sprite = new cc.Sprite( glowStrings[4] );
        var r = this.slots[0].physicalReels[1];
        var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;

        this.scaleAccordingToSlotScreen( sprite );
        sprite.setPosition( r.getParent().getPosition().x + this.slots[0].getPositionX() - diff, r.getParent().getPosition().y + this.slots[0].getPositionY() );
        sprite.setTag(XX_GLOW_TAG);
        this.addChild(sprite, 5);
        sprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.3, 150 ), new cc.FadeTo( 0.3, 255 ) ) ) );

        this.delegate.jukeBox(S_XX_WILD);

        switch ( e.eCode )
        {
            case ELEMENT_2XX:
                e.runAction( new cc.Sequence( new cc.Repeat(moveBy, 2), new cc.Repeat(moveBy.reverse(), 4), new cc.Repeat(moveBy.clone(), 4),
                    new cc.Repeat(moveBy.reverse(), 2), moveBy.reverse(), cc.callFunc( this.wildAnimCB, this ) ) );
                break;

            case ELEMENT_3XX:
                e.runAction( new cc.Sequence( new cc.Repeat(moveBy, 2), new cc.Repeat(moveBy.reverse(), 4), new cc.Repeat(moveBy.clone(), 4),
                    moveBy.reverse(), cc.callFunc( this.wildAnimCB, this) ) );
                break;

            case ELEMENT_4XX:
            case ELEMENT_5XX:
                e.runAction( new cc.Sequence( new cc.Repeat(moveBy, 2), new cc.Repeat(moveBy.reverse(), 4), new cc.Repeat(moveBy.clone(), 4),
                    new cc.Repeat(moveBy.reverse(), 2), cc.callFunc(this.wildAnimCB, this) ));
                break;

            default:
                break;
        }
    },

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSRespin;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    setWinAnimation : function(animCode, screen)
    {
        var tI = 0.3;

        var fire;
        for (var i = 0; i < this.animNodeVec.length; i++)
        {
            fire = this.animNodeVec[i];
            fire.removeFromParent(true);
        }
        this.animNodeVec = [];


        switch (animCode)
        {
            case WIN_TAG:

                for (var i = 0; i < this.slots[0].blurReels.length; i++)
                {
                    var r = this.slots[0].blurReels[i];

                    var node = r.getParent();

                    var moveW = this.slots[0].reelDimentions.x * this.slots[0].getScale();
                    var moveH = this.slots[0].reelDimentions.y * this.slots[0].getScale();

                    for (var j = 0; j < 4; j++)
                    {

                        var fire = new cc.ParticleSun();
                        fire.texture = cc.textureCache.addImage( res.FireStar_png );
                        var pos;


                        fire.setStartColor( cc.color(43, 227, 254) );
                        this.addChild(fire, 5);
                        this.animNodeVec.push(fire);


                        var moveBy = null;

                        var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                        diff*=( 2 - i );

                        switch (j % 4)
                        {
                            case 0:
                                pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(moveW, 0));
                                break;

                            case 1:
                                pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(0, moveH));
                                break;

                            case 2:
                                pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(-moveW, 0));
                                break;

                            case 3:
                                pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(0, -moveH));
                                break;

                            default:
                                break;
                        }
                        fire.runAction( new cc.RepeatForever( new cc.Sequence(moveBy, moveBy.reverse() ) ) );
                    }
                }

                break;

            case BIG_WIN_TAG:
                for(var i = 0; i < this.slots[0].blurReels.length; i++)
                {
                    var r = this.slots[0].blurReels[i];

                    var node = r.getParent();

                    var walkAnimFrames  = [];
                    var walkAnim = null;

                    var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                    diff*=( 2 - i );

                    var glowSprite = new cc.Sprite( glowStrings[0] );
                    {
                        var tmp = new cc.SpriteFrame( glowStrings[0] , cc.rect(0, 0, glowSprite.getContentSize().width, glowSprite.getContentSize().height));
                        walkAnimFrames.push(tmp);
                    }
                    {
                        var tmp = new cc.SpriteFrame( glowStrings[3], cc.rect(0, 0, glowSprite.getContentSize().width, glowSprite.getContentSize().height));
                        walkAnimFrames.push(tmp);
                    }
                    {
                        var tmp = new cc.SpriteFrame( glowStrings[2], cc.rect(0, 0, glowSprite.getContentSize().width, glowSprite.getContentSize().height));
                        walkAnimFrames.push(tmp);
                    }

                    walkAnim = new cc.Animation( walkAnimFrames, 0.75 );
                    //walkAnim.setLoops(-1);
                    var tmpAnimate = cc.animate(walkAnim);
                    glowSprite.runAction( new cc.RepeatForever( tmpAnimate ) );

                    if (glowSprite)
                    {
                        glowSprite.setPosition( node.getPositionX() + this.slots[0].getPositionX() - diff, node.getPositionY() + this.slots[0].getPositionY() );


                        this.addChild(glowSprite, 5);
                        this.animNodeVec.push(glowSprite);
                        this.scaleAccordingToSlotScreen( glowSprite );
                        glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo(0.3, 100), new cc.FadeTo(0.3, 255) ) ) );
                    }
                }
                break;

            default:
                break;
        }
    },

    spin : function()
    {

        this.unschedule(this.updateWinCoin);
        this.deductBet( this.currentBet );
        if (this.displayedScore != this.totalScore)
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        if (this.dummyWildElemnt)
        {
            this.dummyWildElemnt.stopAllActions();
            this.dummyWildElemnt.runAction( new cc.FadeOut(0.2));
        }
        this.dummyWildElemnt = null;
        this.wildElemnt = null;
        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;
        this.wildAnimating = false;
        this.calcCount = 0;

        this.currentWin = 0;
        this.currentWinForcast = 0;
        this.startRolling();

        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    },

    wildAnimCB : function( obj )
    {
        this.schedule(this.reelStopper, 1);

        this.setDummyWild();
    }

});