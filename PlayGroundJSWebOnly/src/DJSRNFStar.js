/**
 * Created by RNF-Mac11 on 2/9/17.
 */



var reelsRandomizationArrayDJSRNFStar =
    [
        [// 85% payout
            15, 40, 15, 50, 5, 35,

            15, 50, 15, 50,15,55,

            15, 60, 15, 25, 10, 30,

            15, 15, 15, 25, 15, 35,

            10, 20, 10, 15, 5, 10,

            5, 10, 3, 5, 10, 3
        ],
        [//92 %
            15, 60, 15, 70, 5, 35,

            15, 55, 15, 50,15,55,

            15, 60, 15, 30, 10, 30,

            15, 20, 15, 25, 15, 35,

            10, 20, 10, 15, 5, 10,

            5, 10, 3, 5, 10, 3
        ],
        [
            // 96-97% payout

            15, 70, 15, 70, 5, 50,
            15, 65, 15, 50,15,55,
            15, 60, 15, 30, 10, 30,
            15, 20, 15, 25, 15, 35,
            10, 20, 10, 15, 5, 10,
            5, 10, 3, 5, 10, 3
        ],
        [// 148% + payout
            1, 70, 5, 70, 5, 50,
            5, 65, 5, 70, 5, 60,
            5, 70, 5, 15, 5, 30,
            5, 20, 5, 25, 5, 35,
            5, 20, 5, 15, 3, 10,
            3, 10, 1, 5, 5, 3
        ],
        [// 300% + payout
            1, 70, 5, 70, 5, 50,
            5, 65, 5, 70, 5, 60,
            5, 70, 5, 15, 5, 30,
            5, 20, 5, 25, 5, 35,
            5, 20, 5, 15, 3, 10,
            3, 10, 1, 5, 5, 3
        ]
    ];

var multiplierArr = [
    ELEMENT_2X, ELEMENT_3X, ELEMENT_5X, ELEMENT_10X, ELEMENT_20X, ELEMENT_40X
];

var multiplierRandArr = [
    62,25,15,7,3,2
];

var DJSRNFStar = GameScene.extend({
    reelsArrayDJSRNFStar:[],
    ctor:function ( level_index ) {
        this.reelsArrayDJSRNFStar = [
            /*1*/ELEMENT_EMPTY,/*2*/ ELEMENT_7_BLUE,/*3*/ ELEMENT_EMPTY,/*4*/ ELEMENT_7X,/*5*/ ELEMENT_EMPTY,
            /*6*/ELEMENT_CHERRY,/*7*/ELEMENT_EMPTY,/*8*/ ELEMENT_7_BAR,/*9*/ ELEMENT_EMPTY,/*10*/ ELEMENT_BAR_3,
            /*11*/ELEMENT_EMPTY,/*12*/ELEMENT_BAR_2,/*13*/ELEMENT_EMPTY,/*14*/ ELEMENT_BAR_1,/*15*/ELEMENT_EMPTY,
            /*16*/ELEMENT_CHERRY,/*17*/ ELEMENT_EMPTY,/*18*/ ELEMENT_BAR_3,/*19*/ELEMENT_EMPTY,/*20*/ ELEMENT_7_BAR,
            /*21*/ELEMENT_EMPTY,/*22*/ ELEMENT_BAR_2,/*23*/ELEMENT_EMPTY,/*24*/ELEMENT_BAR_1,/*25*/ELEMENT_EMPTY,
            /*26*/ELEMENT_BAR_2,/*27*/ ELEMENT_EMPTY,/*28*/ ELEMENT_7_BAR,/*29*/ELEMENT_EMPTY,/*30*/ ELEMENT_7_BLUE,
            /*31*/ELEMENT_EMPTY,/*32*/ELEMENT_BAR_1,/*33*/ ELEMENT_EMPTY,/*34*/ ELEMENT_BAR_3,/*35*/ ELEMENT_EMPTY,
            /*36*/ ELEMENT_7_BLUE
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSRNFStar;
        this.physicalArrayCheck= this.reelsArrayDJSRNFStar;
        this._super( level_index );

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;


    },

    calculatePayment : function( doAnimate )
    {
        var payMentArray = [
        /*0*/    20,// 7x 7x 7x

        /*1*/    10,// Triple 7 Blue

        /*2*/    5,// Triple Cherry

        /*3*/    4, // 7Bar 7Bar 7Bar

        /*4*/    3,  // Triple Bar

        /*5*/    2,  // Double Bar

        /*6*/    1,   // single Bar

        /*7*/    1,   // any bar 7 Bar

        /*8*/    1,   // Triple Bar 7Blue combo

        /*9*/    1,   // Double Bar 7x combo

        /*10*/   1,   // Single Bar 7Bar combo

        /*11*/   2,   // Any 7

        /*12*/   2   // Double Cherry

    ];

        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        for (var i = 0; i < this.slots[0].resultReel.length; i++)
        {
            tmpVector = this.slots[0].resultReel[i];
            tmpVector2.push(tmpVector[1]);
        }

        this.lineVector.push(tmpVector2);

        var pay = 0;
        var countGeneral = 0;
        var winScenario = 13;
        var lineLength = 3;
        var i, j, k;
        var e = null;
        var tmpVec;


        for (i = 0; i < this.totalLines; i++)
        {
            tmpVec = this.lineVector[i];
            for (k = 0; k < winScenario; k++)
            {
                var breakLoop = false;
                countGeneral = 0;
                switch (k)
                {
                    case 0: // 7x 7x 7x
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSRNFStar[this.resultArray[i][j]])
                            {
                                case ELEMENT_7X:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[0];
                            breakLoop = true;

                            for (var l = 0; l < lineLength; l++) {
                            if ( doAnimate )
                            {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 1: // 7 7 7 Blue
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSRNFStar[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_BLUE:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[1];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate )
                            {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 2: // Cherry
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSRNFStar[this.resultArray[i][j]])
                            {
                                case ELEMENT_CHERRY:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[2];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate )
                            {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        else if (countGeneral == 2)
                        {
                            pay = payMentArray[12];
                            breakLoop = true;
                            for (var l = 0; l < 3; l++) {
                            if ( doAnimate )
                            {
                                e = tmpVec[l];
                            }
                            if ( e && e.eCode == ELEMENT_CHERRY )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 3: // 7 7 7 bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSRNFStar[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[3];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate )
                            {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 4: // 3Bar 3Bar 3Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSRNFStar[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[4];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate )
                            {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 5: // any 7 combo
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSRNFStar[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_BAR:
                                case ELEMENT_7_BLUE:
                                case ELEMENT_7_PINK:
                                case ELEMENT_7X:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[11];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate )
                            {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 6: // 2Bar 2Bar 2Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSRNFStar[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[5];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate )
                            {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 7: // Bar Bar Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSRNFStar[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[6];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate )
                            {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 8: // any 7Bar and abr combo
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSRNFStar[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_BAR:
                                case ELEMENT_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[10];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate )
                            {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 9: // any Bar and 7Bar combo
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSRNFStar[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_BAR:
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[7];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++)
                            {
                                if ( doAnimate )
                                {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 10: // 3Bar and 7Pink combo
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSRNFStar[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_BLUE:
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[8];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++)
                            {
                                if ( doAnimate )
                                {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 11: // 2Bar and 7Blue combo
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSRNFStar[this.resultArray[i][j]])
                            {
                                case ELEMENT_7X:
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[9];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++)
                            {
                                if ( doAnimate )
                                {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

                if (breakLoop)
                {
                    break;
                }
            }
        }

        if ( doAnimate )
        {
            this.currentWin = this.currentBet * pay;

            switch (multiplierArr[this.resultArray[0][3]])
            {
                case ELEMENT_2X:
                    this.currentWin*=2;
                    e = tmpVec[3];
                    if (e)
                    {
                        e.aCode = SCALE_TAG;
                    }
                    break;

                case ELEMENT_3X:
                    this.currentWin*=3;
                    e = tmpVec[3];
                    if (e)
                    {
                        e.aCode = SCALE_TAG;
                    }
                    break;

                case ELEMENT_5X:
                    this.currentWin*=5;
                    e = tmpVec[3];
                    if (e)
                    {
                        e.aCode = SCALE_TAG;
                    }
                    break;

                case ELEMENT_10X:
                    this.currentWin*=10;
                    e = tmpVec[3];
                    if (e)
                    {
                        e.aCode = SCALE_TAG;
                    }
                    break;

                case ELEMENT_20X:
                    this.currentWin*=20;
                    e = tmpVec[3];
                    if (e)
                    {
                        e.aCode = SCALE_TAG;
                    }
                    break;

                case ELEMENT_40X:
                    this.currentWin*=40;
                    e = tmpVec[3];
                    if (e)
                    {
                        e.aCode = SCALE_TAG;
                    }
                    break;

                default:
                    break;
            }

            if (this.currentWin > 0)
            {
                this.addWin = this.currentWin / 10;
                this.setCurrentWinAnimation();
            }

            this.checkAndSetAPopUp();
        }
        else
        {
            this.currentWinForcast = this.currentBet * pay;

            switch (multiplierArr[this.resultArray[0][3]])//Change 7 June
            {
                case ELEMENT_2X:
                    this.currentWinForcast*=2;
                    break;

                case ELEMENT_3X:
                    this.currentWinForcast*=3;
                    break;

                case ELEMENT_5X:
                    this.currentWinForcast*=5;
                    break;

                case ELEMENT_10X:
                    this.currentWinForcast*=10;
                    break;

                case ELEMENT_20X:
                    this.currentWinForcast*=20;
                    break;

                case ELEMENT_40X:
                    this.currentWinForcast*=40;
                    break;

                default:
                    break;
            }
        }
    },
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSRNFStar[i]=this.physicalReelElements[idx];
                i++;
            }
        }
    },
    changeTotalProbabilities : function()
    {
        this.totalProbalities = 0;
        this.totalProbalitiesWild = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            //this.extractArrayFromSring();
            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[this.easyModeCode][i];
                if (i < 6)
                {
                    this.totalProbalitiesWild+=multiplierRandArr[i];
                }
            }
        }
        else
        {
            for ( var i = 0; i < 36; i++ )
            {
                this.probabilityArray[this.easyModeCode][i] = reelsRandomizationArrayDJSRNFStar[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSRNFStar[this.easyModeCode][i];
                if (i < 6)
                {
                    this.totalProbalitiesWild+=multiplierRandArr[i];
                }
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
    },

    generateResult : function( scrIndx )
    {
        for (var i = 0; i < this.slots[scrIndx].displayedReels.length; i++)
        {
            var num = this.prevReelIndex[i];

            var tmpnum = 0;

            if (i < 3)
            {
                while (num == this.prevReelIndex[i])
                {
                    num = Math.floor( Math.random() * this.totalProbalities );
                }
                this.prevReelIndex[i] = num;
                this.resultArray[0][i] = num;

                for (var j = 0; j < 36; j++)
                {
                    tmpnum+=this.probabilityArray[this.easyModeCode][j];

                    if (tmpnum >= this.resultArray[0][i])
                    {
                        this.resultArray[0][i] = j;
                        break;
                    }
                }
            }
            else
            {
                while (num == this.prevReelIndex[i])
                {
                    num = Math.floor( Math.random() * this.totalProbalitiesWild ) ;
                }
                this.prevReelIndex[i] = num;
                this.resultArray[0][i] = num;

                for (var j = 0; j < 6; j++)
                {
                    tmpnum+=multiplierRandArr[j];

                    if (tmpnum >= this.resultArray[0][i])
                    {
                        this.resultArray[0][i] = j;
                        break;
                    }
                }
            }


            /*/HACK
             if (i == 0)
             {
             resultArray[0][i] = 13;//2;
             }
             else if (i == 1)
             {
             resultArray[0][i] = 13;//2, 4, 20, 30
             }
             else if(i == 2)
             {
             resultArray[0][i] = 13;
             }
             else
             {
             resultArray[0][i] = 0;
             }//*///HACK

        }

        this.calculatePayment( false );
        this.checkForFalseWin();

    },

    rollUpdator : function(dt)
    {
        var i;
        for (i = 0; i < this.totalSlots; i++)
        {
            this.slots[i].updateSlot(dt);
        }

        for (i = 0; i < this.totalSlots; i++)
        {
            if ( this.slots[i].isSlotRunning() )
            {
                break;
            }
        }

        if (i >= this.totalSlots)
        {
            this.isRolling = false;
            this.unschedule( this.rollUpdator );
            this.calculatePayment( true );

            if (this.isAutoSpinning)
            {
                if (this.currentWin > 0)
                {
                    this.scheduleOnce( this.autoSpinCB, 1.5);
                }
                else
                {
                    this.scheduleOnce( this.autoSpinCB, 1.0);
                }
            }
        }
    },

    setFalseWin : function()
    {
        var highWinIndices = [ 1, 3 ];
        var missIndices = [ 29, 3 ];

        var missedIndx = Math.floor( Math.random() * 3 );

        if( missedIndx == 2 )
        {
            if( Math.floor( Math.random() * 10 ) == 3 )
            {
            }
            else
            {
                missedIndx = Math.floor( Math.random() * 2 );
            }
        }

        var sameIndex = Math.floor( Math.random( ) * highWinIndices.length );

        for ( var i = 0; i < 3; i++ )
        {
            if ( i == missedIndx )
            {
                this.resultArray[0][i] = this.getRandomIndex( missIndices[ Math.floor( Math.random( ) * missIndices.length ) ], true );
            }
            else
            {
                this.resultArray[0][i] = this.getRandomIndex( highWinIndices[ sameIndex ], false );
            }
        }

        this.resultArray[0][3] = Math.floor( Math.random() * 6 );
    },

    setResult : function( indx, lane, sSlot )
    {
        var gap = 387;

        var j = 0;
        var animOriginY;

        var r = this.slots[0].physicalReels[lane];
        var arrSize = r.elementsVec.length;
        var spr;

        var spriteVec = new Array();

        animOriginY = gap * r.direction;
        spr = r.elementsVec[indx];
        if (spr.eCode == ELEMENT_EMPTY)
        {
            gap = 250 * spr.getScale();
        }

        if (indx == 0)
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push(spr);

            spr.setPositionY(animOriginY);

            for (var i = 0; i < 2; i++)
            {
                spr = r.elementsVec[i];
                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (i + 1) * r.direction);
            }
        }
        else if (indx == arrSize - 1)
        {
            spr = r.elementsVec[0];
            spriteVec.push(spr);

            spr.setPositionY(animOriginY);
            for (var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i--)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push(spr);
            spr.setPositionY(animOriginY);

            for (var i = indx; i < indx + 2; i++)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }

        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            gap = -773.5;

            var eee;
            if (i < 2)
            {
                eee = spriteVec[i + 1];
            }
            if (i == 0)
            {
                var ee = spriteVec[i + 1];
                if (Math.abs(e.getPositionY() - ee.getPositionY()) != 387)
                {
                    gap = -(Math.abs(e.getPositionY() - ee.getPositionY()) * 2 * ee.getScale());
                }
            }

            gap*=r.direction;

            var ease;

            if ( this.slots[sSlot].isForcedStop )
            {
                ease = new cc.MoveTo(0.15, cc.p(0, e.getPositionY() + gap)).easing( cc.easeBackOut() );
            }
            else
            {
                ease = new cc.MoveTo(0.3, cc.p(0, e.getPositionY() + gap)).easing( cc.easeBackOut() );
            }
            ease.setTag(MOVING_TAG);
            e.runAction(ease);
        }

        return spriteVec;
    },

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSRNFStar;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    setWinAnimation : function(animCode, screen)
{
    var fire;

    for (var i = 0; i < this.animNodeVec.length; i++)
    {
        fire = this.animNodeVec[i];
        fire.removeFromParent(true);
    }
    this.animNodeVec = [];


    switch (animCode)
    {
        case WIN_TAG:
            for (var k = 0; k < this.slots[0].blurReels.length; k++)
        {
            var r = this.slots[0].blurReels[k];

            var node = r.getParent();

            var glwSprite = new cc.Sprite("#glow_bg.png");

            var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
            diff*=( 3 - k );

            glwSprite.setPosition( cc.p( node.getPositionX() - diff + this.slots[0].getPosition().x, node.getPositionY() + this.slots[0].getPosition().y ) );
            this.addChild(glwSprite, 5);
            this.animNodeVec.push(glwSprite);

            var glwSpriteFG = new cc.Sprite("#glow_1.png");

            glwSpriteFG.setPosition( glwSprite.getPosition() );
            this.addChild(glwSpriteFG, 5);
            this.animNodeVec.push(glwSpriteFG);

            glwSpriteFG.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.3, 50 ), new cc.FadeTo( 0.3, 255 ) ) ) );

            this.scaleAccordingToSlotScreen( glwSprite );
            this.scaleAccordingToSlotScreen( glwSpriteFG );

            if (k == 3)
            {
                glwSprite.setScaleY( glwSprite.getScaleY() * 0.7 );
                glwSpriteFG.setScaleY( glwSpriteFG.getScaleY() * 0.7 );
            }
        }
            break;

        case BIG_WIN_TAG:
            for(var i = 3; i >= 0; i--)
        {
            var r = this.slots[0].blurReels[i];

            var node = r.getParent();

            var glowSprite = new cc.Sprite( "res/DJSRNFStar/bigGlow.png" );

            if (glowSprite)
            {
                var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                diff*=( 3 - i );

                glowSprite.setPosition( cc.p( node.getPositionX() - diff + this.slots[0].getPosition().x, node.getPositionY() + this.slots[0].getPosition().y ) );


                this.addChild(glowSprite, 5);
                this.animNodeVec.push(glowSprite);

                glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ) ) );
                this.scaleAccordingToSlotScreen( glowSprite );

                //glowSprite.setScale(0.8);
                if (i == 3)
                {
                    glowSprite.setScaleY(0.78);
                }
            }
        }
            break;

        default:
            break;
    }
},

    spin : function()
    {

        this.unschedule( this.updateWinCoin );

        this.deductBet( this.currentBet );
        if (this.displayedScore != this.totalScore)
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;
        this.currentWin = 0;
        this.currentWinForcast = 0;
        this.startRolling();

        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    }

});
