


var reelsRandomizationArrayDJSMagnetSlide =
    [
        [// 85% payout
            100, 22, 100,25 , 20, 40,
            12, 7, 12,25, 30, 75,
            100, 70, 16 ,10, 16, 24,
            35, 50, 16, 9, 16, 57,
            30, 70, 300, 44, 20,20,
            13, 6, 13, 19, 20, 50,
        ],
        [//92 %
            100, 22, 100,25 , 20, 40,
            12, 8, 12,25, 30, 75,
            100, 70, 16 ,10, 16, 24,
            35, 50, 16, 10, 16, 57,
            30, 70, 300, 44, 20,20,
            13, 8, 13, 19, 20, 50,
        ],
        [// 95-97% payout
            100, 22, 100,25 , 20, 40,
            12, 9, 12,25, 30, 75,
            100, 70, 16 ,10, 16, 24,
            35, 50, 16, 10, 16, 57,
            30, 70, 300, 44, 20,20,
            13, 9, 13, 19, 20, 50,
        ],
        [//140% payout
            100, 70, 100,25 , 20, 75,
            15, 15, 15,25, 30, 75,
            100, 70, 25 ,13, 16, 25,
            35, 50, 25, 13, 16, 75,
            30, 70, 300, 75, 20,20,
            20, 15, 20, 20, 20, 50,
        ],
        [//300% payout
            100, 100, 100,25 , 20, 100,
            15, 45, 15,25, 30, 75,
            100, 70, 25 ,50, 16, 25,
            35, 50, 25, 50, 16, 75,
            30, 70, 300, 75, 20,20,
            20, 45, 20, 20, 20, 50,
        ],
    ];

var DJSMagnetSlide = GameScene.extend({

    lastSlidingLane : 0,

    resultArrayBackup : new Array(3),
    reelsArrayDJSMagnetSlide:[],
    ctor:function ( level_index ) {
        this.reelsArrayDJSMagnetSlide = [
            /*1*/ELEMENT_EMPTY,/*2*/ELEMENT_D_BAR_1,/*3*/ELEMENT_EMPTY,/*4*/ELEMENT_BAR_3,/*5*/ELEMENT_EMPTY,
            /*6*/ELEMENT_I_D_BAR_1,/*7*/ELEMENT_EMPTY,/*8*/ELEMENT_3X,/*9*/ELEMENT_EMPTY,/*10*/ELEMENT_7_BAR,
            /*11*/ELEMENT_EMPTY,/*12*/ELEMENT_BAR_2,/*13*/ELEMENT_EMPTY,/*14*/ELEMENT_BAR_1,/*15*/ELEMENT_EMPTY,
            /*16*/ELEMENT_2X,/*17*/ELEMENT_EMPTY,/*18*/ELEMENT_7_BAR,/*19*/ELEMENT_EMPTY,/*20*/ELEMENT_BAR_3,
            /*21*/ELEMENT_EMPTY,/*22*/ELEMENT_2X,/*23*/ELEMENT_EMPTY,/*24*/ELEMENT_D_BAR_1,/*25*/ELEMENT_EMPTY,
            /*26*/ELEMENT_BAR_2,/*27*/ELEMENT_EMPTY,/*28*/ELEMENT_I_D_BAR_1,/*29*/ELEMENT_EMPTY,/*30*/ ELEMENT_7_RED,
            /*31*/ELEMENT_EMPTY,/*32*/ELEMENT_3X,/*33*/ ELEMENT_EMPTY,/*34*/ ELEMENT_7_RED,/*35*/ELEMENT_EMPTY,
            /*36*/ELEMENT_BAR_1,
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSMagnetSlide;
        this.physicalArrayCheck= this.reelsArrayDJSMagnetSlide;
        this._super( level_index );

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;


    },

    calculatePayment : function( doAnimate )
    {
        var payMentArray = [

        /*0*/    200,// 3x magnetUp 3x

        /*1*/    100, // 3x magnetDown 3x

        /*2*/    50,  // 2x magnetUp 2x

        /*3*/    25,  // 2x magnetDown 2x

        /*4*/    20,  // 2x magnetUp 3x any combo

        /*5*/    10,   // 2x magnetDown 3x any combo

        /*6*/    15,   // 7 7 7 Red

        /*7*/    10,   // 7 7 7 Bar

        /*8*/    4,   // 3Bar 3Bar 3Bar

        /*9*/    3,   // 2Bar 2Bar 2Bar

        /*10*/   2,   // Bar Bar Bar

        /*11*/   5,   // Any 7 COmbo

        /*12*/   1,   // any Bar 7Bar combo

    ];

        this.changeResults( !doAnimate );
        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        for (var i = 0; i < this.slots[0].resultReel.length; i++)
        {
            tmpVector = this.slots[0].resultReel[i];
            if ( tmpVector.length == 3 )
            {
                tmpVector2.push(tmpVector[1]);
            }
            else
            {
                tmpVector2.push(tmpVector[0]);
            }
        }

        this.lineVector.push(tmpVector2);

        var winScenario = 8;
        var lineLength = 3;
        var pay = 0;
        var tmpPay = 1;
        var count2X = 0;
        var count3X = 0;
        var countWild = 0;
        var countGeneral = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var i, j, k;
        var e = null;
        var pivotElm = null;
        var pivotElm2 = null;
        var tmpVec;

        //log( "2 in %s, doAnimate = %d", __FUNCTION__, doAnimate );
        for (i = 0; i < this.totalLines; i++)
        {
            tmpVec = this.lineVector[i];

            for (k = 0; k < winScenario; k++)
            {
                var breakLoop = false;
                countWild = 0;
                countGeneral = 0;
                switch (k)
                {
                    case 0://for all combination with wild
                        for (j = 0; j < lineLength; j++)
                        {
                            if (this.reelsArrayDJSMagnetSlide[this.resultArray[i][j]] != -1)
                            {

                                switch (this.reelsArrayDJSMagnetSlide[this.resultArray[i][j]])
                                {
                                    case ELEMENT_2X:
                                        count2X++;
                                        tmpPay*=2;
                                        countWild++;
                                        if ( doAnimate ) {
                                            e = tmpVec[j];
                                        }

                                        if (e)
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_3X:
                                        count3X++;
                                        tmpPay*=3;
                                        countWild++;
                                        if ( doAnimate ) {
                                            e = tmpVec[j];
                                        }
                                        if (e)
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        switch (pivotElement)
                                        {
                                            case -1:
                                                pivotElement = this.reelsArrayDJSMagnetSlide[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate ) {
                                                    e = tmpVec[j];
                                                }
                                                if (e)
                                                {
                                                    pivotElm = e;
                                                }
                                                break;

                                            default:
                                                pivotElement2 = this.reelsArrayDJSMagnetSlide[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate ) {
                                                    e = tmpVec[j];
                                                }
                                                if (e)
                                                {
                                                    pivotElm2 = e;
                                                }
                                                break;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }

                        if (j == 3)
                        {
                            if ( count3X == 2 && pivotElement == ELEMENT_I_D_BAR_1 )
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[1];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                                pay = payMentArray[1];
                                breakLoop = true;
                            }
                            else if ( count2X == 2 && pivotElement == ELEMENT_I_D_BAR_1 )
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[1];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                                pay = payMentArray[3];
                                breakLoop = true;
                            }
                            else if ( count3X == 2 && pivotElement == ELEMENT_D_BAR_1 )
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[1];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                                pay = payMentArray[0];
                                breakLoop = true;
                            }
                            else if ( count2X == 2 && pivotElement == ELEMENT_D_BAR_1 )
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[1];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                                pay = payMentArray[2];
                                breakLoop = true;
                            }
                            else if(countWild >= 2)
                            {
                                breakLoop = true;

                                switch (pivotElement)
                                {
                                    case ELEMENT_7_RED:
                                        pay = tmpPay * payMentArray[6];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_7_BAR:
                                        pay = tmpPay * payMentArray[7];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_3:
                                        pay = tmpPay * payMentArray[8];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_2:
                                        pay = tmpPay * payMentArray[9];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_1:
                                        pay = tmpPay * payMentArray[10];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_I_D_BAR_1:
                                        pay = payMentArray[5];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_D_BAR_1:
                                        pay = payMentArray[4];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        /*pay = tmpPay;
                                         if (pivotElm)
                                         {
                                         pivotElm.aCode = SCALE_TAG;;
                                         }*/
                                        break;
                                }
                            }
                            else if(countWild == 1)
                            {
                                breakLoop = true;

                                if (pivotElement == pivotElement2)
                                {
                                    switch (pivotElement)
                                    {
                                        case ELEMENT_7_RED:
                                            pay = tmpPay * payMentArray[6];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_7_BAR:
                                            pay = tmpPay * payMentArray[7];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_3:
                                            pay = tmpPay * payMentArray[8];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_2:
                                            pay = tmpPay * payMentArray[9];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_1:
                                            pay = tmpPay * payMentArray[10];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        default:
                                            //pay = tmpPay;
                                            break;
                                    }
                                }
                                else
                                {
                                    if( pivotElement == ELEMENT_D_BAR_1 || pivotElement2 == ELEMENT_D_BAR_1 ||
                                        pivotElement == ELEMENT_I_D_BAR_1 || pivotElement2 == ELEMENT_I_D_BAR_1)
                                    {
                                        switch ( ( ( pivotElement == ELEMENT_D_BAR_1 || pivotElement == ELEMENT_I_D_BAR_1 ) ? pivotElement2 : pivotElement ) ) {
                                            case ELEMENT_7_RED:
                                                pay = tmpPay * payMentArray[6];
                                                if ( doAnimate ) {
                                                    pivotElm.aCode = SCALE_TAG;;
                                                    pivotElm2.aCode = SCALE_TAG;
                                                }
                                                break;

                                            case ELEMENT_7_BAR:
                                                pay = tmpPay * payMentArray[7];
                                                if ( doAnimate ) {
                                                    pivotElm.aCode = SCALE_TAG;;
                                                    pivotElm2.aCode = SCALE_TAG;
                                                }
                                                break;

                                            case ELEMENT_BAR_3:
                                                pay = tmpPay * payMentArray[8];
                                                if ( doAnimate ) {
                                                    pivotElm.aCode = SCALE_TAG;;
                                                    pivotElm2.aCode = SCALE_TAG;
                                                }
                                                break;

                                            case ELEMENT_BAR_2:
                                                pay = tmpPay * payMentArray[9];
                                                if ( doAnimate ) {
                                                    pivotElm.aCode = SCALE_TAG;;
                                                    pivotElm2.aCode = SCALE_TAG;
                                                }
                                                break;

                                            case ELEMENT_BAR_1:
                                                pay = tmpPay * payMentArray[10];
                                                if ( doAnimate ) {
                                                    pivotElm.aCode = SCALE_TAG;;
                                                    pivotElm2.aCode = SCALE_TAG;
                                                }
                                                break;

                                            default:
                                                //pay = tmpPay;
                                                break;
                                        }
                                    }
                                    else if( ( pivotElement == ELEMENT_BAR_1 && pivotElement2 == ELEMENT_7_BAR ) ||
                                        ( pivotElement2 == ELEMENT_BAR_1 && pivotElement == ELEMENT_7_BAR ) )
                                    {
                                        pay = tmpPay * payMentArray[10];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if( ( ( pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_BAR_3 ) || pivotElement == ELEMENT_7_BAR ) &&
                                        ( ( pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_BAR_3 ) || pivotElement2 == ELEMENT_7_BAR ) )
                                    {
                                        pay = tmpPay * payMentArray[12];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if( pivotElement >= ELEMENT_7_BAR && pivotElement <= ELEMENT_7_RED &&
                                        pivotElement2 >= ELEMENT_7_BAR && pivotElement2 <= ELEMENT_7_RED )
                                    {
                                        pay = tmpPay * payMentArray[11];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                }
                            }
                        }
                        break;

                    case 1: // 7 7 7 red
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSMagnetSlide[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_RED:
                                case ELEMENT_D_BAR_1:
                                case ELEMENT_I_D_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[6];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }

                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 2: // 7 7 7 bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSMagnetSlide[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_BAR:
                                case ELEMENT_D_BAR_1:
                                case ELEMENT_I_D_BAR_1:
                                    countGeneral++;
                                    break;

                                case ELEMENT_BAR_1:
                                    countWild++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[7];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }

                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        else if ( countGeneral && countWild + countGeneral == 3 )
                        {
                            pay = payMentArray[10];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral + countWild; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }

                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 3: // 3Bar 3Bar 3Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSMagnetSlide[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_3:
                                case ELEMENT_D_BAR_1:
                                case ELEMENT_I_D_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[8];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 4: // 2Bar 2Bar 2Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSMagnetSlide[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_2:
                                case ELEMENT_D_BAR_1:
                                case ELEMENT_I_D_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[9];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 5: // Bar Bar Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSMagnetSlide[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_D_BAR_1:
                                case ELEMENT_I_D_BAR_1:
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[10];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;


                    case 6: // any Bar and Diamond Bar combo
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSMagnetSlide[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_RED:
                                case ELEMENT_7_BAR:
                                case ELEMENT_D_BAR_1:
                                case ELEMENT_I_D_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[11];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++)
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 7: // any Bar and Diamond Bar combo
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSMagnetSlide[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                case ELEMENT_7_BAR:
                                case ELEMENT_D_BAR_1:
                                case ELEMENT_I_D_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[12];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++)
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

                if (breakLoop)
                {
                    break;
                }
            }
        }

        if ( doAnimate )
        {
            this.currentWin = this.currentBet * pay;
            if (this.currentWin > 0)
            {
                this.addWin = this.currentWin / 10;
                this.setCurrentWinAnimation();
            }

            this.checkAndSetAPopUp();
        }
        else
        {
            this.currentWinForcast = this.currentBet * pay;

            for( var i = 0; i < 3; i++ )
            {
                this.resultArray[0][i] = this.resultArrayBackup[i];
                //log( "@Dp resultArray[0][%d] in calc = %d", i, resultArray[0][i] );
            }
        }
        //log( "out %s", __FUNCTION__ );
    },

    changeResults : function( isSimulation )
    {
        for( var i = 0; i < 3; i++ ) this.resultArrayBackup[i] = this.resultArray[0][i];

        var eCode = this.reelsArrayDJSMagnetSlide[ this.resultArray[0][1] ];
        var needToChange = false;

        if ( eCode == ELEMENT_I_D_BAR_1 )//moving up
        {
            needToChange = this.getEmptyElementsInNeighbourReels( true, false );
        }
        else if ( eCode == ELEMENT_D_BAR_1 )
        {
            needToChange = this.getEmptyElementsInNeighbourReels( false, false );
        }
        if( needToChange )
        {
            var e = null;
            var replacement = 0;

            var originalResultReel = this.slots[0].resultReel;
            var newResultReel = [];
            var reel;

            if ( eCode == ELEMENT_I_D_BAR_1 )//moving up
            {
                for( var j = 0; j < 3; j++ )
                {
                    reel = [];
                    if( j == 1 && !isSimulation )
                    {
                        reel = originalResultReel[j];
                    }
                    else
                    {
                        var spriteVec;
                        if( !isSimulation )
                        {
                            spriteVec = originalResultReel[j];
                            e = spriteVec[1];
                        }

                        replacement = this.reelsArrayDJSMagnetSlide[ this.resultArray[0][j] ];
                        if( replacement == ELEMENT_EMPTY )
                        {
                            if( !isSimulation )
                            {
                                e = spriteVec[0];
                                reel.push( e );
                            }
                            this.resultArray[0][j] = this.getSlidedIndex( 0, j );
                        }
                        else
                        {
                            reel = spriteVec;
                        }
                    }
                    newResultReel.push( reel );
                }
            }
            else if ( eCode == ELEMENT_D_BAR_1 )
            {
                for( var j = 0; j < 3; j++ )
                {
                    reel = [];
                    if( !isSimulation && j == 1 )
                    {
                        reel = originalResultReel[j];
                    }
                    else
                    {
                        var spriteVec;// = originalResultReel.at( j );
                        if( !isSimulation )
                        {
                            spriteVec = originalResultReel[j];
                            e = spriteVec[1];
                        }
                        replacement = this.reelsArrayDJSMagnetSlide[ this.resultArray[0][j] ];
                        if( replacement == ELEMENT_EMPTY )
                        {
                            if( !isSimulation )
                            {
                                e =  spriteVec[2];
                                reel.push( e );
                            }

                            this.resultArray[0][j] = this.getSlidedIndex( 2, j );//e.getElementIndex();
                        }
                        else
                        {
                            reel = spriteVec;
                        }
                    }
                    newResultReel.push( reel );
                }
            }

            if( !isSimulation )
            {
                this.slots[0].resultReel = newResultReel;
            }
        }
    },
    getSlidedIndex : function( direction, reel )
{
    var newIndex = 0;

    if( direction == 0 )
    {
        if( this.resultArray[0][reel] == 35 )
        {
            newIndex = 0;
        }
        else
        {
            newIndex = this.resultArray[0][reel] - 1;
        }
    }
    else
    {
        if( this.resultArray[0][reel] == 35 )
        {
            newIndex = 0;
        }
        else
        {
            newIndex = this.resultArray[0][reel] + 1;
        }
    }

    return newIndex;
},
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSMagnetSlide[i]=this.physicalReelElements[idx];
                i++;
            }
        }
    },
    changeTotalProbabilities : function()
    {
        this.totalProbalities = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            //this.extractArrayFromSring();
            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[this.easyModeCode][i];
            }
        }
        else
        {
            for ( var i = 0; i < 36; i++ )
            {
                this.probabilityArray[this.easyModeCode][i] = reelsRandomizationArrayDJSMagnetSlide[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSMagnetSlide[this.easyModeCode][i];
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
        if (((this.reelsArrayDJSMagnetSlide[this.resultArray[0][0]] == ELEMENT_PENTA_D || this.reelsArrayDJSMagnetSlide[this.resultArray[0][0]] == ELEMENT_TRIPLE_D) &&
                (this.reelsArrayDJSMagnetSlide[this.resultArray[0][1]] == ELEMENT_PENTA_D || this.reelsArrayDJSMagnetSlide[this.resultArray[0][1]] == ELEMENT_TRIPLE_D)) ||
            ((this.reelsArrayDJSMagnetSlide[this.resultArray[0][0]] == ELEMENT_PENTA_D || this.reelsArrayDJSMagnetSlide[this.resultArray[0][0]] == ELEMENT_TRIPLE_D) &&
                (this.reelsArrayDJSMagnetSlide[this.resultArray[0][1]] == ELEMENT_7_PINK )) ||
            ((this.reelsArrayDJSMagnetSlide[this.resultArray[0][1]] == ELEMENT_PENTA_D || this.reelsArrayDJSMagnetSlide[this.resultArray[0][1]] == ELEMENT_TRIPLE_D) &&
                (this.reelsArrayDJSMagnetSlide[this.resultArray[0][0]] == ELEMENT_7_PINK )))
        {
            if ( !this.slots[0].isForcedStop )
            {
                this.schedule( this.reelStopper, 4.8 );

                this.delegate.jukeBox( S_EXCITEMENT );

                var glowSprite = new cc.Sprite( glowStrings[0] );

                if ( glowSprite )
                {
                    var r = this.slots[0].physicalReels[ this.slots[0].physicalReels.length - 1 ];

                    glowSprite.setPosition( r.getParent().getPosition().x + this.slots[0].getPosition().x, r.getParent().getPosition().y + this.slots[0].getPosition().y );

                    //this.scaleAccordingToSlotScreen( glowSprite );

                    this.addChild( glowSprite, 5 );
                    this.animNodeVec.push( glowSprite );

                    glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ) ) );
                }
            }
        }
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
    },

    generateResult : function( scrIndx )
    {
        for (var i = 0; i < this.slots[scrIndx].displayedReels.length; i++)
        {
            //generate:
                var num = this.prevReelIndex[i];

            var tmpnum = 0;

            while (num == this.prevReelIndex[i])
            {
                num = Math.floor( Math.random() * this.totalProbalities );
            }
            this.prevReelIndex[i] = num;
            this.resultArray[0][i] = num;

            for (var j = 0; j < 36; j++)
            {
                tmpnum+=this.probabilityArray[this.easyModeCode][j];

                if (tmpnum >= this.resultArray[0][i])
                {
                    this.resultArray[0][i] = j;
                    break;
                }
            }


            if( i == 1 )
            {
                if( this.reelsArrayDJSMagnetSlide[ this.resultArray[0][i] ] == ELEMENT_3X ||
                    this.reelsArrayDJSMagnetSlide[ this.resultArray[0][i] ] == ELEMENT_2X )
                {
                    i--;//goto generate;
                }
                else
                {
                    if( this.resultArray[0][i] == 0 )
                    {
                        if( this.reelsArrayDJSMagnetSlide[ 1 ] == ELEMENT_3X ||
                            this.reelsArrayDJSMagnetSlide[ 1 ] == ELEMENT_2X ||
                            this.reelsArrayDJSMagnetSlide[ 35 ] == ELEMENT_3X ||
                            this.reelsArrayDJSMagnetSlide[ 35 ] == ELEMENT_2X )
                        {
                            i--;//goto generate;
                        }
                    }
                    else if( this.resultArray[0][i] == 35 )
                    {
                        if( this.reelsArrayDJSMagnetSlide[ 0 ] == ELEMENT_3X ||
                            this.reelsArrayDJSMagnetSlide[ 0 ] == ELEMENT_2X ||
                            this.reelsArrayDJSMagnetSlide[ 34 ] == ELEMENT_3X ||
                            this.reelsArrayDJSMagnetSlide[ 34 ] == ELEMENT_2X )
                        {
                            i--;//goto generate;
                        }
                    }
                    else
                    {
                        if( this.reelsArrayDJSMagnetSlide[ this.resultArray[0][i] - 1 ] == ELEMENT_3X ||
                            this.reelsArrayDJSMagnetSlide[ this.resultArray[0][i] - 1 ] == ELEMENT_2X ||
                            this.reelsArrayDJSMagnetSlide[ this.resultArray[0][i] + 1 ] == ELEMENT_3X ||
                            this.reelsArrayDJSMagnetSlide[ this.resultArray[0][i] + 1 ] == ELEMENT_2X)
                        {
                            i--;//goto generate;
                        }
                    }
                }
            }
            else
            {
                if( this.reelsArrayDJSMagnetSlide[ this.resultArray[0][i] ] == ELEMENT_D_BAR_1 ||
                    this.reelsArrayDJSMagnetSlide[ this.resultArray[0][i] ] == ELEMENT_I_D_BAR_1 )
                {
                    i--;//goto generate;
                }
                else
                {
                    if( this.resultArray[0][i] == 0 )
                    {
                        if( this.reelsArrayDJSMagnetSlide[ 1 ] == ELEMENT_D_BAR_1 ||
                            this.reelsArrayDJSMagnetSlide[ 1 ] == ELEMENT_I_D_BAR_1 ||
                            this.reelsArrayDJSMagnetSlide[ 35 ] == ELEMENT_D_BAR_1 ||
                            this.reelsArrayDJSMagnetSlide[ 35 ] == ELEMENT_I_D_BAR_1 )
                        {
                            i--;// goto generate;
                        }
                    }
                    else if( this.resultArray[0][i] == 35 )
                    {
                        if( this.reelsArrayDJSMagnetSlide[ 0 ] == ELEMENT_D_BAR_1 ||
                            this.reelsArrayDJSMagnetSlide[ 0 ] == ELEMENT_I_D_BAR_1 ||
                            this.reelsArrayDJSMagnetSlide[ 34 ] == ELEMENT_D_BAR_1 ||
                            this.reelsArrayDJSMagnetSlide[ 34 ] == ELEMENT_I_D_BAR_1 )
                        {
                            i--;// goto generate;
                        }
                    }
                    else
                    {
                        if( this.reelsArrayDJSMagnetSlide[ this.resultArray[0][i] - 1 ] == ELEMENT_D_BAR_1 ||
                            this.reelsArrayDJSMagnetSlide[ this.resultArray[0][i] - 1 ] == ELEMENT_I_D_BAR_1 ||
                            this.reelsArrayDJSMagnetSlide[ this.resultArray[0][i] + 1 ] == ELEMENT_D_BAR_1 ||
                            this.reelsArrayDJSMagnetSlide[ this.resultArray[0][i] + 1 ] == ELEMENT_I_D_BAR_1)
                        {
                            i--;//goto generate;
                        }
                    }
                }
            }
            //log( "resultArray[0][%d] = %d", i, resultArray[0][i] );
        }

        this.checkForFalseWin();
    },

    getPostStopSliding : function()
    {
        var isSliding = false;
        //int indx = 0;

        var spriteVec = this.slots[0].resultReel[1];
        var E = spriteVec[1];


        if ( E.eCode == ELEMENT_I_D_BAR_1 )//moving up
        {
            if( this.getEmptyElementsInNeighbourReels( true ) )
            {
                this.isRolling = isSliding = true;
                return true;
                //indx++;
            }
        }
        else if ( E.eCode == ELEMENT_D_BAR_1 )
        {
            if( this.getEmptyElementsInNeighbourReels( false ) )
            {
                this.isRolling = isSliding = true;
                return true;
                //indx++;
            }
        }
        return false;
    },

    getEmptyElementsInNeighbourReels : function( isUpward, doAnimate )
{
    //log( "in %s, doAnimate = %d", __FUNCTION__, doAnimate );
    if( undefined === doAnimate )   doAnimate = true;
    var haveEmpty = false;
    var eCode = 0;
    for ( var i = 0; i < 3; i+=2 ) {

    eCode = this.reelsArrayDJSMagnetSlide[ this.resultArray[0][i] ];
    if( eCode == ELEMENT_EMPTY )
    {
        haveEmpty = true;

        if( doAnimate )
        {
            this.setPostStopSliding( isUpward, i, 0 );

            this.setLighteningGAF( !i ? true : false, isUpward );
        }

    }
}
    return haveEmpty;
},

    setLighteningGAF : function( isLeft, isUpWard )
{
    var r = this.slots[0].blurReels[1];

    var node = r.getParent();

    //var normal = gaf::GAFAsset::create( delegate.resourcesPathPrefix + "DJSMagnetSlide/thunder_effect/thunder_effect.gaf" ).createObject();//#Dp change 29 Aug
    var myAsset = gaf.Asset.create( "res/DJSMagnetSlide/thunder_effect/thunder_effect.gaf" );
    var normal = myAsset.createObjectAndRun(true);
    normal.start();

    var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
    diff*=( 2 - 1 );

    normal.setPosition( node.getPositionX() + this.slots[0].getPositionX() - diff, node.getPositionY() + this.slots[0].getPositionY() );
    normal.setTag( GAF_OBJECT_TAG );
    /*normal.setAnimationFinishedPlayDelegate(/*[]( object ){
    //back to normal state
    object.setAnimationFinishedPlayDelegate( null );
    object.removeFromParentAndCleanup( true ););*/

    this.addChild( normal, 100 );

    if( !isUpWard )
    {
        if( !isLeft )
        {
            normal.setRotation( 270 );
        }
        else
        {
            normal.setRotation( 180 );
        }
    }
    else
    {
        if( !isLeft )
        {
        }
        else
        {
            normal.setRotation( 90 );
        }
    }

    //this.delegate.jukeBox( S_STATIC );
},

    postStopAnimCB : function(obj)
    {
        if (obj.getTag() == this.lastSlidingLane + 2)
        {
            while( this.getChildByTag( GAF_OBJECT_TAG ) )       this.removeChildByTag( GAF_OBJECT_TAG );
            this.isRolling = false;
            this.calculatePayment( true );

            if (this.isAutoSpinning)
            {
                if (this.currentWin > 0)
                {
                    this.scheduleOnce(this.autoSpinCB, 1.5);
                }
                else
                {
                    this.scheduleOnce(this.autoSpinCB, 1.0);
                }
            }
        }
        obj.setTag(-1);

    },

    postStopSoundCB : function(obj)
    {
        this.delegate.jukeBox(S_SLIDE_1 + obj.getTag());
        obj.removeFromParent(true);
    },

    rollUpdator : function(dt)
    {
        var i;
        for (i = 0; i < this.totalSlots; i++)
        {
            this.slots[i].updateSlot(dt);
        }

        for (i = 0; i < this.totalSlots; i++)
        {
            if (this.slots[i].isSlotRunning())
            {
                break;
            }
        }

        if (i >= this.totalSlots)
        {
            this.isRolling = false;
            this.unschedule(this.rollUpdator);

            if (!this.getPostStopSliding())
            {
                this.calculatePayment( true );
                if (this.isAutoSpinning)
                {
                    if (this.currentWin > 0)
                    {
                        this.scheduleOnce(this.autoSpinCB, 1.5);
                    }
                    else
                    {
                        this.scheduleOnce(this.autoSpinCB, 1.0);
                    }
                }
            }
        }
    },

    setFalseWin : function()
    {
    },

    setWinAnimation : function( animCode, screen )
    {
        var tI = 0.3;

        var fire;
        for (var i = 0; i < this.animNodeVec.length; i++)
        {
            fire = this.animNodeVec[i];
            fire.removeFromParent(true);
        }
        this.animNodeVec = [];


        switch ( animCode )
        {
            case WIN_TAG:
                for (var i = 0; i < this.slots[0].blurReels.length; i++)
            {
                var r = this.slots[0].blurReels[i];

                var node = r.getParent();

                var moveW = this.slots[0].reelDimentions.x * this.slots[0].getScale();
                var moveH = this.slots[0].reelDimentions.y * this.slots[0].getScale();

                var myAsset = gaf.Asset.create( "res/DJSMagnetSlide/thunder_effect_win/thunder_effect_win.gaf" );
                var normal = myAsset.createObjectAndRun(true);
                //var normal = gaf::GAFAsset::create( delegate.resourcesPathPrefix +  ).createObject();//#DP change 29 Aug

                normal.start();
                normal.setLooped(true);

                this.animNodeVec.push( normal );

                var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                diff*=( 2 - i );

                normal.setScaleX( normal.getScaleX() - 0.2 );

                var pos = cc.p(node.getPositionX() + this.slots[0].getPositionX() - diff + normal.getContentSize().width * 0.6, node.getPositionY() + this.slots[0].getPositionY() + normal.getContentSize().height * 0.5 );
                normal.setPosition( pos );

                this.scaleAccordingToSlotScreen( normal );

                this.addChild( normal, 1 );

                if( i == 0 )
                {
                    //GAFObject * normal = gaf::GAFAsset::create( delegate.resourcesPathPrefix + "DJSMagnetSlide/thunder_effect_win/thunder_effect_win.gaf" ).createObject();// #Dp change 29 Aug
                    var myAsset = gaf.Asset.create( "res/DJSMagnetSlide/thunder_effect_win/thunder_effect_win.gaf" );
                    var normal = myAsset.createObjectAndRun(true);
                    normal.start();
                    normal.setLooped(true);

                    this.animNodeVec.push( normal );

                    var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                    diff*=( 2 - i );

                    var pos = cc.p(node.getPositionX() + this.slots[0].getPositionX() - diff + normal.getContentSize().width * 0.5 - moveW, node.getPositionY() + this.slots[0].getPositionY() + normal.getContentSize().height * 0.5 );
                    normal.setPosition( pos );

                    this.scaleAccordingToSlotScreen( normal );
                    normal.setScaleX( normal.getScaleX() - 0.2 );
                    this.addChild( normal, 1 );
                }
            }
                break;

            case BIG_WIN_TAG:
                for(var i = 0; i < this.slots[0].blurReels.length; i++)
                {
                    var r = this.slots[0].blurReels[i];

                    var node = r.getParent();

                    var walkAnimFrames = [];
                    var walkAnim = null;

                    var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                    diff*=( 2 - i );

                    var glowSprite = new cc.Sprite( glowStrings[1] );
                    {
                        var tmp = new cc.SpriteFrame( glowStrings[1], cc.rect(0, 0, glowSprite.getContentSize().width, glowSprite.getContentSize().height));
                        walkAnimFrames.push(tmp);
                    }
                    {
                        var tmp = new cc.SpriteFrame( glowStrings[3], cc.rect(0, 0, glowSprite.getContentSize().width, glowSprite.getContentSize().height));
                        walkAnimFrames.push(tmp);
                    }

                    walkAnim = new cc.Animation( walkAnimFrames, 0.75 );

                    var tmpAnimate = new cc.Animate(walkAnim);
                    glowSprite.runAction( tmpAnimate.repeatForever() );

                    if (glowSprite)
                    {
                        glowSprite.setPosition( node.getPositionX() + this.slots[0].getPositionX() - diff, node.getPositionY() + this.slots[0].getPositionY() );


                        this.addChild(glowSprite, 5);
                        this.animNodeVec.push(glowSprite);

                        this.scaleAccordingToSlotScreen( glowSprite );

                        glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.25, 100 ), new cc.FadeTo( 0.25, 255 ) ) ) );
                    }
                }
                break;

            default:
                break;
        }
    },

    setPostStopSliding : function(isUpward, lane, indx)
    {
        var time = 0.75;
        var waitTime = 0.5;

        if (indx > 0)
        {
            waitTime = (waitTime + time) * indx;

            var node = new cc.Node();
            node.setTag(lane);
            node.runAction( new cc.Sequence( new cc.DelayTime(waitTime - 0.5), new cc.callFunc( this.postStopSoundCB, this) ));
            this.addChild(node);
        }

        this.lastSlidingLane = lane;

        var spriteVec = this.slots[0].resultReel[lane];

        if (isUpward)
        {
            for (var i = 0; i < spriteVec.length; i++)
            {
                var e = spriteVec[i];
                e.runAction(new cc.FadeIn(0.2));
                var gap = 1547 * 0.275 * 0.5;
                if (i == 0)
                {
                    var ee = spriteVec[i + 1];
                    if (Math.abs(e.getPositionY() - ee.getPositionY()) != 387)
                    {
                        gap = (Math.abs(e.getPositionY() - ee.getPositionY()) * ee.getScale() ) + 20;
                    }
                }
                e.runAction( new cc.Sequence( new cc.DelayTime(waitTime), new cc.MoveBy(time, cc.p(0, 183)).easing(cc.easeBackOut()), new cc.callFunc(this.postStopAnimCB, this) ) );
                e.setTag(lane + i);
            }
            if (indx == 0)
                this.delegate.jukeBox(S_SLIDE_1 + lane);
        }
        else
        {
            for (var i = 0; i < spriteVec.length; i++)
            {
                var e = spriteVec[i];
                e.runAction( new cc.FadeIn( 0.2 ) );
                var gap = -1547 * 0.237 * 0.5;

                e.runAction(new cc.Sequence(new cc.DelayTime(waitTime), new cc.MoveBy(time, cc.p(0, gap)).easing(cc.easeBackOut()), new cc.callFunc(this.postStopAnimCB, this)));
                e.setTag(lane + i);

            }
            if (indx == 0)
                this.delegate.jukeBox(S_SLIDE_1 + lane);
        }


    },

    setResult : function( indx, lane, sSlot )
    {
        var gap = 387.0;

        var j = 0;
        var animOriginY;

        var r = this.slots[0].physicalReels[lane];
        var arrSize = r.elementsVec.length;
        var spr;

        var spriteVec = new Array();

        animOriginY = gap * r.direction;
        spr = r.elementsVec[indx];
        if ( spr.eCode == ELEMENT_EMPTY )
        {
            gap = 285 * spr.getScale();
        }

        if ( indx == 0 )
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push(spr);

            spr.setPositionY( animOriginY );

            for (var i = 0; i < 2; i++)
            {
                spr = r.elementsVec[i];
                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (i + 1) * r.direction);
            }
        }
        else if (indx == arrSize - 1)
        {
            spr = r.elementsVec[0];
            spriteVec.push(spr);

            spr.setPositionY(animOriginY);
            for (var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i--)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction );
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push(spr);
            spr.setPositionY(animOriginY);

            for (var i = indx; i < indx + 2; i++)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }

        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            gap = -773.5;

            var eee;
            if (i < 2)
            {
                eee = spriteVec[i + 1];
            }
            if (i == 0)
            {
                var ee = spriteVec[i + 1];
                if ( Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) ) != 387)
                {
                    gap = -Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) * 2 * ee.getScale() );
                }
            }

            gap*=r.direction;
            var ease;

            if ( this.slots[sSlot].isForcedStop )
            {
                ease =  new cc.MoveTo( 0.15, cc.p( 0, e.getPositionY() + gap) ).easing( cc.easeBackOut() );
            }
            else
            {
                ease = new cc.MoveTo( 0.3, cc.p( 0, e.getPositionY() + gap ) ).easing( cc.easeBackOut() );
            }

            ease.setTag( MOVING_TAG );
            e.runAction(ease);
        }

        return spriteVec;
    },

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSMagnetSlide;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    spin : function()
    {
        this.unschedule(this.updateWinCoin);
        this.deductBet( this.currentBet );
        if (this.displayedScore != this.totalScore)
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;

        this.currentWin = 0;
        this.currentWinForcast = 0;
        this.startRolling();


        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    }

});