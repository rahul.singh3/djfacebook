InstantDealPopup = cc.Layer.extend({
    _buyPageBGRawImg:null,
    temp_typeCode:0,
    ctor: function (code) {
        this._super();
        this.setTag(code);
        //var pRet = new CustomButton();
        //if ( pRet && pRet.init() ){
        this.createComponent(code);
        return true;
        //}
    },
    createComponent:function (code) {
        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        this.addChild( darkBG );
        var isTablet = true;
        var modelInstant = InstantDealModel.getInstance();
        var instantDealObj =  InstantDealModel.getInstance().finalInstantDealList[modelInstant.instantDealIndex];
        if (instantDealObj.bgTexture != null) {
            var xScalePoints = 0;

            modelInstant.openInstantDeal = true;
               var templateType = instantDealObj.template_type;
               var bgImageWidth = ((templateType === 1 || templateType === 2 || templateType === 4) && isTablet) ? 1024 : (templateType === 4 && !isTablet) ? 1665 : 1013;
               var bgImageHeight = (templateType === 1 || templateType === 2 || templateType === 4) ? 768 : 729;
               if (templateType===4){
                   bgImageWidth = 1665;//cc.winSize.width;
                   bgImageHeight = 768;//cc.winSize.height;
               }
               this._buyPageBGRawImg = new cc.Sprite();
               this._buyPageBGRawImg.initWithTexture(instantDealObj.bgTexture,cc.rect(0,0,bgImageWidth,bgImageHeight));
               this._buyPageBGRawImg.setPosition(cc.p(cc.winSize.width/2,cc.winSize.height/2));
               this.addChild(this._buyPageBGRawImg);
               var buybtnFrame = "res/instant_deal/";
               var closeBtnFrame = "res/instant_deal/cst_instant_deal_close_button.png";
               var buybtnPosition = cc.p(this._buyPageBGRawImg.getContentSize().width/2 , this._buyPageBGRawImg.getContentSize().height*0.225);
               var closebtnPosition = cc.p(this._buyPageBGRawImg.getContentSize()*0.93);
               this.temp_typeCode = templateType;
               switch (templateType) {
                   case 1:
                       buybtnFrame = buybtnFrame + "cst_instant_deal_blank_button.png";
                       closebtnPosition = cc.p(this._buyPageBGRawImg.getContentSize().width*0.97,this._buyPageBGRawImg.getContentSize().height*0.92);
                       break;
                   case 2:
                       buybtnFrame = buybtnFrame + "cst_instant_deal_accept_claim_button.png";
                       closeBtnFrame = "res/instant_deal/cst_instant_deal_reject_button.png";
                       buybtnPosition = cc.p(this._buyPageBGRawImg.getContentSize().width*0.67,this. _buyPageBGRawImg.getContentSize().height*0.13);
                       closebtnPosition  = cc.p(this._buyPageBGRawImg.getContentSize().width*0.33, this._buyPageBGRawImg.getContentSize().height*0.13);
                       break;
                   case 3:
                       buybtnFrame = buybtnFrame + "cst_instant_deal_coins_button.png";
                       closebtnPosition = cc.p(this._buyPageBGRawImg.getContentSize().width*0.975,this._buyPageBGRawImg.getContentSize().height*0.92);
                       break;

                   default: {
                       buybtnFrame = buybtnFrame + "cst_instant_deal_accept_claim_button.png";
                       closebtnPosition = cc.p(cc.winSize.width * 0.965, cc.winSize.height * 0.95);
                       buybtnPosition = cc.p(cc.winSize.width / 2, cc.winSize.height * 0.15);
                   }
                       break;
               }

               var _buyBtn = new cc.MenuItemImage( buybtnFrame, buybtnFrame, this.menuCB, this );
               _buyBtn.setPosition(buybtnPosition);
               _buyBtn.setTag(501);
               var _closeBtn = new cc.MenuItemImage( closeBtnFrame, closeBtnFrame, this.menuCB, this );
               _closeBtn.setPosition(closebtnPosition);
               _closeBtn.setTag(502);
               var buyMenu = new cc.Menu();
               buyMenu.setPosition( cc.p( 0, 0 ) );
                buyMenu.addChild(_buyBtn);
               buyMenu.addChild(_closeBtn);
               if(templateType === 4) {
                   this.addChild(buyMenu);
               }else{
                   this._buyPageBGRawImg.addChild(buyMenu);
               }
               var _buyBtnText = new CustomLabel( instantDealObj.button_label, "HELVETICALTSTD-BOLD", 55, _buyBtn.getContentSize() );
               _buyBtnText.setPosition( cc.p(_buyBtn.getContentSize().width/2 ,(templateType === 1 || templateType === 4) ? _buyBtn.getContentSize().height*0.55: _buyBtn.getContentSize().height/2) );
               _buyBtn.addChild( _buyBtnText );
               var buyTextStr = (templateType === 1 || templateType === 4) ? instantDealObj.button_label : _buyBtnText;

               if (templateType === 3) {
                   buyTextStr = "Only $" + instantDealObj.product_id;
                   var _buyBtnCoinsTextStr =  DPUtils.getInstance().getNumString(instantDealObj.coins);
                   var _buyBtnCoinsText = new CustomLabel( _buyBtnCoinsTextStr, "HELVETICALTSTD-BOLD", 40, _buyBtn.getContentSize()*0.90 );
                   _buyBtnCoinsText.setPosition( cc.p(_buyBtn.getContentSize().width/2,_buyBtn.getContentSize().height * 0.75) );
                   _buyBtn.addChild( _buyBtnCoinsText );
                   _buyBtnText.setPositionY(_buyBtn.getContentSize().height * 0.30);
                   _buyBtnText.setString(buyTextStr);
                   _buyBtnText.setFontSize(30);
                   _buyBtnCoinsText.setColor(cc.color.YELLOW);
                   _buyBtnCoinsText.enableShadow(cc.color( 0,0,0,255 ),cc.size(0,-3),3);

                   //_buyBtnCoinsText.enableShadow(cc.color(0,0,0,255),3)
                  // _buyBtnCoinsText.enableOutline(cc.color(0,0,0,255),3)

               }else if(templateType === 2){
                   _buyBtnText.setString("ACCEPT");
                   var rejectText = new CustomLabel( "REJECT", "HELVETICALTSTD-BOLD", 48, _buyBtn.getContentSize() );
                   rejectText.setPosition( cc.p(_closeBtn.getContentSize().width/2,_closeBtn.getContentSize().height/2) );
                   _closeBtn.addChild(rejectText);
               }
                var instantdealInfo = InstantDealModel.getInstance().finalInstantDealList[InstantDealModel.getInstance().instantDealIndex];
                instantdealInfo.view_count+=1;
                if (templateType!==4){
                    DPUtils.getInstance().easyBackOutAnimToPopup(this._buyPageBGRawImg ,0.5);
                }else{
                    this._buyPageBGRawImg.setScale(0.75);
                }

                ServerData.getInstance().instantDealViewAndBuy(instantdealInfo._id,instantdealInfo.view_count);
                InstantDealModel.getInstance().updateInstantDealViewAndBuyCount();

           }else {
           this.removeFromParent(true);
           InstantDealModel.getInstance().closeInstantDeal(false);
       }
    },
    menuCB:function ( pSender ) {
        if ( cc.director.getRunningScene().getChildByTag(LOADER_TAG))
            return;
        var tag = pSender.getTag();
        AppDelegate.getInstance().jukeBox(S_BTN_CLICK);
        switch ( tag ) {
            case 501: {
                var instantModelObject =InstantDealModel.getInstance();
                var instantdealInfo = instantModelObject.finalInstantDealList[instantModelObject.instantDealIndex];

                for(var idx = purchaseTags.PURCHASE_0_99; idx <= purchaseTags.PURCHASE_99_99; idx++){
                    var holdImage = instantdealInfo.product_id;
                    if (holdImage===instantModelObject.getPurchaseAmt(idx)){
                        IAPHelper.getInstance().purchase(idx, BuyPageTags.INSTANT_DEAL_CREDITS);
                        break;
                    }


                }
            }
                break;
            case 502: {
              // AppDelegate.getInstance().showingScreen=false;
                if (this.temp_typeCode !==4){
                 DPUtils.getInstance().easyBackInAnimToPopup(this._buyPageBGRawImg,0.3) ;
                 this.runAction(new cc.Sequence(new cc.DelayTime(0.3),new cc.callFunc(function () {
                     this.removeFromParent(true);
                     InstantDealModel.getInstance().closeInstantDeal(false);
                 },this)));
                }else{
                    this.removeFromParent(true);
                    InstantDealModel.getInstance().closeInstantDeal(false);
                }

            }
                break;
        }
    }

});