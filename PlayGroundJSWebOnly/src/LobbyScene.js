var delegate;

var resizer;

var todayBonus;

var dNode;

var coinSpriteSmall;

var bonusItem = null;

var menu;

var label;

var origin;

var def;

var iconsDefaultArrangementVec = [];

var showingScreen;
/*

chrome://flags/#same-site-by-default-cookies
 */
var level;
var visibleSize;
var keyListner;
var displayedScore;
var HelloWorldLayer = cc.Layer.extend({
    //sprite:null,
    ctor:function () {
        var loadingSprite =null;
        //////////////////////////////
        // 1. super init first
        this._super();
        /////////////////////////////
        // 2. add a menu item with "X" image, which is clicked to quit the program
        //    you may modify it.
        // ask the window size
        ServerData.getInstance();
        var size = cc.winSize;
        this.visibleSize = cc.winSize;
        this.delegate = AppDelegate.getInstance();
        this.def = UserDef.getInstance();
        this.setTag( sceneTags.LOBBY_SCENE_TAG );

        if (ServerData.getInstance().configCode == 0) {
            this.delegate.changeBackgroundImage(res.Background_Lobby_html_png);
            var loadingScr = new cc.Sprite(res.Splash_png);
            loadingScr.setTag(SPLASH_TAG);
            loadingScr.setPosition(size.width * 0.5, size.height * 0.5);
            this.addChild(loadingScr, 0);
            this.loadingSprite = new cc.Sprite("res/appLaoder.png");
            this.loadingSprite.setTag(101);
            this.loadingSprite.setPosition(size.width * 0.5, size.height * 0.2);
            loadingScr.addChild(this.loadingSprite);
            this.loadingSprite.runAction(new cc.RepeatForever(new cc.RotateBy(1.5, 360)));
            this.schedule(this.appConfigChecker, 1.0, cc.REPEAT_FOREVER);
        }else{
            if (ServerData.getInstance().maintenance>0 && ServerData.getInstance().maintenance_duration>0) {
                this.setMaintenancePopUp();
                this.unschedule(this.appConfigChecker);
                if (this.getChildByTag(SPLASH_TAG)) {
                    this.getChildByTag(SPLASH_TAG).getChildByTag(101).removeFromParent();

                }
                return ;
            }
            this.setLobbyScene();
        }
        cc.eventManager.addListener({
            event: cc.EventListener.MOUSE,
            onMouseMove: function(event){
                //var str = "MousePosition X: " + event.getLocationX() + "  Y:" + event.getLocationY();
                // do something...
            },
            onMouseUp: function(event){
                //var str = "Mouse Up detected, Key: " + event.getButton();
                // do something...
            },
            onMouseDown: function(event){
                //var str = "Mouse Down detected, Key: " + event.getButton();
                // do something...
            },
            onMouseScroll: function(event){
                //var str = "Mouse Scroll detected, X: " + event.getScrollX() + "  Y:" + event.getScrollY();

                if( DPUtils.getInstance().scrolledAlready )
                {
                    DPUtils.getInstance().scrolledAlready = false;
                    return;
                }

                // do something...
                var self = event.getCurrentTarget();

                if( !self.scrollView )
                    return;

                var nMoveX = event.getScrollY() * 0.5;

                //cc.log( str );

                var inner = self.scrollView.getInnerContainer();
                var curPos  = inner.getPosition();
                var nextPos = cc.p(curPos.x + nMoveX, curPos.y);

                // prevent scrolling past beginning
                if (nextPos.x > 0)
                {
                    //inner.setPosition(cc.p(0, nextPos.y));
                    self.scrollView._startAutoScrollToDestination(cc.p(0, nextPos.y), 0.5, true);
                    self.leftArrow.setVisible( false );
                    return;
                }

                //auto ws = Director::getInstance().getWinSize();
                var size = self.scrollView.getContentSize();
                var innerSize = inner.getContentSize();
                var topMostX = size.width - innerSize.width;

                // prevent scroll past end
                if (nextPos.x < topMostX)
                {
                    self.rightArrow.setVisible(false)
                    //inner.setPosition( cc.p(topMostX, nextPos.y));
                    self.scrollView._startAutoScrollToDestination(cc.p(topMostX, nextPos.y), 0.5, true);
                    return;
                }
                if(nMoveX != 0)
                {
                    self.rightArrow.setVisible(true);
                    self.leftArrow.setVisible(true);
                }
                //inner.setPosition(nextPos);
                self.scrollView._startAutoScrollToDestination(nextPos, 0.5, true);
            }
        },this);
        return true;
    },
    appConfigChecker : function ( dt )
    {
      //  console.log("AppConfig "+ServerData.getInstance().configCode);
        if( ServerData.getInstance().configCode === 1 ){
            if (ServerData.getInstance().maintenance>0 && ServerData.getInstance().maintenance_duration>0){
                this.setMaintenancePopUp();
                this.unschedule(this.appConfigChecker);
                if (this.getChildByTag(SPLASH_TAG)){
                    this.getChildByTag(SPLASH_TAG).getChildByTag(101).removeFromParent();
                }
            }else {
                this.setLobbyScene();
                this.loadingSprite.stopAllActions();
                this.loadingSprite.removeFromParent();
                this.unschedule(this.appConfigChecker);
                this.removeChildByTag(SPLASH_TAG);
            }
        }

    },
    setMaintenancePopUp:function(){
        var layer = new cc.Layer();//Color( cc.color( 0, 0, 0, 160 )/*, this.visibleSize.width, this.visibleSize.height*/ );
        layer.setTag( VIP_LAYER_TAG );
        DPUtils.getInstance().setTouchSwallowing( this, layer );
        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        layer.addChild( darkBG );
        var maintenanceSprite = new cc.Sprite( res.ServerMaintenenceBg );
        maintenanceSprite.setPosition( cc.p( this.visibleSize.width / 2, this.visibleSize.height / 2 ) );
        layer.addChild( maintenanceSprite );

        var min = parseInt(ServerData.getInstance().maintenance_duration / 60);
        var sec = parseInt(ServerData.getInstance().maintenance_duration % 60);

        var totalLabel = new CustomLabel( min > 0?"Please come back after "+min+" minutes.":"Please come back after "+sec+" seconds.", "HELVETICALTSTD-BOLD", 40, new cc.size( 520, 40 ) );
        totalLabel.setPosition( maintenanceSprite.getContentSize().width * 0.5, maintenanceSprite.getContentSize().height*0.26 );
        maintenanceSprite.addChild( totalLabel );
        this.addChild(maintenanceSprite,100);
    },
    setLobbyScene:function () {
        this.startLoadingResources();
        this.addPlists();
        this.showingScreen = false;
        this.displayedScore = userDataInfo.getInstance().totalChips;//this.def.getValueForKey( CHIPS, 5000000 );
        this.totalScore = this.displayedScore;
        var bgSprite = null;
        if (UserDef.getInstance().getValueForKey(SERVER_OCCASION, 0) == 1) {
            bgSprite = new cc.Sprite(res.Background_lobby_christmas);
        } else {
            bgSprite = new cc.Sprite(res.Background_Lobby_png);
        }
        bgSprite.setPosition(this.visibleSize.width / 2, this.visibleSize.height / 2);
        this.addChild(bgSprite);
        this.topSprit = new cc.Sprite(res.Top_Bar_Lobby_png);
        this.topSprit.setPosition(this.visibleSize.width * 0.5, this.visibleSize.height - this.topSprit.getContentSize().height * 0.5);
        this.addChild(this.topSprit, 1);
        var topBarBack = new cc.Sprite(res.Top_Bar_Back_png);
        topBarBack.setPosition(this.topSprit.getPosition());
        this.addChild(topBarBack, 1);
        this.sideBarSprite = null;
        var rateUsSprite;
        var data = ServerData.getInstance();
        //  userDataInfo.getInstance().countryCode ="UK";
        if (data.server_real_money_show){
        if (DPUtils.getInstance().strcasecmp(userDataInfo.getInstance().countryCode, "IN") != 0 && DPUtils.getInstance().strcasecmp(userDataInfo.getInstance().countryCode, "") != 0 &&
            DPUtils.getInstance().strcasecmp(userDataInfo.getInstance().countryCode, "US") != 0 && CSUserdefauts.getInstance().getValueForKey(REAL_MONEY_ADV_CLICK_COUNT, 0) < 2 &&
            (!CSUserdefauts.getInstance().getValueForKey(REAL_MONEY_ADV_CLICKED, 0) || CSUserdefauts.getInstance().getValueForKey(REAL_MONEY_ADV_CLICKED, 0) == 11)) {
            switch (data.server_real_money_show) {
                case 1://william hill
                    break;

                case 2://888 casino
                    this.sideBarSprite = new cc.Sprite(res.Bar_Frame_Lobby_png);
                    this.sideBarSprite.setPosition(this.sideBarSprite.getContentSize().width / 2 - 10, cc.rectGetMidY(this.topSprit.getBoundingBox()) - this.sideBarSprite.getContentSize().height / 2 - 12.5);
                    this.addChild(this.sideBarSprite, 1);

                    rateUsSprite = new ccui.Button();

                    rateUsSprite.loadTextures("res/lobby/sidebar/888.png", "res/lobby/sidebar/888.png");

                    rateUsSprite.setTag(GO_TO_CASINO_BTN_TAG);

                    rateUsSprite.addTouchEventListener(this.touchEvent, this);

                    rateUsSprite.setPosition(this.sideBarSprite.getContentSize().width / 2, this.sideBarSprite.getContentSize().height / 2 - 32.5);
                    this.sideBarSprite.addChild(rateUsSprite);
                    break;

                default:
                    break;
            }


        }
    }
        this.setLevelBar();
        this.setScoreLabel();
        this.bottomSprit = new cc.Sprite( "#bottom_bar.png" );
        this.bottomSprit.setPosition( this.visibleSize.width * 0.5, this.bottomSprit.getContentSize().height * 0.5 );
        this.addChild( this.bottomSprit, 1 );
        if (UserDef.getInstance().getValueForKey(SERVER_OCCASION,0)==1){
            var snow_layer = new cc.Sprite( res.snow_layer_bar_png );
            snow_layer.setPosition( this.bottomSprit.getPosition() );
            this.addChild( snow_layer, 1 );
        }
        this.coinSprite = new cc.Sprite( res.Coin_png );

        this.coinSprite.setAnchorPoint( 0.5, 0 );
        this.coinSprite.setPosition( this.visibleSize.width * 0.5/* + origin.x*/, this.bottomSprit.getContentSize().height * 0.75/* + origin.y*/ );
        this.coinSprite.setTextureRect( cc.rect( 0, 0, this.coinSprite.getContentSize().width, this.coinSprite.getContentSize().height * 0.5 ) );
        this.addChild( this.coinSprite, 1 );

        var barSlitSprite = new cc.Sprite( res.Bar_Slit_Lobby_png );
        barSlitSprite.setPosition( this.visibleSize.width * 0.5, this.bottomSprit.getContentSize().height * 0.75 );
        this.addChild( barSlitSprite, 1 );

        var fillBarSprite = new cc.Sprite( res.Bar_Base_Lobby_png );
        fillBarSprite.setPosition( this.visibleSize.width * 0.5, fillBarSprite.getContentSize().height * 0.75 );
        this.addChild( fillBarSprite, 1 );

        this.lBar = new ccui.LoadingBar( res.Bar_Fill_Lobby_png );
        this.lBar.setPosition( fillBarSprite.getPosition() );
        this.addChild( this.lBar, 1 );
        var fillBarTSprite = new cc.Sprite( res.Bar_Transperant_Lobby_png );
        fillBarTSprite.setPosition( this.visibleSize.width * 0.5, fillBarSprite.getContentSize().height * 0.75 );
        this.addChild( fillBarTSprite, 1 );

        this.menu = new cc.Menu();

        this.fullScreenItem = new cc.MenuItemSprite( new cc.Sprite( res.Full_screen_button_png ), new cc.Sprite( res.Full_screen_button_png ), this.menuCB, this );

        this.fullScreenItem.setPosition( this.visibleSize.width - this.fullScreenItem.getContentSize().width * 0.6, this.visibleSize.height - 30 );
        this.fullScreenItem.setTag( FULL_SCREEN_BTN_TAG );

        this.fullScreenItem.getSelectedImage().setColor( cc.color( 166, 166, 166 ) );
        this.menu.addChild( this.fullScreenItem );

        if ( cc.screen.fullScreen() ) {
            this.fullScreenItem.getNormalImage().setTexture( res.Normal_screen_button_png );
            this.fullScreenItem.getSelectedImage().setTexture( res.Normal_screen_button_png );
        }
        var settingItem = new cc.MenuItemImage( res.Setting_png, res.Setting_png, this.menuCB, this );
        settingItem.setPosition( this.fullScreenItem.getPositionX() - this.fullScreenItem.getContentSize().width * 0.5 - settingItem.getContentSize().width * 0.5 - 10,
            this.fullScreenItem.getPositionY() );
        settingItem.setTag( SETTING_BTN_TAG );

        settingItem.getSelectedImage().setColor( cc.color( 166, 166, 166 ) );
        //settingItem.getSelectedImage().setOpacity( 100 );

        var giftItem = new cc.MenuItemSprite( new cc.Sprite( "#earn_coins_btn.png" ), new cc.Sprite( "#earn_coins_btn.png" ), this.menuCB, this );
        giftItem.setPosition( giftItem.getContentSize().width * 0.9,
            giftItem.getContentSize().height * 0.6 );
        giftItem.setTag( INVITE_BTN_TAG );

        giftItem.getSelectedImage().setColor( cc.color( 166, 166, 166 ) );

        this.menu.addChild( giftItem );


        var vipItem = new cc.MenuItemSprite( new cc.Sprite( "#vip.png" ), new cc.Sprite( "#vip.png" ), this.menuCB, this );
        vipItem.setPosition( vipItem.getContentSize().width/2, this.visibleSize.height - settingItem.getContentSize().height * 0.55 );
        vipItem.setTag( VIP_BTN_TAG );

        vipItem.getSelectedImage().setColor( cc.color( 166, 166, 166 ) );

        this.menu.addChild( vipItem );

        this.bonusY = this.bottomSprit.getContentSize().height * 0.5;
        if ( BonusTimer.getInstance()._haveHourBonus )
        {
            var normal = gaf.Asset.create( res.Collect_gaf ).createObject();

            normal.start();
            normal.setLooped(true);

            var selected = gaf.Asset.create( res.Collect_gaf ).createObject();

            selected.start();
            selected.setLooped(true);

            this.bonusItem = new cc.MenuItemSprite( normal, selected, this.menuCB, this );

            this.bonusItem.setPosition( this.visibleSize.width / 2, this.bonusY );
            this.bonusItem.setTag( BONUS_BTN_TAG );
            this.menu.addChild( this.bonusItem );

            this.bonusItem.getSelectedImage().setPositionY( this.bonusItem.getSelectedImage().getContentSize().height );
            this.bonusItem.getNormalImage().setPositionY( this.bonusItem.getSelectedImage().getContentSize().height );
        }
        else
        {
            this.labelBG = new cc.Sprite( res.Timer_Base_png );
            this.labelBG.setPosition( this.visibleSize.width / 2, this.bonusY );
            this.addChild( this.labelBG, 1 );

            var tempLabel = new cc.LabelTTF( "00:00:00", "DS-DIGIT", 30 );
            this.label = new cc.LabelTTF( "00:00:00", "DS-DIGIT", 30, tempLabel.getContentSize() );
            this.label.setPosition( this.visibleSize.width / 2, this.bonusY );
            this.label.setColor( cc.color( 255, 255, 255 ) );
            this.addChild( this.label, 1 );
        }
        var normal = gaf.Asset.create( res.Buy_Button_gaf ).createObject();

        normal.start();
        normal.setLooped(true);

        var selected = gaf.Asset.create( res.Buy_Button_gaf ).createObject();

        selected.start();
        selected.setLooped(true);

        this.buyBtn = new cc.MenuItemSprite( normal, selected, this.menuCB, this );
        // this.resizer.setFinalScaling( this.buyBtn );

        this.buyBtn.setPosition( this.visibleSize.width * 0.5, this.visibleSize.height - this.buyBtn.getContentSize().height * 0.6 );
        this.buyBtn.setTag( BUY_BTN_TAG );
        this.buyBtn.getSelectedImage().setColor( cc.color( 166, 166, 166 ) );
        this.buyBtn.getSelectedImage().setPositionY( this.buyBtn.getSelectedImage().getContentSize().height );
        this.buyBtn.getNormalImage().setPositionY( this.buyBtn.getSelectedImage().getContentSize().height );
        var normalHalf = gaf.Asset.create( res.Buy_Button_Half_gaf ).createObjectAndRun(true);

        var selectedHalf = gaf.Asset.create( res.Buy_Button_Half_gaf ).createObjectAndRun(true);

        this.buyBtnHalf = new cc.MenuItemSprite( normalHalf, selectedHalf, this.menuCB, this );
        //this.resizer.setFinalScaling( this.buyBtnHalf );

        this.buyBtnHalf.setPosition( this.visibleSize.width * 0.5 - this.buyBtnHalf.getContentSize().width * 0.5 * this.buyBtnHalf.getScaleX(), this.visibleSize.height - this.buyBtnHalf.getContentSize().height * 0.6 * this.buyBtnHalf.getScaleY() );
        this.buyBtnHalf.setTag( BUY_BTN_TAG );
        this.buyBtnHalf.getSelectedImage().setColor( cc.color( 166, 166, 166 ) );
        this.buyBtnHalf.getSelectedImage().setPositionY( this.buyBtnHalf.getSelectedImage().getContentSize().height );
        this.buyBtnHalf.getNormalImage().setPositionY( this.buyBtnHalf.getSelectedImage().getContentSize().height );

        var normalDealHalf = gaf.Asset.create( res.Deal_Button_Half_gaf ).createObjectAndRun(true);

        var selectedDealHalf = gaf.Asset.create( res.Deal_Button_Half_gaf ).createObjectAndRun(true);
        this.dealBtn = new cc.MenuItemSprite( normalDealHalf, selectedDealHalf, this.menuCB, this );

        this.dealBtn.setPosition( this.visibleSize.width * 0.5 + this.buyBtnHalf.getContentSize().width * 0.5 * this.dealBtn.getScaleX(), this.visibleSize.height - this.dealBtn.getContentSize().height * 0.6 * this.dealBtn.getScaleY() );
        this.dealBtn.setTag( DEAL_BTN_TAG );
        this.dealBtn.getSelectedImage().setColor( cc.color( 166, 166, 166 ) );
        this.dealBtn.getSelectedImage().setPositionY( this.dealBtn.getSelectedImage().getContentSize().height );
        this.dealBtn.getNormalImage().setPositionY( this.dealBtn.getSelectedImage().getContentSize().height );

        var tempTimerLabel = new cc.LabelTTF( "00:00", "arial_black", 20 );
        var timerLabel = new cc.LabelTTF( "00:00", "arial_black", 20, tempTimerLabel.getContentSize() );
        var isFirefox = typeof InstallTrigger !== 'undefined';
        if( isFirefox )
        {
            timerLabel.setPosition( this.dealBtn.getContentSize().width * 0.45, this.dealBtn.getContentSize().height * 0.15 );
        }
        else
        {
            timerLabel.setPosition( this.dealBtn.getContentSize().width * 0.45, this.dealBtn.getContentSize().height * 0.25 );
        }
        timerLabel.setTag( TIMER_LABEL_TAG );
        this.dealBtn.addChild( timerLabel );
        var deal = AppDelegate.getInstance().getDealInstance();
        if ( !deal.getDealStatus() )
        {
            this.dealBtn.setEnabled( false );
            this.dealBtn.setVisible( false );
            selectedDealHalf.pauseAnimation();
            normalDealHalf.pauseAnimation();
            //deal.resetDealData();

            this.buyBtnHalf.setEnabled( false );
            this.buyBtnHalf.setVisible( false );
            selectedHalf.pauseAnimation();
            normalHalf.pauseAnimation();
        }
        else
        {
            this.buyBtn.setEnabled( false );
            this.buyBtn.setVisible( false );
            selected.pauseAnimation();
            normal.pauseAnimation();
        }

        this.menu.addChild( settingItem );
        this.menu.addChild( this.buyBtn );
        this.menu.addChild( this.buyBtnHalf );
        this.menu.addChild( this.dealBtn );


        var dailyChallengesItem = new cc.MenuItemImage( res.daily_challenges_png, res.daily_challenges_png, this.menuCB, this );
        //this.resizer.setFinalScaling( dailyChallengesItem );
        dailyChallengesItem.setPosition( this.visibleSize.width * 0.8, dailyChallengesItem.getContentSize().height * 0.6 );
        dailyChallengesItem.setTag( DAILY_CHALLENGES_BTN_TAG );

        dailyChallengesItem.getSelectedImage().setColor( cc.color( 166, 166, 166 ) );

      /*  this.dailyChallengesLabel = new CustomLabel( DailyChallenges.getInstance().getCurrentsTaskStatusString(), "arial_black", 15, cc.size( 37.5, 15 ) );
        this.dailyChallengesLabel.setColor( cc.color( 213, 255, 69, 255 ) );
        this.dailyChallengesLabel.setPosition( cc.p( dailyChallengesItem.getContentSize().width * 0.8, dailyChallengesItem.getContentSize().height * 0.64 ) );
        this.dailyChallengesLabel.setAnchorPoint( cc.p( 0, 0.5 ) );
        dailyChallengesItem.addChild( this.dailyChallengesLabel );


        var totalTaskStatusBG = new cc.Sprite( "#progress_bar_bg.png" );
        totalTaskStatusBG.setScale( 0.27, 0.3 );
        totalTaskStatusBG.setTag( PROGRESS_BAR_TAG );
        totalTaskStatusBG.setPosition( dailyChallengesItem.getContentSize().width * 0.58, dailyChallengesItem.getContentSize().height * 0.32 );

        dailyChallengesItem.addChild( totalTaskStatusBG );


        this.dailyChallengesProgressBar = new ccui.LoadingBar( "#progress_bar_fill.png", 0.01 );
        this.dailyChallengesProgressBar.loadTexture( "progress_bar_fill.png", ccui.Widget.PLIST_TEXTURE );

        //this.dailyChallengesProgressBar.setScale( 0.225, 0.4 );
        this.dailyChallengesProgressBar.setPosition( totalTaskStatusBG.getContentSize().width * 0.5 + 2.5, totalTaskStatusBG.getContentSize().height * 0.5 );
        this.dailyChallengesProgressBar.setPercent( DailyChallenges.getInstance().getCurrentTaskStatusPercent() );

        totalTaskStatusBG.addChild( this.dailyChallengesProgressBar );


        var tempTimerLabel2 = new cc.LabelTTF( "Next in: 00:00:00", "arial_black", 17.5 );
        this.dailyChallengesLabelTimer = new cc.LabelTTF( "Next in: 00:00:00", "arial_black", 17.5, tempTimerLabel2.getContentSize() );
        this.dailyChallengesLabelTimer.setPosition( totalTaskStatusBG.getPosition() );
        dailyChallengesItem.addChild( this.dailyChallengesLabelTimer );

        if( DailyChallenges.getInstance().getCurrentTaskStatusPercent() == 100 )
        {
            totalTaskStatusBG.setVisible( false );
        }
        else
        {
            this.dailyChallengesLabelTimer.setVisible( false );
        }

       */
        this.menu.addChild( dailyChallengesItem );
        this.menu.setPosition( 0, 0 );
        this.addChild( this.menu, 2 );
        this.setScrollView( this.topSprit );
        this.delegate.jukeBox( S_STOP_MUSIC );
        this.delegate.jukeBox( S_LOBBY );
        this.schedule( this.updator, 1.0 );
        this.setTag( sceneTags.LOBBY_SCENE_TAG );

        document.getElementById("support_url").href=support_url;
        var snow = null;
        switch (UserDef.getInstance().getValueForKey(SERVER_OCCASION,0) )
        {
        case 1:
            snow = new cc.ParticleSnow();
            break;

        default:
            break;
        }
        if ( snow ) {
            snow.setTexture(cc.textureCache.addImage( res.FireStar_png ));
            snow.setScale( 1.5 );
            this.addChild( snow, 2 );
            for( var i = 0; i < 5; i++)
            {
                var time = Math.floor( Math.random( ) * 1 );
                 var star = new cc.Sprite(res.FireStar1_png);
                star.setPosition( this.visibleSize.width * 0.5, this.visibleSize.height * 0.8 );
                star.runAction( new cc.RepeatForever(new cc.DelayTime(time),new cc.FadeIn(0.5),new cc.RotateBy( 0.5, 90 ),new cc.FadeOut(0.5),new cc.Sequence(function (star) {
                    var posX = Math.floor( Math.random( ) * this.visibleSize.width ) ;
                    var posY = Math.floor(Math.random() * this.visibleSize.height * 0.9 ) + this.visibleSize.height * 0.6;
                    star.setPosition( cc.p( posX, posY ) );
                    star.setColor( Math.floor( Math.random() * 255 )+20, Math.floor( Math.random( ) * 255 )+30, Math.floor( Math.random( ) * 255 )+40 );
                },this) ) );

                this.addChild( star, 1 );

                // new cc.RotateBy( 1.5, 360 )
            }
        }
        ServerData.getInstance().updateActiveGameMachineOnServer("Lobby","Lobby");

        this.setSomeScreenOnLobby();
        if (UserDef.getInstance().getValueForKey(IS_BUYER,false)){
            this.createHourlyBonusTwoXImage();
        }
       // this.delegate.jukeBox(S_STOP_MUSIC);
       // this.delegate.jukeBox( S_LOBBY );

    },
    touchEvent: function ( itm, type ) {
        if ( this.showingScreen )
        {
            return;
        }
        var unlockedCount = 2;
        for ( var i = iconsDefaultArrangementVec.length - 1; i >= 0; i-- )
        {
            if ( this.def.getValueForKey( LEVEL, 0 ) >= unlockAtLevelArr[i] )
            {
                unlockedCount = i;
                break;
            }
        }
        var tag = itm.getTag();
        switch ( type )
        {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                if ( tag === COMING_SOON_BTN_TAG )
                {
                    //coming soon
                    if ( !this.showingScreen )
                    {
                        this.setComingSoonPage();
                    }
                }
                else if ( GO_TO_CASINO_BTN_TAG === tag )
                {
                    var userdef = CSUserdefauts.getInstance();
                    var data = ServerData.getInstance();
                    cc.sys.openURL( data.server_real_money_url );
                    userdef.setValueForKey( REAL_MONEY_ADV_CLICKED, 1 );
                    this.scrollView.setPosition( cc.p( 0, 2 ) );
                    this.sideBarSprite.removeFromParent( true );
                    this.sideBarSprite = null;
                    userdef.setValueForKey( REAL_MONEY_ADV_CLICK_COUNT, userdef.getValueForKey( REAL_MONEY_ADV_CLICK_COUNT, 0 ) + 1 );
                    ServerData.getInstance().updateCustomDataOnServer();
                }
                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;

            default:
                break;
        }
    },
    leftArrow : null,
    rightArrow : null,
    featuredHotIconsVec : null,
    featuredNewIconsVec : null,
    featuredFavIconsVec:null,

    removeIndexNewIcons:function(arrayData,popIndex){
        if (arrayData.includes(popIndex)){
            for( var i = 0; i < arrayData.length; i++){ if ( arrayData[i] === popIndex) { arrayData.splice(i, 1); i--; }}
        }
        return arrayData;
    },
    enableScreen:function(){
        delegate.showingScreen=false;
        this.scrollView.setEnabled(true);
    },
    setGiftPage:function(){
        this.delegate.showingScreen=true;
        var pop = new GiftPage();
        this.addChild(pop, 10, INVITE_FB_FRIENDS_LAYER_TAG);
        this.giftsReceptionInvoked = true;
    },
    menuRemoveCB:function ( pSender ) {
        var tag = pSender.getTag();
        switch ( tag ) {
            case CLOSE_BTN_AREAR_TAG:{
                this.totalScore  += UserDef.getInstance().getValueForKey(AREARS,0);
                this.addScore = ( this.totalScore - this.displayedScore ) / 10;
                userDataInfo.getInstance().totalChips = this.totalScore;
                this.delegate.jukeBox( S_BTN_CLICK );
                UserDef.getInstance().setValueForKey(AREARS,0);
                this.addCoins();
                this.removeLayer();
                this.delegate.showingScreen = false;
                PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_TOUCH_AREARS_POP);
            }
                break;
            case CLOSE_BTN_TAG:
                if (!this.getParent().getChildByTag(LOADER_TAG)) {
                    this.removeLayer();
                    this.delegate.showingScreen = false;
                }
                break;
            case LIKE_CLOSE_BTN_TAG: {
                this.delegate.assignRewards(2000,true);
                var moveImage = this.getChildByTag(PopUpTags.STAY_IN_TOUCH_LIKE_PAGE).getChildByTag(easyMoveInTag);
                if (moveImage){
                    DPUtils.getInstance().easyBackInAnimToPopup(moveImage,0.3);
                    this.runAction(new cc.Sequence( new cc.DelayTime(0.3),cc.callFunc( function (){
                        this.removeFBLikePage();

                    },this)));

                }else {
                    this.removeFBLikePage();

                }

            }
                break;
        }

    },
    menuCB:function ( pSender ) {
        if (this.delegate.showingScreen || this.isDealLaunch){
            return;
        }
        var tag = pSender.getTag();
        var data = ServerData.getInstance();
        switch ( tag ) {
            case INVITE_BTN_TAG: {
                    this.delegate.showingScreen= true;
                    this.setGiftPage();
            }
                break;
            case FULL_SCREEN_BTN_TAG:
               // this.setLikePage();
                DPUtils.getInstance().toggleFullScreen(pSender, true);
                break;

            case BONUS_BTN_TAG:
                this.delegate.showingScreen= true;
                this.setHourlyPage();
                break;
            case VIP_BTN_TAG:
                this.delegate.showingScreen= true;
                this.setVIP();

                break;
            case SETTING_BTN_TAG:
                this.setSettingPage();
                break;
            case BUY_BTN_TAG:
                switch (data.server_buy_page) {
                    default:
                        this.setBuyPage(BuyPageTags.BUY_CREDITS);
                        break;

                    case 2:
                        if (this.def.getValueForKey(IS_BUYER, false)) {
                            this.setBuyPage(BuyPageTags.BUY_CREDITS);
                        }
                        else {
                            this.setBuyPage(BuyPageTags.FIRST_TIME_CREDITS);
                        }
                        break;
                    case 3:
                    {
                        this.setBuyPage(BuyPageTags.DOUBLE_CREDITS);
                    }
                        break;
                    case 4:
                    {
                        this.setBuyPage(BuyPageTags.OFFER_CREDITS);

                    }
                        break;
                }
                this.def.setValueForKey(BUY_BUTTON_CLICK_COUNT, this.def.getValueForKey(BUY_BUTTON_CLICK_COUNT, 0) + 1);

                break;
            case DEAL_BTN_TAG: {
                var deal = AppDelegate.getInstance().getDealInstance();//DJSDeal.getInstance();
                if (deal.canSetDeal /*&& deal.getNumberOfRunningActions() == 0*/) {
                    if (deal.flushDeal(true)) {
                        DPUtils.getInstance().setTouchSwallowing(this, deal);
                        this.addChild(deal, 5);
                        // keyListner.setEnabled( false );
                    }
                }
            }
                break;
            case DAILY_CHALLENGES_BTN_TAG:
                this.showingScreen = true;
                this.delegate.showingScreen=true;
                var child = new DaliyChallenge( PopUpTags.STAY_IN_TOUCH_DC );
                this.addChild( child, 20 );
                DPUtils.getInstance().setTouchSwallowing( 100, child );
               // DailyChallenges.getInstance().setDailyChallengesPage();
                break;
            case INVITE_COLLECT_BTN_TAG: {
                this.delegate.assignRewards(this.acceptedFriendsNames.length * FB_INVITATION_REWARDS,true);
                var previousFbFriends = this.def.getValueForKey(ACCEPTED_REQUEST_IDS, "");
                for (var i = 0; i < this.acceptedFriendsIds.length; i++) {
                    previousFbFriends = previousFbFriends + this.acceptedFriendsIds[i] + ",";
                    //previousFbFriends.append(",");
                }
                this.acceptedFriendsNames = [];
                this.acceptedFriendsIds = [];
                this.def.setValueForKey(ACCEPTED_REQUEST_IDS, previousFbFriends);
                this.acceptedFriendsNames.length = 0;
                this.acceptedFriendsIds.length = 0;
                pSender.getParent().getParent().getParent().removeFromParent(true);
                this.delegate.invitation_rewards_assigned = true;
                delegate.showingScreen = false;

                PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_IN_TOUCH_ACCEPT_REWARDS);
            }
                break;
            case LEFT_ARROW: {
                var nMoveX = 400;
                //cc.log( str );
                var inner = this.scrollView.getInnerContainer();
                var curPos = inner.getPosition();
                var nextPos = cc.p(curPos.x + nMoveX, curPos.y);
                // prevent scrolling past beginning
                if (nextPos.x > 0) {
                    inner.setPosition(cc.p(0, nextPos.y));
                    this.leftArrow.setVisible(false);
                }else {
                    this.rightArrow.setVisible(true);
                    this.leftArrow.setVisible(true);
                    this.scrollView._startAutoScrollToDestination(nextPos, 0.5, true);
                }
            }
                break;

            case RIGHT_ARROW: {
                var nMoveX = -400;
                var inner = this.scrollView.getInnerContainer();
                var curPos = inner.getPosition();
                var nextPos = cc.p(curPos.x + nMoveX, curPos.y);
                var size = this.scrollView.getContentSize();
                var innerSize = inner.getContentSize();
                var topMostX = size.width - innerSize.width;
                if (nextPos.x < topMostX) {
                    this.rightArrow.setVisible(false)
                    inner.setPosition(cc.p(topMostX, nextPos.y));
                }else {
                    this.rightArrow.setVisible(true);
                    this.leftArrow.setVisible(true);
                    this.scrollView._startAutoScrollToDestination(nextPos, 0.5, true);
                }
            }
                break;

            default:
                break;
        }
        this.delegate.jukeBox( S_BTN_CLICK );

    },
    removeLayer:function(){
        this.scrollView.setEnabled(true);
        this.showingScreen = false;
        if ( this.dNode){
            this.dNode.clear();
        }
        var moveImage = this.getChildByTag(VIP_LAYER_TAG).getChildByTag(easyMoveInTag);
        if (moveImage){
            DPUtils.getInstance().easyBackInAnimToPopup(moveImage,0.3);
            this.runAction(new cc.Sequence( new cc.DelayTime(0.3),cc.callFunc( function ( ){
                this.removeChildByTag(VIP_LAYER_TAG, true);

            },this)));
        }else {
            this.removeChildByTag(VIP_LAYER_TAG, true);
        }
    },
    removeFBLikePage:function(){
        showLikeUsBtn(false);
        this.removeChildByTag(PopUpTags.STAY_IN_TOUCH_LIKE_PAGE, true);
        this.scrollView.setEnabled(true);
        this.showingScreen = false;
        if (this.dNode) {
            this.dNode.clear();
        }
        this.delegate.showingScreen = false;
        PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_IN_TOUCH_LIKE_PAGE);
        ServerData.getInstance().updateCustomDataOnServer();
    },
    setVIP : function()
    {
        this.scrollView.setEnabled( false );
        this.showingScreen = true;

        var layer = new cc.Layer();//Color( cc.color( 0, 0, 0, 160 )/*, this.visibleSize.width, this.visibleSize.height*/ );

        layer.setTag( VIP_LAYER_TAG );

        DPUtils.getInstance().setTouchSwallowing( 100, layer );

        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        layer.addChild( darkBG );

        var vipSprite = new cc.Sprite( res.Lobby_Vip_png );
        vipSprite.setTag(easyMoveInTag);
        vipSprite.setPosition( cc.p( this.visibleSize.width / 2, this.visibleSize.height / 2 ) );

        layer.addChild( vipSprite );


        var i;

        for ( i = 6; i >= 0; i-- )
        {
            if ( this.def.getValueForKey( VIP_POINTS, 0 ) >= vipArray[i] ) {
                break;
            }
        }

        var string = null;

        switch ( i )
        {

            case 0:
                string = "Bronze Member - " + DPUtils.getInstance().getNumString( this.def.getValueForKey( VIP_POINTS, 0 ) ) + "Points ";
                break;

            case 1:
                string = "Silver Member - " + DPUtils.getInstance().getNumString( this.def.getValueForKey( VIP_POINTS, 0 ) ) + " Points ";
                break;

            case 2:
                string = "Gold Member - " + DPUtils.getInstance().getNumString( this.def.getValueForKey( VIP_POINTS, 0 ) ) + " Points ";
                break;

            case 3:
                string = "Sapphire Member - " + DPUtils.getInstance().getNumString( this.def.getValueForKey( VIP_POINTS, 0 ) ) + " Points ";
                break;

            case 4:
                string = "Emerald Member - " + DPUtils.getInstance().getNumString( this.def.getValueForKey( VIP_POINTS, 0 ) ) + " Points ";
                break;

            case 5:
                string = "Ruby Member - " + DPUtils.getInstance().getNumString( this.def.getValueForKey( VIP_POINTS, 0 ) ) + " Points ";
                break;

            case 6:
                string = "Diamond Member - " + DPUtils.getInstance().getNumString( this.def.getValueForKey( VIP_POINTS, 0 ) ) + " Points ";
                break;

            default:
                break;
        }



        if ( string )
        {
            var labelBlack = new cc.LabelTTF( string, "MyriadPro-Regular", 40 );

            labelBlack.setColor( cc.color( 255, 255, 0, 255 ) );

            labelBlack.setPosition( this.visibleSize.width * 0.5, this.visibleSize.height * 0.75 - labelBlack.getContentSize().height * 1.25 );

            vipSprite.addChild( labelBlack );
        }



        if ( i < 6 )
        {
            var progressBackSprite = new cc.Sprite( res.GrandWheelProgress_bar_bg );

            progressBackSprite.setPosition( cc.p( this.visibleSize.width / 2, this.visibleSize.height * 0.62 ) );

            vipSprite.addChild( progressBackSprite );



            var loadBar = new ccui.LoadingBar(res.GrandWheelProgress_bar_fill, 0.01 );
            loadBar.loadTexture( res.GrandWheelProgress_bar_fill, ccui.Widget.LOCAL_TEXTURE );

            loadBar.setPosition( cc.p( progressBackSprite.getContentSize().width * 0.5, progressBackSprite.getContentSize().height * 0.5 ) );

            progressBackSprite.addChild( loadBar );

            loadBar.setPercent( this.def.getValueForKey( VIP_POINTS, 0 ) / vipArray[i + 1] * 100 );



            var pTierStr = "res/vip/" + i + ".png";

            var pTierSprite = new cc.Sprite( pTierStr );

            pTierSprite.setPosition( cc.p( cc.rectGetMinX( progressBackSprite.getBoundingBox() ) - pTierSprite.getContentSize().width / 2, progressBackSprite.getPositionY() ) );

            vipSprite.addChild( pTierSprite );



            var nTierStr = "res/vip/" + ( i + 1 ) + ".png";

            var nTierSprite = new cc.Sprite( nTierStr );

            nTierSprite.setPosition( cc.p( cc.rectGetMaxX( progressBackSprite.getBoundingBox() )  + nTierSprite.getContentSize().width / 2, progressBackSprite.getPositionY() ) );

            vipSprite.addChild( nTierSprite );
        }

        var closeItem = new cc.MenuItemImage( res.Close_Buttpn_png, res.Close_Buttpn_png, this.menuRemoveCB, this );


        closeItem.setPosition( cc.p( 910.5 + closeItem.getContentSize().width / 2, 637 + closeItem.getContentSize().width / 2 ) );

        closeItem.setTag( CLOSE_BTN_TAG );

        // closeItem.getSelectedImage().setColor( cc.color( 169, 169, 169, 255 ) );


        var menu = new cc.Menu;
        menu.addChild( closeItem );
        menu.setPosition( cc.p( 0, 0 ) );
        vipSprite.addChild( menu );

        this.addChild( layer, 10 );
        DPUtils.getInstance().easyBackOutAnimToPopup(vipSprite,0.5);

        // this.resizer.setFinalScaling( layer );//change 31 Aug
    },
    notCalled : true,
    giftsReceptionInvoked : false,
    playMusic:false,
    updator : function ( dt )
    {
        if ( CSUserdefauts.getInstance().getValueForKey(MUSIC, true) && AppDelegate.getInstance().onForGround)
         {
         if ( !AppDelegate.getInstance().sPlayer.isMusicPlaying() )
         {
            // console.log("isBackgroundMusicPlaying false");
             AppDelegate.getInstance().jukeBox( S_LOBBY );
         }else {
            // console.log("isBackgroundMusicPlaying true");

         }
         }

        if (!this.checkPopUpValue && PopUps.getInstance().listOfPopUps && PopUps.getInstance().listOfPopUps.length>0  ){
            this.checkPopUpValue = true;
            PopUps.getInstance().showPopUpAccToIndex(PopUps.getInstance().listOfPopUps[0]);
        }else {
            /*if (!this.playMusic && CSUserdefauts.getInstance().getValueForKey(MUSIC,true)){
                 this.playMusic = true;
                 this.delegate.jukeBox(S_STOP_MUSIC);
                 this.delegate.jukeBox(S_LOBBY);
            }*/
        }
        if( UserDef.getInstance().getValueForKey( BET, 0 ) == 0 && this.notCalled ) {
            // ServerCommunicator.getInstance().SCSpin(0, 0, "firstPage");//Change 19 June
            this.notCalled = false;
        }

        var deal = AppDelegate.getInstance().getDealInstance();//DJSDeal.getInstance();

        var tim = deal.getDealStatus();


        if ( tim )
        {
            var label = this.dealBtn.getChildByTag( TIMER_LABEL_TAG );
            label.setString( tim );

            if ( !this.dealBtn.isEnabled() )
            {
                this.dealBtn.setEnabled( true );
                this.dealBtn.setVisible( true );
                var normal = this.dealBtn.getNormalImage();
                normal.resumeAnimation();

                normal = this.dealBtn.getSelectedImage();
                normal.resumeAnimation();

                this.buyBtnHalf.setEnabled( true );
                this.buyBtnHalf.setVisible( true );
                normal = this.buyBtnHalf.getNormalImage();
                normal.resumeAnimation();

                normal = this.buyBtnHalf.getSelectedImage();
                normal.resumeAnimation();

                this.buyBtn.setEnabled( false );
                this.buyBtn.setVisible( false );
                normal = this.buyBtnHalf.getNormalImage();
                normal.pauseAnimation();

                normal = this.buyBtnHalf.getSelectedImage();
                normal.pauseAnimation();
            }
        }
        else
        {
            if ( this.dealBtn.isEnabled() )
            {
                this.dealBtn.setEnabled( false );
                this.dealBtn.setVisible( false );
                //deal.resetDealData();

                var normal = this.dealBtn.getNormalImage();
                normal.pauseAnimation();

                normal = this.dealBtn.getSelectedImage();
                normal.pauseAnimation();

                this.buyBtnHalf.setEnabled( false );
                this.buyBtnHalf.setVisible( false );
                normal = this.buyBtnHalf.getNormalImage();
                normal.pauseAnimation();

                normal = this.buyBtnHalf.getSelectedImage();
                normal.pauseAnimation();

                this.buyBtn.setEnabled( true );
                this.buyBtn.setVisible( true );
                normal = this.buyBtnHalf.getNormalImage();
                normal.resumeAnimation();

                normal = this.buyBtnHalf.getSelectedImage();
                normal.resumeAnimation();
            }
        }

        var sttr = BonusTimer.getInstance().updateTimeForHourlyBonus();

        BonusTimer.getInstance().updateTimeForDaliyBonus();
        if ( BonusTimer.getInstance()._haveHourBonus )
        {
            if ( !cc.colorEqual( this.coinSprite.getColor(), cc.color( 255, 255, 255 ) ) )
            {
                this.coinSprite.setTextureRect( cc.rect( 0, 0, this.coinSprite.getContentSize().width, this.coinSprite.getContentSize().height + 25 ) );
                this.coinSprite.setColor( cc.color( 255, 255, 255 ) );
                this.lBar.setPercent( 100 );

                if ( this.bonusItem )
                {
                    this.bonusItem.setEnabled( true );
                    this.bonusItem.setVisible( true );
                    if ( this.label )
                    {
                        this.label.setVisible( false );
                        this.labelBG.setVisible( false );
                    }
                }
                else
                {
                    var normal = gaf.Asset.create( res.Collect_gaf ).createObject();

                    normal.start();
                    normal.setLooped(true);

                    var selected = gaf.Asset.create( res.Collect_gaf ).createObject();

                    selected.start();
                    selected.setLooped(true);

                    this.bonusItem = new cc.MenuItemSprite( normal, selected, this.menuCB, this );

                    this.bonusItem.setPosition( this.visibleSize.width / 2, this.bonusY );
                    this.bonusItem.setTag( BONUS_BTN_TAG );
                    //this.resizer.setFinalScaling( bonusItem );//change 8 Dec
                    this.menu.addChild( this.bonusItem );

                    this.bonusItem.getSelectedImage().setPositionY( this.bonusItem.getSelectedImage().getContentSize().height );
                    this.bonusItem.getNormalImage().setPositionY( this.bonusItem.getSelectedImage().getContentSize().height );

                    if ( this.label )//change 8 Dec
                    {
                        this.label.setVisible( false );
                        this.labelBG.setVisible( false );
                    }
                }
            }
        }
        else
        {
            if ( !cc.colorEqual(this.coinSprite.getColor(), cc.color( 166, 166, 166 ) ) )
            {
                this.coinSprite.setColor( cc.color( 166, 166, 166 ) );
                this.coinSprite.setTextureRect( cc.rect( 0, 0, this.coinSprite.getContentSize().width, this.coinSprite.getContentSize().height - 25 ) );
            }

            if ( this.label )
            {
                this.label.setVisible( true );
                this.labelBG.setVisible( true );
                this.label.setString( sttr );
            }
            else
            {
                this.labelBG = new cc.Sprite( res.Timer_Base_png );
                this.labelBG.setPosition(  this.visibleSize.width / 2, this.bonusY );
                this.addChild( this.labelBG, 1 );

                var tempLabel = new cc.LabelTTF( "00:00:00", "DS-DIGIT", 30 );
                this.label = new cc.LabelTTF( "00:00:00", "DS-DIGIT", 30, tempLabel.getContentSize() );
                this.label.setPosition( cc.p( this.visibleSize.width / 2, this.bonusY ) );
                this.label.setColor( cc.color( 255, 255, 255, 255 ) );
                this.addChild( this.label, 1 );
            }
            if ( this.bonusItem )
            {
                this.bonusItem.setEnabled( false );
                this.bonusItem.setVisible( false );
            }
            this.lBar.setPercent( BonusTimer.getInstance()._percentValue );
        }

        if( ServerData.getInstance().offer_id )
        {
            if( !this.showingScreen && !cc.director.getRunningScene().getChildByTag( SPLASH_TAG ) )
            {
                this.setOfferPopup();
            }
        }
        else if( FacebookObj && FacebookObj.receivedGiftsDetails && !this.giftsReceptionInvoked )//change 19 June
        {

            //cc.log( "Set a gift reception pop here!!" );
            if( this.isFirstLaunch )
            {
                for( var i = 0; i < FacebookObj.receivedGiftsDetails.data.length; i++ )
                {
                    //cc.log( "response.data[" + i + "].id = " + FacebookObj.receivedGiftsDetails.data[i].from.id );
                    //cc.log( "response.data[" + i + "].name = " + FacebookObj.receivedGiftsDetails.data[i].from.name );
                    FacebookObj.deleteRequest( FacebookObj.receivedGiftsDetails.data[i].id );
                }
                this.giftsReceptionInvoked = true;
            }
            PopUps.getInstance().addPopUpIndex(PopUpTags.STAY_IN_TOUCH_GIFT_POP);

        }
        /* else if ( BonusTimer.getInstance()._haveDailyBonus )
         {
             if ( !this.showingScreen )
             {
                 BonusTimer.getInstance().setTime( BONUS_DAY, 60*60*24);
                 BonusTimer.getInstance()._haveDailyBonus = false;
                 BonusTimer.getInstance().updateTimeForDaliyBonus();
                 this.setDailyBonusPage();
             }
         }*/

        /*var myAsset = gaf.Asset.create(res.Collect_gaf);
         var myAnimation = myAsset.createObjectAndRun(true);
         myAnimation.setPosition( 500, 500 );
         this.addChild( myAnimation, 100 );*/


        var t = ServerData.getInstance().current_time_seconds;

        if ( CSUserdefauts.getInstance().getValueForKey(HAPPY_HOURS, 0) - t <= 0 && this.haveHappyHours )
        {
            CSUserdefauts.getInstance().deleteValueForKey( HAPPY_HOURS );

            this.haveHappyHours = false;
            AppDelegate.getInstance().haveHappyHoursRunning = false;

        }
        else if( !this.haveHappyHours && CSUserdefauts.getInstance().getValueForKey(HAPPY_HOURS, 0) - t > 0 )
        {
            this.haveHappyHours = true;
            AppDelegate.getInstance().haveHappyHoursRunning = true;

        }

      /*  var challenges = DailyChallenges.getInstance();

        challenges.checkDailyChallengeState();


        if ( challenges.getCurrentTaskStatusPercent() == 100 )
        {
            if ( !this.dailyChallengesLabelTimer.isVisible() )
            {
                this.dailyChallengesLabelTimer.setVisible( true );
                this.dailyChallengesProgressBar.getParent().setVisible( false );
            }

            this.dailyChallengesLabelTimer.setString( challenges.getTimeString() );
        }
        else if ( this.dailyChallengesLabelTimer.isVisible() )
        {
            this.dailyChallengesLabelTimer.setVisible( false );
            this.dailyChallengesProgressBar.getParent().setVisible( true );
        }*/
    },
    setSettingPage : function()
    {
        this.showingScreen = true;
        this.scrollView.setEnabled( false );

        var layer = new cc.Layer();//Color( cc.color( 0, 0, 0, 166 ) );
        layer.setTag( VIP_LAYER_TAG );
        layer.setPosition( cc.p( 0, 0 ) );
        DPUtils.getInstance().setTouchSwallowing( 100, layer );
        this.addChild( layer, 10 );



        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( this.visibleSize.width * 0.5, this.visibleSize.height * 0.5 ) );
        layer.addChild( darkBG );

        //this.resizer.setFinalScaling( layer );

        var buySprite = new cc.Sprite( "#bg.png" );
        buySprite.setPosition( cc.p( this.visibleSize.width / 2, this.visibleSize.height / 2 ) );
        buySprite.setTag(easyMoveInTag);
        layer.addChild( buySprite );

        var buyMenu = new cc.Menu();

        buyMenu.setPosition( cc.p( 0, 0 ) );
        buySprite.addChild( buyMenu );

        var x = 0;//1236;
        var y = 0;
        var maxX = 0;
        var minX = 0;

        for ( var i = 2; i >= 0; i-- )
        {
            var buyBtn = null;
            /*if (i == 3)
            {
                y = 56.5;
                x = buySprite.getContentSize().width / 2;
                buyBtn = new cc.MenuItemSprite( new cc.Sprite( "#game_center.png" ), new cc.Sprite( "#game_center.png" ), this.settingMenuCB, this );


                buyBtn.setTag( GAME_CENTER_BTN_TAG );

            }
            else*/ if (i == 2)
        {
            y = 56.5;//131.5;
            x = buySprite.getContentSize().width / 2;
            buyBtn = new cc.MenuItemSprite( new cc.Sprite( "#customer_support.png" ), new cc.Sprite( "#customer_support.png" ), this.settingMenuCB, this );
            buyBtn.setTag( SUPPORT_BTN_TAG );
        }
        else if( i <= 1 )
        {
            y = 131.5;//207;
            if ( i == 1 )
            {
                buyBtn = new cc.MenuItemSprite( new cc.Sprite( "#sound.png" ), new cc.Sprite( "#sound.png" ), this.settingMenuCB, this );

                var sprite = new cc.Sprite( "#sound_disabled.png" );
                sprite.setTag( SOUND_BTN_TAG );
                buyBtn.addChild( sprite );
                sprite.setPosition( cc.p( sprite.getContentSize().width / 2, buyBtn.getContentSize().height / 2 ) );
                if ( CSUserdefauts.getInstance().getValueForKey( SOUND, true ) )
                {
                    sprite.setVisible( false );
                }

                buyBtn.setTag( SOUND_BTN_TAG );
                x = minX + buyBtn.getContentSize().width * 0.5;
            }
            else
            {
                buyBtn = new cc.MenuItemSprite( new cc.Sprite( "#music.png" ), new cc.Sprite( "#music.png" ), this.settingMenuCB, this );

                var sprite = new cc.Sprite( "#music_disabled.png" );
                sprite.setTag( MUSIC_BTN_TAG );
                buyBtn.addChild( sprite );
                sprite.setPosition( cc.p(sprite.getContentSize().width / 2, buyBtn.getContentSize().height / 2 ) );
                if ( CSUserdefauts.getInstance().getValueForKey( MUSIC, true ) )
                {
                    sprite.setVisible( false );
                }

                buyBtn.setTag( MUSIC_BTN_TAG );
                x = maxX - buyBtn.getContentSize().width * 0.5;
            }
        }

            if ( buyBtn )
            {
                buyBtn.setPosition( cc.p( x, y ) );
                buyBtn.getSelectedImage().setColor( cc.color( 169, 169, 169, 255 ) );

                buyMenu.addChild( buyBtn );
                if ( minX == 0 )
                {
                    maxX = cc.rectGetMaxX( buyBtn.getBoundingBox() );
                    minX = cc.rectGetMinX( buyBtn.getBoundingBox() );
                }
            }
        }

        var versionStr = "App Version: " + APP_VERSION + ", Build Version " + BUILD_VERSION;//change 13 May
        var versionLabel = new cc.LabelTTF( versionStr, "MyriadPro-Regular", 15 );
        versionLabel.setPosition( cc.p( buySprite.getContentSize().width / 2, versionLabel.getContentSize().height * 0.75 ) );
        buySprite.addChild( versionLabel );

        var closeItem = new cc.MenuItemImage( res.Close_Button_2_png, res.Close_Button_2_png, this.menuRemoveCB, this );

        closeItem.setPosition( cc.p( buySprite.getContentSize().width - closeItem.getContentSize().width / 2, buySprite.getContentSize().height - closeItem.getContentSize().height / 2 ) );
        closeItem.setTag( CLOSE_BTN_TAG );

        closeItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );

        buyMenu.addChild( closeItem );
        DPUtils.getInstance().easyBackOutAnimToPopup(buySprite,0.3);
        //this.resizer.setFinalScaling( buySprite );
    },

    settingMenuCB : function( pSender )
    {

        var tag = pSender.getTag();

        switch ( tag )
        {
            case MUSIC_BTN_TAG:
                if ( CSUserdefauts.getInstance().getValueForKey( MUSIC, true ) )
                {
                    pSender.getChildByTag( tag ).setVisible( true );
                    this.delegate.jukeBox( S_STOP_MUSIC );
                    CSUserdefauts.getInstance().setValueForKey( MUSIC, false );
                }
                else
                {
                    pSender.getChildByTag( tag ).setVisible( false );

                    CSUserdefauts.getInstance().setValueForKey( MUSIC, true );
                    this.delegate.jukeBox( S_LOBBY );
                }
                break;

            case SOUND_BTN_TAG:

                if (  CSUserdefauts.getInstance().getValueForKey( SOUND, true ) )
                {
                    pSender.getChildByTag( tag ).setVisible( true );
                    this.delegate.jukeBox( S_STOP_EFFECTS );
                    CSUserdefauts.getInstance().setValueForKey( SOUND, false );

                }
                else
                {
                    CSUserdefauts.getInstance().setValueForKey( SOUND, true );
                    pSender.getChildByTag( tag ).setVisible( false );
                }
                break;

            case SUPPORT_BTN_TAG:
                var email = this.def.getValueForKey("userEmail");
                support_url = SUPPORT_BASE_URL + "email=" + email + "&app_id=" + APP_ID + "&user_id=" + userDataInfo.getInstance().user_id;
                if (window.open(support_url)){

                }else{
                    var email = this.def.getValueForKey("userEmail");
                    var subject = 'Feedback - Double Jackpot Slots (Support Id :'+userDataInfo.getInstance().user_id+')';
                    var bodyMesage = "[Please Explain what is happening here]"+"\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nModel: "+ServerData.getInstance().device_model+"\nDevice_Version: "+ServerData.getInstance().device_version+"\n App Version: "+APP_VERSION+"\nEmail id: "+email;
                    location.href = this.getMailtoUrl("support@phonato.com",subject,bodyMesage);
                }

                break;

            case GAME_CENTER_BTN_TAG:
                //SendMessageWithParams( "gameCenterAction", nullptr );
                break;

            default:
                break;
        }
        this.delegate.jukeBox( S_BTN_CLICK );
    },
    getMailtoUrl:function(to, subject, body){
        var args = [];
        if (typeof subject !== 'undefined') {
            args.push('subject=' + encodeURIComponent(subject));
        }
        if (typeof body !== 'undefined') {
            args.push('body=' + encodeURIComponent(body))
        }

        var url = 'mailto:' + encodeURIComponent(to);
        if (args.length > 0) {
            url += '?' + args.join('&');
        }
        return url;
    },
    setDailyBonusPage : function()
    {
        var t = ServerData.getInstance().current_time_seconds;

        this.def.setValueForKey( BONUS_DAY, t );
        this.delegate._haveDailyBonus = false;
        this.scrollView.setEnabled( false );
        this.showingScreen = true;

        this.delegate.jukeBox( S_WHEEL );

        var bgSprite = new cc.Sprite( res.Daily_spin_wheel_bg_png );
        bgSprite.setPosition( cc.p( this.visibleSize.width / 2, this.visibleSize.height / 2 ) );

        var ab = 1;
        var degree = 40;
        for ( var i = 0; i < 2; i++ )
        {
            var lightSprite = new cc.Sprite( res.Wheel_Light_png );
            lightSprite.setAnchorPoint( cc.p( 0.5, 0 ) );
            lightSprite.setPosition( this.visibleSize.width / 2, 25 );
            this.addChild( lightSprite, 5 );
            lightSprite.setRotation( degree * ab );
            lightSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.RotateBy( 3.0, -degree * 2 * ab ), new cc.DelayTime( 0.1 ), new cc.RotateBy( 3.0, degree * 2 * ab ), new cc.DelayTime( 0.1 ) ) ) );

            lightSprite.setTag( LIGHT_TAG );

            ab = -1;
        }
        bgSprite.setTag( LAYER_TAG );
        DPUtils.getInstance().setTouchSwallowing( this, bgSprite );


        this.addChild( bgSprite, 5 );

        var wheelSprite = new cc.Sprite( res.Daily_spin_wheel_png );
        wheelSprite.setPosition( cc.p( bgSprite.getContentSize().width / 2, bgSprite.getContentSize().height / 2 + 10 ) );
        wheelSprite.setTag( WHEEL_TAG );
        bgSprite.addChild( wheelSprite );


        var stopperSprite = new cc.Sprite( res.Daily_spin_wheel_stopper_png );
        stopperSprite.setPosition( bgSprite.getContentSize().width / 2, bgSprite.getContentSize().height / 2 + stopperSprite.getContentSize().height * 0.72 );
        stopperSprite.setTag( WHEEL_STOPPER_TAG );
        bgSprite.addChild( stopperSprite );

        var spinItm = new cc.Sprite( res.Daily_spin_wheel_Btn_png );

        spinItm.setPosition( bgSprite.getContentSize().width / 2, bgSprite.getContentSize().height / 2 );
        bgSprite.addChild( spinItm );

        bgSprite.setTag( D_NODE_TAG );

        var lowerSprite = new cc.Sprite( res.Daily_spin_wheel_Pop_png );
        lowerSprite.setPosition( lowerSprite.getContentSize().width / 2, -lowerSprite.getContentSize().height / 2 );
        lowerSprite.runAction( new cc.MoveTo( 0.5, cc.p( lowerSprite.getContentSize().width / 2, lowerSprite.getContentSize().height / 2 ) ) );
        bgSprite.addChild( lowerSprite, 5 );


        var dayOfBonus = (userDataInfo.getInstance().consecutive_days_count+1);
        var rewardDayCount = dayOfBonus++;
        if( rewardDayCount > ServerData.getInstance().bonus_max_consecutive_days){
          rewardDayCount =  ServerData.getInstance().bonus_max_consecutive_days;
        }
        var  todayBonus = ServerData.getInstance().daily_bonus_coins * rewardDayCount;

        var tBonusString = "$" + DPUtils.getInstance().getNumString( todayBonus );
        var tBonusLabel = new cc.LabelTTF( tBonusString, "HELVETICALTSTD-BOLD", 45 );
        tBonusLabel.setPosition( lowerSprite.getContentSize().width * 0.1, 110 );
        lowerSprite.addChild( tBonusLabel );

        var wBonusLabel = new cc.LabelTTF( "$0", "HELVETICALTSTD-BOLD", 45 );
        wBonusLabel.setPosition( lowerSprite.getContentSize().width * 0.4, 110 );
        lowerSprite.addChild( wBonusLabel );

        var VIPBonusLabel = new cc.LabelTTF( "$0", "HELVETICALTSTD-BOLD", 45 );
        VIPBonusLabel.setPosition( lowerSprite.getContentSize().width * 0.68, 110 );
        lowerSprite.addChild( VIPBonusLabel );

        var totalLabel = new cc.LabelTTF( tBonusString, "HELVETICALTSTD-BOLD", 45 );
        totalLabel.setPosition( lowerSprite.getContentSize().width * 0.9, 110 );
        lowerSprite.addChild( totalLabel );

        DPUtils.getInstance().setTouchSwallowing( null, bgSprite );

        this.spinMenuCB( null );
    },
    createHourlyBonusTwoXImage:function(){
        var sp2x = new cc.Sprite("res/2x.png");
        sp2x.setPosition(cc.p(this.coinSprite.getPositionX()+this.coinSprite.getContentSize().width/2,this.coinSprite.getPositionY()+sp2x.getContentSize().height));
        this.addChild(sp2x,2);
        var nodeScaleX = sp2x.getScaleX();
        var nodeScaleY = sp2x.getScaleY();
        var largeScale = 1.1 ;
        sp2x.runAction(new cc.RepeatForever( new cc.Sequence(new cc.ScaleTo(0.20,nodeScaleX*largeScale ,nodeScaleY*largeScale),new cc.ScaleTo(0.20,nodeScaleX ,nodeScaleY))));
    },
    spinMenuCB : function( pSender )
    {

        this.delegate.jukeBox( S_BONUS_WIN );

        var sprite = this.getChildByTag( D_NODE_TAG ).getChildByTag( WHEEL_TAG );

        if ( sprite.getRotation() != 0 )
        {
            return;
        }
        BonusTimer.getInstance().setTime( BONUS_DAY, 60*60*24);
        BonusTimer.getInstance()._haveDailyBonus=false;
        BonusTimer.getInstance().updateTimeForDaliyBonus();
        var tot = this.getBonusWin();
        userDataInfo.getInstance().bonusChips = tot;
        ServerData.getInstance().collectBonus("daily_bonus", tot);
        this.displayedScore= this.totalScore;
        sprite = this.getChildByTag( D_NODE_TAG ).getChildByTag( WHEEL_TAG );
        sprite.runAction( new cc.Sequence( new cc.RotateBy( 3, 720 ), new cc.RotateBy( 1, ( ( ( 16 - this.curBonusIndx ) * 22.5 ) ) + 10 ),
            new cc.RotateBy( 0.25, -10 ), new cc.DelayTime( 1 ), cc.callFunc( function ( sprite ){

                var lowerSprite = new cc.Sprite( res.Daily_spin_wheel_Pop_png );
                lowerSprite.setPosition( lowerSprite.getContentSize().width / 2, lowerSprite.getContentSize().height / 2 );
                sprite.getParent().addChild( lowerSprite, 5 );

                var animSprite = new cc.Sprite( "#single_line1.png" );


//2.create spriteframe array
                var animFrames = [];
                for (var i = 1; i < 20; i++) {
                    var str = "single_line" + i + ".png";
                    var frame = cc.spriteFrameCache.getSpriteFrame(str);
                    animFrames.push(frame);
                }
//3.create a animation with the spriteframe array along with a period time
                var animation = new cc.Animation(animFrames, 0.1);

                var action = cc.animate( animation );

                animSprite.runAction( action );

                animSprite.setPosition( lowerSprite.getContentSize().width / 2, lowerSprite.getContentSize().height / 2 );
                lowerSprite.addChild( animSprite );


                var tBonusString = "$" + DPUtils.getInstance().getNumString( this.todayBonus );
                var tBonusLabel = new cc.LabelTTF( tBonusString, "HELVETICALTSTD-BOLD", 45 );
                tBonusLabel.setPosition( lowerSprite.getContentSize().width * 0.1, 110 );
                lowerSprite.addChild( tBonusLabel );

                var wBonusString = "$" + DPUtils.getInstance().getNumString( this.bonusWin );
                var wBonusLabel = new cc.LabelTTF( wBonusString, "HELVETICALTSTD-BOLD", 45 );
                wBonusLabel.setPosition( lowerSprite.getContentSize().width * 0.40, tBonusLabel.getPositionY() );
                lowerSprite.addChild( wBonusLabel );

                var VIPBonusString = "$" + DPUtils.getInstance().getNumString( this.VIPBonus );
                var VIPBonusLabel = new cc.LabelTTF( VIPBonusString, "HELVETICALTSTD-BOLD", 45 );
                VIPBonusLabel.setPosition( lowerSprite.getContentSize().width * 0.68, tBonusLabel.getPositionY() );
                lowerSprite.addChild( VIPBonusLabel );

                var totalString = "$" + DPUtils.getInstance().getNumString( tot );
                var totalLabel = new CustomLabel( totalString, "HELVETICALTSTD-BOLD", 45, new cc.size( 170, 45 ) );
                totalLabel.setPosition( lowerSprite.getContentSize().width * 0.9, tBonusLabel.getPositionY() );
                lowerSprite.addChild( totalLabel );

                //ServerCommunicator.getInstance().SCBonusCollection( 1, tot );
                // ServerCommunicator.getInstance().SCUpdateData();

                lowerSprite.runAction( new cc.Sequence( new cc.DelayTime( 3.5 ), cc.callFunc( function( sprite ){
                    sprite.getParent().removeFromParent( );

                    var light = this.getChildByTag( LIGHT_TAG );
                    while ( light )
                    {
                        light.removeFromParent( true );
                        light = this.getChildByTag( LIGHT_TAG );
                    }
                    if (this.dNode){
                        this.dNode.clear();
                    }
                    this.showingScreen = false;
                    this.scrollView.setEnabled( true );
                    this.totalScore = this.totalScore+userDataInfo.getInstance().bonusChips;
                    this.addScore = ( this.totalScore - this.displayedScore ) / 10;
                    this.unschedule( this.updateWinCoin );
                    this.schedule( this.updateWinCoin, 0.08 );
                    userDataInfo.getInstance().totalChips = this.totalScore;
                    userDataInfo.getInstance().bonusChips =0;
                    this.delegate.jukeBox( S_STOP_MUSIC );
                    this.delegate.jukeBox( S_STOP_EFFECTS );
                    this.delegate.jukeBox( S_LOBBY );
                    this.delegate.jukeBox( S_COIN );
                    this.addSurpiseBonusCoinsLbl(this.totalScore+userDataInfo.getInstance().bonusChips);
                    if( ServerData.getInstance().server_paid_wheel_control >=1  &&
                        ServerData.getInstance().server_paid_wheel_control <= this.def.getValueForKey( LEVEL ) )//#change Dec 13
                    {
                        var wheelLayer = new GrandWheelLayer( PAID_WHEEL, null );
                        DPUtils.getInstance().setTouchSwallowing( null, wheelLayer );
                        this.addChild( wheelLayer, 50 );

                    }else{
                        this.delegate.showingScreen = false;

                        PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_TOUCH_DAILY_BONUS_POP);

                    }
                    //DailyChallenges::getInstance().updateTaskData( C_DAILY_BONUS_COUNT, 1 );

                }, this, tot, sprite ) ) );

            }, this, tot, sprite ) ) );
    },

    getBonusWin : function()
    {
        var total = 0;

        var totalProb = 0;

        for ( var i = 0; i < 16; i++ )
        {
            if( this.def.getValueForKey( IS_BUYER, false ) )
            {
                totalProb+=dailySpinProbArrBuyer[i];
            }
            else
            {
                totalProb+=dailySpinProbArr[i];
            }//Change 19 Dec
        }

        this.curBonusIndx = Math.floor( Math.random( ) * totalProb );
        totalProb = 0;

        for ( var i = 0; i < 16; i++ )
        {
            //totalProb+=dailySpinProbArr[i];
            if( this.def.getValueForKey( IS_BUYER, false ) )
            {
                totalProb+=dailySpinProbArrBuyer[i];
            }
            else
            {
                totalProb+=dailySpinProbArr[i];
            }//change 19 Dec

            if ( totalProb >= this.curBonusIndx )
            {
                this.curBonusIndx = i;
                break;
            }
        }

        this.bonusWin = dailySpinArr[this.curBonusIndx];
        var userData = userDataInfo.getInstance();
        var dayOfBonus = (userData.consecutive_days_count+1);
        userData.consecutive_days_count++;
        var rewardDayCount = dayOfBonus;
        if( rewardDayCount > ServerData.getInstance().bonus_max_consecutive_days){
            rewardDayCount =  ServerData.getInstance().bonus_max_consecutive_days;
        }
        this.todayBonus = ServerData.getInstance().daily_bonus_coins * rewardDayCount;

        var vip = this.def.getValueForKey( VIP_POINTS, 0 );

        var i;
        for ( i = 6; i >= 0; i-- )
        {
            if ( vipArray[i] <= vip )
            {
                break;
            }
        }

        var bonusPercent = 0;

        switch ( i ) {
            case 1:
                bonusPercent = 10;
                break;

            case 2:
                bonusPercent = 30;
                break;

            case 3:
                bonusPercent = 50;
                break;

            case 4:
                bonusPercent = 75;
                break;

            case 5:
                bonusPercent = 100;
                break;

            case 6:
                bonusPercent = 150;
                break;

            default:
                break;
        }

        this.VIPBonus = ( this.bonusWin * ( bonusPercent / 100.0 ) );

        total = this.VIPBonus + this.bonusWin + this.todayBonus;

        return total;
    },
    updateWinCoin : function( dt )
    {
        this.displayedScore+=this.addScore;
        if ( this.displayedScore >= this.totalScore )
        {
            this.displayedScore = this.totalScore;
            this.unschedule( this.updateWinCoin );
        }
        this.setScoreLabel();

        var l = this.getChildByTag( VIP_LAYER_TAG );
        if ( l )
        {
            if ( l.getChildByTag( 444 ) )
            {
                var label = l.getChildByTag( 444 ).getChildByTag( VIP_LAYER_TAG );
                if ( label )
                {
                    var label2 = this.getChildByTag( SCORE_LABEL_TAG );

                    if( label2 )
                    {
                        label.setString( label2.getString() );
                    }
                }
            }
        }
    },
    setHappyHoursPop : function()
    {
        var data = ServerData.getInstance();
        this.delegate.showHappyHoursPop = false;
        var defualts = CSUserdefauts.getInstance();
        var t = ServerData.getInstance().current_time_seconds;
        if ( defualts.getValueForKey( NEXT_HAPPY_HOUR_IN, 0 ) - t  <= 0 )
        {
            this.haveHappyHours = true;
            AppDelegate.getInstance().haveHappyHoursRunning = true;
            defualts.setValueForKey( HAPPY_HOURS, t + data.server_happy_hours * SECONDS_IN_AN_HOUR );
            defualts.setValueForKey( NEXT_HAPPY_HOUR_IN, t + data.server_happy_hours * SECONDS_IN_A_DAY );

            var dNode = new cc.LayerColor( cc.color( 0, 0, 0, 169 ) );
            dNode.setPosition( cc.p( 0, 0 ) );

            var darkBG = new cc.Sprite( res.Dark_BG_png );
            darkBG.setScale( 2 );
            darkBG.setOpacity( 169 );
            darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
            dNode.addChild( darkBG );

            DPUtils.getInstance().setTouchSwallowing( this, dNode );

            var popBackSprite = new cc.Sprite( res.Happy_Hour_Pop_png );
            popBackSprite.setPosition( cc.p( this.visibleSize.width / 2, this.visibleSize.height / 2 ) );
            dNode.addChild( popBackSprite );

            // this.resizer.setFinalScaling( popBackSprite );

            var closeItem = new cc.MenuItemImage( res.Close_Buttpn_png, res.Close_Buttpn_png, this.happyPopCB, this );

            closeItem.setPosition( cc.p( 810 - closeItem.getContentSize().width / 2, 490 - closeItem.getContentSize().width / 2 ) );
            closeItem.setTag( CLOSE_BTN_TAG );

            closeItem.getSelectedImage().setColor( cc.color( 169, 169, 169, 0 ) );

            var menu = new cc.Menu();
            menu.addChild( closeItem );
            menu.setPosition( cc.p( 0, 0 ) );
            popBackSprite.addChild( menu );


            this.addChild( dNode, 5, VIP_LAYER_TAG );

            this.showingScreen = true;
            popBackSprite.setTag(easyMoveInTag);
            DPUtils.getInstance().easyBackOutAnimToPopup(popBackSprite,0.5);
        }
    },

    happyPopCB : function( pSender )
    {
        /*if ( this.delegate.getChangingScene() ) {
         return;
         }*/

        pSender.getParent().getParent().getParent().removeFromParent( true );

        this.showingScreen = false;
        this.delegate.jukeBox( S_BTN_CLICK );
        this.delegate.showingScreen = false;

        PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_TOUCH_HAPPY_HOUR_POP);

    },

    doShowSpecialOfferPop : function()
    {
        var data = ServerData.getInstance();
        var doShow =  false
        var showSaleViewCountt = this.def.getValueForKey("showSaleViewCount", 0);
        if ( showSaleViewCountt == 0 )
        {
            var value = Math.floor( Math.random( ) * 3 );
            if ( value == 2 && data.server_salepopup_frequency && this.def.getValueForKey( everUsedSpecialPopup, false ) )
            {
                doShow = true;
            }
        }

        if( ! this.def.getValueForKey( everUsedSpecialPopup, false ) )
        {
            this.def.setValueForKey( everUsedSpecialPopup, true );
        }
        return doShow;
    },

    setSpecialOfferPop : function()
    {
        var showSaleViewCountt = this.def.getValueForKey("showSaleViewCount", 0);

        var data = ServerData.getInstance();
        var currentOfferCode = 1;
        switch ( data.server_buy_page )
        {
            case 2:
                if ( this.def.getValueForKey( IS_BUYER, false ) ) {
                    currentOfferCode = BuyPageTags.SPECIAL_OFFER_1;
                }
                else
                {
                    currentOfferCode = BuyPageTags.SPECIAL_OFFER_2;
                }
                break;

            case 3:
                currentOfferCode = BuyPageTags.SPECIAL_OFFER_2;
                break;

            case 4:
                currentOfferCode = BuyPageTags.SPECIAL_OFFER_3;
                break;

            default:
                currentOfferCode = BuyPageTags.SPECIAL_OFFER_1;
                break;
        }
        if( cc.textureCache.getTextureForKey( "res/lobby/buyPage_1_19/specialofferpopup/" + ( currentOfferCode - BuyPageTags.SPECIAL_OFFER_1 + 1 ) + ".png" ) ) {
            this.def.setValueForKey("showSaleViewCount", showSaleViewCountt);

            var offer = new SpecialOffers(currentOfferCode);
            this.addChild(offer, 6);
            DPUtils.getInstance().setTouchSwallowing(100, offer);
            offer.setPosition(cc.p(0, 0));
            offer.setTag(PopUpTags.STAY_TOUCH_SPECIAL_OFFER_POP);
            this.showingScreen = true;
        }
        else
        {
            this.showingScreen = true;
            this.setLoader();
            var node = new cc.Node();
            this.addChild( node );
            node.schedule( function( dt ){
                if( cc.textureCache.getTextureForKey( "res/lobby/buyPage_1_19/specialofferpopup/" + ( currentOfferCode - BuyPageTags.SPECIAL_OFFER_1 + 1 ) + ".png" ) ) {
                    this.def.setValueForKey("showSaleViewCount", showSaleViewCountt);

                    this.getParent().setSpecialOfferPop();
                    node.removeFromParent( true );
                    cc.director.getRunningScene().removeChildByTag( SPLASH_TAG );
                }
            } , 0.5 );

        }
    },

    setComingSoonPage : function()
    {//change 31 Aug
        this.scrollView.setEnabled( false );
        this.showingScreen = true;

        this.delegate.jukeBox( S_BTN_CLICK );
        var layer = new cc.Layer();//Color( cc.color( 0, 0, 0, 160 ) );
        layer.setTag( VIP_LAYER_TAG );
        this.addChild( layer, 10 );

        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        layer.addChild( darkBG );

        DPUtils.getInstance().setTouchSwallowing( this, layer );


        var buySprite = new cc.Sprite( "#coming_soon_bg.png" );
        buySprite.setPosition( cc.p( this.visibleSize.width / 2, this.visibleSize.height / 2 ) );
        layer.addChild( buySprite );

        var closeItem = new cc.MenuItemSprite( new cc.Sprite( "#ok.png" ), new cc.Sprite( "#ok.png" ), this.menuRemoveCB, this );

        closeItem.setPosition( cc.p( buySprite.getContentSize().width / 2, 50 ) );
        closeItem.setTag( CLOSE_BTN_TAG );

        closeItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );


        var menu = new cc.Menu();
        menu.addChild( closeItem );
        menu.setPosition( cc.p( 0, 0 ) );
        buySprite.addChild( menu );
        buySprite.setTag(easyMoveInTag);
        DPUtils.getInstance().easyBackOutAnimToPopup(buySprite,0.5);
    },

    setBuyPage : function(pageid) {
        var buyOfferPage = new BuyPages(pageid);
        buyOfferPage.setPosition(cc.p(0, 0));
        this.addChild(buyOfferPage, 5);
        DPUtils.getInstance().setTouchSwallowing(100, buyOfferPage);
    },
    setInstantDealPop : function(pageid) {
        var buyOfferPage = new InstantDealPopup(pageid);
        buyOfferPage.setTag(PopUpTags.STAY_IN_TOUCH_INSTANT_DEAL);
        buyOfferPage.setPosition(cc.p(0, 0));
        this.addChild(buyOfferPage, 5);
        DPUtils.getInstance().setTouchSwallowing(100, buyOfferPage);
    },
    addCoins : function()
    {
        this.delegate.jukeBox( S_COIN_MORE );

        for ( var i = 0; i < 10; i++ )
        {
            var r = 1;
            var cSprite = new cc.Sprite( res.Coin_png );
            cSprite.setPosition( this.coinSprite.getPosition() );
            cSprite.runAction( new cc.RepeatForever( new cc.RotateBy( 0.75 + Math.random(),  0, 360, 0 ) ) );
            cSprite.runAction( new cc.ScaleTo( 1, 0.25 ) );
            cSprite.setTag( i );
            var bezier = [ cSprite.getPosition(), cc.p( this.visibleSize.width * 3/4, this.visibleSize.height / 2 ), this.coinSpriteSmall.getPosition() ];
            var bTo = new cc.BezierTo( 1, bezier );
            if ( i == 9 )
            {
                cSprite.runAction( new cc.Sequence( new cc.DelayTime( 0.1 * i ), bTo, cc.callFunc( function( cSprite ) {//lambda
                        this.coinSpriteSmall.stopAllActions();
                        this.coinSpriteSmall.setScale(1.0);
                        cSprite.removeFromParent(true);
                    }
                    , this ) ) );
            }
            else
            {
                cSprite.runAction( new cc.Sequence( new cc.DelayTime( 0.1 * i ), bTo, cc.callFunc( function ( cSprite ){
                    if( cSprite.getTag() == 0 )
                    {
                        this.unschedule( this.updateWinCoin );
                        this.schedule( this.updateWinCoin, 0.08 );
                    }
                    cSprite.removeFromParent( true );
                    this.coinSpriteSmall.setScale( 1.0 );
                    var sTo = new cc.ScaleBy( 0.05, 1.2 );
                    this.coinSpriteSmall.runAction( new cc.Sequence( sTo, sTo.reverse() ) );
                }, this ) ) );
            }
            this.addChild(cSprite, 1);
        }
    },
    isOpenBox:false,
    setHourlyPage:function(){
        this.scrollView.setEnabled( false );

        this.showingScreen = true;
        this.isOpenBox = false;
        var layer = new cc.Layer();//Color( cc.color( 0, 0, 0, 160 )/*, this.visibleSize.width, this.visibleSize.height*/ );

        layer.setTag( VIP_LAYER_TAG );

        DPUtils.getInstance().setTouchSwallowing( 100, layer );

        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        layer.addChild( darkBG );
        var imagesPath = "res/lobby/bottom_bar/hourlyBonus/";

        var hourlyBonusBG = new cc.Sprite( imagesPath + "Hourly_BonusBG.png" );
        hourlyBonusBG.setTag(easyMoveInTag);
        hourlyBonusBG.setPosition( cc.p( this.visibleSize.width / 2, this.visibleSize.height / 2 ) );

        layer.addChild( hourlyBonusBG );

        var borderObject = gaf.Asset.create("res/lobby/bottom_bar/hourlyBonus/hourly_bonus_border/hourly_bonus_border.gaf").createObjectAndRun( true );
        borderObject.setPosition( cc.p( 0, hourlyBonusBG.getContentSize().height) );
        borderObject.setTag( 10001 );
        borderObject.setVisible( true );
        borderObject.start();
        hourlyBonusBG.addChild( borderObject );

        var coinTextAnim = gaf.Asset.create("res/lobby/bottom_bar/hourlyBonus/hourly_bonus_textcollectbonus/hourly_bonus_textcollectbonus.gaf").createObjectAndRun( true );
        coinTextAnim.setPosition( cc.p( hourlyBonusBG.getContentSize().width*0.29, hourlyBonusBG.getContentSize().height*0.85) );
        coinTextAnim.setTag( 10001 );
        coinTextAnim.setVisible( true );
        coinTextAnim.start();
        hourlyBonusBG.addChild( coinTextAnim );



        var collect_btn = new ccui.Button(imagesPath+"collect_btn.png",imagesPath+"collect_tap_btn.png");
        collect_btn.setPosition( cc.p( hourlyBonusBG.getContentSize().width*0.26, hourlyBonusBG.getContentSize().height*0.17 ) );
        hourlyBonusBG.addChild(collect_btn);

        var getIt_btn = new ccui.Button(imagesPath+"GetIt_btn.png",imagesPath+"GetIt_tap_btn.png");
        getIt_btn.setPosition( cc.p( hourlyBonusBG.getContentSize().width*0.73, collect_btn.getPositionY() ) );
        hourlyBonusBG.addChild(getIt_btn);

        var boxImage = new ccui.Button(imagesPath+"box_close.png",imagesPath+"box_close.png");
        boxImage.setPosition( cc.p( getIt_btn.getPositionX(), hourlyBonusBG.getContentSize().height*0.44 ) );
        hourlyBonusBG.addChild(boxImage);

        var boxAnim = gaf.Asset.create(imagesPath +"box_Anim/box_Anim.gaf" ).createObject();
        boxAnim.start();
        boxAnim.playSequence("ClosedBox_Loop", true);//ClosedBox_Change ,ClosedBox_Loop
        boxAnim.setPosition(cc.p(0,boxImage.getContentSize().height));
        boxImage.addChild(boxAnim);
        boxAnim.setFrame(1);

        var hourly_bonus_coinsAnim = gaf.Asset.create("res/lobby/bottom_bar/hourlyBonus/hourly_bonus_coins/hourly_bonus_coins.gaf").createObjectAndRun( true );
        hourly_bonus_coinsAnim.setPosition( cc.p( hourlyBonusBG.getContentSize().width*0.15, hourlyBonusBG.getContentSize().height*0.57) );
        hourly_bonus_coinsAnim.setTag( 10001 );
        hourly_bonus_coinsAnim.setVisible( true );
        hourly_bonus_coinsAnim.start();
        hourlyBonusBG.addChild( hourly_bonus_coinsAnim );

        var coin_Image = new cc.Sprite( UserDef.getInstance().getValueForKey(IS_BUYER,false)? imagesPath+"Hourly_1000.png": imagesPath + "Hourly_500.png" );
        coin_Image.setPosition( cc.p( collect_btn.getPositionX(), hourlyBonusBG.getContentSize().height*0.27 ) );
        hourlyBonusBG.addChild(coin_Image);

        collect_btn.addTouchEventListener(function (sender,type) {

            if (type == ccui.Widget.TOUCH_ENDED){
                AppDelegate.getInstance().jukeBox(S_BTN_CLICK);
                collect_btn.setTouchEnabled(false);
                getIt_btn.setTouchEnabled(false);
                boxImage.setTouchEnabled(false);
                var bonus = 500;
                if (UserDef.getInstance().getValueForKey(IS_BUYER,false)){
                    bonus = ServerData.getInstance().hourly_bonus_coins*2;
                }else{
                    bonus = ServerData.getInstance().hourly_bonus_coins;
                }
                sender.getParent().getParent().getParent().collectBonusSeleted(bonus);
            }

        },this);

        var boxBonusLbl = new CustomLabel( "" + 0, "TT0504M_", 32, boxImage.getContentSize() );
        boxBonusLbl.setColor( cc.color.GREEN );
        boxBonusLbl.setPosition( cc.p( boxImage.getPositionX(),boxImage.getPositionY()+10) );
        hourlyBonusBG.addChild( boxBonusLbl );
        boxBonusLbl.setVisible(false);

        var callbackFun = function (sender,type) {

            if (type == ccui.Widget.TOUCH_ENDED){
                collect_btn.setTouchEnabled(false);
                getIt_btn.setTouchEnabled(false);
                boxImage.setTouchEnabled(false);
                AppDelegate.getInstance().jukeBox(S_GIFT_BOX_OPEN);
                boxAnim.playSequence("CloseBox_Change", false);//ClosedBox_Change ,ClosedBox_Loop
                boxAnim.start();
                var value  = sender.getParent().getParent().getParent().getBoxRewardBonus();
                boxAnim.runAction(new cc.Sequence(new cc.DelayTime(0.5),new cc.CallFunc(function () {
                    boxAnim.playSequence("OpenBox_Loop", true);
                    boxAnim.start();
                    boxBonusLbl.runAction(new cc.Sequence(new cc.DelayTime(0.6),new cc.CallFunc(function () {
                        boxBonusLbl.setString(value);
                        boxBonusLbl.setVisible(true);
                        boxBonusLbl.runAction(new cc.Sequence(new cc.MoveBy(0.5,cc.p(0,80)),new cc.DelayTime(0.6),new cc.CallFunc(function () {
                            sender.getParent().getParent().getParent().collectBonusSeleted(value);
                        },this)));
                    },this)));

                    //sender.getParent().getParent().getParent().collectBonusSeleted();

                },this)));
            }

        };

        getIt_btn.addTouchEventListener(callbackFun);
        boxImage.addTouchEventListener(callbackFun);

        this.addChild( layer, 100 );
        DPUtils.getInstance().easyBackOutAnimToPopup(hourlyBonusBG,0.5);

    },
    collectBonusSeleted:function(bonusAmt){
        this.removeChildByTag(VIP_LAYER_TAG);
        if (bonusAmt<=0){
            bonusAmt = 500;
        }
        ServerData.getInstance().collectBonus("hourly_bonus", bonusAmt);
        BonusTimer.getInstance().setTime( BONUS_HOUR, 60*60);
        BonusTimer.getInstance()._haveHourBonus = false;
        BonusTimer.getInstance().updateTimeForHourlyBonus();
        var t = ServerData.getInstance().current_time_seconds;
        this.def.setValueForKey(BONUS_HOUR, t);
        this.delegate._haveHourBonus = false;
        this.bonusItem.setEnabled(false);
        this.bonusItem.setVisible(false);
        this.coinSprite.setColor(cc.color(166, 166, 166));
        this.coinSprite.setTextureRect(cc.rect(0, 0, this.coinSprite.getContentSize().width, this.coinSprite.getContentSize().height - 25));

        this.displayedScore = this.totalScore;
        this.totalScore += bonusAmt;
        this.addScore = ( this.totalScore - this.displayedScore ) / 10;
        userDataInfo.getInstance().totalChips = this.totalScore;
        this.addCoins();
        this.addSurpiseBonusCoinsLbl(bonusAmt);
        AppDelegate.getInstance().showingScreen = false;
        this.scrollView.setEnabled( true );
        this.showingScreen = false;

    },
    sumOfProbabilities:function(probabilityArray){
        var sum = 0;
        for (var number in probabilityArray) {

            sum+=probabilityArray[number];
        }
        return sum;
    },
    fallingIndexOfNumber:function(probabilityArray,num){
        var range = 0, idx = 0;
        for (var number in probabilityArray) {

            range += probabilityArray[number];
            if (num < range) {
                break;
            }
            idx++;
        }
        //    log("CSAppHelper.fallingIndexOfNumber# Rand: %zd, fallingIdx: %zd", num, idx);
        return idx;
    },
    getBoxRewardBonus:function(){
        var buyerMultiplyer = UserDef.getInstance().getValueForKey(IS_BUYER,false)?2:1;
        var hourlyBonusArr = [300,400,500,600,700,800,900,1000,1100,1200,1300,1400 ];
        var  hourlyBonusProbArr = [3,4,6,8,2,3,2,2,2,1,1,1];
        var probabilityTotal = this.sumOfProbabilities(hourlyBonusProbArr);
        var num = Math.floor( Math.random() * probabilityTotal );
        var curBonusIndx    = this.fallingIndexOfNumber(hourlyBonusProbArr, num<0?0:num);
        var TotalbonusWin = hourlyBonusArr[curBonusIndx] * buyerMultiplyer;
        return TotalbonusWin;
    },
    removeDailyChanges:function(){
        this.showingScreen = false;
        AppDelegate.getInstance().showingScreen = false;
        this.removeChildByTag(PopUpTags.STAY_IN_TOUCH_DC);
    },
    addSurpiseBonusCoinsLbl:function(rewardCoins){
        var labl = this.getChildByTag( SCORE_LABEL_TAG );
        var surpiseBonusCoinsLbl = this.getChildByTag( SURPRISE_LABEL_TAG );
        if (surpiseBonusCoinsLbl){
            surpiseBonusCoinsLbl.stopAllActions();
            surpiseBonusCoinsLbl.removeFromParent();
        }
        var surpiseBonusCoinsLbl = new CustomLabel( "+"+DPUtils.getInstance().getNumString(rewardCoins), "TT0504M_", 30, new cc.size( 215, 30 ) );
        surpiseBonusCoinsLbl.setColor( cc.color.GREEN );
        surpiseBonusCoinsLbl.setPosition( cc.p( labl.getPositionX()+labl.getContentSize().width/3 , labl.getPositionY()-labl.getContentSize().height ) );
        surpiseBonusCoinsLbl.setTag( SURPRISE_LABEL_TAG );
        this.addChild( surpiseBonusCoinsLbl, 2 );
        surpiseBonusCoinsLbl.runAction(new cc.Sequence( new cc.MoveBy(1.5,cc.p(-labl.getPositionX()/3,labl.getContentSize().height*1.5)),cc.callFunc(function () {
            surpiseBonusCoinsLbl.removeFromParent();
        },this)));
    },
    setScrollViewArray:function(){
        var joiningVersion =  CSUserdefauts.getInstance().getValueForKey(USER_JOINING_VERSION,-1);
        if (joiningVersion<=-1){
            CSUserdefauts.getInstance().setValueForKey(USER_JOINING_VERSION,APP_VERSION); //USER_JOINING_VERSION userJoiningVersion
        }
        if(userDataInfo.getInstance().favourite_machines.length>0){
            this.featuredFavIconsVec = userDataInfo.getInstance().favourite_machines ;
            this.removeIndexNewIcons(this.featuredFavIconsVec,2026);
            this.removeIndexNewIcons(this.featuredFavIconsVec,2027);
            this.featuredFavIconsVec  = this.removeIndexNewIcons(this.featuredFavIconsVec,2028);

        }

        var data = ServerData.getInstance();
        if( data.server_hot_machine_order && data.server_hot_machine_order.length )
        {
            this.featuredHotIconsVec = DPUtils.getInstance().extractVectorFromSring( data.server_hot_machine_order );
            if (this.featuredHotIconsVec && this.featuredHotIconsVec.length){
                if (this.featuredHotIconsVec[0]==0){
                    this.featuredHotIconsVec.splice(0,1);
                    if (this.featuredHotIconsVec) {
                        var i = 0;
                        for ( i = 0; i < this.featuredHotIconsVec.length; i++) {
                            if (this.featuredHotIconsVec[i] >= TOTAL_SLOT_MACHINES || this.featuredHotIconsVec[i] < DJS_QUINTUPLE_5X || UserDef.getInstance().getValueForKey( SERVER_MACHINE_UNAVAILABLE, "" ).indexOf( "" + this.featuredHotIconsVec[i] ) != -1) {
                                this.featuredHotIconsVec.splice(i, 1);
                                i = 0;
                            }
                        }
                    }
                }else{
                    this.featuredHotIconsVec =[];
                    data.server_hot_machine_order ="";
                }
            }
        }
        if( data.server_new_machine_order && data.server_new_machine_order.length )
        {

            this.featuredNewIconsVec = this.extractVectorFromSring( data.server_new_machine_order );
            if (this.featuredNewIconsVec && this.featuredNewIconsVec.length>0  ) {
                var joiningVersion =  CSUserdefauts.getInstance().getValueForKey(USER_JOINING_VERSION,1.18);
                var serverVersion = this.featuredNewIconsVec[ 0 ];
                if (joiningVersion>serverVersion){
                    this.featuredNewIconsVec=[];
                    data.server_new_machine_order="";
                }else {
                    this.featuredNewIconsVec.splice(0, 1);
                    if (this.featuredNewIconsVec) {
                        var newIcons = [];
                        for (var i = 0; i < this.featuredNewIconsVec.length; i++) {
                            newIcons.push(parseInt(this.featuredNewIconsVec[i]));
                        }
                        this.featuredNewIconsVec = newIcons;
                    }
                    if (this.featuredHotIconsVec && this.featuredNewIconsVec) {
                        for (var j = 0; j < this.featuredHotIconsVec.length; j++) {
                            var hotvalue = this.featuredHotIconsVec[j];
                            for (var j = 0; j < this.featuredNewIconsVec.length; j++) {
                                if (this.featuredNewIconsVec[j] == hotvalue) {
                                    this.featuredNewIconsVec.splice(j, 1);
                                }
                            }
                        }
                    }
                    if (this.featuredNewIconsVec){
                        var idx = 0;
                        while (idx < this.featuredNewIconsVec.length) {
                            if (this.featuredNewIconsVec[idx] >= TOTAL_SLOT_MACHINES || this.featuredNewIconsVec[idx] < DJS_QUINTUPLE_5X ||  UserDef.getInstance().getValueForKey( SERVER_MACHINE_UNAVAILABLE, "" ).indexOf( "" + this.featuredNewIconsVec[idx] ) != -1) {
                                this.featuredNewIconsVec.splice(idx, 1);
                                idx = 0;
                            } else {
                                idx++;

                            }
                        }
                    }

                }
            }
        }
        // console.log("featuredNewIconsVec"+this.featuredNewIconsVec);
        //  console.log("featuredHotIconsVec"+this.featuredHotIconsVec);

        iconsDefaultArrangementVec = data.server_machine_order;
    },
    setScrollView:function(tpSprite){
        this.setScrollViewArray();
        var unlockedCount = 2;
        for ( var i = iconsDefaultArrangementVec.length - 1; i >= 0; i-- )
        {
            if ( this.def.getValueForKey( LEVEL, 0 ) >= unlockAtLevelArr[i] )
            {
                unlockedCount = i;
                break;
            }
        }
        var scollFrameSize = new cc.Size( this.visibleSize.width, this.visibleSize.height - 15 );
        this.scrollView = new ccui.ScrollView();
        this.scrollView.setContentSize( scollFrameSize );
        this.scrollView.addEventListener(function(ref, eventType)
        {
            if ( eventType == ccui.ScrollView.EVENT_CONTAINER_MOVED )
            {
                //this.pauseResumeGAFAnimations();
                var inner = ref.getParent().scrollView.getInnerContainer();
                var curPos  = inner.getPosition();
                var nextPos = cc.p(curPos.x, curPos.y);

                //auto ws = Director::getInstance().getWinSize();
                var size = ref.getParent().scrollView.getContentSize();
                var innerSize = inner.getContentSize();
                var topMostX = size.width - innerSize.width;

                // prevent scroll past end
                if ( nextPos.x < topMostX )
                {
                    ref.getParent().rightArrow.setVisible(false)
                }
                else if( nextPos.x > 0 )
                {
                    ref.getParent().leftArrow.setVisible(false);
                }
                else if( ref.getParent().rightArrow )
                {
                    ref.getParent().rightArrow.setVisible(true);
                    ref.getParent().leftArrow.setVisible(true);
                }
            }
        }, this);

        if ( this.sideBarSprite )
        {
            this.scrollView.setPosition( cc.rectGetMaxX( this.sideBarSprite.getBoundingBox() ), 25 );
        }
        else
        {
            this.scrollView.setPosition( 0, 25 );
        }

        this.scrollView.setDirection( ccui.ScrollView.DIR_HORIZONTAL );
        this.scrollView.setBounceEnabled( true );
        this.scrollView.setScrollBarPositionFromCornerForHorizontal( cc.p( 0, 150 ) );

        var gapX = -1;
        var posX = 0;

        var width = 0, height = 0;
        var isLocked;
        var lastPlayMachineId = CSUserdefauts.getInstance().getValueForKey("lastPlayMachineId",-1);

         var lastPlayedVector = [lastPlayMachineId];
        if (lastPlayMachineId>=DJS_QUINTUPLE_5X && lastPlayMachineId<TOTAL_SLOT_MACHINES){
            var menuItem = new CustomButton( lastPlayMachineId, 0, false);

            if ( gapX == -1 )
            {
                gapX = ( this.visibleSize.width - ( menuItem.getContentSize().width * 4 ) ) * 0.25;
                if ( !this.sideBarSprite )
                {
                    gapX = ( this.visibleSize.width - ( menuItem.getContentSize().width * 4.2 ) ) * 0.25;
                }
            }
            menuItem.setPosition( cc.p( gapX + menuItem.getContentSize().width * 0.5 * menuItem.getScaleX(), this.visibleSize.height / 2 + 25 ) );
            posX = menuItem.getPositionX();
            menuItem.setTag( lastPlayMachineId );
            this.scrollView.addChild( menuItem );
            var featureSprite = new cc.Sprite( res.last_played_theme );
            featureSprite.setPosition( cc.p( featureSprite.getContentSize().width * 0.4, menuItem.getContentSize().height * 0.9 ) );
            menuItem.addChild( featureSprite, 10 );
            width = menuItem.getContentSize().width;
            height = menuItem.getContentSize().height;
            if( this.featuredHotIconsVec && this.featuredHotIconsVec.length >= 1 )
            {
                for (var idx =0;idx<this.featuredHotIconsVec.length;idx++){
                     if (lastPlayMachineId == this.featuredHotIconsVec[idx]){
                         this.featuredHotIconsVec.splice(idx,1);
                     }
                }

            }
            if( this.featuredNewIconsVec && this.featuredNewIconsVec.length >= 1 )
            {
                for (var idx =0;idx<this.featuredNewIconsVec.length;idx++){
                    if (lastPlayMachineId == this.featuredNewIconsVec[idx]){
                        this.featuredNewIconsVec.splice(idx,1);
                    }
                }

            }

        }
        if( this.featuredHotIconsVec && this.featuredHotIconsVec.length >= 1 )
        {
            for ( var i = 0; i < this.featuredHotIconsVec.length; i++ )
            {
                var menuItem = new CustomButton( this.featuredHotIconsVec[i], i, false);

                if ( gapX == -1 )
                {
                    gapX = ( this.visibleSize.width - ( menuItem.getContentSize().width * 4 ) ) * 0.25;
                    if ( !this.sideBarSprite )
                    {
                        gapX = ( this.visibleSize.width - ( menuItem.getContentSize().width * 4.2 ) ) * 0.25;
                    }
                }

                if ( i == 0 )
                {
                    if ( lastPlayMachineId>=DJS_QUINTUPLE_5X &&  lastPlayMachineId<TOTAL_SLOT_MACHINES ){
                        menuItem.setPosition( gapX + posX + menuItem.getContentSize().width * menuItem.getScaleX() , this.visibleSize.height / 2 + 25 );
                        posX = menuItem.getPositionX();
                    }else{
                        menuItem.setPosition( gapX + menuItem.getContentSize().width * 0.5 * menuItem.getScaleX() , this.visibleSize.height / 2 + 25 );
                        posX = menuItem.getPositionX();
                    }
                }
                else
                {
                    menuItem.setPosition( cc.p( gapX + posX + menuItem.getContentSize().width * menuItem.getScaleX(), this.visibleSize.height / 2 + 25 ) );
                    posX = menuItem.getPositionX();
                }
                menuItem.setTag( this.featuredHotIconsVec[i] );
                this.scrollView.addChild( menuItem );
                width = menuItem.getContentSize().width;
                height = menuItem.getContentSize().height;
                var featureSprite = new cc.Sprite( res.hot_machine_theme );
                featureSprite.setPosition( cc.p( featureSprite.getContentSize().width * 0.4, menuItem.getContentSize().height * 0.9 ) );
                menuItem.addChild( featureSprite, 10 );

            }
        }
        if( this.featuredNewIconsVec && this.featuredNewIconsVec.length >= 1 ) {
            for (var i = 0; i < this.featuredNewIconsVec.length; i++) {
                if( this.featuredHotIconsVec && this.featuredHotIconsVec.length && this.featuredHotIconsVec.indexOf( this.featuredNewIconsVec[i] ) > -1 )
                    continue;//#changed as per discussion with Raghib on Nov 21 2017

                var menuItem = new CustomButton( this.featuredNewIconsVec[i], i, false);

                if ( gapX == -1 )
                {
                    gapX = ( this.visibleSize.width - ( menuItem.getContentSize().width * 4 ) ) * 0.25;
                    if ( !this.sideBarSprite )
                    {
                        gapX = ( this.visibleSize.width - ( menuItem.getContentSize().width * 4.2 ) ) * 0.25;
                    }
                }
                if ( i == 0  )
                {
                    if ( (this.featuredHotIconsVec && this.featuredHotIconsVec.length >= 1 ) || (lastPlayMachineId>=DJS_QUINTUPLE_5X &&  lastPlayMachineId<TOTAL_SLOT_MACHINES) ){
                        menuItem.setPosition( gapX + posX + menuItem.getContentSize().width * menuItem.getScaleX() , this.visibleSize.height / 2 + 25 );

                        posX = menuItem.getPositionX();
                    }else{
                        menuItem.setPosition( gapX + menuItem.getContentSize().width * 0.5 * menuItem.getScaleX() , this.visibleSize.height / 2 + 25 );
                        posX = menuItem.getPositionX();
                    }

                }
                else
                {
                    menuItem.setPosition( gapX + posX + menuItem.getContentSize().width * menuItem.getScaleX() , this.visibleSize.height / 2 + 25 );
                    posX = menuItem.getPositionX();
                }
                menuItem.setTag( this.featuredNewIconsVec[ i ] );

                this.scrollView.addChild( menuItem );
                //this.addChild( menuItem );

                //buttonsVec.push_back( menuItem );
                width = menuItem.getContentSize().width;
                height = menuItem.getContentSize().height;
                var featureSprite = new cc.Sprite( res.new_machine_theme );
                featureSprite.setPosition( cc.p( featureSprite.getContentSize().width * 0.4, menuItem.getContentSize().height * 0.9 ) );
                //featureSprite.setScale( 0.6 );
                menuItem.addChild( featureSprite, 10 );
            }


        }
        if( this.featuredFavIconsVec && this.featuredFavIconsVec.length >= 1 ) {
            for (var i = 0; i < this.featuredFavIconsVec.length; i++) {
                if( this.featuredHotIconsVec && this.featuredHotIconsVec.length && this.featuredHotIconsVec.indexOf( this.featuredFavIconsVec[i] ) > -1 )
                    continue;//#changed as per discussion with Raghib on Nov 21 2017
                if( this.featuredNewIconsVec && this.featuredNewIconsVec.length && this.featuredNewIconsVec.indexOf( this.featuredFavIconsVec[i] ) > -1 )
                    continue;//#changed as per discussion with Raghib on Nov 21 2017
                if (this.featuredFavIconsVec[i]==lastPlayMachineId)
                    continue;

                var menuItem = new CustomButton( this.featuredFavIconsVec[i], i, false);

                if ( gapX == -1 )
                {
                    gapX = ( this.visibleSize.width - ( menuItem.getContentSize().width * 4 ) ) * 0.25;
                    if ( !this.sideBarSprite )
                    {
                        gapX = ( this.visibleSize.width - ( menuItem.getContentSize().width * 4.2 ) ) * 0.25;
                    }
                }
                if ( i == 0  )
                {
                    if ( (this.featuredHotIconsVec && this.featuredHotIconsVec.length >= 1 ) ||(this.featuredNewIconsVec && this.featuredNewIconsVec.length >= 1 )|| (lastPlayMachineId>=DJS_QUINTUPLE_5X &&  lastPlayMachineId<TOTAL_SLOT_MACHINES) ){
                        menuItem.setPosition( gapX + posX + menuItem.getContentSize().width * menuItem.getScaleX() , this.visibleSize.height / 2 + 25 );

                        posX = menuItem.getPositionX();
                    }else{
                        menuItem.setPosition( gapX + menuItem.getContentSize().width * 0.5 * menuItem.getScaleX() , this.visibleSize.height / 2 + 25 );
                        posX = menuItem.getPositionX();
                    }

                }
                else
                {
                    menuItem.setPosition( gapX + posX + menuItem.getContentSize().width * menuItem.getScaleX() , this.visibleSize.height / 2 + 25 );
                    posX = menuItem.getPositionX();
                }
                menuItem.setTag( this.featuredFavIconsVec[ i ] );

                this.scrollView.addChild( menuItem );
                var favBtn = new FavriteButton(this.featuredFavIconsVec[ i ],i,true);
                favBtn.setPosition( cc.p(menuItem.getContentSize().width*0.8, menuItem.getContentSize().height * 0.1) );
                menuItem.addChild( favBtn, 10 );
                favBtn.setIcons();
                width = menuItem.getContentSize().width;
                height = menuItem.getContentSize().height;
            }

        }
        /* Featured end */
//778602010001156 brijesh yadav UBIN0577863
        for ( var i = 0; i < iconsDefaultArrangementVec.length; i++ )
        {
            isLocked = true;
            if( i <= unlockedCount )
            {
                isLocked = false;
            }

            //if( std::find( featuredIconsVec.begin(), featuredIconsVec.end(), iconsDefaultArrangementVec.at( i ) ) != featuredIconsVec.end() )
            if(( this.featuredHotIconsVec && this.featuredHotIconsVec.length && this.featuredHotIconsVec.indexOf( iconsDefaultArrangementVec[i] ) > -1) || this.featuredNewIconsVec && this.featuredNewIconsVec.length && this.featuredNewIconsVec.indexOf( iconsDefaultArrangementVec[i] ) > -1 )
                continue;//#changed as per discussion with Raghib on Nov 21 2017
             if (iconsDefaultArrangementVec[i]==lastPlayMachineId)
                 continue;
            if( this.featuredFavIconsVec && this.featuredFavIconsVec.length && this.featuredFavIconsVec.indexOf( iconsDefaultArrangementVec[i] ) > -1)
                continue;

            var menuItem = new CustomButton( iconsDefaultArrangementVec[ i ], i, isLocked);

            if ( gapX == -1 )
            {
                gapX = ( this.visibleSize.width - ( menuItem.getContentSize().width * 4 ) ) * 0.25;
                if ( !this.sideBarSprite )
                {
                    gapX = ( this.visibleSize.width - ( menuItem.getContentSize().width * 4.2 ) ) * 0.25;
                }
            }

            if ( i == 0  )
            {
                if ( (this.featuredHotIconsVec && this.featuredHotIconsVec.length >= 1 )|| (this.featuredNewIconsVec && this.featuredNewIconsVec.length >= 1 )|| (lastPlayMachineId>=DJS_QUINTUPLE_5X &&  lastPlayMachineId<TOTAL_SLOT_MACHINES) ){
                    menuItem.setPosition( gapX + posX + menuItem.getContentSize().width * menuItem.getScaleX() , this.visibleSize.height / 2 + 25 );

                    posX = menuItem.getPositionX();
                }else{
                    menuItem.setPosition( gapX + menuItem.getContentSize().width * 0.5 * menuItem.getScaleX() , this.visibleSize.height / 2 + 25 );
                    posX = menuItem.getPositionX();
                }

            }
            else
            {
                menuItem.setPosition( gapX + posX + menuItem.getContentSize().width * menuItem.getScaleX() , this.visibleSize.height / 2 + 25 );
                posX = menuItem.getPositionX();
            }

            menuItem.setTag( iconsDefaultArrangementVec[ i ] );

            this.scrollView.addChild( menuItem );
            if (!isLocked){
                var favBtn = new FavriteButton(iconsDefaultArrangementVec[ i ],i,false);
                favBtn.setPosition( cc.p(menuItem.getContentSize().width*0.8, menuItem.getContentSize().height * 0.1) );
                menuItem.addChild( favBtn, 10 );
                favBtn.setIcons();
            }
            width = menuItem.getContentSize().width;
            height = menuItem.getContentSize().height;
        }

        var menuItem = new ccui.Button();

        menuItem.loadTextures( "comming_soon.png", "comming_soon.png", "", ccui.Widget.PLIST_TEXTURE );

        menuItem.setPosition( gapX + posX + menuItem.getContentSize().width * menuItem.getScaleX(), this.visibleSize.height / 2 + 25 );
        posX = menuItem.getPositionX();


        menuItem.setTag( COMING_SOON_BTN_TAG );

        menuItem.addTouchEventListener( this.touchEvent, this );

        this.scrollView.addChild( menuItem, 2 );

        this.addChild( this.scrollView, 1 );
        var containerSize;
        if ( !this.sideBarSprite )
        {
            containerSize = cc.size( posX + width * 0.6, this.visibleSize.height );//change 19 Dec
        }
        else
        {
            containerSize = cc.size( posX + width * 1.5, this.visibleSize.height );//change 19 Dec
        }
        this.scrollView.setInnerContainerSize( containerSize );

        this.leftArrow = new cc.MenuItemSprite( new cc.Sprite( "res/arrow_btn.png" ), new cc.Sprite( "res/arrow_btn.png" ), this.menuCB, this );
        //this.resizer.setFinalScaling( this.leftArrow );
        this.leftArrow.setPosition( this.leftArrow.getContentSize().width * 0.5, this.visibleSize.height * 0.5 );
        this.leftArrow.setTag( LEFT_ARROW );
        this.leftArrow.setRotation( 180 );
        this.leftArrow.setVisible( false );

        this.rightArrow = new cc.MenuItemSprite( new cc.Sprite( "res/arrow_btn.png" ), new cc.Sprite( "res/arrow_btn.png" ), this.menuCB, this );
        // this.resizer.setFinalScaling( this.rightArrow );
        this.rightArrow.setPosition( this.visibleSize.width - this.rightArrow.getContentSize().width * 0.5, this.visibleSize.height * 0.5 );
        this.rightArrow.setTag( RIGHT_ARROW );

        var menu = new cc.Menu();
        menu.addChild( this.leftArrow );
        menu.addChild( this.rightArrow );
        menu.setPosition( cc.p( 0, 0 ) );
        this.addChild( menu, 2 );
    },
    extractVectorFromSring : function( arrayString )
    {
        if (arrayString && arrayString.length > 0) {

            var numString = "";
            var returningVec = [];

            var i = 0;

            //int a = 0;

            while (1) {
                if (i >= arrayString.length || arrayString[i] == ',') {
                    if (numString.length) {
                        var id = numString;
                        returningVec.push(id);
                    }
                    numString = "";
                    //a++;
                } else {
                    numString = numString + arrayString[i];
                }

                if (i >= arrayString.length) {
                    break;
                }
                i++;
            }

            return returningVec;
        }else {
            return [];
        }
    },
    startLoadingResources:function(){
        var data = ServerData.getInstance();
        var currentOfferCode = 1;
        var buyerOfferCode = 2;
        var insufficentCode =1;
        switch ( data.server_buy_page )
        {
            case 2:
                if ( this.def.getValueForKey( IS_BUYER, false ) ) {
                    currentOfferCode = BuyPageTags.SPECIAL_OFFER_1;
                    buyerOfferCode = BuyPageTags.BUY_CREDITS;
                    insufficentCode = BuyPageTags.INSUFFICIENT_CREDITS_1;

                }
                else
                {
                    currentOfferCode = BuyPageTags.SPECIAL_OFFER_2;
                    buyerOfferCode = BuyPageTags.FIRST_TIME_CREDITS
                    insufficentCode = BuyPageTags.INSUFFICIENT_CREDITS_2;
                    DPUtils.getInstance().ImageLoadr( "res/lobby/buyPage_1_19/purchasetext.png" );

                }
                break;

            case 3:
                currentOfferCode = BuyPageTags.SPECIAL_OFFER_2;
                buyerOfferCode = BuyPageTags.DOUBLE_CREDITS;
                insufficentCode = BuyPageTags.INSUFFICIENT_CREDITS_2;
                break;

            case 4:
                currentOfferCode = BuyPageTags.SPECIAL_OFFER_3;
                buyerOfferCode = BuyPageTags.DOUBLE_CREDITS;
                insufficentCode = BuyPageTags.INSUFFICIENT_CREDITS_3;
                break;

            default:
                currentOfferCode = BuyPageTags.SPECIAL_OFFER_1;
                buyerOfferCode = BuyPageTags.OFFER_CREDITS;
                insufficentCode = BuyPageTags.INSUFFICIENT_CREDITS_1;
                break;
        }
        DPUtils.getInstance().ImageLoadr( "res/lobby/buyPage_1_19/specialofferpopup/" + ( currentOfferCode - BuyPageTags.SPECIAL_OFFER_1 + 1 ) + ".png" );
        DPUtils.getInstance().ImageLoadr( "res/lobby/buyPage_1_19/bg_" + ( buyerOfferCode - BuyPageTags.INSUFFICIENT_CREDITS_1 ) + ".png" );
        DPUtils.getInstance().ImageLoadr( "res/lobby/buyPage_1_19/outofcreditpopup/bg_" + ( insufficentCode - BuyPageTags.INSUFFICIENT_CREDITS_1+1 ) + ".png" );
        DPUtils.getInstance().ImageLoadr( "res/megaWin/mega_win_coins_plate/mega_win_coins_plate.png" );

        for (var i=0;i<=6;i++){
            DPUtils.getInstance().ImageLoadr( "res/lobby/buyPage_1_19/" + i + ".png" );
        }
        DPUtils.getInstance().ImageLoadr( "res/lobby/buyPage_1_19/close_btn.png" );

    },
    addPlists : function ( ){
        var spriteFrameCache = cc.spriteFrameCache;
        spriteFrameCache.removeSpriteFrames();
        spriteFrameCache.addSpriteFrames( res.LobbyPlist_Lobby_plist, res.LobbyPlist_Lobby_png );
        spriteFrameCache.addSpriteFrames( res.lobbyIcons_plist, res.lobbyIcons_png );
        spriteFrameCache.addSpriteFrames( res.FBPopUp_Lobby_plist, res.FBPopUp_Lobby_png );
        spriteFrameCache.addSpriteFrames( res.Settings_Lobby_plist, res.Settings_Lobby_png );
        //spriteFrameCache.addSpriteFrames( res.DailyChallengePlist_Lobby_plist, res.DailyChallengePlist_Lobby_png );
        spriteFrameCache.addSpriteFrames( res.Tournament_Lobby_plist, res.Tournament_Lobby_png );
        spriteFrameCache.addSpriteFrames( res.Daily_Wheel_Pop_plist, res.Daily_Wheel_Pop_png );
        spriteFrameCache.addSpriteFrames( res.Gift_Pop_plist, res.Gift_Pop_png );
    },
    setLikePage : function()
    {
        this.scrollView.setEnabled( false );
        this.showingScreen = true;
        var layer = new cc.Layer();//Color( cc.color( 0, 0, 0, 160 ) );
        layer.setTag( PopUpTags.STAY_IN_TOUCH_LIKE_PAGE );
        DPUtils.getInstance().setTouchSwallowing( this, layer );
        this.addChild( layer, 10 );
        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        layer.addChild( darkBG );

        var buySprite = new cc.Sprite( res.FirstTimeBuyPage );
        buySprite.setPosition( cc.p( this.visibleSize.width / 2, this.visibleSize.height / 2 ) );
        layer.addChild( buySprite );

        showLikeUsBtn( true );
        //showFacebookLikeBtn( true );

        var closeItem = new cc.MenuItemImage( res.Close_Buttpn_png, res.Close_Buttpn_png, this.menuRemoveCB, this );

        //closeItem.setPosition( cc.p( this.visibleSize.width * 0.86, this.visibleSize.height * 0.77 ) );

        closeItem.setPosition( cc.p( buySprite.getContentSize().width - closeItem.getContentSize().width / 2, buySprite.getContentSize().height - closeItem.getContentSize().height / 2 ));
        closeItem.setTag( LIKE_CLOSE_BTN_TAG );
        //closeItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );

        var menu = new cc.Menu();
        menu.addChild( closeItem );
        menu.setPosition( cc.p( 0, 0 ) );
        buySprite.addChild( menu, 5 );
        buySprite.setTag(easyMoveInTag);
        DPUtils.getInstance().easyBackOutAnimToPopup(buySprite,0.5);
        //this.resizer.setFinalScaling( layer );
    },
    setDealOffer:function(){
        var deal = AppDelegate.getInstance().getDealInstance();//DJSDeal.getInstance();
        DPUtils.getInstance().setTouchSwallowing( this, deal );
        this.addChild( deal, 5 );
        this.delegate.dealFlushed = true;
        this.showingScreen = true;
    },
    isFirstLaunch : false,
    isDealLaunch:false,
    setSomeScreenOnLobby : function()
    {
        AppDelegate.getInstance().showingScreen = true;
        ServerData.getInstance();
        InstantDealModel.getInstance().showInstantDeal();

        if( CSUserdefauts.getInstance().getValueForKey( "isFirstLaunch", true ) )//change 18 May
        {
            PopUps.getInstance().addPopUpIndex(PopUpTags.STAY_IN_TOUCH_LIKE_PAGE);
            CSUserdefauts.getInstance().setValueForKey( "isFirstLaunch", false );
            this.isFirstLaunch = true;
        }
        if ( this.def.getValueForKey( AREARS, 0 ) ){
            PopUps.getInstance().addPopUpIndex(PopUpTags.STAY_TOUCH_AREARS_POP);

        }
        if( ServerData.getInstance().offer_id && ServerData.getInstance().offer_id.length  ){
            PopUps.getInstance().addPopUpIndex(PopUpTags.STAY_TOUCH_ONE_LINK_POP);

        }else if (ServerData.getInstance().offer_message && ServerData.getInstance().offer_message.length>0){
            PopUps.getInstance().addPopUpIndex(PopUpTags.STAY_TOUCH_ONE_LINK_POP);
        }
        if ( this.acceptedFriendsIds.length && !this.delegate.invitation_rewards_assigned )
        {
            PopUps.getInstance().addPopUpIndex(PopUpTags.STAY_IN_TOUCH_ACCEPT_REWARDS);

        }
        if ( BonusTimer.getInstance()._haveDailyBonus )
        {
            PopUps.getInstance().addPopUpIndex(PopUpTags.STAY_TOUCH_DAILY_BONUS_POP);
        }
        var deal = AppDelegate.getInstance().getDealInstance();//DJSDeal.getInstance();

        if ( deal.getDealStatus() && deal.getNumberOfRunningActions() == 0 && !this.delegate.appJustLaunched && !this.delegate.dealFlushed )
        {
            if ( deal.flushDeal( true ) )
            {
                this.isDealLaunch = true;
                PopUps.getInstance().addPopUpIndex(PopUpTags.STAY_TOUCH_DEAL_POP);

            }
        }
        if( this.delegate.showHappyHoursPop )
        {
            PopUps.getInstance().addPopUpIndex(PopUpTags.STAY_TOUCH_HAPPY_HOUR_POP);

        }
        if ( this.doShowSpecialOfferPop()){
            PopUps.getInstance().addPopUpIndex(PopUpTags.STAY_TOUCH_SPECIAL_OFFER_POP);

        }
        if( this.delegate.appJustLaunched )
        {
            if (FacebookObj){
                this.UserFacebookFriendsList( FacebookObj.appfriendList );
                this.handleGiftableFriendsList();
            }
            AppDelegate.getInstance().initiateTournament( 0 );

        }
        AppDelegate.getInstance().showingScreen = false;

        this.delegate.appJustLaunched = false;

    },
    checkPopUpValue:false,
    UserFacebookFriendsList : function( data )
    {
        var previousFbFriends = CSUserdefauts.getInstance().getValueForKey( ACCEPTED_REQUEST_IDS, "" );
        var previousRequestIds = CSUserdefauts.getInstance().getValueForKey( PREVIOUS_REQUEST_IDS, "" );

        if ( data && data.length ) {

            var dictionary = null;
            var array = null;


            dictionary = data;
            array = dictionary;//["data"];
//cc.log( "array.lencth = " + array.length );

            var elm;

            for (var i in array )
            {
                elm = array[i];

                var str = elm;

                if ( str["id"] )
                {
                    var id = str["id"];
                    if ( previousRequestIds && previousRequestIds.length && previousRequestIds.indexOf(id) != -1 )
                    {
                        if ( previousFbFriends.indexOf( id ) == -1 )
                        {
                            if( !this.findInVector( id ) )
                            {
                                this.acceptedFriendsIds.push( id );

                                id = str["name"];

                                this.acceptedFriendsNames.push( id );
                            }
                        }
                    }
                }
            }
        }
    },
    acceptedFriendsIds : new Array(),
    findInVector : function( id )
    {
        //cc.log( "in findInVector with id = " + id );
        var found = true;
        var i;
        for ( i = 0; i < this.acceptedFriendsIds.length; i++ )
        {
            //cc.log( "findInVector = this.acceptedFriendsIds[i] = " + this.acceptedFriendsIds[i] );
            if ( this.acceptedFriendsIds[i].indexOf( id ) != -1 )
            {
                break;
            }
        }

        if ( i >= this.acceptedFriendsIds.length )
        {
            found = false;
        }

        return found;
    },
    setArearsMessage : function( arears )
    {
        ServerData.getInstance().updateData("coins",userDataInfo.getInstance().totalChips + arears);
        this.showingScreen = true;



        var layer = new cc.Layer();//Color( cc.color( 0, 0, 0, 166 ) );
        layer.setTag( VIP_LAYER_TAG );
        layer.setPosition( cc.p( 0, 0 ) );
        DPUtils.getInstance().setTouchSwallowing( this, layer );
        this.addChild( layer, 10 );


        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( this.visibleSize.width * 0.5, this.visibleSize.height * 0.5 ) );
        layer.addChild( darkBG );



        var bgSprite = new cc.Sprite( res.Arears_Bg_png );
        bgSprite.setPosition( cc.p( this.visibleSize.width / 2, this.visibleSize.height / 2 ) );
        layer.addChild( bgSprite );

        var ypos = 0.39;
        if ( ServerData.getInstance().BROWSER_TYPE === OPERA   || ServerData.getInstance().BROWSER_TYPE === CHROME || ServerData.getInstance().BROWSER_TYPE === FIREFOX){
            ypos = 0.35;
        }

        var label = new cc.LabelTTF( DPUtils.getInstance().getNumString( arears ), "CarterOne", 40 );
        label.setPosition( cc.p( bgSprite.getContentSize().width * 0.5, bgSprite.getContentSize().height *ypos) );
        bgSprite.addChild( label );

        var submitItem = new cc.MenuItemImage( res.Thanks_png, res.Thanks_png, this.menuRemoveCB, this );

        submitItem.setPosition( cc.p( bgSprite.getContentSize().width * 0.5, submitItem.getContentSize().height * 0.4 ) );
        submitItem.setTag( CLOSE_BTN_AREAR_TAG );

        submitItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );

        var menu = new cc.Menu();
        menu.setPosition( cc.p( 0, 0 ) );
        menu.addChild( submitItem );
        bgSprite.addChild( menu );
        bgSprite.setTag(easyMoveInTag);
        DPUtils.getInstance().easyBackOutAnimToPopup(bgSprite,0.3);
    },

    handleGiftableFriendsList : function()
    {
        var json = UserDef.getInstance().getValueForKey( GIFTS_MAP, null);
        if( FacebookObj.appFriendListForGift && json )
        {
            /*if (json.length > 0)*/ {
            var jObj = json;//JSON.parse(json);

            for (var key in jObj) {
                if ( jObj.hasOwnProperty( key ) ) {
                    for( var i = 0; i < FacebookObj.appFriendListForGift.length; i++ )
                    {
                        //cc.log( "FacebookObj.appFriendListForGift[i].toString() = " + FacebookObj.appFriendListForGift[i].id.toString() );
                        if( jObj[key] + SECONDS_IN_A_DAY > ServerData.getInstance().current_time_seconds && FacebookObj.appFriendListForGift[i].id.toString() === key )
                        {
                            FacebookObj.appFriendListForGift.splice( i, 1 );
                            i--;
                        }
                    }
                    //UserDef.getInstance().setValueForKey(key, jObj[key]);
                    //console.log(key + " = " + jObj[key]);
                }
            }
        }

        }
    },
    setLevelBar : function()
    {
        this.delegate.setLevel( this.def.getValueForKey( LEVEL, 0 ), CSUserdefauts.getInstance().getValueForKey( THIS_LEVEL_BET, 0 ), this );
    },
    setOfferPopup : function()
    {

        this.scrollView.setEnabled( false );
        this.showingScreen = true;
        var layer = new cc.Layer();
        layer.setTag( PopUpTags.STAY_IN_TOUCH_OFFER_PAGE );
        DPUtils.getInstance().setTouchSwallowing( this, layer );
        this.addChild( layer, 10 );
        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        layer.addChild( darkBG );
        var buySprite = null;
        var closeItem = null;

        if (ServerData.getInstance().offer_coins>0){

            buySprite = new cc.Sprite( res.OfferSuccessPage );

            {
                var timerLabel = new CustomLabel( DPUtils.getInstance().getNumString( ServerData.getInstance().offer_coins ), "arial_black", 40, new cc.size( 220, 40 ) );
                timerLabel.setPosition( cc.p( buySprite.getContentSize().width * 0.5, buySprite.getContentSize().height * 0.4 ) );
                buySprite.addChild( timerLabel );
            }

            closeItem = new cc.MenuItemImage( res.Collect_Btn_png, res.Collect_Btn_png, this.offerMenuCB, this );
            closeItem.setTag( CLOSE_BTN_TAG );
        }else{
            buySprite = new cc.Sprite( res.OfferInvalidPage );
            closeItem = new cc.MenuItemImage( res.levelUpOk_png, res.levelUpOk_png, this.offerMenuCB, this );
            var timerLabel = new CustomLabel( ServerData.getInstance().offer_message, "arial_black", 90, new cc.size( 350, 90 ) );
            timerLabel.setPosition( cc.p( buySprite.getContentSize().width * 0.5, buySprite.getContentSize().height * 0.7 ) );
            buySprite.addChild( timerLabel );
            this.delegate.offerData = null;
        }

        buySprite.setPosition( cc.p( this.visibleSize.width / 2, this.visibleSize.height / 2 ) );
        layer.addChild( buySprite );
        var offerMenu = new cc.Menu();
        offerMenu.setPosition( cc.p( 0, 0 ) );
        layer.addChild( offerMenu );
        closeItem.setPosition( cc.p( cc.rectGetMidX( buySprite.getBoundingBox() ), cc.rectGetMinY( buySprite.getBoundingBox() ) + closeItem.getContentSize().height * 0.75 ) );
        closeItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );
        var menu = new cc.Menu();
        menu.addChild( closeItem );
        menu.setPosition( cc.p( 0, 0 ) );
        layer.addChild( menu, 5 );
        //this.resizer.setFinalScaling( layer );

    },
    offerMenuCB : function( pSender )
    {
        //if ( !this.getParent().getChildByTag( LOADER_TAG ) )
        {
            var tag = pSender.getTag();

            switch (tag)
            {
                case CLOSE_BTN_TAG:
                    //cc.log( "Do something here" );
                    this.callOfferCollection();
                    break;
                default:
                    PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_TOUCH_ONE_LINK_POP);
                    break;
            }
            ServerData.getInstance().offer_message="";
            this.removeChildByTag( PopUpTags.STAY_IN_TOUCH_OFFER_PAGE );
            this.scrollView.setEnabled( true );
            this.showingScreen = false;
            if (this.dNode){
                this.dNode.clear();
            }
            this.delegate.jukeBox( S_BTN_CLICK );
        }
    },
    setLoader : function()
    {
        var loadingScr = new cc.Layer();
        loadingScr.setTag( LOADER_TAG )
        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 200 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        loadingScr.addChild( darkBG );
        cc.director.getRunningScene().addChild( loadingScr );
        var loadingSprite = new cc.Sprite( "res/appLaoder.png" );
        loadingSprite.setPosition( cc.winSize.width * 0.5, cc.winSize.height * 0.5 );
        loadingScr.addChild( loadingSprite );
        loadingSprite.runAction( new cc.RepeatForever( new cc.RotateBy( 1.5, 360 ) ) );
        DPUtils.getInstance().setTouchSwallowing( null, darkBG );
    },
    callOfferCollection : function()
    {
        this.setLoader();
        ServerData.getInstance().hitServerWithCodeAndCallBack(this.OfferCallBackFromServer,ServerData.getInstance().offer_id, ACCEPTENCE)
    },
    OfferCallBackFromServer:function(valueMap) {
        if (valueMap && valueMap["result"] == 1) {
            if ( cc.director.getRunningScene().getChildByTag(LOADER_TAG))
            {
                cc.director.getRunningScene().getChildByTag(LOADER_TAG).removeFromParent(true);
            }
            if (ServerData.getInstance().offer_coins>0) {

                AppDelegate.getInstance().assignRewards( ServerData.getInstance().offer_coins,true );
                ServerData.getInstance().offer_id = "";

            }
        }
        AppDelegate.getInstance().showingScreen = false;

        PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_TOUCH_ONE_LINK_POP);

    },
    setScoreLabel : function()
    {
        var labl = this.getChildByTag( SCORE_LABEL_TAG );

        var string = DPUtils.getInstance().getNumString( this.displayedScore );

        if( labl )
        {
            labl.setString( string );
            var lay = this.getChildByTag( VIP_LAYER_TAG );
            if (lay)
            {
                var tmpLabel = lay.getChildByTag( VIP_LAYER_TAG );
                if ( tmpLabel )
                {
                    if( ServerData.getInstance().server_buy_page == 3 )
                    {
                        tmpLabel = tmpLabel.getChildByTag( VIP_LAYER_TAG );
                    }
                    tmpLabel.setString( string );

                    var lbl = lay.getChildByTag( 8487 );
                    if( lbl )
                    {
                        lbl.setString( this.delegate.getVipString() );
                    }
                }
            }
        }
        else
        {
            labl = new CustomLabel( string, "TT0504M_", 30, new cc.size( 215, 30 ) );
            labl.setColor( cc.color( 255, 255, 255 ) );
            var ypos = 0.958;
            if (  ServerData.getInstance().BROWSER_TYPE === OPERA || ServerData.getInstance().BROWSER_TYPE === CHROME || ServerData.getInstance().BROWSER_TYPE === FIREFOX){
                ypos = 0.95;
            }
            labl.setPosition( cc.p( 275 , this.visibleSize.height * ypos ) );
            labl.setTag( SCORE_LABEL_TAG );
            this.addChild( labl, 1 );
            this.coinSpriteSmall = new cc.Sprite( res.Coin_Small_png );
            this.coinSpriteSmall.setPosition( cc.p( 150 , /*this.visibleSize.height - 22.5 * 1.2*/labl.getPositionY() ) );
            this.addChild( this.coinSpriteSmall, 1 );
        }
    },
    setFBInvitationAcceptionRewardsPopUp : function()
    {
        var dNode = new cc.DrawNode();
        dNode.setPosition( cc.p( 0, 0 ) );

        var cColor = cc.color( 0, 0, 0, 0.8 );
        dNode.drawRect( cc.p( 0, 0 ), cc.p( this.visibleSize.width, this.visibleSize.height ), cColor, this.visibleSize.height, cColor );
        DPUtils.getInstance().setTouchSwallowing( 100, dNode );

        var bgSprite = new cc.Sprite( "res/lobby/inviteAcceptPopUp.png" );
        bgSprite.setPosition( this.visibleSize.width * 0.5, this.visibleSize.height * 0.5 );
        dNode.addChild( bgSprite );


        var scroll = new ccui.ScrollView();

        var scollFrameSize = cc.size( bgSprite.getContentSize().width, bgSprite.getContentSize().height * 0.4 );

        scroll.setContentSize( scollFrameSize );
        scroll.setBounceEnabled( true );
        scroll.setPosition( cc.p( 0, bgSprite.getContentSize().height * 0.2 ) );
        scroll.setDirection( ccui.ScrollView.DIR_VERTICAL );

        scroll.setScrollBarAutoHideEnabled( false )
        scroll.setScrollBarOpacity(255);
        scroll.setScrollBarColor( cc.color( 255, 255, 255, 255 ) );

        bgSprite.addChild( scroll );

        var height = ( this.acceptedFriendsNames.length + 1.5 ) * 30 * 1.2;

        if( height < scollFrameSize.height )
        {
            height = scollFrameSize.height;
        }
        scroll.setInnerContainerSize( cc.size( scollFrameSize.width, height ) );

        var pos = height - 30 * 1.2;

        for( var i = 0; i < this.acceptedFriendsNames.length ; i++ )
        {
            var sprite = new cc.LabelTTF( this.acceptedFriendsNames[i], "HELVETICALTSTD-BOLD", 30 );

            sprite.setPosition( cc.p( bgSprite.getContentSize().width / 2, pos ) );

            pos-=sprite.getContentSize().height * 1.2;

            scroll.addChild( sprite );
        }

        var totalCoins = new cc.LabelTTF( " " + this.acceptedFriendsNames.length * FB_INVITATION_REWARDS, "HELVETICALTSTD-BOLD", 50 );

        totalCoins.setPosition( cc.p( bgSprite.getContentSize().width * 0.3, bgSprite.getContentSize().height * 0.15 ) );
        bgSprite.addChild( totalCoins );

        var closeItem = new cc.MenuItemImage( "res/collect_btn.png", "res/collect_btn.png", this.menuCB, this );

        closeItem.setPosition( cc.p( bgSprite.getContentSize().width * 0.75, totalCoins.getPositionY() ) );
        closeItem.setTag( INVITE_COLLECT_BTN_TAG );

        closeItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );

        var menu = new cc.Menu();
        menu.addChild( closeItem );
        menu.setPosition( cc.p( 0, 0 ) );
        bgSprite.addChild( menu );
        DPUtils.getInstance().setTouchSwallowing( 100, bgSprite );

        this.resizer.setFinalScaling( bgSprite );

        this.addChild( dNode, 2 );
    },
    checkAndHandlePaidBonusWheel : function()
    {
        var grandWheel = this.getChildByTag( PAID_WHEEL );
        if( grandWheel )grandWheel.startPaidRolling();
    },
    getPayWheelCoins : function() {
        var grandWheel = this.getChildByTag( PAID_WHEEL );

        if( grandWheel )return grandWheel.payWheelCoins;

        return 0;//If this statement is being executed, Check this
    }


});

var HelloWorldScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new HelloWorldLayer();
        this.addChild(layer);
    }
});

