/**
 * Created by RNF-Mac11 on 1/20/17.
 */

SpecialOffers = cc.Layer.extend({
    bgSprite:null,
    ctor:function ( code ) {
        this._super();
        //var pRet = new CustomButton();
        //if ( pRet && pRet.init() ){

        this.initializeComponent( code );
        return true;
        //}


    },
    offerCode : 0,
    initializeComponent:function ( code ) {

        this.offerCode = code;
        this.setOffer();
    },
    setOffer : function()
    {
        var visibleSize = cc.winSize;
        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        this.addChild( darkBG );
        var str = "res/lobby/buyPage_1_19/specialofferpopup/" + ( this.offerCode - BuyPageTags.SPECIAL_OFFER_1 + 1 ) + ".png";

        this.bgSprite = new cc.Sprite( str );
        this.bgSprite.setPosition( cc.p( this.getContentSize().width / 2, this.getContentSize().height / 2 ) );
        this.addChild(  this.bgSprite );

        var closeItem;

        if ( this.offerCode === BuyPageTags.SPECIAL_OFFER_3 )
        {
            var tempTimerLabel = new cc.LabelTTF( "00:00:00", "CarterOne", 20.5 );
            this.timerLabel = new cc.LabelTTF( "00:00:00", "CarterOne", 20.5, tempTimerLabel.getContentSize() );
            this.timerLabel.setPosition( cc.p(  this.bgSprite.getContentSize().width - this.timerLabel.getContentSize().width * 1.6,  this.bgSprite.getContentSize().height * 0.71 ) );
            this.bgSprite.addChild( this.timerLabel );
            this.schedule( this.updator );
        }
        closeItem = new cc.MenuItemImage( res.Special_Offer_Btn_png, res.Special_Offer_Btn_png, this.menuCB, this );
        closeItem.setPosition( cc.p(  this.bgSprite.getContentSize().width - closeItem.getContentSize().width ,  this.bgSprite.getContentSize().height- closeItem.getContentSize().height  ) );
        closeItem.setTag( CLOSE_BTN_TAG );
        closeItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );
        var menu = new cc.Menu();
        menu.addChild( closeItem );
        menu.setPosition( cc.p( 0, 0 ) );
        this.bgSprite.addChild( menu );
        var priceTags = [ purchaseTags.PURCHASE_99_99, purchaseTags.PURCHASE_49_99, purchaseTags.PURCHASE_19_99 ];
        var xPos = 0;
        var yPos =  this.bgSprite.getContentSize().height*0.2;
        var j=6;
        for ( var i = 0; i < 3; i++ )
        {
            var buyBtn;
            var sttr = "res/lobby/buyPage_1_19/" + j + ".png";
            buyBtn = new cc.MenuItemImage( sttr, sttr, this.menuCB, this );
            if (i==0) {
                xPos =buyBtn.getContentSize().width*1.85;
            }else if (i==1) {
                xPos = this.bgSprite.getContentSize().width*0.5;
            }else if (i==2) {
                xPos = this.bgSprite.getContentSize().width - buyBtn.getContentSize().width*1.85;
            }
            buyBtn.setPosition(cc.p(xPos,yPos) );
            buyBtn.setTag( priceTags[i] );
            menu.addChild( buyBtn );
            j--;
        }

       /* if ( cc.director.getRunningScene().getTag() === sceneTags.GAME_SCENE_TAG )
        {
            bgSprite.setPositionY( this.getContentSize().height * 0.55 );
            bgSprite.setScale( 0.9 );
        }
*/
       DPUtils.getInstance().easyBackOutAnimToPopup( this.bgSprite,0.5);
    },
    
    menuCB : function( itm )
    {
        if ( cc.director.getRunningScene().getChildByTag(LOADER_TAG))
            return;

        var scene = null;
        var layer = null;
        var game = null;

        var delegate = AppDelegate.getInstance();

        switch ( itm.getTag() )
        {
            case CLOSE_BTN_TAG: {

                DPUtils.getInstance().easyBackInAnimToPopup(this.bgSprite,0.3);
                this.runAction(new cc.Sequence( new cc.DelayTime(0.3),cc.callFunc( function ( ){
                    this.removeFromParent(true);
                    scene = cc.Director._getInstance().getRunningScene();
                    layer = scene;//.getChildByTag( sceneTags.GAME_SCENE_TAG );
                    if (layer.getTag() === sceneTags.GAME_SCENE_TAG) {
                        game = layer;
                        //game.canShowAdv = true;
                    } else {
                        layer = scene.getChildByTag(sceneTags.LOBBY_SCENE_TAG);
                        var lobby = layer;
                        lobby.showingScreen = false;
                    }
                    delegate.showingScreen = false;

                    PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_TOUCH_SPECIAL_OFFER_POP);
                },this)));









            }
                break;

            default:
                //if ( !this.getParent().getParent().getChildByTag( LOADER_TAG ) )
                {
                    IAPHelper.getInstance().purchase( itm.getTag(), this.offerCode );
                    delegate.showingScreen = false;

                }
                break;
        }
        delegate.jukeBox( S_BTN_CLICK );
    },
    
    updator :function ( dt )
    {
      //  var t = ServerData.getInstance().current_time_seconds;
        if ( this.timerLabel) {

            var now = new Date();//ServerData.getInstance().currentTime;
            var days_passed = now.getDay() - 5;
            var min = 59 - now.getMinutes();
            var sec = 59 - now.getSeconds();
            var hour = 0;
            if (days_passed === 0)
                hour = now.getHours() - 18;

            else if (days_passed === 1)
                hour = 24 + now.getHours() - 18;

            else if (days_passed === -5)
                hour = 48 + now.getHours() - 18;

            else if (days_passed === -4)
                hour = 72 + now.getHours() - 18;


            hour = 77 - hour;


            var string = hour < 10 ? "0" + hour : "" + hour;

            min < 10 ? string = string + ":0" + min : string = string + ":" + min;

            sec < 10 ? string = string + ":0" + sec : string = string + ":" + sec;

            this.timerLabel.setString(string);
        }
    }


});