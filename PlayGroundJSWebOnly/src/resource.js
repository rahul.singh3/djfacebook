var res = {

    //Font Files
    Arial_TTF : "res/fonts/arial.ttf",
    Arial_black_TTF : "res/fonts/arial_black.ttf",
    Arimo_TTF : "res/fonts/Arimo-Bold.ttf",
    Carter_TTF : "res/fonts/CarterOne.ttf",
    GORGIA_TTF : "res/fonts/GEORGIAB.TTF",
    //HELVETICALTSTD_BOLD_TTF : "res/fonts/HELVETICALTSTD-BOLD.OTF",
    //MyriadPro_TTF : "res/fonts/MyriadPro-Regular.otf",
    DS_DIGIT_TTF : "res/fonts/DS-DIGIT.TTF",
    BM_FONT_1 : "res/fonts/font_new2-export.fnt",
    BM_FONT_2 : "res/fonts/font2-export.fnt",
    TCB_FONT  : "res/fonts/TT0504M_.TTF",

//Font Files
    Tournament_Lobby_png : "res/tournament/Tournament.png",
    Tournament_Lobby_plist : "res/tournament/Tournament.plist",
    Daily_Wheel_Pop_png : "res/lobby/daily_spin_wheel/Plist.png",
    Daily_Wheel_Pop_plist : "res/lobby/daily_spin_wheel/Plist.plist",
    tournament_img : "res/tournament/cst_loading_person.png",
    tournament_img1 : "res/tournament/cst_person_male.png",
    tournament_img2 : "res/tournament/cst_purpleShine.png",
    tournament_img3 : "res/tournament/cst_yellowShine.png",

    Gift_Pop_png : "res/giftPop/giftPop.png",
    Gift_Pop_plist : "res/giftPop/giftPop.plist",

    AppLoader_png : "res/appLaoder.png",
    Arears_Bg_png : "res/arears_bg.png",
    Thanks_png : "res/thanks_btn.png",
    Splash_png : "res/splash_1_19.png",
    Snow_png : "res/snow.png",
    ServerMaintenenceBg:"res/serverMaintenenceBg.png",
    Background_Lobby_png : "res/lobby/background_1_18.png",
    Background_lobby_christmas:"res/lobby/background_christmas.png",
    Bar_Base_Lobby_png : "res/lobby/bar_base.png",
    Bar_Fill_Lobby_png : "res/lobby/bar_fill.png",
    Bar_Slit_Lobby_png : "res/lobby/bar_slit.png",
    Coin_Small_png : "res/lobby/coinSmall.png",
    Bar_Transperant_Lobby_png : "res/lobby/bar_transperant.png",
    Top_Bar_Lobby_png : "res/top_bar_1_18.png",
    Top_Bar_Back_png : "res/lobby/bg_1_18.png",
    Bar_Frame_Lobby_png : "res/lobby/sidebar/bar_frame.png",
    Coin_png : "res/coin_1_18.png",
    Grand_win_text : "res/GameButtons/grand_win_text.png",
    Mega_win_text : "res/GameButtons/mega_win_text.png",
    Big_win_text : "res/GameButtons/big_win_text.png",
    Flare : "res/GameButtons/flare.png",
    Bullet : "res/GameButtons/bullet.png",
    Particle_Plist : "res/GameButtons/particle_texture.plist",
    Setting_png : "res/GameButtons/setting_1_18.png",
    Timer_Base_png : "res/lobby/timer_base.png",
    Lobby_Lock_png : "res/lobby/lock.png",
    Machine_unavailable_png : "res/lobby/machineUnavailable.png",
    Close_Buttpn_png : "res/lobby/buyPage_1_19/close_btn.png",
    Close_Button_2_png : "res/close_btn.png",
    Daily_spin_wheel_bg_png : "res/lobby/daily_spin_wheel/bg.png",
    Daily_spin_wheel_png : "res/lobby/daily_spin_wheel/wheel_1_18.png",
    Wheel_Light_png : "res/lobby/daily_spin_wheel/light.png",
    Daily_spin_wheel_stopper_png : "res/lobby/daily_spin_wheel/stopper.png",
    Daily_spin_wheel_Btn_png : "res/lobby/daily_spin_wheel/btn.png",
    Daily_spin_wheel_Pop_png : "res/lobby/daily_spin_wheel/bonus_popup.png",
    Happy_Hour_Pop_png : "res/happyHourPopBack.png",
    Level_Bar_Fill_png : "res/fill_1_18.png",
    Level_Diamond_png : "res/lobby/level_diamond_1_18.png",
    Collect_Btn_png : "res/collect_btn.png",
    Invite_success_popUp : "res/lobby/inviteAcceptPopUp.png",
    Full_screen_button_png : "res/full_screen_button_1_18.png",
    Normal_screen_button_png : "res/exit_full_screen_button_1_18.png",

    //Deal Def BG
    Deal_Default_Bg_Buyer_png : "res/deal_default_bg_buyer_1_19.png",
    Deal_Default_Bg_NonBuyer_png : "res/deal_default_bg_nonBuyer_1_19.png",
    deal_buy_btn_png : "res/deal_buy_btn_1_19.png",
    //Special Offers
    /*Special_Offer_Bg1_png : "res/specialofferpopup/1.png",
    Special_Offer_Bg2_png : "res/specialofferpopup/2.png",
    Special_Offer_Bg3_png : "res/specialofferpopup/3.png",
    Special_Offer_Btn1_png : "res/specialofferpopup/btn_1.png",
    Special_Offer_Btn2_png : "res/specialofferpopup/btn_2.png",
    Special_Offer_Btn3_png : "res/specialofferpopup/btn_3.png",
        */
    Special_Offer_Btn_png : "res/lobby/buyPage_1_19/close_btn.png",
    snow_layer_bar_png :"res/lobby/snow_layer.png",
    //Special Offers
    // EXtreme_reel_bg :"res/DJSXTremeMachine/reel_bg.png",
    //EXtreme_reel_fg :"res/DJSXTremeMachine/reel_fg.png",

    //Most Popular & Best buy tag
    //Most_popular_png : "res/doubleBonanza/most_popular_tag.png",
    // Best_Value_png : "res/doubleBonanza/best_value_tag.png",
    //Most Popular & Best buy tag

    //VIP Layer
    Lobby_Vip_png : "res/vip/vip.png",
    Lobby_Vip_0_png : "res/vip/0.png",
    Lobby_Vip_1_png : "res/vip/1.png",
    Lobby_Vip_2_png : "res/vip/2.png",
    Lobby_Vip_3_png : "res/vip/3.png",
    Lobby_Vip_4_png : "res/vip/4.png",
    Lobby_Vip_5_png : "res/vip/5.png",
    Lobby_Vip_6_png : "res/vip/6.png",
    //VIP Layer

    //Gaf_lobby
    /*Test_gaf : "res/test/test.gaf",
     Test_png : "res/test/test.png",*/
    Collect_gaf : "res/lobby/collect/collect.gaf",
    Collect_png : "res/lobby/collect/collect.png",
    Buy_Button_gaf : "res/lobby/buy_button_1_18/buy_button_1_18.gaf",
    Buy_Button_png : "res/lobby/buy_button_1_18/buy_button_1_18.png",
    Buy_Button_Half_gaf : "res/lobby/buy_button_half_1_18/buy_button_half_1_18.gaf",
    Buy_Button_Half_png : "res/lobby/buy_button_half_1_18/buy_button_half_1_18.png",
    Deal_Button_Half_gaf : "res/lobby/deal_button_half_1_18/deal_button_half_1_18.gaf",
    Deal_Button_Half_png : "res/lobby/deal_button_half_1_18/deal_button_half_1_18.png",

    //Gaf_lobby

    //lobby Plists start
    lobbyIcons_png : "res/lobby/lobbyIcons_1_18.png",
    lobbyIcons_plist : "res/lobby/lobbyIcons_1_18.plist",
    LobbyPlist_Lobby_png : "res/lobby/LobbyPlist_1_18.png",
    LobbyPlist_Lobby_plist : "res/lobby/LobbyPlist_1_18.plist",
    FBPopUp_Lobby_png : "res/lobby/FBPopUp.png",
    FBPopUp_Lobby_plist : "res/lobby/FBPopUp.plist",
    Settings_Lobby_png : "res/setting_popup/Settings.png",
    Settings_Lobby_plist : "res/setting_popup/Settings.plist",

    purchasedCreditsPop_png : "res/purchasedCreditsPop.png",
    levelUpOk_png : "res/levelUpOk.png",

    //lobby Plists end

    daily_challenges_png : "res/daily_challenges.png",

    empty_element_png : "res/empty.png",
    glow_white_png : "res/glow_white.png",
    glow_png : "res/glow.png",


    //Game Resource
    GlowIconPng : "res/glow_icon.png",
    mega_win_coins_plate_gaf : "res/megaWin/mega_win_coins_plate/mega_win_coins_plate.gaf",
    mega_win_coins_plate_png : "res/megaWin/mega_win_coins_plate/mega_win_coins_plate.png",
    mega_win_text_gaf : "res/megaWin/mega_win_text/mega_win_text.gaf",
    mega_win_text_png : "res/megaWin/mega_win_text/mega_win_text.png",
    FireSnow_png : "res/snow.png",
    FireStar_png : "res/star.png",
    FireStar1_png : "res/star1.png",

    //insufficientCoins popup
    LevelUpSlider : "res/levelUp.png",
    //insufficientCoins popup

    //Info Scene
    paytable_bg_png : "res/paytable_bg.png",
    Background_Lobby_html_png : "res/Background_Lobby_html.png",
    //Info Scene

    //AppRater
    rating_submit_btn : "res/Bribe_pop/rate_us_button.png",
    rating_close_btn : "res/Bribe_pop/cross_button.png",
    rating_background : "res/Bribe_pop/feedback_bg.png",
    rating_emoji1 : "res/Bribe_pop/emoji/1.png",
    rating_emoji2 : "res/Bribe_pop/emoji/2.png",
    rating_emoji3 : "res/Bribe_pop/emoji/3.png",
    rating_emoji4 : "res/Bribe_pop/emoji/4.png",
    rating_emoji5 : "res/Bribe_pop/emoji/5.png",
    rating_emoji1_disable : "res/Bribe_pop/emoji/1_disabled.png",
    rating_emoji2_disable : "res/Bribe_pop/emoji/2_disabled.png",
    rating_emoji3_disable : "res/Bribe_pop/emoji/3_disabled.png",
    rating_emoji4_disable : "res/Bribe_pop/emoji/4_disabled.png",
    rating_emoji5_disable : "res/Bribe_pop/emoji/5_disabled.png",


    //AppRater

    arrow_btn_lobby : "res/arrow_btn.png",
    tournament_bg_png : "res/tournamentWinBg.png",
    share_bg_png : "res/share_bg.png",
    tick_png : "res/tick.png",
    shareButton_png : "res/share.png",

    //No Connection
    noConnPopBG : "res/noConnection/noConnectionBg.png",
    noConnServerPopBG : "res/noConnection/bg.png",
    noConnServerPopBG2 : "res/noConnection/serverNonResponsive.png",
    refreshBtn : "res/noConnection/retry_button.png",
    wifi_1 : "res/noConnection/wifi_1.png",
    wifi_2 : "res/noConnection/wifi_2.png",
    wifi_3 : "res/noConnection/wifi_3.png",
    wifi_4 : "res/noConnection/wifi_4.png",
    wifi_5 : "res/noConnection/wifi_5.png",
    //No Connection

    scrollBarPixel_png : "res/scrollBarPixel.png",

    Dark_BG_png : "res/reelBG.png",
    notMatch_png :"res/notMatch.png",
    FirstTimeBuyPage : "res/firstTimeLikePage_1_18.png",

    OfferSuccessPage : "res/offerSuccess.png",
    OfferInvalidPage : "res/offerFailure.png",
   // Offer_Error_2 : "res/offerError_2.png",
   // Offer_Error_3 : "res/offerError_3.png",
  //  Offer_Error_4 : "res/offerError_4.png",

    GrandWheelStatusPopup_2 : "res/GrandWheelStatusPopup_2.png",
    GrandWheelStatusPopup : "res/GrandWheelStatusPopup.png",

    GrandWheel_0 : "res/GrandWheel/0.png",
    GrandWheel_1 : "res/GrandWheel/1.png",
    GrandWheel_2 : "res/GrandWheel/2.png",
    GrandWheel_3 : "res/GrandWheel/3.png",
    GrandWheel_4 : "res/GrandWheel/4.png",
    GrandWheel_5 : "res/GrandWheel/5.png",

    GrandWheelBg : "res/GrandWheel/bg.png",
    GrandWheelButton : "res/GrandWheel/btn.png",
    GrandWheelBoundry : "res/GrandWheel/boundry.png",
    GrandWheelProgress_bar_bg : "res/GrandWheel/progress_bar_bg.png",
    GrandWheelProgress_bar_fill : "res/GrandWheel/progress_bar_fill.png",

    paidGrandWheel_1 : "res/paidGrandWheel/1.png",
    paidGrandWheel_2 : "res/paidGrandWheel/2.png",
    paidGrandWheel_3 : "res/paidGrandWheel/3.png",
    paidGrandWheel_4 : "res/paidGrandWheel/4.png",
    paidGrandWheel_boundry : "res/paidGrandWheel/boundry.png",
    paidGrandWheel_btn : "res/paidGrandWheel/btn.png",
    paidGrandWheel_paidWheelPaymentPopUp : "res/paidGrandWheel/paidWheelPaymentPopUp.png",
    paidGrandWheel_pay_button : "res/paidGrandWheel/pay_button.png",
    paidGrandWheel_replacement : "res/paidGrandWheel/replacement.png",
    paidGrandWheel_stopper : "res/paidGrandWheel/stopper.png",
    paidGrandWheel_wheel : "res/paidGrandWheel/wheel.png",
    giantWheelPayOutPopUp : "res/giantWheelPayOutPopUp.png",
    collectBonus : "res/collectBonus.png",
    grandWheelButton : "res/GameButtons/grandWheelButton.png",
    grandWheelButtonFiller : "res/GameButtons/grandWheelButtonFiller.png",
    hot_machine_theme :"res/hot.png",
    new_machine_theme :"res/new.png",
    last_played_theme :"res/last_played.png",
    favorite_enable: "res/cst_fav_enable.png",
    favorite_disable: "res/cst_fav_disable.png",
    buyPage_offer :"res/lobby/buyPage_1_19/bg_4.png",
    buyPage_offer1 :"res/lobby/buyPage_1_19/bg_3.png",
    buyPage_offer2 :"res/lobby/buyPage_1_19/bg_5.png",
    buyPage_offer3 :"res/lobby/buyPage_1_19/bg_6.png",

    buyPage_offer_buy1 :"res/lobby/buyPage_1_19/0.png",
    buyPage_offer_buy2 :"res/lobby/buyPage_1_19/1.png",
    buyPage_offer_buy3 :"res/lobby/buyPage_1_19/2.png",
    buyPage_offer_buy4 :"res/lobby/buyPage_1_19/3.png",
    buyPage_offer_buy5 :"res/lobby/buyPage_1_19/4.png",
    buyPage_offer_buy6 :"res/lobby/buyPage_1_19/5.png",
    buyPage_offer_close :"res/lobby/buyPage_1_19/close_btn.png",
    instant_deal_claim:"res/instant_deal/cst_instant_deal_accept_claim_button.png",
    instant_deal_blank:"res/instant_deal/cst_instant_deal_blank_button.png",
    instant_deal_close:"res/instant_deal/cst_instant_deal_close_button",
    instant_deal_coins:"res/instant_deal/cst_instant_deal_coins_button.png",
    instant_deal_reject:"res/instant_deal/cst_instant_deal_reject_button.png",
    dc_bg :"res/DailyChallences/dc_bg.png",
    dc_right_arrow :"res/DailyChallences/right_arrow.png",
    dc_task1 :"res/DailyChallences/task_1.png",
    dc_task2 :"res/DailyChallences/task_2.png",
    dc_task3 :"res/DailyChallences/task_3.png",
    dc_DcBlank :"res/DailyChallences/DcBlank.png",
    dc_dc_blank_mission :"res/DailyChallences/dc_blank_mission.png",
    dc_dc_result_bar :"res/DailyChallences/dc_result_bar.png",
    dc_dc_wheel :"res/DailyChallences/dc_wheel.png",
    dc_gift_box :"res/DailyChallences/gift_box.png",
    dc_lock_icon :"res/DailyChallences/lock_icon.png",
    dc_progress_bar :"res/DailyChallences/progress_bar.png",
    dc_progress_bg :"res/DailyChallences/progress_bg.png",
    dc_tick_mark :"res/DailyChallences/tick_mark.png",
    dc_win_number_display :"res/DailyChallences/win_number_display.png",
    dc_header_01 :"res/DailyChallences/01.png",
    dc_header_02 :"res/DailyChallences/02.png",
    dc_header_03 :"res/DailyChallences/03.png",
    dc_header_11:"res/DailyChallences/11.png",
    dc_header_12:"res/DailyChallences/12.png",
    dc_header_13:"res/DailyChallences/13.png",
    dc_gift_box_anim_png : "res/DailyChallences/gift_box_anim/gift_box_anim.png",
    dc_gift_box_anim_gaf : "res/DailyChallences/gift_box_anim/gift_box_anim.gaf",
    dc_lock_anim_png : "res/DailyChallences/lock_anim/lock_anim.png",
    dc_lock_anim_gaf : "res/DailyChallences/lock_anim/lock_anim.gaf",
    dc_gift_anim_png : "res/DailyChallences/dc_gift_anim/dc_gift_anim.png",
    dc_gift_anim_gaf : "res/DailyChallences/dc_gift_anim/dc_gift_anim.gaf",
    dc_bg_icon :"res/DailyChallences/dc_bg_icon.png",
    dc_fill_icon:"res/DailyChallences/dc_fill_icon.png",
    gift_box_disable:"res/DailyChallences/gift_box_disable.png",
    hourly2x_png :"res/2x.png",
    hourly_bonus_bg:"res/lobby/bottom_bar/hourlyBonus/Hourly_BonusBG.png",
    hourly_border_anim_png:"res/lobby/bottom_bar/hourlyBonus/hourly_bonus_border/hourly_bonus_border.png",
    hourly_border_anim:"res/lobby/bottom_bar/hourlyBonus/hourly_bonus_border/hourly_bonus_border.gaf",
    hourly_collect:"res/lobby/bottom_bar/hourlyBonus/collect_btn.png",
    hourly_gift:"res/lobby/bottom_bar/hourlyBonus/collect_tap_btn.png",
    hourly_box:"res/lobby/bottom_bar/hourlyBonus/box_close.png",
    hourly_getit:"res/lobby/bottom_bar/hourlyBonus/GetIt_btn.png",
    hourly_gettap:"res/lobby/bottom_bar/hourlyBonus/GetIt_tap_btn.png",
    hourly_boxanim_png:"res/lobby/bottom_bar/hourlyBonus/box_Anim/box_Anim.png",
    hourly_boxanim:"res/lobby/bottom_bar/hourlyBonus/box_Anim/box_Anim.gaf",
    hourly_nonbuyertext:"res/lobby/bottom_bar/hourlyBonus/Hourly_500.png",
    hourly_buyertext:"res/lobby/bottom_bar/hourlyBonus/Hourly_1000.png",
    hourly_coinsbgText_png:"res/lobby/bottom_bar/hourlyBonus/hourly_bonus_coins/hourly_bonus_coins.png",
    hourly_coinsbgText_gaf:"res/lobby/bottom_bar/hourlyBonus/hourly_bonus_coins/hourly_bonus_coins.gaf",
    hourly_bgText_png:"res/lobby/bottom_bar/hourlyBonus/hourly_bonus_textcollectbonus/hourly_bonus_textcollectbonus.png",
    hourly_bgText_gaf:"res/lobby/bottom_bar/hourlyBonus/hourly_bonus_textcollectbonus/hourly_bonus_textcollectbonus.gaf",

};
/*
var defBuyPageArray = {
    //Default Buy Page
    Def_Buy_Page_Bg_png : "res/lobby/buyPage/buypage.png",
    Def_Buy_Page_Bg_0_png : "res/lobby/buyPage/0.png",
    Def_Buy_Page_Bg_1_png : "res/lobby/buyPage/1.png",
    Def_Buy_Page_Bg_2_png : "res/lobby/buyPage/2.png",
    Def_Buy_Page_Bg_3_png : "res/lobby/buyPage/3.png",
    Def_Buy_Page_Bg_4_png : "res/lobby/buyPage/4.png",
    Def_Buy_Page_Bg_5_png : "res/lobby/buyPage/5.png"
    //Default Buy Page
};

var doubleBonanzaBUyPageArray = {
    //Double Bonanza Buy Page
    Double_Bonanza_Buy_Page_Bg_png : "res/doubleBonanza/bg_2.png",
    Double_Bonanza_Buy_Page_0_png : "res/doubleBonanza/0.png",
    Double_Bonanza_Buy_Page_1_png : "res/doubleBonanza/1.png",
    Double_Bonanza_Buy_Page_2_png : "res/doubleBonanza/2.png",
    Double_Bonanza_Buy_Page_3_png : "res/doubleBonanza/3.png",
    Double_Bonanza_Buy_Page_4_png : "res/doubleBonanza/4.png",
    Double_Bonanza_Buy_Page_5_png : "res/doubleBonanza/5.png"
    //Double Bonanza Buy Page
};

var firstTimeBuyPageArray = {
    //First Time Buy Page
    First_Time_Buy_Page_Bg_png : "res/firstTimeOffer/buypage.png",
    First_Time_Buy_Page_0_png : "res/firstTimeOffer/0.png",
    First_Time_Buy_Page_1_png : "res/firstTimeOffer/1.png",
    First_Time_Buy_Page_2_png : "res/firstTimeOffer/2.png",
    First_Time_Buy_Page_3_png : "res/firstTimeOffer/3.png",
    First_Time_Buy_Page_4_png : "res/firstTimeOffer/4.png",
    First_Time_Buy_Page_5_png : "res/firstTimeOffer/5.png"
    //First Time Buy Page
};

var limitedTimeBuyPageArray = {
    //Limited Time Offer
    Limited_Offer_Bg1_png : "res/SpecialOffer/0.png",
    Limited_Offer_Bg2_png : "res/SpecialOffer/1.png",
    Limited_Offer_Bg3_png : "res/SpecialOffer/2.png",
    Limited_Offer_Btn1_png : "res/SpecialOffer/3.png",
    Limited_Offer_Btn2_png : "res/SpecialOffer/4.png",
    Limited_Offer_Btn3_png : "res/SpecialOffer/5.png",
    Limited_Offer_Btn_png : "res/SpecialOffer/bg.png"
    //Limited TIme Offer
};
*/
var lobbyIconsResources = {//change 15 MAy
    Button_DJSBonusMachine_gaf : "res/lobby/DJSBonusMachine/DJSBonusMachine.gaf",
    //Button_DJSBonusMachine_png : "res/lobby/DJSBonusMachine/DJSBonusMachine.png",
    Button_DJSCrazyCherry_gaf : "res/lobby/DJSCrazyCherry/DJSCrazyCherry.gaf",
    //Button_DJSCrazyCherry_png : "res/lobby/DJSCrazyCherry/DJSCrazyCherry.png",
    Button_DJSDiamond_gaf : "res/lobby/DJSDiamond/DJSDiamond.gaf",
    //Button_DJSDiamond_png : "res/lobby/DJSDiamond/DJSDiamond.png",
    Button_DJSDiamond3X_gaf : "res/lobby/DJSDiamond3X/DJSDiamond3X.gaf",
    //Button_DJSDiamond3X_png : "res/lobby/DJSDiamond3X/DJSDiamond3X.png",
    Button_DJSDiamondsForever_gaf : "res/lobby/DJSDiamondsForever/DJSDiamondsForever.gaf",
    //Button_DJSDiamondsForever_png : "res/lobby/DJSDiamondsForever/DJSDiamondsForever.png",
    Button_DJSFire_gaf : "res/lobby/DJSFire/DJSFire.gaf",
    //Button_DJSFire_png : "res/lobby/DJSFire/DJSFire.png",
    Button_DJSFreeSpinner_gaf : "res/lobby/DJSFreeSpinner/DJSFreeSpinner.gaf",
    //Button_DJSFreeSpinner_png : "res/lobby/DJSFreeSpinner/DJSFreeSpinner.png",
    Button_DJSFreeSpins_gaf : "res/lobby/DJSFreeSpins/DJSFreeSpins.gaf",
    //Button_DJSFreeSpins_png : "res/lobby/DJSFreeSpins/DJSFreeSpins.png",
    Button_DJSFreeze_gaf : "res/lobby/DJSFreeze/DJSFreeze.gaf",
    //Button_DJSFreeze_png : "res/lobby/DJSFreeze/DJSFreeze.png",
    Button_DJSLightningRewind_gaf : "res/lobby/DJSLightningRewind/DJSLightningRewind.gaf",
    //Button_DJSLightningRewind_png : "res/lobby/DJSLightningRewind/DJSLightningRewind.png",
    Button_DJSLightningRewindTwik_gaf : "res/lobby/DJSLightningRewindTwik/DJSLightningRewindTwik.gaf",
    //Button_DJSLightningRewindTwik_png : "res/lobby/DJSLightningRewindTwik/DJSLightningRewindTwik.png",
    Button_DJSQuintuple5x_gaf : "res/lobby/DJSQuintuple5x/DJSQuintuple5x.gaf",
    //Button_DJSQuintuple5x_png : "res/lobby/DJSQuintuple5x/DJSQuintuple5x.png",
    Button_DJSRespin_gaf : "res/lobby/DJSRespin/DJSRespin.gaf",
    //Button_DJSRespin_png : "res/lobby/DJSRespin/DJSRespin.png",
    Button_DJSRNFStar_gaf : "res/lobby/DJSRNFStar/DJSRNFStar.gaf",
    //Button_DJSRNFStar_png : "res/lobby/DJSRNFStar/DJSRNFStar.png",
    Button_DJSSpinTillYouWin_gaf : "res/lobby/DJSSpinTillYouWin/DJSSpinTillYouWin.gaf",
    //Button_DJSSpinTillYouWin_png : "res/lobby/DJSSpinTillYouWin/DJSSpinTillYouWin.png",
    Button_DJSSpinTillYouWinTwik_gaf : "res/lobby/DJSSpinTillYouWinTwik/DJSSpinTillYouWinTwik.gaf",
    //Button_DJSSpinTillYouWinTwik_png : "res/lobby/DJSSpinTillYouWinTwik/DJSSpinTillYouWinTwik.png",
    Button_DJSTriplePay_gaf : "res/lobby/DJSTriplePay/DJSTriplePay.gaf",
    //Button_DJSTriplePay_png : "res/lobby/DJSTriplePay/DJSTriplePay.png",
    Button_DJSWildX_gaf : "res/lobby/DJSWildX/DJSWildX.gaf",
    //Button_DJSWildX_png : "res/lobby/DJSWildX/DJSWildX.png",
    Button_DJSXMultipliers_gaf : "res/lobby/DJSXMultipliers/DJSXMultipliers.gaf",
    //Button_DJSXMultipliers_png : "res/lobby/DJSXMultipliers/DJSXMultipliers.png",
    Button_DJSCrystalSlot_gaf : "res/lobby/DJSCrystalSlot/DJSCrystalSlot.gaf",
    Button_DJSNeonSlot_gaf : "res/lobby/DJSNeonSlot/DJSNeonSlot.gaf",
    Button_DJSHalloweenSlot_gaf : "res/lobby/DJSHalloweenSlot/DJSHalloweenSlot.gaf",
    Button_DJSMagnetSlide_gaf : "res/lobby/DJSMagnetSlide/DJSMagnetSlide.gaf",
    Button_DJSGoldenSlot_gaf : "res/lobby/DJSGoldenSlot/DJSGoldenSlot.gaf",
    Button_DJSDoubleDiamond_gaf : "res/lobby/DJSDoubleDiamond/DJSDoubleDiamond.gaf"
};

/*var lobbyIconsGafPngResources = {//change 26 MAy
    Button_DJSBonusMachine_png : "res/lobby/DJSBonusMachine/DJSBonusMachine.png",
    Button_DJSCrazyCherry_png : "res/lobby/DJSCrazyCherry/DJSCrazyCherry.png",
    Button_DJSDiamond_png : "res/lobby/DJSDiamond/DJSDiamond.png",
    Button_DJSDiamond3X_png : "res/lobby/DJSDiamond3X/DJSDiamond3X.png",
    Button_DJSDiamondsForever_png : "res/lobby/DJSDiamondsForever/DJSDiamondsForever.png",
    Button_DJSFire_png : "res/lobby/DJSFire/DJSFire.png",
    Button_DJSFreeSpinner_png : "res/lobby/DJSFreeSpinner/DJSFreeSpinner.png",
    Button_DJSFreeSpins_png : "res/lobby/DJSFreeSpins/DJSFreeSpins.png",
    Button_DJSFreeze_png : "res/lobby/DJSFreeze/DJSFreeze.png",
    Button_DJSLightningRewind_png : "res/lobby/DJSLightningRewind/DJSLightningRewind.png",
    Button_DJSLightningRewindTwik_png : "res/lobby/DJSLightningRewindTwik/DJSLightningRewindTwik.png",
    Button_DJSQuintuple5x_png : "res/lobby/DJSQuintuple5x/DJSQuintuple5x.png",
    Button_DJSRespin_png : "res/lobby/DJSRespin/DJSRespin.png",
    Button_DJSRNFStar_png : "res/lobby/DJSRNFStar/DJSRNFStar.png",
    Button_DJSSpinTillYouWin_png : "res/lobby/DJSSpinTillYouWin/DJSSpinTillYouWin.png",
    Button_DJSSpinTillYouWinTwik_png : "res/lobby/DJSSpinTillYouWinTwik/DJSSpinTillYouWinTwik.png",
    Button_DJSTriplePay_png : "res/lobby/DJSTriplePay/DJSTriplePay.png",
    Button_DJSWildX_png : "res/lobby/DJSWildX/DJSWildX.png",
    Button_DJSXMultipliers_png : "res/lobby/DJSXMultipliers/DJSXMultipliers.png",
}*/

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}


if( false || !!document.documentMode ) {//change 15 May

}
else{
    for (var i in lobbyIconsResources) {
        g_resources.push(lobbyIconsResources[i]);
    }
}

var glows = {
    glow_png : "res/Glows/glow.png",
    glow_blue_png : "res/Glows/glowBlue.png",
    glow_green_png : "res/Glows/glowGreen.png",
    glow_orange_png : "res/Glows/glowOrange.png",
    glow_pink_png : "res/Glows/glowPink.png",
    glow_purple_png : "res/Glows/glowPurple.png",
    glow_yellow_png : "res/Glows/glowYellow.png"
};

for (var i in glows) {
    g_resources.push(glows[i]);
}

var game_assets = {
    effect_line : "res/megaWin/effect_line.png"
};

for (var i in game_assets) {
    g_resources.push(game_assets[i]);
}


var bonusRes = {
    //Bonus Resources
    Bonus_Game_png : "res/DJSBonusMachine/GamePlist.png",
    Bonus_Game_plist : "res/DJSBonusMachine/GamePlist.plist",
    Bonus_glow_png : "res/DJSBonusMachine/glow_low_winning.png",
    Bonus_glow1_png : "res/DJSBonusMachine/glow_fg1.png",
    Bonus_lights_png : "res/DJSBonusMachine/FreeSpin_text.png",
    Bonus_bg_png : "res/DJSBonusMachine/bg.png",
    Bonus_payTable_1_bg_png : "res/DJSBonusMachine/paytable_1.png",
    Bonus_payTable_2_bg_png : "res/DJSBonusMachine/paytable_2.png",
    Bonus_3_gaf : "res/DJSBonusMachine/3/3.gaf",
    Bonus_3_png : "res/DJSBonusMachine/3/3.png",
    Bonus_4_gaf : "res/DJSBonusMachine/4/4.gaf",
    Bonus_4_png : "res/DJSBonusMachine/4/4.png",
    Bonus_3x_gaf : "res/DJSBonusMachine/3x/3x.gaf",
    Bonus_3x_png : "res/DJSBonusMachine/3x/3x.png",
    Bonus_5_gaf : "res/DJSBonusMachine/5/5.gaf",
    Bonus_5_png : "res/DJSBonusMachine/5/5.png",
    Bonus_6_gaf : "res/DJSBonusMachine/6/6.gaf",
    Bonus_6_png : "res/DJSBonusMachine/6/6.png",
    Bonus_7_gaf : "res/DJSBonusMachine/7/7.gaf",
    Bonus_7_png : "res/DJSBonusMachine/7/7.png",
    Bonus_5x_gaf : "res/DJSBonusMachine/5x/5x.gaf",
    Bonus_5x_png : "res/DJSBonusMachine/5x/5x.png",
    Bonus_x_gaf : "res/DJSBonusMachine/x/x.gaf",
    Bonus_x_png : "res/DJSBonusMachine/x/x.png",
    //Bonus Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};

var DJSBonusMachine_resources = [];
for( var i in bonusRes )
{
    DJSBonusMachine_resources.push(bonusRes[i]);
}

var crazyCherryRes = {
    //DJSCrazyCherry Resources

    DJSCrazyCherry_Game_png : "res/DJSCrazyCherry/GamePlist.png",
    DJSCrazyCherry_Game_plist : "res/DJSCrazyCherry/GamePlist.plist",
    DJSCrazyCherry_bg_png : "res/DJSCrazyCherry/bg.png",
    DJSCrazyCherry_payTable_1_bg_png : "res/DJSCrazyCherry/paytable_1.png",
    DJSCrazyCherry_payTable_2_bg_png : "res/DJSCrazyCherry/paytable_2.png",

    //DJSCrazyCherry Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};

var DJSCrazyCherry_resources = [];
for( var i in crazyCherryRes )
{
    DJSCrazyCherry_resources.push(crazyCherryRes[i]);
}

var Diamond3xRes = {
    //DJSDiamond3X Resources

    DJSDiamond3X_Game_png : "res/DJSDiamond3X/GamePlist.png",
    DJSDiamond3X_Game_plist : "res/DJSDiamond3X/GamePlist.plist",
    DJSDiamond3X_bg_png : "res/DJSDiamond3X/bg.png",
    DJSDiamond3X_black_bg_png : "res/DJSDiamond3X/black_bg.png",
    DJSDiamond3X_lights_png : "res/DJSDiamond3X/lights.png",
    DJSDiamond3X_wheel_bg_png : "res/DJSDiamond3X/wheel_bg.png",
    DJSDiamond3X_wheel_png : "res/DJSDiamond3X/wheel.png",
    DJSDiamond3X_payTable_1_bg_png : "res/DJSDiamond3X/paytable_1.png",
    DJSDiamond3X_payTable_2_bg_png : "res/DJSDiamond3X/paytable_2.png",

    //DJSDiamond3X Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};

var DJSDiamond3x_resources = [];
for( var i in Diamond3xRes )
{
    DJSDiamond3x_resources.push(Diamond3xRes[i]);
}

var DJSDiamondRes = {
    //DJSDiamond Resources

    DJSDiamond_Game_png: "res/DJSDiamond/GamePlist.png",
    DJSDiamond_Game_plist: "res/DJSDiamond/GamePlist.plist",
    DJSDiamond_bg_png: "res/DJSDiamond/bg.png",
    DJSDiamond_payTable_1_bg_png: "res/DJSDiamond/paytable_1.png",
    DJSDiamond_payTable_2_bg_png: "res/DJSDiamond/paytable_2.png",

    //DJSDiamond Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};

var DJSDiamond_resources = [];
for( var i in DJSDiamondRes )
{
    DJSDiamond_resources.push(DJSDiamondRes[i]);
}

var DiamondsForeverRes = {
    //DJSDiamondsForever Resources

    DJSDiamondsForever_Game_png : "res/DJSDiamondsForever/GamePlist.png",
    DJSDiamondsForever_Game_plist : "res/DJSDiamondsForever/GamePlist.plist",
    DJSDiamondsForever_bg_png : "res/DJSDiamondsForever/bg.png",
    DJSDiamondsForever_payTable_1_bg_png : "res/DJSDiamondsForever/paytable_1.png",
    DJSDiamondsForever_payTable_2_bg_png : "res/DJSDiamondsForever/paytable_2.png",

    //DJSDiamondsForever Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};
var DJSDiamondsForever_resources = [];
for( var i in DiamondsForeverRes )
{
    DJSDiamondsForever_resources.push(DiamondsForeverRes[i]);
}

var DJSDoubleDiamondRes = {
    //DJSDiamondsForever Resources

    DJSDoubleDiamond_Game_png : "res/DJSDoubleDiamond/GamePlist.png",
    DJSDoubleDiamond_Game_plist : "res/DJSDoubleDiamond/GamePlist.plist",
    DJSDoubleDiamond_bg_png : "res/DJSDoubleDiamond/bg.png",
    DJSDoubleDiamond_payTable_1_bg_png : "res/DJSDoubleDiamond/paytable_1.png",
    DJSDoubleDiamond_payTable_2_bg_png : "res/DJSDoubleDiamond/paytable_2.png",

    //DJSDiamondsForever Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};
var DJSDoubleDiamond_resources = [];
for( var i in DJSDoubleDiamondRes )
{
    DJSDoubleDiamond_resources.push(DJSDoubleDiamondRes[i]);
}

var QuintupleRes = {
    //Quintuple Resources
    Quintuple_png : "res/DJSQuintuple5x/elements.png",
    Quintuple_plist : "res/DJSQuintuple5x/elements.plist",
    Quintuple_Game_png : "res/DJSQuintuple5x/GamePlist.png",
    Quintuple_Game_plist : "res/DJSQuintuple5x/GamePlist.plist",

    Quintuple_bg_png : "res/DJSQuintuple5x/bg.png",
    Quintuple_payTable_1_bg_png : "res/DJSQuintuple5x/paytable_1.png",
    Quintuple_payTable_2_bg_png : "res/DJSQuintuple5x/paytable_2.png",
    Quintuple_payTable_3_bg_png : "res/DJSQuintuple5x/paytable_3.png",

    Quintuple_2x_gaf : "res/DJSQuintuple5x/2x/2x.gaf",
    Quintuple_2x_png : "res/DJSQuintuple5x/2x/2x.png",
    Quintuple_3x_gaf : "res/DJSQuintuple5x/3x/3x.gaf",
    Quintuple_3x_png : "res/DJSQuintuple5x/3x/3x.png",
    Quintuple_5x_gaf : "res/DJSQuintuple5x/5x/5x.gaf",
    Quintuple_5x_png : "res/DJSQuintuple5x/5x/5x.png",
    Quintuple_10x_gaf : "res/DJSQuintuple5x/10x/10x.gaf",
    Quintuple_10x_png : "res/DJSQuintuple5x/10x/10x.png",
    //Quintuple Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};
var Quintuple_resources = [];
for( var i in QuintupleRes )
{
    Quintuple_resources.push(QuintupleRes[i]);
}

var fireRes = {
    //Fire Resources
    Fire_Game_png : "res/DJSFire/GamePlist.png",
    Fire_Game_plist : "res/DJSFire/GamePlist.plist",
    Fire_FireFrames_plist : "res/DJSFire/FireFrames.plist",
    Fire_FireFrames_png : "res/DJSFire/FireFrames.png",

    Fire_glow_png : "res/DJSFire/glowYellowFG.png",

    Fire_bg_png : "res/DJSFire/bg.png",
    Fire_payTable_1_bg_png : "res/DJSFire/paytable_1.png",
    Fire_payTable_2_bg_png : "res/DJSFire/paytable_2.png",
    Fire_1_gaf : "res/DJSFire/1/1.gaf",
    Fire_1_png : "res/DJSFire/1/1.png",
    Fire_17_gaf : "res/DJSFire/17/17.gaf",
    Fire_17_png : "res/DJSFire/17/17.png",
    //Fire Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};

var DJSFire_resources = [];
for( var i in fireRes )
{
    DJSFire_resources.push(fireRes[i]);
}

var spinTillYouWinRes = {
    //Spin Till You Win Resources
    STYW_Game_png : "res/DJSSpinTillYouWin/GamePlist.png",
    STYW_Game_plist : "res/DJSSpinTillYouWin/GamePlist.plist",
    STYW_bg_png : "res/DJSSpinTillYouWin/bg.png",
    STYW_payTable_1_bg_png : "res/DJSSpinTillYouWin/paytable_1.png",
    STYW_payTable_2_bg_png : "res/DJSSpinTillYouWin/paytable_2.png",
    STYW_2x_gaf : "res/DJSSpinTillYouWin/2x/2x.gaf",
    STYW_2x_png : "res/DJSSpinTillYouWin/2x/2x.png",
    STYW_3x_gaf : "res/DJSSpinTillYouWin/3x/3x.gaf",
    STYW_3x_png : "res/DJSSpinTillYouWin/3x/3x.png",
    STYW_spinElm_gaf : "res/DJSSpinTillYouWin/spinElm/spinElm.gaf",
    STYW_spinElm_png : "res/DJSSpinTillYouWin/spinElm/spinElm.png",
    //Spin Till You Win Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};
var DJSSpinTillYouWin_resources = [];
for( var i in spinTillYouWinRes )
{
    DJSSpinTillYouWin_resources.push(spinTillYouWinRes[i]);
}

var freeSpinsRes = {
    //DJSFreeSpins Resources

    DJSFreeSpins_Game_png : "res/DJSFreeSpins/GamePlist.png",
    DJSFreeSpins_Game_plist : "res/DJSFreeSpins/GamePlist.plist",
    DJSFreeSpins_bg_png : "res/DJSFreeSpins/bg.png",
    DJSFreeSpins_payTable_1_bg_png : "res/DJSFreeSpins/paytable_1.png",
    DJSFreeSpins_payTable_2_bg_png : "res/DJSFreeSpins/paytable_2.png",

    //DJSFreeSpins Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};
var DJSFreeSpins_resources = [];
for( var i in freeSpinsRes )
{
    DJSFreeSpins_resources.push(freeSpinsRes[i]);
}

var freeSpinnerRes = {
    //DJSFreeSpinner Resources

    DJSFreeSpinner_Game_png : "res/DJSFreeSpinner/GamePlist.png",
    DJSFreeSpinner_Game_plist : "res/DJSFreeSpinner/GamePlist.plist",
    DJSFreeSpinner_bg_png : "res/DJSFreeSpinner/bg.png",
    DJSFreeSpinner_payTable_1_bg_png : "res/DJSFreeSpinner/paytable_1.png",
    DJSFreeSpinner_payTable_2_bg_png : "res/DJSFreeSpinner/paytable_2.png",

    //DJSFreeSpinner Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};
var DJSFreeSpinner_resources = [];
for( var i in freeSpinnerRes )
{
    DJSFreeSpinner_resources.push(freeSpinnerRes[i]);
}

var freezeRes = {
    //Freeze Resources
    Freeze_Game_png : "res/DJSFreeze/GamePlist.png",
    Freeze_Game_plist : "res/DJSFreeze/GamePlist.plist",
    Freeze_glow_png : "res/DJSFreeze/glow.png",
    Freeze_glow1_png : "res/DJSFreeze/glow1.png",
    Freeze_lights_png : "res/DJSFreeze/lights.png",
    Freeze_low_win_png : "res/DJSFreeze/low_win_bg.png",
    Freeze_bg_png : "res/DJSFreeze/bg.png",
    Freeze_payTable_1_bg_png : "res/DJSFreeze/paytable_1.png",
    Freeze_payTable_2_bg_png : "res/DJSFreeze/paytable_2.png",
    Freeze_1_gaf : "res/DJSFreeze/1/1.gaf",
    Freeze_1_png : "res/DJSFreeze/1/1.png",
    Freeze_5_gaf : "res/DJSFreeze/5/5.gaf",
    Freeze_5_png : "res/DJSFreeze/5/5.png",
    Freeze_6_gaf : "res/DJSFreeze/6/6.gaf",
    Freeze_6_png : "res/DJSFreeze/6/6.png",
    Freeze_7_gaf : "res/DJSFreeze/7/7.gaf",
    Freeze_7_png : "res/DJSFreeze/7/7.png",
    Freeze_17_gaf : "res/DJSFreeze/17/17.gaf",
    Freeze_17_png : "res/DJSFreeze/17/17.png",
    Freeze_x_gaf : "res/DJSFreeze/x/x.gaf",
    Freeze_x_png : "res/DJSFreeze/x/x.png",
    //Freeze Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};
var DJSFreeze_resources = [];
for( var i in freezeRes )
{
    DJSFreeze_resources.push(freezeRes[i]);
}

var lighteningRewindRes = {
    //DJSLightningRewind Resources

    DJSLightningRewind_Game_png : "res/DJSLightningRewind/GamePlist.png",
    DJSLightningRewind_Game_plist : "res/DJSLightningRewind/GamePlist.plist",
    DJSLightningRewind_bg_png : "res/DJSLightningRewind/bg.png",
    DJSLightningRewind_payTable_1_bg_png : "res/DJSLightningRewind/paytable_1.png",
    DJSLightningRewind_payTable_2_bg_png : "res/DJSLightningRewind/paytable_2.png",
    DJSLightningRewind_black_bg_png : "res/DJSLightningRewind/black_bg.png",

    //DJSLightningRewind Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};
var DJSLightningRewind_resources = [];
for( var i in lighteningRewindRes )
{
    DJSLightningRewind_resources.push(lighteningRewindRes[i]);
}

var lighteningRewindTwikRes = {

    //DJSLightningRewindTwik Resources

    DJSLightningRewindTwik_Game_png: "res/DJSLightningRewindTwik/GamePlist.png",
    DJSLightningRewindTwik_Game_plist: "res/DJSLightningRewindTwik/GamePlist.plist",
    DJSLightningRewindTwik_bg_png: "res/DJSLightningRewindTwik/bg.png",
    DJSLightningRewindTwik_payTable_1_bg_png: "res/DJSLightningRewindTwik/paytable_1.png",
    DJSLightningRewindTwik_payTable_2_bg_png: "res/DJSLightningRewindTwik/paytable_2.png",

    //DJSLightningRewindTwik Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};
var DJSLightningRewindTwik_resources = [];
for( var i in lighteningRewindTwikRes )
{
    DJSLightningRewindTwik_resources.push(lighteningRewindTwikRes[i]);
}

var respinRes = {
    //DJSRespin Resources

    DJSRespin_Game_png : "res/DJSRespin/GamePlist.png",
    DJSRespin_Game_plist : "res/DJSRespin/GamePlist.plist",
    DJSRespin_bg_png : "res/DJSRespin/bg.png",
    DJSRespin_payTable_1_bg_png : "res/DJSRespin/paytable_1.png",
    DJSRespin_payTable_2_bg_png : "res/DJSRespin/paytable_2.png",

    //DJSRespin Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};
var DJSRespin_resources = [];
for( var i in respinRes )
{
    DJSRespin_resources.push(respinRes[i]);
}

var rnfStarRes = {
    //DJSRNFStar Resources

    DJSRNFStar_Game_png : "res/DJSRNFStar/GamePlist.png",
    DJSRNFStar_Game_plist : "res/DJSRNFStar/GamePlist.plist",
    DJSRNFStar_bg_png : "res/DJSRNFStar/bg.png",
    DJSRNFStar_payTable_1_bg_png : "res/DJSRNFStar/paytable_1.png",
    DJSRNFStar_payTable_2_bg_png : "res/DJSRNFStar/paytable_2.png",
    DJSRNFStar_bigGlow_png : "res/DJSRNFStar/bigGlow.png",
    DJSRNFStar_glow_machine_png : "res/DJSRNFStar/glow_machine.png",
    DJSRNFStar_multiplier_png : "res/DJSRNFStar/multiplier.png",

    //DJSRNFStar Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};
var DJSRNFStar_resources = [];
for( var i in rnfStarRes )
{
    DJSRNFStar_resources.push(rnfStarRes[i]);
}

var spinTillYouWinTwikRes = {
    //Spin Till You Win Twik Resources
    DJSSpinTillYouWinTwik_Game_png : "res/DJSSpinTillYouWinTwik/GamePlist.png",
    DJSSpinTillYouWinTwik_Game_plist : "res/DJSSpinTillYouWinTwik/GamePlist.plist",
    DJSSpinTillYouWinTwik_bg_png : "res/DJSSpinTillYouWinTwik/bg.png",
    DJSSpinTillYouWinTwik_payTable_1_bg_png : "res/DJSSpinTillYouWinTwik/paytable_1.png",
    DJSSpinTillYouWinTwik_payTable_2_bg_png : "res/DJSSpinTillYouWinTwik/paytable_2.png",
    //Spin Till You Win Twik Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};
var DJSSpinTillYouWinTwik_resources = [];
for( var i in spinTillYouWinTwikRes )
{
    DJSSpinTillYouWinTwik_resources.push(spinTillYouWinTwikRes[i]);
}

var triplePayRes = {
    //DJSTriplePay Resources

    DJSTriplePay_Game_png : "res/DJSTriplePay/GamePlist.png",
    DJSTriplePay_Game_plist : "res/DJSTriplePay/GamePlist.plist",
    DJSTriplePay_bg_png : "res/DJSTriplePay/bg.png",
    DJSTriplePay_payTable_1_bg_png : "res/DJSTriplePay/paytable_1.png",
    DJSTriplePay_payTable_2_bg_png : "res/DJSTriplePay/paytable_2.png",

    //DJSTriplePay Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};
var DJSTriplePay_resources = [];
for( var i in triplePayRes )
{
    DJSTriplePay_resources.push(triplePayRes[i]);
}

var wildXRes = {
    //DJSWildX Resources

    DJSWildX_Game_png : "res/DJSWildX/GamePlist.png",
    DJSWildX_Game_plist : "res/DJSWildX/GamePlist.plist",
    DJSWildX_bg_png : "res/DJSWildX/bg.png",
    DJSWildX_payTable_1_bg_png : "res/DJSWildX/paytable_1.png",
    DJSWildX_payTable_2_bg_png : "res/DJSWildX/paytable_2.png",

    //DJSWildX Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};
var DJSWildX_resources = [];
for( var i in wildXRes )
{
    DJSWildX_resources.push(wildXRes[i]);
}

var xMultipliersRes = {
    //DJSXMultipliers Resources

    DJSXMultipliers_Game_png : "res/DJSXMultipliers/GamePlist.png",
    DJSXMultipliers_Game_plist : "res/DJSXMultipliers/GamePlist.plist",
    DJSXMultipliers_bg_png : "res/DJSXMultipliers/bg.png",

//DJSXMultipliers Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};

var DJSXMultipliers_resources = [];
for( var i in xMultipliersRes )
{
    DJSXMultipliers_resources.push(xMultipliersRes[i]);
}

for( var i in QuintupleRes )
{
    DJSXMultipliers_resources.push(QuintupleRes[i]);
}

var DJSCrystalSlotRes = {
    //DJSCrystalSlot Resources

    DJSCrystalSlot_Game_png: "res/DJSCrystalSlot/GamePlist.png",
    DJSCrystalSlot_Game_plist: "res/DJSCrystalSlot/GamePlist.plist",
    DJSCrystalSlot_bg_png: "res/DJSCrystalSlot/bg.png",
    DJSCrystalSlot_payTable_1_bg_png: "res/DJSCrystalSlot/paytable_1.png",
    DJSCrystalSlot_payTable_2_bg_png: "res/DJSCrystalSlot/paytable_2.png",
    DJSCrystalSlot_payTable_3_bg_png: "res/DJSCrystalSlot/paytable_3.png",

    DJSCrystalSlot_x_gaf : "res/DJSCrystalSlot/x/x.gaf",
    DJSCrystalSlot_x_png : "res/DJSCrystalSlot/x/x.png",

    DJSCrystalSlot_2_gaf : "res/DJSCrystalSlot/2/2.gaf",
    DJSCrystalSlot_2_png : "res/DJSCrystalSlot/2/2.png",

    DJSCrystalSlot_2x_gaf : "res/DJSCrystalSlot/2x/2x.gaf",
    DJSCrystalSlot_2x_png : "res/DJSCrystalSlot/2x/2x.png",

    DJSCrystalSlot_3x_gaf : "res/DJSCrystalSlot/3x/3x.gaf",
    DJSCrystalSlot_3x_png : "res/DJSCrystalSlot/3x/3x.png",

    DJSCrystalSlot_4x_gaf : "res/DJSCrystalSlot/4x/4x.gaf",
    DJSCrystalSlot_4x_png : "res/DJSCrystalSlot/4x/4x.png",

    DJSCrystalSlot_5_gaf : "res/DJSCrystalSlot/5/5.gaf",
    DJSCrystalSlot_5_png : "res/DJSCrystalSlot/5/5.png",

    DJSCrystalSlot_5x_gaf : "res/DJSCrystalSlot/5x/5x.gaf",
    DJSCrystalSlot_5x_png : "res/DJSCrystalSlot/5x/5x.png",

    DJSCrystalSlot_6_gaf : "res/DJSCrystalSlot/6/6.gaf",
    DJSCrystalSlot_6_png : "res/DJSCrystalSlot/6/6.png",

    DJSCrystalSlot_7_gaf : "res/DJSCrystalSlot/7/7.gaf",
    DJSCrystalSlot_7_png : "res/DJSCrystalSlot/7/7.png",

    DJSCrystalSlot_19_gaf : "res/DJSCrystalSlot/19/19.gaf",
    DJSCrystalSlot_19_png : "res/DJSCrystalSlot/19/19.png",
    //DJSCrystalSlot Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist",

    Jackpot_bg_png : "res/jackpot/jackpot_winning_bg.png",
    Jackpot_close_btn_bg : "res/jackpot/tournament_collect_button.png"
};

var DJSCrystalSlot_resources = [];
for( var i in DJSCrystalSlotRes )
{
    DJSCrystalSlot_resources.push(DJSCrystalSlotRes[i]);
}

var DJSGoldenSlotRes = {
    //DJSGoldenSlot Resources

    DJSGoldenSlot_Game_png: "res/DJSGoldenSlot/GamePlist.png",
    DJSGoldenSlot_Game_plist: "res/DJSGoldenSlot/GamePlist.plist",
    DJSGoldenSlot_bg_png: "res/DJSGoldenSlot/bg.png",
    DJSGoldenSlot_payTable_1_bg_png: "res/DJSGoldenSlot/paytable_1.png",
    DJSGoldenSlot_payTable_2_bg_png: "res/DJSGoldenSlot/paytable_2.png",
    DJSGoldenSlot_payTable_3_bg_png: "res/DJSGoldenSlot/paytable_3.png",

    DJSGoldenSlot_x_gaf : "res/DJSGoldenSlot/x/x.gaf",
    DJSGoldenSlot_x_png : "res/DJSGoldenSlot/x/x.png",

    DJSGoldenSlot_2_gaf : "res/DJSGoldenSlot/2/2.gaf",
    DJSGoldenSlot_2png : "res/DJSGoldenSlot/2/2.png",

    DJSGoldenSlot_3_gaf : "res/DJSGoldenSlot/3/3.gaf",
    DJSGoldenSlot_3_png : "res/DJSGoldenSlot/3/3.png",

    DJSGoldenSlot_4_gaf : "res/DJSGoldenSlot/4/4.gaf",
    DJSGoldenSlot_4_png : "res/DJSGoldenSlot/4/4.png",

    DJSGoldenSlot_5_gaf : "res/DJSGoldenSlot/5/5.gaf",
    DJSGoldenSlot_5_png : "res/DJSGoldenSlot/5/5.png",

    DJSGoldenSlot_6_gaf : "res/DJSGoldenSlot/6/6.gaf",
    DJSGoldenSlot_6_png : "res/DJSGoldenSlot/6/6.png",

    DJSGoldenSlot_7_gaf : "res/DJSGoldenSlot/7/7.gaf",
    DJSGoldenSlot_7_png : "res/DJSGoldenSlot/7/7.png",

    //DJSGoldenSlot Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist",

    Jackpot_bg_png : "res/jackpot/jackpot_winning_bg.png",
    Jackpot_close_btn_bg : "res/jackpot/tournament_collect_button.png"
};

var DJSGoldenSlot_resources = [];
for( var i in DJSGoldenSlotRes )
{
    DJSGoldenSlot_resources.push(DJSGoldenSlotRes[i]);
}

var DJSNeonSlotRes = {
    //DJSNeonSlot Resources

    DJSNeonSlot_Game_png: "res/DJSNeonSlot/GamePlist.png",
    DJSNeonSlot_Game_plist: "res/DJSNeonSlot/GamePlist.plist",
    DJSNeonSlot_bg_png: "res/DJSNeonSlot/bg.png",
    DJSNeonSlot_payTable_1_bg_png: "res/DJSNeonSlot/paytable_1.png",
    DJSNeonSlot_payTable_2_bg_png: "res/DJSNeonSlot/paytable_2.png",

    DJSNeonSlot_1_gaf : "res/DJSNeonSlot/1/1.gaf",
    DJSNeonSlot_1_png : "res/DJSNeonSlot/1/1.png",

    DJSNeonSlot_2_gaf : "res/DJSNeonSlot/2/2.gaf",
    DJSNeonSlot_2_png : "res/DJSNeonSlot/2/2.png",

    DJSNeonSlot_2x_gaf : "res/DJSNeonSlot/2x/2x.gaf",
    DJSNeonSlot_2x_png : "res/DJSNeonSlot/2x/2x.png",

    DJSNeonSlot_3_gaf : "res/DJSNeonSlot/3/3.gaf",
    DJSNeonSlot_3_png : "res/DJSNeonSlot/3/3.png",

    DJSNeonSlot_3x_gaf : "res/DJSNeonSlot/3x/3x.gaf",
    DJSNeonSlot_3x_png : "res/DJSNeonSlot/3x/3x.png",

    DJSNeonSlot_4_gaf : "res/DJSNeonSlot/4/4.gaf",
    DJSNeonSlot_4_png : "res/DJSNeonSlot/4/4.png",

    DJSNeonSlot_4x_gaf : "res/DJSNeonSlot/4x/4x.gaf",
    DJSNeonSlot_4x_png : "res/DJSNeonSlot/4x/4x.png",

    DJSNeonSlot_5_gaf : "res/DJSNeonSlot/5/5.gaf",
    DJSNeonSlot_5_png : "res/DJSNeonSlot/5/5.png",

    DJSNeonSlot_5x_gaf : "res/DJSNeonSlot/5x/5x.gaf",
    DJSNeonSlot_5x_png : "res/DJSNeonSlot/5x/5x.png",

    DJSNeonSlot_6_gaf : "res/DJSNeonSlot/6/6.gaf",
    DJSNeonSlot_6_png : "res/DJSNeonSlot/6/6.png",

    DJSNeonSlot_7_gaf : "res/DJSNeonSlot/7/7.gaf",
    DJSNeonSlot_7_png : "res/DJSNeonSlot/7/7.png",

    //DJSNeonSlot Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};

var DJSNeonSlot_resources = [];
for( var i in DJSNeonSlotRes )
{
    DJSNeonSlot_resources.push(DJSNeonSlotRes[i]);
}

var DJSHalloweenSlotRes = {
    //DJSHalloweenSlot Resources

    DJSHalloweenSlot_Game_png: "res/DJSHalloweenSlot/GamePlist.png",
    DJSHalloweenSlot_Game_plist: "res/DJSHalloweenSlot/GamePlist.plist",
    DJSHalloweenSlot_bg_png: "res/DJSHalloweenSlot/bg.png",
    DJSHalloweenSlot_payTable_1_bg_png: "res/DJSHalloweenSlot/paytable_1.png",
    DJSHalloweenSlot_payTable_2_bg_png: "res/DJSHalloweenSlot/paytable_2.png",

    DJSHalloweenSlot_1_gaf : "res/DJSHalloweenSlot/1/1.gaf",
    DJSHalloweenSlot_1_png : "res/DJSHalloweenSlot/1/1.png",

    DJSHalloweenSlot_2x_gaf : "res/DJSHalloweenSlot/2x/2x.gaf",
    DJSHalloweenSlot_2x_png : "res/DJSHalloweenSlot/2x/2x.png",

    DJSHalloweenSlot_5_gaf : "res/DJSHalloweenSlot/5/5.gaf",
    DJSHalloweenSlot_5_png : "res/DJSHalloweenSlot/5/5.png",

    DJSHalloweenSlot_5x_gaf : "res/DJSHalloweenSlot/5x/5x.gaf",
    DJSHalloweenSlot_5x_png : "res/DJSHalloweenSlot/5x/5x.png",

    DJSHalloweenSlot_6_gaf : "res/DJSHalloweenSlot/6/6.gaf",
    DJSHalloweenSlot_6_png : "res/DJSHalloweenSlot/6/6.png",

    DJSHalloweenSlot_7_gaf : "res/DJSHalloweenSlot/7/7.gaf",
    DJSHalloweenSlot_7_png : "res/DJSHalloweenSlot/7/7.png",

    DJSHalloweenSlot_17_gaf : "res/DJSHalloweenSlot/17/17.gaf",
    DJSHalloweenSlot_17_png : "res/DJSHalloweenSlot/17/17.png",

    DJSHalloweenSlot_web_png : "res/DJSHalloweenSlot/cobWebs/web1.png",

    //DJSHalloweenSlot Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};

var DJSHalloweenSlot_resources = [];
for( var i in DJSHalloweenSlotRes )
{
    DJSHalloweenSlot_resources.push(DJSHalloweenSlotRes[i]);
}

var DJSMagnetSlideSlotRes = {
    //DJSMagnetSlideSlot Resources

    DJSMagnetSlideSlot_Game_png: "res/DJSMagnetSlide/GamePlist.png",
    DJSMagnetSlideSlot_Game_plist: "res/DJSMagnetSlide/GamePlist.plist",
    DJSMagnetSlideSlot_bg_png: "res/DJSMagnetSlideS/bg.png",
    DJSMagnetSlideSlot_payTable_1_bg_png: "res/DJSMagnetSlide/paytable_1.png",
    DJSMagnetSlideSlot_payTable_2_bg_png: "res/DJSMagnetSlide/paytable_2.png",
    DJSMagnetSlideSlot_payTable_3_bg_png: "res/DJSMagnetSlide/paytable_3.png",
    DJSMagnetSlideSlot_payTable_4_bg_png: "res/DJSMagnetSlide/paytable_4.png",
    DJSMagnetSlideSlot_payTable_5_bg_png: "res/DJSMagnetSlide/paytable_5.png",
    DJSMagnetSlideSlot_payTable_6_bg_png: "res/DJSMagnetSlide/paytable_6.png",
    DJSMagnetSlideSlot_payTable_7_bg_png: "res/DJSMagnetSlide/paytable_7.png",

    DJSMagnetSlideSlot_1_gaf : "res/DJSMagnetSlide/1/1.gaf",
    DJSMagnetSlideSlot_1_png : "res/DJSMagnetSlide/1/1.png",

    DJSMagnetSlideSlot_2x_gaf : "res/DJSMagnetSlide/2x/2x.gaf",
    DJSMagnetSlideSlot_2x_png : "res/DJSMagnetSlide/2x/2x.png",

    DJSMagnetSlideSlot_3x_gaf : "res/DJSMagnetSlide/3x/3x.gaf",
    DJSMagnetSlideSlot_3x_png : "res/DJSMagnetSlide/3x/3x.png",

    DJSMagnetSlideSlot_4_gaf : "res/DJSMagnetSlide/4/4.gaf",
    DJSMagnetSlideSlot_4_png : "res/DJSMagnetSlide/4/4.png",

    DJSMagnetSlideSlot_5_gaf : "res/DJSMagnetSlide/5/5.gaf",
    DJSMagnetSlideSlot_5_png : "res/DJSMagnetSlide/5/5.png",

    DJSMagnetSlideSlot_6_gaf : "res/DJSMagnetSlide/6/6.gaf",
    DJSMagnetSlideSlot_6_png : "res/DJSMagnetSlide/6/6.png",

    DJSMagnetSlideSlot_7_gaf : "res/DJSMagnetSlide/7/7.gaf",
    DJSMagnetSlideSlot_7_png : "res/DJSMagnetSlide/7/7.png",

    DJSMagnetSlideSlot_8_gaf : "res/DJSMagnetSlide/8/8.gaf",
    DJSMagnetSlideSlot_8_png : "res/DJSMagnetSlide/8/8.png",

    DJSMagnetSlideSlot_11_gaf : "res/DJSMagnetSlide/11/11.gaf",
    DJSMagnetSlideSlot_11_png : "res/DJSMagnetSlide/11/11.png",

    DJSMagnetSlideSlot_thunder_effect_gaf : "res/DJSMagnetSlide/thunder_effect/thunder_effect.gaf",
    DJSMagnetSlideSlot_thunder_effect_png : "res/DJSMagnetSlide/thunder_effect/thunder_effect.png",

    DJSMagnetSlideSlot_thunder_effect_win_gaf : "res/DJSMagnetSlide/thunder_effect_win/thunder_effect_win.gaf",
    DJSMagnetSlideSlot_thunder_effect_win_png : "res/DJSMagnetSlide/thunder_effect_win/thunder_effect_win.png",

    //DJSMagnetSlideSlot Resources

    GameButtons_png : "res/GameButtons/GameButtons_1_18.png",
    GameButtons_plist : "res/GameButtons/GameButtons_1_18.plist",
    MachUnlock_png : "res/GameButtons/LevelUpPop_1_18.png",
    MachUnlock_plist : "res/GameButtons/LevelUpPop_1_18.plist"
};

var DJSMagnetSlideSlot_resources = [];
for( var i in DJSMagnetSlideSlotRes )
{
    DJSMagnetSlideSlot_resources.push(DJSMagnetSlideSlotRes[i]);
}

var soundFiles = {
    //Sound Files
    Sound_Ambience_back_mp3 : "res/sounds/ambience_back.mp3",
    Sound_Ambience_mp3      : "res/sounds/ambience_lobby.mp3",
    Sound_btn_click_mp3     : "res/sounds/btn_click.mp3",
    Sound_Bonus_bg_mp3      : "res/sounds/bonus_bg.mp3",
    Sound_Bonus_pop_mp3     : "res/sounds/bonusPop.mp3",
    Sound_Wild_mp3          : "res/sounds/wild.mp3",
    Sound_level_up_mp3      : "res/sounds/level_up.mp3",
    Sound_reel_stop_mp3     : "res/sounds/reel_stop.mp3",
    Sound_excitement_mp3    : "res/sounds/excitement.mp3",
    Sound_reel_1_mp3        : "res/sounds/reel_1.mp3",
    Sound_reel_2_mp3        : "res/sounds/reel_2.mp3",
    Sound_reel_3_mp3        : "res/sounds/reel_3.mp3",
    Sound_reel_4_mp3        : "res/sounds/reel_4.mp3",
    Sound_reel_5_mp3        : "res/sounds/reel_5.mp3",
    Sound_reel_6_mp3        : "res/sounds/reel_6.mp3",
    Sound_reel_7_mp3        : "res/sounds/reel_7.mp3",
    Sound_reel_8_mp3        : "res/sounds/reel_8.mp3",
    Sound_reel_9_mp3        : "res/sounds/reel_9.mp3",
    Sound_reel_10_mp3        : "res/sounds/reel_10.mp3",
    Sound_reel_11_mp3        : "res/sounds/reel_11.mp3",
    Sound_reel_12_mp3        : "res/sounds/reel_12.mp3",
    Sound_reel_13_mp3        : "res/sounds/reel_13.mp3",
    Sound_reel_14_mp3        : "res/sounds/reel_14.mp3",
    Sound_reel_15_mp3        : "res/sounds/reel_15.mp3",
    Sound_coins_mp3         : "res/sounds/coins.mp3",
    Sound_coin_more_mp3     : "res/sounds/coin_more.mp3",
    Sound_Slide_1           : "res/sounds/slide_1.mp3",
    Sound_Slide_2           : "res/sounds/slide_2.mp3",
    Sound_Slide_3           : "res/sounds/slide_3.mp3",
    SOUND_FLASH             : "res/sounds/flash.mp3",
    SOUND_REVERSAL          : "res/sounds/reverse.mp3",
    SOUND_SLIDE_1           : "res/sounds/slide_1.mp3",
    SOUND_SLIDE_2           : "res/sounds/slide_2.mp3",
    SOUND_SLIDE_3           : "res/sounds/slide_3.mp3",
    SOUND_WHEEL_BG          : "res/sounds/bonus_bg.mp3",
    SOUND_BONUS_WIN         : "res/sounds/bonusWin.mp3",
    SOUND_WHEEL             : "res/sounds/wheel.mp3",
    SOUND_BONUS_POP         : "res/sounds/bonusPop.mp3",
    SOUND_WILD              : "res/sounds/wild.mp3",
    SOUND_XX_WILD           : "res/sounds/xxWild.mp3",
    SOUND_RESPIN            : "res/sounds/reverse.mp3",
    SOUND_RESPIN_ROLL       : "res/sounds/respinRoll.mp3",
    SOUND_LEVEL_UP          : "res/sounds/level_up.mp3",
    SOUND_FREESPIN          : "res/sounds/fSpin.mp3",//when free spin arrived
    SOUND_FREESPIN_E        : "res/sounds/fSpinFirstReelExcitment.mp3",//when free spin icon arrives at first lane
    SOUND_SPIN_TILL_WIN     : "res/sounds/spinTillWin.mp3",
    SOUND_DJS_FIRE          : "res/sounds/DJSFire.mp3",
    /*SOUND_REEL_ALT_1        : "res/sounds/reel_alt_1.mp3",
    SOUND_REEL_ALT_2        : "res/sounds/reel_alt_2.mp3",
    SOUND_REEL_ALT_3        : "res/sounds/reel_alt_3.mp3",
    SOUND_REEL_ALT_4        : "res/sounds/reel_alt_4.mp3",
    SOUND_REEL_ALT_5        : "res/sounds/reel_alt_5.mp3",
    SOUND_REEL_ALT_6        : "res/sounds/reel_alt_6.mp3",*/
    S_GIFT_BOX_OPEN         : "res/sounds/GiftBoxOpen.mp3",
    S_MISSION_OPEN         : "res/sounds/missionOpen.mp3",
    SOUND_ICE_FREEZE        : "res/sounds/iceFreezing.mp3",
    SOUND_MEGA_WIN          : "res/sounds/megaWin.mp3",
    SOUND_STATIC            : "res/sounds/static.mp3",
    SOUND_STATIC_WIN        : "res/sounds/staticWin.mp3",
    SOUND_PUMPKIN           : "res/sounds/pumpkin.mp3",
    SOUND_WITCH             : "res/sounds/witch.mp3",
    SOUND_ZOMBIE            : "res/sounds/zombie.mp3",
    SOUND_JACKPOT_WIN       : "res/sounds/jackpotWin.mp3",
    SOUND_HUGE_WIN_COINS    : "res/sounds/hugeWinCoins.mp3",
    SOUND_BIG_WIN_COINS     : "res/sounds/bigWinCoins.mp3",
    SOUND_BIG_WIN_SOUND     : "res/sounds/big_win_sound.mp3",
    SOUND_GRAND_WIN_COINS   : "res/sounds/grandWinCoins.mp3",
    SOUND_HUGE_WIN_COINS_TONE   : "res/sounds/hugeWinCoins_tone.mp3",
    SOUND_BIG_WIN_COINS_TONE: "res/sounds/bigWinCoins_tone.mp3",
    SOUND_GRAND_WIN_COINS_TONE: "res/sounds/grandWinCoins_tone.mp3",
    SOUND_PAID_WHEEL_BG : "res/sounds/paidWheelBG.mp3",
    SOUND_PAID_WHEEL_SPIN : "res/sounds/paidWheelSpin.mp3",
    SOUND_WHEEL_BONUS : "res/sounds/wheelBonus.mp3",
    SOUND_WHEEL_BONUS_SHORT : "res/sounds/wheelBonusShort.mp3",
    //Sound Files
};