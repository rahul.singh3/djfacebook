/**
 * Created by RNF-Mac11 on 1/23/17.
 */

var BIASING_MODE_85     =       0;
    var BIASING_MODE_92 = 1;
    var BIASING_MODE_97 = 2;
    var BIASING_MODE_140 = 3;
    var BIASING_MODE_300 = 4;

var GameScene = cc.LayerColor.extend({

    reelI : 0,

    autoSpinJustStarted : false,

    advCalled : false,

    callForAdOnWinning : false,

    trickVector : null,

    topSprite : null,

    bottomBar : null,

    retryItem : null,

    analyticsHelper : null,

    tipsLabel : null,

    resizer : null,
    reelsContainer:null,
    haveWild : false,

    canChangeBet : false,

    isSpinningFast : false,

    delegate : null,

    data : null,

    canStop : false,
    didBet : false,
    haveFreeSpins : false,
    touchPressed : false,
    loadingResources : false,

    easyModeCode : 0,//0 = 85%, 1 = 92 %, 2 = 96%, 3 = 140%

    haltSpin : false,

    isAutoSpinning : false,

    isRolling : false,

    excitementChecked : false,
    fadeInOutChked : false,
    haveReversal : false,
    isWildGeneration : false,//for result generation of only
    wildAnimating : false,

    animNodeVec : new Array(),

    activeScreens : 0,

    prevLevel : 0,

    prevLevelPercent : 0,

    percentAddFector : 0.0,

    currentBet : 0,

    freeSpinCount : 0,
    mCode : 0,
    resourceCount : 0,
    totalLines : 0,
    totalProbalities : 0,
    totalProbalitiesWild : 0,

    totalSlots : 0,

    prevReelIndex : null,

    resultArray : null,

    probabilityArray : null,//29 June added for tricky array implementation
    probabilityArraryCheck:null,
    physicalArrayCheck:null,
    probabilityArrayServer:null,
    displayedWin : 0,

    addScore : 0,
    addWin : 0,
    currentWin : 0,
    currentWinForcast : 0,
    displayedScore : 0,
    totalScore : 0,

    drawNode : null,

    vSize : null,

    spinItem : null,
    dealBtn : null,
    buyBtnHalf : null,
    buyBtn : null,
    def : null,

    lineVector : null,

    textureVec : null,

    grandWheelButton : null,

    slots : [ null, null, null, null ],

    keyPressed : false,

    canShowSpecialOfferPopUp : false,

    updatorTime : 0.08,
    dailyChallengeFillBar:null,
    dailyChallenge_Btn:null,
    dailyChanllengeBarBG:null,
    dailyChanllengeTxt:null,

    ctor:function ( lvl ) {
        this._super();

        cc.eventManager.addListener({
            event: cc.EventListener.MOUSE,
            onMouseScroll: function(event){
                throw 0;
            }
        },this);


        //this.preLoadSounds( lvl );

        this.delegate = AppDelegate.getInstance();
        this.preLoadSounds( lvl );
        this.data = ServerData.getInstance();

        //delegate.setChangingScene( true );

        this.setVariables( lvl );

        var listener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                if ( !target.haltSpin ) target.waitAndRemoveBigWinTag(0);
                return true;
            }
        });
        cc.eventManager.addListener( listener, this );



        if ('keyboard' in cc.sys.capabilities) {
            cc.eventManager.addListener({
                event: cc.EventListener.KEYBOARD,
                onKeyPressed: function (key, event) {

                    var self = event.getCurrentTarget();
                    if( key == 32 && !self.keyPressed && !DPUtils.getInstance().keyAlreadyPressed )
                    {
                        self.keyPressed = true;
                        if( !self.isAutoSpinning && !self.haveFreeSpins )
                        {
                            self.spinItem.loadTextures( "spin_pressed.png", "spin_pressed.png", "", ccui.Widget.PLIST_TEXTURE );
                        }
                        self.touchEvent( null, ccui.Widget.TOUCH_BEGAN );
                    }

                },
                onKeyReleased: function (key, event) {
                    var self = event.getCurrentTarget();
                    if( key == 32 && self.keyPressed && !DPUtils.getInstance().keyAlreadyPressed )
                    {
                        self.keyPressed = false;
                        if( !self.isAutoSpinning && !self.haveFreeSpins )
                        {
                            self.spinItem.loadTextures( "spin.png", "spin_pressed.png", "", ccui.Widget.PLIST_TEXTURE );
                        }
                        self.touchEvent( null, ccui.Widget.TOUCH_ENDED );
                    }
                }
            }, this);
        }
        else
        {
            //cc.log("KEYBOARD Not supported");
        }

        return true;
    },
    spacePressed : false,
    /*getKeyStr: function (keycode)
     {
     if (keycode == cc.KEY.none)
     {
     return "";
     }

     for (var keyTemp in cc.KEY)
     {
     if (cc.KEY[keyTemp] == keycode)
     {
     return keyTemp;
     }
     }
     return "";
     },*/
    removepreLoadSounds:function(mach){
        var loadSoundEffect = [soundFiles.Sound_excitement_mp3 ,soundFiles.Sound_coins_mp3,soundFiles.Sound_coin_more_mp3, soundFiles.SOUND_MEGA_WIN];
        for (var index in loadSoundEffect){
            AppDelegate.getInstance().sPlayer.unloadEffect(loadSoundEffect[index]);
        }
        for (var idx =0;idx<15;idx++){

            var soundfileName = "res/sounds/reel_"+(idx+1)+".mp3";
            AppDelegate.getInstance().sPlayer.unloadEffect(soundfileName);

        }
        switch ( mach )
        {
            case DJS_DIAMOND_3X:
                AppDelegate.getInstance().sPlayer.unloadEffect( soundFiles.SOUND_WHEEL );
                AppDelegate.getInstance().sPlayer.unloadEffect( soundFiles.SOUND_WHEEL_BG );
                AppDelegate.getInstance().sPlayer.unloadEffect( soundFiles.SOUND_BONUS_POP );
            case DJS_BONUS_MACHINE:
            case DJS_SPIN_TILL_YOU_WIN:
            case DJS_SPIN_TILL_YOU_WIN_TWIK:
                AppDelegate.getInstance().sPlayer.unloadEffect( soundFiles.SOUND_SPIN_TILL_WIN );
                break;

            case DJS_CRAZY_CHERRY:
            case DJS_DIAMOND:
            case DJS_QUINTUPLE_5X:
            case DJS_DOUBLE_DIAMOND:
                AppDelegate.getInstance().sPlayer.unloadEffect( soundFiles.SOUND_FLASH );
                break;

            case DJS_DIAMONDS_FOREVER:
            case DJS_TRIPLE_PAY:
                AppDelegate.getInstance().sPlayer.unloadEffect( soundFiles.SOUND_SLIDE_1 );
                AppDelegate.getInstance().sPlayer.unloadEffect( soundFiles.SOUND_SLIDE_2 );
                AppDelegate.getInstance().sPlayer.unloadEffect( soundFiles.SOUND_SLIDE_3 );
                break;

            case DJS_FIRE:
                AppDelegate.getInstance().sPlayer.unloadEffect( soundFiles.SOUND_DJS_FIRE );
                break;

            case DJS_FREE_SPINNER:
                AppDelegate.getInstance().sPlayer.unloadEffect( soundFiles.SOUND_SPIN_TILL_WIN );
                AppDelegate.getInstance().sPlayer.unloadEffect( soundFiles.SOUND_FLASH );
                break;

            case DJS_FREE_SPIN:
                AppDelegate.getInstance().sPlayer.unloadEffect( soundFiles.SOUND_FREESPIN );
                AppDelegate.getInstance().sPlayer.unloadEffect( soundFiles.SOUND_FREESPIN_E );
                break;

            case DJS_FREEZE:
                AppDelegate.getInstance().sPlayer.unloadEffect( soundFiles.SOUND_ICE_FREEZE );
                break;

            case DJS_LIGHTNING_REWIND:
            case DJS_LIGHTNING_REWIND_TWIK:
                AppDelegate.getInstance().sPlayer.unloadEffect( soundFiles.SOUND_REVERSAL );
                break;

            case DJS_RESPIN:
                AppDelegate.getInstance().sPlayer.unloadEffect( soundFiles.SOUND_XX_WILD );
                break;

            case DJS_WILD_X:
                AppDelegate.getInstance().sPlayer.unloadEffect( soundFiles.SOUND_WILD );
                break;

        }
    },
    preLoadSounds : function( mach )
    {

        var loadSoundEffect = [soundFiles.Sound_excitement_mp3 ,soundFiles.Sound_coins_mp3,soundFiles.Sound_coin_more_mp3, soundFiles.SOUND_MEGA_WIN];
        for (var index in loadSoundEffect){
            AppDelegate.getInstance().sPlayer.preloadEffect(loadSoundEffect[index]);
        }
        for (var idx =0;idx<15;idx++){

            var soundfileName = "res/sounds/reel_"+(idx+1)+".mp3";
            AppDelegate.getInstance().sPlayer.preloadEffect(soundfileName);

        }
        switch ( mach )
        {
            case DJS_DIAMOND_3X:
                AppDelegate.getInstance().sPlayer.preloadEffect( soundFiles.SOUND_WHEEL );
                AppDelegate.getInstance().sPlayer.preloadEffect( soundFiles.SOUND_WHEEL_BG );
                AppDelegate.getInstance().sPlayer.preloadEffect( soundFiles.SOUND_BONUS_POP );
            case DJS_BONUS_MACHINE:
            case DJS_SPIN_TILL_YOU_WIN:
            case DJS_SPIN_TILL_YOU_WIN_TWIK:
                AppDelegate.getInstance().sPlayer.preloadEffect( soundFiles.SOUND_SPIN_TILL_WIN );
                break;

            case DJS_CRAZY_CHERRY:
            case DJS_DIAMOND:
            case DJS_QUINTUPLE_5X:
            case DJS_DOUBLE_DIAMOND:
                AppDelegate.getInstance().sPlayer.preloadEffect( soundFiles.SOUND_FLASH );
                break;

            case DJS_DIAMONDS_FOREVER:
            case DJS_TRIPLE_PAY:
                AppDelegate.getInstance().sPlayer.preloadEffect( soundFiles.SOUND_SLIDE_1 );
                AppDelegate.getInstance().sPlayer.preloadEffect( soundFiles.SOUND_SLIDE_2 );
                AppDelegate.getInstance().sPlayer.preloadEffect( soundFiles.SOUND_SLIDE_3 );
                break;

            case DJS_FIRE:
                AppDelegate.getInstance().sPlayer.preloadEffect( soundFiles.SOUND_DJS_FIRE );
                break;

            case DJS_FREE_SPINNER:
                AppDelegate.getInstance().sPlayer.preloadEffect( soundFiles.SOUND_SPIN_TILL_WIN );
                AppDelegate.getInstance().sPlayer.preloadEffect( soundFiles.SOUND_FLASH );
                break;

            case DJS_FREE_SPIN:
                AppDelegate.getInstance().sPlayer.preloadEffect( soundFiles.SOUND_FREESPIN );
                AppDelegate.getInstance().sPlayer.preloadEffect( soundFiles.SOUND_FREESPIN_E );
                break;

            case DJS_FREEZE:
                AppDelegate.getInstance().sPlayer.preloadEffect( soundFiles.SOUND_ICE_FREEZE );
                break;

            case DJS_LIGHTNING_REWIND:
            case DJS_LIGHTNING_REWIND_TWIK:
                AppDelegate.getInstance().sPlayer.preloadEffect( soundFiles.SOUND_REVERSAL );
                break;

            case DJS_RESPIN:
                AppDelegate.getInstance().sPlayer.preloadEffect( soundFiles.SOUND_XX_WILD );
                break;

            case DJS_WILD_X:
                AppDelegate.getInstance().sPlayer.preloadEffect( soundFiles.SOUND_WILD );
                break;

        }


    },
      updateProbabiltyArrayOnInApp:function(){
        if (this.mCode == DJS_GOLDEN_SLOT ||this.mCode == DJS_NEON_SLOT || this.mCode == DJS_CRYSTAL_SLOT||this.mCode == DJS_SPIN_TILL_YOU_WIN ) {
            if (this.probabilityArraryCheck && this.probabilityArraryCheck.length == 5) {
                this.probabilityArray = this.probabilityArraryCheck[this.easyModeCode];
            }
        }
      },
    addMegaWinGaf : function ()
    {
        var normal = gaf.Asset.create( res.mega_win_coins_plate_gaf ).createObjectAndRun( true );
        normal.setTag( MEGA_ANIM_WIN_TAG );
        normal.setVisible( false );
        normal.stop();

        this.addChild( normal, 9 );
        normal.setPosition( this.vSize.width / 2 - normal.getContentSize().width / 2, this.vSize.height / 2 + normal.getContentSize().height / 2 );

        var normalText = gaf.Asset.create( res.mega_win_text_gaf ).createObjectAndRun( true );
        normalText.setTag( MEGA_TEXT_WIN_TAG );
        normalText.setVisible( false );
        normalText.stop();
        normalText.setPosition( this.vSize.width / 2 - normalText.getContentSize().width / 2, normal.getPositionY() + normalText.getContentSize().height );
        this.addChild( normalText, 9 );


        var prizeLabel = new CustomLabel( "MEGA WIN!","arial_black", 75, cc.size( normal.getContentSize().width * 0.8, normal.getContentSize().height ) );
        prizeLabel.setPosition( cc.p( normal.getContentSize().width * 0.5, -normal.getContentSize().height * 0.5 ) );
        prizeLabel.setTag( MEGA_ANIM_WIN_TAG );
        normal.addChild( prizeLabel, 9 );
    },
    updateDailyChallengesData:function(winamt,totalbetV){
        if(winamt>0){
            VSDailyChallenge.getInstance().updateUserDailyChallgesRecord(C_WIN_1M_IN_TOTAL_WIN,this.currentWin,0);
            VSDailyChallenge.getInstance().updateUserDailyChallgesRecord(C_WIN_M_IN_TOTAL_WIN,this.currentWin,0);
            VSDailyChallenge.getInstance().updateUserDailyChallgesRecord(C_WIN_200_MORE_SPIN,1,totalbetV);
          if ( winamt >= totalbetV * 25 ) //25
            {
                VSDailyChallenge.getInstance().updateUserDailyChallgesRecord(C_BIG_WIN_TIMES,1,totalbetV);
                VSDailyChallenge.getInstance().updateUserDailyChallgesRecord(C_10X_WIN_TIMES,1,totalbetV);
            }else if ( winamt >= totalbetV * 10 ) //25
            {
                VSDailyChallenge.getInstance().updateUserDailyChallgesRecord(C_10X_WIN_TIMES,1,totalbetV);
            }

        }
    },
    addPlists : function( sender, data )
    {
        var spriteFrameCache = cc.spriteFrameCache;
        spriteFrameCache.removeSpriteFrames();
        spriteFrameCache.addSpriteFrames( "res/" + machinesString[this.mCode - DJS_QUINTUPLE_5X] + "/GamePlist.plist" , "res/" + machinesString[this.mCode - DJS_QUINTUPLE_5X] + "/GamePlist.png" );

        if( this.mCode == DJS_QUINTUPLE_5X || this.mCode == DJS_X_MULTIPLIERS )
        {
            spriteFrameCache.addSpriteFrames( "res/DJSQuintuple5x/elements.plist", "res/DJSQuintuple5x/elements.png" );
        }

        spriteFrameCache.addSpriteFrames( res.FBPopUp_Lobby_plist, res.FBPopUp_Lobby_png );
        spriteFrameCache.addSpriteFrames( "res/GameButtons/GameButtons_1_18.plist", "res/GameButtons/GameButtons_1_18.png" );
        spriteFrameCache.addSpriteFrames( "res/GameButtons/LevelUpPop_1_18.plist", "res/GameButtons/LevelUpPop_1_18.png" );
        spriteFrameCache.addSpriteFrames( "res/setting_popup/Settings.plist", "res/setting_popup/Settings.png");
        spriteFrameCache.addSpriteFrames( "res/tournament/Tournament.plist", "res/tournament/Tournament.png");
        spriteFrameCache.addSpriteFrames( "res/dailychallenge/DailyChallengePlist.plist", "res/dailychallenge/DailyChallengePlist.png");
    },

    autoSpinCB: function( dt )
    {
        if ( this.isAutoSpinning && !this.haveReversal )
        {
            if ( this.totalScore >= this.currentBet * this.totalSlots )
            {
                var spins = this.def.TOTAL_PLAYED;
                this.def.TOTAL_PLAYED = spins + 1;
                this.surpriseTimeDealy=0.5;
                this.isWinningOccured = false;
                this.spin();
                var percentOfbar = VSDailyChallenge.getInstance().getStatusOfCurrentDailyChallenge();
                this.dailyChallengeFillBar.setPercentage(percentOfbar);
                this.delegate.setLevel( this.def.getValueForKey(LEVEL), CSUserdefauts.getInstance().getValueForKey(THIS_LEVEL_BET, 0) + this.currentBet, this );
            }
            else
            {
                //this.def.setValueForKey( CHIPS, this.totalScore );//change 12 Dec
                userDataInfo.getInstance().totalChips = this.totalScore;
                this.setInsufficientPage();
                this.removeAutoSpinning();
            }
        }
    },

    buyMenuCB : function( menuItm )
    {
        /*if ( delegate.getChangingScene() ) {
         return;
         }*/
        var tag = menuItm.getTag();

        switch ( tag )
        {
            case CLOSE_BTN_TAG:
                //if ( this.getParent() && !this.getParent().getChildByTag( LOADER_TAG ) )
            {
                var moveImage = this.getChildByTag(OVERLAPPING_LAYER_TAG).getChildByTag(easyMoveInTag);
                if (moveImage){
                    DPUtils.getInstance().easyBackInAnimToPopup(moveImage,0.3);
                    this.runAction(new cc.Sequence( new cc.DelayTime(0.3),cc.callFunc( function ( ){
                        this.removeChildByTag(OVERLAPPING_LAYER_TAG, true);
                        this.drawNode.clear();
                    },this)));

                }else {
                    this.removeChildByTag( OVERLAPPING_LAYER_TAG );
                    this.drawNode.clear();
                }

                this.delegate.jukeBoxStop( S_LEVEL_UP );

            }
                break;

            default:
                //if ( !this.getParent().getChildByTag( LOADER_TAG ) )
                IAPHelper.getInstance().purchase( tag, BuyPageTags.BUY_CREDITS );
                break;
        }

        this.delegate.jukeBox( S_BTN_CLICK );
    },

    canStopCB : function( dt )
    {
        this.canStop = true;
    },

    checkAndSetEasyModeCode : function()
    {
        switch ( this.def.getValueForKey( MACHINE_TUNNING, 0 ) ) {
            case 1:
                if ( this.easyModeCode != BIASING_MODE_85 )
                {
                    this.easyModeCode = BIASING_MODE_85;//85 %
                    this.changeTotalProbabilities();
                }
                break;

            case 2:
                if ( this.easyModeCode != BIASING_MODE_92 )
                {
                    this.easyModeCode = BIASING_MODE_92;//92 %
                    this.changeTotalProbabilities();
                }
                break;

            default:
                if ( CSUserdefauts.getInstance().getValueForKey( EASY_MODE_BET, 0 ) <= 0 && this.easyModeCode != BIASING_MODE_97 )
                {
                    this.easyModeCode = BIASING_MODE_97;//95 %
                    this.changeTotalProbabilities();
                }
                else if ( CSUserdefauts.getInstance().getValueForKey( EASY_MODE_BET, 0 ) > 0 && this.easyModeCode != BIASING_MODE_140 )
                {
                    this.easyModeCode = BIASING_MODE_140;//140 %
                    this.changeTotalProbabilities();
                }
                break;
        }

        /*if( this.mCode == ServerCommunicator.getInstance().TRICKY_MACHINE_CODE )
         {
         cc.log( "from Server" );
         }
         else
         {
         cc.log( "from local" );
         }
         cc.log( "current mode = " + this.easyModeCode );
         cc.log( "current array = " + this.probabilityArray[this.easyModeCode] );*/
    },

    checkAndSetAPopUp : function()
    {
        var serverDataObj = ServerData.getInstance();
        var totalbet = this.currentBet;
        if (this.mCode === DJS_X_MULTIPLIERS){
            totalbet = this.currentBet*4;
        }
        if ( this.delegate.showLevelUp )
        {
            this.setLevelUpNotification();
        }
        serverDataObj.updateReelSpin(this.currentWin,this.easyModeCode,this.currentWin/totalbet >= 10,function (valueMap) {
            if (valueMap && valueMap["result"] === 1) {
                var machine_tuning = valueMap["machine_tuning"];
                UserDef.getInstance().setValueForKey(MACHINE_TUNNING , machine_tuning );
             /*   var  piggy_bank_map =  valueMap["piggy_bank"];
                if (piggy_bank_map && piggy_bank_map.length>0){
                    userDataInfo.getInstance().piggy_bank_id=piggy_bank_map["product_id"] ;
                    userDataInfo.getInstance().piggy_bank_coins=piggy_bank_map["coins"] ;
                    userDataInfo.getInstance().piggy_bank_max_coins=piggy_bank_map["max_coins"] ;
                }*/

            }
        },totalbet,false);

        if( !this.isAutoSpinning && this.delegate.showMachineUnlocked && !this.delegate.haveHappyHoursRunning )
        {
            PopUps.getInstance().addPopUpIndex(PopUpTags.STAY_IN_TOUCH_MACHINE_UNLOCK);
        }
        this.checkForSpecialOfferPopUp();
        if(  this.canShowSpecialOfferPopUp)
        {
            this.canShowSpecialOfferPopUp = false;
            PopUps.getInstance().addPopUpIndex(PopUpTags.STAY_TOUCH_SPECIAL_OFFER_POP);
        }
        this.setupBribePopup();

        if (PopUps.getInstance().listOfPopUps.length>0 && this.currentWin<this.currentBet*this.totalSlots*25){
            PopUps.getInstance().showPopUpAccToIndex(PopUps.getInstance().listOfPopUps[0]);
        }

    },

    setMachineUnlockedPopUp : function()
    {
        this.delegate.showMachineUnlocked = false;
        var layer = new cc.Layer();//Color( cc.color( 0, 0, 0, 166 ) );
        layer.setTag( OVERLAPPING_LAYER_TAG );
        this.addChild( layer, 10 );
        DPUtils.getInstance().setTouchSwallowing( null, layer );

        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        layer.addChild( darkBG );

        var buySprite = new cc.Sprite( "#mach_unlock_pop.png" );
        buySprite.setPosition( cc.p( this.vSize.width / 2, this.vSize.height / 2 ) );
        layer.addChild( buySprite );

        //cc.log( "#mach_unlock_pop" + (iconsDefaultArrangementVec[this.delegate.unlockedMachineCode] - DJS_QUINTUPLE_5X + 1) + ".png" );
        var machineSprite = new cc.Sprite( "#mach_unlock_pop" + (iconsDefaultArrangementVec[this.delegate.unlockedMachineCode] - DJS_QUINTUPLE_5X + 1) + ".png" );
        machineSprite.setPosition( machineSprite.getContentSize().width * 0.8, buySprite.getContentSize().height * 0.5 - 10 );
        buySprite.addChild( machineSprite );

        var buyMenu = new cc.Menu();

        buyMenu.setPosition( cc.p( 0, 0 ) );
        buySprite.addChild( buyMenu );


        /*var closeItem = new cc.MenuItemImage( res.Close_Button_2_png, res.Close_Button_2_png, this.setMachineUnlockedPopUpMenuCB, this );

         closeItem.setPosition( cc.p( buySprite.getContentSize().width - closeItem.getContentSize().width / 2, buySprite.getContentSize().height - closeItem.getContentSize().height / 2 ) );
         closeItem.setTag( CLOSE_BTN_TAG );

         closeItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );

         buyMenu.addChild( closeItem );*/

        var shareItem = new cc.MenuItemImage( res.share_bg_png, res.share_bg_png, this.setMachineUnlockedPopUpMenuCB, this );
        //this.resizer.setFinalScaling( shareItem );
        shareItem.setPosition( shareItem.getContentSize().width * 0.7, shareItem.getContentSize().height * 0.75 );
        shareItem.setTag( SHARE_BTN_TAG );

        var tick = new cc.Sprite( res.tick_png );
        tick.setPosition( cc.p( tick.getContentSize().width * 0.75, tick.getContentSize().height * 0.75 ) );
        tick.setTag( 1 );
        shareItem.addChild( tick );

        if( ServerData.getInstance().isShareEnabled[SHARE_MACHINE_UNLOCKED ] )//change 12 May
        {
            buyMenu.addChild( shareItem );
        }

        var closeItem = new cc.MenuItemImage( res.levelUpOk_png, res.levelUpOk_png, this.setMachineUnlockedPopUpMenuCB, this );

        closeItem.setPosition( cc.p( buySprite.getContentSize().width * 0.7, shareItem.getPositionY() ) );
        closeItem.setTag( CLOSE_BTN_TAG );

        closeItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );

        buyMenu.addChild( closeItem );

        this.resizer.setFinalScaling( buySprite );
    },

    setMachineUnlockedPopUpMenuCB : function( ref )
    {
        var menuItm = ref;
        var tag = menuItm.getTag();

        switch ( tag )
        {
            case CLOSE_BTN_TAG:
                this.delegate.showingScreen = false;
                menuItm.getParent().getParent().getParent().removeFromParent( true );
                PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_IN_TOUCH_MACHINE_UNLOCK);
                var shareBtn = menuItm.getParent().getChildByTag( SHARE_BTN_TAG );
                if (shareBtn && shareBtn.getChildByTag(1)) {
                    if (shareBtn.getChildByTag(1).isVisible()) {
                        //FacebookObj.shareOnTimeLine( APP_NAME, "https://www.facebook.com/doublejackpotslots/?fref=ts&ref=br_tf",
                        //"https://7starslots.com/facebook_inapp_verification/doublejackpot/coin.png", "Machine Unlocked", "New Machine Unlocked" );
                        FacebookObj.shareOnTimeLine(FacebookObj.userFbName + " unlocked a new machine!", "https://apps.facebook.com/doublejackpotslots/?fb_source=feed",
                            "https://7starslots.com/facebook_inapp_verification/doublejackpot/share_folder/" +
                            (iconsDefaultArrangementVec[this.delegate.unlockedMachineCode] - DJS_QUINTUPLE_5X + 1) + ".jpg", "Double Jackpot Slots!", "Play Classic 3 reel slot machines on Double Jackpot Slots! Join me now!");//change 12 May //#Jitu
                    }
                }
                break;

            default:
                this.delegate.showingScreen = false;
                menuItm.getChildByTag( 1 ).setVisible( !menuItm.getChildByTag( 1 ).isVisible() );
                PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_TOUCH_SPECIAL_OFFER_POP);
                break;
        }
    },
    setupDailyChanllengeUI:function(){

        this.dailyChallenge_Btn = new ccui.Button(res.dc_bg_icon );
        this.dailyChallenge_Btn.setPosition(cc.p(this.vSize.width-this.dailyChallenge_Btn.getContentSize().width/2, this.vSize.height-this.dailyChallenge_Btn.getContentSize().height*0.8));
        var moveXPos = this.dailyChallenge_Btn.getContentSize().width/2;
        this.dailyChallenge_Btn.setTag(DAILY_CHALLENGE_BONUS_CLAIM_TAG);
       // this.dailyChallenge_Btn.runAction(new cc.MoveTo(0.5,cc.p(this.vSize.width-moveXPos,this.dailyChallenge_Btn.getPositionY())));
        this.addChild(this.dailyChallenge_Btn,5);
        // var dailyChallengebar = new cc.Sprite(res.dc_bg_icon);
        // dailyChallengebar.setPosition(this.dailyChallenge_Btn.getContentSize()/2);
        // this.dailyChallenge_Btn.addChild(dailyChallengebar);

        this.dailyChallengeFillBar = new cc.ProgressTimer(new cc.Sprite(res.dc_fill_icon));
        this.dailyChallengeFillBar.setPosition( cc.p( this.dailyChallenge_Btn.getContentSize().width / 2, this.dailyChallenge_Btn.getContentSize().height / 2 ) );
        this.dailyChallengeFillBar.setType(cc.ProgressTimer.TYPE_RADIAL);
        this.dailyChallengeFillBar.setPercentage(0);
        this.dailyChallenge_Btn.addChild(this.dailyChallengeFillBar,1);
        var percentOfbar = VSDailyChallenge.getInstance().getStatusOfCurrentDailyChallenge();
        this.dailyChallengeFillBar.setPercentage(percentOfbar);
        //var updateProgressBar  =  new cc.ProgressFromTo(0.5,0,percentOfbar);
        //this.dailyChallengeFillBar.runAction(updateProgressBar);
        var dc_gift_anim = gaf.Asset.create( res.dc_gift_anim_gaf).createObject();
        dc_gift_anim.setPosition(0 ,this.dailyChallenge_Btn.getContentSize().height);
        this.dailyChallenge_Btn.addChild(dc_gift_anim,1);
        dc_gift_anim.setTag(5); // normal ,fast

        var isAchived = VSDailyChallenge.getInstance().isMileStoneAchived();
        dc_gift_anim.playSequence(isAchived ? "fast" : "normal" ,true);
        dc_gift_anim.start();
        this.dailyChallenge_Btn.addTouchEventListener(function (ref,type) {
            if(type == ccui.Widget.TOUCH_ENDED  && !ref.getParent().getChildByTag(BIG_WIN_LAYER_TAG)){
                ref.getParent().showDCPopUp();
            }

        },this);

        this.dailyChanllengeBarBG = new cc.Sprite(res.dc_dc_result_bar);
        var clipper = new cc.ClippingNode();
        clipper.setContentSize( this.dailyChanllengeBarBG.getContentSize() );
        clipper.setAnchorPoint(  cc.p(0.5, 0.5) );
        clipper.setPosition(cc.p(this.dailyChallenge_Btn.getContentSize().width*0.50-this.dailyChanllengeBarBG.getContentSize().width/2,this.dailyChanllengeBarBG.getContentSize().height*0.60) );
        this.dailyChallenge_Btn.addChild(clipper,-1);
        var solidRectStencil = new cc.DrawNode();
        //solidRectStencil.drawRect()
        var pos = cc.p(this.dailyChanllengeBarBG.getContentSize().width,this.dailyChanllengeBarBG.getContentSize().height);
        solidRectStencil.drawRect(cc.p(0, 0),pos,cc.color.MAGENTA);
        clipper.setStencil(solidRectStencil);

        this.dailyChanllengeBarBG.setPosition(cc.p(1.5*this.dailyChanllengeBarBG.getContentSize().width, clipper.getContentSize().height/2));
        clipper.addChild(this.dailyChanllengeBarBG);
        this.dailyChanllengeTxt = new CustomLabel( "daily challenge task heading", "HELVETICALTSTD-BOLD", 50, new cc.size( this.dailyChanllengeBarBG.getContentSize().width*0.8, this.dailyChanllengeBarBG.getContentSize().height*0.8 ) );

        this.dailyChanllengeTxt.setAnchorPoint(cc.p(0,0.5));
        this.dailyChanllengeTxt.setPosition(this.dailyChanllengeBarBG.getContentSize().width*0.05,this.dailyChanllengeBarBG.getContentSize().height/2);
        this.dailyChanllengeBarBG.addChild(this.dailyChanllengeTxt, 2);
        this.dailyChallenge_Btn.setScale(0.6);
    },
    showDCPopUp:function(){
        var child = new DaliyChallenge( PopUpTags.STAY_IN_TOUCH_DC );
        this.addChild( child, 20 );
        DPUtils.getInstance().setTouchSwallowing( 100, child );
        var dc_gift_anim = this.dailyChallenge_Btn.getChildByTag(5);
        if (dc_gift_anim){
            dc_gift_anim.stop();
            dc_gift_anim.playSequence("normal" ,true);
            dc_gift_anim.start();
        }

        if(this.dailyChallengeFillBar.getPercentage() >= 100){
            this.dailyChallengeFillBar.setPercentage(0);
        }
    },
    showDailyTaskCompleteBarStatus:function( heading){
        this.dailyChanllengeTxt.setString(heading);
        this.dailyChanllengeBarBG.stopAllActions();
        this.dailyChanllengeBarBG.setPositionX(1.5*this.dailyChanllengeBarBG.getContentSize().width);
        this.dailyChanllengeBarBG.runAction( new cc.Sequence( new cc.moveTo( 0.5,cc.p(this.dailyChanllengeBarBG.getContentSize().width/2, this.dailyChanllengeBarBG.getPositionY()) ), new cc.DelayTime(2.0 ), new cc.moveTo( 0.5,cc.p(1.5*this.dailyChanllengeBarBG.getContentSize().width, this.dailyChanllengeBarBG.getPositionY()) ) ,cc.callFunc( function() {//lambda

            }
            , this ) ) );
    // start animation
    var dc_gift_anim = this.dailyChallenge_Btn.getChildByTag(5);
    var isAchived = VSDailyChallenge.getInstance().isMileStoneAchived();
    dc_gift_anim.stop();
    dc_gift_anim.playSequence(isAchived ? "fast" : "normal" ,true);
    dc_gift_anim.start();
    var updateProgressBar  = new cc.ProgressFromTo(0.5,this.dailyChallengeFillBar.getPercentage(),100);
    this.dailyChallengeFillBar.runAction(updateProgressBar);
},
    checkForFalseWin : function()
    {
        var isCallCalculatePayment = false;

        if ( this.data.server_false_win )
        {
            if ( Math.floor( Math.random() * this.data.server_false_win ) == 0 )
            {
                //this.calculatePayment( false );

                if ( this.currentWinForcast <= 0 )
                {
                    this.setFalseWin();
                    //this.falseWinLabel.setVisible( true );
                }
            }
        }
        if (isCallCalculatePayment){
            this.winningControl(isCallCalculatePayment);
        }
    },
    winningControl:function(isCallCalculatePayment){

    },
    setBet : function ( bet )
    {
        var totalBet = this.def.getValueForKey(BET, 0);
        totalBet+=bet;

        this.def.setValueForKey(BET, totalBet);

        totalBet = this.def.SESSION_BET;
        totalBet+=bet;
        this.def.SESSION_BET = totalBet;
    },

    deductBet : function( betAmt )
    {
        this.totalScore-=betAmt;
        this.setBet( betAmt );
        userDataInfo.getInstance().totalChips = this.totalScore;

        CSUserdefauts.getInstance().setValueForKey("lastPlayMachineId",this.mCode);
        VSDailyChallenge.getInstance().updateUserDailyChallgesRecord(C_PLAY_1000_SPINS,1,betAmt);

        //analyticsHelper.setLastBetPlaced( betAmt );

        var maxBet = this.def.getValueForKey( MAX_BET_PLACED, 0 );

        if ( maxBet < betAmt ) {
            this.def.setValueForKey( MAX_BET_PLACED, betAmt );
        }

       // DailyChallenges.getInstance().updateTaskData( C_BET_ITERATIONS, 1 );
       // DailyChallenges.getInstance().updateTaskData( C_BET_AMOUNT, betAmt );

        if ( this.def.getValueForKey( BET, 0 ) > CSUserdefauts.getInstance().getValueForKey( MINIMUM_EASY_MODE_BET_LIMIT, 0 ) && CSUserdefauts.getInstance().getValueForKey( EASY_MODE_BET_300, 0 ) <= 0 )
        {
            this.checkAndSetEasyModeCode();
        }

        if ( CSUserdefauts.getInstance().getValueForKey( EASY_MODE_BET_300, 0 ) > 0 )
        {
            if( this.currentBet >= 100000 || ( CSUserdefauts.getInstance().getValueForKey( BET_LOWER_LIMIT ) && this.currentBet < CSUserdefauts.getInstance().getValueForKey( BET_LOWER_LIMIT ) ) )//Change Raghib Oct 30 2017
            {
                if( this.easyModeCode != BIASING_MODE_97 )
                {
                    this.easyModeCode = BIASING_MODE_97;
                    this.changeTotalProbabilities();
                }
            }
            else
            {
                if( this.easyModeCode != BIASING_MODE_300 )
                {
                    this.easyModeCode = BIASING_MODE_300;// 300 %
                    this.changeTotalProbabilities();
                }

                CSUserdefauts.getInstance().setValueForKey( EASY_MODE_BET_300, CSUserdefauts.getInstance().getValueForKey( EASY_MODE_BET_300, 0 ) - betAmt );

                if( CSUserdefauts.getInstance().getValueForKey( EASY_MODE_BET_300, 0 ) <= 0 )
                {
                    CSUserdefauts.getInstance().setValueForKey( EASY_MODE_WIN_CHIPS_300, -1 );
                    this.checkAndSetEasyModeCode();
                }
            }
        }
        else if ( CSUserdefauts.getInstance().getValueForKey( EASY_MODE_BET, 0 ) > 0 )
        {
            CSUserdefauts.getInstance().setValueForKey( EASY_MODE_BET, CSUserdefauts.getInstance().getValueForKey( EASY_MODE_BET, 0 ) - betAmt );
        }
        /*if ( this.def.getValueForKey( EASY_MODE_BET_300, 0 ) > 0 )
        {
            if( this.easyModeCode != 4 )
            {
                this.easyModeCode = 4;// 300 %
                this.changeTotalProbabilities();
            }

            this.def.setValueForKey( EASY_MODE_BET_300, this.def.getValueForKey( EASY_MODE_BET_300, 0 ) - betAmt );

            if( this.def.getValueForKey( EASY_MODE_BET_300, 0 ) <= 0 )
            {
                this.def.setValueForKey( EASY_MODE_WIN_CHIPS_300, -1 );
                this.checkAndSetEasyModeCode();
            }
        }*///#Change Nov 20 2017


        if ( this.easyModeCode != BIASING_MODE_85 && this.totalScore >= CHIPS_CAP_LIMIT && this.def.getValueForKey( MACHINE_TUNNING, 0 ) != 10 )//change 12 Dec raghib
        {
            this.easyModeCode = BIASING_MODE_85;// 85 %
            this.changeTotalProbabilities();
        }

        /*if ( this.def.getValueForKey( EASY_MODE_BET, 0 ) > 0 )
        {
            this.def.setValueForKey( EASY_MODE_BET, this.def.getValueForKey( EASY_MODE_BET, 0 ) - betAmt );
        }*///#Change Nov 20

        if ( !this.didBet )
        {
           // DailyChallenges.getInstance().updateTaskData( C_DIFFERENT_SLOTS, 1, this.mCode - DJS_QUINTUPLE_5X );
            this.didBet = true;
        }

        if( ServerData.getInstance().biasing_status==1 )
        {
            var lbl = this.getChildByTag( 2100 );//hack
            if( lbl )//hack
            {
                this.delegate.spinCount++;
                switch ( this.easyModeCode )
                {
                    case 0:
                        lbl.setString( "EasyMode = " + this.easyModeCode + "\n 85% \n spin count  = " + this.delegate.spinCount );
                        break;

                    case 1:
                        lbl.setString( "EasyMode = " + this.easyModeCode + "\n 92% \n spin count  = " + this.delegate.spinCount );
                        break;

                    case 2:
                        lbl.setString( "EasyMode = " + this.easyModeCode + "\n 97% \n spin count  = " + this.delegate.spinCount );
                        break;

                    case 3:
                        lbl.setString( "EasyMode = " + this.easyModeCode + "\n 140% \n spin count  = " + this.delegate.spinCount );
                        break;

                    case 4:
                        lbl.setString( "EasyMode = " + this.easyModeCode + "\n 300% \n spin count  = " + this.delegate.spinCount );
                        break;
                }
            }
            var lblArray = this.getChildByTag( 210111 );//hack
            if (lblArray) {
                var arrayData=null;
                if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode ){
                    if (this.mCode == DJS_GOLDEN_SLOT ||this.mCode == DJS_NEON_SLOT || this.mCode == DJS_CRYSTAL_SLOT||this.mCode == DJS_SPIN_TILL_YOU_WIN )
                         arrayData = this.probabilityArray;
                     else
                        arrayData = this.probabilityArray[this.easyModeCode];


                }else{
                    arrayData = this.probabilityArray[this.easyModeCode];

                }
                if (arrayData && arrayData.length>0){
                    var p1 = arrayData.slice(0,arrayData.length/2);
                    var p2 = arrayData.slice(arrayData.length/2);
                    lblArray.setString(p1+"\n"+p2);
                }

            }

        }
        //cc.log( "this.easyModeCode = " + this.easyModeCode );

        /*if( this.mCode == ServerCommunicator.getInstance().TRICKY_MACHINE_CODE )
         {
         cc.log( "from Server" );
         }
         else
         {
         cc.log( "from local" );
         }
         cc.log( "current mode = " + this.easyModeCode );
         cc.log( "current array = " + this.probabilityArray[this.easyModeCode] );*/

        this.grandWheel.updateGrandWheelData( betAmt );
        ServerData.getInstance().updateCustomDataOnServer();

    },

    extractArrayFromSring : function( code, str ) {
        var arrayString = str;

        var numString = "";

        var i = 0;

        var a = 0;
        var pEnd;
 if (arrayString){
        while (1) {
            if (arrayString[i] == ',' || i >= arrayString.length) {
                if (numString.length) {
                    this.probabilityArray[code][a] = parseInt(numString);
                } else {
                    break;
                }
                numString = "";
                a++;
            } else {
                numString = numString + arrayString[i];
            }

            if (i >= arrayString.length) {
                break;
            }
            i++;
        }
    }
    },

    getRandomIndex : function( index, doMoreRandom )
    {
        if ( doMoreRandom )
        {
            if ( Math.floor( Math.random() * 2 ) )
            {
                index++;
            }
            else
            {
                index--;
            }
        }

        if ( index >= 36 )
        {
            index = 0;
        }
        else if ( index < 0 )
        {
            index = 35;
        }

        return index;
    },
    removeDC:function(){
        AppDelegate.getInstance().showingScreen = false;
        this.removeChildByTag(PopUpTags.STAY_IN_TOUCH_DC);
    },
    isMoveToLobby:false,
    isWinningOccured:false,
    menuCB : function( pSender )
    {
        /*if ( this.def.MUSIC, true )
         {
         if ( !this.delegate.sPlayer.isBackgroundMusicPlaying() )
         {
         this.delegate.jukeBox( S_AMBIENCE );
         }
         }*/

        /*if ( delegate.getChangingScene() ) {
         return;
         }*/
        if (this.isMoveToLobby  || this.isAutoSpinning ||  this.loadingResources || this.haveReversal || this.wildAnimating || this.getChildByTag( OVERLAPPING_LAYER_TAG ) || this.haveFreeSpins || this.isRolling || this.getChildByTag(BIG_WIN_LAYER_TAG))
        {
            return;
        }

        var node = pSender;
        var scene = null;

        if ( !this.touchPressed )
        {
            this.removeAutoSpinning();
        }

        var betIndx = 0;

        for ( var i = 0; i < this.betRangeVec.length; i++ )
        {
            if ( this.currentBet == this.betRangeVec[ i ] )
            {
                betIndx = i;
                break;
            }
        }

        switch ( node.getTag() )
        {
            case INFO_BTN_TAG:
                //scene = new cc.Scene();
                var child = new InfoScene( this.mCode );
                this.addChild( child, 20 );
                DPUtils.getInstance().setTouchSwallowing( 100, child );
                //cc.director.pushScene( scene );
                break;

            case SETTING_BTN_TAG:
                this.setSettingPage();
                break;

            case LOBBY_BTN_TAG:
            {
                if (this.isWinningOccured){
                    return;
                }
                this.isMoveToLobby = true;
                this.removepreLoadSounds(this.mCode);
                this.delegate.jukeBoxStop(S_STOP_EFFECTS);
                ServerData.getInstance().updateCustomDataOnServer();
                PopUps.getInstance().listOfPopUps =[];
                userDataInfo.getInstance().totalChips = this.totalScore;
                cc.director.runScene( new HelloWorldScene() );
            }
                break;

            case BUY_BTN_TAG:
                //this.def.setValueForKey(CHIPS, this.totalScore);
                userDataInfo.getInstance().totalChips = this.totalScore;
                switch ( this.data.server_buy_page )
                {
                    default:
                        this.setBuyPage(BuyPageTags.BUY_CREDITS);
                        break;

                    case 2:
                        if (this.def.getValueForKey(IS_BUYER, false)) {
                            this.setBuyPage(BuyPageTags.BUY_CREDITS);
                        }
                        else {
                            this.setBuyPage(BuyPageTags.FIRST_TIME_CREDITS);
                        }
                        break;
                    case 3:
                    {
                        this.setBuyPage(BuyPageTags.DOUBLE_CREDITS);
                    }
                        break;
                    case 4:
                    {
                        this.setBuyPage(BuyPageTags.OFFER_CREDITS);

                    }
                        break;
                }
                this.def.setValueForKey( BUY_BUTTON_CLICK_COUNT, this.def.getValueForKey( BUY_BUTTON_CLICK_COUNT, 0 ) + 1 );
                break;

            case BET_UP_BTN_TAG:
                if ( betIndx < this.betRangeVec.length - 1 && this.canChangeBet &&  this.isCheckUnlockBet(this.betRangeVec[ betIndx + 1 ]) )
                {
                    this.currentBet = this.betRangeVec[ betIndx + 1 ];
                    this.setBetLabel();
                }
                break;

            case BET_DOWN_BTN_TAG:
                if ( betIndx > 0 && this.canChangeBet  &&   this.isCheckUnlockBet(this.betRangeVec[ betIndx - 1 ]))
                {
                    this.currentBet = this.betRangeVec[ betIndx - 1 ];
                    this.setBetLabel();
                }
                break;

            case BET_MAX_BTN_TAG:
                if ( /*!this.isScheduled( this.updateWinCoin, this ) &&*/ this.canChangeBet )
                {
                   // this.isScheduled( this.updateWinCoin, this );
                    var betIndex = this.getMaxBetIndexAccordToLevel();
                    if ( this.isCheckUnlockBet(this.betRangeVec[ betIndex])) {
                        this.currentBet = this.betRangeVec[ betIndex ];
                        this.setBetLabel();
                    }

                }
                break;

            case CLOSE_BTN_TAG:
                if ( !this.getParent().getChildByTag( LOADER_TAG ) )
                {
                    this.drawNode.clear();
                    this.removeChildByTag( VIP_LAYER_TAG );
                }
                break;

            case DEAL_BTN_TAG:
            {
                var deal = AppDelegate.getInstance().getDealInstance();//DJSDeal.getInstance();
                if ( deal.canSetDeal && deal.getNumberOfRunningActions() == 0 )
                {
                    if ( deal.flushDeal( true ) )
                    {
                        this.addChild( deal, 5 );
                        DPUtils.getInstance().setTouchSwallowing( null, deal );
                    }
                }
            }
                break;

            case FULL_SCREEN_BTN_TAG:

                DPUtils.getInstance().toggleFullScreen( pSender, true );//change 11 May
                break;

            case SHARE_BTN_TAG:
                this.removeMegaWinAnimation();
                if(FacebookObj){
                    FacebookObj.shareOnTimeLine( FacebookObj.userFbName + " got a Mega Win!!","https://apps.facebook.com/doublejackpotslots/?fb_source=feed",
                        "https://7starslots.com/facebook_inapp_verification/doublejackpot/share_folder/mega_win.jpg", "Double Jackpot Slots Mega Win!", "Try your luck at the Double Jackpot Slot Machines! The slot reels are on Fire!"  );//#Jitu
                }
                break;

            case GRANG_WHEEL_BUTTON_TAG:
                this.grandWheel.checkAndSetGrandWheelOnParent();
                break;

            default:
                break;
        }

        if ( this.canChangeBet && ( node.getTag() == BET_DOWN_BTN_TAG || node.getTag() == BET_UP_BTN_TAG || node.getTag() == BET_MAX_BTN_TAG  ) )
        {
            this.setCurrentTournament();

            if( this.betIndicator )
            {
                this.betIndicator.setVisible( true );
                if( this.currentBet >= 10000 )
                {
                    this.betIndicator.setSpriteFrame( "enough.png" );//setTexture( "enough.png" );
                }
                else
                {
                    this.betIndicator.setSpriteFrame( "not_enough.png" );//setTexture( "not_enough.png" );
                }
            }
        }
        this.delegate.jukeBox( S_BTN_CLICK );
    },

    removeAnimations : function()
    {
        var fire;
        for ( var i = 0; i < this.animNodeVec.length; i++ )
        {
            fire = this.animNodeVec[ i ];
            fire.removeFromParent( true );
        }
        this.animNodeVec = [];
    },

    reelStopper : function( dt )
    {
        this.activeScreens-=this.slots[this.totalSlots - this.activeScreens].stopARoll( dt );
        if ( this.activeScreens <= 0 )
        {
            this.unschedule( this.reelStopper );
        }
    },

    removeAutoSpinning : function()
    {
        this.isAutoSpinning = false;
        this.spinItem.loadTextures( "spin.png", "spin_pressed.png", "", ccui.Widget.PLIST_TEXTURE );
        var glowSprite = this.spinItem.getChildByTag( GLOW_SPRITE_TAG );
        glowSprite.setTexture( res.glow_png );
        this.unschedule( this.autoSpinCB );
    },

    removeMegaWinAnimation : function()
    {
        if( this.getChildByTag( MEGA_WIN_TAG ) )
        {
            this.removeChildByTag( MEGA_WIN_TAG );

            var normal = this.getChildByTag( MEGA_ANIM_WIN_TAG );
            if( normal )
            {
                normal.setVisible( false );
                normal.stop();

                normal = this.getChildByTag( MEGA_TEXT_WIN_TAG );
                normal.setVisible( false );
                normal.stop();
            }
        }
    },

    rescheduleStopper : function()
    {
        if ( this.mCode == DJS_X_MULTIPLIERS )
        {
            this.schedule( this.reelStopper, 0.15 );
        }
        else {
            this.schedule(this.reelStopper, 0.4);
        }
    },

    setAppRatingPopUp : function()
    {
        if( this.getChildByTag( STAY_IN_TOUCH_POP_TAG ) )
        {
            return;
        }
        var drawNode = new cc.DrawNode();
        drawNode.setPosition( cc.p( 0, 0 ) );
        drawNode.setTag( STAY_IN_TOUCH_POP_TAG );

        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        drawNode.addChild( darkBG );

        var popBGSprite = new cc.Sprite( "res/Bribe_pop/feedback_bg.png" );
        popBGSprite.setPosition( cc.p( this.vSize.width * 0.5, this.vSize.height / 2 ) );
        drawNode.addChild( popBGSprite );

        var menu = new cc.Menu();
        menu.setPosition( cc.p(0,0) );
        popBGSprite.addChild( menu );

        for ( var i = 0; i < 5; i++ )
         {
         var starItem = new cc.MenuItemImage( "res/Bribe_pop/emoji/"+(i+1)+"_disabled.png","res/Bribe_pop/emoji/"+(i+1)+"_disabled.png",
         this.starMenuCB, this );
         starItem.setPosition( cc.p( popBGSprite.getContentSize().width*0.12*i+popBGSprite.getContentSize().width*0.27, popBGSprite.getContentSize().height*0.48 ) );
         starItem.setTag( i+1 );
         menu.addChild(starItem);
         var starSprite = new cc.Sprite( "res/Bribe_pop/emoji/"+(i+1)+".png" );
         starSprite.setPosition( starItem.getPosition() );
         popBGSprite.addChild( starSprite  );
         starSprite.setVisible( false );
         starSprite.setTag( i+1);

         }

         var submitItem = new cc.MenuItemImage( "res/Bribe_pop/rate_us_button.png", "res/Bribe_pop/rate_us_button.png",
         this.starMenuCB, this );
         submitItem.setPosition( cc.p( popBGSprite.getContentSize().width*0.5 ,  popBGSprite.getContentSize().height * 0.12 ) );
         submitItem.setTag( ALLOW_BTN_TAG );
         menu.addChild( submitItem );
         //showLikeUsBtn( true );
        var closeItem = new cc.MenuItemImage( res.rating_close_btn, res.rating_close_btn,
            this.starMenuCB, this );
        closeItem.setScale( 0.75 );
        closeItem.setPosition(  cc.p( popBGSprite.getContentSize().width*0.95 ,  popBGSprite.getContentSize().height * 0.93 ) );
        closeItem.setTag( CLOSE_BTN_TAG );
        closeItem.getSelectedImage().setColor( cc.color( 169, 169, 169, 255 ) );
        menu.addChild( closeItem );
        this.addChild( drawNode, 10 );
        var count = CSUserdefauts.getInstance().getValueForKey(RATING_SHOWN_COUNT,0)+1;
        CSUserdefauts.getInstance().setValueForKey(RATING_SHOWN_COUNT,count);
        popBGSprite.setTag(STAY_IN_TOUCH_POP_TAG);
        DPUtils.getInstance().setTouchSwallowing( null, popBGSprite );

    },

    setAutoSpin : function ( obj )
    {
        obj.removeFromParent( true );
        if ( this.touchPressed && !this.haveReversal )
        {
            if ( this.totalScore >= this.currentBet * this.totalSlots )
            {
                this.isAutoSpinning = true;
                this.autoSpinJustStarted = true;
                this.spinItem.loadTextures( "auto_spin.png", "auto_spin_pressed.png", "", ccui.Widget.PLIST_TEXTURE  );
                var glowSprite = this.spinItem.getChildByTag( GLOW_SPRITE_TAG );
                glowSprite.setTexture( res.glow_white_png );
                //srand( time( 0 ) );
                if( this.betIndicator ) this.betIndicator.setVisible( false );
                this.surpriseTimeDealy=0.5;
                this.isWinningOccured = false;
                this.spin();
                var percentOfbar = VSDailyChallenge.getInstance().getStatusOfCurrentDailyChallenge();
                this.dailyChallengeFillBar.setPercentage(percentOfbar);
                this.delegate.setLevel( this.def.getValueForKey(LEVEL, 0), CSUserdefauts.getInstance().getValueForKey(THIS_LEVEL_BET, 0) + this.currentBet, this );
            }
            else
            {
                this.setInsufficientPage();
            }
        }
    },

    setBetLabel : function()
    {
        var label = this.getChildByTag( BET_LABEL_TAG );

        var string = DPUtils.getInstance().getNumString( this.currentBet );

        if( label )
        {
            label.setString( string );
        }
        else
        {
            var shiftX = 0;
            if ( this.mCode == DJS_X_MULTIPLIERS )
            {
                shiftX = 98;
            }

            var heading = new cc.LabelTTF( "BET", "GEORGIAB", 25 );
            heading.setPosition( cc.p( this.vSize.width * 0.29 - heading.getContentSize().width - shiftX, heading.getContentSize().height * 5.6 ) );
            heading.setColor( cc.color( 255, 255, 0, 255 ) );
            this.addChild( heading, 2 );

            var defStr = "000,000";
            var defSize = 60;

            if( this.betRangeVec[this.betRangeVec.length -1] != 500000 )
            {
                defStr = "0,000,000";
                defSize = 50;
            }
            var lbl = new cc.LabelTTF( defStr, "DS-DIGIT", defSize );
            lbl.setAnchorPoint( cc.p( 0, 0.5 ) );
            var isIE = /*@cc_on!@*/false || !!document.documentMode;//changed 9 May for IE
            if( isIE ) {
                lbl.setPosition( cc.p( this.vSize.width * 0.27 - shiftX, 60 * 2.25 ) );
            }
            else
            {
                lbl.setPosition( cc.p( this.vSize.width * 0.27 - shiftX, 60 * 2.5 ) );
            }

            lbl.setOpacity( 50 );
            lbl.setColor( cc.color( 255, 0, 0, 255 ) );
            this.addChild( lbl, 2 );

            if ( this.mCode == DJS_X_MULTIPLIERS )
            {
                var footNote = new cc.LabelTTF( "x4", "GEORGIAB", 25 );
                footNote.setPosition( cc.p( cc.rectGetMaxX( lbl.getBoundingBox() ) + footNote.getContentSize().width / 2, heading.getContentSize().height * 5 ) );
                footNote.setColor( cc.color( 255, 255, 0, 255 ) );
                this.addChild( footNote, 2 );
            }

            label = new cc.LabelTTF( string, "DS-DIGIT", defSize );
            label.setAnchorPoint( cc.p( 1, 0.5 ) );
            label.setPosition( cc.p( lbl.getPositionX() + lbl.getContentSize().width, lbl.getPositionY() ) );
            label.setTag( BET_LABEL_TAG );
            label.setColor( cc.color( 255, 0, 0, 255 ) );
            this.addChild( label, 2 );
        }
    },

    setBuyPage : function(pageid) {
        var buyOfferPage = new BuyPages(pageid);
        buyOfferPage.setPosition(cc.p(0, 0));
        this.addChild(buyOfferPage, 5);
        DPUtils.getInstance().setTouchSwallowing(null, buyOfferPage);
    },
    getCoinUpdatingFactor : function()
    {
        if ( this.currentWin > this.currentBet * 25 )
        {
            //huge win
         return this.currentWin / 151;//12.08

        }
        else if ( this.currentWin >= this.currentBet * 10 )
        {
            //big win
            return this.currentWin / 61;//4.88
        }
        else
        {
            //win
            return this.currentWin / 23;//1.84
        }
    },
    setOfferPopup : function()
    {
        var layer = new cc.Layer();
        layer.setTag( VIP_LAYER_TAG );
        DPUtils.getInstance().setTouchSwallowing( this, layer );
        this.addChild( layer, 10 );
        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        layer.addChild( darkBG );
        var buySprite = null;
        var closeItem = null;

        if (ServerData.getInstance().offer_coins>0){

            buySprite = new cc.Sprite( res.OfferSuccessPage );

            {
                var timerLabel = new CustomLabel( "$" + DPUtils.getInstance().getNumString( ServerData.getInstance().offer_coins ), "arial_black", 70, new cc.size( 300, 70 ) );
                timerLabel.setPosition( cc.p( buySprite.getContentSize().width * 0.5, buySprite.getContentSize().height * 0.58 ) );
                buySprite.addChild( timerLabel );
            }

            closeItem = new cc.MenuItemImage( res.Collect_Btn_png, res.Collect_Btn_png, this.offerMenuCB, this );
            closeItem.setTag( CLOSE_BTN_TAG );
        }else{
            buySprite = new cc.Sprite( res.OfferInvalidPage );
            closeItem = new cc.MenuItemImage( res.levelUpOk_png, res.levelUpOk_png, this.offerMenuCB, this );
            var timerLabel = new CustomLabel( ServerData.getInstance().offer_message, "arial_black", 60, new cc.size( 250, 60 ) );
            timerLabel.setPosition( cc.p( buySprite.getContentSize().width * 0.5, buySprite.getContentSize().height * 0.6 ) );
            buySprite.addChild( timerLabel );
            this.delegate.offerData = null;
        }

        buySprite.setPosition( cc.p( this.visibleSize.width / 2, this.visibleSize.height / 2 ) );
        layer.addChild( buySprite );
        var offerMenu = new cc.Menu();
        offerMenu.setPosition( cc.p( 0, 0 ) );
        layer.addChild( offerMenu );
        closeItem.setPosition( cc.p( cc.rectGetMidX( buySprite.getBoundingBox() ), cc.rectGetMinY( buySprite.getBoundingBox() ) + closeItem.getContentSize().height * 0.75 ) );
        closeItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );
        var menu = new cc.Menu();
        menu.addChild( closeItem );
        menu.setPosition( cc.p( 0, 0 ) );
        layer.addChild( menu, 5 );
        this.resizer.setFinalScaling( layer );
    },
    offerMenuCB : function( pSender )
    {
        //if ( !this.getParent().getChildByTag( LOADER_TAG ) )
        {
            var tag = pSender.getTag();
            var isCollected = false;
            switch (tag)
            {
                case CLOSE_BTN_TAG:
                    isCollected = true;
                    this.callOfferCollection();
                    break;
                default:
                    break;

            }

            ServerData.getInstance().offer_message="";
            this.removeChildByTag( VIP_LAYER_TAG );
            this.showingScreen = false;
            this.delegate.jukeBox( S_BTN_CLICK );
            if (!isCollected){
                PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_TOUCH_ONE_LINK_POP);
            }
        }
    },
    callOfferCollection : function()
    {
        this.setLoader();
        ServerData.getInstance().hitServerWithCodeAndCallBack(this.OfferCallBackFromServer,ServerData.getInstance().offer_id, ACCEPTENCE)
    },
    OfferCallBackFromServer:function(valueMap) {
        if (valueMap && valueMap["result"] == 1) {
            if (this.getChildByTag(SPLASH_TAG)){
                this.getChildByTag(SPLASH_TAG).removeFromParent();
            }
            if (ServerData.getInstance().offer_coins>0) {
                AppDelegate.getInstance().assignRewards( ServerData.getInstance().offer_coins,true );
                ServerData.getInstance().offer_id = "";

            }
        }
        AppDelegate.getInstance().showingScreen = false;

        PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_TOUCH_ONE_LINK_POP);

    },
    setCurrentWinAnimation : function( sCode )
    {
        if( sCode === undefined )
        {
            sCode = 0;
        }

        if( this.mCode != DJS_X_MULTIPLIERS )         this.addWin = this.addScore = this.getCoinUpdatingFactor();



        if ( this.addWin > 0 )
        {
            this.isWinningOccured = true;
            if ( this.mCode != DJS_X_MULTIPLIERS )
            {
                this.updateDailyChallengesData(this.currentWin,this.currentBet);
            }

            if( CSUserdefauts.getInstance().getValueForKey(EASY_MODE_WIN_CHIPS_300,0) > 0 )
            {
                CSUserdefauts.getInstance().setValueForKey(EASY_MODE_WIN_CHIPS_300,CSUserdefauts.getInstance().getValueForKey(EASY_MODE_WIN_CHIPS_300,0)- this.currentWin);

                if( CSUserdefauts.getInstance().getValueForKey(EASY_MODE_WIN_CHIPS_300,0) <= 0 )
                {
                    CSUserdefauts.getInstance().setValueForKey(EASY_MODE_BET_300,-1);

                    this.checkAndSetEasyModeCode();
                }
            }

            if ( this.callForAdOnWinning )
            {

                //DJSAdvHandler.getInstance().showAdvertisement( GAME_SCENE_TAG );//change 12 Dec
                this.advCalled = true;
                this.callForAdOnWinning = false;
            }

            if ( this.def.getValueForKey( MAXIMUM_WIN, 0 ) < this.currentWin )
            {
                this.def.setValueForKey( MAXIMUM_WIN, this.currentWin );
                this.def.setValueForKey( MAXIMUM_WIN_MACHINE, machinesString[this.mCode - DJS_QUINTUPLE_5X] );
            }

            this.delegate.jukeBox( S_STOP_MUSIC );
            this.delegate.jukeBox( S_STOP_EFFECTS );
            this.delegate.jukeBox( S_AMBIENCE );
            this.schedule( this.updateWinCoin, this.updatorTime );

            if (this.currentWin >= this.currentBet * this.totalSlots * 10 || ( DJS_FREE_SPIN == this.mCode && sCode == 10 ) )
            {
                this.def.setValueForKey(WIN_10X, 1);

                this.setWinAnimation( BIG_WIN_TAG, sCode );

                var tmp = this.slots[sCode].resultReel;
                for ( var j = 0; j < tmp.length; j++ )
                {
                    var tmpSpriteVec = tmp[ j ];
                    for ( var k = 0; k < tmpSpriteVec.length; k++ )
                    {
                        var e = tmpSpriteVec[ k ];
                        e.animateNow();
                    }
                }

                if ( this.data.server_make_him_win && this.currentWin > this.currentBet * this.totalSlots * 25 )
                {
                    this.setMegaWinAnimation();
                }
                else
                {
                    //this.delegate.jukeBox( S_COIN_MORE );
                    this.checkAndAssignJackPot();
                }
            }
            else
            {
                if( this.mCode === DJS_MAGNET_SLIDE )    this.delegate.jukeBox( S_STATIC_WIN );
                else    this.delegate.jukeBox( S_COIN );

                this.checkAndAssignJackPot();

                this.setWinAnimation( WIN_TAG, sCode );
                var tmp = this.slots[sCode].resultReel;
                for ( var j = 0; j < tmp.length; j++ )
                {
                    var tmpSpriteVec = tmp[ j ];
                    for ( var k = 0; k < tmpSpriteVec.length; k++ )
                    {
                        var e = tmpSpriteVec[ k ];
                        e.animateNow();
                    }
                }
            }

            if ( this.delegate.tournaments[0] && this.delegate.tournaments[0].isVisible() )
            {
                this.delegate.tournaments[0].updateMyPts( this.currentWin );
            }
            else if ( this.delegate.tournaments[1] && this.delegate.tournaments[1].isVisible() )
            {
                this.delegate.tournaments[1].updateMyPts( this.currentWin );
            }
            else if( this.delegate.tournaments[2] && this.delegate.tournaments[2].isVisible() )
            {
                this.delegate.tournaments[2].updateMyPts( this.currentWin );
            }

            if ( !this.haveFreeSpins || this.mCode == DJS_SPIN_TILL_YOU_WIN || this.mCode == DJS_SPIN_TILL_YOU_WIN_TWIK || this.mCode == DJS_BONUS_MACHINE )
            {
                this.totalScore+=this.currentWin;

                //this.def.setValueForKey(CHIPS, this.totalScore);
                userDataInfo.getInstance().totalChips = this.totalScore;
                //this.addScore = ( this.totalScore - this.displayedScore ) / 30;

                //analyticsHelper.checkAndUpdateMaxWin( totalScore );
            }

            if ( ( this.addScore > 0 && !this.haveFreeSpins ) || this.mCode == DJS_SPIN_TILL_YOU_WIN || this.mCode == DJS_SPIN_TILL_YOU_WIN_TWIK || this.mCode == DJS_BONUS_MACHINE )
            {
                this.schedule( this.updateCoin, this.updatorTime );
            }

            if ( this.slots[0].dNode )
            {
                var col = cc.color.GREEN;//cc.color( 11.0/255.0, 225.0/255.0, 1.0/255.0, 1 );

                this.slots[sCode].dNode.clear();
                for ( var i = 0; i < this.slots[sCode].totalReels; i++ )
                {
                    this.slots[sCode].dNode.drawSegment( cc.p( this.slots[sCode].reelMids[ i * 3 ], this.slots[sCode].reelMids[ ( i * 3 ) + 1 ] ), cc.p( this.slots[sCode].reelMids[ ( i * 3 ) + 2 ], this.slots[sCode].reelMids[ ( i * 3 ) + 1 ] ), 2.5, col );
                }
            }

            this.haltSpin = true;
            this.scheduleOnce( this.unHalt, 0.75 );
        }
        if( this.addWin > 0 && !this.getChildByTag( JACKPOT_WIN_LAYER_TAG ) )//#Change
        {
            if( this.currentWin >= this.currentBet * 100 )
            {
                if( this.setBigWinAnimation( GRAND_WIN_TAG ) )
                    this.delegate.jukeBox( S_GRAND_WIN_COINS );
                else
                    this.delegate.jukeBox( S_GRAND_WIN_COINS_TONE );
            }
            else if ( this.currentWin >= this.currentBet * 50 )
            {
                if( this.setBigWinAnimation(MEGA_WIN_TAG) )
                    this.delegate.jukeBox( S_HUGE_WIN_COINS );
                else
                    this.delegate.jukeBox( S_HUGE_WIN_COINS_TONE );
            }
            else if ( this.currentWin >= this.currentBet * 25 )
            {

                if( this.setBigWinAnimation(BIG_WIN_TAG) )
                    this.delegate.jukeBox( S_BIG_WIN_COINS );
                else
                    this.delegate.jukeBox( S_BIG_WIN_COINS_TONE );

            }
            else if ( this.currentWin >= this.currentBet * 10 )//#Change Oct 31 2017
            {
                this.delegate.jukeBox( S_BIG_WIN_SOUND );
            }
            else //if ( currentWin >= currentBet * 10 )
            {
                //win
                if( this.mCode == DJS_MAGNET_SLIDE ) this.delegate.jukeBox( S_STATIC );
                else this.delegate.jukeBox( S_COIN );
            }
            if (this.currentWin>0){
                userDataInfo.getInstance().totalChips = this.totalScore;
                this.runAction(new cc.Sequence(new cc.DelayTime(this.surpriseTimeDealy),new cc.CallFunc(function () {
                    this.addSurpiseBonusCoinsLbl(this.currentWin);
                    this.isWinningOccured = false;
                },this)));
                this.surpriseTimeDealy+=0.5;
            }
        }

    },
    surpriseTimeDealy:0.5,
    addSurpiseBonusCoinsLbl:function(rewardCoins){
        var labl = this.getChildByTag( SCORE_LABEL_TAG );
        var surpiseBonusCoinsLbl = this.getChildByTag( SURPRISE_LABEL_TAG );
        if (surpiseBonusCoinsLbl){
            surpiseBonusCoinsLbl.stopAllActions();
            surpiseBonusCoinsLbl.removeFromParent();
        }
        if (rewardCoins>0) {
            var surpiseBonusCoinsLbl = new CustomLabel("+" + DPUtils.getInstance().getNumString(rewardCoins), "TT0504M_", 30, new cc.size(215, 30));
            surpiseBonusCoinsLbl.setColor(cc.color.GREEN);
            surpiseBonusCoinsLbl.setPosition(cc.p(labl.getPositionX() + labl.getContentSize().width / 3, labl.getPositionY() - labl.getContentSize().height));
            surpiseBonusCoinsLbl.setTag(SURPRISE_LABEL_TAG);
            this.addChild(surpiseBonusCoinsLbl, 2);
            surpiseBonusCoinsLbl.runAction(new cc.Sequence(new cc.MoveBy(1.5, cc.p(-labl.getPositionX() / 3, labl.getContentSize().height * 1.5)), cc.callFunc(function () {
                surpiseBonusCoinsLbl.removeFromParent();
            }, this)));
        }
    },
    setBigWinAnimation : function ( winCode )
{
    if( !this.data.server_win_animation_control[winCode - BIG_WIN_TAG] )         return false;
    

    var bolt = new cc.Node();//cc.LayerColor( cc.color( 0, 0, 0, 160 ) );
    bolt.setTag( BIG_WIN_LAYER_TAG );
    this.addChild( bolt, 50 );

    var darkBG = new cc.Sprite( res.Dark_BG_png );
    darkBG.setScale( 2 );
    darkBG.setOpacity( 169 );
    darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
    bolt.addChild( darkBG );

    var sprite = null;
    switch ( winCode ) {
        case GRAND_WIN_TAG:
            sprite = new cc.Sprite( res.Grand_win_text/*"GameButtons/grand_win_text.png"*/ );
            break;

        case MEGA_WIN_TAG:
            sprite = new cc.Sprite( res.Mega_win_text/*"GameButtons/mega_win_text.png"*/ );
            break;

        case BIG_WIN_TAG:
            sprite = new cc.Sprite( res.Big_win_text/*"GameButtons/big_win_text.png"*/ );
            break;

        default:
            break;
    }
    if( sprite )
    {
        sprite.setPosition( cc.p( this.vSize.width * 0.5, this.vSize.height * 0.6 ) );
        sprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.MoveBy( 0.03, cc.p( 3, 0 ) ), new cc.MoveBy( 0.03, cc.p( -3, 0 ) ) ) ) );
        if( winCode != GRAND_WIN_TAG )          sprite.setScale( 0.9 );
    }

    if( winCode == BIG_WIN_TAG )
    {
        var flareSprite = new cc.Sprite( res.Flare/*"GameButtons/flare.png"*/ );
        flareSprite.setPosition( -sprite.getContentSize().width * sprite.getScale() * 0.5, sprite.getPositionY() );
        bolt.addChild( flareSprite );

        var mBy = new cc.MoveBy( 0.5, cc.p( sprite.getContentSize().width * 2.5, 0 ) );
        flareSprite.runAction( new cc.RepeatForever( new cc.Sequence( mBy, mBy.reverse() ) ) );
    }

    //sprite.setGlobalZOrder( 5 );
    //DPUtils.getInstance().setTouchSwallowing( null, bolt );

    /*Particle2D_On_3D * fireWorks = Particle2D_On_3DWithTotalParticles( 20 );
    fireWorks.setSpeed( 1000 );
    fireWorks.setGravity( cc.p( fireWorks.getGravity().x, fireWorks.getGravity().y * 8 ) );
    fireWorks.setPosVar( cc.p( sprite.getContentSize().width * 0.5 * sprite.getScale(), 0 ) );
    fireWorks.setAutoRemoveOnFinish( true );

    bolt.addChild( fireWorks );*/
    bolt.addChild( sprite );

    var label = new cc.LabelBMFont( /*"fonts/font_new2-export.fnt"*/"0", res.BM_FONT_1 );
    if( winCode == GRAND_WIN_TAG )          label.setPosition( cc.p( this.vSize.width * 0.5, this.vSize.height * 0.25 ) );
    else                                    label.setPosition( cc.p( this.vSize.width * 0.5, this.vSize.height * 0.35 ) );
    label.setTag( BIG_WIN_LAYER_TAG );
    label.setScale( 0.7 );

    while ( label.getContentSize().width * label.getScale() > this.vSize.width * 0.9 )
    {
        label.setScale( label.getScale() - 0.01 );
    }

    bolt.addChild( label );

    //fireWorks.setPosition( label.getPositionX(), -100 );

    sprite.setScale( 0 );
    if( winCode != GRAND_WIN_TAG )          sprite.runAction( new cc.ScaleTo( 0.5 , 0.9 ) );
    else
    {
        sprite.runAction( new cc.Sequence( new cc.ScaleTo( 0.5 , 0.9 ), cc.callFunc( function( sprite ){//lambda

        var leftBullet = new cc.Sprite( res.Bullet/*"GameButtons/bullet.png"*/ );
        leftBullet.setFlippedX( true );
        leftBullet.setPosition( cc.p( -leftBullet.getContentSize().width * 0.5, this.vSize.height * 0.5 ) );
        bolt.addChild( leftBullet );

        leftBullet.runAction( new cc.Sequence( new cc.MoveTo( 0.375, cc.p( this.vSize.width * 0.24, leftBullet.getPositionY() ) ), cc.callFunc( function( bolt ){//lambda
            var fire = new cc.ParticleSystem( res.Particle_Plist/*"GameButtons/particle_texture.plist"*/ );
            //fire.setTotalParticles(500);
            fire.setPosition( leftBullet.getPositionX() + leftBullet.getContentSize().width * 0.5, leftBullet.getPositionY() );

            bolt.addChild( fire );
        }, this, leftBullet ), new cc.MoveTo( 0.5, cc.p( this.vSize.width * 0.25, leftBullet.getPositionY() ) ) ) );

        var rightBullet = new cc.Sprite( res.Bullet/*"GameButtons/bullet.png"*/ );
        rightBullet.setPosition( cc.p( this.vSize.width + rightBullet.getContentSize().width * 0.5, this.vSize.height * 0.5 ) );
        bolt.addChild( rightBullet );

        rightBullet.runAction( new cc.Sequence( new cc.MoveTo( 0.375, cc.p( this.vSize.width * 0.74, rightBullet.getPositionY() ) ), cc.callFunc( function( bolt ){//lambda
            var fire = new cc.ParticleSystem( res.Particle_Plist/*"GameButtons/particle_texture.plist"*/ );
           // fire.setTotalParticles(500);

            fire.setPosition( rightBullet.getPositionX() - rightBullet.getContentSize().width * 0.5, rightBullet.getPositionY() );

            bolt.addChild( fire );
        }, this, rightBullet ), new cc.MoveTo( 0.5, cc.p( this.vSize.width * 0.75, rightBullet.getPositionY() ) ) ) );

    }, this, bolt ) ) );
    }

    label.setScale( 0 );
    label.runAction( new cc.ScaleTo( 0.5 , 0.7 ) );
    

    return true;
},
    checkAndAssignJackPot : function()
    {
        if( !this.getChildByTag( JACKPOT_LABEL_TAG ) ) return;

        var lastWinning = CSUserdefauts.getInstance().getValueForKey( JACKPOT_LAST_WINNING, 0 );
        var curTime = ServerData.getInstance().current_time_seconds;

        if( this.def.getValueForKey( IS_BUYER, false ) && userDataInfo.getInstance().progressive_jackpot &&
            ( !lastWinning || curTime - lastWinning >= SECONDS_IN_A_DAY * userDataInfo.getInstance().progressive_jackpot ) && this.currentBet >= 10000 &&
            Math.floor( Math.random() * 2 ) )
        {
            this.getChildByTag( JACKPOT_LABEL_TAG ).stopAllActions();
            CSUserdefauts.getInstance().setValueForKey( JACKPOT_LAST_WINNING, curTime );

            userDataInfo.getInstance().progressive_jackpot_winning_count++;//this.def.setValueForKey( JACKPOT_WIN_COUNT, this.def.getValueForKey( JACKPOT_WIN_COUNT, 0 ) + 1 );

            ServerData.getInstance().updateData("jackpot_win_count",    "1"
            ,"","singleincrement");
            //DJSAnalyticsHelper::getInstance().postAppAnalyticsToServer( true );
            var drawNode = new cc.Node();
            drawNode.setPosition( cc.p( 0, 0 ) );

            drawNode.setTag( JACKPOT_WIN_LAYER_TAG );
            this.addChild( drawNode, 10 );

            var darkBG = new cc.Sprite( res.Dark_BG_png );
            darkBG.setScale( 2 );
            darkBG.setOpacity( 169 );
            darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
            drawNode.addChild( darkBG );


            var nodd = new cc.Node();
            drawNode.addChild( nodd );

            var node2 = new cc.Node();
            drawNode.addChild( node2 );

            var degree = 0;
            for ( var i = 0; i < 4; i++ )
            {
                var light = new cc.Sprite( "res/megaWin/effect_line.png" );

                light.setRotation( degree );
                nodd.addChild( light );

                var light2 = new cc.Sprite( "res/megaWin/effect_line.png" );
                //light2.setPosition( cc.p( Vec2 ( vSize / 2 ) + origin ) );
                light2.setRotation( degree + 45 );
                node2.addChild( light2 );

                degree+=90;
            }
            nodd.setPosition( cc.p( this.vSize.width / 2, this.vSize.height / 2 ) );
            nodd.runAction( new cc.RepeatForever( new cc.RotateBy( 5.0, 360 ) ) );
            node2.setPosition( cc.p( this.vSize.width / 2, this.vSize.height / 2 ) );
            node2.runAction( new cc.RepeatForever( new cc.RotateBy( 5.0, -360 ) ) );

            //var color =  cc.color( 255.0, 255.0, 255.0, 100.0 );
            //drawNode.drawSegment( cc.p( 0, 0 ), cc.p( this.vSize.width * 1.5, /*this.vSize.height * 1.25*/0 ), this.vSize.height * 1.25, color );

            var popBGSprite = new cc.Sprite( "res/jackpot/jackpot_winning_bg.png" );
            popBGSprite.setPosition( cc.p( this.vSize.width * 0.5, this.vSize.height / 2 ) );
            DPUtils.getInstance().setTouchSwallowing( null, popBGSprite );
            drawNode.addChild( popBGSprite );



            var node = this.getChildByTag( JACKPOT_LABEL_TAG );
            var label = new cc.LabelBMFont( node.getString(), res.BM_FONT_2 );//cc.LabelTTF( node.getString(), "DS-DIGIT", 40 );
            label.setPosition( cc.p( this.vSize.width * 0.5, this.vSize.height * 0.53 ) );
            popBGSprite.addChild( label );
            label.setScale( 0.4 );

            this.resizer.setFinalScaling( popBGSprite );
            //resizer.setFinalScaling( node );

            var menu = new cc.Menu();
            menu.setPosition( cc.p( 0, 0 ) );
            popBGSprite.addChild( menu );

            var closeItem = new cc.MenuItemImage( "res/jackpot/tournament_collect_button.png", "res/jackpot/tournament_collect_button.png", this.jackpotMenuCB, this );

            closeItem.setPosition( cc.p( this.vSize.width * 0.5, this.vSize.height * 0.32 ) );
            closeItem.setTag( CLOSE_BTN_TAG );

            //closeItem.getSelectedImage().setColor( Color3B::GRAY );
            menu.addChild( closeItem );

            this.delegate.jukeBox( S_JACKPOT_WIN );

            var sun1 = new cc.ParticleSun();
            var sun2 = new cc.ParticleSun();

            sun1.setPosition( cc.p( this.vSize.width * 0.25, this.vSize.height * 0.2 ) );
            sun2.setPosition( cc.p( this.vSize.width * 0.75, this.vSize.height * 0.2 ) );
            //sun1.setScale(0.6);
            //sun2.setScale(0.6);
            sun1.setTexture( cc.textureCache.addImage( "res/tournament/cst_purpleShine.png" ) );
            sun2.setTexture( cc.textureCache.addImage( "res/tournament/cst_yellowShine.png" ) );

            sun2.setVisible( false );

            //appdel.jukeBox(TOURNAMENT);
            var mTo1 = new cc.MoveTo( 0.5, cc.p( cc.rectGetMinX(popBGSprite.getBoundingBox() ) - 50, cc.rectGetMaxY( popBGSprite.getBoundingBox() ) - 100 ) );
            var mTo2 = new cc.MoveTo( 0.5, cc.p( cc.rectGetMaxX(popBGSprite.getBoundingBox() ) + 50, cc.rectGetMaxY( popBGSprite.getBoundingBox() ) - 100 ) );
            sun1.runAction( new cc.Sequence( mTo1, cc.callFunc(function(){
                var exp = new cc.ParticleExplosion();

                exp.setTexture( cc.textureCache.addImage("res/tournament/cst_purpleShine.png") );
                exp.setPosition( sun1.getPosition() );
                this.addChild( exp );
                sun1.removeFromParent( true );


            },this) ) );

            sun2.runAction( new cc.Sequence( new cc.DelayTime( 0.7 ),cc.callFunc(function(){
                //this.appdel.jukeBox(MachineId.TOURNAMENT);
                sun2.setVisible(true);

            },this), mTo2, cc.callFunc(function(){

                var exp1 = new cc.ParticleExplosion();
                exp1.setTexture( cc.textureCache.addImage("res/tournament/cst_yellowShine.png") );
                exp1.setPosition( sun2.getPosition() );
                this.addChild( exp1 );
                sun2.removeFromParent( true );
            },this)));

            drawNode.addChild( sun1 );
            drawNode.addChild( sun2 );
        }
    },
    jackpotMenuCB : function( pSender )
    {
        this.delegate.assignRewards( this.jackPot.jackPotInitialValue ,true);
        JackPotManager.getInstance().setJackPotWithMachineCode( this.mCode - DJS_QUINTUPLE_5X );
        this.getChildByTag( JACKPOT_LABEL_TAG ).runAction( new cc.RepeatForever( new cc.Sequence( new cc.DelayTime( 0.05 ), cc.callFunc(function(){
            this.setJackPotLabel();
        },this) ) ) );

        this.jackPot.jackPotInitialValue = this.def.getValueForKey( JACKPOT_INITIAL_VALUE + "_" + machinesString[ this.mCode - DJS_QUINTUPLE_5X ] );

        var node = pSender;
        node.getParent().getParent().getParent().removeFromParent( true );
        this.isWinningOccured = false;
    },
    unHalt : function( dt )
    {
        this.haltSpin = false;
    },

    onTouchesBegan : function ( touch, event )
    {
        if ( !this.haltSpin && touch.getID() == 0 )
        {
            this.waitAndRemoveBigWinTag(0);
        }
        return true;
    },
    waitAndRemoveBigWinTag:function(dt){
        if ( this.getChildByTag( MEGA_WIN_TAG )){
            this.removeMegaWinAnimation();
        }
        if ( this.getChildByTag( BIG_WIN_LAYER_TAG )){
            this.removeChildByTag( BIG_WIN_LAYER_TAG );
            this.displayedWin = this.currentWin;
            this.unschedule(  this.updateWinCoin );
            this.setWinLabel();
            this.displayedScore = this.totalScore;
            this.unschedule( this.updateCoin );
            this.setScoreLabel();
            this.delegate.jukeBox( S_STOP_EFFECTS );
        }

        if (PopUps.getInstance().listOfPopUps.length>=1){
            PopUps.getInstance().showPopUpAccToIndex(PopUps.getInstance().listOfPopUps[0]);
        }else{
            this.delegate.showingScreen = false;
        }
    },
    scaleAccordingToSlotScreen : function( node, isFg )
    {
        if( isFg )
        {
            if ( this.slots[0].getScale() != 1 )
            {
                node.setScaleY( this.slots[0].getScaleY() );
            }

            //resizer.setFinalScaling( node );
        }
        else
        {
            if ( this.slots[0].getScale() != 1 )
            {
                node.setScale( this.slots[0].getScale() );
            }

            this.resizer.setFinalScaling( node );
            this.resizer.setFinalScaling( node );
        }
    },

    setGameBG : function(i)
    {

        /*{
            //TextureCache * tCache = Director.getInstance().getTextureCache();
            //tCache.removeAllTextures();
            for ( i = 0; i < 3; i++)
            {
                var string = machinesString[this.mCode - DJS_QUINTUPLE_5X] + "/paytable_" + (i + 1) + ".png";

                /*Texture2D * texture2D = tCache.addImage(string);
                 if ( texture2D )
                 {
                 textureVec.pushBack( texture2D );
                 }*
            }
        }*/


        var string =  "res/" + machinesString[this.mCode - DJS_QUINTUPLE_5X] + "/bg.png";

        var sprite = new cc.Sprite( string );
        if ( sprite )
        {
            sprite.setPosition( cc.p( this.vSize.width * 0.5, this.vSize.height * 0.5 ) );
            this.addChild( sprite, this.mCode >= DJS_CRYSTAL_SLOT && this.mCode <= DJS_GOLDEN_SLOT ? 2 : 0 );
        }


        this.topSprite = new cc.Sprite( res.Top_Bar_Lobby_png );
        this.topSprite.setPosition( cc.p( this.vSize.width * 0.5, this.vSize.height - this.topSprite.getContentSize().height * 0.5 ) );
        this.addChild( this.topSprite, this.mCode >= DJS_CRYSTAL_SLOT && this.mCode <= DJS_GOLDEN_SLOT ? 2 : 0 );

        var topBarBack = new cc.Sprite( res.Top_Bar_Back_png );//
        this.resizer.setFinalScaling( topBarBack );
        topBarBack.setPosition( this.topSprite.getPosition() );//X(), this.vSize.height - topBarBack.getContentSize().height * 0.5 * topBarBack.getScaleY() );
        this.addChild( topBarBack, this.mCode >= DJS_CRYSTAL_SLOT && this.mCode <= DJS_GOLDEN_SLOT ? 2 : 0 );

        this.bottomBar = new cc.Sprite( "#bottom_bar.png" );

        this.bottomBar.setPosition(cc.p( 209 + this.bottomBar.getContentSize().width / 2, this.vSize.height - 587 - this.bottomBar.getContentSize().height / 2));
        this.addChild(this.bottomBar, 2);

    },

    setGameScene : function()
    {
        this.setGameBG( this.resourceCount );


        if( this.mCode == DJS_X_MULTIPLIERS )
        {
            for ( var i = 0; i < 4; i++ )
            {
                this.setSlotScrns( i );
            }
        }
        else
        {
            this.setSlotScrns( 0 );
        }




        if ( this.mCode == DJS_FREE_SPIN )
        {
            this.setpayLines();
        }

        this.setScoreLabel();

        this.setBetLabel();

        this.setWinLabel();
        //this.setSlotScrns( this.reelI );
        this.setMenu();

        this.delegate.jukeBox( S_AMBIENCE );


        if( this.mCode >= DJS_CRYSTAL_SLOT && this.mCode < DJS_CRYSTAL_SLOT + TOTAL_MACHINES_WITH_JACKPOT )
        {
            this.jackPot = JackPot.getInstance( this.mCode - DJS_QUINTUPLE_5X );
            if( this.currentBet >= 10000 )
                this.betIndicator = new cc.Sprite( "#enough.png" );
            else
                this.betIndicator = new cc.Sprite( "#not_enough.png" );
            this.betIndicator.setPosition( this.bottomBar.getPositionX(), cc.rectGetMaxY( this.bottomBar.getBoundingBox() ) + this.betIndicator.getContentSize().height * 0.5 );
            this.addChild( this.betIndicator, 2 );
            this.setJackPotLabel();
        }




        this.delegate.setLevel( this.def.getValueForKey(LEVEL, 0), CSUserdefauts.getInstance().getValueForKey(THIS_LEVEL_BET, 0), this );

        if ( this.mCode != DJS_X_MULTIPLIERS )
        {
            this.rankingBase = new cc.Sprite( "#cst_ranking_base.png" );
            this.rankingBase.setPosition( cc.p( this.rankingBase.getContentSize().width * 0.5, this.vSize.height * 0.5 + 25 ) );
            this.addChild( this.rankingBase, 2 );

            this.connectingTournament( 0 );
            this.setSlotScreenPosition();
           /* if (this.mCode!=DJS_XTREME_MACHINE){
                this.setSlotScreenPosition();

            }else{
                this.setReelContainerPosition();
            }*/
        }
        this.setupDailyChanllengeUI();

        // gaf code of mega win here
        this.addMegaWinGaf();
    },
    betIndicator : null,
    jackPot : null,
    setJackPotLabel : function( node )
    {
        var label = this.getChildByTag( JACKPOT_LABEL_TAG );
        var string = "$ " + DPUtils.getInstance().getNumString( this.jackPot.updateJackPot( node ) );

        if( label )
        {
            label.setString( string );
        }
        else
        {
            label = new cc.LabelBMFont( string, res.BM_FONT_2 );/*new cc.LabelBMFont( string, "fonts/font2-export.fnt" );//*/
            label.setScale( 0.25 );
            label.setPosition( this.vSize.width * 0.45, this.vSize.height * 0.9 );
            label.setTag( JACKPOT_LABEL_TAG );
            label.setAnchorPoint( cc.p( 0, 0.5 ) );
            this.addChild( label, 3 );

            label.runAction( new cc.RepeatForever( new cc.Sequence( new cc.DelayTime( 0.05 ), cc.callFunc(  this.setJackPotLabel, this ) ) ) );
        }
    },
    setGlow : function( itm )
    {
        var glowSprite = new cc.Sprite( "res/glow_white.png" );

        glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.ScaleTo( 1 + Math.random(), 0 ), new cc.ScaleTo( 1 + Math.random(), 0.6, 2 ) ) ) );
        glowSprite.setPosition( itm.getContentSize().width / 2, itm.getContentSize().height / 2 );
        glowSprite.setOpacity( 255 * 0.75 );
        itm.addChild( glowSprite );
    },
    betRangeVec : [],
    setInitialBet : function()
    {

        /*if( this.totalScore >= 100000000 )
        {
            this.betRangeVec = [
                50,
                100,
                500,
                1000,
                5000,
                10000,
                50000,
                100000,
                500000,
                1000000,
                5000000];
        }
        else*/
        {

        }
        this.betRangeVec = [
            50,
            100,
            500,
            1000,
            5000,
            10000,
            50000,
            100000,
            500000];
        this.currentBet = 50;
        this.setInitialBetValue();
    },
    setInitialBetValue:function(){
        var initialBet = this.totalScore / ServerData.getInstance().server_bet_factor_3reel;
        if ( this.mCode === DJS_X_MULTIPLIERS )
        {
            initialBet = this.totalScore / (ServerData.getInstance().server_bet_factor_3reel*4);
        }
        if(this.def.getValueForKey( LEVEL, 0 )>=13){

            this.currentBet = this.getinitialbetByBetFactor(initialBet);

        }else{

            if(this.def.getValueForKey( LEVEL, 0 )>=11){
                this.currentBet =100000;
                if(initialBet<=this.currentBet){
                    this.currentBet = this.getinitialbetByBetFactor(initialBet);
                }
            }else  if(this.def.getValueForKey( LEVEL, 0 )>=9){
                this.currentBet =50000;
                if(initialBet<=this.currentBet){
                    this.currentBet = this.getinitialbetByBetFactor(initialBet);
                }
            }else  if(this.def.getValueForKey( LEVEL, 0 )>=7){
                this.currentBet =10000;
                if(initialBet<=this.currentBet){
                    this.currentBet = this.getinitialbetByBetFactor(initialBet);
                }
            }else  if(this.def.getValueForKey( LEVEL, 0 )>=5){
                this.currentBet =5000;
                if(initialBet<=this.currentBet){
                    this.currentBet = this.getinitialbetByBetFactor(initialBet);
                }
            }else  if(this.def.getValueForKey( LEVEL, 0 )>=3){
                this.currentBet =1000;
                if(initialBet<=this.currentBet){
                    this.currentBet = this.getinitialbetByBetFactor(initialBet);
                }
            }else  if(this.def.getValueForKey( LEVEL, 0 )>=1){
                this.currentBet =500;
                if(initialBet<=this.currentBet){
                    this.currentBet = this.getinitialbetByBetFactor(initialBet);
                }
            }else  if(this.def.getValueForKey( LEVEL, 0 )>=0){
                this.currentBet =100;
                if(initialBet<=this.currentBet){
                    this.currentBet = this.getinitialbetByBetFactor(initialBet);
                }
            }
        }
    },
    getinitialbetByBetFactor:function(initialBet){
        for ( var i = this.betRangeVec.length - 1; i >= 0; i-- )
        {
            if ( initialBet >= this.betRangeVec[i] )
            {
                initialBet = this.betRangeVec[i];
                break;
            }
        }

        if ( initialBet < this.betRangeVec[0] )
        {
            initialBet = this.betRangeVec[0];
        }
        else if( initialBet > this.betRangeVec[ this.betRangeVec.length - 1 ] )
        {
            initialBet = this.betRangeVec[ this.betRangeVec.length - 1 ];
        }

        return initialBet;
    },
    setInsufficientPage : function()
    {
        if ( this.getChildByTag( VIP_LAYER_TAG ) || this.getChildByTag( STAY_IN_TOUCH_POP_TAG ) )
        {
            return;
        }

        var currentOfferCode = 1;
        switch ( this.data.server_buy_page )
        {
            case 2:
                if ( this.def.getValueForKey( IS_BUYER, false ) )
                {
                    currentOfferCode = BuyPageTags.INSUFFICIENT_CREDITS_1;
                }
                else
                {
                    currentOfferCode = BuyPageTags.INSUFFICIENT_CREDITS_2;
                }
                break;

            case 3:
                currentOfferCode = BuyPageTags.INSUFFICIENT_CREDITS_2;
                break;

            case 4:
                currentOfferCode = BuyPageTags.INSUFFICIENT_CREDITS_3;
                break;

            default:
                currentOfferCode = BuyPageTags.INSUFFICIENT_CREDITS_1;
                break;
        }
        var offer = new InsufficientPop( currentOfferCode );
        offer.setPosition( cc.p( 0, 0 ) );
        offer.setTag( VIP_LAYER_TAG );
        this.addChild( offer, 5 );
        DPUtils.getInstance().setTouchSwallowing( null, offer );

    },

    setLevelUpNotification : function()
    {
       var rewards = this.delegate.levelUpScore;
        this.totalScore+=rewards;
        //this.def.setValueForKey(CHIPS, this.totalScore );
        userDataInfo.getInstance().totalChips = this.totalScore;
        if (rewards>0){
            this.addSurpiseBonusCoinsLbl(rewards);

            // this.runAction(new cc.Sequence(new cc.DelayTime(this.surpriseTimeDealy),new cc.CallFunc(function () {
            // },this)));
        }
        //if ( !this.isScheduled( this.updateCoin, this ) )
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }
        var vip = this.def.getValueForKey( VIP_POINTS, 0 ) + this.delegate.levelUpVIP;

        ServerData.getInstance().updateData("vip_points", vip,""  ,"singlereplace");

        this.def.setValueForKey(VIP_POINTS, vip);

        var bgSprite = new cc.Sprite( "res/levelUp.png" );


        var vipLabel = new CustomLabel( "" + this.delegate.levelUpVIP, "arial_black", 22.5, cc.size( 85, 22.5 ) );
        vipLabel.setPosition( cc.p( 225, 22.5 * 1.2 ) );
        bgSprite.addChild( vipLabel );

        var coinLabel = new CustomLabel( "" + this.delegate.levelUpScore, "arial_black", 22.5, cc.size( 117.5, 22.5 ) );
        coinLabel.setPosition( cc.p( 97.5, 22.5 * 1.2 ) );
        bgSprite.addChild( coinLabel );


        bgSprite.setPosition( cc.p( this.vSize.width * 0.75, this.vSize.height + bgSprite.getContentSize().height * 0.5 ) );

        bgSprite.runAction( new cc.Sequence( new cc.MoveBy( 0.5, cc.p( 0, -bgSprite.getContentSize().height ) ), new cc.DelayTime( 0.75 ), new cc.MoveBy( 0.5, cc.p( 0, bgSprite.getContentSize().height ) ),
            cc.callFunc( function( bgSprite ) {
                bgSprite.removeFromParent( true );
            }, this ) ) );

        this.addChild( bgSprite, 2 );
    },


    fullScreenItem : null,
    settingItem : null,
    setMenu : function()
    {
        var menu = new cc.Menu();

        this.fullScreenItem = new cc.MenuItemSprite( new cc.Sprite( res.Full_screen_button_png ), new cc.Sprite( res.Full_screen_button_png ), this.menuCB, this );

        this.fullScreenItem.setPosition( this.vSize.width - this.fullScreenItem.getContentSize().width * 0.5, this.vSize.height - 30 );
        this.fullScreenItem.setTag( FULL_SCREEN_BTN_TAG );

        this.fullScreenItem.getSelectedImage().setColor( cc.color( 166, 166, 166 ) );
        this.resizer.setFinalScaling( this.fullScreenItem );
        menu.addChild( this.fullScreenItem );
        if ( cc.screen.fullScreen() ) {
            this.fullScreenItem.getNormalImage().setTexture( res.Normal_screen_button_png );
            this.fullScreenItem.getSelectedImage().setTexture( res.Normal_screen_button_png );
        }

        this.settingItem = new cc.MenuItemImage( res.Setting_png, res.Setting_png, this.menuCB, this );
        this.resizer.setFinalScaling( this.settingItem );
        this.settingItem.setPosition( this.fullScreenItem.getPositionX() - this.fullScreenItem.getContentSize().width * 0.5 - this.settingItem.getContentSize().width * 0.5 - 5,
            this.fullScreenItem.getPositionY() );
        this.settingItem.setTag( SETTING_BTN_TAG );

        this.settingItem.getSelectedImage().setColor( cc.color( 166, 166, 166 ) );

        /*var settingItem = new cc.MenuItemImage( res.Setting_png, res.Setting_png, this.menuCB, this );
         this.resizer.setFinalScaling( settingItem );
         settingItem.setPosition( this.vSize.width - 80, this.vSize.height - 27 );
         settingItem.setTag( SETTING_BTN_TAG );

         settingItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );*/

        var lobbyItem = new cc.MenuItemSprite( new cc.Sprite( "#lobby_button.png" ), new cc.Sprite( "#lobby_button.png" ), this.menuCB, this );
        this.resizer.setFinalScaling( lobbyItem );
        lobbyItem.setPosition( lobbyItem.getContentSize().width/2, this.vSize.height - this.settingItem.getContentSize().height * 0.55);//( 80, this.vSize.height - 27/*cc.p( lobbyItem.getContentSize().width/2,
         //this.vSize.height - lobbyItem.getContentSize().height * 0.55 )*/ );
        lobbyItem.setTag( LOBBY_BTN_TAG );
        lobbyItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );

        var normal = gaf.Asset.create( res.Buy_Button_gaf ).createObject();

        normal.start();
        normal.setLooped(true);

        var selected = gaf.Asset.create( res.Buy_Button_gaf ).createObject();

        normal.setPositionY( normal.getContentSize().height );
        selected.setPositionY( selected.getContentSize().height );

        selected.start();
        selected.setLooped(true);

        this.buyBtn = new cc.MenuItemSprite( normal, selected,
            this.menuCB, this );
        this.resizer.setFinalScaling( this.buyBtn );

        this.buyBtn.setPosition( cc.p( this.vSize.width * 0.5,
            this.vSize.height - this.buyBtn.getContentSize().height * 0.6 ) );
        this.buyBtn.setTag( BUY_BTN_TAG );
        this.buyBtn.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );


        var normalHalf = gaf.Asset.create( res.Buy_Button_Half_gaf ).createObject();

        normalHalf.start();
        normalHalf.setLooped(true);

        var selectedHalf = gaf.Asset.create( res.Buy_Button_Half_gaf ).createObject();

        selectedHalf.start();
        selectedHalf.setLooped(true);

        this.buyBtnHalf = new cc.MenuItemSprite( normalHalf, selectedHalf,
            this.menuCB, this );
        this.resizer.setFinalScaling( this.buyBtnHalf );

        this.buyBtnHalf.setPosition( cc.p( this.vSize.width * 0.5 - this.buyBtnHalf.getContentSize().width * 0.5 * this.buyBtnHalf.getScaleX(), this.vSize.height - this.buyBtnHalf.getContentSize().height * 0.6 * this.buyBtnHalf.getScaleY() ) );
        this.buyBtnHalf.setTag( BUY_BTN_TAG );
        this.buyBtnHalf.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );
        this.buyBtnHalf.getSelectedImage().setPositionY( this.buyBtnHalf.getSelectedImage().getContentSize().height );
        this.buyBtnHalf.getNormalImage().setPositionY( this.buyBtnHalf.getSelectedImage().getContentSize().height );


        var normalDealHalf = gaf.Asset.create( res.Deal_Button_Half_gaf ).createObject();

        normalDealHalf.start();
        normalDealHalf.setLooped(true);

        var selectedDealHalf = gaf.Asset.create( res.Deal_Button_Half_gaf ).createObject();

        selectedDealHalf.start();
        selectedDealHalf.setLooped(true);

        this.dealBtn = new cc.MenuItemSprite( normalDealHalf, selectedDealHalf,
            this.menuCB, this );
        this.resizer.setFinalScaling( this.dealBtn );

        this.dealBtn.setPosition( cc.p( this.vSize.width * 0.5 + this.buyBtnHalf.getContentSize().width * 0.5 * this.dealBtn.getScaleX(), this.vSize.height - this.dealBtn.getContentSize().height * 0.6 * this.dealBtn.getScaleY() ) );
        this.dealBtn.setTag( DEAL_BTN_TAG );
        this.dealBtn.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );
        this.dealBtn.getSelectedImage().setPositionY( this.dealBtn.getSelectedImage().getContentSize().height );
        this.dealBtn.getNormalImage().setPositionY( this.dealBtn.getSelectedImage().getContentSize().height );

        var tempTimerLabel = new cc.LabelTTF( "00:00", "arial_black", 20 );
        var timerLabel = new cc.LabelTTF( "00:00", "arial_black", 20, tempTimerLabel.getContentSize().length );
        var isFirefox = typeof InstallTrigger !== 'undefined';
        if( isFirefox )
        {
            timerLabel.setPosition( this.dealBtn.getContentSize().width * 0.45, this.dealBtn.getContentSize().height * 0.15 );
        }
        else
        {
            timerLabel.setPosition( this.dealBtn.getContentSize().width * 0.45, this.dealBtn.getContentSize().height * 0.25 );
        }

        timerLabel.setTag( TIMER_LABEL_TAG );
        this.dealBtn.addChild( timerLabel );

        var deal = AppDelegate.getInstance().getDealInstance();//DJSDeal.getInstance();

        if ( !deal.getDealStatus() )
        {
            this.dealBtn.setEnabled( false );
            this.dealBtn.setVisible( false );
            selectedDealHalf.pauseAnimation();
            normalDealHalf.pauseAnimation();
            //deal.resetDealData();

            this.buyBtnHalf.setEnabled( false );
            this.buyBtnHalf.setVisible( false );
            selectedHalf.pauseAnimation();
            normalHalf.pauseAnimation();
        }
        else
        {
            this.buyBtn.setEnabled( false );
            this.buyBtn.setVisible( false );
            selected.pauseAnimation();
            normal.pauseAnimation();
            this.schedule( this.updateDeal );
        }


        menu.addChild( this.buyBtn );
        menu.addChild( this.buyBtnHalf );
        menu.addChild( this.dealBtn );

        menu.addChild( this.settingItem );

        this.setGrandWheelButton( this.settingItem, menu );
        menu.addChild( lobbyItem );

        {
            var btnBar = new cc.Sprite( "#btns_bg.png" );
            btnBar.setPosition( cc.p( this.vSize.width / 2, btnBar.getContentSize().height / 2 ) );
            this.addChild( btnBar, this.mCode >= DJS_CRYSTAL_SLOT && this.mCode <= DJS_GOLDEN_SLOT ? 2 : 1 );

            var infoItem = new cc.MenuItemSprite( new cc.Sprite( "#paytable.png" ), new cc.Sprite( "#paytable_pressed.png" ), this.menuCB, this );
            this.resizer.setFinalScaling( infoItem );
            infoItem.setPosition(cc.p( infoItem.getContentSize().width/2 + 15, infoItem.getContentSize().height * 0.75 ) );
            infoItem.setTag( INFO_BTN_TAG );
            menu.addChild( infoItem );
            this.setGlow( infoItem );

            var betDownItem = new cc.MenuItemSprite( new cc.Sprite( "#betDown.png" ), new cc.Sprite( "#betDown_pressed.png" ), this.menuCB, this );
            this.resizer.setFinalScaling( betDownItem );
            betDownItem.setPosition( cc.p( infoItem.getPositionX() + infoItem.getContentSize().width * 0.9, infoItem.getPositionY() ) );
            betDownItem.setTag( BET_DOWN_BTN_TAG );
            menu.addChild( betDownItem );
            this.setGlow( betDownItem );

            var betUpItem = new cc.MenuItemSprite( new cc.Sprite( "#betUp.png" ), new cc.Sprite( "#betUp_pressed.png" ), this.menuCB, this );
            this.resizer.setFinalScaling( betUpItem );
            betUpItem.setPosition( cc.p( betDownItem.getPositionX() + infoItem.getContentSize().width * 0.9, infoItem.getPositionY() ) );
            betUpItem.setTag( BET_UP_BTN_TAG );
            menu.addChild( betUpItem );
            this.setGlow( betUpItem );

            var buyItem = new cc.MenuItemSprite( new cc.Sprite( "#buy_credits.png" ), new cc.Sprite( "#buy_credits_pressed.png" ), this.menuCB, this );
            this.resizer.setFinalScaling( buyItem );
            buyItem.setPosition( cc.p( betUpItem.getPositionX() + buyItem.getContentSize().width * 0.8, infoItem.getPositionY() ) );
            buyItem.setTag( BUY_BTN_TAG );
            menu.addChild( buyItem );
            {
                var glowSprite = new cc.Sprite( res.glow_white_png );
                glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.ScaleTo( 1, 0 ), new cc.ScaleTo( 1, 1 ) ) ) );
                glowSprite.setPosition( buyItem.getContentSize().width / 2, buyItem.getContentSize().height / 2 );
                glowSprite.setOpacity( 255 * 0.75 );
                buyItem.addChild( glowSprite );
            }

            var betMaxItem = new cc.MenuItemSprite( new cc.Sprite( "#bet_max.png" ), new cc.Sprite( "#bet_max_pressed.png" ), this.menuCB, this );
            this.resizer.setFinalScaling( betMaxItem );
            betMaxItem.setPosition( cc.p( buyItem.getPositionX() + buyItem.getContentSize().width * 0.8,
                buyItem.getPositionY() ) );
            betMaxItem.setTag( BET_MAX_BTN_TAG );
            menu.addChild( betMaxItem );
            this.setGlow( betMaxItem );

            this.spinItem = new ccui.Button();

            this.spinItem.loadTextures( "spin.png", "spin_pressed.png", "", ccui.Widget.PLIST_TEXTURE );

            this.resizer.setFinalScaling( this.spinItem );

            this.spinItem.setPosition( cc.p( betMaxItem.getPositionX() + betMaxItem.getContentSize().width * 1.25, betMaxItem.getPositionY() - 5 ) );
            this.spinItem.setTag( SPIN_BTN_TAG );

            this.spinItem.addTouchEventListener( this.touchEvent, this );
            this.addChild( this.spinItem, this.mCode >= DJS_CRYSTAL_SLOT && this.mCode <= DJS_GOLDEN_SLOT && this.mCode <= DJS_GOLDEN_SLOT ? 2 : 1 );

            var glowSprite = new cc.Sprite( res.glow_png );
            glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.ScaleTo( 1, 0 ), new cc.ScaleTo( 1, 1 ) ) ) );
            glowSprite.setPosition( this.spinItem.getContentSize().width / 2, this.spinItem.getContentSize().height / 2 );
            glowSprite.setTag( GLOW_SPRITE_TAG );
            this.spinItem.addChild( glowSprite );
        }

        menu.setPosition( cc.p( 0, 0 ) );
        this.addChild( menu, this.mCode >= DJS_CRYSTAL_SLOT && this.mCode <= DJS_GOLDEN_SLOT && this.mCode <= DJS_GOLDEN_SLOT ? 2 : 1 );
    },
    setGrandWheelButton : function( item, menu )
{
    if( ( this.data.server_inner_wheel_control<=0 ) && !CSUserdefauts.getInstance().getValueForKey( GRAND_WHEEL_SPIN_COUNT ) )
    {
        CSUserdefauts.getInstance().deleteValueForKey( GRAND_WHEEL_SPIN_TO_MAKE );
    }

    if( !this.grandWheelButton && CSUserdefauts.getInstance().getValueForKey( GRAND_WHEEL_SPIN_TO_MAKE ) )
    {
        this.grandWheelButton = new cc.MenuItemImage( res.grandWheelButton, res.grandWheelButton, this.menuCB, this );
        this.resizer.setFinalScaling( this.grandWheelButton );
        this.grandWheelButton.setPosition( cc.p( cc.rectGetMinX( item.getBoundingBox() ) - this.grandWheelButton.getContentSize().width/2,
            item.getPositionY() ) );
        this.grandWheelButton.setTag( GRANG_WHEEL_BUTTON_TAG );

        this.grandWheelButton.getSelectedImage().setColor( cc.color(160, 160, 160, 255 ) );

        var sprite = new cc.Sprite( res.grandWheelButtonFiller );



        var pTimer = new cc.ProgressTimer( sprite );
        pTimer.setTag( PROGRESS_TIMER_TAG );
        this.grandWheelButton.addChild( pTimer, 1 );
        pTimer.setPosition( cc.p( this.grandWheelButton.getContentSize().width / 2, this.grandWheelButton.getContentSize().height / 2 ) );
        pTimer.setPercentage( ( CSUserdefauts.getInstance().getValueForKey( GRAND_WHEEL_SPIN_COUNT ) / CSUserdefauts.getInstance().getValueForKey( GRAND_WHEEL_SPIN_TO_MAKE, 0 ) ) * 100 );
        pTimer.setType( cc.ProgressTimer.TYPE_RADIAL );

        this.grandWheel.checkForGrandWheelActivation( CSUserdefauts.getInstance().getValueForKey( GRAND_WHEEL_SPIN_COUNT, 0 ), CSUserdefauts.getInstance().getValueForKey( GRAND_WHEEL_BET_AMOUNT,0 ) );

        menu.addChild( this.grandWheelButton );
    }
    else
    {
        //settingItem.setPositionX( vSize.width - settingItem.getContentSize().width );
    }
},
    setpayLines : function()
    {
    },

    setSettingPage : function()
    {
        var layer = new cc.Layer();//Color( cc.color( 0, 0, 0, 166 ) );
        layer.setTag( OVERLAPPING_LAYER_TAG );
        this.addChild( layer, 10 );
        DPUtils.getInstance().setTouchSwallowing( null, layer );

        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( this.vSize.width * 0.5, this.vSize.height * 0.5 ) );
        layer.addChild( darkBG );

        var buySprite = new cc.Sprite( "#bg.png" );
        buySprite.setPosition( cc.p( this.vSize.width / 2, this.vSize.height / 2 ) );
        layer.addChild( buySprite );

        var buyMenu = new cc.Menu();

        buyMenu.setPosition( cc.p( 0, 0 ) );
        buySprite.addChild( buyMenu );

        var x = 0;//1236;
        var y = 0;
        var maxX = 0;
        var minX = 0;

        for ( var i = 2; i >= 0; i-- )
        {
            var buyBtn = null;

            /*if (i == 3)
             {
             y = 56.5;
             x = buySprite.getContentSize().width / 2;
             buyBtn = new cc.MenuItemSprite( new cc.Sprite( "#game_center.png" ), new cc.Sprite( "#game_center.png" ),  this.settingMenuCB, this );


             buyBtn.setTag( GAME_CENTER_BTN_TAG );
             }
             else*/ if ( i == 2 )
        {
            y = 56.5;//131.5;
            x = buySprite.getContentSize().width / 2;
            buyBtn = new cc.MenuItemSprite( new cc.Sprite( "#customer_support.png" ), new cc.Sprite( "#customer_support.png" ),  this.settingMenuCB, this );
            buyBtn.setTag( SUPPORT_BTN_TAG );
        }
        else if( i <= 1 )
        {
            y = 131.5;//207;
            if ( i == 1 )
            {
                buyBtn = new cc.MenuItemSprite( new cc.Sprite( "#sound.png" ), new cc.Sprite( "#sound.png" ), this.settingMenuCB, this );

                var sprite = new cc.Sprite( "#sound_disabled.png" );
                sprite.setTag( SOUND_BTN_TAG );
                buyBtn.addChild( sprite );
                sprite.setPosition( cc.p( sprite.getContentSize().width / 2, buyBtn.getContentSize().height / 2 ) );
                if ( CSUserdefauts.getInstance().getValueForKey( SOUND, true ) )
                {
                    sprite.setVisible( false );
                }

                buyBtn.setTag( SOUND_BTN_TAG );
                x = minX + buyBtn.getContentSize().width * 0.5;
            }
            else
            {
                buyBtn = new cc.MenuItemSprite( new cc.Sprite( "#music.png" ), new cc.Sprite( "#music.png" ), this.settingMenuCB, this );

                var sprite = new cc.Sprite( "#music_disabled.png" );
                sprite.setTag( MUSIC_BTN_TAG );
                buyBtn.addChild( sprite );
                sprite.setPosition( cc.p( sprite.getContentSize().width / 2, buyBtn.getContentSize().height / 2 ) );
                if ( CSUserdefauts.getInstance().getValueForKey( MUSIC, true ) )
                {
                    sprite.setVisible( false );
                }

                buyBtn.setTag( MUSIC_BTN_TAG );
                x = maxX - buyBtn.getContentSize().width * 0.5;
            }
        }

            buyBtn.setPosition( cc.p( x, y ) );
            buyBtn.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );

            buyMenu.addChild( buyBtn );
            if ( minX == 0 )
            {
                maxX = cc.rectGetMaxX( buyBtn.getBoundingBox() );
                minX = cc.rectGetMinX( buyBtn.getBoundingBox() );
            }
        }

        var versionStr = "App Version: " + APP_VERSION + ", Build Version " + BUILD_VERSION;//change 12 May
        var versionLabel = new cc.LabelTTF( versionStr, "MyriadPro-Regular", 15 );
        versionLabel.setPosition( cc.p( buySprite.getContentSize().width / 2, versionLabel.getContentSize().height * 0.75 ) );
        buySprite.addChild( versionLabel );

        var closeItem = new cc.MenuItemImage( res.Close_Button_2_png, res.Close_Button_2_png, this.buyMenuCB, this );

        closeItem.setPosition( cc.p( buySprite.getContentSize().width - closeItem.getContentSize().width / 2, buySprite.getContentSize().height - closeItem.getContentSize().height / 2 ) );
        closeItem.setTag( CLOSE_BTN_TAG );

        closeItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );

        buyMenu.addChild( closeItem );

        this.resizer.setFinalScaling( buySprite );
        buySprite.setTag(easyMoveInTag);
        DPUtils.getInstance().easyBackOutAnimToPopup(buySprite,0.3);

    },

    setScoreLabel : function()
    {
        var label = this.getChildByTag( SCORE_LABEL_TAG );

        var string = DPUtils.getInstance().getNumString( this.displayedScore );

        if( label )
        {
            label.setString( string );

            var tmpLayer = this.getChildByTag( OVERLAPPING_LAYER_TAG );

            if ( !tmpLayer )
            {
                tmpLayer = this.getChildByTag( VIP_LAYER_TAG );
            }

            if ( tmpLayer )
            {
                var tmpLabel = tmpLayer.getChildByTag( OVERLAPPING_LAYER_TAG );

                if ( !tmpLabel )
                {
                    if( tmpLayer.getChildByTag( VIP_LAYER_TAG ) )
                        tmpLabel = tmpLayer.getChildByTag( VIP_LAYER_TAG ).getChildByTag( VIP_LAYER_TAG );
                }

                if ( tmpLabel )
                {
                    tmpLabel.setString( string );

                    if( tmpLayer.getChildByTag( VIP_LAYER_TAG ) ) {
                        var lbl = tmpLayer.getChildByTag(VIP_LAYER_TAG).getChildByTag(8487);
                        if (lbl) {
                            lbl.setString(this.delegate.getVipString());
                        }
                    }
                }
            }
        }
        else
        {
            var ypos = 0.958;
            if ( ServerData.getInstance().BROWSER_TYPE == CHROME || ServerData.getInstance().BROWSER_TYPE == FIREFOX){
                ypos = 0.95;
            }
            label = new CustomLabel( string, "TT0504M_", 30, cc.size( 215, 30 ) );
            label.setColor( cc.color( 255, 255, 255 ) );
            label.setPosition( cc.p( 275, this.vSize.height * ypos ) );
            label.setTag( SCORE_LABEL_TAG );
            this.addChild( label, this.mCode >= DJS_CRYSTAL_SLOT && this.mCode <= DJS_GOLDEN_SLOT ? 2 : 1 );

            //this.addChild( delegate.IAP );

            var coinSpriteSmall = new cc.Sprite( res.Coin_Small_png );
            coinSpriteSmall.setPosition( cc.p( 150, label.getPositionY() ) );
            //coinSpriteSmall.setScale( 0.25 );
            this.resizer.setFinalScaling( coinSpriteSmall );
            this.addChild( coinSpriteSmall, this.mCode >= DJS_CRYSTAL_SLOT && this.mCode <= DJS_GOLDEN_SLOT ? 2 : 1 );
        }
    },
    setReelContainerPosition : function()
    {
        var yPos = 20;
       if (this.reelsContainer) {
           var string =  "res/" + machinesString[this.mCode - DJS_QUINTUPLE_5X] + "/reel_Shadow.png";
           var sprite = new cc.Sprite( string );
           if ( sprite )
           {
               //sprite.setPosition( cc.p( this.vSize.width*0.58 /*-sprite.getContentSize().width*0.68*/, this.vSize.height * 0.5  + yPos) );
               sprite.setPosition( cc.p( this.reelsContainer.getContentSize().width*0.58 /*-sprite.getContentSize().width*0.68*/, this.reelsContainer.getContentSize().height * 0.5  + yPos) );

               this.reelsContainer.addChild( sprite, 1000 );
               this.reelsContainer.setPosition( cc.p( this.vSize.width*0.58 , this.vSize.height * 0.5 + origin.y + yPos ) );
           }
       }

    },
    setSlotScreenPosition : function()
    {
        var width = this.vSize.width - this.slots[0].getContentSize().width - this.rankingBase.getContentSize().width * this.rankingBase.getScaleX();
        var height = this.vSize.height - this.slots[0].getContentSize().height;
        width/=2;
        height/=2;
        if ( width >= 60 && height >= 180 )
        {
            while ( width > 60 ) {
                this.slots[0].setScale( this.slots[0].getScale() + 0.05 );
                width = this.vSize.width - this.slots[0].getContentSize().width * this.slots[0].getScale() - this.rankingBase.getContentSize().width * this.rankingBase.getScaleX();
                width/=2;
            }
        }
        this.slots[0].setPosition( cc.p( this.vSize.width - this.slots[0].reelDimentions.x * 0.5 * this.slots[0].getScale() - width, this.vSize.height / 2 + 60 ) );

        var fgSprite = null;
        switch ( this.mCode ) {
            case DJS_FREEZE:
                fgSprite = new cc.Sprite( "#frame.png" );
                this.scaleAccordingToSlotScreen( fgSprite, true );
                fgSprite.setPosition( this.vSize.width - fgSprite.getContentSize().width / 2 , this.vSize.height * 0.5 + 54 );
                this.addChild( fgSprite, 1 );
                break;

            case DJS_CRYSTAL_SLOT:
                this.slots[0].setPositionY( this.slots[0].getPositionY() - 30 );
                break;

            case DJS_NEON_SLOT:
                this.slots[0].setPositionY( this.slots[0].getPositionY() - 25 );
                break;

            default:
                break;
        }
    },

    checkForSpecialOfferPopUp : function()
    {
        if ( this.delegate.showLevelUp || this.isAutoSpinning || this.haveWild )
        {
            return;
        }

        var showSaleViewCount = this.def.getValueForKey("showSaleViewCount", 0);
        var occur_wins = this.def.getValueForKey( "no_games_show_advert", 0 );

        if ( showSaleViewCount < this.data.server_salepopup_frequency && Math.floor( Math.random() * 5 ) == 0 && this.def.getValueForKey(LEVEL, 0) >= 5 )//todo ask for the "occur_wins" condition
        {
            if ( this.currentWin >= this.currentBet * this.totalSlots * 10 )
            {
                this.canShowSpecialOfferPopUp = true;
            }
        }
        //cc.log( "out checkForSpecialOfferPopUp" );
    },

    setSpecialOfferPop : function()
    {
        //cc.log( "in setSpecialOfferPop" );


        var showSaleViewCount = this.def.getValueForKey("showSaleViewCount", 0);
        var currentOfferCode = 1;
            switch ( this.data.server_buy_page )
            {
                case 2:
                    if ( this.def.getValueForKey(IS_BUYER, false) ) {
                        currentOfferCode = BuyPageTags.SPECIAL_OFFER_1;
                    }
                    else
                    {
                        currentOfferCode = BuyPageTags.SPECIAL_OFFER_2;
                    }
                    break;

                case 3:
                    currentOfferCode = BuyPageTags.SPECIAL_OFFER_2;
                    break;

                case 4:
                    currentOfferCode = BuyPageTags.SPECIAL_OFFER_3;
                    break;

                default:
                    currentOfferCode = BuyPageTags.SPECIAL_OFFER_1;
                    break;
            }
        if( cc.textureCache.getTextureForKey( "res/lobby/buyPage_1_19/specialofferpopup/" + ( currentOfferCode - BuyPageTags.SPECIAL_OFFER_1 + 1 ) + ".png" ) ) {

            var offer = new SpecialOffers(currentOfferCode);
            this.addChild(offer, 6);
            DPUtils.getInstance().setTouchSwallowing(null, offer);
            offer.setPosition(cc.p(0, 0));
            offer.setTag(VIP_LAYER_TAG);
            this.def.setValueForKey("showSaleViewCount", showSaleViewCount + 1);
            this.canShowSpecialOfferPopUp = false;
        }else{
            PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_TOUCH_SPECIAL_OFFER_POP);
        }
    },
    isCheckUnlockBet:function( betValue){
    var isNotLocked = false;
    if(this.def.getValueForKey( LEVEL, 0 )>=13){
        isNotLocked=true;
    }else{
        if (this.def.getValueForKey( LEVEL, 0 )===0) {
            if(betValue<=100)isNotLocked=true;
        }else if (this.def.getValueForKey( LEVEL, 0 )===1||this.def.getValueForKey( LEVEL, 0 )===2) {
            if(betValue<=500)isNotLocked=true;
        }else if (this.def.getValueForKey( LEVEL, 0 )===3||this.def.getValueForKey( LEVEL, 0 )===4) {
            if(betValue<=1000)isNotLocked=true;
        }else if (this.def.getValueForKey( LEVEL, 0 )===5||this.def.getValueForKey( LEVEL, 0 )===6) {
            if(betValue<=5000)isNotLocked=true;
        }else if (this.def.getValueForKey( LEVEL, 0 )===7||this.def.getValueForKey( LEVEL, 0 )===8) {
            if(betValue<=10000)isNotLocked=true;
        }else if (this.def.getValueForKey( LEVEL, 0 )===9||this.def.getValueForKey( LEVEL, 0 )===10) {
            if(betValue<=50000)isNotLocked=true;
        }else if (this.def.getValueForKey( LEVEL, 0 )===11||this.def.getValueForKey( LEVEL, 0 )===12) {
            if(betValue<=100000)isNotLocked=true;
        }
    }
    return isNotLocked;
},
    getMaxBetIndexAccordToLevel:function(){
        if(this.def.getValueForKey( LEVEL, 0 )<13){
            if (this.def.getValueForKey( LEVEL, 0 )===0) {
                return 1;
            }else if (this.def.getValueForKey( LEVEL, 0 )===1||this.def.getValueForKey( LEVEL, 0 )===2) {
                return 2;
            }else if (this.def.getValueForKey( LEVEL, 0 )===3||this.def.getValueForKey( LEVEL, 0 )===4) {
                return 3;
            }else if (this.def.getValueForKey( LEVEL, 0 )===5||this.def.getValueForKey( LEVEL, 0 )===6) {
                return 4;
            }else if (this.def.getValueForKey( LEVEL, 0 )===7||this.def.getValueForKey( LEVEL, 0 )===8) {
                return 5;
            }else if (this.def.getValueForKey( LEVEL, 0 )===9||this.def.getValueForKey( LEVEL, 0 )===10) {
                return 6;
            }else if (this.def.getValueForKey( LEVEL, 0 )===11||this.def.getValueForKey( LEVEL, 0 )===12) {
                return 7;
            }
        }
        return this.betRangeVec.length - 1;
    },
    settingMenuCB : function ( pSender )
    {
        var menuItm = pSender;
        var tag = menuItm.getTag();

        switch ( tag )
        {
            case MUSIC_BTN_TAG:
                if ( CSUserdefauts.getInstance().getValueForKey(MUSIC, true) )
                {
                    menuItm.getChildByTag( tag ).setVisible( true );

                    this.delegate.jukeBox( S_STOP_MUSIC );
                    CSUserdefauts.getInstance().setValueForKey(MUSIC, false);
                }
                else
                {
                    menuItm.getChildByTag( tag ).setVisible( false );

                    CSUserdefauts.getInstance().setValueForKey(MUSIC, true);
                    this.delegate.jukeBox( S_AMBIENCE );
                }
                break;

            case SOUND_BTN_TAG:
                if ( CSUserdefauts.getInstance().getValueForKey(SOUND, true) )
                {
                    menuItm.getChildByTag( tag ).setVisible( true );

                    this.delegate.jukeBox( S_STOP_EFFECTS );
                    CSUserdefauts.getInstance().setValueForKey(SOUND, false);
                }
                else
                {
                    CSUserdefauts.getInstance().setValueForKey(SOUND, true);
                    menuItm.getChildByTag( tag ).setVisible( false );
                }
                break;

            case SUPPORT_BTN_TAG:
                if (support_url && support_url.length>0 && window.open(support_url)){

                }else{
                    var email = this.def.getValueForKey("userEmail");
                    var subject = 'Feedback - Double Jackpot Slots (Support Id :'+userDataInfo.getInstance().user_id+')';
                    var bodyMesage = "[Please Explain what is happening here]"+"\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nModel: "+ServerData.getInstance().device_model+"\nDevice_Version: "+ServerData.getInstance().device_version+"\n App Version: "+APP_VERSION+"\nEmail id: "+email;
                    location.href = this.getMailtoUrl("support@phonato.com",subject,bodyMesage);
                }
                break;

            case GAME_CENTER_BTN_TAG:
                //SendMessageWithParams( "gameCenterAction", null );
                break;

            default:
                break;
        }
    },
    //falseWinLabel : null,
    physicalReelElements:null,
    splPhysicalReelElements:null,
    probabilityArray_Server:null,
    setVariables : function( lvl )
    {
        this.isMoveToLobby=false;
        this.isWinningOccured = false;
        AppDelegate.getInstance().showingScreen = false;
        switch ( lvl )
        {
            case DJS_RESPIN:
                this.totalElements = 45;
                this.probabilityArray = new Array( 5 );// ( int * ) malloc( sizeof( int ) * 45 );
                for ( var i = 0; i < this.probabilityArray.length; i++ )
                {
                    this.probabilityArray[i] = new Array();
                }
                break;

            default:
                this.probabilityArray = new Array( 5 );
                for ( var i = 0; i < this.probabilityArray.length; i++ )
                {
                    this.probabilityArray[i] = new Array();
                }
                break;
        }

        this.def = UserDef.getInstance();

        this.setTag( sceneTags.GAME_SCENE_TAG );

        this.mCode = lvl;

        this.reelI = 2;

        this.activeScreens = -1;

        this.freeSpinCount = 0;

        this.totalSlots = 1;

        this.resourceCount = 0;

        this.currentWin = 0;

        this.currentWinForcast = 0;

        this.displayedWin = 0;

        this.totalScore = userDataInfo.getInstance().totalChips;//this.def.getValueForKey( CHIPS, 5000000 );

        this.setInitialBet();

        this.displayedScore = this.totalScore;

        this.canStop = true;

        this.canChangeBet = true;

        this.advCalled = false;

        this.haltSpin = false;

        this.loadingResources = true;

        this.touchPressed = false;

        this.isRolling = false;

        this.isAutoSpinning = false;

        this.haveFreeSpins = false;

        this.haveReversal = false;

        this.wildAnimating = false;

        this.haveWild = false;

        this.callForAdOnWinning = false;

        this.didBet = false;

        this.grandWheel = GrandWheel.getInstance( this );

        //this.canShowSpecialOfferPopUp = false;


        for ( var i = 0; i < 4; i++ )
        {
            this.slots[i] = null;
        }

        /*for ( var i = 0; i < 3; i++ )
         {
         for ( var j = 0; j < 3; j++ )
         {
         this.resultArray[i][j] = -1;
         }
         this.prevReelIndex[i] = -1;
         }*/

        this.resultArray = new Array( );

        this.prevReelIndex = new Array();

        this.lineVector = new Array();

        var clo = 3;
        if( this.mCode == DJS_RNF_STAR || this.mCode == DJS_X_MULTIPLIERS )
        {
            clo = 4;
        }

        for ( var i = 0; i < clo; i++ )
        {
            this.resultArray[i] = new Array();
            for ( var j = 0; j < clo; j++ )
            {
                this.resultArray[i][j] = -1;
            }
            this.prevReelIndex[i] = -1;
        }

        this.drawNode = new cc.DrawNode();
        this.drawNode.setPosition( cc.p( 0, 0 ) );
        this.addChild( this.drawNode, 7 );

        this.vSize = cc.winSize;

        this.resizer = DJSResizer.getInstance();

        //analyticsHelper = DJSAnalyticsHelper.getInstance();



        this.def.setValueForKey(LAST_PLAYED_THEME, machinesString[this.mCode - DJS_QUINTUPLE_5X]);

        this.easyModeCode = BIASING_MODE_97;

        if ( this.def.getValueForKey(BET, 0) <= CSUserdefauts.getInstance().getValueForKey(MINIMUM_EASY_MODE_BET_LIMIT, 0) )
        {
            this.easyModeCode = BIASING_MODE_140;
        }
        else if ( this.def.getValueForKey(MACHINE_TUNNING, 0) == 1 )
        {
            this.easyModeCode = BIASING_MODE_85;
        }
        else if ( this.def.getValueForKey(MACHINE_TUNNING, 0) == 2 )
        {
            this.easyModeCode = BIASING_MODE_92;
        }
        else
        {
            if ( CSUserdefauts.getInstance().getValueForKey(EASY_MODE_BET, 0) > 0 )
            {
                this.easyModeCode = BIASING_MODE_140;
            }
        }


        if ( this.totalScore >= CHIPS_CAP_LIMIT )
        {
            this.easyModeCode = BIASING_MODE_85;
        }

        this.def.setValueForKey(CURRENT_SCENE_CODE, sceneTags.GAME_SCENE_TAG);
        ServerData.getInstance().updateActiveGameMachineOnServer("3_ReelSlots",machinesString[this.mCode-DJS_QUINTUPLE_5X]);
         this.def.setValueForKey("last_played_theme",machinesString[this.mCode-DJS_QUINTUPLE_5X]);
        ServerData.getInstance().machineData = null;
        ServerData.getInstance().TRICKY_MACHINE_CODE = 0;
        ServerData.getInstance().loadSlotData(this.mCode);

        // if( ServerData.getInstance().machineData && this.mCode == ServerData.getInstance().TRICKY_MACHINE_CODE )
        // {
        //     this.updateServerData();
        //
        // }

        /*connectionRay = null;
         connectionRay2 = null;
         connectionLabel = null;
         nextIn = null;
         timerLabel = null;
         good = null;
         finalRank = null;
         rank = null;

         retryItem = null;
*/
        //delegate.haveNetwork( 2 );




        /*this.falseWinLabel = new cc.LabelTTF("False Win generated", "GEORGIAB", 80 );
         this.falseWinLabel.setPosition( this.vSize.width / 2, this.vSize.height * 0.75 );
         this.falseWinLabel.setColor( cc.color( 0, 255, 0 ) );
         this.addChild( this.falseWinLabel, 100 );*/
        PopUps.getInstance().listOfPopUps = [];

    },
    addnotMatchPhysicalReel:function(){
        var sp = new cc.Sprite(res.GlowIconPng);
        sp.setPosition(cc.p(this.vSize.width-sp.getContentSize().width*2,this.vSize.height*0.8));
        this.addChild(sp,10);
    },
    addnotMatchProbability:function(){
        var sp = new cc.Sprite(res.notMatch_png);
        var posY = this.vSize.height*0.8;
        if (this.dailyChallenge_Btn){
            posY = this.dailyChallenge_Btn.getPositionY();
        }
        sp.setPosition(cc.p(this.vSize.width-sp.getContentSize().width,posY));
        this.addChild(sp,10);
    },
    updateServerData:function(){
        if( ServerData.getInstance().machineData && this.mCode === ServerData.getInstance().Machine_Code )
        {
           // console.log("Machine Loaded Called");

            var isVerified = true;
            var isPhysicalVerified = true;
            var slot_config = ServerData.getInstance().machineData;
            var bet_range_array = DPUtils.getInstance().extractVectorFromSring(slot_config["bet_range_array"]);
            if (bet_range_array && bet_range_array.length>0){
                this.betRangeVec =  bet_range_array;
            }
            var response_array = DPUtils.getInstance().extractVectorFromSring(slot_config["physical_reel"]);
            if (response_array && response_array.length>=36){
                this.physicalReelElements = response_array;
                if (this.physicalArrayCheck.length>=36) {
                    for (var idx = 0; idx < this.physicalReelElements.length; idx++) {
                            if (this.physicalReelElements[idx]!==this.physicalArrayCheck[idx]){
                                isPhysicalVerified = false;
                                break;
                            }
                    }
                }
            }else {
                isPhysicalVerified = false;
            }
            if (!isPhysicalVerified){
                ServerData.getInstance().TRICKY_MACHINE_CODE = 0;
                this.addnotMatchPhysicalReel();
            }
            response_array = DPUtils.getInstance().extractVectorFromSring(slot_config["special_physical_reel"]);
            if (isPhysicalVerified && response_array && response_array.length>=36){
                this.splPhysicalReelElements = response_array;
            }

            var probabilitykeys = ["payout_array_85","payout_array_92","payout_array_97","payout_array_140","payout_array_300"];



            if (slot_config[probabilitykeys[0]].length>=36 && slot_config[probabilitykeys[4]].length>=36&& slot_config[probabilitykeys[3]].length>=36&& slot_config[probabilitykeys[1]].length>=36 && slot_config[probabilitykeys[2]].length>=36){
                 if (this.probabilityArraryCheck.length>4){
                     for (var index =0;index<this.probabilityArraryCheck.length;index++){
                         var array = this.probabilityArraryCheck[index];
                         var serverArray = DPUtils.getInstance().extractVectorFromSring(slot_config[probabilitykeys[index]]);

                         for (var idx=0;idx<array.length;idx++){
                             if (array[idx]!=serverArray[idx]){
                                 isVerified = false;
                                 break;
                             }
                         }
                         if (!isVerified){
                             break;
                         }
                     }
                 }
            }else{
                isVerified = false;
            }
            if (!isVerified){
                ServerData.getInstance().TRICKY_MACHINE_CODE = 0;
            }
            var isCheck = true;
            this.probabilityArray = this.probabilityArraryCheck;
            for( var i = 0; i < 5; i++ ) {
                this.probabilityArray[i] = [];
                this.extractArrayFromSring(i, slot_config[probabilitykeys[i]]);
                if (this.totalElements !== this.probabilityArray[i].length ) {
                    isCheck = false;
                    ServerData.getInstance().TRICKY_MACHINE_CODE = 0;
                    for (var j = 0; j < this.probabilityArray.length; j++) {
                        this.probabilityArray[j] = [];
                    }
                    break;
                }
               // console.log("Machine Loaded Called Initialized");
            }
            if (isCheck){
                this.probabilityArrayServer = this.probabilityArray;
                ServerData.getInstance().TRICKY_MACHINE_CODE = ServerData.getInstance().Machine_Code;
                this.updateServerDataOnMachine();
                this.changeTotalProbabilities();
            }else{
                if (this.mCode === DJS_GOLDEN_SLOT ||this.mCode === DJS_NEON_SLOT || this.mCode === DJS_CRYSTAL_SLOT||this.mCode === DJS_SPIN_TILL_YOU_WIN )
                    this.probabilityArray = this.probabilityArray[this.easyModeCode];
                else
                    this.probabilityArray = this.probabilityArray;
            }
            if ( !isVerified ){
                this.addnotMatchProbability();
            }
        }

    },
    totalElements : 36,
    addMegaWinCoins : function( dNode )
    {
        this.delegate.jukeBox( S_COIN_MORE );

        for (var i = 0; i < 10; i++)
        {

            var r = Math.random();
            var time = r + 0.5;
            var cSprite = new cc.Sprite( res.Coin_png );
            cSprite.setScale( 0.75 );
            this.resizer.setFinalScaling( cSprite );
            cSprite.setPosition( Math.floor( Math.random() * this.vSize.width ), this.vSize.height + cSprite.getContentSize().height * 0.75 );
            cSprite.runAction( new cc.RepeatForever( new cc.RotateBy( 0.75 + r, 0, 360, 0 ) ) );
            //cSprite.runAction(ScaleTo.create(1, 0.4));
            cSprite.setTag( i );

            cSprite.runAction( new cc.Sequence( new cc.DelayTime( time ), new cc.MoveTo( 1, cc.p( cSprite.getPositionX(), -cSprite.getContentSize().height * 0.5 ) ),
                cc.callFunc( this.animCB, this) ) );

            dNode.addChild(cSprite, 1);
        }
    },

    animCB : function( node )
    {
        var r = Math.random()
        var time = r + 0.5;

        node.setPosition( Math.floor( Math.random() * this.vSize.width ), this.vSize.height + node.getContentSize().height * 0.75 );

        node.runAction( new cc.Sequence( new cc.DelayTime( time ), new cc.MoveTo( 1, cc.p( node.getPositionX(), -node.getContentSize().height * 0.5 ) ),
            cc.callFunc(  this.animCB, this ) ) );
    },

    setMegaWinAnimation : function()
    {
        if( this.getChildByTag( MEGA_WIN_TAG ) )
        {
            return;
        }
        this.delegate.jukeBox( S_MEGA_WIN );

        var dNode = new cc.DrawNode();

        dNode.setPosition( cc.p( 0, 0 ) );
        dNode.setTag( MEGA_WIN_TAG );

        var color = cc.color( 0, 0, 0, 160 );
        //dNode.drawRect( cc.p( 0, 0 ), cc.p( this.vSize.width, this.vSize.height ), color, this.vSize.height, color );

        //animNodeVec.push_back( dNode );

        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        dNode.addChild( darkBG );

        var node = new cc.Node();
        dNode.addChild( node );

        var node2 = new cc.Node();
        dNode.addChild( node2 );

        var degree = 0;
        for ( var i = 0; i < 4; i++ )
        {
            var light = new cc.Sprite( "res/megaWin/effect_line.png" );

            light.setRotation( degree );
            node.addChild( light );

            var light2 = new cc.Sprite( "res/megaWin/effect_line.png" );

            light2.setRotation( degree + 45 );
            node2.addChild( light2 );

            degree+=90;
        }
        node.setPosition( cc.p ( this.vSize.width / 2, this.vSize.height / 2 ) );
        node.runAction( new cc.RepeatForever( new cc.RotateBy( 5.0, 360 ) ) );
        node2.setPosition( cc.p ( this.vSize.width / 2, this.vSize.height / 2 ) );
        node2.runAction( new cc.RepeatForever( new cc.RotateBy( 5.0, -360 ) ) );

        //DPUtils.getInstance().setTouchSwallowing( dNode );

        var normal = this.getChildByTag( MEGA_ANIM_WIN_TAG );
        if ( normal )
        {
            normal.setVisible( true );
            normal.start();

            var prizeLabel = normal.getChildByTag( MEGA_ANIM_WIN_TAG );

            prizeLabel.setString( DPUtils.getInstance().getNumString( this.currentWin ) );

            if( ServerData.getInstance().isShareEnabled[SHARE_MEGA_WIN] )//change 12 May
            {
                var shareItem = new cc.MenuItemImage( res.shareButton_png, res.shareButton_png, this.menuCB, this );
                shareItem.setPosition( cc.p( normal.getPositionX() + normal.getContentSize().width / 2,
                    cc.rectGetMinY( normal.getBoundingBox() ) - shareItem.getContentSize().height - normal.getContentSize().height ) );
                shareItem.setTag( SHARE_BTN_TAG );


                var menu = new cc.Menu( shareItem );
                menu.setPosition( cc.p( 0, 0 ) );
                dNode.addChild( menu );
            }

            normal = this.getChildByTag( MEGA_TEXT_WIN_TAG );
            normal.setVisible( true );
            normal.start();
        }

        this.addMegaWinCoins( dNode );

        this.addChild( dNode, 8 );




    },

    setWinAnimation : function( animCode, screen )
    {
        var tI = 0.3;

        var firee;
        for (var i = 0; i < this.animNodeVec.length; i++)
        {
            firee = this.animNodeVec[i];
            firee.removeFromParent(true);
        }
        this.animNodeVec = [];


        switch (animCode)
        {
            case WIN_TAG:
                for (var i = 0; i < this.slots[0].blurReels.length; i++)
                {
                    var r = this.slots[0].blurReels[i];

                    var node = r.getParent();

                    //Texture2D * texture = null;
                    //TextureCache * tCache = Director.getInstance().getTextureCache();

                    var moveW = this.slots[0].reelDimentions.x * this.slots[0].getScale();
                    var moveH = this.slots[0].reelDimentions.y * this.slots[0].getScale();

                    for (var j = 0; j < 4; j++)
                    {
                        var fire = new cc.ParticleSun();
                        fire.texture = cc.textureCache.addImage( res.FireSnow_png );

                        switch ( this.mCode )
                        {
                            case DJS_TRIPLE_PAY:
                            case DJS_DIAMONDS_FOREVER:
                                fire.setStartColor( cc.color( 0, 255, 0 ) );
                                break;

                            case DJS_DIAMOND_3X:
                                fire.texture = cc.textureCache.addImage( res.FireStar_png );
                                fire.setStartColor( cc.color( 255, 255, 0 ) );
                                fire.setEndColor( cc.color( 255, 0, 0 ) );
                                break;

                            case DJS_DIAMOND:
                                fire.texture = cc.textureCache.addImage( res.FireStar_png );
                                fire.setStartColor( cc.color( 0, 255, 0 ) );
                                break;

                            case DJS_WILD_X:
                                fire.texture = cc.textureCache.addImage( res.FireStar_png );
                                fire.setStartColor( cc.color( 249, 94, 254 ) );
                                break;

                            case DJS_CRAZY_CHERRY:
                                fire.texture = cc.textureCache.addImage( res.FireStar_png );
                                break;

                            default:
                                break;
                        }

                        var pos;

                        this.addChild(fire, 5);
                        this.animNodeVec.push(fire);
                        var moveBy = null;
                        var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                        diff*=( 2 - i );

                        switch (j % 4)
                        {
                            case 0:
                                pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(moveW, 0));
                                break;

                            case 1:
                                pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(0, moveH));
                                break;

                            case 2:
                                pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(-moveW, 0));
                                break;

                            case 3:
                                pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(0, -moveH));
                                break;

                            default:
                                break;
                        }

                        fire.runAction( new cc.RepeatForever( new cc.Sequence( moveBy, moveBy.reverse() ) ) );
                    }
                }

                break;

            case BIG_WIN_TAG:
                for(var i = 0; i < this.slots[0].blurReels.length; i++)
                {
                    var r = this.slots[0].blurReels[i];

                    var node = r.getParent();

                    var glowSprite = null;

                    var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                    diff*=( 2 - i );

                    switch ( this.mCode )
                    {
                        case DJS_QUINTUPLE_5X:
                        case DJS_FREE_SPINNER:
                            //case DJS_FIRE:
                            glowSprite = new cc.Sprite( glowStrings[0] );
                            break;

                        case DJS_DIAMOND_3X:
                            glowSprite = new cc.Sprite( glowStrings[4] );
                            break;

                        case DJS_DIAMOND:
                        case DJS_TRIPLE_PAY:
                            glowSprite = new cc.Sprite( glowStrings[3] );
                            break;

                        case DJS_DIAMONDS_FOREVER:
                        case DJS_CRAZY_CHERRY:
                        case DJS_WILD_X:
                            glowSprite = new cc.Sprite( glowStrings[2] );
                            break;

                        default:
                            break;
                    }

                    if (glowSprite)
                    {
                        glowSprite.setPosition( node.getPositionX() + this.slots[0].getPositionX() - diff, node.getPositionY() + this.slots[0].getPositionY() );


                        this.addChild(glowSprite, 5);
                        this.animNodeVec.push(glowSprite);
                        //scaleAccordingToSlotScreen( glowSprite );
                        glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo(0.3, 100), new cc.FadeTo(0.3, 255) ) ) );
                    }
                }
                break;

            default:
                break;
        }
    },

    setWinLabel : function()
    {
        var label = this.getChildByTag( WIN_LABEL_TAG );
        var string = DPUtils.getInstance().getNumString( this.displayedWin );

        if( label )
        {
            label.setString( string );

            if( this.getChildByTag( BIG_WIN_LAYER_TAG ) )
            {
                var lbl = this.getChildByTag( BIG_WIN_LAYER_TAG ).getChildByTag( BIG_WIN_LAYER_TAG );
                lbl.setString( string );
                while ( lbl.getContentSize().width * lbl.getScale() > this.vSize.width * 0.9 )
                {
                    lbl.setScale( lbl.getScale() - 0.01 );
                }
                //log( "lbl.W = %f", lbl.getContentSize().width );
            }
        }
        else
        {
            var shiftX = 0;
            if ( this.mCode === DJS_X_MULTIPLIERS )
            {
                shiftX = 98;
            }
            var heading = new cc.LabelTTF( "WIN", "GEORGIAB", 25 );
            heading.setPosition( cc.p( this.vSize.width * 0.62 - heading.getContentSize().width - shiftX, heading.getContentSize().height * 5.6 ) );
            heading.setColor( cc.color( 255, 255, 0 ) );
            this.addChild( heading, 3 );//change 6 Dec

            var lbl = new cc.LabelTTF( "00,000,000,000", "DS-DIGIT", 60 );
            lbl.setAnchorPoint( cc.p( 0, 0.5 ) );
            var isIE = /*@cc_on!@*/false || !!document.documentMode;//changed 9 May for IE
            if( isIE ) {
                lbl.setPosition(cc.p(this.vSize.width * 0.6 - shiftX, lbl.getContentSize().height * 2.25));
            }
            else
            {
                lbl.setPosition( cc.p( this.vSize.width * 0.6 - shiftX, lbl.getContentSize().height * 2.5 ) );
            }
            lbl.setOpacity( 50 );
            this.addChild( lbl, 3 );//change 6 Dec

            label = new cc.LabelTTF( string, "DS-DIGIT", 60 );
            label.setAnchorPoint( cc.p( 1, 0.5 ) );

            if( isIE ) {//changed 9 May for IE
                label.setPosition( cc.p( this.vSize.width * 0.6 + lbl.getContentSize().width - shiftX, label.getContentSize().height * 2.25 ) );
            }
            else
            {
                label.setPosition( cc.p( this.vSize.width * 0.6 + lbl.getContentSize().width - shiftX, label.getContentSize().height * 2.5 ) );
            }

            label.setTag( WIN_LABEL_TAG );
            this.addChild( label, 3 );//change 6 Dec
        }
    },
    userRatingIndex:0,
    starMenuCB : function( pSender )
    {
        var itm = pSender;
        var tag = itm.getTag();
        this.delegate.jukeBox(S_BTN_CLICK);
        switch ( tag )
        {
            case ALLOW_BTN_TAG:
            {
                var drawNode = this.getChildByTag( STAY_IN_TOUCH_POP_TAG );
                if (drawNode)
                {
                    drawNode.removeAllChildren( true );
                    drawNode.removeFromParent( true );
                }
                if (this.userRatingIndex !== 0){
                    if(this.userRatingIndex === 5){
                        cc.sys.openURL( "https://www.facebook.com/doublejackpotslots/?fref=tsm" );
                        CSUserdefauts.getInstance().setValueForKey("kFBRatedCurrentVersion",true);
                    }
                }
               // this.delegate.assignRewards( 2000 );
                this.delegate.showingScreen = false;
                showLikeUsBtn( false );

            }
                PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_TOUCH_BRIBE_POP);
                ServerData.getInstance().updateCustomDataOnServer();


                break;

            case CLOSE_BTN_TAG:
            {
                var drawNode = this.getChildByTag( STAY_IN_TOUCH_POP_TAG );
                if (drawNode)
                {
                    drawNode.removeAllChildren( true );
                    drawNode.removeFromParent( true );
                }
               // showLikeUsBtn( false );
                this.delegate.showingScreen = false;

                PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_TOUCH_BRIBE_POP);

                ServerData.getInstance().updateCustomDataOnServer();
            }
                break;

            default:
            {
                var drawNode = this.getChildByTag( STAY_IN_TOUCH_POP_TAG );

                if ( drawNode )
                {
                    var drawNodeSprite = drawNode.getChildByTag( STAY_IN_TOUCH_POP_TAG );
                   if (drawNodeSprite) {
                       for (var i = 1; i < 6; i++) {
                           if (drawNodeSprite.getChildByTag(i)){
                               var menuItemImage = drawNodeSprite.getChildByTag(i);
                               menuItemImage.setVisible(false);
                           }

                       }
                       if(drawNodeSprite.getChildByTag( tag )){
                           var menuItemImage = drawNodeSprite.getChildByTag(tag);
                           menuItemImage.setVisible(true);
                       }
                       this.userRatingIndex = tag;

                   }
                }

                CSUserdefauts.getInstance().setValueForKey(APP_RATE_STARS, this.userRatingIndex);
                ServerData.getInstance().updateCustomDataOnServer();

            }
                break;
        }

        this.delegate.jukeBox( S_BTN_CLICK );
    },

    startLoadingMechanism : function( mCode )
    {

        this.addPlists( null, null );

        this.updateLoadingResource();
    },
    startRolling : function( isWild )
    {
        this.removeMegaWinAnimation();
        this.removeChildByTag( BIG_WIN_LAYER_TAG );
        //analyticsHelper.checkAndUploadData();

        this.isSpinningFast = false;
       // if(this.mCode!=DJS_XTREME_MACHINE) {
            if (this.slots[0].dNode) {
                var col = cc.color(0.65 * 255, 0.65 * 255, 0.65 * 255/*, 0.4 * 255*/);//Change 19 May
                for (var j = 0; j < this.totalSlots; j++) {
                    this.slots[j].dNode.clear();
                    for (var i = 0; i < this.slots[j].totalReels; i++) {
                        this.slots[j].dNode.drawSegment(cc.p(this.slots[j].reelMids[i * 3], this.slots[j].reelMids[(i * 3) + 1]),
                            cc.p(this.slots[j].reelMids[(i * 3) + 2], this.slots[j].reelMids[(i * 3) + 1]), 2.5, col);
                    }
                }
            }
        //}

        this.isWildGeneration = isWild;
        this.delegate.jukeBox( S_STOP_EFFECTS );
        for ( var i = 0; i < this.animNodeVec.length; i++ )
        {
            var node = this.animNodeVec[ i ];
            node.removeFromParent( true );
        }
        this.animNodeVec = [];


        for ( var i = 0; i < this.totalSlots; i++ )
        {
            if(this.slots[i]) {
                this.slots[i].startRoll(isWild);
            }else {
                this.reelsContainer.startRoll( isWild );
            }
            if ( !this.haveReversal )
            {
                this.generateResult( i );

                this.checkAndSetControl( i );//change 29 May
            }
        }
        this.activeScreens = this.totalSlots;

        this.schedule( this.rollUpdator );
            if (DJS_X_MULTIPLIERS == this.mCode) {
                this.schedule(this.reelStopper, 0.3);
            } else {
                this.schedule(this.reelStopper, 0.5);
            }
        var num = Math.floor(Math.random() * 14);
           // console.log(""+num);
        this.delegate.jukeBox( (num>=0 && num<15)?num+S_REEL_1:S_REEL_1 );


        /* switch ( this.mCode ) {
             case DJS_FIRE:
                 this.delegate.jukeBox( Math.floor( Math.random() * 3 ) + S_REEL_ALT_1 );
                 break;

             case DJS_FREEZE:
             case DJS_BONUS_MACHINE:
                 this.delegate.jukeBox( Math.floor( Math.random() * 3 ) + S_REEL_ALT_4 );
                 break;

             default:
                 this.delegate.jukeBox( Math.floor( Math.random() * 3 ) + S_REEL_1 );
                 break;
         }*/

    },

    checkAndSetControl : function( screenCode )//Change 29 May
    {
        if( this.mCode == DJS_RESPIN || this.mCode == DJS_FREE_SPIN )
        {
            return;
        }

        var control = ServerData.getInstance().control;
        /*var winFactor = 0;

         switch ( control )
         {
         case 1:
         winFactor = 25;
         break;

         case 2:
         winFactor = 10;
         break;
         }*/

        if( control != 0 && this.currentWinForcast > control * this.currentBet )
        {
            //cc.log( "this.currentWinForcast = " + this.currentWinForcast );
            while ( true )
            {
                this.generateResult( screenCode );
                //this.calculatePayment( false );

                if( this.currentWinForcast <= control * this.currentBet )
                {
                    //cc.log( "Here result with control generated" );
                    break;
                }
            }
        }
    },

    touchEvent : function( pSender, type )
    {
        /*if ( this.def.MUSIC )
         {
         if ( !this.delegate.sPlayer.isBackgroundMusicPlaying() )
         {
         this.delegate.jukeBox( S_AMBIENCE );
         }
         }
         if ( this.delegate.getChangingScene() ) {
         return;
         }*/
        if (this.delegate.showingScreen){
            return;
        }

        if ( this.haveReversal || this.wildAnimating || this.getChildByTag( OVERLAPPING_LAYER_TAG ) || this.haveFreeSpins || this.freeSpinCount > 0 )
        {
            return;
        }

        switch ( type )
        {
            case ccui.Widget.TOUCH_BEGAN:

                if ( !this.haltSpin )
                {
                    this.touchPressed = true;
                    this.autoSpinJustStarted = false;
                    if ( this.isAutoSpinning )
                    {
                        touchPressed = false;
                        this.removeAutoSpinning();
                    }
                    else if( !this.isRolling )
                    {
                        /*if(this.mCode!=DJS_XTREME_MACHINE) {
                            for (var i = 0; i < this.totalSlots; i++) {
                                this.slots[i].setReelsDirection(1);
                            }
                        }*/
                        for (var i = 0; i < this.totalSlots; i++) {
                            this.slots[i].setReelsDirection(1);
                        }
                        var node = new cc.Node();
                        node.setTag( AUTO_SPIN_TAG );

                        node.runAction( new cc.Sequence( new cc.DelayTime( 1.0 ), cc.callFunc(  this.setAutoSpin, this ) ) );
                        this.addChild( node );
                    }
                }

                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                if ( !this.haltSpin )
                {
                    if ( !this.autoSpinJustStarted )
                    {
                        if( this.isRolling )
                        {
                            if ( this.canStop )
                            {
                                for ( var ab = 0; ab <= 14; ab++ )
                                {
                                    this.delegate.jukeBoxStop( S_REEL_1 + ab );
                                }
                                 cc.log("jukeBoxStop");
                                this.delegate.jukeBoxStop( S_EXCITEMENT );


                                this.unschedule(  this.reelStopper );
                               // if(this.mCode!=DJS_XTREME_MACHINE) {

                                    while (this.activeScreens > 0) {
                                        this.reelStopper(-1);
                                    }
                               // }
                            }
                            this.isSpinningFast = true;
                        }
                        else
                        {
                            if ( this.totalScore >= this.currentBet * this.totalSlots )
                            {
                                //this.falseWinLabel.setVisible( false );
                              //  if(this.mCode!=DJS_XTREME_MACHINE) {
                                    for (var i = 0; i < this.totalSlots; i++) {
                                        this.slots[i].setReelsDirection(1);
                                    }
                               // }
                                this.haveFreeSpins = false;
                                this.freeSpinCount = 0;
                                this.canStop = false;
                                this.scheduleOnce( this.canStopCB, 0.25 );
                                if( this.betIndicator ) this.betIndicator.setVisible( false );
                                this.removeChildByTag( 2101 );
                                this.checkAndApplyStrategy();
                                this.surpriseTimeDealy =0.5;
                                this.isWinningOccured = false;
                                this.spin();
                                var percentOfbar = VSDailyChallenge.getInstance().getStatusOfCurrentDailyChallenge();
                                this.dailyChallengeFillBar.setPercentage(percentOfbar);                                //this.def.setValueForKey( CHIPS, this.totalScore );
                                userDataInfo.getInstance().totalChips = this.totalScore;
                                this.delegate.setLevel( this.def.getValueForKey(LEVEL, 0), CSUserdefauts.getInstance().getValueForKey(THIS_LEVEL_BET, 0) + this.currentBet * this.totalSlots, this );

                                var spins = this.def.getValueForKey(TOTAL_PLAYED, 0);
                                this.def.setValueForKey(TOTAL_PLAYED, spins + 1 );
                            }
                            else
                            {
                                this.setInsufficientPage();
                            }
                        }
                    }

                    if ( this.getChildByTag( AUTO_SPIN_TAG ) )
                    {
                        this.removeChildByTag( AUTO_SPIN_TAG );
                    }
                }
                this.touchPressed = false;
                break;

            case ccui.Widget.TOUCH_CANCELED:
                this.touchPressed = false;
                break;

            default:
                break;
        }

    },
    applyStrategy : false,
    checkAndApplyStrategy : function()
{
    if( !CSUserdefauts.getInstance().getValueForKey( STRATEGY_ASSIGNED ) && this.def.getValueForKey( LEVEL ) < 5 && !this.def.getValueForKey( IS_BUYER ) && ServerData.getInstance().server_strategy)
    {
        var randomNo = Math.floor( Math.random() * 1000 ) + 2000;
        if( this.totalScore < randomNo && this.currentBet <= 1000 )
        {
            this.applyStrategy = true;
            if( ServerData.getInstance().biasing_status==1 )
            {//hack
                var heading = new cc.LabelTTF( "Strategy win generated", "GEORGIAB", 50 );
                heading.setPosition( cc.p( this.vSize.width * 0.5, this.vSize.height * 0.85 ) );
                //heading.setColor( Color3B::YELLOW );
                this.addChild( heading, 50 );
                //heading.setTag( 2101 );
            }
        }
    }
},
    updateCoin : function( dt )
    {
        this.displayedScore+=this.addScore;
        if ( this.displayedScore >= this.totalScore )
        {
            this.displayedScore = this.totalScore;
            this.unschedule( this.updateCoin );
        }
        this.setScoreLabel();

        var l = this.getChildByTag( OVERLAPPING_LAYER_TAG );
        if ( l )
        {

            if ( l.getChildByTag( 444 ) )
            {
                var label = l.getChildByTag( 444 ).getChildByTag( VIP_LAYER_TAG );

                var label2 = this.getChildByTag( SCORE_LABEL_TAG );

                label.setString( label2.getString() );
            }
        }
    },

    /*
     * Udate deal Functionality
     */
    updateDeal : function( dt )
    {
        var deal = this.delegate.getDealInstance();//DJSDeal.getInstance();

        var tim = deal.getDealStatus();

        if ( tim )
        {
            var label = this.dealBtn.getChildByTag( TIMER_LABEL_TAG );
            label.setString( tim );

            if ( !this.dealBtn.isEnabled() )
            {
                this.dealBtn.setEnabled( true );
                this.dealBtn.setVisible( true );

                var normal = this.dealBtn.getNormalImage();
                normal.resumeAnimation();

                normal = this.dealBtn.getSelectedImage();
                normal.resumeAnimation();

                this.buyBtnHalf.setEnabled( true );
                this.buyBtnHalf.setVisible( true );
                this.normal = this.buyBtnHalf.getNormalImage();
                this.normal.resumeAnimation();

                normal = this.buyBtnHalf.getSelectedImage();
                normal.resumeAnimation();

                this.buyBtn.setEnabled( false );
                this.buyBtn.setVisible( false );
                normal = this.buyBtnHalf.getNormalImage();
                normal.pauseAnimation();

                normal = this.buyBtnHalf.getSelectedImage();
                normal.pauseAnimation();
            }
        }
        else
        {
            if ( this.dealBtn.isEnabled() )
            {
                this.dealBtn.setEnabled( false );
                this.dealBtn.setVisible( false );

                //deal.resetDealData();

                var normal = this.dealBtn.getNormalImage();
                normal.pauseAnimation();

                normal = this.dealBtn.getSelectedImage();
                normal.pauseAnimation();

                this.buyBtnHalf.setEnabled( false );
                this.buyBtnHalf.setVisible( false );
                normal = this.buyBtnHalf.getNormalImage();
                normal.pauseAnimation();

                normal = this.buyBtnHalf.getSelectedImage();
                normal.pauseAnimation();

                this.buyBtn.setEnabled( true );
                this.buyBtn.setVisible( true );
                normal = this.buyBtnHalf.getNormalImage();
                normal.resumeAnimation();

                normal = this.buyBtnHalf.getSelectedImage();
                normal.resumeAnimation();
            }
        }
    },

    /*
     * updateLevelBar
     */
    updateLevelBar : function( dt )
    {
        var lBar = this.getChildByTag( LEVEL_FILL_TAG );
        lBar.setPercent( lBar.getPercent() + this.percentAddFector );
        if ( lBar.getPercent() >= CSUserdefauts.getInstance().getValueForKey( LEVEL_BAR, 0 ) )
        {
            lBar.stopAllActions();
            lBar.runAction( new cc.FadeIn( 0.2 ) );
            lBar.setPercent( CSUserdefauts.getInstance().getValueForKey( LEVEL_BAR, 0 ) );
            this.unschedule( this.updateLevelBar );
        }
    },

    updateLoadingResource : function( dt )
    {
        this.setGameScene();
        this.loadingResources = false;
        /*if ( this.mCode != DJS_X_MULTIPLIERS )
         {
         this.delegate.tournamentToFG();
         }*/

       if( ServerData.getInstance().biasing_status==1 )
       {//hack
           var heading = new cc.LabelTTF( "EasyMode = " + this.easyModeCode, "GEORGIAB", 50 );
           heading.setPosition( cc.p( this.vSize.width * 0.6, this.vSize.height * 0.8 ) );
           heading.setFontFillColor( cc.color( 255, 255, 0 ) );
           this.addChild( heading, 5 );
           heading.setTag( 2100 );
           var arrayData = DPUtils.getInstance().getNumString( CSUserdefauts.getInstance().getValueForKey( GRAND_WHEEL_BET_AMOUNT ) );

            var wheelBet = new cc.LabelTTF( "Bet = " + arrayData, "GEORGIAB", 50 );
            wheelBet.setPosition( cc.p( this.vSize.width * 0.6, this.vSize.height * 0.6 ) );
            wheelBet.setFontFillColor( cc.color( 255, 255, 0 ) );
            this.addChild( wheelBet, 5 );
            wheelBet.setTag( 21011 );
            if (this.probabilityArray.length>0){
                arrayData = this.probabilityArray[this.easyModeCode];
            }

           var arrayprobability = new cc.LabelTTF( "Array = " + arrayData, "GEORGIAB", 17 );
           arrayprobability.setPosition( cc.p( this.vSize.width * 0.01, this.vSize.height * 0.27 ) );
           arrayprobability.setFontFillColor( cc.color( 255, 255, 0 ) );
           this.addChild( arrayprobability, 5 );
           arrayprobability.setAnchorPoint(0,0.5);
           arrayprobability.setTag( 210111 );


         }
    },
    enableScreen:function(){
        delegate.showingScreen=false;
    },
    updateWinCoin : function( dt )
    {
        this.displayedWin+=this.addWin;
        if ( this.displayedWin >= this.currentWin )
        {
            this.displayedWin = this.currentWin;
            this.unschedule(  this.updateWinCoin );
        }
        this.setWinLabel();
    },

    setupBribePopup : function()
    {
        var showOtherPop = false;
        if (!this.isAutoSpinning && this.currentWin >= this.currentBet * this.totalSlots * 25 && this.data.server_bribe_popup && !this.data.isAppratingShown){
            if ( !CSUserdefauts.getInstance().getValueForKey( "kFBRatedCurrentVersion", false ) && CSUserdefauts.getInstance().getValueForKey(RATING_SHOWN_COUNT,0)<=2 && this.def.getValueForKey(LEVEL,0)>this.data.server_after_xgames_show_bribepopup && CSUserdefauts.getInstance().getValueForKey(APP_RATE_STARS,-1)<5 && userDataInfo.getInstance().totalChips>=BRIBE_TOTAL_CHIPS_LIMIT){
                showOtherPop = true;
                PopUps.getInstance().addPopUpIndex(PopUpTags.STAY_TOUCH_BRIBE_POP);
                this.data.isAppratingShown = true;
                //this.setAppRatingPopUp();
            }
        }

        return showOtherPop;
    },
    troom1 : false,
    troom2 : true,
    troom3 : true,
    setCurrentTournament : function()
    {
        if ( this.delegate.stopTournament )
        {
            if( this.currentBet >= 50 && this.currentBet <= 500 )
            {
                this.delegate.tournaments[0].setVisible( true );
                this.delegate.tournaments[1].setVisible( false );
                this.delegate.tournaments[2].setVisible( false );
                this.troom1 = true;
                this.troom2 = false;
                this.troom3 = false;
            }
            else if( this.currentBet > 500 && this.currentBet <= 10000 )
            {
                this.delegate.tournaments[0].setVisible( false );
                this.delegate.tournaments[1].setVisible( true );
                this.delegate.tournaments[2].setVisible( false );
                this.troom1 = false;
                this.troom2 = true;
                this.troom3 = false;
            }
            else if( this.currentBet >= 50000 )
            {
                this.delegate.tournaments[0].setVisible( false );
                this.delegate.tournaments[1].setVisible( false );
                this.delegate.tournaments[2].setVisible( true );
                this.troom1 = false;
                this.troom2 = false;
                this.troom3 = true;
            }
        }
    },
    connectingRay : null,
    connectingRay2 : null,
    connectionLabel : null,
    connectingTournament : function( dt )
    {
        //delegate.setNetForGame( false );
        //delegate.haveNetwork( 2 );
        if ( this.retryItem )
        {
            this.retryItem.getParent().getParent().removeFromParent( true );
            this.retryItem = null;
        }

        if ( this.connectionRay != null )
        {
            this.connectionRay.setVisible( true );
            this.connectionRay2.setVisible( true );

        }
        else
        {
            this.connectionRay = new cc.Sprite( "#cst_tournament_ray.png" );
            this.connectionRay.setPosition( cc.p( this.rankingBase.getContentSize().width * 0.5, this.rankingBase.getContentSize().height * 0.5 ) );


            this.connectionRay2 = new cc.Sprite( "#cst_tournament_ray.png" );
            this.connectionRay2.setPosition( cc.p( this.rankingBase.getContentSize().width * 0.5, this.rankingBase.getContentSize().height * 0.5 ) );
            this.connectionRay2.setScaleY( 4.0 );
            this.connectionRay2.setOpacity( 120 );

            this.rankingBase.addChild( this.connectionRay );
            this.rankingBase.addChild( this.connectionRay2 );
        }
        var rot = new cc.RotateBy( 1.5, 360 );
        this.connectionRay.stopAllActions();
        this.connectionRay.runAction( new cc.Spawn( new cc.Repeat( rot, 2 + Math.floor( Math.random() * 3 ) ), new cc.Sequence( cc.callFunc( function(){
            this.connectionLabel.setString( "INITIALIZING." );

        }, this ), new cc.DelayTime( 0.7 ), cc.callFunc( function(){
            this.connectionLabel.setString( "CONNECTING." );

        }, this), new cc.DelayTime( 0.7 ), cc.callFunc( function(){
            this.connectionLabel.setString( "CONNECTING.." );

        }, this), new cc.DelayTime( 0.7 ), cc.callFunc( function(){
            this.connectionLabel.setString( "CONNECTING..." );

        }, this), new cc.DelayTime( 0.7 ), cc.callFunc( function(){
            this.connectionLabel.setString( "CONNECTING." );

        }, this), cc.callFunc( this.showTournament, this, 0 ) ) ) );



        if ( this.connectionLabel != null )
        {
            this.connectionLabel.setString( "CONNECTING..." );
            this.connectionLabel.setVisible( true );
        }
        else
        {
            this.connectionLabel = new cc.LabelTTF( "INITIALIZING...","arial_black", 19.5 );
            this.connectionLabel.setAnchorPoint( cc.p( 0, 0.5 ) );
            this.connectionLabel.setPosition( cc.p( cc.rectGetMinX( this.rankingBase.getBoundingBox() ) + 15, this.rankingBase.getContentSize().height * 0.5 ) );
            this.rankingBase.addChild( this.connectionLabel );
        }
    },
    showTournament : function( dt )
    {
        this.connectionRay2.setVisible( false );

        this.connectionRay.setVisible( false );
        this.connectionLabel.setVisible( false );

        //if ( this.delegate.getNetForGame() )
        {
            if( this.delegate.showNewTournament )
            {
                this.delegate.initiateTournament( 0 );
            }

            if ( this.delegate.stopTournament )
            {
                for ( var i = 0; i < 3; i++ )
                {
                    this.delegate.tournaments[i].setSch();
                    this.rankingBase.addChild( this.delegate.tournaments[i] );
                    if ( i == 0 )
                    {
                        this.delegate.tournaments[i].setVisible( true );
                    }
                    else
                    {
                        this.delegate.tournaments[i].setVisible( false );
                    }
                }
            }
            this.setCurrentTournament();
        }
        /*else
         {
         Sprite * noConSprite = SpriteWithSpriteFrameName( "conection_lost.png" );
         noConSprite.setPosition( cc.p( origin.x + rankingBase.getContentSize().width * 0.5, origin.y + rankingBase.getContentSize().height * 0.5 ) );
         rankingBase.addChild( noConSprite );

         retryItem = MenuItemSprite( SpriteWithSpriteFrameName( "retry_btn.png" ), SpriteWithSpriteFrameName( "retry_btn.png" ), CC_CALLBACK_1( GameScene::reConnectMenuCB, this ) );

         retryItem.setPosition( cc.p( rankingBase.getContentSize().width / 2 , rankingBase.getContentSize().height * 0.2 ) );
         retryItem.setTag( CLOSE_BTN_TAG );

         retryItem.getSelectedImage().setColor( Color3B::GRAY );

         Menu * menu = Menu( retryItem, NULL );
         menu.setPosition( Vec2::ZERO );
         noConSprite.addChild( menu );
         }*/
    },

    reConnectMenuCB : function( sender )
    {
        /*delegate.setNetForGame( false );
         delegate.haveNetwork( 2 );

         this.retryItem.getParent().getParent().removeFromParentAndCleanup( true );

         this.retryItem = nullptr;

         this.connectingTournament( 0 );*/
    },
    finalTPrize : 0,
    setTournamentOver : function()
    {
        if( this.delegate.stopTournament && this.troom1 )
        {
            this.finishingRank = this.delegate.tournaments[0].finalPlayerRank;
            this.finalTPrize = this.delegate.tournaments[0]._finalPrizePool;

            if ( this.finishingRank < 4 )
            {
                switch ( this.finishingRank )
                {
                    case 1:
                        this.finalTPrize = 0.6 * ( this.delegate.tournaments[0]._finalPrizePool );
                        break;
                    case 2:
                        this.finalTPrize = 0.25 * ( this.delegate.tournaments[0]._finalPrizePool );
                        break;
                    case 3:
                        this.finalTPrize = 0.15 * ( this.delegate.tournaments[0]._finalPrizePool );
                        break;
                    default:
                        break;
                }

            }
            this.showPlayerResults();
            this.delegate.releaseTournament();
            this.scheduleOnce( this.freshTournament, 30.0 );
            this.scheduleOnce( this.connectingTournament, 30.0 );

        }
        if( this.delegate.stopTournament && this.troom2 )
        {
            this.finishingRank = this.delegate.tournaments[1].finalPlayerRank;
            this.finalTPrize = this.delegate.tournaments[1]._finalPrizePool;

            if ( this.finishingRank < 4 )
            {
                switch ( this.finishingRank )
                {
                    case 1:
                        this.finalTPrize = 0.6 * ( this.delegate.tournaments[1]._finalPrizePool );
                        break;
                    case 2:
                        this.finalTPrize = 0.25 * ( this.delegate.tournaments[1]._finalPrizePool );
                        break;
                    case 3:
                        this.finalTPrize = 0.15 * ( this.delegate.tournaments[1]._finalPrizePool );
                        break;
                    default:
                        break;
                }

            }

            this.showPlayerResults();


            this.delegate.releaseTournament();
            this.scheduleOnce( this.freshTournament, 30.0 );
            this.scheduleOnce( this.connectingTournament, 30.0 );
        }
        if( this.delegate.stopTournament && this.troom3 )
        {
            this.finishingRank = this.delegate.tournaments[2].finalPlayerRank;
            this.finalTPrize = this.delegate.tournaments[2].prizeAmt;

            if ( this.finishingRank < 4 )
            {
                switch ( this.finishingRank )
                {
                    case 1:
                        this.finalTPrize = 0.6 * ( this.delegate.tournaments[2]._finalPrizePool );
                        break;

                    case 2:
                        this.finalTPrize = 0.25 * ( this.delegate.tournaments[2]._finalPrizePool );
                        break;

                    case 3:
                        this.finalTPrize = 0.15 * ( this.delegate.tournaments[2]._finalPrizePool );
                        break;

                    default:
                        break;
                }

            }

            this.showPlayerResults();

            this.delegate.releaseTournament();
            this.scheduleOnce( this.freshTournament, 30.0 );
            this.scheduleOnce( this.connectingTournament, 30.0 );
        }
    },
    nextIn : null,
    good : null,
    rank : null,
    showPlayerResults : function()
    {
        if( this.finishingRank < 4  && this.finishingRank > 0 )
        {
            ServerData.getInstance().updateData("win_tournament","1","","singleincrement");
            this.removeAutoSpinning();
            this.removeMegaWinAnimation();
            if ( this.getChildByTag( BIG_WIN_LAYER_TAG )){
                this.removeChildByTag( BIG_WIN_LAYER_TAG );
                this.displayedWin = this.currentWin;
                this.unschedule(  this.updateWinCoin );
                this.setWinLabel();
                this.displayedScore = this.totalScore;
                this.unschedule( this.updateCoin );
                this.setScoreLabel();
                this.delegate.jukeBox( S_STOP_EFFECTS );
                this.delegate.showingScreen=false;
            }
            var tWin = new tournamentWin();
            var strplace = "" + this.finishingRank;
            switch ( this.finishingRank )
            {
                case 1:
                    strplace = "" + this.finishingRank + "st";
                   // DailyChallenges.getInstance().updateTaskData( C_WIN_TOURNAMENT, 1 );
                    break;

                case 2:
                    strplace = "" + this.finishingRank + "nd";
                    break;

                case 3:
                    strplace = "" + this.finishingRank + "rd";
                    break;

                default:
                    break;
            }

            var color = cc.color( 0, 0, 0, 0.8 );
            //this.drawNode.drawRect( cc.p( 0, 0 ), cc.p( this.vSize.width, this.vSize.height ), color, this.vSize.height, color );

            var lblwin = new cc.LabelTTF( DPUtils.getInstance().getNumString( this.finalTPrize ), "CarterOne", 45 );
            //lblwin.enableOutline( cc.color( 189, 103, 2, 255 ), 2 );
            lblwin.setColor( cc.color( 255, 222, 0 ) );

            var lblplace = new cc.LabelTTF( strplace, "HELVETICALTSTD-BOLD", 60);

//cc.log( "cc.rectGetMaxY(tWin.collectButton.getBoundingBox()) " + cc.rectGetMaxY(tWin.collectButton.getBoundingBox()) + " lblwin.getContentSize().height " + lblwin.getContentSize().height );
            lblwin.setPosition( cc.p( tWin.collectButton.getPositionX(), tWin.collectButton.getPositionY() + tWin.collectButton.getContentSize().height * 0.45
                + lblwin.getContentSize().height * 0.75 ) );
            lblplace.setPosition( cc.p( cc.rectGetMinX(tWin.winBase.getBoundingBox()) + tWin.winBase.getContentSize().width * 0.5, tWin.winBase.getPositionY() + lblplace.getContentSize().height * 1.5 ) );
            //lblplace.enableOutline( cc.color( 143, 82, 4, 255 ), 2 );
            lblplace.setColor( cc.color( 164, 93, 3 ) );
            tWin.addChild( lblplace );
            tWin.addChild( lblwin );
            this.addChild( tWin, 7 );
            tWin.setCallBack2( this, this.updateScoreTWin );
        }

        this.tournamentPause = false;
        this.nextTournamentTimer = 30.0;
        this.schedule( this.tTimer, 1 );
        //delegate.haveNetwork( 2 );
        if ( this.nextIn != null )
        {
            this.nextIn.setString( "Next In:" );
            this.nextIn.setVisible( true );
        }
        else
        {
            this.nextIn = new cc.LabelTTF( "Next In:","arial_black", 25 );
            this.nextIn.setPosition( cc.p( this.rankingBase.getContentSize().width * 0.5, this.rankingBase.getContentSize().height * 0.77 ) );
            this.rankingBase.addChild( this.nextIn );

        }
        if( this.timerLabel != null )
        {
            this.timerLabel.setString( ( "" + this.nextTournamentTimer ) );
            this.timerLabel.setVisible( true );
        }
        else
        {
            this.timerLabel = new cc.LabelTTF( "" + this.nextTournamentTimer, "arial_black", 40 );
            this.timerLabel.setPosition( cc.p( this.rankingBase.getContentSize().width * 0.5, cc.rectGetMinY(this.nextIn.getBoundingBox()) - this.timerLabel.getContentSize().height * 0.3 ) );
            this.timerLabel.setColor( cc.color( 0, 255, 0 ) );
            //this.timerLabel.setAlignment( CC_DLL TextHAlignment::CENTER );
            //this.timerLabel.enableGlow( cc.color( 0, 255, 0 ) ) ;
            this.rankingBase.addChild( this.timerLabel );
        }
        if( this.good != null )
        {
            if ( this.finishingRank < 4 && this.finishingRank > 0 )
            {
                this.good.setString( "Incredible!" );
                this.good.setVisible( true );

            }
            else if ( this.finishingRank <= 10 && this.finishingRank > 0 )
            {
                this.good.setString( "Excellent!" );
                this.good.setVisible( true );
            }
            else if ( this.finishingRank <= 30 && this.finishingRank > 0 )
            {
                this.good.setString( "Very Good!" );
                this.good.setVisible( true );
            }
            else if ( this.finishingRank <= 50 && this.finishingRank > 0 )
            {
                this.good.setString( "Good!" );
                this.good.setVisible( true );
            }
            else
            {
                this.good.setVisible( false );
            }
        }
        else
        {
            this.good = new cc.LabelTTF( " ","arial_black", 30 );
            this.good.setPosition( cc.p( this.rankingBase.getContentSize().width * 0.5, cc.rectGetMinY( this.timerLabel.getBoundingBox() ) - this.good.getContentSize().height * 0.3 ) );
            //this.good.enableOutline( cc.color( 0, 0, 0 ) );
            this.good.setColor( cc.color( 255, 127, 0 ) );
            if ( this.finishingRank < 4 )
            {
                this.good.setString( "Incredible!" );
            }
            this.rankingBase.addChild( this.good );
        }
        if( this.finalRank != null )
        {
            this.finalRank.setString( "FINAL RANK" );
            this.finalRank.setVisible( true );
        }
        else
        {
            this.finalRank = new cc.LabelTTF( "FINAL RANK", "arial_black", 20 );
            this.finalRank.setPosition( cc.p( this.rankingBase.getContentSize().width * 0.5, cc.rectGetMinY(this.good.getBoundingBox())-this.finalRank.getContentSize().height * 0.2 ) );
            this.rankingBase.addChild( this.finalRank );
        }

        if( this.rank != null )
        {
            this.rank.setString( this.setTheRank( this.finishingRank ) );
            this.rank.setVisible( true );
        }
        else
        {
            this.rank = new cc.LabelTTF( "" + this.finishingRank, "arial_black", 60 );
            this.rank.setString( this.setTheRank( this.finishingRank ) );
            this.rank.setPosition( cc.p( this.rankingBase.getContentSize().width * 0.5, cc.rectGetMinY(this.finalRank.getBoundingBox()) - this.rank.getContentSize().height * 0.3 ) );
            //this.rank.setAlignment( CC_DLL TextHAlignment::CENTER );
            //this.rank.enableShadow();
            this.rank.setColor( cc.color( 255, 255, 0 ) );
            this.rankingBase.addChild( this.rank );
        }


        if ( this.connectionRay != null )
        {
            this.connectionRay.setVisible( true );
            this.connectionRay2.setVisible( true );

        }
        this.connectionRay.stopAllActions();
        var rot = new cc.RotateBy( 1.2, 360 );
        this.connectionRay.runAction( new cc.RepeatForever( rot ) );
    },

    setTheRank : function( rank )
    {
        var rankStr = null;
        if ( this.finishingRank % 10 == 1 ) {
            rankStr = "" + this.finishingRank + "st";
        }
        else if ( this.finishingRank % 10 == 2 ){
            rankStr = "" + this.finishingRank + "nd";
        }
        else if ( this.finishingRank % 10 == 3 ){
            rankStr = "" + this.finishingRank + "rd";
        }
        else {
            rankStr = "" + this.finishingRank + "th";
        }
        return rankStr;
    },

    freshTournament : function( dt )
    {
        this.delegate.initiateTournament( 0 );
    },

    tTimer : function( dt )
    {
        this.nextTournamentTimer--;
        if( this.nextTournamentTimer <= 0 )
        {
            this.hideWaitingTournament();
        }
        else
        {
            this.timerLabel.setString( "" + this.nextTournamentTimer );
        }
    },

    updateScoreTWin : function( a )
    {
        //this.drawNode.clear();
        AppDelegate.getInstance().assignRewards( a.finalTPrize,true );
    },

    hideWaitingTournament : function()
    {
        this.unschedule( this.tTimer );
        if( this.tournamentPause ) // when app got running in backgroud n started again
        {
            //if( cc.Scheduler.isScheduled( this.freshTournament, this ) )
            {
                this.unschedule( this.freshTournament );
                this.unschedule( this.connectingTournament );
            }
        }
        if( this.nextIn != null )
        {
            this.connectionRay.setVisible( false );
            this.connectionRay2.setVisible( false );

            this.nextIn.setVisible( false );
            this.timerLabel.setVisible( false );
            this.good.setVisible( false );
            this.finalRank.setVisible( false );
            this.rank.setVisible( false );
        }
    },
    grandWheel : null,
    resetGrandWheel : function()
    {


        var userDefaults = CSUserdefauts.getInstance();

        userDefaults.deleteValueForKey( GRAND_WHEEL_BET_AMOUNT );
        userDefaults.deleteValueForKey( GRAND_WHEEL_SPIN_COUNT );

        this.grandWheel.grandWheelActive = false;
        var pTimer = this.grandWheelButton.getChildByTag( PROGRESS_TIMER_TAG );
        var pTimer1 = pTimer.getChildByTag( PROGRESS_TIMER_ANIM_TAG );
        if (pTimer){

            pTimer.stopAllActions();
            pTimer.setRotation( 0.0 );
            pTimer.setPercentage( 0.0 );
            pTimer.setOpacity( 255 );
            var pTimer1 = pTimer.getChildByTag( PROGRESS_TIMER_ANIM_TAG );
            if (pTimer1){
                pTimer1.stopAllActions();
                pTimer1.removeFromParent( PROGRESS_TIMER_ANIM_TAG );
            }

            //pTimer.removeChildByTag( PROGRESS_TIMER_TAG );
        }
        if (pTimer1){
            pTimer1.stopAllActions();
            pTimer1.removeChildByTag( PROGRESS_TIMER_ANIM_TAG );
        }

        //check for reavailability of the giant wheel here

        if( this.data.server_inner_wheel_control<=0  )
        {
            if (this.grandWheelButton){
                this.grandWheelButton.setEnabled( false );
                this.grandWheelButton.setVisible( false );
                this.grandWheelButton.removeFromParent( true );
                this.grandWheelButton = null;
            }
            userDefaults.deleteValueForKey( GRAND_WHEEL_SPIN_TO_MAKE );
            userDefaults.setValueForKey( GRAND_WHEEL_SPIN_TO_MAKE, 0 );

        }
        else
        {
            if( this.data.server_inner_wheel_control>0  )
            {
                if( this.data.server_inner_wheel_control < MINIMUM_SPIN_COUNT ){
                    userDefaults.setValueForKey( GRAND_WHEEL_SPIN_TO_MAKE, MINIMUM_SPIN_COUNT );
                    userDefaults.deleteValueForKey(GRAND_WHEEL_SPIN_COUNT);
                    userDefaults.setValueForKey( GRAND_WHEEL_SPIN_COUNT, 0 );


                }
                else{
                    userDefaults.setValueForKey( GRAND_WHEEL_SPIN_TO_MAKE, this.data.server_inner_wheel_control );
                    userDefaults.deleteValueForKey(GRAND_WHEEL_SPIN_COUNT);
                    userDefaults.setValueForKey( GRAND_WHEEL_SPIN_COUNT, 0 );
                }
            }
        }
        ServerData.getInstance().updateCustomDataOnServer();
    }

});
