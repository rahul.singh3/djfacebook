/**
 * Created by RNF-Mac11 on 2/2/17.
 */


InfoScene = cc.Layer.extend({

     bgSprite :null,

    ctor:function ( code ) {
        this._super( /*cc.color( 255, 0, 0, 255 )*/ );
        //var pRet = new CustomButton();
        //if ( pRet && pRet.init() ){
        this.create( code );
        return true;
        //}
    },
    scrollView:null,

    create:function ( lvl ) {
        var visibleSize = cc.winSize;

        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 220 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        this.addChild( darkBG );

        this.bgSprite = new cc.Sprite("res/paytable_bg.png");

        this.bgSprite.setPosition( visibleSize.width / 2, -visibleSize.height / 2 );
        this.addChild(this.bgSprite);

        var scollFrameSize = cc.size( visibleSize.width - 15, visibleSize.height - 210 );
        this.scrollView = new ccui.ScrollView();
        //scrollView.setScrollBarEnabled( true );
        //scrollView.setScrollBarColor( cc.color( 255, 255, 0 , 255 ) );
        //scrollView.setBackGroundColorType(Layout::BackGroundColorType::NONE);
        //scrollView.setBackGroundColor(Color3B(200, 200, 200));
        this.scrollView.setContentSize(scollFrameSize);
        this.scrollView.setPosition( cc.p(2, 50) );
        this.scrollView.setDirection( ccui.ScrollView.DIR_VERTICAL );
        this.scrollView.setBounceEnabled(true);

        this.scrollView.setScrollBarAutoHideEnabled( false )
        this.scrollView.setScrollBarOpacity(255);
        this.scrollView.setScrollBarColor( cc.color( 255, 255, 255, 255 ) );
        this.scrollView.setScrollBarPositionFromCornerForVertical( cc.p( 48, 0 ) );

        var delegate = AppDelegate.getInstance();

        var height = 0;
        for (var i = 10; i >= 0; i--)
        {
            var sprite = null;
            var string = null;
            switch (lvl)
            {
                case DJS_X_MULTIPLIERS:
                    string = "res/" + machinesString[0] + "/paytable_" + (i + 1) + ".png";
                    //texture = tCache.getTextureForKey(string.getCString());
                    break;

                default:
                    string = "res/" + machinesString[lvl - DJS_QUINTUPLE_5X] + "/paytable_" + ( i + 1 ) + ".png";
                    //texture = tCache.getTextureForKey(string.getCString());
                    break;
            }

            sprite = new cc.Sprite( string );



            if ( sprite && sprite.getContentSize().height )
            {
                var resizer = DJSResizer.getInstance();

                resizer.setFinalScaling( sprite );

                sprite.setPosition(visibleSize.width / 2, height + ( ( sprite.getContentSize().height / 2 ) * sprite.getScaleY() ) );
                this.scrollView.addChild(sprite);
                height+=(sprite.getContentSize().height * sprite.getScaleY() );
            }
        }

        this.bgSprite.addChild(this.scrollView);

        var containerSize = cc.size(scollFrameSize.width, height);
        this.scrollView.setInnerContainerSize(containerSize);

        /*******Back Button************/
        var backSprite = new cc.Sprite(res.Close_Button_2_png);
        var backSprite2 = new cc.Sprite(res.Close_Button_2_png);
        backSprite2.setColor( cc.color( 169, 169, 169 ) );
        var backItem = new cc.MenuItemSprite( backSprite,backSprite2, null, this.menuCB, this );

        backItem.setPosition( cc.p( this.bgSprite.getContentSize().width - backItem.getContentSize().width, this.bgSprite.getContentSize().height - backItem.getContentSize().width  ) );

        backItem.setTag( BACK_TAG );

        var menu = new cc.Menu(backItem);
        menu.setPosition( cc.p( 0, 0 ) );
        this.bgSprite.addChild(menu, 2);

        DPUtils.getInstance().setTouchSwallowing( null, this );
        /*******Back Button************/
        this.bgSprite.runAction(new cc.Sequence(new cc.DelayTime(0.1), new cc.MoveTo(0.4, cc.p( visibleSize.width / 2, visibleSize.height / 2 ))));
        //this.scrollHandler();
    },

    menuCB : function( pSender)
    {
        var delegate = AppDelegate.getInstance();

        var menuItm = pSender;
        var tag = menuItm.getTag();

        switch (tag)
        {
            case BACK_TAG:
                //if ( !backPressed )
            {
                var visibleSize = cc.winSize;
                this.bgSprite.runAction( new cc.MoveTo(0.4, cc.p( visibleSize.width / 2, -visibleSize.height / 2 )));
                this.runAction( new cc.Sequence( new cc.DelayTime(0.42), cc.callFunc( function( ) {
                    this.removeFromParent(true);
                }, this ) ) );


                delegate.jukeBox( S_BTN_CLICK );
            }
                break;

            default:
                break;
        }
    },
/*scrollHandler:function()
{
    cc.eventManager.addListener({
        event: cc.EventListener.MOUSE,
        onMouseMove: function(event){
            //var str = "MousePosition X: " + event.getLocationX() + "  Y:" + event.getLocationY();
            // do something...
        },
        onMouseUp: function(event){
            //var str = "Mouse Up detected, Key: " + event.getButton();
            // do something...
        },
        onMouseDown: function(event){
            //var str = "Mouse Down detected, Key: " + event.getButton();
            // do something...
        },
        onMouseScroll: function(event){
            //var str = "Mouse Scroll detected, X: " + event.getScrollX() + "  Y:" + event.getScrollY();

            // do something...
            var self = event.getCurrentTarget();

            var nMoveY = event.getScrollY() * 0.5;

            //cc.log( str );

            var inner = self.scrollView.getInnerContainer();
            var curPos  = inner.getPosition();
            var nextPos = cc.p(curPos.x, curPos.y + nMoveY);

            // prevent scrolling past beginning
            if (nextPos.y > 0)
            {
                //inner.setPosition(cc.p(0, nextPos.y));
                self.scrollView._startAutoScrollToDestination(cc.p(nextPos.x, 0), 0.5, true);
                return;
            }

            //auto ws = Director::getInstance()->getWinSize();
            var size = self.scrollView.getContentSize();
            var innerSize = inner.getContentSize();
            var topMostY = size.height - innerSize.height;

            // prevent scroll past end
            if (nextPos.y < topMostY)
            {
                self.scrollView._startAutoScrollToDestination(cc.p(nextPos.x, topMostY), 0.5, true);
                return;
            }

            self.scrollView._startAutoScrollToDestination(nextPos, 0.5, true);
        }
    },this);
}*/
    handleScroll: function(event){
        //var str = "Mouse Scroll detected, X: " + event.getScrollX() + "  Y:" + event.getScrollY();

        // do something...
        var self = event.getCurrentTarget();

        var nMoveY = event.getScrollY() * 0.5;
        //if( !isMAC )//Change 17 May
        {
            nMoveY = -nMoveY;
        }
        //cc.log( str );

        if( !self.scrollView )
        {
            return;
        }

        var inner = self.scrollView.getInnerContainer();
        var curPos  = inner.getPosition();
        var nextPos = cc.p(curPos.x, curPos.y + nMoveY);

        // prevent scrolling past beginning
        if (nextPos.y > 0)
        {
            //inner.setPosition(cc.p(0, nextPos.y));
            self.scrollView._startAutoScrollToDestination(cc.p(nextPos.x, 0), 0.5, true);
            return;
        }

        //auto ws = Director::getInstance()->getWinSize();
        var size = self.scrollView.getContentSize();
        var innerSize = inner.getContentSize();
        var topMostY = size.height - innerSize.height;

        // prevent scroll past end
        if (nextPos.y < topMostY)
        {
            self.scrollView._startAutoScrollToDestination(cc.p(nextPos.x, topMostY), 0.5, true);
            return;
        }

        self.scrollView._startAutoScrollToDestination(nextPos, 0.5, true);
    }

});