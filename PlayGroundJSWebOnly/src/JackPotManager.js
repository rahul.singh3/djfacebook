var JackPotManager = (function () {
    // Instance stores a reference to the Singleton
    var instance;

    function init() {
        // Singleton
        // Private methods and variables

        return {
            checkAndSetJackpotData : function()
            {
                for ( var i = 0; i < TOTAL_MACHINES_WITH_JACKPOT; i++ )
                {
                    var str = JACKPOT_START_TIME + "_" + machinesString[ DJS_CRYSTAL_SLOT + i - DJS_QUINTUPLE_5X];
                    var time = UserDef.getInstance().getValueForKey( str );

                    if( !time || time <= SECONDS_IN_A_DAY * 3 )//todo hard coded for 3 days
                    {
                        this.setJackPotWithMachineCode( DJS_CRYSTAL_SLOT + i - DJS_QUINTUPLE_5X );
                    }
                }
            },
            setJackPotWithMachineCode : function( mCode )
        {
            var startTime = ServerData.getInstance().current_time_seconds;

            var jackPotInitialValue = Math.floor( Math.random() * ( 17000000 - 14000000 ) ) + 14000000;

            var jackPotFinalValue = Math.floor( Math.random() * ( 22000000 - 20000000 ) ) + 20000000;

            var incrementFactor = ( jackPotFinalValue - jackPotInitialValue ) / ( SECONDS_IN_A_DAY * 3 );

            //incrementFactor = Math.random() % ( incrementFactor - 12, incrementFactor + 12 );//todo

            incrementFactor /= 20;
            incrementFactor = parseInt( incrementFactor );
            if( incrementFactor < 1 )
                incrementFactor = 1;

            UserDef.getInstance().setValueForKey( JACKPOT_START_TIME + "_" + machinesString[ mCode], startTime );

            UserDef.getInstance().setValueForKey( JACKPOT_INITIAL_VALUE + "_" + machinesString[ mCode], jackPotInitialValue );

            UserDef.getInstance().setValueForKey( JACKPOT_FINAL_VALUE + "_" + machinesString[ mCode], jackPotFinalValue );

            UserDef.getInstance().setValueForKey( JACKPOT_INCREMENT_FACTOR + "_" + machinesString[ mCode], incrementFactor );

            /*log( "jackPotInitialValue = %ld", jackPotInitialValue );
            log( "jackPotFinalValue = %ld", jackPotFinalValue );
            log( "incrementFactor = %ld", incrementFactor );*/
        }
        };
    };
    return {
        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function () {
            if ( !instance ) {
                instance = init();
            }
            return instance;
        }
    };
})();