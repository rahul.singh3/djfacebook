



var reelsRandomizationArrayDJSNeonSlot =
    [
        [// 85% payout
            5, 55, 5, 60, 5, 50,
            5, 45, 5, 80, 5, 70,
            5, 65, 5, 41, 5, 18,
            5, 8, 5, 7, 4, 50,
            5, 45, 5, 70, 5, 60,
            100, 65, 100, 55, 100, 80,
        ],
        [//92 %
            5, 55, 5, 60, 5, 50,
            5, 45, 5, 80, 5, 70,
            5, 65, 5, 40, 5, 19,
            5, 9, 5, 8, 4, 50,
            5, 45, 5, 70, 5, 60,
            100, 65, 100, 55, 100, 80
        ],
        [//97 %
            5, 55, 5, 60, 5, 50,
            5, 45, 5, 80, 5, 70,
            5, 65, 5, 42, 5, 19,
            5, 10, 5, 8, 4, 50,
            5, 45, 5, 70, 5, 60,
            100, 65, 100, 55, 100, 80,
        ],
        [//140 %
            5, 55, 5, 60, 5, 50,
            5, 45, 5, 80, 5, 70,
            5, 65, 5, 55, 5, 35,
            5, 22, 10, 13, 4, 50,
            5, 45, 5, 70, 5, 60,
            100, 65, 100, 55, 100, 80,
        ],
        [//300 %
            5, 55, 5, 60, 5, 50,
            5, 45, 5, 80, 5, 70,
            5, 65, 5, 80, 5, 65,
            5, 40, 10, 27, 4, 50,
            5, 45, 5, 70, 5, 60,
            100, 65, 100, 55, 100, 80
        ],
    ];

var DJSNeonSlot = GameScene.extend({
    reelsArrayDJSNeonSlot:[],
    ctor:function ( level_index ) {
        this.reelsArrayDJSNeonSlot = [
            /*1*/ELEMENT_EMPTY,/*2*/ELEMENT_7_BAR,/*3*/ELEMENT_EMPTY,/*4*/ELEMENT_7_BLUE,/*5*/ELEMENT_EMPTY,
            /*6*/ELEMENT_7_PINK,/*7*/ELEMENT_EMPTY,/*8*/ELEMENT_7_RED,/*9*/ELEMENT_EMPTY,/*10*/ELEMENT_BAR_1,
            /*11*/ELEMENT_EMPTY,/*12*/ELEMENT_BAR_2,/*13*/ELEMENT_EMPTY,/*14*/ELEMENT_BAR_3,/*15*/ELEMENT_EMPTY,
            /*16*/ELEMENT_2X,/*17*/ELEMENT_EMPTY,/*18*/ELEMENT_3X,/*19*/ELEMENT_EMPTY,/*20*/ELEMENT_4X,
            /*21*/ELEMENT_EMPTY,/*22*/ELEMENT_5X,/*23*/ELEMENT_EMPTY,/*24*/ELEMENT_7_PINK,/*25*/ELEMENT_EMPTY,
            /*26*/ELEMENT_7_RED,/*27*/ELEMENT_EMPTY,/*28*/ELEMENT_BAR_2,/*29*/ELEMENT_EMPTY,/*30*/ELEMENT_7_BLUE,
            /*31*/ELEMENT_EMPTY,/*32*/ELEMENT_BAR_3,/*33*/ELEMENT_EMPTY,/*34*/ELEMENT_7_BAR,/*35*/ELEMENT_EMPTY,
            /*36*/ELEMENT_BAR_1,
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSNeonSlot;
        this.physicalArrayCheck= this.reelsArrayDJSNeonSlot;
        this._super( level_index );

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;
    },


    calculatePayment : function( doAnimate )
    {
        var payMentArray = [

            /*0*/     15,  // 7 7 7 Red

            /*1*/     10,  // 7 7 7 Pink

            /*2*/     7,  // 7Bar 7Bar 7Bar

            /*3*/     5,  // 7 7 7 blue

            /*4*/     4,   // 3Bar 3Bar 3Bar

            /*5*/     3,   // 2Bar 2Bar 2Bar

            /*6*/     2,   // Bar Bar Bar

            /*7*/     1,   // Any 7 Combo

            /*8*/     1,   // Any Bar Combo
        ];

        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        for ( var i = 0; i < this.slots[0].resultReel.length; i++ )
        {
            tmpVector = this.slots[0].resultReel[i];
            tmpVector2.push( tmpVector[1] );
        }

        this.lineVector.push( tmpVector2 );

        var pay = 0;
        var tmpPay = 1;
        var lineLength = 3;
        var count2X = 0;
        var count3X = 0;
        var count4X = 0;
        var count5X = 0;
        var countWild = 0;
        var countGeneral = 0;
        var countGeneral2 = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var i, j, k;
        var e = null;
        var pivotElm = null;
        var pivotElm2 = null;
        var tmpVec;

        for ( i = 0; i < this.totalLines; i++ )
        {
            tmpVec = this.lineVector[i];
            for ( k = 0; k < 10; k++ )
            {
                var breakLoop = false;
                countGeneral = 0;
                countGeneral2 = 0;
                switch ( k )
                {
                    case 0://for all combination with wild
                        for ( j = 0; j < lineLength; j++ )
                        {
                            if ( this.resultArray[0][j] != -1 && this.reelsArrayDJSNeonSlot[this.resultArray[0][j]] != ELEMENT_EMPTY )
                            {
                                switch ( this.reelsArrayDJSNeonSlot[this.resultArray[i][j]] )
                                {
                                    case ELEMENT_2X:
                                        count2X++;
                                        tmpPay*=2;
                                        countWild++;
                                        if ( doAnimate && j < tmpVec.length ) {
                                            e = tmpVec[j];
                                        }

                                        if ( e )
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_3X:
                                        count3X++;
                                        tmpPay*=3;
                                        countWild++;

                                        if ( doAnimate && j < tmpVec.length ) {
                                            e = tmpVec[j];
                                        }
                                        if ( e )
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_4X:
                                        count4X++;
                                        tmpPay*=4;
                                        countWild++;
                                        if ( doAnimate && j < tmpVec.length ) {
                                            e = tmpVec[j];
                                        }
                                        if ( e )
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_5X:
                                        count5X++;
                                        tmpPay*=5;
                                        countWild++;
                                        if ( doAnimate && j < tmpVec.length ) {
                                            e = tmpVec[j];
                                        }
                                        if ( e )
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        switch ( pivotElement )
                                        {
                                            case -1:
                                                pivotElement = this.reelsArrayDJSNeonSlot[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate && j < tmpVec.length ) {
                                                    e = tmpVec[j];
                                                }
                                                if ( e )
                                                {
                                                    pivotElm = e;
                                                }
                                                break;

                                            default:
                                                pivotElement2 = this.reelsArrayDJSNeonSlot[this.resultArray[i][j]];
                                                countGeneral++;

                                                if ( doAnimate && j < tmpVec.length ) {
                                                    e = tmpVec[j];
                                                }
                                                if ( e )
                                                {
                                                    pivotElm2 = e;
                                                }
                                                break;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                tmpPay = 0;
                                break;
                            }
                        }

                        if ( j == 3 )
                        {
                            if( countWild >= 2 )
                            {
                                breakLoop = true;

                                switch ( pivotElement )
                                {
                                    case ELEMENT_7_RED:
                                        pay = tmpPay * payMentArray[0];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_7_PINK:
                                        pay = tmpPay * payMentArray[1];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_7_BLUE:
                                        pay = tmpPay * payMentArray[3];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_7_BAR:
                                        pay = tmpPay * payMentArray[2];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_3:
                                        pay = tmpPay * payMentArray[4];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_2:
                                        pay = tmpPay * payMentArray[5];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_1:
                                        pay = tmpPay * payMentArray[6];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        pay = tmpPay;
                                        if ( pivotElm && pivotElm )
                                        {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;
                                }
                            }
                            else if( countWild == 1 )
                            {
                                breakLoop = true;

                                if ( pivotElement == pivotElement2 )
                                {
                                    switch ( pivotElement )
                                    {
                                        case ELEMENT_7_RED:
                                            pay = tmpPay * payMentArray[0];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_7_PINK:
                                            pay = tmpPay * payMentArray[1];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_7_BLUE:
                                            pay = tmpPay * payMentArray[3];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_7_BAR:
                                            pay = tmpPay * payMentArray[2];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_3:
                                            pay = tmpPay * payMentArray[4];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_2:
                                            pay = tmpPay * payMentArray[5];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_1:
                                            pay = tmpPay * payMentArray[6];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        default:
                                            pay = tmpPay;
                                            break;
                                    }
                                }
                                else
                                {
                                    if ( pivotElement >= ELEMENT_7_BAR && pivotElement <= ELEMENT_7_RED &&
                                        pivotElement2 >= ELEMENT_7_BAR && pivotElement2 <= ELEMENT_7_RED )
                                    {
                                        pay = tmpPay * payMentArray[7];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if ( ( pivotElement == ELEMENT_7_BAR || pivotElement == ELEMENT_BAR_1 ) &&
                                        ( pivotElement2 == ELEMENT_7_BAR || pivotElement2 == ELEMENT_BAR_1 ) )
                                    {
                                        pay = tmpPay * payMentArray[8];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if ( ( ( pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_BAR_3 ) || pivotElement == ELEMENT_7_BAR ) &&
                                        ( ( pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_BAR_3 ) || pivotElement2 == ELEMENT_7_BAR ) )
                                    {
                                        pay = tmpPay * payMentArray[8];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if( ( ( pivotElement == ELEMENT_BAR_2 || pivotElement == ELEMENT_7_RED ) &&
                                            ( pivotElement2 == ELEMENT_BAR_2 || pivotElement2 == ELEMENT_7_RED ) ) ||
                                        ( ( pivotElement2 == ELEMENT_BAR_2 || pivotElement2 == ELEMENT_7_RED ) &&
                                            ( pivotElement == ELEMENT_BAR_2 || pivotElement == ELEMENT_7_RED ) ) )
                                    {
                                        pay = tmpPay * payMentArray[8];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if( ( ( pivotElement == ELEMENT_BAR_1 || pivotElement == ELEMENT_7_BLUE ) &&
                                            ( pivotElement2 == ELEMENT_BAR_1 || pivotElement2 == ELEMENT_7_BLUE ) ) ||
                                        ( ( pivotElement2 == ELEMENT_BAR_1 || pivotElement2 == ELEMENT_7_BLUE ) &&
                                            ( pivotElement == ELEMENT_BAR_1 || pivotElement == ELEMENT_7_BLUE ) ) )
                                    {
                                        pay = tmpPay * payMentArray[8];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if( ( ( pivotElement == ELEMENT_BAR_3 || pivotElement == ELEMENT_7_PINK ) &&
                                            ( pivotElement2 == ELEMENT_BAR_3 || pivotElement2 == ELEMENT_7_PINK ) ) ||
                                        ( ( pivotElement2 == ELEMENT_BAR_3 || pivotElement2 == ELEMENT_7_PINK ) &&
                                            ( pivotElement == ELEMENT_BAR_3 || pivotElement == ELEMENT_7_PINK ) ) )
                                    {
                                        pay = tmpPay * payMentArray[8];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else
                                    {
                                        pay = 0;
                                    }
                                }
                            }
                        }
                        break;

                    case 1: // 7 7 7 red
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSNeonSlot[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                case ELEMENT_BAR_2:
                                    countGeneral2++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[0];
                            breakLoop = true;
                        }
                        else if( countGeneral && countGeneral + countGeneral2 == 3 )
                        {
                            pay = payMentArray[8];
                            breakLoop = true;
                        }

                        if( breakLoop )
                        {
                            for ( var l = 0; l < countGeneral + countGeneral2; l++ ) {

                                if ( doAnimate && l < tmpVec.length ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 2: // 7 7 7 white
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSNeonSlot[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_PINK:
                                    countGeneral++;
                                    break;

                                case ELEMENT_BAR_3:
                                    countGeneral2++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[1];
                            breakLoop = true;
                        }
                        else if( countGeneral && countGeneral + countGeneral2 == 3 )
                        {
                            pay = payMentArray[8];
                            breakLoop = true;
                        }

                        if( breakLoop )
                        {
                            for ( var l = 0; l < countGeneral + countGeneral2; l++ ) {

                                if ( doAnimate && l < tmpVec.length ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 3: // 7 7 7 Blue
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSNeonSlot[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_BLUE:
                                    countGeneral++;
                                    break;

                                case ELEMENT_BAR_1:
                                    countGeneral2++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[3];
                            breakLoop = true;

                        }
                        else if( countGeneral && countGeneral + countGeneral2 == 3 )
                        {
                            pay = payMentArray[8];
                            breakLoop = true;
                        }

                        if( breakLoop )
                        {
                            for ( var l = 0; l < countGeneral + countGeneral2; l++ ) {

                                if ( doAnimate && l < tmpVec.length ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 4: // 7 7 7 bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSNeonSlot[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[2];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                                if ( doAnimate && l < tmpVec.length ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 5: // 3Bar 3Bar 3Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSNeonSlot[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[4];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                                if ( doAnimate && l < tmpVec.length ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 6: // 2Bar 2Bar 2Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSNeonSlot[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[5];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                                if ( doAnimate && l < tmpVec.length ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 7: // Bar Bar Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSNeonSlot[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[6];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                                if ( doAnimate && l < tmpVec.length ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 8: // any 7 combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSNeonSlot[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_BAR:
                                case ELEMENT_7_BLUE:
                                case ELEMENT_7_PINK:
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[7];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {
                                if ( doAnimate && l < tmpVec.length ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 9: // any Bar combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSNeonSlot[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[8];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( doAnimate && l < tmpVec.length ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

                if ( breakLoop )
                {
                    break;
                }
            }
        }



        if( doAnimate )
        {
            this.currentWin = this.currentBet * pay;
            if ( this.currentWin > 0 )
            {
                this.addWin = this.currentWin / 10;

                this.setCurrentWinAnimation();

            }

            this.checkAndSetAPopUp();

        }
        else
        {
            this.currentWinForcast = this.currentBet * pay;
        }
    },
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSNeonSlot[i]=this.physicalReelElements[idx];
                i++;
            }
        }
        this.probabilityArray = this.probabilityArray[this.easyModeCode];

    },
    changeTotalProbabilities : function()
    {

        this.totalProbalities = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            this.probabilityArray = this.probabilityArrayServer[this.easyModeCode];
            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[i];
            }
        }
        else
        {
            for ( var i = 0; i < 36; i++ )
            {
                this.probabilityArray[i] = reelsRandomizationArrayDJSNeonSlot[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSNeonSlot[this.easyModeCode][i];
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
        if( ( this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] == ELEMENT_7_RED && this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] == ELEMENT_7_RED )  ||
            ( this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] == ELEMENT_7_BLUE && this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] == ELEMENT_7_BLUE ) ||
            ( this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] == ELEMENT_7_PINK && this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] == ELEMENT_7_PINK ) ||
            ( ( this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] <= ELEMENT_10X ) &&
                ( this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] <= ELEMENT_10X ) ) ||
            ( this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] == ELEMENT_7_RED && ((this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] == ELEMENT_7_RED && ((this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] == ELEMENT_7_BLUE && ((this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] == ELEMENT_7_BLUE && ((this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] == ELEMENT_7_PINK && ((this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] == ELEMENT_7_PINK && ((this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] == ELEMENT_7_BAR && ((this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] == ELEMENT_7_BAR && ((this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] == ELEMENT_BAR_3 && ((this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSNeonSlot[this.resultArray[0][1]] == ELEMENT_BAR_3 && ((this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSNeonSlot[this.resultArray[0][0]] <= ELEMENT_10X ) ) ) )
        {
            if ( !this.slots[0].isForcedStop )
            {
                this.schedule( this.reelStopper, 4.8 );

                this.delegate.jukeBox( S_EXCITEMENT );

                var glowSprite = new cc.Sprite( glowStrings[0] );

                if ( glowSprite )
                {
                    var r = this.slots[0].physicalReels[( this.slots[0].physicalReels.length - 1 )];

                    glowSprite.setPosition( r.getParent().getPosition().x + this.slots[0].getPosition().x, r.getParent().getPosition().y + this.slots[0].getPosition().y );

                    //this.scaleAccordingToSlotScreen( glowSprite );

                    this.addChild( glowSprite, 5 );
                    this.animNodeVec.push( glowSprite );

                    glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ) ) );
                }
            }
        }
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
        var tmp = this.slots[0].resultReel[laneIndex];

        var e = tmp[1];

        switch (e.eCode)
        {
            case ELEMENT_7_RED:
                if (laneIndex != 0)
                {
                    break;
                }
            case ELEMENT_2X:
            case ELEMENT_3X:
            case ELEMENT_4X:
            case ELEMENT_5X:
                e.runAction( new cc.Repeat( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ), 2 ) );
                this.delegate.jukeBox(S_FLASH);
                break;

            default:
                break;
        }
    },

    generateResult : function( scrIndx )
    {
        for ( var i = 0; i < this.slots[scrIndx].displayedReels.length; i++ )
        {
            //generate:
            var num = parseInt(this.prevReelIndex[i]);

            var tmpnum = 0;

            while (num == parseInt(this.prevReelIndex[i]))
            {
                num = Math.floor( Math.random() * this.totalProbalities );
            }
            this.prevReelIndex[i] = num;
            this.resultArray[0][i] = num;

            for (var j = 0; j < 36; j++)
            {
                tmpnum+=this.probabilityArray[j];

                if (tmpnum >= this.resultArray[0][i])
                {
                    this.resultArray[0][i] = j;
                    break;
                }
            }
            //cc.log( "this.resultArray[0][" + i + "] = " + this.resultArray[0][i] );
        }
        /*/hack
        this.resultArray[0][0] = 19;
        this.resultArray[0][1] = 19;
        this.resultArray[0][2] = 19;//*///hack

        this.checkForFalseWin();
    },

    rollUpdator : function(dt)
    {
        var i;
        for (i = 0; i < this.totalSlots; i++)
        {
            this.slots[i].updateSlot(dt);
        }

        for (i = 0; i < this.totalSlots; i++)
        {
            if ( this.slots[i].isSlotRunning() )
            {
                break;
            }
        }

        if (i >= this.totalSlots)
        {
            //cc.log( "in rollUpdator 2" );

            this.unschedule( this.rollUpdator );
            this.calculatePayment( true );

            if (this.isAutoSpinning)
            {
                if (this.currentWin > 0)
                {
                    this.scheduleOnce( this.autoSpinCB, 1.5);
                }
                else
                {
                    this.scheduleOnce( this.autoSpinCB, 1.0);
                }
            }

            this.isRolling = false;
        }
    },

    setFalseWin : function()
    {
        var highWinIndices = [ 3, 5, 7, ];
        var missIndices = [ 3, 5, 7, 15, 17, 19, 21, ];

        var missedIndx = Math.floor( Math.random() * 3 );

        if( missedIndx == 2 )
        {
            if( Math.floor( Math.random() * 10 ) == 3 )
            {
            }
            else
            {
                missedIndx = Math.floor( Math.random() * 2 );
            }
        }

        var sameIndex = Math.floor( Math.random() * highWinIndices.length );

        for ( var i = 0; i < 3; i++ )
        {
            if ( i == missedIndx )
            {
                this.resultArray[0][i] = this.getRandomIndex( missIndices[( Math.floor( Math.random() * missIndices.length ) )], true );
            }
            else
            {
                this.resultArray[0][i] = this.getRandomIndex( highWinIndices[sameIndex], false );
            }
        }
    },

    setResult : function( indx, lane, sSlot )
    {
        var gap = 387.0;

        var j = 0;
        var animOriginY;

        var r = this.slots[0].physicalReels[lane];
        var arrSize = r.elementsVec.length;
        var spr;

        var spriteVec = [];

        animOriginY = gap * r.direction;
        spr = r.elementsVec[indx];
        if ( spr.eCode == ELEMENT_EMPTY )
        {
            gap = 285 * spr.getScale();
        }

        if ( indx == 0 )
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push(spr);

            spr.setPositionY( animOriginY );

            for (var i = 0; i < 2; i++)
            {
                spr = r.elementsVec[i];
                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (i + 1) * r.direction);
            }
        }
        else if (indx == arrSize - 1)
        {
            spr = r.elementsVec[0];
            spriteVec.push(spr);

            spr.setPositionY(animOriginY);
            for (var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i--)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction );
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push(spr);
            spr.setPositionY(animOriginY);

            for (var i = indx; i < indx + 2; i++)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }

        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            gap = -773.5;

            var eee;
            if (i < 2)
            {
                eee = spriteVec[i + 1];
            }
            if (i == 0)
            {
                var ee = spriteVec[i + 1];
                if ( Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) ) != 387)
                {
                    gap = -Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) * 2 * ee.getScale() );
                }
            }

            gap*=r.direction;
            var ease;

            if ( this.slots[sSlot].isForcedStop )
            {
                ease =  new cc.MoveTo( 0.15, cc.p( 0, e.getPositionY() + gap) ).easing( cc.easeBackOut() );
            }
            else
            {
                ease = new cc.MoveTo( 0.3, cc.p( 0, e.getPositionY() + gap ) ).easing( cc.easeBackOut() );
            }

            ease.setTag( MOVING_TAG );
            e.runAction(ease);
        }

        return spriteVec;
    },

    setWinAnimation : function( animCode, screen )
    {
        var tI = 0.3;

        var fire;
        for (var i = 0; i < this.animNodeVec.length; i++)
        {
            fire = this.animNodeVec[i];
            fire.removeFromParent(true);
        }
        this.animNodeVec = [];


        switch ( animCode )
        {
            case WIN_TAG:

                for (var i = 0; i < this.slots[0].blurReels.length; i++)
                {
                    var r = this.slots[0].blurReels[i];

                    var node = r.getParent();

                    var moveW = this.slots[0].reelDimentions.x * this.slots[0].getScale();
                    var moveH = this.slots[0].reelDimentions.y * this.slots[0].getScale();

                    for (var j = 0; j < 4; j++)
                    {

                        var fire = new cc.ParticleSun();
                        fire.texture = cc.textureCache.addImage( res.FireStar_png );
                        var pos;

                        this.addChild(fire, 1);
                        this.animNodeVec.push(fire);

                        //fire.texture.setScale( 0.5 );

                        fire.setStartColor( cc.color( 141, 0, 1 ) );
                        fire.setEndColor( cc.color( 0, 141, 1 ) );

                        var moveBy = null;

                        var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                        diff*=( 2 - i );

                        switch (j % 4)
                        {
                            case 0:
                                pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY() );
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(moveW, 0));
                                break;

                            case 1:
                                pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(0, moveH));
                                break;

                            case 2:
                                pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(-moveW, 0));
                                break;

                            case 3:
                                pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(0, -moveH));
                                break;

                            default:
                                break;
                        }
                        //fire.setPosition( cc.p( this.vSize.width * 0.5, this.vSize.height * 0.5 ) );
                        fire.runAction( new cc.RepeatForever( new cc.Sequence( moveBy, moveBy.reverse() ) ) );
                    }
                }

                break;

            case BIG_WIN_TAG:
                for(var i = 0; i < this.slots[0].blurReels.length; i++)
                {
                    var r = this.slots[0].blurReels[i];

                    var node = r.getParent();

                    var walkAnimFrames = [];
                    var walkAnim = null;

                    var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                    diff*=( 2 - i );

                    var glowSprite = new cc.Sprite( glowStrings[1] );
                    {
                        var tmp = new cc.SpriteFrame( glowStrings[1], cc.rect(0, 0, glowSprite.getContentSize().width, glowSprite.getContentSize().height));
                        walkAnimFrames.push(tmp);
                    }
                    {
                        var tmp = new cc.SpriteFrame( glowStrings[3], cc.rect(0, 0, glowSprite.getContentSize().width, glowSprite.getContentSize().height));
                        walkAnimFrames.push(tmp);
                    }
                    {
                        var tmp = new cc.SpriteFrame( glowStrings[5], cc.rect(0, 0, glowSprite.getContentSize().width, glowSprite.getContentSize().height));
                        walkAnimFrames.push(tmp);
                    }

                    walkAnim = new cc.Animation( walkAnimFrames, 0.75 );

                    var tmpAnimate = new cc.Animate(walkAnim);
                    glowSprite.runAction( tmpAnimate.repeatForever() );

                    if (glowSprite)
                    {
                        glowSprite.setPosition( node.getPositionX() + this.slots[0].getPositionX() - diff, node.getPositionY() + this.slots[0].getPositionY() );


                        this.addChild(glowSprite, 1);
                        this.animNodeVec.push(glowSprite);

                        this.scaleAccordingToSlotScreen( glowSprite );

                        glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo(0.25, 100), new cc.FadeTo(0.25, 255) ) ) );
                    }
                }
                break;

            default:
                break;
        }
    },

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSNeonSlot;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    spin : function()
    {
        //cc.log( "in Spin" );
        this.unschedule(this.updateWinCoin);

        this.deductBet( this.currentBet );
        if (this.displayedScore != this.totalScore)
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;
        this.currentWin = 0;
        this.currentWinForcast = 0;
        this.startRolling();

        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();

        //cc.log( "out Spin" );
    }

});