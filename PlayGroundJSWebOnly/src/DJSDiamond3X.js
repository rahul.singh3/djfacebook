/**
 * Created by RNF-Mac11 on 2/10/17.
 */



var reelsRandomizationArrayDJSDiamond3X =
    [
        [// 85% payout
            30, 16, 20, 55, 25, 65,
            25, 7, 20, 55, 20, 50,
            20, 50, 10, 20,5, 2,
            10, 25, 10, 20, 10, 25,
            5, 38, 30, 16, 5, 35,
            5, 35, 7, 2, 5, 35
        ],
        [// 92 %
            30, 16, 20, 65, 25, 70,
            25, 7, 20, 55, 20, 50,
            20, 50, 10, 25,5, 3,
            10, 25, 10, 20, 10, 25,
            5, 38, 30, 16, 5, 35,
            5, 35, 7, 3, 5, 35
        ],
        [  // 95-97% payout
            30, 16, 20, 65, 25, 70,
            25, 9, 20, 55, 20, 50,
            20, 50, 10, 25,5, 3,
            10, 25, 10, 20, 10, 25,
            5, 38, 30, 16, 5, 35,
            5, 35, 7, 3, 5, 35
        ],
        [// 140% payout
            30, 16, 20, 65, 25, 70,
            25, 15, 20, 55, 20, 50,
            20, 50, 10, 25,5, 7,
            10, 25, 10, 20, 10, 25,
            5, 38, 30, 16, 5, 35,
            5, 35, 7, 7, 5, 35
        ],
        [// 300% payout
            30, 16, 20, 65, 25, 70,
            25, 15, 20, 55, 20, 50,
            20, 50, 10, 25,5, 7,
            10, 25, 10, 20, 10, 25,
            5, 38, 30, 16, 5, 35,
            5, 35, 7, 7, 5, 35
        ]
    ];

var spinArr =
    [
        500, 12, 250, 15, 200, 20, 75, 25, 125, 30, 100, 35, 150, 40, 50, 10
    ];

var spinProbArr =
    [
        1, 90, 1, 80, 1, 75, 4, 60, 3, 70, 5, 30, 3, 25, 20, 130
    ];

var DJSDiamond3X = GameScene.extend({

    bonusElemnt : null,

    haveBonus : false,

    bonusWin : 0,

    curBonusIndx : 0,
    reelsArrayDJSDiamond3X:[],
    ctor:function ( level_index ) {
        this.reelsArrayDJSDiamond3X = [
            /*1*/ELEMENT_EMPTY,/*2*/ ELEMENT_CHERRY,/*3*/ ELEMENT_EMPTY,/*4*/ ELEMENT_7_RED,/*5*/ ELEMENT_EMPTY,
            /*6*/ELEMENT_7_BLUE,/*7*/ELEMENT_EMPTY,/*8*/ ELEMENT_DOUBLE_D,/*9*/ ELEMENT_EMPTY,/*10*/ ELEMENT_BAR_1,
            /*11*/ELEMENT_EMPTY,/*12*/ELEMENT_BAR_2,/*13*/ELEMENT_EMPTY,/*14*/ ELEMENT_BAR_3,/*15*/ELEMENT_EMPTY,
            /*16*/ELEMENT_7_RED,/*17*/ ELEMENT_EMPTY,/*18*/ ELEMENT_BONUS,/*19*/ELEMENT_EMPTY,/*20*/ ELEMENT_BAR_1,
            /*21*/ELEMENT_EMPTY,/*22*/ ELEMENT_BAR_2,/*23*/ELEMENT_EMPTY,/*24*/ELEMENT_BAR_3,/*25*/ELEMENT_EMPTY,
            /*26*/ ELEMENT_7_BLUE,/*27*/ ELEMENT_EMPTY,/*28*/ ELEMENT_CHERRY,/*29*/ELEMENT_EMPTY,/*30*/ ELEMENT_BAR_1,
            /*31*/ELEMENT_EMPTY,/*32*/ELEMENT_BAR_2,/*33*/ ELEMENT_EMPTY,/*34*/ ELEMENT_BONUS,/*35*/ ELEMENT_EMPTY,
            /*36*/ELEMENT_BAR_3
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSDiamond3X;
        this.physicalArrayCheck= this.reelsArrayDJSDiamond3X;
        this._super( level_index );

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;
    },

        bonusMenuCB : function(pSender)
{
    var node = pSender;

    var sprite = this.getChildByTag(D_NODE_TAG).getChildByTag(WHEEL_TAG).getChildByTag(WHEEL_TAG);

    if (sprite.getRotation() != 0)
    {
        return;
    }

    var totalProb = 0;

    for (var i = 0; i < 16; i++)
    {
        totalProb+=spinProbArr[i];
    }

    this.curBonusIndx = Math.floor(Math.random() * totalProb);
    totalProb = 0;

    for (var i = 0; i < 16; i++)
    {
        totalProb+=spinProbArr[i];
        if (totalProb >= this.curBonusIndx)
        {
            this.curBonusIndx = i;
            break;
        }
    }

    this.bonusWin = spinArr[this.curBonusIndx];

    switch (node.getTag())
    {
        case SPIN_BTN_TAG:
            sprite = this.getChildByTag(D_NODE_TAG).getChildByTag(WHEEL_TAG).getChildByTag(WHEEL_TAG);

            //sprite.runAction( new cc.Sequence( new cc.RotateBy(3, 720), new cc.RotateBy(1, ((16 - this.curBonusIndx) * 22.5)) + 10), new cc.RotateBy(0.25, -10), new cc.DelayTime(1), cc.callFunc( this.bonusWheelCB, this));
            sprite.runAction( new cc.Sequence( new cc.RotateBy(3, 720), new cc.RotateBy(1, (((16 - this.curBonusIndx) * 22.5)) + 10), new cc.RotateBy(0.25, -10),
                new cc.DelayTime(1), cc.callFunc(this.bonusWheelCB, this)));
            sprite = this.getChildByTag(D_NODE_TAG).getChildByTag(WHEEL_TAG).getChildByTag(WHEEL_LIGHT_TAG);

            sprite.runAction(new cc.Sequence( new cc.RotateBy(3, -720), new cc.RotateBy(1, -((this.curBonusIndx * 360/16)) - 10), new cc.RotateBy(0.25, 10)));

            this.delegate.jukeBox(S_WHEEL);
            break;

        default:
            break;
    }
},

bonusWheelCB : function(obj)
{
    var sprite = obj.getParent().getParent();

    /*this.delegate.jukeBox(S_STOP_MUSIC );
    this.delegate.jukeBox(S_STOP_EFFECTS );
    this.delegate.jukeBox(S_AMBIENCE );
    this.delegate.jukeBox(S_BONUS_WIN);*/

    var popBG = new cc.Sprite( "#popup_bg.png" );
    popBG.setPosition(cc.p( this.vSize.width / 2, this.vSize.height / 2));
    var posX = cc.rectGetMinX(popBG.getBoundingBox());
    sprite.addChild(popBG, 6);
    popBG.setScale(0);
    popBG.runAction( new cc.Sequence( new cc.ScaleTo(0.5, 1), new cc.DelayTime(5), cc.callFunc(this.removeWheelCB, this)));

    var winStr = "WHEEL PRIZE = $" + this.bonusWin;
    var WheelWinLabel = new cc.LabelTTF( winStr, "GEORGIAB", 30);
    WheelWinLabel.setPosition(popBG.getContentSize().width / 2, popBG.getContentSize().height * 0.75 - WheelWinLabel.getContentSize().height);
    WheelWinLabel.setColor(cc.color( 255, 255, 0 ) );
    popBG.addChild(WheelWinLabel);

    this.calculatePayment( true );
    this.currentWin = this.currentWin + (this.bonusWin * this.currentBet);

    var betStr = "BET MULTIPLIER = x" + this.currentBet;
    var betLabel = new cc.LabelTTF( betStr, "GEORGIAB", 30 );
    betLabel.setPosition( popBG.getContentSize().width / 2, popBG.getContentSize().height * 0.5 );
    popBG.addChild( betLabel );

    var totalStr = "$  " + this.currentBet * this.bonusWin;
    var totalLabel = new cc.LabelTTF( totalStr, "GEORGIAB", 35 );
    totalLabel.setPosition( popBG.getContentSize().width / 2, popBG.getContentSize().height * 0.25 );
    popBG.addChild( totalLabel );

    var Ssprite = sprite.getChildByTag( WHEEL_STOPPER_TAG );
    var string = "#flap_" + ( this.curBonusIndx + 1 ) + ".png";
    var flapSprite = new cc.Sprite( string );
    flapSprite.setPosition( Ssprite.getPosition() );
    flapSprite.setScale( Ssprite.getContentSize().height / flapSprite.getContentSize().height );
    sprite.addChild( flapSprite, 6 );


    flapSprite.runAction( new cc.ScaleTo( 0.5, 1 ) );
    flapSprite.runAction( new cc.MoveTo( 0.5, cc.p( posX + flapSprite.getContentSize().width * 0.2, this.vSize.height / 2 ) ) );
},

    calculatePayment : function( doAnimate )
    {
        var payMentArray = [
        /*0*/    1200,// 2x 2x 2x
        /*1*/    30,  // 7 7 7 Red
        /*2*/    15,  // 7 7 7 Blue
        /*3*/    12,  // Any 7
        /*4*/    10,  // 3Bar 3Bar 3Bar
        /*5*/    7,  // 2Bar 2Bar 2Bar
        /*6*/    3,  // Bar Bar Bar
        /*7*/    4,  // Triple Cherry
        /*8*/    2,   // Double Cherry
        /*9*/    1,   // Single Cherry
        /*10*/   1   // Any Bar
    ];

        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        for (var i = 0; i < this.slots[0].resultReel.length; i++)
        {
            tmpVector = this.slots[0].resultReel[i];
            tmpVector2.push(tmpVector[1]);
        }

        this.lineVector.push(tmpVector2);

        if (this.haveBonus)
        {
            this.bonusElemnt = tmpVector2[2];

            this.bonusElemnt.aCode = SCALE_TAG;
        }

        var winScenario = 9;
        var lineLength = 3;
        var pay = 0;
        var tmpPay = 1;
        var count2X = 0;
        var countWild = 0;
        var countGeneral = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var i, j, k;
        var e = null;
        var pivotElm = null;
        var pivotElm2 = null;
        var tmpVec;


        for (i = 0; i < this.totalLines; i++)
        {
            tmpVec = this.lineVector[i];
            for (k = 0; k < winScenario; k++)
            {
                var breakLoop = false;
                countGeneral = 0;
                switch (k)
                {
                    case 0://for all combination with wild
                        for (j = 0; j < lineLength; j++)
                        {
                            if (this.reelsArrayDJSDiamond3X[this.resultArray[i][j]] != -1 )
                            {
                                switch (this.reelsArrayDJSDiamond3X[this.resultArray[i][j]])
                                {
                                    case ELEMENT_DOUBLE_D:
                                        count2X++;
                                        tmpPay*=2;
                                        countWild++;
                                        if ( doAnimate )
                                        {
                                            e = tmpVec[j];
                                        }

                                        if (e)
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        switch (pivotElement)
                                        {
                                            case -1:
                                                pivotElement = this.reelsArrayDJSDiamond3X[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate )
                                                {
                                                    e = tmpVec[j];
                                                }
                                                if (e)
                                                {
                                                    pivotElm = e;
                                                }
                                                break;

                                            default:
                                                pivotElement2 = this.reelsArrayDJSDiamond3X[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate )
                                                {
                                                    e = tmpVec[j];
                                                }
                                                if (e)
                                                {
                                                    pivotElm2 = e;
                                                }
                                                break;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }

                        if (j == 3)
                        {
                            if (count2X == 3)
                            {
                                pay = payMentArray[0];
                                breakLoop = true;
                            }
                            else if(countWild >= 2)
                            {
                                breakLoop = true;

                                switch (pivotElement)
                                {
                                    case ELEMENT_7_RED:
                                        pay = tmpPay * payMentArray[1];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }

                                        break;

                                    case ELEMENT_7_BLUE:
                                        pay = tmpPay * payMentArray[2];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_3:
                                        pay = tmpPay * payMentArray[4];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_2:
                                        pay = tmpPay * payMentArray[5];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_1:
                                        pay = tmpPay * payMentArray[6];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_CHERRY:
                                        pay = tmpPay * payMentArray[7];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        pay = tmpPay;
                                        if (pivotElm)
                                        {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;
                                }
                            }
                            else if(countWild == 1)
                            {
                                breakLoop = true;

                                if (pivotElement == pivotElement2)
                                {
                                    switch (pivotElement)
                                    {
                                        case ELEMENT_7_RED:
                                            pay = tmpPay * payMentArray[1];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }

                                            break;

                                        case ELEMENT_7_BLUE:
                                            pay = tmpPay * payMentArray[2];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_3:
                                            pay = tmpPay * payMentArray[4];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_2:
                                            pay = tmpPay * payMentArray[5];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_1:
                                            pay = tmpPay * payMentArray[6];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_CHERRY:
                                            pay = tmpPay * payMentArray[7];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        default:
                                            pay = tmpPay;
                                            break;
                                    }
                                }
                                else
                                {
                                    if ((pivotElement == ELEMENT_7_BLUE && pivotElement2 == ELEMENT_7_RED) ||
                                        (pivotElement2 == ELEMENT_7_BLUE && pivotElement == ELEMENT_7_RED))
                                    {
                                        pay = tmpPay * payMentArray[3];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if((pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_BAR_3) &&
                                        (pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_BAR_3))
                                    {
                                        pay = tmpPay * payMentArray[10];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if(pivotElement == ELEMENT_CHERRY)
                                    {
                                        pay = tmpPay * payMentArray[8];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }

                                    }
                                    else if(pivotElement2 == ELEMENT_CHERRY)
                                    {
                                        pay = tmpPay * payMentArray[8];
                                        if ( doAnimate ) {
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if(pivotElement == ELEMENT_BONUS || pivotElement2 == ELEMENT_BONUS)
                                    {
                                        pay = tmpPay;
                                    }
                                    else
                                    {
                                        pay = tmpPay;
                                    }
                                }
                            }
                        }
                        break;

                    case 1: // 7 7 7 red
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSDiamond3X[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[1];
                            breakLoop = true;

                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }

                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 2: // 7 7 7 Blue
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSDiamond3X[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_BLUE:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[2];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 3: // 3Bar 3Bar 3Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSDiamond3X[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[4];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 4: // 2Bar 2Bar 2Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSDiamond3X[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[5];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 5: // Bar Bar Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSDiamond3X[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[6];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 6: // any 7 combo
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSDiamond3X[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_BLUE:
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[3];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 7: // any Bar and 7Bar combo
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSDiamond3X[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[10];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++)
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 8: // any cherry combo
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSDiamond3X[this.resultArray[i][j]])
                            {
                                case ELEMENT_CHERRY:
                                    countGeneral++;
                                    if ( doAnimate ) {
                                        e = tmpVec[j];
                                    }
                                    if (e)
                                    {
                                        e.aCode = SCALE_TAG;
                                    }
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[7];
                            breakLoop = true;
                        }
                        else if (countGeneral == 2)
                        {
                            pay = payMentArray[8];
                            breakLoop = true;
                        }
                        else if (countGeneral == 1)
                        {
                            pay = payMentArray[9];
                            breakLoop = true;
                        }
                        break;

                    default:
                        break;
                }

                if (breakLoop)
                {
                    break;
                }
            }
        }

        if ( doAnimate ) {
            this.currentWin = this.currentBet * pay;
            if (this.currentWin > 0 && !this.haveBonus)
            {
                this.addWin = this.currentWin / 10;
                this.setCurrentWinAnimation();
            }

            if( !this.haveBonus )
            {
                this.checkAndSetAPopUp();
            }
        }
        else
        {
            this.currentWinForcast = this.currentBet * pay;
        }
    },
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSDiamond3X[i]=this.physicalReelElements[idx];
                i++;
            }
        }
    },
    changeTotalProbabilities : function()
    {
        this.totalProbalities = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            //this.extractArrayFromSring();
            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[this.easyModeCode][i];
            }
        }
        else
        {
            for ( var i = 0; i < 36; i++ )
            {
                this.probabilityArray[this.easyModeCode][i] = reelsRandomizationArrayDJSDiamond3X[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSDiamond3X[this.easyModeCode][i];
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
        if ( ( ( this.reelsArrayDJSDiamond3X[this.resultArray[0][0]] == ELEMENT_7_BLUE || this.reelsArrayDJSDiamond3X[this.resultArray[0][0]] == ELEMENT_DOUBLE_D || this.reelsArrayDJSDiamond3X[this.resultArray[0][0]] == ELEMENT_7_RED ) &&
            ( this.reelsArrayDJSDiamond3X[this.resultArray[0][1]] == ELEMENT_7_BLUE || this.reelsArrayDJSDiamond3X[this.resultArray[0][1]] == ELEMENT_DOUBLE_D|| this.reelsArrayDJSDiamond3X[this.resultArray[0][1]] == ELEMENT_7_RED ) ) ||
            ( ( this.reelsArrayDJSDiamond3X[this.resultArray[0][0]] == ELEMENT_DOUBLE_D && this.reelsArrayDJSDiamond3X[this.resultArray[0][1]] == ELEMENT_BAR_3 ) || ( this.reelsArrayDJSDiamond3X[this.resultArray[0][0]] == ELEMENT_BAR_3 &&
            this.reelsArrayDJSDiamond3X[this.resultArray[0][1]] == ELEMENT_DOUBLE_D ) ) )
        {
            if ( !this.slots[0].isForcedStop )
            {
                this.schedule( this.reelStopper, 4.8 );

                this.delegate.jukeBox( S_EXCITEMENT );

                var glowSprite = new cc.Sprite( glowStrings[0] );

                if ( glowSprite )
                {
                    var r = this.slots[0].physicalReels[ this.slots[0].physicalReels.length - 1 ];

                    glowSprite.setPosition( r.getParent().getPosition().x + this.slots[0].getPosition().x, r.getParent().getPosition().y + this.slots[0].getPosition().y );

                    //this.scaleAccordingToSlotScreen( glowSprite );

                    this.addChild( glowSprite, 5 );
                    this.animNodeVec.push( glowSprite );

                    glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ) ) );
                }
            }
        }
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
        var tmp = this.slots[0].resultReel[laneIndex];

        var e = tmp[1];

        switch ( e.eCode )
        {
            case ELEMENT_7_RED:
                if ( laneIndex != 0 )
                {
                    break;
                }
            case ELEMENT_2X:
            case ELEMENT_3X:
            case ELEMENT_4X:
            case ELEMENT_5X:
                e.runAction( new cc.Repeat( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ), 2 ) );
                this.delegate.jukeBox( S_FLASH );
                break;

            default:
                break;
        }

    },

    generateResult : function( scrIndx )
    {
        for (var i = 0; i < this.slots[scrIndx].displayedReels.length; i++)
        {
            for( ; ; ) {
                //generate:
                var num = this.prevReelIndex[i];

                var tmpnum = 0;

                while (num == this.prevReelIndex[i]) {
                    num = Math.floor(Math.random() * this.totalProbalities);
                }
                this.prevReelIndex[i] = num;
                this.resultArray[0][i] = num;

                for (var j = 0; j < 36; j++)
                {
                    tmpnum += this.probabilityArray[this.easyModeCode][j];

                    if (tmpnum >= this.resultArray[0][i]) {
                        this.resultArray[0][i] = j;
                        break;
                    }
                }

                /*/HACK
                 if (i == 0)
                 {
                 this.resultArray[0][i] = 0;//2;
                 }
                 else if (i == 1)
                 {
                 this.resultArray[0][i] = 1;//2, 4, 20, 30
                 }
                 else if(i == 2)
                 {
                 this.resultArray[0][i] = 17;
                 }
                 else
                 {
                 this.resultArray[0][i] = 0;
                 }//*///HACK

                if (this.reelsArrayDJSDiamond3X[this.resultArray[0][i]] == ELEMENT_BONUS) {
                    if (i != 2) {
                        //goto
                        //generate;
                    }
                    else {
                        this.haveBonus = true;
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }

        if ( !this.haveBonus )
        {
            this.calculatePayment( false );
            this.checkForFalseWin();
        }

    },

        removeWheelCB : function(obj)
{
    this.delegate.jukeBox(S_STOP_MUSIC);
    this.delegate.jukeBox(S_STOP_EFFECTS);
    this.delegate.jukeBox(S_AMBIENCE);
    //this.delegate.sPlayer.setBackgroundMusicVolume(1);
    var sprite = obj.getParent();
    sprite.removeFromParent(true);
    this.haveBonus = false;
    this.isRolling = false;
    if (this.currentWin > 0)
    {
        this.addWin = this.currentWin / 10;
        this.setCurrentWinAnimation();

        if (this.isAutoSpinning)
        {
            if (this.currentWin > 0)
            {
                this.scheduleOnce(this.autoSpinCB, 1.5);
            }
            else
            {
                this.scheduleOnce(this.autoSpinCB, 1.0);
            }
        }
    }

    this.checkAndSetAPopUp();

},

    rollUpdator : function(dt)
    {
        var i;
        for (i = 0; i < this.totalSlots; i++)
        {
            this.slots[i].updateSlot(dt);
        }

        for (i = 0; i < this.totalSlots; i++)
        {
            if (this.slots[i].isSlotRunning())
            {
                break;
            }
        }

        if (i >= this.totalSlots)
        {
            this.isRolling = false;
            this.unschedule(this.rollUpdator);

            if (this.haveBonus)
            {
                this.isRolling = true;
                this.setBonusExcitementAnim();
            }
            else
            {
                this.calculatePayment( true );

                if (this.isAutoSpinning)
                {
                    if (this.currentWin > 0)
                    {
                        this.scheduleOnce(this.autoSpinCB, 1.5);
                    }
                    else
                    {
                        this.scheduleOnce(this.autoSpinCB, 1.0);
                    }
                }
            }
        }
    },

    setBonusExcitementAnim : function()
    {
        this.delegate.jukeBox(S_BONUS_POP);
        this.slots[0].lightsOff(0, 2);

        var sprite = new cc.Sprite( glowStrings[0] );

        var r = this.slots[0].blurReels[2];
        sprite.setPosition( r.getParent().getPosition().x + this.slots[0].getPosition().x, r.getParent().getPosition().y + this.slots[0].getPosition().y );
        sprite.runAction(new cc.RepeatForever(new cc.Sequence(new cc.FadeTo(0.5, 150), new cc.FadeTo(0.5, 255))));
        this.addChild(sprite, 5);

        this.animNodeVec.push(sprite);

        this.scaleAccordingToSlotScreen( sprite );

        this.scheduleOnce(this.stopBonusExcitement, 1.5);
    },

    setBonusWheel : function()
{
    this.delegate.jukeBox(S_WHEEL_BG);

    //LayerColor * layer = LayerColor::create( Color4B( 0, 0, 0, 160 ) );
    var layer = new cc.Node();
    layer.setPosition( cc.p( 0, 0 ) );
    layer.setTag(D_NODE_TAG);
    this.addChild(layer, 6);
    

    var ab = 1;
    var degree = 40;
    for ( var i = 0; i < 2; i++ )
    {
        var lightSprite = new cc.Sprite( "res/lobby/daily_spin_wheel/light.png" );
        lightSprite.setAnchorPoint( cc.p( 0.5, 0 ) );
        lightSprite.setPosition( this.vSize.width / 2 , 25 );
        layer.addChild( lightSprite );
        lightSprite.setRotation( degree * ab );
        lightSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.RotateBy( 3.0, -degree * 2 * ab ), new cc.DelayTime( 0.1 ), new cc.RotateBy( 3.0, degree * 2 * ab ), 
            new cc.DelayTime( 0.1 ) ) ) );

        lightSprite.setTag( LIGHT_TAG );

        ab = -1;
    }

    var wheelBGSprite = new cc.Sprite( "res/DJSDiamond3X/wheel_bg.png" );
    wheelBGSprite.setPosition(cc.p(this.vSize.width/2, this.vSize.height / 2));
    layer.addChild(wheelBGSprite);
    wheelBGSprite.setTag( WHEEL_TAG );
    this.resizer.setFinalScaling( wheelBGSprite );
    var shiftY = 7.5;

    var wheelSprite = new cc.Sprite( "res/DJSDiamond3X/wheel.png" );
    wheelSprite.setPosition(cc.p(this.vSize.width/2, this.vSize.height / 2 + shiftY));
    wheelSprite.setTag(WHEEL_TAG);
    wheelBGSprite.addChild(wheelSprite);
    //DJSResizer::getInstance().setFinalScaling( wheelSprite );

    var wheelLightSprite = new cc.Sprite( "res/DJSDiamond3X/lights.png" );
    wheelLightSprite.setPosition(cc.p(this.vSize.width/2, this.vSize.height / 2 + shiftY));
    wheelLightSprite.setTag(WHEEL_LIGHT_TAG);
    wheelBGSprite.addChild(wheelLightSprite);
    //DJSResizer::getInstance().setFinalScaling( wheelLightSprite );

    var stopperSprite = new cc.Sprite("#stopper.png");
    stopperSprite.setPosition(cc.p(this.vSize.width/2, this.vSize.height / 2 + stopperSprite.getContentSize().height * 0.85 + shiftY) );
    stopperSprite.setTag(WHEEL_STOPPER_TAG);
    layer.addChild(stopperSprite);

    this.resizer.setFinalScaling( stopperSprite );

    var menu = new cc.Menu();
    menu.setPosition(cc.p(0, 0));
    var spinItem = new cc.MenuItemSprite( new cc.Sprite( "#btn.png" ), new cc.Sprite( "#btn.png" ), this.bonusMenuCB, this);

    spinItem.setPosition(cc.p(this.vSize.width/2, this.vSize.height / 2 + shiftY));
    spinItem.setTag(SPIN_BTN_TAG);
    menu.addChild(spinItem);
    layer.addChild(menu);

    this.resizer.setFinalScaling( spinItem );
},

    setFalseWin : function()
    {
        var highWinIndices = [ 3 ];
        var missIndices = [ 3, 7 ];

        var missedIndx = Math.floor( Math.random() * 3 );

        if( missedIndx == 2 )
        {
            if( Math.floor( Math.random() * 10 ) == 3 )
            {
            }
            else
            {
                missedIndx = Math.floor( Math.random() * 2 );
            }
        }

        var sameIndex = Math.floor( Math.random( ) * highWinIndices.length );

        for ( var i = 0; i < 3; i++ )
        {
            if ( i == missedIndx )
            {
                this.resultArray[0][i] = this.getRandomIndex( missIndices[ Math.floor( Math.random( ) * missIndices.length ) ], true );
            }
            else
            {
                this.resultArray[0][i] = this.getRandomIndex( highWinIndices[ sameIndex ], false );
            }
        }
    },

    setResult : function( indx, lane, sSlot )
    {
        var gap = 387.0;

        var j = 0;
        var animOriginY;

        var r = this.slots[0].physicalReels[lane];
        var arrSize = r.elementsVec.length;
        var spr;

        var spriteVec = new Array();

        animOriginY = gap * r.direction;
        spr = r.elementsVec[indx];
        if ( spr.eCode == ELEMENT_EMPTY )
        {
            gap = 285 * spr.getScale();
        }

        if ( indx == 0 )
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push(spr);

            spr.setPositionY( animOriginY );

            for (var i = 0; i < 2; i++)
            {
                spr = r.elementsVec[i];
                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (i + 1) * r.direction);
            }
        }
        else if (indx == arrSize - 1)
        {
            spr = r.elementsVec[0];
            spriteVec.push(spr);

            spr.setPositionY(animOriginY);
            for (var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i--)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction );
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push(spr);
            spr.setPositionY(animOriginY);

            for (var i = indx; i < indx + 2; i++)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }

        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            gap = -773.5;

            var eee;
            if (i < 2)
            {
                eee = spriteVec[i + 1];
            }
            if (i == 0)
            {
                var ee = spriteVec[i + 1];
                if ( Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) ) != 387)
                {
                    gap = -Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) * 2 * ee.getScale() );
                }
            }

            gap*=r.direction;
            var ease;

            if ( this.slots[sSlot].isForcedStop )
            {
                ease =  new cc.MoveTo( 0.15, cc.p( 0, e.getPositionY() + gap) ).easing( cc.easeBackOut() );
            }
            else
            {
                ease = new cc.MoveTo( 0.3, cc.p( 0, e.getPositionY() + gap ) ).easing( cc.easeBackOut() );
            }

            ease.setTag( MOVING_TAG );
            e.runAction(ease);
        }

        return spriteVec;
    },

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSDiamond3X;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    spin : function()
    {

        this.unschedule( this.updateWinCoin );

        this.deductBet( this.currentBet );
        if (this.displayedScore != this.totalScore)
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;
        this.haveBonus = false;
        this.currentWin = 0;
        this.currentWinForcast = 0;
        this.startRolling();

        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    },

    stopBonusExcitement : function()
    {
        this.setBonusWheel();

        var sprite = this.slots[0].getChildByTag(DARK_BG_TAG);

        while (sprite)
        {
            sprite.removeFromParent(true);
            sprite = this.slots[0].getChildByTag(DARK_BG_TAG);
        }
    }

});
