/**
 * Created by RNF-Mac11 on 1/16/17.
 */

var machineLoadingStarted = false;

CustomButton = ccui.Button.extend({

    iCode : 0,
    index : 0,

    isLocked : false,

    centerLockSprite : null,

    centerLabl : null,

    ctor:function ( icde, indx, locked ) {
        this._super();
        //var pRet = new CustomButton();
        //if ( pRet && pRet.init() ){
        this.initializeComponent( icde, indx, locked );
        return true;
        //}


    },

    initializeComponent:function ( icde, indx, locked ) {
        this.iCode = icde;
        this.index = indx;
        this.isLocked = locked;

        this.centerLockSprite = null;

        this.setIcon();
    },
    setIcon : function()
    {
        this.loadTextures( "icon_" + ( this.iCode - DJS_QUINTUPLE_5X ) + ".png", "icon_" + ( this.iCode - DJS_QUINTUPLE_5X ) + ".png", "", ccui.Widget.PLIST_TEXTURE );
        var isIE = /*@cc_on!@*/false || !!document.documentMode;//changed 15 May for IE

        var verOffset;
        var nAgt = navigator.userAgent;
        if( isIE || (verOffset = nAgt.indexOf('Edge')) != -1 ) {

            //this.loadTextures( "blankIcon.png", "blankIcon.png", "", ccui.Widget.PLIST_TEXTURE );
        }
        else
        {
            var string = "res/lobby/" + machinesString[ this.iCode - DJS_QUINTUPLE_5X ] + "/" + machinesString[ this.iCode - DJS_QUINTUPLE_5X ] + ".png";
            var self = this;
            //this.loadTextures( "blankIcon.png", "blankIcon.png", "", ccui.Widget.PLIST_TEXTURE );
            DPUtils.getInstance().ImageLoadr( string, function(err, img){

                //if( self.iCode == DJS_CRYSTAL_SLOT )        return;

                var string = "res/lobby/" + machinesString[ self.iCode - DJS_QUINTUPLE_5X ] + "/" + machinesString[ self.iCode - DJS_QUINTUPLE_5X ];
                var texture2d = new cc.Texture2D();
                texture2d.initWithElement(img);
                texture2d.handleLoadedTexture();

                cc.textureCache.cacheImage( string + ".png", img );

                //cc.log( "string = " + string );

                var myAsset = gaf.Asset.create( string + ".gaf" );
                var myAnimation = myAsset.createObjectAndRun(true);
                //myAnimation.setAnchorPoint(cc.p(0.5,0.5));
                myAnimation.setTag( GAF_OBJECT_TAG );
                self.addChild(myAnimation);
                myAnimation.setPositionY( self.getContentSize().height );
                //myAnimation.setPositionX(0);
            }, self );
        }

        this.addTouchEventListener( this.touchEvent, this );
        this.checkAndSetJackPotLabel();
        if ( this.isLocked )
        {
            this.centerLockSprite = new cc.Sprite( res.Lobby_Lock_png );
            this.addChild(this.centerLockSprite);
            this.centerLockSprite.setScale(0.75);
            this.centerLockSprite.setOpacity(0);
            this.centerLockSprite.setPosition( this.getContentSize().width / 2, this.getContentSize().height * 0.75 );

            this.centerLabl = new cc.LabelTTF( " Machine\n Locked \n\n Unlock at \n Level " + unlockAtLevelArr[this.index], "HELVETICALTSTD-BOLD", 30 );
            this.centerLabl.setOpacity(0);
            this.centerLabl.setPosition( this.getContentSize().width * 0.5, cc.rectGetMinY( this.centerLockSprite.getBoundingBox() ) - this.centerLabl.getContentSize().height * 0.8);
            this.addChild(this.centerLabl, 3);
        }


    },
    jackpot :null,
    checkAndSetJackPotLabel : function()
    {
        if( this.iCode >= DJS_CRYSTAL_SLOT && this.iCode <= DJS_NEON_SLOT )
        {
            this.jackPot = JackPot.getInstance( this.iCode - DJS_QUINTUPLE_5X );
            var string = "$ " + DPUtils.getInstance().getNumString( this.jackPot.updateJackPot( null ) );

            var label = new cc.LabelTTF( "Downloading 0.0%", "HELVETICALTSTD-BOLD.OTF", 25 );
            label.setPosition( this.getContentSize().width * 0.5, this.getContentSize().height * 0.44 );
            label.setTag( JACKPOT_LABEL_TAG );

            label.runAction( new cc.RepeatForever( new cc.Sequence( new cc.DelayTime( 0.05 ), cc.callFunc( function( label ) {//lambda
            var string = "$ " + DPUtils.getInstance().getNumString( this.jackPot.updateJackPot( label ) );
            label.setString( string );
                }, this )
                 ) ) );

            this.addChild( label,2 );
        }
    },

    touchEvent : function ( pSender, type ) {
        var lobby = null;
        var delegate = AppDelegate.getInstance();
        switch ( type )
        {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                lobby = cc.Director._getInstance().getRunningScene().getChildByTag( sceneTags.LOBBY_SCENE_TAG );
                PopUps.getInstance().listOfPopUps =[];
                ServerData.getInstance().machineData = null;
                ServerData.getInstance().TRICKY_MACHINE_CODE = 0;
                 var version = parseFloat(APP_VERSION);

                if( version<this.getVersionForAvailiblity() && UserDef.getInstance().getValueForKey( SERVER_MACHINE_UNAVAILABLE, "" ).indexOf( "" + this.iCode ) != -1 )
                {
                    this.setunAvailableIcon();
                }
                else if( !this.isLocked || lobby.haveHappyHours || -1 != ServerData.getInstance().server_featured_machine_order.indexOf( "" + this.iCode ) )
                {
                    delegate.appJustLaunchedForInstantDeal = false;
                    delegate.jukeBox( S_STOP_MUSIC );
                    this.changeScene();
                }
                else
                {
                    this.setLockClickAnim();
                }
                delegate.jukeBox( S_BTN_CLICK );
                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;

            default:
                break;
        }
    },
    getVersionForAvailiblity:function(){
       var arrayString = UserDef.getInstance().getValueForKey( SERVER_MACHINE_UNAVAILABLE, "" )
        if (arrayString && arrayString.length){
            var i = 0;
            var numString = "";
            while ( 1 ) {
                if (i >= arrayString.length || arrayString[i] == ',') {
                    if (numString.length) {

                        var id = parseFloat(numString);
                        return id;
                    }
                    numString = "";
                    //a++;
                }else{
                    numString = numString + arrayString[i];
                }
                if (i >= arrayString.length) {
                    break;
                }
                i++;

            }
        }
        return -1.0;
    },
    setunAvailableIcon : function()
    {
        if ( this.getChildByTag( 1010 ) )
        {
            return;
        }

        var normal = this.getChildByTag( GAF_OBJECT_TAG );
        normal.pauseAnimation();

        var centerLockSprite = new cc.Sprite( res.Machine_unavailable_png );
        centerLockSprite.setTag( 1010 );
        this.addChild(centerLockSprite);
        //centerLockSprite.setScale(0.75);
        centerLockSprite.setOpacity(0);
        centerLockSprite.setPosition( this.getContentSize().width / 2, this.getContentSize().height * 0.5 );//(this.getContentSize().width / 2, this.getContentSize().height * 0.75);

        centerLockSprite.runAction( new cc.FadeIn( 0.5 ) );

        if( this.getChildByTag( JACKPOT_LABEL_TAG ) ) this.getChildByTag( JACKPOT_LABEL_TAG ).runAction( new cc.FadeOut( 0.5 ) );

        normal.runAction( new cc.Sequence( new cc.FadeOut( 0.5 ), cc.callFunc( function( normal ){//lambda
            normal.setVisible( false );
            this.setColor( cc.color( 0, 0, 0 ) );
        }, this ), new cc.DelayTime( 5.0 ), cc.callFunc( function( normal ){//lambda
            normal.setVisible( true );
                normal.runAction( new cc.FadeIn( 0.5 ) );
                if( this.getChildByTag( JACKPOT_LABEL_TAG ) ) this.getChildByTag( JACKPOT_LABEL_TAG ).runAction( new cc.FadeIn( 0.5 ) );
                centerLockSprite.removeFromParent( true );
                //centerLabl.removeFromParentAndCleanup( true );
                normal.resumeAnimation();
                this.setColor( cc.color( 255, 255, 255 ) );
        }, this )
        ) );
    },
    changeScene : function ()
    {
        //AppDelegate.getInstance().changeBackgroundImage( res.Splash_png );
        machineLoadingStarted = true;
        cc.director.getNotificationNode().runAction( new cc.Sequence( new cc.DelayTime( 45.0 ), cc.callFunc( function() {
            if( machineLoadingStarted )
            {
                window.location.reload(true);
            }
        }, this ) ) );

        cc.LoaderScene.preload( this.getResource( this.iCode ), function () {

            var delegate = AppDelegate.getInstance();
            var scene = this.getScene( this.iCode );
            machineLoadingStarted = false;
            cc.director.getNotificationNode().stopAllActions();
            if ( scene )
            {
                delegate.changingScene = true;
                cc.Director._getInstance().runScene( scene );
                //delegate.changeBackgroundImage( res.Background_Lobby_html_png  );
            }

        }, this);
    },

    getResource : function( tag )
    {
        var res = null;

        switch ( tag )
        {
            case DJS_QUINTUPLE_5X:
                res = Quintuple_resources;
                break;

            case DJS_LIGHTNING_REWIND:
                res = DJSLightningRewind_resources;
                break;

            case DJS_X_MULTIPLIERS:
                res = DJSXMultipliers_resources;
                break;

            case DJS_DIAMONDS_FOREVER:
                res = DJSDiamondsForever_resources;
                break;

            case DJS_TRIPLE_PAY:
                res = DJSTriplePay_resources;
                break;

            case DJS_DIAMOND_3X:
                res = DJSDiamond3x_resources;
                break;

            case DJS_DIAMOND:
                res = DJSDiamond_resources;
                break;

            case DJS_CRAZY_CHERRY:
                res = DJSCrazyCherry_resources;
                break;

            case DJS_WILD_X:
                res = DJSWildX_resources;
                break;

            case DJS_RESPIN:
                res = DJSRespin_resources;
                break;

            case DJS_RNF_STAR:
                res = DJSRNFStar_resources;
                break;

            case DJS_FREE_SPIN:
                res = DJSFreeSpins_resources;
                break;

            case DJS_SPIN_TILL_YOU_WIN:
                res = DJSSpinTillYouWin_resources;
                break;

            case DJS_FREE_SPINNER:
                res = DJSFreeSpinner_resources;
                break;

            case DJS_SPIN_TILL_YOU_WIN_TWIK:
                res = DJSSpinTillYouWinTwik_resources;
                break;

            case DJS_LIGHTNING_REWIND_TWIK:
                res = DJSLightningRewindTwik_resources;
                break;

            case DJS_FIRE:
                res = DJSFire_resources;
                break;

            case DJS_FREEZE:
                res = DJSFreeze_resources;
                break;

            case DJS_BONUS_MACHINE:
                res = DJSBonusMachine_resources;
                break;

            case DJS_CRYSTAL_SLOT:
                res = DJSCrystalSlot_resources;
                break;

            case DJS_GOLDEN_SLOT:
                res = DJSGoldenSlot_resources;
                break;

            case DJS_NEON_SLOT:
                res = DJSNeonSlot_resources;
                break;

            case DJS_HALLOWEEN_SLOT:
                res = DJSHalloweenSlot_resources;
                break;

            case DJS_MAGNET_SLIDE:
                res = DJSMagnetSlideSlot_resources;
                break;

            case DJS_DOUBLE_DIAMOND:
                res = DJSDoubleDiamond_resources;
                    break;

            default:
                break;
        }
        return res;
    },

    getScene : function( tag )
    {
        var scene = null;

        switch ( tag )
        {
            case DJS_QUINTUPLE_5X:
                scene = new DJSQuintuple5x( tag );
                break;

            case DJS_LIGHTNING_REWIND:
                scene = new DJSLightningRewind( tag );
                break;

            case DJS_X_MULTIPLIERS:
                scene = new DJSXMultipliers( tag );
                break;

            case DJS_DIAMONDS_FOREVER:
                scene = new DJSDiamondsForever( tag );
                break;

            case DJS_TRIPLE_PAY:
                scene = new DJSTriplePay( tag );
                break;

            case DJS_DIAMOND_3X:
                scene = new DJSDiamond3X( tag );
                break;

            case DJS_DIAMOND:
                scene = new DJSDiamond(tag);
                break;

            case DJS_CRAZY_CHERRY:
                scene = new DJSCrazyCherry( tag );
                break;

            case DJS_WILD_X:
                scene = new DJSWildX( tag );
                break;

            case DJS_RESPIN:
                scene = new DJSRespin( tag );
                break;

            case DJS_RNF_STAR:
                scene = new DJSRNFStar( tag );
                break;

            case DJS_FREE_SPIN:
                scene = new DJSFreeSpins( tag );
                break;

            case DJS_SPIN_TILL_YOU_WIN:
                scene = new DJSSpinTillYouWin( tag );
                break;

            case DJS_FREE_SPINNER:
                scene = new DJSFreeSpinner( tag );
                break;

            case DJS_SPIN_TILL_YOU_WIN_TWIK:
                scene = new DJSSpinTillYouWinTwik( tag );
                break;

            case DJS_LIGHTNING_REWIND_TWIK:
                scene = new DJSLightningRewindTwik( tag );
                break;

            case DJS_FIRE:
                scene = new DJSFire( tag );
                break;

            case DJS_FREEZE:
                scene = new DJSFreeze( tag );
                break;

            case DJS_BONUS_MACHINE:
                scene = new DJSBonusMachine( tag );
                break;

            case DJS_CRYSTAL_SLOT:
                scene = new DJSCrystalSlot( tag );
                break;

            case DJS_GOLDEN_SLOT:
                scene = new DJSGoldenSlot( tag );
                break;

            case DJS_NEON_SLOT:
                scene = new DJSNeonSlot( tag );
                break;

            case DJS_HALLOWEEN_SLOT:
                scene = new DJSHalloweenSlot( tag );
                break;

            case DJS_MAGNET_SLIDE:
                scene = new DJSMagnetSlide( tag );
                break;

            case DJS_DOUBLE_DIAMOND:
                scene = new DJSDoubleDiamond( tag );
                break;

            default:
                break;
        }
        return scene;
    },

    setLockClickAnim : function()
    {
        if ( this.centerLockSprite && this.centerLockSprite.getOpacity() )
        {
            return;
        }

        var normal = this.getChildByTag( GAF_OBJECT_TAG );
        if( normal )
        {
            normal.pauseAnimation();
            normal.runAction( new cc.Sequence( new cc.FadeOut( 0.5 ), cc.callFunc( function ( normal ){//lambda
                normal.setVisible( false );
                this.setColor( cc.color( 0, 0, 0 ) );//Change 23 May
            }, this ), new cc.DelayTime( 5.0 ), cc.callFunc( function( normal ){//lambda
                normal.setVisible( true );
                normal.runAction( new cc.FadeIn( 0.5 ) );
                this.centerLockSprite.runAction( new cc.FadeOut( 0.5 ) );
                this.centerLabl.runAction( new cc.FadeOut( 0.5 ) );
                normal.resumeAnimation();
                this.setColor( cc.color( 255, 255, 255 ) );//Change 23 May
            }, this ) ) );
        }
        else{
            this.runAction( new cc.Sequence( new cc.DelayTime( 0.5 ), cc.callFunc( function ( normal ){//lambda
                this.setColor( cc.color( 0, 0, 0 ) );
            }, this ), new cc.DelayTime( 5.0 ), cc.callFunc( function( normal ){//lambda
                this.setColor( cc.color( 255, 255, 255 ) );
                this.centerLockSprite.runAction( new cc.FadeOut( 0.5 ) );
                this.centerLabl.runAction( new cc.FadeOut( 0.5 ) );

            }, this ) ) );
        }
        this.centerLabl.runAction( new cc.FadeIn( 0.5 ) );
        this.centerLockSprite.runAction( new cc.FadeIn( 0.5 ) );
    }
});