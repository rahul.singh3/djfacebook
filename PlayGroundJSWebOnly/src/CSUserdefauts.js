var CSUserdefauts = (function () {
    // Instance stores a reference to the Singleton
    var instance;

    function init() {
        // Singleton
        // Private methods and variables

        return {
            storageContainer : null,//Change 16 May
            setValueForKey : function ( key, value )
            {
                this.storageContainer.set( key, value );
            },
            getValueForKey :function( key, defValue )
            {
                var value = this.storageContainer.get( key );
                if( value === undefined )
                {
                    if( defValue === undefined )
                    {
                        value = 0;
                        /*if( key == CHIPS )
                        {
                            value = 5000000;
                        }*/
                    }
                    else
                    {
                        value = defValue;
                    }
                    //this.storageContainer.set( key, value );
                }
                return value;
            },
            deleteValueForKey : function ( key )
            {
                this.storageContainer.delete( key );
            }
        };

    };
    return {
        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function () {
            if ( !instance ) {
                instance = init();
                instance.storageContainer = new Map();//change 16 MAy
            }
            return instance;
        }
    };

})();