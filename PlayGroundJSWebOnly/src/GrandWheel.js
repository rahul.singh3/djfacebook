
var IN_GAME_WHEEL     =   501;
var PAID_WHEEL        =   502;

var GrandWheel = (function () {
    // Instance stores a reference to the Singleton
    var instance;

    function init() {
        // Singleton
        // Private methods and variables
        function privateMethod(){
            //console.log( "I am private" );
        }
        var privateVariable = "Im also private";
        var privateRandomNumber = Math.random();
        return {
            factor : 0.0,
            grandWheelActive : false,
            //parent : null,
            userDefault : null,
            construct : function( p )
        {
            //this.parent = p;
            this.userDefault = CSUserdefauts.getInstance();
            this.factor = this.userDefault.getValueForKey( GRAND_WHEEL_SPIN_TO_MAKE ) / MINIMUM_SPIN_COUNT;
        },

        checkForGrandWheelActivation : function( count, amount )
        {
            if( !this.grandWheelActive && count >= this.userDefault.getValueForKey( GRAND_WHEEL_SPIN_TO_MAKE, 0 ) )
            {
                 this.grandWheelActive = true;
                 var btn = cc.director.getRunningScene().grandWheelButton;
                if( btn){
                    var timerV =  btn.getChildByTag( PROGRESS_TIMER_TAG );
                    if( timerV && timerV.getChildByTag( PROGRESS_TIMER_ANIM_TAG )){
                        cc.director.getRunningScene().grandWheelButton.getChildByTag( PROGRESS_TIMER_TAG ).getChildByTag(PROGRESS_TIMER_ANIM_TAG).runAction( new cc.RepeatForever( new cc.Sequence( new cc.RotateBy( 0.75, 360 ).easing( cc.easeBackInOut() ), new cc.DelayTime( 0.75 ) ) ) );
                        return;
                    }
                }


                  var sprite = new cc.Sprite( res.grandWheelButtonFiller );
                  sprite.setPosition( sprite.getContentSize().width * 0.5, sprite.getContentSize().height * 0.5 );
                  sprite.setTag( PROGRESS_TIMER_ANIM_TAG );
                  sprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.RotateBy( 0.75, 360 ).easing( cc.easeBackInOut() ), new cc.DelayTime( 0.75 ) ) ) );
                  if( cc.director.getRunningScene().grandWheelButton )
                      cc.director.getRunningScene().grandWheelButton.getChildByTag( PROGRESS_TIMER_TAG ).addChild( sprite, 1 );
                  if( cc.director.getRunningScene().grandWheelButton ){
                      cc.director.getRunningScene().grandWheelButton.getChildByTag( PROGRESS_TIMER_TAG ).setOpacity( 0 );

                  }

            }
        },

        checkAndSetGrandWheelOnParent : function()
        {
            if ( this.grandWheelActive )
            {
                //set grandwheel here
                if( cc.director.getRunningScene().isAutoSpinning )        cc.director.getRunningScene().removeAutoSpinning();

                var wheelLayer = new GrandWheelLayer( IN_GAME_WHEEL, cc.director.getRunningScene() );
                //DPUtils.getInstance().setTouchSwallowing( null, wheelLayer );
                cc.director.getRunningScene().addChild( wheelLayer, 50 );

            }
            else
            {
                //set current status on game scene
                this.setCurrentStatus();
            }
        },

        setCurrentStatus : function()
        {
            var visibleSize = cc.winSize;

            var layerColor = new cc.LayerColor( cc.color( 0, 0, 0, 160 ) );


            var bgSprite = new cc.Sprite( res.GrandWheelStatusPopup );
            bgSprite.setPosition( cc.p( visibleSize.width * 0.5, visibleSize.height * 0.5 ) );
            layerColor.addChild( bgSprite );

            var bgSprite2 = new cc.Sprite( res.GrandWheelStatusPopup_2 );
            bgSprite2.setPosition( bgSprite.getContentSize().width * 0.5, bgSprite.getContentSize().height * 0.5 );
            bgSprite.addChild( bgSprite2 );

            bgSprite2.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeOut( 0.25 ), new cc.FadeIn( 0.25 ) ) ) );

            var wheelSprite = new cc.Sprite( "res/GrandWheel/" + this.getMultiplierForInGameWheelStatus() + ".png" );
            wheelSprite.setPosition( bgSprite.getContentSize().width * 0.5, bgSprite.getContentSize().height * 0.55 );
            wheelSprite.setScale( 0.7 );
            wheelSprite.setRotation( 360 / 15 );
            bgSprite.addChild( wheelSprite );

            wheelSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.RotateBy( 0.75, 360 ).easing( cc.easeBackInOut() ), new cc.DelayTime( 1.5 ) ) ) );

            var spinItm = new cc.Sprite( res.GrandWheelButton );
            spinItm.setPosition( wheelSprite.getPosition() );
            spinItm.setScale( wheelSprite.getScale() );
            //spinItm.setColor( Color3B::GRAY );
            bgSprite.addChild( spinItm );

            var boundrySprite = new cc.Sprite( res.GrandWheelBoundry );
            boundrySprite.setPosition( wheelSprite.getPosition() );
            boundrySprite.setScale( wheelSprite.getScale() );
            bgSprite.addChild( boundrySprite );

            var lBarBG = new cc.Sprite( res.GrandWheelProgress_bar_bg );

            var lBar = new ccui.LoadingBar( res.GrandWheelProgress_bar_fill );
            lBar.setPercent( ( this.userDefault.getValueForKey( GRAND_WHEEL_SPIN_COUNT, 0 ) / this.userDefault.getValueForKey( GRAND_WHEEL_SPIN_TO_MAKE, 0 ) ) * 100 );
            lBar.setPosition( cc.p( lBarBG.getContentSize().width * 0.5 + 2.5, lBarBG.getContentSize().height * 0.5 ) );
            lBarBG.addChild( lBar );

            lBarBG.setPosition( cc.p( bgSprite.getContentSize().width * 0.5, bgSprite.getContentSize().height * 0.15 ) );
            bgSprite.addChild( lBarBG );

            var statusLabel = new cc.LabelTTF( this.userDefault.getValueForKey( GRAND_WHEEL_SPIN_COUNT ) + "/" + this.userDefault.getValueForKey( GRAND_WHEEL_SPIN_TO_MAKE, 0 ), "Arimo-Bold", 20 );
            statusLabel.setPosition( lBar.getPosition() );
            //statusLabel.enableOutline( cc.Color( 0, 0, 0, 255 ), 2 );
            lBarBG.addChild( statusLabel );


            var tipLabel = new cc.LabelTTF( "Spin with higher bets to win more at the Giant Wheel", "Arimo-Bold", 22.5 );
            tipLabel.setPosition( cc.p( bgSprite.getContentSize().width * 0.5, cc.rectGetMinY( lBarBG.getBoundingBox() ) - tipLabel.getContentSize().height * 0.8 ) );
            tipLabel.setFontFillColor( cc.color( 213, 255, 69, 255 ) );
            bgSprite.addChild( tipLabel );

            var skipItm = new cc.MenuItemImage( res.Close_Buttpn_png, res.Close_Buttpn_png, this.menuCB, this );

            skipItm.setPosition( cc.p( bgSprite.getContentSize().width - skipItm.getContentSize().width * 0.5, bgSprite.getContentSize().height - skipItm.getContentSize().height * 0.5 ) );
            skipItm.setTag( CLOSE_BTN_TAG );

            var menu = new cc.Menu( skipItm );
            menu.setPosition( cc.p( 0, 0 ) );
            bgSprite.addChild( menu, 5 );

            DJSResizer.getInstance().setFinalScaling( bgSprite );
            DPUtils.getInstance().setTouchSwallowing( null, bgSprite );

            cc.director.getRunningScene().addChild( layerColor, 50 );
        },

        menuCB : function( pSender)
        {
            //auto menuItm = (MenuItem * )pSender;
            pSender.getParent().getParent().getParent().removeFromParent( true );
            AppDelegate.getInstance().jukeBox(S_BTN_CLICK);
        },

        getMultiplierForInGameWheelStatus : function()
        {
            var betAmount = this.userDefault.getValueForKey( GRAND_WHEEL_BET_AMOUNT );
            var facto = this.userDefault.getValueForKey( GRAND_WHEEL_SPIN_COUNT ) / this.userDefault.getValueForKey( GRAND_WHEEL_SPIN_TO_MAKE );

            if( betAmount > (ONE_MILLION * 10 * facto * this.factor) )
            {
                return 5;
            }
            else if( betAmount > (ONE_MILLION * 5 * facto * this.factor) )
            {
                return 4;
            }
            else if( betAmount > (ONE_MILLION * 2 * facto * this.factor) )
            {
                return 3;
            }
            else if( betAmount > (500000.0 * facto * this.factor) )
            {
                return 2;
            }
            else if( betAmount > (200000.0 * facto * this.factor) )
            {
                return 1;
            }
            else
            {
                return 0;
            }
        },

        updateGrandWheelData : function( currentBet )
        {
            var count = this.userDefault.getValueForKey( GRAND_WHEEL_SPIN_COUNT, 0 ) + 1;
            if( count <= this.userDefault.getValueForKey( GRAND_WHEEL_SPIN_TO_MAKE, 0 ) )
            {
                var amount = this.userDefault.getValueForKey( GRAND_WHEEL_BET_AMOUNT,0 ) + currentBet;
                this.userDefault.setValueForKey( GRAND_WHEEL_SPIN_COUNT , count );
                this.userDefault.setValueForKey( GRAND_WHEEL_BET_AMOUNT, amount );
                var pTimer = cc.director.getRunningScene()/*this.parent*/.grandWheelButton.getChildByTag( PROGRESS_TIMER_TAG );
                pTimer.setPercentage( ( count / this.userDefault.getValueForKey( GRAND_WHEEL_SPIN_TO_MAKE, 0 ) ) * 100 );

                var label = cc.director.getRunningScene().getChildByTag( 21011 );

                if( label )             label.setString( DPUtils.getInstance().getNumString( amount ) );

                this.checkForGrandWheelActivation( count, amount );

                if( count>=this.userDefault.getValueForKey( GRAND_WHEEL_SPIN_TO_MAKE, 0 ) )
                    cc.director.getRunningScene()/*this.parent*/.grandWheelButton.runAction( new cc.Repeat( new cc.Sequence( new cc.ScaleTo( 0.75, 1.2 ).easing( cc.easeBackInOut() ),
                        new cc.ScaleTo( 0.75, 1.0 ).easing( cc.easeBackInOut() ) ), 2 ) );//setCurrentStatus();
            }else if(count>this.userDefault.getValueForKey( GRAND_WHEEL_SPIN_TO_MAKE, 0 )){
                var amount = this.userDefault.getValueForKey( GRAND_WHEEL_BET_AMOUNT,0 ) + currentBet;
                this.checkForGrandWheelActivation( count, amount );

            }
        }
        };
    };
    return {
        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function ( p ) {
            if ( !instance ) {
                instance = init();
                instance.construct( p );
            }
            return instance;
        }
    };
})();