var DJSResizer = (function () {
    // Instance stores a reference to the Singleton
    var instance;

    function init() {
        // Singleton
        // Private methods and variables
        function privateMethod(){
            //console.log( "I am private" );
        }
        var privateVariable = "Im also private";
        var privateRandomNumber = Math.random();
        return {
            // Public methods and variables
            /*publicMethod: function () {
             console.log( "The public can see me!" );
             },
             publicProperty: "I am also public",
             getRandomNumber: function() {
             return privateRandomNumber;
             }*/
            finalScaleX : 0,
            finalScaleY : 0,
            nodeArray : new Array(),
            nodeScaleArray : new Array(),
            setFinalScaling : function ( node ) {
                return;
                this.finalScaleX = cc.view.getFrameSize().width;
                this.finalScaleY = cc.view.getFrameSize().height;
                this.nodeArray.push(node);
                this.nodeScaleArray.push( cc.p( node.getScaleX(), node.getScaleY() ) );
                var scale = Math.abs( this.finalScaleX - this.finalScaleY );

                if ( this.finalScaleX > this.finalScaleY )
                {
                    node.setScaleX( node.getScaleX() * ( this.finalScaleY / this.finalScaleX ) );
                }
                else if ( this.finalScaleX < this.finalScaleY )
                {
                    node.setScale( node.getScaleX() + scale, node.getScaleY() - scale );
                }
            },
            reScaleNodes : function ()
            {
                return;
                this.finalScaleX = cc.view.getFrameSize().width;
                this.finalScaleY = cc.view.getFrameSize().height;

                var scale = Math.abs( this.finalScaleX - this.finalScaleY );

                if ( this.finalScaleX > this.finalScaleY )
                {
                    for (var i in this.nodeArray) {
                       var node = this.nodeArray[i];
                        node.setScaleX( this.nodeScaleArray[i].x );
                        node.setScaleY( this.nodeScaleArray[i].y );
                        node.setScaleX( node.getScaleX() * ( this.finalScaleY / this.finalScaleX ) );
                    }
                }
                else if ( this.finalScaleX < this.finalScaleY )
                {
                    for (var i in this.nodeArray) {
                        var node = this.nodeArray[i];
                        node.setScaleX( this.nodeScaleArray[i].x );
                        node.setScaleY( this.nodeScaleArray[i].y );
                        node.setScale( node.getScaleX() + scale, node.getScaleY() - scale );
                    }
                }
            }
        };
    };
    return {
        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function () {
            if ( !instance ) {
                instance = init();
            }
            return instance;
        }
    };
})();