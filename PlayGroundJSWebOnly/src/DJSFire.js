/**
 * Created by RNF-Mac11 on 2/7/17.
 */



var reelsRandomizationArrayDJSFire =
    [
        [// 85% payout
            2000, 700, 800, 800, 4500,
            300, 4000, 500, 3500, 450,
            5000, 750, 100, 800, 3500,
            300, 1000, 500, 3200, 500,
            5700, 800, 750, 700, 4500,
            400, 4500, 400, 5000, 450,
            3000, 300, 4200, 200, 4800, 200
        ],
        [//92 %
            2000, 700, 870, 800, 4500,
            300, 4000, 500, 3500, 450,
            5000, 750, 110, 800, 3500,
            300, 1000, 500, 3200, 500,
            5700, 800, 870, 700, 4500,
            400, 4500, 400, 5000, 450,
            3000, 300, 4200, 200, 4800, 200
        ],
        [//97 %
            2000, 700, 900, 800, 4500,
            300, 4000, 500, 3500, 450,
            5000, 750, 120, 800, 3500,
            300, 1000, 500, 3200, 500,
            5700, 800, 920, 700, 4500,
            400, 4500, 400, 5000, 450,
            3000, 300, 4200, 200, 4800, 200
        ],
        [//140 %
            2000, 600, 1100, 500, 4500,
            300, 4000, 400, 3500, 450,
            5000, 750, 400, 800, 3500,
            200, 1000, 300, 3200, 400,
            5700, 700, 1100, 700, 4500,
            300, 4500, 200, 5000, 450,
            3000, 300, 4200, 200, 4800, 200
        ],
        [//300 %
            2000, 600, 1500, 500, 4500,
            300, 4000, 400, 3500, 450,
            5000, 750, 1300, 800, 3500,
            200, 1000, 300, 3200, 400,
            5700, 700, 1100, 700, 4500,
            300, 4500, 200, 5000, 450,
            3000, 300, 4200, 200, 4800, 200
        ]
    ];

var DJSFire = GameScene.extend({

    fireGenerated : false,

    haveFireElementAt : new Array( 3 ),
    reelsArrayDJSFire:[],
    ctor:function ( level_index ) {
        this.reelsArrayDJSFire = [
            /*1*/ELEMENT_7_BAR, /*2*/ELEMENT_EMPTY, /*3*/ELEMENT_2X, /*4*/ELEMENT_EMPTY, /*5*/ELEMENT_BAR_1,
            /*6*/ELEMENT_EMPTY, /*7*/ELEMENT_BAR_2, /*8*/ELEMENT_EMPTY,/*9*/ELEMENT_BAR_3, /*10*/ELEMENT_EMPTY,
            /*11*/ELEMENT_CHERRY, /*12*/ELEMENT_EMPTY, /*13*/ELEMENT_5X, /*14*/ELEMENT_EMPTY, /*15*/ELEMENT_BAR_3,
            /*16*/ELEMENT_EMPTY, /*17*/ELEMENT_7_BAR, /*18*/ELEMENT_EMPTY, /*19*/ELEMENT_CHERRY, /*20*/ELEMENT_EMPTY,
            /*21*/ELEMENT_7_BAR, /*22*/ELEMENT_EMPTY, /*23*/ELEMENT_2X, /*24*/ELEMENT_EMPTY, /*25*/ELEMENT_BAR_2,
            /*26*/ELEMENT_EMPTY, /*27*/ELEMENT_CHERRY, /*28*/ELEMENT_EMPTY, /*29*/ELEMENT_BAR_1, /*30*/ELEMENT_EMPTY,
            /*31*/ELEMENT_BAR_3, /*32*/ELEMENT_EMPTY, /*33*/ELEMENT_BAR_2, /*34*/ELEMENT_EMPTY, /*35*/ ELEMENT_BAR_1,
            /*36*/ELEMENT_EMPTY
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSFire;
        this.physicalArrayCheck= this.reelsArrayDJSFire;
        this._super( level_index );

        this.startLoadingMechanism( level_index );

        var spriteFrameCache = cc.spriteFrameCache;
        spriteFrameCache.addSpriteFrames( "res/DJSFire/FireFrames.plist", "res/DJSFire/FireFrames.png" );


        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;


    },

    calculatePayment : function( doAnimate )
    {
        var payMentArray = [
            /*0*/     20, // 7Bar 7Bar 7Bar

            /*1*/     10, // Cherry Cherry Cherry

            /*2*/     2, // Any cherry and 7 Bar combo

            /*3*/     1,  // Any Bar Combo

            /*4*/     5,  // 3Bar 3Bar 3Bar

            /*5*/     3,   // 2Bar 2Bar 2Bar

            /*6*/     2   // Bar Bar Bar
        ];

        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        for ( var i = 0; i < this.slots[0].resultReel.length; i++ )
        {
            tmpVector = this.slots[0].resultReel[ i ];
            tmpVector2.push( tmpVector[ 1 ] );
        }

        this.lineVector.push( tmpVector2 );

        //bool generateThrice = false;

        var pay = 0;
        var tmpPay = 1;
        var lineLength = 3;
        var count2X = 0;
        var count5X = 0;
        var countWild = 0;
        var countGeneral = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var i, j, k;
        var e = null;
        var pivotElm = null;
        var pivotElm2 = null;
        var tmpVec;

        for ( i = 0; i < this.totalLines; i++ )
        {
            tmpVec = this.lineVector[ i ];
            for ( k = 0; k < 8; k++ )
            {
                var breakLoop = false;
                countGeneral = 0;
                switch ( k )
                {
                    case 0://for all combination with wild
                        for ( j = 0; j < lineLength; j++ )
                        {
                            if ( this.resultArray[0][j] != -1 && this.reelsArrayDJSFire[this.resultArray[i][j]]!= ELEMENT_EMPTY )
                            {
                                switch ( this.reelsArrayDJSFire[this.resultArray[i][j]] )
                                {
                                    case ELEMENT_2X:
                                        count2X++;
                                        tmpPay*=2;
                                        countWild++;

                                        if ( doAnimate )
                                        {
                                            if ( j < tmpVec.length )
                                            {
                                                e = tmpVec[j];
                                            }
                                            this.haveFireElementAt[j] = true;
                                            this.canChangeBet = false;
                                        }

                                        if ( e )
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_5X:
                                        count5X++;
                                        tmpPay*=5;
                                        countWild++;

                                        if ( doAnimate )
                                        {
                                            if ( j < tmpVec.length )
                                            {
                                                e = tmpVec[j];
                                            }
                                            this.haveFireElementAt[j] = true;
                                            this.canChangeBet = false;
                                        }
                                        if ( e )
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        switch ( pivotElement )
                                        {
                                            case -1:
                                                pivotElement = this.reelsArrayDJSFire[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate ) {
                                                    if ( j < tmpVec.length )
                                                    {
                                                        e = tmpVec[j];
                                                    }
                                                }
                                                if ( e )
                                                {
                                                    pivotElm = e;
                                                }
                                                break;

                                            default:
                                                pivotElement2 = this.reelsArrayDJSFire[this.resultArray[i][j]];
                                                countGeneral++;

                                                if ( doAnimate ) {
                                                    if ( j < tmpVec.length )
                                                    {
                                                        e = tmpVec[j];
                                                    }
                                                }
                                                if ( e )
                                                {
                                                    pivotElm2 = e;
                                                }
                                                break;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                //break;
                            }
                        }

                        if ( j == 3 )
                        {
                            if ( countWild >= 3 )
                            {
                                breakLoop = true;
                                this.fireGenerated = false;
                                pay = tmpPay * payMentArray[0];
                                //generateThrice = true;
                            }
                            else if( countWild >= 2 )
                            {
                                breakLoop = true;

                                switch ( pivotElement )
                                {
                                    case ELEMENT_7_BAR:
                                        pay = tmpPay * payMentArray[0];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_CHERRY:
                                        pay = tmpPay * payMentArray[1];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_3:
                                        pay = tmpPay * payMentArray[4];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_2:
                                        pay = tmpPay * payMentArray[5];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_1:
                                        pay = tmpPay * payMentArray[6];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        break;
                                }
                            }
                            else if( countWild == 1 )
                            {
                                breakLoop = true;

                                if ( pivotElement == pivotElement2 )
                                {
                                    switch ( pivotElement )
                                    {
                                        case ELEMENT_7_BAR:
                                            pay = tmpPay * payMentArray[0];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_CHERRY:
                                            pay = tmpPay * payMentArray[1];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_3:
                                            pay = tmpPay * payMentArray[4];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_2:
                                            pay = tmpPay * payMentArray[5];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_1:
                                            pay = tmpPay * payMentArray[6];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        default:
                                            //pay = tmpPay;
                                            break;
                                    }
                                }
                                else
                                {
                                    if ( ( pivotElement == ELEMENT_7_BAR || pivotElement == ELEMENT_CHERRY ) &&
                                        ( pivotElement2 == ELEMENT_7_BAR || pivotElement2 == ELEMENT_CHERRY ) )
                                    {
                                        pay = tmpPay * payMentArray[2];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if ( ( pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_BAR_3 ) &&
                                        ( pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_BAR_3 ) )
                                    {
                                        pay = tmpPay * payMentArray[3];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    /*else
                                     {
                                     pay = tmpPay;
                                     }*/
                                }
                            }
                        }
                        break;

                    case 1: // 7 7 7 bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFire[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[0];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                                if ( doAnimate ) {
                                    if ( l < tmpVec.length )
                                    {
                                        e = tmpVec[ l ];
                                    }
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 2: // Triple Cherry
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFire[this.resultArray[i][j]] )
                            {
                                case ELEMENT_CHERRY:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[1];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                                if ( doAnimate ) {
                                    if ( l < tmpVec.length )
                                    {
                                        e = tmpVec[ l ];
                                    }
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 3: // 7bar and Cherry Combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFire[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_BAR:
                                case ELEMENT_CHERRY:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[2];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                                if ( doAnimate ) {
                                    if ( l < tmpVec.length )
                                    {
                                        e = tmpVec[ l ];
                                    }
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 4: // 3Bar 3Bar 3Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFire[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[4];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                                if ( doAnimate ) {
                                    if ( l < tmpVec.length )
                                    {
                                        e = tmpVec[ l ];
                                    }
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 5: // 2Bar 2Bar 2Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFire[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[5];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                                if ( doAnimate ) {
                                    if ( l < tmpVec.length )
                                    {
                                        e = tmpVec[ l ];
                                    }
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 6: // Bar Bar Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFire[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[6];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                                if ( doAnimate ) {
                                    if ( l < tmpVec.length )
                                    {
                                        e = tmpVec[ l ];
                                    }
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 7: // any Bar combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFire[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[3];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( doAnimate ) {
                                    if ( l < tmpVec.length )
                                    {
                                        e = tmpVec[ l ];
                                    }
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

                if ( breakLoop )
                {
                    break;
                }
            }
        }

        if ( !this.fireGenerated && doAnimate )
        {
            /*if ( generateThrice )
             {
             #ifndef HACK_BUILD
             setFlamesAnimation();
             #endif
             }*/
            for ( var i = 0; i < 3; i++ )
            {
                this.haveFireElementAt[i] = false;
            }
            this.canChangeBet = true;
        }
        /*else if ( fireGenerated )
         {
         #ifndef HACK_BUILD
         setFlamesAnimation();
         #endif
         }*/

        if( doAnimate )
        {
            this.currentWin = this.currentBet * pay;
            if ( this.currentWin > 0 )
            {
                this.addWin = this.currentWin / 10;
                this.setCurrentWinAnimation( 0 );
            }

            this.checkAndSetAPopUp();
        }
        else
        {
            this.currentWinForcast = this.currentBet * pay;
        }
    },

    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSFire[i]=this.physicalReelElements[idx];
                i++;
            }
        }
    },
    changeTotalProbabilities : function()
    {
        this.totalProbalities = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            //this.extractArrayFromSring();
            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[this.easyModeCode][i];
            }
        }
        else
        {
            for ( var i = 0; i < 36; i++ )
            {
                this.probabilityArray[this.easyModeCode][i] = reelsRandomizationArrayDJSFire[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSFire[this.easyModeCode][i];
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
    },
tmpp:0,
    generateResult : function( scrIndx )
    {
        var wildGeneratedCount = 0;
        for ( ; ; ) {
            //generate:
            for (var i = 0; i < this.slots[scrIndx].displayedReels.length; i++) {
                if (!this.haveFireElementAt[i]) {
                    var num = this.prevReelIndex[i];

                    var tmpnum = 0;

                    while (num == this.prevReelIndex[i]) {
                        num = Math.floor(Math.random() * this.totalProbalities);
                    }
                    this.prevReelIndex[i] = num;
                    this.resultArray[0][i] = num;

                    for (var j = 0; j < 36; j++) {
                        tmpnum += this.probabilityArray[this.easyModeCode][j];

                        if (tmpnum >= this.resultArray[0][i]) {
                            this.resultArray[0][i] = j;
                            break;
                        }
                    }
                    //resultArray[0][i] = 17;
                    /*if( this.tmpp == 0 )
                    {
                        this.resultArray[0][0] = 22;
                        this.resultArray[0][1] = 14;
                        this.resultArray[0][2] = 16;

                    }
                    else if( this.tmpp == 1 )
                    {
                        //this.resultArray[0][0] = 22;
                        this.resultArray[0][1] = 28;
                        this.resultArray[0][2] = 26;
                        //this.tmpp++;
                    }
                    else
                    {
                        //this.resultArray[0][2] = 10;
                        //this.tmpp++;
                    }*/

                    //cc.log( "this.resultArray[0][" + i + "] = " + this.resultArray[0][i] );
                    if (this.reelsArrayDJSFire[this.resultArray[0][i]] == ELEMENT_2X || this.reelsArrayDJSFire[this.resultArray[0][i]] == ELEMENT_5X) {
                        this.fireGenerated = true;
                        wildGeneratedCount++;
                    }
                }
            }

            if ( wildGeneratedCount > 1 ) {
                wildGeneratedCount = 0;
                this.fireGenerated = false;
                //goto
                //generate;
            }
            else
            {
                break;
            }
        }
        /*
         int highPay[][3] = {
         {15, 21, 15}, //2x 5x 2x
         {15, 19, 15}, //2x 4x 2x
         {15, 17, 15}, //2x 3x 2x
         {7, 7, 7},    //Triple 7 Red
         {5, 5, 5},    //Triple 7 Yellow
         {29, 29, 29}, //Triple 7 Green
         {1, 1, 1},    //Triple 7 Bar
         {31, 31, 31}, //Triple 3Bar
         {27, 27, 27}, //Triple 2Bar
         {23, 23, 23}, //Triple Bar
         };*/
        /*/HACK
         this.resultArray[0][0] = 22;
         this.resultArray[0][1] = 28;
         this.resultArray[0][2] = 29;
         //*///HACK
        var i;

        for ( i = 0; i < 3; i++ )
        {
            if ( this.haveFireElementAt[i] )
            {
                break;
            }
        }
        if ( i >= 3 )
        {
            this.calculatePayment( false );
            this.checkForFalseWin();

            if( this.applyStrategy )
            {
                if( this.currentBet <= 100 )
                {
                    this.applyStrategy = false;
                    CSUserdefauts.getInstance().setValueForKey( STRATEGY_ASSIGNED, true );
                    var highPay = [18, 18, 12]; //diamond diamond 5x

                    for ( var i = 0; i < 3; i++ )
                    {
                        this.resultArray[0][i] = highPay[i];
                    }
                    this.fireGenerated = true;
                }
                else if( this.currentBet <= 1000 )
                {
                    this.applyStrategy = false;
                    CSUserdefauts.getInstance().setValueForKey( STRATEGY_ASSIGNED, true );
                    var highPay = [30, 30, 12];//triple bar triple bar 5x

                    this.fireGenerated = true;
                    for ( var i = 0; i < 3; i++ )
                    {
                        this.resultArray[0][i] = highPay[i];
                    }
                }
            }
        }
        this.tmpp++;
    },

    removeFlamesAnimation : function()
    {
        for ( var i = FLAME_ANIM_TAG; i < FLAME_ANIM_TAG + 3; i++ )
        {
            while ( this.getChildByTag( i ) )
            {
                this.removeChildByTag( i );
            }
        }
    },

    rollUpdator : function(dt)
    {
        var i;
        for (i = 0; i < this.totalSlots; i++)
        {
            this.slots[i].updateSlot(dt);
        }

        for (i = 0; i < this.totalSlots; i++)
        {
            if ( this.slots[i].isSlotRunning() )
            {
                break;
            }
        }

        if (i >= this.totalSlots)
        {
            this.isRolling = false;
            this.unschedule( this.rollUpdator );
            this.calculatePayment( true );

            if (this.isAutoSpinning)
            {
                if (this.currentWin > 0)
                {
                    this.scheduleOnce( this.autoSpinCB, 1.5);
                }
                else
                {
                    this.scheduleOnce( this.autoSpinCB, 1.0);
                }
            }
        }
    },

    setFalseWin : function()
    {
        if ( !this.canChangeBet )
        {
            return;
        }
        var highWinIndices = [ 0, 18, 30 ];
        var missIndices = [ 2, 12 ];

        var missedIndx = Math.floor( Math.random() * 3 );

        if( missedIndx == 2 )
        {
            if( Math.floor( Math.random() * 10 ) == 3 )
            {
            }
            else
            {
                missedIndx = Math.floor( Math.random() * 2 );
            }
        }

        var sameIndex = Math.floor( Math.random() * highWinIndices.length );

        for ( var i = 0; i < 3; i++ )
        {
            if ( i == missedIndx )
            {
                this.resultArray[0][i] = this.getRandomIndex( missIndices[ Math.floor( Math.random() * missIndices.length ) ], true );
            }
            else
            {
                this.resultArray[0][i] = this.getRandomIndex( highWinIndices[ sameIndex ], false );
            }

            //cc.log( "in false win this.resultArray[0][" + i + "] = " + this.resultArray[0][i] );
        }
    },

    setFlamesAnimation : function( laneCode )
    {
        //cc.log( "laneCode = " + laneCode );
        if ( this.getChildByTag( FLAME_ANIM_TAG + laneCode ) )
        {
            return;
        }
        var r = this.slots[0].blurReels[laneCode];

        var node = r.getParent();

        var flameSprite = new cc.Sprite( "#fire_1.png" );
        var flameSpriteUp = new cc.Sprite( "#fire_1.png" );

        var walkAnimFrames = [];

        for (var i = 1; i < 28; i++) {
            var str = "fire_" + i + ".png";
            var frame = cc.spriteFrameCache.getSpriteFrame(str);
            walkAnimFrames.push(frame);
        }
        var walkAnim = new cc.Animation(walkAnimFrames, 0.03);

        var tmpAnimate = cc.animate( walkAnim );

        flameSprite.runAction(tmpAnimate.repeatForever());

        var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
        diff*=( 2 - laneCode );

        flameSprite.setPosition( node.getPositionX() + this.slots[0].getPositionX() - diff, node.getPositionY() + this.slots[0].getPositionY() );

        flameSprite.setTag( FLAME_ANIM_TAG + laneCode );
        this.addChild(flameSprite, 5);

        flameSpriteUp.runAction( tmpAnimate.clone() );

        flameSpriteUp.setPosition( flameSprite.getPosition() );

        flameSpriteUp.setTag( FLAME_ANIM_TAG + laneCode );
        flameSpriteUp.setRotation( 180 );
        this.addChild(flameSpriteUp, 5);

        //DJSResizer::getInstance().setFinalScaling( flameSprite );
        //DJSResizer::getInstance().setFinalScaling( flameSpriteUp );
        this.scaleAccordingToSlotScreen( flameSprite );
        this.scaleAccordingToSlotScreen( flameSpriteUp );

        this.delegate.jukeBox( S_DJS_FIRE );
    },

    setResult : function( indx, lane, sSlot )
    {
        var gap = 387;

        var j = 0;
        var animOriginY;

        var r = this.slots[0].physicalReels[lane];
        var arrSize = r.elementsVec.length;
        var spr;

        var spriteVec = new Array();

        animOriginY = gap * r.direction;
        spr = r.elementsVec[indx];
        if ( typeof ( spr ) != undefined && spr.eCode == ELEMENT_EMPTY )
        {
            gap = 285 * spr.getScale();
        }

        if (indx == 0)
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push(spr);
            if ( !this.haveFireElementAt[lane] )
                spr.setPositionY(animOriginY);

            for (var i = 0; i < 2; i++)
            {
                spr = r.elementsVec[i];
                spriteVec.push(spr);
                if ( !this.haveFireElementAt[lane] )
                    spr.setPositionY(animOriginY + gap * (i + 1) * r.direction);
            }
        }
        else if (indx == arrSize - 1)
        {
            spr = r.elementsVec[0];
            spriteVec.push(spr);
            if ( !this.haveFireElementAt[lane] )
                spr.setPositionY(animOriginY);
            for (var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i--)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                if ( !this.haveFireElementAt[lane] )
                    spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push(spr);
            if ( !this.haveFireElementAt[lane] )
                spr.setPositionY(animOriginY);

            for (var i = indx; i < indx + 2; i++)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                if ( !this.haveFireElementAt[lane] )
                    spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }

        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            gap = -773.5;

            var eee;
            if (i < 2)
            {
                eee = spriteVec[i + 1];
            }
            if (i == 0)
            {
                var ee = spriteVec[i + 1];
                if ( Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) ) != 387 )
                {
                    gap = - Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) * 2 * ee.getScale() );
                }
            }

            gap*=r.direction;
            var ease;

            var time = 0.3;

            if ( this.slots[sSlot].isForcedStop )
            {
                ease = new cc.MoveTo( 0.15, cc.p( 0, e.getPositionY() + gap ) ).easing( cc.easeBackOut() );
                time = 0.15;
            }
            else
            {
                ease = new cc.MoveTo( 0.3, cc.p( 0, e.getPositionY() + gap ) ).easing( cc.easeBackOut() );
            }

            ease.setTag( MOVING_TAG );
            if ( !this.haveFireElementAt[lane] )
                e.runAction(ease);

            if ( this.reelsArrayDJSFire[ indx ] == ELEMENT_2X || this.reelsArrayDJSFire[ indx ] == ELEMENT_5X )
            {
                var node = new cc.Node();//lambda

                node.runAction( new cc.Sequence( new cc.DelayTime( time ),
                    cc.callFunc( function( ){
                        node.removeFromParent( true );
                        this.setFlamesAnimation( lane );
                    }, this, lane, node ) ) );
                this.addChild( node );
            }
        }

        return spriteVec;
    },

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSFire;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    setWinAnimation : function( animCode, screen )
    {
        var tI = 0.3;

        var fire;
        for ( var i = 0; i < this.animNodeVec.length; i++ )
        {
            fire = this.animNodeVec[ i ];
            fire.removeFromParent( true );
        }
        this.animNodeVec = [];


        switch ( animCode )
        {
            case WIN_TAG:

                for (var i = 0; i < this.slots[0].blurReels.length; i++)
                {
                    var r = this.slots[0].blurReels[ i ];

                    var node = r.getParent();

                    var moveW = this.slots[0].reelDimentions.x * this.slots[0].getScale();
                    var moveH = this.slots[0].reelDimentions.y * this.slots[0].getScale();

                    for ( var j = 0; j < 4; j++ )
                    {

                        var fire = new cc.ParticleSun();
                        fire.texture = cc.textureCache.addImage( res.FireStar_png );

                        var pos;

                        this.addChild( fire, 5 );
                        this.animNodeVec.push( fire );

                        var moveBy = null;

                        var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                        diff*=( 2 - i );

                        switch ( j % 4 )
                        {
                            case 0:
                                pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition( pos );
                                moveBy = new cc.MoveBy( tI, cc.p( moveW, 0 ) );
                                break;

                            case 1:
                                pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition( pos );
                                moveBy = new cc.MoveBy( tI, cc.p( 0, moveH ) );
                                break;

                            case 2:
                                pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition( pos );
                                moveBy = new cc.MoveBy( tI, cc.p( -moveW, 0 ) );
                                break;

                            case 3:
                                pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition( pos );
                                moveBy = new cc.MoveBy( tI, cc.p( 0, -moveH ) );
                                break;

                            default:
                                break;
                        }
                        fire.runAction( new cc.RepeatForever( new cc.Sequence( moveBy, moveBy.reverse() ) ) );
                    }
                }

                break;

            case BIG_WIN_TAG:
                for(var i = 0; i < this.slots[0].blurReels.length; i++)
                {
                    var r = this.slots[0].blurReels[i];

                    var node = r.getParent();

                    var glowSprite = new cc.Sprite( glowStrings[6] );
                    var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                    diff*=( 2 - i );

                    if (glowSprite)
                    {
                        glowSprite.setPosition( node.getPositionX() + this.slots[0].getPositionX() - diff, node.getPositionY() + this.slots[0].getPositionY() );


                        this.addChild(glowSprite, 5);
                        this.animNodeVec.push(glowSprite);

                        glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeOut( 0.3 ), new cc.FadeIn( 0.3 ) ) ) );

                        this.scaleAccordingToSlotScreen( glowSprite );
                    }
                }
                break;

            default:
                break;
        }
    },

    spin : function()
    {

        this.unschedule( this.updateWinCoin );

        this.deductBet( this.currentBet );
        if (this.displayedScore != this.totalScore)
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }
        this.fireGenerated = false;
        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;
        this.currentWin = 0;
        this.currentWinForcast = 0;
        var i = 0;
        for( i = 0; i < 3; i++ )
        {
            if ( this.haveFireElementAt[i] )
            {
                break;
            }
        }
        if ( i >= 3 )
        {
            this.removeFlamesAnimation();
        }
        this.startRolling();

        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    }

});