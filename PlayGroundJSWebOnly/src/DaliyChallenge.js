       var  MISSION_ANIM_TAG = 11;
       var MISSION_HEADING_TAG = 12;
       var MISSION_LOCK_GIFT_TAG = 13;

DaliyChallenge = cc.Layer.extend({
    bgDCSprite :null,
    leftArrow:null,
    rightArrow:null,
    pageView:null,
    itemIdx:0,
    nodeVec:[],
    totalIndexOfScroll:3,
    numberoftaskOpen:1,
    dc_blank_mission:null,
    btnTag:0,
    dailyChanllengeTxt:null,
    BonusTimeObj:null,
    isBoxOpen:false,
    customaLabeButton:null,
    giftBoxAnim:null,
    taskBaseImage:null,
    layoutTag:0,
    spWinDisplayWin:null,
    movePageView:false,
    ctor: function (code) {
        this._super();
        this.setTag(code);
        //var pRet = new CustomButton();
        //if ( pRet && pRet.init() ){
        this.createComponent(code);
        return true;
        //}
    },
    createComponent: function (code) {
        var lvl = code;
        var visibleSize = cc.winSize;
        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( visibleSize.width * 0.5, visibleSize.height * 0.5 ) );
        this.addChild( darkBG );
        this.bgDCSprite = new cc.Sprite( res.dc_bg );
        this.bgDCSprite.setPosition( cc.p( visibleSize.width / 2, visibleSize.height / 2 ) );
        this.addChild( this.bgDCSprite );

        this.dc_blank_mission = new cc.Sprite(res.dc_dc_blank_mission);
        this.dc_blank_mission.setAnchorPoint(cc.p(0, 1));
        this.dc_blank_mission.setPosition(cc.p(this.bgDCSprite.getContentSize().width*0.1, this.bgDCSprite.getContentSize().height*0.93));

        this.bgDCSprite.addChild(this.dc_blank_mission);

        this.createHeadingComponents();

        this.leftArrow = new cc.MenuItemSprite( new cc.Sprite( res.dc_right_arrow ), new cc.Sprite( res.dc_right_arrow ), this.menuCB, this );

        this.leftArrow.setPosition( cc.p(this.leftArrow.getContentSize().width * 0.8, this.bgDCSprite.getContentSize().height * 0.32 ));
        this.leftArrow.setTag( LEFT_ARROW );
        this.leftArrow.setRotation( 180 );
        this.leftArrow.setVisible( true );

        this.rightArrow = new cc.MenuItemSprite( new cc.Sprite( res.dc_right_arrow ), new cc.Sprite( res.dc_right_arrow ), this.menuCB, this );
        this.rightArrow.setPosition(cc.p( this.bgDCSprite.getContentSize().width - this.rightArrow.getContentSize().width * 0.8,  this.leftArrow.getPositionY() ));
        this.rightArrow.setTag( RIGHT_ARROW );

        var spBlank = new cc.Sprite(res.dc_DcBlank);
        spBlank.setPosition(cc.p(this.bgDCSprite.getContentSize().width*0.5, this.bgDCSprite.getContentSize().height*0.3));
        this.bgDCSprite.addChild(spBlank);
        var menu = new cc.Menu();
        menu.addChild( this.leftArrow );
        menu.addChild( this.rightArrow );
        menu.setPosition( cc.p( 0, 0 ) );
        this.bgDCSprite.addChild( menu ,2);
        var closestr = "res/lobby/buyPage_1_19/close_btn.png";
        var buycloseItem = new cc.MenuItemImage( closestr, closestr, this.menuCB, this );
        buycloseItem.setPosition(cc.p(this.bgDCSprite.getContentSize().width-buycloseItem.getContentSize().width*0.9,this.bgDCSprite.getContentSize().height-buycloseItem.getContentSize().height));
        buycloseItem.setTag( CLOSE_BTN_TAG );
        var menu1 = new cc.Menu(buycloseItem);
        menu1.setPosition( cc.p( 0, 0 ) );
        this.bgDCSprite.addChild( menu1 ,2);

        this.initalStageOfPageLayoutBar(spBlank.getContentSize(),spBlank.getPosition(),spBlank);
        var pageVisibleSize =spBlank.getContentSize();
        var idx = 0;
        this.itemIdx =0;
        for (var i = 0; i < this.totalIndexOfScroll; i++) { // task Size divided by 3 for pagelayout
            var pageViewLayout = new ccui.Layout();
            pageViewLayout.setAnchorPoint(cc.p(0,1));
            pageViewLayout.setContentSize(pageVisibleSize);
            pageViewLayout.setPosition(cc.p(0,0));
            pageViewLayout.setPositionX(pageVisibleSize.width*i);
            pageViewLayout.setTag(i);
            this.addElementOnPageLayout(pageViewLayout,idx,i);
            this.pageView.insertCustomItem(pageViewLayout, i);
            idx =idx+3;
        }
        this.BonusTimeObj =  BonusTimer.getInstance();
        this.BonusTimeObj.updateTimeForDaliyChanllenge();
        this.dailyChanllengeTxt = new cc.LabelTTF( "00:00:00", "DS-DIGIT", 30 );
        this.dailyChanllengeTxt.setAnchorPoint(cc.p(0,0.5));
        this.dailyChanllengeTxt.setPosition(this.bgDCSprite.getContentSize().width*0.85,this.bgDCSprite.getContentSize().height*0.77);
        this.bgDCSprite.addChild(this.dailyChanllengeTxt);
        this.updator(0);
        this.schedule( this.updator, 1.0 );
        DPUtils.getInstance().easyBackOutAnimToPopup(this.bgDCSprite,0.3);
        this.scheduleOnce( this.updatorMovePageView, 0.7 );
    },
    updatorMovePageView:function(dt){
        if (this.numberoftaskOpen>=4) {
            this.menuCB(this.rightArrow);
        }else{
            this.leftArrow.setEnabled(false);
            this.rightArrow.setEnabled(true);
        }
    },
    updator : function ( dt )
    {
        if (this.dailyChanllengeTxt){
            this.dailyChanllengeTxt.setString(this.BonusTimeObj.updateTimeForDaliyChanllenge());

        }

    },
    addElementOnPageLayout:function(layout,index,LayoutTag){
        var layoutTag =LayoutTag;
        var xPos = 0;
        var idx = index;
        var taskHeading ="Complete your current Mission to unlock the next one!";
        for(var i=0;i<3;i++) {
            var dailyTask = null;
            var str = "res/DailyChallences/task_3.png";
            if (idx >= VSDailyChallenge.getInstance()._dailyChallengeTasks.length) {
                str = "res/DailyChallences/task_3.png";
            } else {
                dailyTask = VSDailyChallenge.getInstance()._dailyChallengeTasks[idx];
                if (dailyTask.isActive) {
                    str = "res/DailyChallences/task_2.png";
                }else if(dailyTask.collected){
                    str = "res/DailyChallences/task_1.png";
                }
            }
            var taskBaseImage = new cc.Sprite(str);
            taskBaseImage.setAnchorPoint(cc.p(0, 0.5));
            taskBaseImage.setPosition(cc.p(xPos, layout.getContentSize().height / 2));
            layout.addChild(taskBaseImage);
            taskBaseImage.setTag(this.btnTag);
            xPos += taskBaseImage.getContentSize().width + taskBaseImage.getContentSize().width / 24;
            if (i === 1) {
                xPos = xPos;
            }
            var yPos = taskBaseImage.getContentSize().height * 0.45;
            if (dailyTask) {
                taskHeading = dailyTask.collected ? "Mission Completed! Way To Go!" : dailyTask.isActive ? dailyTask.taskHeading : "Complete your current Mission to unlock the next one!";
                yPos = dailyTask.collected ? taskBaseImage.getContentSize().height * 0.4 : dailyTask.isActive ? taskBaseImage.getContentSize().height * 0.47 : taskBaseImage.getContentSize().height * 0.47;
                if (dailyTask.isActive) {
                    if (dailyTask.task_id === DC_TASK_4 || dailyTask.task_id === DC_TASK_5) {
                        yPos = taskBaseImage.getContentSize().height * 0.4;
                    }
                }

            } else {
                taskHeading = "Your Next Mission Is Coming Soon";
                yPos = taskBaseImage.getContentSize().height * 0.33;
            }
            var headingNameTxt = new cc.LabelTTF(taskHeading, "arial_black", 18, cc.size(taskBaseImage.getContentSize().width * 0.60, taskBaseImage.getContentSize().height * 0.75), cc.TEXT_ALIGNMENT_CENTER, cc.VERTICAL_TEXT_ALIGNMENT_TOP);
            headingNameTxt.setPosition(cc.p(taskBaseImage.getContentSize().width / 2, yPos));
            headingNameTxt.setTag(MISSION_HEADING_TAG);
            taskBaseImage.addChild(headingNameTxt);
            if (dailyTask) {

                if (dailyTask.isActive || dailyTask.collected) {
                    if (dailyTask.progress / dailyTask.initalWaitage < 1) {
                        this.setupTaskProgressUI(dailyTask,taskBaseImage);
                    }
                }
                if (!dailyTask.isActive && !dailyTask.collected) {
                    //this.setSpriteGreyScaled(taskBaseImage);
                }

            }
            var heightOffset = 0.25;
            if (dailyTask) {
                heightOffset = dailyTask.collected ? 0.2 : dailyTask.progress / dailyTask.initalWaitage >= 1 ? 0.18 : dailyTask.isActive ? 0.16 : 0.19;
            }
            if (dailyTask && !dailyTask.collected) {
                var currentMissionStusBtn = new ccui.Button(dailyTask.isActive?res.dc_gift_box : res.dc_lock_icon,dailyTask.isActive?res.dc_gift_box : res.dc_lock_icon);
                currentMissionStusBtn.setPosition(cc.p(taskBaseImage.getContentSize().width/2, taskBaseImage.getContentSize().height* heightOffset));
                taskBaseImage.addChild(currentMissionStusBtn);
                currentMissionStusBtn.setTag(this.btnTag);
                var giftBoxAnim =null;
                if(dailyTask.isActive){
                    giftBoxAnim =  gaf.Asset.create( "res/DailyChallences/gift_box_anim/gift_box_anim.gaf").createObject();
                    giftBoxAnim.playSequence(dailyTask.progress/dailyTask.initalWaitage >= 1 ? "fast": "normal", true);
                    giftBoxAnim.start();
                    giftBoxAnim.setAnimationFinishedPlayDelegate(null);
                    giftBoxAnim.setLooped(true);
                }else{
                    giftBoxAnim =  gaf.Asset.create("res/DailyChallences/lock_anim/lock_anim.gaf").createObjectAndRun(true);
                    giftBoxAnim.start();
                    giftBoxAnim.setLooped(true);
                    giftBoxAnim.setAnimationFinishedPlayDelegate(null);

                }
                giftBoxAnim.setTag(MISSION_LOCK_GIFT_TAG);
                giftBoxAnim.setPosition( cc.p(0 ,currentMissionStusBtn.getContentSize().height));
                currentMissionStusBtn.addChild(giftBoxAnim,1);
                if(dailyTask.isActive){
                    if(dailyTask.progress/dailyTask.initalWaitage >= 1){
                        currentMissionStusBtn.setEnabled(true);
                        currentMissionStusBtn.loadTextures(res.gift_box_disable,res.gift_box_disable);
                        this.giftBoxAnim = giftBoxAnim;
                        this.customaLabeButton = currentMissionStusBtn;
                        this.taskBaseImage = taskBaseImage;
                        this.layoutTag = layoutTag;
                        this.spWinDisplayWin = new cc.Sprite(res.dc_win_number_display);
                        this.spWinDisplayWin.setPosition(cc.p(taskBaseImage.getContentSize().width/2, taskBaseImage.getContentSize().height*0.35));
                        taskBaseImage.addChild(this.spWinDisplayWin,2);
                        var rewardBonusLbl = new CustomLabel( dailyTask.reward, "arial_black", 25, this.spWinDisplayWin.getContentSize()*0.70 );
                        rewardBonusLbl.setAnchorPoint(cc.p(0.5,0.5));
                        rewardBonusLbl.setColor( cc.color( 255, 255, 255 ) );
                        rewardBonusLbl.setPosition(cc.p(this.spWinDisplayWin.getContentSize().width/2, this.spWinDisplayWin.getContentSize().height*0.47));                rewardBonusLbl.setTag( 101 );
                        this.spWinDisplayWin.addChild(rewardBonusLbl,2);
                        this.spWinDisplayWin.setVisible(false);
                        currentMissionStusBtn.addTouchEventListener(  this.touchEvent, this );
                    }else{
                      //  currentMissionStusBtn.setEnabled(false);
                    }

                }else{
                    //currentMissionStusBtn.setEnabled(false);
                }
                this.btnTag++;
            } else {
                if (dailyTask) {
                    this.btnTag++;
                    var missionStatusImage = new cc.Sprite("res/DailyChallences/tick_mark.png");
                    missionStatusImage.setPosition(taskBaseImage.getContentSize().width / 2, taskBaseImage.getContentSize().height * heightOffset);
                    taskBaseImage.addChild(missionStatusImage);
                }
            }
            this.itemIdx++;
            idx++;
        }
    },
    touchEvent: function ( itm, type ) {
        if ( this.isBoxOpen) {
            return;
        }
        var tag = itm.getTag();
        switch ( type ) {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
            {
                this.isBoxOpen = true;
                if (this.customaLabeButton){
                    var delegate = AppDelegate.getInstance();
                    this.pageView.setEnabled(false);
                    this.isBoxOpen = true;
                    delegate.jukeBox(S_GIFT_BOX_OPEN);
                    this.giftBoxAnim.playSequence("open", false);
                    this.giftBoxAnim.start();
                    this.giftBoxAnim.setAnimationFinishedPlayDelegate(function (object) {
                        object.setAnimationFinishedPlayDelegate(null);
                    },this);
                    this.customaLabeButton.runAction(new cc.Sequence( new cc.DelayTime(0.4),cc.callFunc( function ( ){
                        this.customaLabeButton.setEnabled(false);
                        this.customaLabeButton.loadTextureDisabled(res.dc_tick_mark,ccui.Widget.LOCAL_TEXTURE);
                        this.giftBoxAnim.stop();
                        this.giftBoxAnim.setVisible(false);
                        var tag = this.customaLabeButton.getTag();
                        this.customaLabeButton.setPositionY(this.taskBaseImage.getContentSize().height*0.2);
                        this.openNextTaskAfterCompletedCurrentTask(tag,this.layoutTag);
                    },this)));

                    this.spWinDisplayWin.runAction( new cc.Sequence( new cc.DelayTime(0.7), new cc.callFunc( function(){
                        var index = this.customaLabeButton.getTag();
                        var dailyChallObj = VSDailyChallenge.getInstance();
                        var currentDailyTask = dailyChallObj._dailyChallengeTasks[index];
                        currentDailyTask.isActive = false;
                        currentDailyTask.collected = true;
                        var currIndx = index+1;
                        if(dailyChallObj._dailyChallengeTasks.length > currIndx) {
                            var dailyTask = dailyChallObj._dailyChallengeTasks[currIndx];
                            dailyTask.isActive = true;
                        }
                        dailyChallObj.updateDailyChanllengeRecord();
                        this.spWinDisplayWin.runAction(new cc.MoveTo(1.0,cc.p(this.spWinDisplayWin.getPositionX(),this.taskBaseImage.getContentSize().height*0.60)));
                        this.rotateRewardAmt(currentDailyTask.reward);
                        var headingNameTxt =  this.taskBaseImage.getChildByTag(MISSION_HEADING_TAG);
                        if(headingNameTxt){
                            headingNameTxt.setString("Mission Completed! Way To Go!");
                            headingNameTxt.setPositionY(this.taskBaseImage.getContentSize().height*0.4);
                        }
                        this.taskBaseImage.setTexture(res.dc_task1);

                    }, this ) ) );


                }
            }
                break;
        }

    },
    rotateRewardAmt:function(winningAmt){
        var rewardValue = winningAmt;
        var rewardBonusLbl =  this.spWinDisplayWin.getChildByTag(101);
        var  delegate = AppDelegate.getInstance();
        ServerData.getInstance().updateData("coins",userDataInfo.getInstance().totalChips+parseInt(winningAmt));
        this.spWinDisplayWin.setVisible(true);
        rewardBonusLbl.runAction( new cc.Sequence( new cc.callFunc( function(){
            var coinsRotationTime = parseInt(winningAmt/13);
            coinsRotationTime = coinsRotationTime == 0 ? 1 : coinsRotationTime;
            var displayedScore = parseInt(coinsRotationTime);
            var increaseAmount = parseInt(winningAmt) ;
            rewardBonusLbl.setString(displayedScore);
            delegate.jukeBox(S_COIN_MORE);
            //(callback, interval, repeat, delay, key)
            this.schedule( function(dt){
                displayedScore +=coinsRotationTime;
                if ( displayedScore >= increaseAmount) {
                    this.unschedule("rotateRewardAmt");
                    rewardBonusLbl.setString(DPUtils.getInstance().getNumString(increaseAmount));
                }else{
                    rewardBonusLbl.setString(DPUtils.getInstance().getNumString(displayedScore));
                }
            },0.08,"rotateRewardAmt");

        }, this ),new cc.DelayTime(1.5),new cc.callFunc( function(){
            this.spWinDisplayWin.setVisible(false);
            var delegate = AppDelegate.getInstance();
            delegate.assignRewards(parseInt(rewardValue),false);
             if (this.movePageView){
                 this.menuCB(this.rightArrow);
             }
            var missionStatusImage = new cc.Sprite("res/DailyChallences/tick_mark.png");
            missionStatusImage.setPosition(this.taskBaseImage.getContentSize().width / 2, this.taskBaseImage.getContentSize().height * 0.2);
            this.taskBaseImage.addChild(missionStatusImage);
            this.movePageView = false;
            this.isBoxOpen = false;
        },this)) );
    },

    openNextTaskAfterCompletedCurrentTask:function(index,layoutTag){
        var dailyChallObj = VSDailyChallenge.getInstance();
        var currentDailyTask = dailyChallObj._dailyChallengeTasks[index];
        currentDailyTask.isActive = false;
        currentDailyTask.collected = true;
        var currIndx = index+1;
        if(dailyChallObj._dailyChallengeTasks.length > currIndx) {
            var dailyTask = dailyChallObj._dailyChallengeTasks[currIndx];
            dailyTask.isActive = true;
            if (currIndx>=3) {
                layoutTag=1;
            }
            this.createHeadingComponents();
            var layout = this.pageView.getItem(layoutTag);
            var taskBaseImage = layout.getChildByTag(currIndx);
            //taskBaseImage.setNormalImage(res.dc_task2);
            this.runAction(new cc.Sequence( new cc.DelayTime(0.3),cc.callFunc( function ( ){
                this.setupTaskProgressUI(dailyTask,taskBaseImage);
                var headingNameTxt =  taskBaseImage.getChildByTag(MISSION_HEADING_TAG);
                if(headingNameTxt){
                    headingNameTxt.setString(dailyTask.taskHeading);
                    if ( dailyTask.task_id==DC_TASK_4 || dailyTask.task_id==DC_TASK_5) {
                        headingNameTxt.setPositionY(taskBaseImage.getContentSize().height*0.4);
                    }
                }
                var currentLockBtn =  taskBaseImage.getChildByTag(currIndx);
                var currentMissionStusBtn = new ccui.Button(res.dc_gift_box ,res.dc_gift_box);
                currentMissionStusBtn.setPosition(currentLockBtn.getPosition());
                currentMissionStusBtn.setPositionY(taskBaseImage.getContentSize().height*0.16);
                taskBaseImage.addChild(currentMissionStusBtn);
                currentMissionStusBtn.setTag(currIndx);
                //currentMissionStusBtn.setEnabled(false);
                var giftBoxAnim =  gaf.Asset.create( res.dc_gift_box_anim_gaf ).createObject();
                giftBoxAnim.playSequence("normal",true);
                giftBoxAnim.start();
                giftBoxAnim.setAnimationFinishedPlayDelegate(null);
                giftBoxAnim.setTag(2);
                if(currentLockBtn){
                    currentLockBtn.setVisible(false);
                    currentLockBtn.removeFromParent();
                }
                taskBaseImage.setTexture(res.dc_task2);
            },this)));
            if(dailyTask.isActive &&  index+1 >= 3){
                this.movePageView = true;
            }
            var delegate = AppDelegate.getInstance();
            delegate.jukeBox(S_MISSION_OPEN);
        }
        dailyChallObj.updateDailyChanllengeRecord();
        this.pageView.setEnabled(true);
    },
    setupTaskProgressUI:function(dailyTask,taskBaseImage){
        var yPos = taskBaseImage.getContentSize().height*0.34;

        if (dailyTask.task_id== DC_TASK_4 || dailyTask.task_id== DC_TASK_5) {

            yPos =taskBaseImage.getContentSize().height*0.36;
        }
        var fuelBarBorder =new cc.Sprite(res.dc_progress_bg);
        fuelBarBorder.setPosition(cc.p(taskBaseImage.getContentSize().width/2,yPos));
        taskBaseImage.addChild(fuelBarBorder);
        var fuelBar = new cc.ProgressTimer(new cc.Sprite(res.dc_progress_bar));
        fuelBar.setType(cc.ProgressTimer.TYPE_BAR);
        fuelBar.setAnchorPoint(cc.p(0, 0));
        fuelBar.setBarChangeRate(cc.p(1, 0)); // To make width 100% always
        fuelBar.setMidpoint(cc.p(0.0, 0.1));
        fuelBarBorder.addChild(fuelBar, 50); // Add it inside the border sprite
        if(dailyTask.progress != 0){
            var percent= dailyTask.progress*100/dailyTask.initalWaitage;
            fuelBar.runAction(new cc.ProgressTo(0,percent));
        }
        var strData =    DPUtils.getInstance().abbreviateNumber(dailyTask.progress) +"/"+DPUtils.getInstance().abbreviateNumber(dailyTask.initalWaitage);

        var totalLabel = new CustomLabel( strData, "HELVETICALTSTD-BOLD", 18, new cc.size( 520, 40 ) );
        totalLabel.setPosition( fuelBarBorder.getContentSize().width * 0.5, fuelBarBorder.getContentSize().height*0.45 );
        fuelBarBorder.addChild( totalLabel,50 );

    },
    createHeadingComponents:function() {
        var isHasNodes = false;
        if (this.nodeVec && this.nodeVec.length) {
            this.nodeVec = [];
            isHasNodes = true;
        }
        this.totalIndexOfScroll = VSDailyChallenge.getInstance()._dailyChallengeTasks.length / 3;
        var i = 1;
        var j = 1;
        this.numberoftaskOpen = VSDailyChallenge.getInstance().getNumberOfTaskCompletionDailyChallenge();
        for (i = 1; i <= this.totalIndexOfScroll; i++) {
            var nodePage = new cc.Node();
            nodePage.setAnchorPoint(cc.p(0, 1));
            nodePage.setContentSize(this.dc_blank_mission.getContentSize());
            this.dc_blank_mission.addChild(nodePage);
            //nodePage.setPosition(this.dc_blank_mission.getPosition());
            this.nodeVec.push(nodePage);
            var xPos = 8;
            var xGap = 15;
            var lblHeadingPosX = 0;
            var n = j + 3;
            for (; j < n; j++) {
                var spbg = null;
                var spbgRunningTaskOrCompleted = null;
                var spCircle = null;
                if (j == 1 || j == 4 || j == 7) {
                    spbg = new cc.Sprite(res.dc_header_01);
                    spbg.setAnchorPoint(cc.p(0, 1));
                    spbg.setTag(j);
                    spbg.setPosition(cc.p(xPos, nodePage.getContentSize().height * 0.5));
                    spCircle = new cc.Sprite(res.dc_dc_wheel);
                    spCircle.setAnchorPoint(cc.p(0, 1));
                    spCircle.setPosition(cc.p(cc.rectGetMaxX(spbg.getBoundingBox()) - spCircle.getContentSize().width * 0.75, nodePage.getContentSize().height * 0.4));
                    xGap = 15;
                    if (j <= this.numberoftaskOpen) {
                        spbgRunningTaskOrCompleted = new cc.Sprite(res.dc_header_11);
                        spbgRunningTaskOrCompleted.setAnchorPoint(cc.p(0, 1));
                        spbgRunningTaskOrCompleted.setTag(j);
                        spbgRunningTaskOrCompleted.setPosition(spbg.getPosition());
                    }
                } else if (j == 2 || j == 5 || j == 8) {

                    spbg = new cc.Sprite(res.dc_header_02);
                    spbg.setAnchorPoint(cc.p(0, 1));
                    spbg.setTag(j);
                    spbg.setPosition(cc.p(xPos, nodePage.getContentSize().height * 0.5));
                    spCircle = new cc.Sprite(res.dc_dc_wheel);
                    spCircle.setAnchorPoint(cc.p(0, 1));

                    spCircle.setPosition(cc.p(cc.rectGetMaxX(spbg.getBoundingBox()) - spCircle.getContentSize().width * 0.7, nodePage.getContentSize().height * 0.4));
                    lblHeadingPosX = 10;
                    xGap = 15;
                    if (j <= this.numberoftaskOpen) {
                        spbgRunningTaskOrCompleted = new cc.Sprite(res.dc_header_12);
                        spbgRunningTaskOrCompleted.setAnchorPoint(cc.p(0, 1));
                        spbgRunningTaskOrCompleted.setTag(j);
                        spbgRunningTaskOrCompleted.setPosition(spbg.getPosition());
                    }
                } else {
                    var str = "res/DailyChallences/03.png";
                    spbg = new cc.Sprite(res.dc_header_03);
                    spbg.setAnchorPoint(cc.p(0, 1));
                    spbg.setTag(j);
                    spbg.setPosition(cc.p(xPos, nodePage.getContentSize().height * 0.5));
                    lblHeadingPosX = 20;
                    if (j <= this.numberoftaskOpen) {
                        spbgRunningTaskOrCompleted = new cc.Sprite(res.dc_header_13);
                        spbgRunningTaskOrCompleted.setAnchorPoint(cc.p(0, 1));
                        spbgRunningTaskOrCompleted.setTag(j);
                        spbgRunningTaskOrCompleted.setPosition(spbg.getPosition());
                    }
                }
                var strHeading = "MISSION" + j;
                var headerxPos = 0.98;
                if (j==1 || j==2 || j==4 || j==5){
                    headerxPos = 0.92;
                }
                var pos = spbgRunningTaskOrCompleted ? spbgRunningTaskOrCompleted.getContentSize() : spbg.getContentSize();
                var headingNameTxt = new cc.LabelTTF(strHeading, "arial_black", 30, cc.size(pos.width * headerxPos, pos.height * 1.5), cc.TEXT_ALIGNMENT_CENTER, cc.VERTICAL_TEXT_ALIGNMENT_TOP);
                headingNameTxt.setPosition(cc.p(lblHeadingPosX, pos.height * 0.68));
                headingNameTxt.setTag(j);
                headingNameTxt.setAnchorPoint(cc.p(0, 1));
                headingNameTxt.setColor( cc.color( 251, 160, 255 ) );
                headingNameTxt.enableShadow(cc.color( 24,1,25,255 ),cc.size(0,-3),3);
                if (spbgRunningTaskOrCompleted) {
                    spbgRunningTaskOrCompleted.addChild(headingNameTxt);
                } else {
                    spbg.addChild(headingNameTxt);
                }
                nodePage.addChild(spbg);
                if (spbgRunningTaskOrCompleted) {
                    nodePage.addChild(spbgRunningTaskOrCompleted, 1);
                }
                if (spCircle) {
                    nodePage.addChild(spCircle, 10);
                }
                xPos += spbg.getContentSize().width - xGap * 2;

            }
        }
        for (var i = 0; i < this.nodeVec.length; i++) {
            if (i != 0) {
                this.nodeVec[i].setVisible(false);

            }

        }

            if (isHasNodes) {
                this.enableDisableArrowButton();
            }

    },
    enableDisableArrowButton:function() {
        if (!this.pageView) {
            return;
        }
        var sizeOfPages = this.pageView.getItems().length-1;
        var index = this.pageView.getCurrentPageIndex();
        for (var i=0; i<this.nodeVec.length; i++) {
            this.nodeVec[i].setVisible(false);
        }
        if(index<0)index=0;
        this.nodeVec[index].setVisible(true);
        if (index == 0) {
            this.leftArrow.setEnabled(false);
            this.rightArrow.setEnabled(true);
        } else if (index == sizeOfPages) {
            this.leftArrow.setEnabled(true);
            this.rightArrow.setEnabled(false);
        } else {
            this.leftArrow.setEnabled(true);
            this.rightArrow.setEnabled(true);
        }
    },
    initalStageOfPageLayoutBar:function(blankSize,blankPos,blankimage){
        this.pageView = new ccui.PageView();
        this.pageView.setDirection(ccui.ScrollView.DIR_HORIZONTAL);
        this.pageView.setContentSize(blankSize);
        this.pageView.addEventListener(function(ref, eventType) {
            if (eventType == ccui.ScrollView.EVENT_AUTOSCROLL_ENDED) {
                ref.getParent().getParent().getParent().enableDisableArrowButton();
            }
        }, this);
        this.pageView.removeAllPages();
        this.pageView.setCurrentPageIndex(0);
        this.pageView.setAnchorPoint(cc.p(0.5,0.5));
        this.pageView.setPosition(cc.p(blankimage.getContentSize().width*0.5,blankimage.getContentSize().height*0.5));
        blankimage.addChild(this.pageView);
        this.pageView.setSwallowTouches(true);

    },

    menuCB:function ( pSender ) {

        var tag = pSender.getTag();
        switch (tag) {

            case CLOSE_BTN_TAG: {
                if (this.isBoxOpen){
                    return;
                }
                this.unschedule( this.updator );
                DPUtils.getInstance().easyBackInAnimToPopup(this.bgDCSprite, 0.3);
                this.runAction(new cc.Sequence(new cc.DelayTime(0.3), cc.callFunc(function () {
                    var delegate = AppDelegate.getInstance();
                    delegate.showingScreen = false;
                    this.removeFromParent(true);
                }, this)));
            }

                break;
            case RIGHT_ARROW:
                {

                    var sizeOfPages = this.pageView.getItems().length-1;
                    var index = this.pageView.getCurrentPageIndex();

                    if (index == sizeOfPages) {
                        this.leftArrow.setEnabled(true);
                        this.rightArrow.setEnabled(false);
                        return;
                    } else {
                        this.pageView.scrollToItem(++index);
                        if (index == sizeOfPages) {
                            this.leftArrow.setEnabled(true);
                            this.rightArrow.setEnabled(false);
                        } else {
                            this.leftArrow.setEnabled(true);
                            this.rightArrow.setEnabled(true);
                        }
                    }

                }

                break;

            case LEFT_ARROW:
            {
                var index = this.pageView.getCurrentPageIndex();
                if (index == 0) {
                    this.leftArrow.setEnabled(false);
                    this.rightArrow.setEnabled(true);
                    return;
                } else {
                    this.pageView.scrollToItem(--index);
                    if (index == 0) {
                        this.leftArrow.setEnabled(false);
                        this.rightArrow.setEnabled(true);
                    } else {
                        this.leftArrow.setEnabled(true);
                        this.rightArrow.setEnabled(true);
                    }
                }

            }

                break;

        }
    }

});