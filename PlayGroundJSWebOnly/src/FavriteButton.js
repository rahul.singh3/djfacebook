FavriteButton = ccui.Button.extend({

    iCode : 0,
    enable : 0,
    index:0,
    ctor:function ( icde, indx, enable ) {
        this._super();
        //var pRet = new CustomButton();
        //if ( pRet && pRet.init() ){
        this.initialized( icde, indx, enable );
        return true;
        //}


    },
    initialized:function (iCodee, indexe, Enable) {
        this.iCode = iCodee;
        this.index = indexe;
        this.enable = Enable;
    },
     setIcons:function () {
         if ( this.enable) {
             this.loadTextures(res.favorite_enable, res.favorite_enable);
         }else{
             this.loadTextures(res.favorite_disable, res.favorite_disable);
         }
         this.addTouchEventListener( this.touchEvent, this );

     },
    touchEvent : function ( pSender, type ) {
        switch (type) {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
            {
                var faviconsVec =[];
                if (this.enable) {
                    if(userDataInfo.getInstance().favourite_machines.length>0)
                    {
                     faviconsVec = userDataInfo.getInstance().favourite_machines;

                        if (faviconsVec && faviconsVec.length>0){
                            if (faviconsVec.includes(this.iCode)){
                                for( var i = 0; i < faviconsVec.length; i++){ if ( faviconsVec[i] === this.iCode) { faviconsVec.splice(i, 1); i--; }}
                                userDataInfo.getInstance().favourite_machines = faviconsVec;
                                ServerData.getInstance().updatefavouriteMachinesOnServer(this.iCode,"REMOVE");
                            }
                        }
                    }
                    this.loadTextures(res.favorite_disable, res.favorite_disable);
                    this.enable =false;
                }else{

                    if(userDataInfo.getInstance().favourite_machines.length>0) {
                        faviconsVec = userDataInfo.getInstance().favourite_machines;
                        if (faviconsVec.indexOf( this.iCode ) > -1){

                        }else{
                            faviconsVec.push(this.iCode);
                            userDataInfo.getInstance().favourite_machines = faviconsVec;
                            ServerData.getInstance().updatefavouriteMachinesOnServer(this.iCode,"ADD");
                        }


                    }else{
                        userDataInfo.getInstance().favourite_machines.push(this.iCode);
                        ServerData.getInstance().updatefavouriteMachinesOnServer(this.iCode,"ADD");

                    }



                    this.loadTextures(res.favorite_enable, res.favorite_enable);
                    this.enable =true;
                }
            }
                break;
            default:
                break;

        }
    }





});