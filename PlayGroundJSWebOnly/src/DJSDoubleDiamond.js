/**
 * Created by RNF-Mac11 on 1/23/17.
 */



var reelsRandomizationArrayDJSDoubleDiamond =
    [
        [// 85% payout
            20, 2, 5, 5, 5, 20,
            10, 25, 10,10, 25, 5,
            15, 3, 25, 20, 25, 20,
            10, 25, 3,25, 10, 10,
            5, 5, 10, 2, 5, 20,
            5, 20, 10,25, 10, 5,
        ],
        [//92 %
            20, 3, 5, 5, 5, 20,
            10, 30, 10,10, 25, 5,
            15, 3, 25, 20, 25, 20,
            10, 25, 3,20, 10, 10,
            5, 5, 10, 2, 5, 20,
            5, 20, 10,25, 10, 5,
        ],
        [// 95-97% payout
            /*20, 3, 5, 5, 5, 20,
            10, 25, 10,10, 25, 3,
            15, 3, 25, 20, 25, 20,
            10, 25, 3,20, 10, 10,
            5, 3, 10, 3, 5, 20,
            5, 20, 10,25, 10, 5,*/
            20, 3, 5, 5, 5, 20,
            10, 30, 10,10, 25, 5,
            15, 3, 25, 20, 25, 20,
            10, 25, 3,20, 10, 10,
            5, 5, 10, 2, 5, 20,
            5, 20, 10,25, 10, 5,
        ],
        [//140% payout
            20, 4, 5, 5, 5, 20,
            10, 30, 10,15, 25, 3,
            15, 5, 25, 20, 25, 20,
            10, 25, 3,20, 10, 10,
            5, 3, 10, 5, 5, 20,
            5, 20, 10,25, 10, 5,
        ],
        [//300% payout
            20, 7, 5, 5, 5, 20,
            10, 30, 10,30, 10, 3,
            15, 7, 10, 30, 10, 20,
            10, 25, 3,20, 10, 10,
            5, 3, 10, 7, 5, 20,
            5, 20, 10,25, 10, 5,
        ],
    ];

var DJSDoubleDiamond = GameScene.extend({
    reelsArrayDJSDoubleDiamond:[],
    ctor:function ( level_index ) {
        this.reelsArrayDJSDoubleDiamond = [
            /*1*/ELEMENT_EMPTY,/*2*/ ELEMENT_3X,/*3*/ ELEMENT_EMPTY,/*4*/ ELEMENT_CHERRY,/*5*/ ELEMENT_EMPTY,
            /*6*/ ELEMENT_BAR_2,/*7*/ELEMENT_EMPTY,/*8*/ ELEMENT_BAR_1,/*9*/ ELEMENT_EMPTY,/*10*/ ELEMENT_7_RED,
            /*11*/ELEMENT_EMPTY,/*12*/ELEMENT_CHERRY,/*13*/ELEMENT_EMPTY,/*14*/ ELEMENT_3X,/*15*/ELEMENT_EMPTY,
            /*16*/ELEMENT_7_RED,/*17*/ ELEMENT_EMPTY,/*18*/ ELEMENT_BAR_2,/*19*/ELEMENT_EMPTY,/*20*/ ELEMENT_BAR_1,
            /*21*/ ELEMENT_EMPTY,/*22*/ ELEMENT_BAR_3,/*23*/ELEMENT_EMPTY,/*24*/ELEMENT_7_RED,/*25*/ELEMENT_EMPTY,
            /*26*/ ELEMENT_CHERRY,/*27*/ ELEMENT_EMPTY,/*28*/ ELEMENT_3X,/*29*/ELEMENT_EMPTY,/*30*/ ELEMENT_BAR_2,
            /*31*/ELEMENT_EMPTY,/*32*/ELEMENT_BAR_3,/*33*/ ELEMENT_EMPTY,/*34*/ ELEMENT_BAR_1,/*35*/ ELEMENT_EMPTY,
            /*36*/ ELEMENT_CHERRY,
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSDoubleDiamond;
        this.physicalArrayCheck= this.reelsArrayDJSDoubleDiamond;
        this._super( level_index );

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;


    },

    calculatePayment : function( doAnimate )
    {
        var payMentArray = [
        /*0*/    100,// 3x 3x 3x

        /*1*/    75, // 7 7 7 Red

        /*2*/    40,  // Triple Bar

        /*3*/    20,  // double Bar

        /*4*/    10,  // Triple Cherry

        /*5*/    10,   // single Bar

        /*6*/    3,   // any bar

        /*7*/    3,   // Single Diamond 3x

        /*8*/    2,   // Double Cherry

        /*9*/    1,   // Single Cherry
    ];

        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        for (var i = 0; i < this.slots[0].resultReel.length; i++)
        {
            tmpVector = this.slots[0].resultReel[i];
            tmpVector2.push(tmpVector[1]);
        }

        this.lineVector.push(tmpVector2);
        var winScenario = 7;
        var lineLength = 3;
        var pay = 0;
        var tmpPay = 1;
        var count3X = 0;
        var countWild = 0;
        var countGeneral = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var i, j, k;
        var e = null;
        var pivotElm = null;
        var pivotElm2 = null;
        var tmpVec;
        
        for (i = 0; i < this.totalLines; i++)
        {
            tmpVec = this.lineVector[i];
            for (k = 0; k < winScenario; k++)
            {
                var breakLoop = false;
                countGeneral = 0;
                switch (k)
                {
                    case 0://for all combination with wild
                        for (j = 0; j < lineLength; j++)
                        {
                            if ( this.reelsArrayDJSDoubleDiamond[this.resultArray[i][j]] != -1 && this.reelsArrayDJSDoubleDiamond[this.resultArray[i][j]] != ELEMENT_EMPTY )
                            {
                                switch (this.reelsArrayDJSDoubleDiamond[this.resultArray[i][j]])
                                {
                                    case ELEMENT_3X:
                                        count3X++;
                                        tmpPay*=payMentArray[7];
                                        countWild++;
                                        if ( doAnimate ) {
                                            e = tmpVec[j];
                                        }
                                        if (e)
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        /*#ifdef BIASING_BUILD
                                                                            _any3x++;
                                        #endif*/
                                        break;

                                    default:
                                        switch (pivotElement)
                                        {
                                            case -1:
                                                pivotElement = this.reelsArrayDJSDoubleDiamond[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate ) {
                                                    e = tmpVec[j];
                                                }
                                                if (e)
                                                {
                                                    pivotElm = e;
                                                }
                                                break;

                                            default:
                                                pivotElement2 = this.reelsArrayDJSDoubleDiamond[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate ) {
                                                    e = tmpVec[j];
                                                }
                                                if (e)
                                                {
                                                    pivotElm2 = e;
                                                }
                                                break;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }

                        if (j == 3)
                        {
                            if (count3X == 3)
                            {
                                pay = payMentArray[0];
                                breakLoop = true;
                            }
                            else if(countWild >= 2)
                            {
                                breakLoop = true;

                                //tmpPay = payMentArray[4];

                                switch (pivotElement)
                                {
                                    case ELEMENT_7_RED:
                                        pay = tmpPay * payMentArray[1];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }

                                        break;

                                    case ELEMENT_BAR_3:
                                        pay = tmpPay * payMentArray[2];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_2:
                                        pay = tmpPay * payMentArray[3];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_1:
                                        pay = tmpPay * payMentArray[5];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_CHERRY:
                                        pay = tmpPay * payMentArray[4];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        break;
                                }
                            }
                            else if(countWild == 1)
                            {
                                breakLoop = true;

                                if (pivotElement == pivotElement2)
                                {
                                    switch (pivotElement)
                                    {
                                        case ELEMENT_7_RED:
                                            pay = tmpPay * payMentArray[1];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_3:
                                            pay = tmpPay * payMentArray[2];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_2:
                                            pay = tmpPay * payMentArray[3];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_1:
                                            pay = tmpPay * payMentArray[5];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_CHERRY:
                                            pay = tmpPay * payMentArray[4];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        default:
                                            //pay = tmpPay;
                                            break;
                                    }
                                }
                                else
                                {
                                    if((pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_BAR_3) &&
                                        (pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_BAR_3))
                                    {
                                        pay = tmpPay * payMentArray[6];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if( pivotElement == ELEMENT_CHERRY )
                                    {
                                        pay = tmpPay * payMentArray[8];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if( pivotElement2 == ELEMENT_CHERRY )
                                    {
                                        pay = tmpPay * payMentArray[8];
                                        if ( doAnimate ) {
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else
                                    {
                                        //pay = tmpPay;
                                    }
                                }
                            }
                        }
                        break;

                    case 1: // 7 7 7 Red
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSDoubleDiamond[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[1];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }

                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 2: // 3Bar 3Bar 3Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSDoubleDiamond[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[2];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 3: // 2Bar 2Bar 2Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSDoubleDiamond[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[3];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 4: // Bar Bar Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSDoubleDiamond[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[5];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 5: // any Bar combo
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSDoubleDiamond[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[6];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++)
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 6: // Cherry checking
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSDoubleDiamond[this.resultArray[i][j]])
                            {
                                case ELEMENT_CHERRY:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[4];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++)
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        else if (countGeneral == 2)
                        {
                            pay = payMentArray[8];
                            breakLoop = true;
                            for (var l = 0; l < lineLength; l++)
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if ( e && e.eCode == ELEMENT_CHERRY )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        else if (countGeneral == 1)
                        {
                            pay = payMentArray[9];
                            breakLoop = true;
                            for (var l = 0; l < lineLength; l++)
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if ( e && e.eCode == ELEMENT_CHERRY )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

                if (breakLoop)
                {
                    break;
                }
            }
        }
    

        if( doAnimate )
        {
            this.currentWin = this.currentBet * pay;
            if ( this.currentWin > 0 )
            {
                //addWin = currentWin / 10;
                this.setCurrentWinAnimation();
            }

            this.checkAndSetAPopUp();
        }
        else
        {
            this.currentWinForcast = this.currentBet * pay;
        }
    },
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSDoubleDiamond[i]=this.physicalReelElements[idx];
                i++;
            }
        }
    },
    changeTotalProbabilities : function()
    {
        this.totalProbalities = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            //this.extractArrayFromSring();
            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[this.easyModeCode][i];
            }
        }
        else
        {
            for ( var i = 0; i < 36; i++ )
            {
                this.probabilityArray[this.easyModeCode][i] = reelsRandomizationArrayDJSDoubleDiamond[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSDoubleDiamond[this.easyModeCode][i];
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
        var tmp = this.slots[0].resultReel[laneIndex];

        var e = tmp[1];

        switch ( e.eCode )
        {
            case ELEMENT_7_RED:
                if ( laneIndex != 0 )
                {
                    break;
                }
            case ELEMENT_2X:
            case ELEMENT_3X:
            case ELEMENT_4X:
            case ELEMENT_5X:
                e.runAction( new cc.Repeat( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ), 2 ) );
                this.delegate.jukeBox( S_FLASH );
                break;

            default:
                break;
        }

    },

    generateResult : function( scrIndx )
    {
        for ( var i = 0; i < this.slots[scrIndx].displayedReels.length; i++ )
        {
            //generate:
            var num = parseInt( this.prevReelIndex[i] );

            var tmpnum = 0;

            while ( num == parseInt( this.prevReelIndex[i] ) )
            {
                num = Math.floor( Math.random() * this.totalProbalities );

            }
            this.prevReelIndex[i] = num;
            this.resultArray[0][i] = num;

            for (var j = 0; j < 36; j++)
            {
                tmpnum+=this.probabilityArray[this.easyModeCode][j];

                if (tmpnum >= this.resultArray[0][i])
                {
                    this.resultArray[0][i] = j;
                    break;
                }
            }
            //console.log( " rand =  " + num + " index = " + this.resultArray[0][i] );
        }

        /*cc.log( "this.resultArray[0][0] = " + this.resultArray[0][0] );
        cc.log( "this.resultArray[0][1] = " + this.resultArray[0][1] );
        cc.log( "this.resultArray[0][2] = " + this.resultArray[0][2] );*/

        /*
         int highPay[][3] = {
         {15, 21, 15}, //2x 5x 2x
         {15, 19, 15}, //2x 4x 2x
         {15, 17, 15}, //2x 3x 2x
         {7, 7, 7},    //Triple 7 Red
         {5, 5, 5},    //Triple 7 Yellow
         {29, 29, 29}, //Triple 7 Green
         {1, 1, 1},    //Triple 7 Bar
         {31, 31, 31}, //Triple 3Bar
         {27, 27, 27}, //Triple 2Bar
         {23, 23, 23}, //Triple Bar
         };*/
        /*/HACK
        this.resultArray[0][0] = 21;
        this.resultArray[0][1] = 17;
        this.resultArray[0][2] = 17;
        //*///HACK


        this.calculatePayment( false );
        this.checkForFalseWin();

    },
    rollUpdator : function(dt)
    {
        var i;
        for (i = 0; i < this.totalSlots; i++)
        {
            this.slots[i].updateSlot(dt);
        }

        for (i = 0; i < this.totalSlots; i++)
        {
            if ( this.slots[i].isSlotRunning() )
            {
                break;
            }
        }

        if (i >= this.totalSlots)
        {
            this.isRolling = false;
            this.unschedule( this.rollUpdator );
            this.calculatePayment( true );

            if (this.isAutoSpinning)
            {
                if (this.currentWin > 0)
                {
                    this.scheduleOnce( this.autoSpinCB, 1.5);
                }
                else
                {
                    this.scheduleOnce( this.autoSpinCB, 1.0);
                }
            }
        }
    },

    setFalseWin : function()
    {
        var highWinIndices = [ 3, 5, 7 ];
        var missIndices = [ 3, 5, 7, 15, 17, 19, 21 ];

        var missedIndx = Math.floor( Math.random() * 3 );

        if( missedIndx == 2 )
        {
            if( Math.floor( Math.random() * 10 ) == 3 )
            {
            }
            else
            {
                missedIndx = Math.floor( Math.random() * 2 );
            }
        }

        var sameIndex = Math.floor( Math.random( ) * highWinIndices.length );

        for ( var i = 0; i < 3; i++ )
        {
            if ( i == missedIndx )
            {
                this.resultArray[0][i] = this.getRandomIndex( missIndices[ Math.floor( Math.random( ) * missIndices.length ) ], true );
            }
            else
            {
                this.resultArray[0][i] = this.getRandomIndex( highWinIndices[ sameIndex ], false );
            }
        }
    },

    setResult : function( indx, lane, sSlot )
    {
        //cc.log( "indx = " + indx + ", lane = " + lane + ", sSlot = " + sSlot );
        var gap = 387.0;

        var j = 0;
        var animOriginY;

        var r = this.slots[0].physicalReels[lane];
        var arrSize = r.elementsVec.length;
        var spr;

        var spriteVec = [];

        animOriginY = gap * r.direction;
        spr = r.elementsVec[indx];
        if ( spr.eCode == ELEMENT_EMPTY )
        {
            gap = 285 * spr.getScale();
        }

        if ( indx == 0 )
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push(spr);

            spr.setPositionY( animOriginY );

            for (var i = 0; i < 2; i++)
            {
                spr = r.elementsVec[i];
                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (i + 1) * r.direction);
            }
        }
        else if (indx == arrSize - 1)
        {
            spr = r.elementsVec[0];
            spriteVec.push(spr);

            spr.setPositionY(animOriginY);
            for (var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i--)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction );
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push(spr);
            spr.setPositionY(animOriginY);

            for (var i = indx; i < indx + 2; i++)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }

        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            gap = -773.5;

            var eee;
            if (i < 2)
            {
                eee = spriteVec[i + 1];
            }
            if (i == 0)
            {
                var ee = spriteVec[i + 1];
                if ( Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) ) != 387)
                {
                    gap = -Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) * 2 * ee.getScale() );
                }
            }

            gap*=r.direction;
            var ease;

            if ( this.slots[sSlot].isForcedStop )
            {
                ease =  new cc.MoveTo( 0.15, cc.p( 0, e.getPositionY() + gap) ).easing( cc.easeBackOut() );
            }
            else
            {
                ease = new cc.MoveTo( 0.3, cc.p( 0, e.getPositionY() + gap ) ).easing( cc.easeBackOut() );
            }

            ease.setTag( MOVING_TAG );
            e.runAction(ease);
        }

        return spriteVec;
    },

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSDoubleDiamond;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    spin : function()
    {

        this.unschedule( this.updateWinCoin );

        this.deductBet( this.currentBet );
        if (this.displayedScore != this.totalScore)
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;
        this.currentWin = 0;
        this.currentWinForcast = 0;
        this.startRolling();

        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    }

});


/*var DJSQ = cc.Scene.extend( {
 onEnter:function () {
 this._super();
 var layer = new DJSDoubleDiamond( lvl );
 this.addChild(layer);
 }
 });*/