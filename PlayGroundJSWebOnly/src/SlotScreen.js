/**
 * Created by RNF-Mac11 on 1/23/17.
 */


SlotScreen = cc.Node.extend({

    ctor:function ( pArr, level_index, scrCode ) {
        this._super();
        this.initScreen( pArr, level_index, scrCode );
        return true;
    },
    vSize : cc.size( 0, 0 ),

    reelDimentions : cc.p( 0, 0 ),

    totalReels : 0,

    mCode : 0,

    isForcedStop : false,

    resultReel : null,

    resultReelAncient : null,//for storing the resulr reel before the freespins in DJS_FREE_SPIN

    displayedReels : null,

    physicalReels : null,

    blurReels : null,

    blurReelsWild : null,

    wildReel : null,

    reelMids : null,

    dNode : null,

    wildSpriteVec : null,

    initScreen : function ( pArr, level_index, scrCode ) {
        var posX = 0, posY = 0;

        this.wildSpriteVec = new Array();

        this.mCode = level_index;

        this.totalReels = 3;

        if (this.mCode == DJS_RNF_STAR)
        {
            this.totalReels = 4;
        }

        this.isForcedStop = false;

        this.vSize = cc.winSize;

        this.physicalReels = new Array();

        this.blurReels = new Array();

        this.resultReel = new Array();

        this.resultReelAncient = new Array();

        this.displayedReels = new Array();

        this.reelMids = new Array();

        this.wildReel = new Array();

        this.blurReelsWild = new Array();

        for (var i = 0; i < this.totalReels; i++)
        {
            var stringBG;
            switch ( this.mCode )
            {
                case DJS_FREEZE:
                    switch ( i )
                    {
                        default:
                            stringBG = "reel_bg_corner.png";
                            break;

                        case 1:
                            stringBG = "reel_bg.png";
                            break;
                    }
                    break;

                default:
                    if (i == 3)
                    {
                        stringBG = "reel1_bg.png";
                    }
                    else
                    {
                        stringBG = "reel_bg.png";
                    }
                    break;
            }
            var reelBG = null;
            /*  if (this.mCode===DJS_XTREME_MACHINE){
                  reelBG = new cc.Sprite( res.EXtreme_reel_bg );
              }else{
                  reelBG = new cc.Sprite( "#" + stringBG );
              }

             */
            reelBG = new cc.Sprite( "#" + stringBG );

            if( this.mCode !== DJS_X_MULTIPLIERS )
            {
                DJSResizer.getInstance().setFinalScaling( reelBG );
            }

            //reelWidth = reelBG.getScaleX() * reelBG.getContentSize().width;
            this.reelDimentions = cc.p( reelBG.getScaleX() * reelBG.getContentSize().width, reelBG.getScaleY() * reelBG.getContentSize().height );

            this.setContentSize( cc.size( this.getContentSize().width + this.reelDimentions.x, this.reelDimentions.y ) );

            switch (this.mCode)
            {
                case DJS_X_MULTIPLIERS:
                    switch (scrCode)
                    {
                        case 0:
                            posX = this.vSize.width / 2 - reelBG.getContentSize().width * 0.6 + (i - 2) * reelBG.getContentSize().width;

                            posY = this.vSize.height - reelBG.getContentSize().height * 0.9;
                            break;

                        case 2:
                            posX = this.vSize.width / 2 - reelBG.getContentSize().width * 0.6 + (i - 2) * reelBG.getContentSize().width;

                            posY = this.vSize.height / 2 - reelBG.getContentSize().height * 0.35;
                            break;

                        case 1:
                            posX = this.vSize.width - reelBG.getContentSize().width * 0.9 + (i - 2) * reelBG.getContentSize().width;

                            posY = this.vSize.height - reelBG.getContentSize().height * 0.9;
                            break;

                        case 3:
                            posX = this.vSize.width - reelBG.getContentSize().width * 0.9 + (i - 2) * reelBG.getContentSize().width;

                            posY = this.vSize.height / 2 - reelBG.getContentSize().height * 0.35;
                            break;

                        default:
                            break;
                    }
                    break;

                case DJS_RNF_STAR:
                    posX = (i - 2) * reelBG.getContentSize().width * reelBG.getScaleX() - reelBG.getContentSize().width * reelBG.getScaleX();
                    posY = 0;
                    break;

                case DJS_FREEZE:
                    switch ( i )
                    {
                        case 0:
                            posX = -reelBG.getContentSize().width * reelBG.getScaleX() - new cc.Sprite( "#reel_bg.png").getContentSize().width * reelBG.getScaleX();
                            break;
                        case 1:
                            posX = posX + new cc.Sprite( "#reel_bg_corner.png" ).getContentSize().width * 0.5 * reelBG.getScaleX() + reelBG.getContentSize().width * 0.5 * reelBG.getScaleX();
                            break;

                        case 2:
                            posX = posX + new cc.Sprite( "#reel_bg.png" ).getContentSize().width * 0.5 * reelBG.getScaleX() + reelBG.getContentSize().width * 0.5 * reelBG.getScaleX();
                            break;

                        default:
                            break;
                    }

                    posY = 0;

                    break;
               /* case DJS_XTREME_MACHINE:
                    posX = (i - 2) * reelBG.getContentSize().width * reelBG.getScaleX();
                    posY = - reelBG.getContentSize().height * 0.12;
                    break;*/
                default:
                    posX = (i - 2) * reelBG.getContentSize().width * reelBG.getScaleX();

                    posY = 0;
                    break;
            }


            this.addChild(reelBG);

            var clip = new cc.ClippingNode();


            var stencil = new cc.DrawNode();

            var Magenta = new cc.color( 255, 0, 255, 0.5 * 255 );

            if ( this.mCode == DJS_X_MULTIPLIERS )
            {
                stencil.drawRect(cc.p( cc.rectGetMinX(reelBG.getBoundingBox()), cc.rectGetMinY(reelBG.getBoundingBox()) + 2.5), cc.p(cc.rectGetMaxX(reelBG.getBoundingBox()), cc.rectGetMaxY(reelBG.getBoundingBox()) - 5), Magenta);
            }
            else if( this.mCode == DJS_DOUBLE_DIAMOND /*||this.mCode == DJS_XTREME_MACHINE*/)
            {
                stencil.drawRect(cc.p(cc.rectGetMinX(reelBG.getBoundingBox()), cc.rectGetMinY(reelBG.getBoundingBox()) + 27.5), cc.p(cc.rectGetMaxX(reelBG.getBoundingBox()), cc.rectGetMaxY(reelBG.getBoundingBox()) - 27.5), Magenta);
            }
            else
            {
                stencil.drawRect(cc.p(cc.rectGetMinX(reelBG.getBoundingBox()), cc.rectGetMinY(reelBG.getBoundingBox()) + 2.5), cc.p(cc.rectGetMaxX(reelBG.getBoundingBox()), cc.rectGetMaxY(reelBG.getBoundingBox()) - 2.5), Magenta);
            }
            clip.setStencil(stencil);
            clip.setInverted(false);


            var r = new Reel(this.mCode, i, reelTypes.PHYSICAL_REEL_TAG, pArr);

            clip.addChild(r);
            this.physicalReels.push(r);

            this.displayedReels.push(r);

            var bR = new Reel(this.mCode, i, reelTypes.BLUR_REEL_TAG);

            clip.addChild(bR);
            this.blurReels.push(bR);

            this.dNode = null;
            if (this.mCode == DJS_WILD_X)
            {
                var wR = new Reel(this.mCode, i, reelTypes.WILD_REEL_TAG);
                wR.setWildReel(wildArr);
                clip.addChild(wR);
                this.wildReel.push(wR);
            }

            if(this.mCode != DJS_FREE_SPIN)
            {
                this.dNode = new cc.DrawNode();
                this.dNode.setPosition( cc.p( 0, 0 ) );
                this.addChild(this.dNode);
            }
            else
            {
                var wR = new Reel(this.mCode, i, reelTypes.WILD_REEL_TAG);
                wR.setWildReel(reelsArrayWild1);
                clip.addChild(wR);
                this.wildReel.push(wR);

                var wRBlur = new Reel(this.mCode, i, reelTypes.WILD_BLUR_REEL_TAG);
                wRBlur.setWildBlurReel(reelsArrayWild1);
                clip.addChild(wRBlur);
                this.blurReelsWild.push(wRBlur);
            }

            this.addChild(clip, 1);

            //var wildSprit = null;

            if (this.mCode == DJS_FREE_SPIN)
            {
                var wildSprit = new cc.Sprite("#9.png");
                clip.addChild(wildSprit, 1);
                wildSprit.setScaleY(0);
                //wildSprit.setVisible(false);
                this.wildSpriteVec.push(wildSprit);

                var DNode = new cc.DrawNode();
                DNode.setOpacityModifyRGB(true);
                DNode.setTag(D_NODE_TAG);
                //DNode.setOpacity(100);
                wildSprit.addChild(DNode, 5);
            }
            var stringFG;// = __String::createWithFormat("machine%d/reel_fg.png", lvl - MACH_1 + 1);

            var addOutLine = false;

            switch ( this.mCode )
            {
                case DJS_FREEZE:
                    switch ( i )
                    {
                        default:
                            stringFG = "reel_fg_corner.png";
                            break;

                        case 1:
                            stringFG = "reel_fg.png";
                            break;
                    }
                    break;

                default:
                    if (i == 3)
                    {
                        stringFG = "reel1_fg.png";
                        addOutLine = true;
                    }
                    else
                    {
                        stringFG = "reel_fg.png";
                    }
                    break;
            }

            var reelFG = null;

            /*
                        if (this.mCode == DJS_XTREME_MACHINE){
                            reelFG = new cc.Sprite( res.EXtreme_reel_fg );
                        }else{
                            reelFG = new cc.Sprite( "#" + stringFG );
                        }

             */
            reelFG = new cc.Sprite( "#" + stringFG );

            if ( this.mCode == DJS_FREEZE  )
            {
                if ( i == 2 ) {
                    reelBG.setFlippedX( true );
                    reelFG.setFlippedX( true );
                }
            }
            if( this.mCode != DJS_X_MULTIPLIERS )
            {
                DJSResizer.getInstance().setFinalScaling( reelFG );
            }
            if(reelFG)
                this.addChild(reelFG, 2);

            if ( addOutLine )
            {
                var outLineSprite = new cc.Sprite( "DJSRNFStar/multiplier.png" );
                DJSResizer.getInstance().setFinalScaling( outLineSprite );
                outLineSprite.setPosition( cc.p( posX, posY ) );
                this.addChild( outLineSprite, 2 );
            }
            if(reelFG)
                reelFG.setPosition(cc.p(posX, posY));
            if(reelBG)
                reelBG.setPosition(cc.p(posX, posY));

            clip.setPosition(reelBG.getPosition());

            /*if (wildSprit)
            {
                wildSprit.setPosition(reelFG.getPosition());
            }*/
            if (this.dNode)
            {
                var col = cc.color(0.65 * 255, 0.65 * 255, 0.65 * 255/*, 0.4 * 255*/);//Change 19 May
                this.dNode.drawSegment(cc.p(cc.rectGetMinX(reelBG.getBoundingBox()), cc.rectGetMidY(reelBG.getBoundingBox())), cc.p(cc.rectGetMaxX(reelBG.getBoundingBox()), cc.rectGetMidY(reelBG.getBoundingBox())), 2.5, col);

                this.reelMids.push(cc.rectGetMinX(reelBG.getBoundingBox()));
                this.reelMids.push(cc.rectGetMidY(reelBG.getBoundingBox()));
                this.reelMids.push(cc.rectGetMaxX(reelBG.getBoundingBox()));
            }

        }
    },

    isSlotRunning : function()
    {
        var running = true;
        var i;
        for (i = 0; i < this.resultReel.length; i++)
        {
            var spriteVec = this.resultReel[i];
            var sprite = spriteVec[0];
            if( sprite.getActionByTag( MOVING_TAG ) )
            {
                break;
            }
        }
        if (i >= this.resultReel.length)
        {
            var game = this.getParent();

            if (this.resultReel.length == this.totalReels)
            {
                game.removeAnimations();
                running = false;
            }
            else if (this.resultReel.length == 2 && !game.excitementChecked)
            {
                game.checkExcitement();
                game.checkFadeInOut(1);
            }
            else if (this.resultReel.length == 1 && !game.fadeInOutChked && !this.isForcedStop)
            {
                game.checkFadeInOut(0);
            }
        }

        return running;
    },

    lightsOff : function( start, end )
    {
        if( start === undefined )
        {
            start = 0;
        }

        if( end === undefined )
        {
            end = 3;
        }

        for (var k = start; k < end; k++)
        {
            var sprite = new cc.Sprite( "res/reel_dark.png" );
            sprite.setTag( DARK_BG_TAG);
            sprite.setColor( cc.color( 255, 0, 0, 255 ) );
            var r = this.blurReels[k];
            sprite.setPosition(r.getParent().getPosition());
            sprite.setOpacity(0);
            sprite.runAction( new cc.FadeTo( 0.5, 150 ) );
            this.addChild(sprite, 1);

            if ( DJS_DIAMOND_3X == this.mCode )
            {
                var game = this.getParent();
                game.resizer.setFinalScaling( sprite );

                sprite.setScale( sprite.getScaleX() * 2, sprite.getScaleX() * 2 );
            }
        }
    },

    setReelsDirection : function( dir )
    {
        for ( var i = 0; i < this.displayedReels.length; i++ )
        {
            var r = this.displayedReels[i];
            r.direction = dir;
        }
    },

    startRoll : function( isWild )
    {
        this.isForcedStop = false;

        var node = this.getChildByTag(DARK_BG_TAG);
        while (node)
        {
            node.removeFromParent(true);
            node = this.getChildByTag(DARK_BG_TAG);
        }

        var sTill = null

        var fire = null

        var freeze = null;

        var halloween = null;

        if ( this.mCode == DJS_SPIN_TILL_YOU_WIN || this.mCode == DJS_SPIN_TILL_YOU_WIN_TWIK )
        {
            sTill = this.getParent();
        }
        else if( this.mCode == DJS_FIRE )
        {
            fire = this.getParent();
        }
        else if( this.mCode == DJS_FREEZE )
        {
            freeze = this.getParent();
        }
        else if( this.mCode == DJS_HALLOWEEN_SLOT )
        {
            halloween = this.getParent();
        }

        for (var i = 0; i < this.displayedReels.length; i++)
        {
            var r = this.displayedReels[i];
            if ( sTill && sTill.spinTillYouWinActive > 0 && i == 1 )
            {
            }
            else if( fire && fire.haveFireElementAt[i] )
            {
            }
            else if( freeze && freeze.haveFireElementAt[i] )
            {
            }
            else if( halloween && halloween.haveFireElementAt[i] )
            {
            }
            else
            {
                r.setFadeOut( this.isForcedStop );
            }
        }

        for (var i = 0; i < this.wildReel.length; i++)
        {
            var r = this.wildReel[i];
            if ( this.mCode == DJS_FREE_SPIN )
            {
                r.setFadeOut( this.isForcedStop );
            }
            else
            {
                r.clearWildReel();
            }
        }

        for (var i = 0; i < this.resultReel.length; i++)
        {
            var spriteVec = this.resultReel[i];

            for (var j = 0; j < spriteVec.length; j++)
            {
                var sprite = spriteVec[j];
                var e = sprite;
                e.aCode = -1;

                sprite.stopAllActions();
                if ( sTill && sTill.spinTillYouWinActive > 0 && i == 1 )
                {
                }
                else if( fire && fire.haveFireElementAt[i] )
                {
                }
                else if( freeze && freeze.haveFireElementAt[i] )
                {
                }
                else if( halloween && halloween.haveFireElementAt[i] )
                {
                }
                else
                {
                    sprite.runAction(new cc.FadeOut(0.2));
                }
            }
        }

        this.resultReel = [];

        var game = this.getParent();
        if (this.mCode == DJS_FREE_SPIN && !game.haveFreeSpins)
        {
            this.resultReelAncient = [];
        }

        this.displayedReels = [];

        this.displayedReels = this.blurReels;

        if (this.mCode == DJS_FREE_SPIN && isWild)
        {
            this.displayedReels = this.blurReelsWild;
        }

        for (var i = 0; i < this.displayedReels.length; i++)
        {
            if (isWild && this.mCode == DJS_RESPIN && i == 1)
            {
            }
            else
            {
                var r = this.displayedReels[i];
                if ( sTill && sTill.spinTillYouWinActive > 0 && i == 1 )//chk this
                {
                }
                else if( fire && fire.haveFireElementAt[i] )
                {
                }
                else if( freeze && freeze.haveFireElementAt[i] )
                {
                }
                else if( halloween && halloween.haveFireElementAt[i] )
                {
                }
                else
                {
                    r.setFadeIn();
                }

                r.setParentScale();
                r.rolling = true;
            }
        }

        for (var i = 0; i < this.physicalReels.length; i++)
        {
            var r = this.physicalReels[i];
            r.setParentScale();
        }
    },

    stopARoll : function( dt )
    {
        var reelsStopped = 0;
        var i;

        if (dt == -1)
        {
            this.isForcedStop = true;
        }
        var game = this.getParent();
        if (game.haveReversal)
        {
            for (i = this.displayedReels.length - 1; i >= 0; i--)
            {
                var r = this.displayedReels[i];
                if (r.rolling)
                {
                    r.stopRolling( this.isForcedStop );
                    this.resultReel.push(game.setResult(game.resultArray[game.totalSlots - game.activeScreens][i], i, game.totalSlots - game.activeScreens));
                    break;
                }
            }
            if (i <= 0)
            {
                reelsStopped = 1;
            }

            AppDelegate.getInstance().jukeBox(S_REEL_STOP);
        }
        else
        {
            for (i = 0; i < this.displayedReels.length; i++)
            {
                var r = this.displayedReels[i];
                if (r.rolling)
                {
                    r.stopRolling( this.isForcedStop );

                    var veC = game.setResult(game.resultArray[game.totalSlots - game.activeScreens][i], i, game.totalSlots - game.activeScreens);

                    this.resultReel.push(veC);

                    if (!game.haveFreeSpins && DJS_FREE_SPIN == this.mCode)
                    {
                        this.resultReelAncient.push(veC);
                    }
                    //log("i = %d, size = %d", i, displayedReels.length);
                    if (i == 0 && ! this.isForcedStop)
                    {
                        game.rescheduleStopper();
                    }
                    break;
                }
            }
            if (i >= this.displayedReels.length - 1)
            {
                reelsStopped = 1;
            }
            AppDelegate.getInstance().jukeBox( S_REEL_STOP );
        }
        return reelsStopped;
    },

    updateSlot : function( dt )
    {
        var i;
        var fSpin = null;
        var sTill = null;
        var fire = null;
        var freeze = null;
        var halloween = null;

        if (this.mCode == DJS_FREE_SPIN)
        {
            fSpin = this.getParent();
        }
        else if ( this.mCode == DJS_SPIN_TILL_YOU_WIN || this.mCode == DJS_SPIN_TILL_YOU_WIN_TWIK )
        {
            sTill = this.getParent();
        }
        else if( this.mCode == DJS_FIRE )
        {
            fire = this.getParent();
        }
        else if( this.mCode == DJS_FREEZE )
        {
            freeze = this.getParent();
        }
        else if( this.mCode == DJS_HALLOWEEN_SLOT )
        {
            halloween = this.getParent();
        }

        for (i = 0; i < this.displayedReels.length; i++)
        {
            if (fSpin)
            {
                if (fSpin.wildLaneCode[i] <= 0)
                {
                    var r = this.displayedReels[i];
                    r.updateRoll(dt);
                }
            }
            else if ( sTill && sTill.spinTillYouWinActive > 0 && i == 1 )
            {
            }
            else if( fire && fire.haveFireElementAt[i] )
            {
            }
            else if( freeze && freeze.haveFireElementAt[i] )
            {
            }
            else if( halloween && halloween.haveFireElementAt[i] )
            {
            }
            else
            {
                var r = this.displayedReels[i];
                r.updateRoll(dt);
            }
        }
    },

    updateSlotWild : function( dt )
    {
        var game = this.getParent();
        var r = this.wildReel[game.currentWildIndx];
        r.updateWildRoll(dt);
    }

});