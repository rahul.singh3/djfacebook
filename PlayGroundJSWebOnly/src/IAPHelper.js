/**
 * Created by RNF-Mac11 on 3/17/17.
 */

var chipsArray = [
    [0, 0, 40000, 0, 250000, 0,2000000],//INSUFFICIENT_CREDITS_1
    [0, 0, 80000, 0, 500000, 0,4000000],//INSUFFICIENT_CREDITS_2
    [0, 0, 120000, 0, 750000, 0,6000000],//INSUFFICIENT_CREDITS_3
    [5000,12000, 40000, 100000, 250000, 750000, 2000000],//BUY_CREDITS
    [15000,36000, 120000, 300000, 750000, 2250000, 6000000],//todo *3 Offer Credits
    [10000,24000, 80000, 200000, 500000, 1500000, 4000000],//todo *2 FIRST_TIME_CREDITS
    [10000,24000, 80000, 200000, 500000, 1500000, 4000000],//todo *2 DOUBLE_CREDITS
    [0, 0, 0,0, 250000, 750000, 2000000],// SPECIAL_OFFER_1
    [0, 0, 0,0, 500000, 1500000, 4000000],// SPECIAL_OFFER_2
    [0, 0, 0,0, 750000, 2250000, 6000000]// SPECIAL_OFFER_3
];

var vipsArray = [
    [0, 0, 250, 0, 1000, 0,10000],//INSUFFICIENT_CREDITS_1
    [0, 0, 250, 0, 1000, 0,10000],//INSUFFICIENT_CREDITS_2
    [0, 0, 250, 0, 1000, 0,10000],//INSUFFICIENT_CREDITS_3
    [50,100, 250, 500, 1000, 5000, 10000],//BUY_CREDITS
    [50,100, 250, 500, 1000, 5000, 10000],//Offer Credits
    [50,100, 250, 500, 1000, 5000, 10000],//FIRST_TIME_CREDITS
    [50,100, 250, 500, 1000, 5000, 10000],//DOUBLE_CREDITS
    [0, 0, 0,0, 1000, 5000, 10000],//SPECIAL_OFFER_1
    [0, 0, 0,0, 1000, 5000, 10000],//SPECIAL_OFFER_2
    [0, 0, 0,0, 1000, 5000, 10000]//SPECIAL_OFFER_3
];

var fileId = [
    "product_0_99",
    "product_1_99",
    "product_4_99",
    "product_9_99",
    "product_19_99",
    "product_49_99",
    "product_99_99"

];

var prodPrice = [0.99, 1.99,4.99, 9.99, 19.99, 49.99, 99.99];



var URLPrefix = "https://7starslots.com/facebook_inapp_verification/doublejackpot/";//*/"https://phonatogames.com/publish/html5/fbdeal/";

var IAPHelper = (function () {
    // Instance stores a reference to the Singleton
    var instance;

    function init() {
        // Singleton

        return {
            pageId : 0,
            productId : 0,
            chipsToCredit : 0,
            vipToCredit : 0,
            isDealBuy : 0,
            is_deal_no:0,
            inApp_Type:0,
            purchase : function( prodId, pId)
            {
                this.vipBonus = 0;
                this.pageId = pId;
                this.productId = prodId;
                 var  checkBonusReward = false;


                switch (this.pageId) {
                    case BuyPageTags.INSTANT_DEAL_CREDITS:
                    {
                        this.inApp_Type = INSTANT_DEAL_IAP;
                        var instantModelObject =InstantDealModel.getInstance();
                        var instantdealInfo = instantModelObject.finalInstantDealList[instantModelObject.instantDealIndex];
                        var currentPay = instantdealInfo.coins;
                        this.chipsToCredit = parseInt( currentPay );
                        this.isDealBuy = 0;
                    }
                        break;
                    case BuyPageTags.DEAL_OFFER:
                    {
                        this.inApp_Type = DEAL_IAP;
                        this.chipsToCredit = parseInt( UserDef.getInstance().getValueForKey( DEAL_REWARD_CHIPS, 0 ) );
                        this.isDealBuy = 1;
                    }
                        break;
                    case BuyPageTags.PAID_WHEEL_PURCHASE:{
                        this.isDealBuy = 0;
                        this.inApp_Type = PAID_WHEEL_IAP;
                        var lobby = cc.director.getRunningScene().getChildByTag( sceneTags.LOBBY_SCENE_TAG );
                        this.chipsToCredit = lobby.getPayWheelCoins();
                    }
                        break;
                    case BuyPageTags.SPECIAL_OFFER_1 :
                    case BuyPageTags.SPECIAL_OFFER_2 :
                    case BuyPageTags.SPECIAL_OFFER_3 :
                        this.inApp_Type = SALE_POP_UP_IAP;
                        this.isDealBuy = 0;
                        break;
                    case BuyPageTags.INSUFFICIENT_CREDITS_1:
                    case BuyPageTags.INSUFFICIENT_CREDITS_2:
                    case BuyPageTags.INSUFFICIENT_CREDITS_3:
                        this.inApp_Type = OUT_OF_CRDIT_IAP;
                        this.isDealBuy = 0;
                        break;
                     default:
                        this.inApp_Type =BUY_PAGE_IAP;
                        this.isDealBuy = 0;
                        break;
                }


                this.vipToCredit = 0;
                if( this.pageId !== BuyPageTags.DEAL_OFFER && this.pageId !== BuyPageTags.PAID_WHEEL_PURCHASE && this.pageId !== BuyPageTags.INSTANT_DEAL_CREDITS)

                {
                    this.chipsToCredit = chipsArray[ this.pageId - BuyPageTags.INSUFFICIENT_CREDITS_1 ][ prodId - purchaseTags.PURCHASE_0_99 ];
                    this.vipToCredit = vipsArray[ this.pageId - BuyPageTags.INSUFFICIENT_CREDITS_1 ][ prodId - purchaseTags.PURCHASE_0_99 ];
                    this.isDealBuy = 0;
                    checkBonusReward = true;
                }
                var bonusRewards = 0;
                if (checkBonusReward){
                    var vipRewards = [ 0, 10, 30, 50, 75, 100, 150 ];
                    bonusRewards = ( this.chipsToCredit * ( vipRewards[this.currentVIPLevel()] / 100.0 ) );
                }
                var prodURL = URLPrefix + fileId[ prodId - purchaseTags.PURCHASE_0_99 ] + ".html";
                if (FacebookObj){
                   // var def = UserDef.getInstance();
                    var VVip = this.vipToCredit;
                    FacebookObj.productId= this.productId;
                    FacebookObj.productPrice= prodPrice[prodId - purchaseTags.PURCHASE_0_99];
                    FacebookObj.chips= this.chipsToCredit+bonusRewards;
                    FacebookObj.dealNo= this.inApp_Type;
                    FacebookObj.transId= 0;
                    FacebookObj.IapVipPts= VVip;
                    FacebookObj.purchaseItem( prodURL );
                }


                this.vipBonus = bonusRewards;
            },
            currentVIPLevel : function()
            {
                var def = UserDef.getInstance();
                var vipLevel = def.getValueForKey( VIP_POINTS, 0 );

                var i;
                for ( i = 6; i >= 0; i-- )
                {
                    if ( vipLevel >= vipArray[i] ) {
                        vipLevel = i;
                        break;
                    }
                }
                return vipLevel;
            },
            getDollarPriceValue : function( pId )
            {
                var dollarValue = 0.0;

                switch ( pId )
                {
                    case purchaseTags.PURCHASE_0_99:
                        dollarValue = 0.99;
                        break;

                    case purchaseTags.PURCHASE_4_99:
                        dollarValue = 4.99;
                        break;

                    case purchaseTags.PURCHASE_9_99:
                        dollarValue = 9.99;
                        break;

                    case purchaseTags.PURCHASE_19_99:
                        dollarValue = 19.99;
                        break;

                    case purchaseTags.PURCHASE_49_99:
                        dollarValue = 49.99;
                        break;

                    case purchaseTags.PURCHASE_99_99:
                        dollarValue = 99.99;
                        break;
                }

                return dollarValue;
            },
            vipBonus : 0,
            setVariablesFromJSON:function(response){
                if (response==1){
                    this.ReceiptVerificationCB();
                }
            },
            ReceiptVerificationCB : function( )
            {

                var bonusRewards = this.vipBonus;
                var def = UserDef.getInstance();
                var VVip = def.getValueForKey( VIP_POINTS, 0 );
                VVip+=this.vipToCredit;
                def.setValueForKey( VIP_POINTS, VVip );

                var iapCount = def.getValueForKey( IAP_COUNT, 0 ) + 1;
                def.setValueForKey( IAP_COUNT , iapCount );
                var isSuccedPOPup = true;
                if( this.pageId === BuyPageTags.PAID_WHEEL_PURCHASE )
                {
                    var lobby = cc.director.getRunningScene().getChildByTag( sceneTags.LOBBY_SCENE_TAG );//Change Nov 24 2017 for paid bonus wheel

                    if( lobby )
                    {
                        isSuccedPOPup = false;
                        cc.director.getRunningScene().removeChildByTag( LOADER_TAG );
                        lobby.checkAndHandlePaidBonusWheel();
                    }
                }
                else {
                    if (userDataInfo.getInstance().isInAppVerified){
                        var dataJson = {
                            'coins': userDataInfo.getInstance().totalChips+this.chipsToCredit+bonusRewards,
                            'vip_points': def.getValueForKey( VIP_POINTS, 0 ),
                        };
                        ServerData.getInstance().updateActiveMultipleReplaceOnServer(dataJson);
                    }
                }
                var reAssign300 = false;
                var purchase_frequency = ServerData.getInstance().server_purchase_frequency;
                if( def.getValueForKey( IS_BUYER, false ) && ServerData.getInstance().purchase_strategy && purchase_frequency && iapCount % purchase_frequency == 0 )
                {
                    if( userDataInfo.getInstance().totalChips < 10000000 )//10 mil
                    {
                        reAssign300 = true;
                    }
                }
                var userdef  = CSUserdefauts.getInstance();

                if ( !def.getValueForKey( IS_BUYER, false ) || reAssign300 )
                {
                    var chipsCredited = ServerData.getInstance().server_buyer_strategy * this.chipsToCredit;


                    if( chipsCredited > 5000000 )       chipsCredited = 5000000;//5 mil

                    if(chipsCredited < 100000)//Change Raghib Oct 30 2017
                    {
                        userdef.setValueForKey( BET_LOWER_LIMIT , 0 );
                    }
                    else if(chipsCredited < 500000 )
                    {
                        userdef.setValueForKey( BET_LOWER_LIMIT , 500 );
                    }
                    else if(chipsCredited < 1200000 )
                    {
                        userdef.setValueForKey( BET_LOWER_LIMIT , 1000 );
                    }
                    else if(chipsCredited < 2500000 )
                    {
                        userdef.setValueForKey( BET_LOWER_LIMIT , 5000 );
                    }
                    else
                    {
                        userdef.setValueForKey( BET_LOWER_LIMIT , 10000 );
                    }
                    reAssign300 = true;
                    userdef.setValueForKey( EASY_MODE_BET_300, chipsCredited );
                    userdef.setValueForKey( EASY_MODE_WIN_CHIPS_300, chipsCredited );
                }
                var isDealRefresh = false;
                if ( !def.getValueForKey( IS_BUYER, false ) )
                {
                    var serdataObj = ServerData.getInstance();
                    if (serdataObj.buy_page_offer_new_user &&  serdataObj.server_buy_page== 2)
                          serdataObj.server_buy_page=4;

                    var userdataObject = userDataInfo.getInstance();
                    userdataObject.deal_remaining_seconds = SECONDS_IN_AN_HOUR / 2;
                    userdataObject.deal_reflush_remaining_seconds = -1;
                    var productId = serdataObj.server_deal_buyer_image.length>1 ? serdataObj.server_deal_buyer_product : BUYER_DEFAULT_PRODUCT_ID -purchaseTags.PURCHASE_0_99;
                    var reward_chips = serdataObj.server_deal_buyer_image.length>1? serdataObj.server_deal_buyer_coins : BUYER_DEFAULT_CHIPS ;
                    def.setValueForKey( DEAL_PRODUCT_ID, productId );
                    def.setValueForKey( DEAL_REWARD_CHIPS, reward_chips );
                    serdataObj.updateDealActive(userdataObject.deal_remaining_seconds, def.getValueForKey(DEAL_PRODUCT_ID,0) ,def.getValueForKey(DEAL_REWARD_CHIPS,BUYER_DEFAULT_CHIPS),1);
                    AppDelegate.getInstance().dealFlushed = false;

                    var deal = AppDelegate.getInstance().getDealInstance();

                    if ( deal.getDealStatus() || deal.dealOnScreen )
                    {
                        AppDelegate.getInstance().dealHiding(true);
                        //deal.resetDealData();
                    }
                    isDealRefresh = true;
                }
                def.setValueForKey( IS_BUYER, true );
                ServerData.getInstance().setNewBuyPageStrategy();
                if (isDealRefresh){
                    var lobby = cc.director.getRunningScene().getChildByTag( sceneTags.LOBBY_SCENE_TAG );//Change Nov 24 2017 for paid bonus wheel
                    if( lobby )
                    {
                        lobby.createHourlyBonusTwoXImage();
                    }
                    deal.setDealData(1);
                }
                if (isSuccedPOPup) {
                    cc.director.getRunningScene().removeChildByTag(LOADER_TAG);
                    this.setSuccessFulPop(bonusRewards);
                }

                var dollarSpent = def.getValueForKey( DOLLARS_SPENT, 0 );
                dollarSpent+=Math.ceil( this.getDollarPriceValue( this.productId ) );
                def.setValueForKey( DOLLARS_SPENT, dollarSpent );


                var data = ServerData.getInstance();

                if ( data.server_machine_tunning_buyer >= 1 && !reAssign300 )
                {
                    if ( userdef.getValueForKey( EASY_MODE_BET, 0 ) < 0 )
                    {
                        userdef.setValueForKey( EASY_MODE_BET, 0 );
                    }
                    userdef.setValueForKey( EASY_MODE_BET, userdef.getValueForKey( EASY_MODE_BET, 0 ) + ( this.chipsToCredit / data.server_machine_tunning_buyer ) );

                    var game = cc.director.getRunningScene();//.getChildByTag( sceneTags.GAME_SCENE_TAG );

                    if ( game.getTag() == sceneTags.GAME_SCENE_TAG )
                    {
                        if ( def.getValueForKey( MACHINE_TUNNING, 0 ) == 0 )
                        {
                            if ( userdef.getValueForKey( EASY_MODE_BET, 0 ) > 0 )
                            {
                                game.easyModeCode = BIASING_MODE_140;
                                game.updateProbabiltyArrayOnInApp();
                                game.changeTotalProbabilities();
                            }
                        }
                    }
                }




            },
            setSuccessFulPop : function( bonusRewards )
            {

                var scene = cc.director.getRunningScene();

                var layer = scene;//.getChildByTag( sceneTags.GAME_SCENE_TAG );

                var game = null;

                var lobby = null;

                var size = cc.winSize;


                if ( layer.getTag() == sceneTags.GAME_SCENE_TAG )
                {
                    game = layer;
                }
                else
                {
                    layer = scene.getChildByTag( sceneTags.LOBBY_SCENE_TAG );
                    lobby = layer;
                }

                var dNode = new cc.DrawNode();

                dNode.setPosition( cc.p( 0, 0 ) );

                var darkBG = new cc.Sprite( res.Dark_BG_png );
                darkBG.setScale( 2 );
                darkBG.setOpacity( 169 );
                darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
                dNode.addChild( darkBG );

                //var color =  cc.color( 0.0, 0.0, 0.0, 100.0 );
                //dNode.drawRect( cc.p( 0, 0 ), cc.p( size.width, size.height ), color );

                var bgSprite = new cc.Sprite( "res/purchasedCreditsPop.png" );
                bgSprite.setPosition( cc.p( size.width / 2, size.height / 2 ) );
                dNode.addChild( bgSprite );

                var fontSize = 25;
                var siz = cc.size( 150, fontSize );
                var totalString = DPUtils.getInstance().getNumString( this.chipsToCredit + bonusRewards );

                var totalLabel = new CustomLabel( totalString, "CarterOne", fontSize, siz );
                totalLabel.setPosition( cc.p( 485, 141 ) );
                bgSprite.addChild( totalLabel );

                if ( totalLabel.getFontSize() < fontSize ) {
                    fontSize = totalLabel.getFontSize();
                }

                var bonusString = DPUtils.getInstance().getNumString( bonusRewards );

                var bonusLabel = new CustomLabel( bonusString, "CarterOne", fontSize, siz );
                bonusLabel.setPosition( cc.p( 307.5, totalLabel.getPositionY() ) );
                bgSprite.addChild( bonusLabel );

                if ( bonusLabel.getFontSize() < fontSize ) {
                    fontSize = bonusLabel.getFontSize();
                }

                var coinString = DPUtils.getInstance().getNumString( this.chipsToCredit );

                var coinLabel = new CustomLabel( coinString, "CarterOne", fontSize, siz );
                coinLabel.setPosition( cc.p( 110, totalLabel.getPositionY() ) );
                bgSprite.addChild( coinLabel );

                if ( coinLabel.getFontSize() < fontSize ) {
                    fontSize = coinLabel.getFontSize();
                }

                if ( totalLabel.getFontSize() > fontSize )
                {
                    totalLabel.setFontSize( fontSize );
                }

                if ( bonusLabel.getFontSize() > fontSize )
                {
                    bonusLabel.setFontSize( fontSize );
                }

                if ( coinLabel.getFontSize() > fontSize )
                {
                    coinLabel.setFontSize( fontSize );
                }

                var item = new cc.MenuItemImage( "res/levelUpOk.png", "res/levelUpOk.png", this.menuCB2, this );
                item.getSelectedImage().setColor( cc.color( 169,169, 169 ) );
                item.setPosition( cc.p( size.width / 2, cc.rectGetMinY(bgSprite.getBoundingBox()) + item.getContentSize().height * 0.75 ) );

                var menu = new cc.Menu( item, null );
                menu.setPosition( cc.p( 0, 0 ) );
                dNode.addChild( menu );



                if ( game )
                {
                    game.addChild( dNode, 10 );
                }
                else if( lobby )
                {
                    lobby.addChild( dNode, 10 );
                }

                DPUtils.getInstance().setTouchSwallowing( null, darkBG );

                //ServerCommunicator.getInstance().SCUpdateData();
                //DJSAnalyticsHelper::getInstance().postAppAnalyticsToServer( true );
            },
            menuCB2 : function( pSender )
            {
                if (InstantDealModel.getInstance().openInstantDeal){
                    var scene = cc.director.getRunningScene();
                    var layer = scene.getChildByTag( sceneTags.LOBBY_SCENE_TAG );
                    var lobby = layer;
                    if (lobby){
                        lobby.removeChildByTag(PopUpTags.STAY_IN_TOUCH_INSTANT_DEAL,true);
                        InstantDealModel.getInstance().closeInstantDeal(true);
                    }

                }
                var delegate = AppDelegate.getInstance();
                delegate.assignRewards(this.chipsToCredit+this.vipBonus,false);
                pSender.getParent().getParent().removeFromParent( true );
            },
            setWaitingScreen : function () {

                var dNode = new cc.DrawNode();

                dNode.setPosition( cc.p( 0, 0 ) );
                dNode.setContentSize(cc.winSize);
                var loadingScr = new cc.Sprite( res.Dark_BG_png );
                loadingScr.setScale( 2 );
                loadingScr.setOpacity( 169 );
                loadingScr.setPosition( cc.p( dNode.getContentSize().width * 0.5, dNode.getContentSize().height * 0.5 ) );
                dNode.addChild( loadingScr );
                dNode.setTag( LOADER_TAG )



                var loadingSprite = new cc.Sprite( "res/appLaoder.png" );
                loadingSprite.setPosition( cc.p( loadingScr.getContentSize().width * 0.5, loadingScr.getContentSize().height * 0.5 ));
                loadingSprite.setScale(0.6);
                loadingScr.addChild( loadingSprite );
                loadingSprite.runAction( new cc.RepeatForever( new cc.RotateBy( 1.5, 360 ) ) );
                DPUtils.getInstance().setTouchSwallowing( null, dNode );
                cc.director.getRunningScene().addChild(dNode,1000);

            }
        };
    };
    return {
        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function () {
            if ( !instance ) {
                instance = init();
            }
            return instance;
        }
    };
})();