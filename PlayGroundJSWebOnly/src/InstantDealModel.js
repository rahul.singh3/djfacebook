
InstantDealVar = (function () {
    return{
        _id:0,
        aggressive_meter:0,
        template_type:0,
        device_type:1,
        max_buy_count:0,
        max_view_count:50,
        view_count:0,
        buy_count:0,
        reward:0,
        image_url:"",
        product_id:"",
        button_label:"BUY",
        coins:0,
        bgTexture:null,
        init:function (valueMap) {
            // var valueMap1 = JSON.stringify(obj);
            // var    v = ServerData.getInstance().encodedJsonData();
            if(valueMap){
                this._id = valueMap["_id"];
                this.aggressive_meter = valueMap["aggressive_meter"];
                this.template_type = valueMap["template_type"];
                this.device_type =  valueMap["device_type"];
                this.max_buy_count =  valueMap["max_buy_count"];
                this.max_view_count =  valueMap["max_view_count"];
                this.view_count = valueMap["view_count"];
                this.buy_count =  valueMap["buy_count"];
                this.coins     =     parseInt( valueMap["coins"]);
                this.image_url =  valueMap["image_url"]?valueMap["image_url"]:"";
                this.product_id =  valueMap["product_id"]?valueMap["product_id"]:"";
                this.button_label =  valueMap["button_label"]?valueMap["button_label"]:"BUY";
                if (this.button_label.length===0){
                    this.button_label = "BUY";
                }
                this.bgTexture = null;

            }
            return this;
        },
        getInstantDealVar:function () {
            var valueMap;
            valueMap={
                '_id': this._id,
                'button_label' : this.button_label,
                'product_id':this.product_id,
                'image_url' :this.image_url,
                'coins':this.coins,
                'buy_count':this.buy_count,
                'view_count':this.view_count,
                'max_view_count':this.max_view_count,
                'max_buy_count':this.max_buy_count,
                'device_type':this.device_type,
                'template_type':this.template_type,
                'aggressive_meter':this.aggressive_meter,

            };
            return valueMap;

        }
    };
});
InstantDealParams = (function () {
    return{
        time_diff:0,
        show_no_of_deals_on_machine_switch:0,
        aggressive_meter:1,
        init:function (valueMap) {
            // var valueMap1 = JSON.stringify(obj);
            // var    v = ServerData.getInstance().encodedJsonData();
            if(valueMap){
                this.time_diff = valueMap["time_diff"];
                this.show_no_of_deals_on_machine_switch = valueMap["show_no_of_deals_on_machine_switch"];


            }
            return this;
        },
        getInstantDealVar:function () {
            var valueMap;
            valueMap={
                'time_diff': this.time_diff,
                'show_no_of_deals_on_machine_switch' : this.show_no_of_deals_on_machine_switch,
                'aggressive_meter':this.aggressive_meter,
            };
            return valueMap;

        }
    };
});
InstantDealModel = (function () {
    // Instance stores a reference to the Singleton
    var instance;
    function init() {
        return{
            _InstantDealData:[],
            _buyPageBGRawImg:null,
            _tempBgSprites:[],
            isLoadedImages:false,
            finalInstantDealList:[],
            openInstantDeal:false,
            refreshdataModel:function (instant_deals) {
                for (var id in instant_deals){
                    var datainstant_deals = instant_deals[parseInt(id)];
                    var istantDealVar = new InstantDealVar();
                    istantDealVar.init(datainstant_deals);
                    this._InstantDealData.push(istantDealVar);
                }
                if (this._InstantDealData.length>0){
                    this.indexValue =0;
                    for (var idx = 0; idx< this._InstantDealData.length; idx++){
                        this.isLoadedImages = true;
                        this.loadImage(idx);
                    }
                }
            },
            closeInstantDeal:function(comeFromPurchase){
                if (comeFromPurchase){
                    instance.finalInstantDealList[instance.instantDealIndex].buy_count += 1;
                    ServerData.getInstance().instantDealViewAndBuy(instance.finalInstantDealList[instance.instantDealIndex]._id,false);
                    instance.updateInstantDealViewAndBuyCount();
                }


                cc.log("Instant closeDeal: "+ instance.finalInstantDealList.length);
                   instance.openInstantDeal = false;
                if (instance.instantDealIndex < instance.finalInstantDealList.length - 1) {
                    instance.instantDealIndex = instance.instantDealIndex + 1;
                    instance.showInstantDealIfAvailable();
                }else{
                    AppDelegate.getInstance().showingScreen=false;
                    PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_IN_TOUCH_INSTANT_DEAL);

                }
            },
            updateInstantDealViewAndBuyCount:function(){
                var valuevector=[];
                for(var dealvari in instance.finalInstantDealList) {
                    var dealvar = instance.finalInstantDealList[dealvari];
                    var valueMap={
                        'id': dealvar._id,
                        'buy_count': dealvar.buy_count,
                        'view_count': dealvar.view_count,
                    };
                    valuevector.push(valueMap);
                }
                CSUserdefauts.getInstance().setValueForKey("instant_deal_inapp_list",valuevector);
                ServerData.getInstance().updateCustomDataOnServer();

            },
            indexValue:0,
            loadImage:function (idx) {
                var dataInstant = this._InstantDealData[idx];
                var imagePath = dataInstant.image_url;
                if (imagePath && imagePath.length) {
                    cc.loader.loadImg(imagePath, {isCrossOrigin: true}, function (err, texture) {
                        cc.log("Instant Deal Texture : "+texture+"idx ="+idx+"_InstantDealData ="+instance._InstantDealData.length);
                        if (texture && err == null) {
                            var texture2d = new cc.Texture2D();
                            texture2d.initWithElement(texture);
                            texture2d.handleLoadedTexture();
                            instance._tempBgSprites.push(instance._InstantDealData[idx]);
                            instance._tempBgSprites[instance._tempBgSprites.length-1].bgTexture = texture2d;
                            instance.indexValue++;
                            // instance._tempBgSprites[instance._tempBgSprites.length-1].bgTexture.retain();
                            cc.log("Instant Deal indexValue : "+instance.indexValue);
                            if (instance.indexValue == instance._InstantDealData.length)
                                instance.createGroupOfSamePID();
                        }
                    });
                }
            },
            createGroupOfSamePID:function () {
                instance.finalInstantDealList=[];
                for(var idx = purchaseTags.PURCHASE_0_99; idx <= purchaseTags.PURCHASE_99_99; idx++){
                    var tempDeal =[];
                    for (var productId in instance._tempBgSprites){
                        var holdImage = instance._tempBgSprites[productId].product_id;
                        if (holdImage===instance.getPurchaseAmt(idx)){
                            tempDeal.push(instance._tempBgSprites[productId]);
                        }
                    }
                    if (tempDeal.length > 0) {
                        var random = Math.floor( Math.random( ) * tempDeal.length-1 ); //RandomHelper::random_int(0,(int)tempDeal.size()-1);
                        instance.finalInstantDealList.push(tempDeal[random==-1?0:random]);
                    }
                }
                if(instance.finalInstantDealList.length>2){
                    var i, j;
                    var n= instance.finalInstantDealList.length;
                    for (i = 0; i < n-1; i++){
                        // Last i elements are already in place
                        for (j = 0; j < n-i-1; j++){
                            var task1 = instance.finalInstantDealList[j];
                            var task2 = instance.finalInstantDealList[j+1];
                            if (parseFloat(task1.product_id) > parseFloat(task2.product_id)){
                                var b = instance.finalInstantDealList[j+1];
                                instance.finalInstantDealList[j+1] = instance.finalInstantDealList[j];
                                instance.finalInstantDealList[j] = b;
                            }
                        }


                    }
                }

                cc.log("Instant Deal finalInstantDealList count : "+instance.finalInstantDealList?instance.finalInstantDealList.length:0);


            },
            getPurchaseAmt:function(no){
                var amountAd = "0.99";
                switch (no) {
                    case purchaseTags.PURCHASE_0_99:
                        amountAd = "0.99";
                        break;
                    case purchaseTags.PURCHASE_01_99:
                        amountAd = "1.99";
                        break;
                    case purchaseTags.PURCHASE_4_99:
                        amountAd = "4.99";
                        break;
                    case purchaseTags.PURCHASE_9_99:
                        amountAd = "9.99";
                        break;
                    case purchaseTags.PURCHASE_19_99:
                        amountAd = "19.99";
                        break;
                    case purchaseTags.PURCHASE_49_99:
                        amountAd = "49.99";
                        break;
                    case purchaseTags.PURCHASE_99_99:
                        amountAd = "99.99";
                        break;
                }
                return amountAd;
            },
            getInstantDealViewCount:function () {

            },
            setinstant_deal_paramsData:function(instant_deal_params){
               instance.instant_deal_params = new InstantDealParams().init(instant_deal_params);
            },
            showInstantDealOnMachineSwitch:function () {
                if (instance.finalInstantDealList.length > 0) {
                    var sec =  new Date().getSeconds()
                    var noOfSecondsPassed = instance.instantDealTime-sec;
                    var isRequiredTimeSpent = noOfSecondsPassed >= instance.instant_deal_params.time_diff;
                    if (isRequiredTimeSpent && instance.instant_deal_params.show_no_of_deals_on_machine_switch > 0){
                         for (var index =0;index<instance.finalInstantDealList.length;index++){
                             var deal = instance.finalInstantDealList[index];
                             if (deal.view_count >= deal.max_view_count || deal.buy_count >= deal.max_buy_count){
                                 instance.finalInstantDealList.splice(index,1);
                             }
                         }
                        var tempInstantDealList=[];
                        for (var i = 0; i < instance.instant_deal_params.show_no_of_deals_on_machine_switch; i++) {
                            if (instance.finalInstantDealList.length > 0) {
                                var random = Math.floor( Math.random( ) * instance.finalInstantDealList.length-1 ); //RandomHelper::random_int(0,(int)tempDeal.size()-1);
                                tempInstantDealList.push( instance.finalInstantDealList[random>0?random:0]);
                                instance.finalInstantDealList.splice(random); // erase the nth element
                            }
                        }
                        instance.finalInstantDealList =[];

                        if(tempInstantDealList.length>2){
                            var i, j;
                            var n= tempInstantDealList.length;
                            for (i = 0; i < n-1; i++){
                                // Last i elements are already in place
                                for (j = 0; j < n-i-1; j++){
                                    var task1 = tempInstantDealList[j];
                                    var task2 = tempInstantDealList[j+1];
                                    if (task1.product_id > task2.product_id){
                                        var b = tempInstantDealList[j+1];
                                        tempInstantDealList[j+1] = tempInstantDealList[j];
                                        tempInstantDealList[j] = b;
                                    }
                                }


                            }
                        }
                      instance.finalInstantDealList = tempInstantDealList;

                        if (instance.finalInstantDealList.length > 0){
                            instance.showInstantDealPopUp();
                        }
                        instance.instant_deal_params.show_no_of_deals_on_machine_switch = 0;
                        instance.InstantDealShowTime = new Date().getSeconds();
                    }

                }
            },
            instantDealIndex:0,
            instantDealTime:0,
            instant_deal_params:null,
            showInstantDealPopUp:function (dt) {
                if (instance.finalInstantDealList.length > 0 && instance.finalInstantDealList[0].bgTexture != null) {
                    instance.instantDealIndex = 0;
                    var sec =  new Date().getTime();
                    instance.instantDealTime = new Date().getSeconds();
                    PopUps.getInstance().addPopUpIndex(PopUpTags.STAY_IN_TOUCH_INSTANT_DEAL);
                    cc.director.getScheduler().unschedule("InstantDealPopup",instance);
                }
            },
            deleteImageIfNotReq:function () {
            },
            showInstantDeal:function () {
            if(AppDelegate.getInstance().appJustLaunchedForInstantDeal){
                cc.director.getScheduler().unschedule("InstantDealPopup",instance);
                instance.showInstantDealPopUp(0);
                cc.director.getScheduler().schedule(instance.showPopUp,instance,1,false,"InstantDealPopup");

            }else if(!AppDelegate.getInstance().appJustLaunchedForInstantDeal ){
                instance.showInstantDealOnMachineSwitch();
                }
            },
            showPopUp:function(){
                instance.showInstantDealPopUp(0);
            },
            showInstantDealIfAvailable:function () {
                cc.log("Instant showInstantDealIfAvailable finalInstantDealList count : "+instance.finalInstantDealList?instance.finalInstantDealList.length:0);

                if(instance.finalInstantDealList.length > instance.instantDealIndex)
                {

                    var instantDealObj = instance.finalInstantDealList[instance.instantDealIndex];

                    if (instantDealObj.bgTexture != null) {
                        var scene = cc.director.getRunningScene();
                        var layer = scene.getChildByTag( sceneTags.LOBBY_SCENE_TAG );
                        if (layer || scene.getTag() == sceneTags.LOBBY_SCENE_TAG) {
                            AppDelegate.getInstance().showingScreen=true;
                            layer.setInstantDealPop(0);
                        }

                    }
                }
            }
        };
    };
    return {
        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function () {
            if ( !instance ) {
                instance = init();
            }
            return instance;
        }
    };
})();