/**
 * Created by RNF-Mac11 on 2/8/17.
 */



var reelsRandomizationArrayDJSFreeze =
    [
        [// 85% payout
            2000, 2000, 400, 1000, 1500,
            800, 3500, 800, 3000, 800,
            4000, 800, 1200, 1000, 3000,
            800, 800, 1000, 3500, 1000,
            5700, 2000, 300, 2000, 3500,
            1000, 4000, 500, 4000, 1000,
            3000, 1000, 3000, 200, 5000, 2000
        ],
        [//92 %
            2000, 2000, 400,1700, 1500,
            800, 3500, 800, 3000, 800,
            4000, 800, 1200, 1000, 3000,
            800, 800, 1000, 3500, 1000,
            5700, 2000, 400, 2000, 3500,
            1000, 4000, 500, 4000, 1000,
            3000, 1000, 3200, 5, 5500, 1
        ],
        [//97 % payout
            2000, 2000, 400, 500, 1500,
            800, 3500, 800, 3000, 800,
            4000, 800, 1200, 1000, 3000,
            800, 800, 1000, 3500, 1000,
            5700, 2000, 400, 2000, 3500,
            1000, 4000, 500, 4000, 1000,
            3000, 1000, 3200, 5, 5500, 1
        ],
        [//140 %

            3000, 1200, 600, 50, 3000,
            600, 5000, 600, 4500, 600,
            5000, 600, 1200, 800, 3000,
            600, 1000, 800, 3500, 800,
            6000, 1200, 600, 500, 3500,
            800, 4000, 300, 4000, 800,
            4000, 800, 4200, 1, 7000, 1
        ],
        [//300 %

            3000, 20, 5000, 10, 9000,
            10, 11000, 10, 10500, 10,
            11000, 10, 7200, 20, 9000,
            10, 7000, 20, 9500, 20,
            12000, 25, 6000, 10, 15500,
            10, 15000, 5, 15000, 5,
            18000, 5, 18200, 1, 30000, 1
        ]
    ];

var DJSFreeze = GameScene.extend({

    fireGenerated : false,

    haveFireElementAt : new Array( 3 ),
    reelsArrayDJSFreeze:[],
    ctor:function ( level_index ) {
        this.reelsArrayDJSFreeze = [
            /*1*/ELEMENT_7_BAR, /*2*/ELEMENT_EMPTY, /*3*/ELEMENT_X, /*4*/ELEMENT_EMPTY, /*5*/ELEMENT_BAR_1,
            /*6*/ELEMENT_EMPTY, /*7*/ELEMENT_BAR_2, /*8*/ELEMENT_EMPTY,/*9*/ELEMENT_BAR_3, /*10*/ELEMENT_EMPTY,
            /*11*/ELEMENT_CHERRY, /*12*/ELEMENT_EMPTY, /*13*/ELEMENT_7_BAR, /*14*/ELEMENT_EMPTY, /*15*/ELEMENT_BAR_3,
            /*16*/ELEMENT_EMPTY, /*17*/ELEMENT_7_BAR, /*18*/ELEMENT_EMPTY, /*19*/ELEMENT_CHERRY, /*20*/ELEMENT_EMPTY,
            /*21*/ELEMENT_7_BAR, /*22*/ELEMENT_EMPTY, /*23*/ELEMENT_X, /*24*/ELEMENT_EMPTY, /*25*/ELEMENT_BAR_2,
            /*26*/ELEMENT_EMPTY, /*27*/ELEMENT_CHERRY, /*28*/ELEMENT_EMPTY, /*29*/ELEMENT_BAR_1, /*30*/ELEMENT_EMPTY,
            /*31*/ELEMENT_BAR_3, /*32*/ELEMENT_EMPTY, /*33*/ELEMENT_BAR_2, /*34*/ELEMENT_EMPTY, /*35*/ ELEMENT_BAR_1,
            /*36*/ELEMENT_EMPTY
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSFreeze;
        this.physicalArrayCheck= this.reelsArrayDJSFreeze;
        this._super( level_index );

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;


    },

    calculatePayment : function( doAnimate )
    {
        var payMentArray = [
            /*0*/     200,//triple wild

            /*1*/     50, // 7Bar 7Bar 7Bar

            /*2*/     25, // Cherry Cherry Cherr

            /*3*/     15,  // 3Bar 3Bar 3Bar

            /*4*/     10,  // 2Bar 2Bar 2Bar

            /*5*/     5,   // Bar Bar Bar

            /*6*/     4,   // 7 Bar and cherry combo

            /*7*/     2  // any bar combo
        ];

        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        for ( var i = 0; i < this.slots[0].resultReel.length; i++ )
        {
            tmpVector = this.slots[0].resultReel[ i ];
            tmpVector2.push( tmpVector[ 1 ] );
        }

        this.lineVector.push( tmpVector2 );

        //bool generateThrice = false;

        var pay = 0;
        var lineLength = 3;
        var count2X = 0;
        var countWild = 0;
        var countGeneral = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var i, j, k;
        var e = null;
        var pivotElm = null;
        var pivotElm2 = null;
        var tmpVec;

        for ( i = 0; i < this.totalLines; i++ )
        {
            tmpVec = this.lineVector[ i ];
            for ( k = 0; k < 8; k++ )
            {
                var breakLoop = false;
                countGeneral = 0;
                switch ( k )
                {
                    case 0://for all combination with wild
                        for ( j = 0; j < lineLength; j++ )
                        {
                            if ( this.resultArray[0][j] != -1 && this.reelsArrayDJSFreeze[this.resultArray[i][j]]!= ELEMENT_EMPTY )
                            {
                                switch ( this.reelsArrayDJSFreeze[this.resultArray[i][j]] )
                                {
                                    case ELEMENT_X:
                                        count2X++;
                                        countWild++;

                                        if ( doAnimate )
                                        {
                                            if ( j < tmpVec.length )
                                            {
                                                e = tmpVec[ j ];
                                            }
                                            this.haveFireElementAt[j] = true;
                                            this.canChangeBet = false;
                                        }

                                        if ( e )
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        switch ( pivotElement )
                                        {
                                            case -1:
                                                pivotElement = this.reelsArrayDJSFreeze[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate ) {
                                                    if ( j < tmpVec.length )
                                                    {
                                                        e = tmpVec[ j ];
                                                    }
                                                }
                                                if ( e )
                                                {
                                                    pivotElm = e;
                                                }
                                                break;

                                            default:
                                                pivotElement2 = this.reelsArrayDJSFreeze[this.resultArray[i][j]];
                                                countGeneral++;

                                                if ( doAnimate ) {
                                                    if ( j < tmpVec.length )
                                                    {
                                                        e = tmpVec[ j ];
                                                    }
                                                }
                                                if ( e )
                                                {
                                                    pivotElm2 = e;
                                                }
                                                break;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                //break;
                            }
                        }

                        if ( j == 3 )
                        {
                            if ( countWild >= 3 )
                            {
                                breakLoop = true;
                                this.fireGenerated = false;
                                pay = payMentArray[0];

                            }
                            else if( countWild >= 2 )
                            {
                                breakLoop = true;

                                switch ( pivotElement )
                                {
                                    case ELEMENT_7_BAR:
                                        pay = payMentArray[1];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_CHERRY:
                                        pay = payMentArray[2];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_3:
                                        pay = payMentArray[3];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_2:
                                        pay = payMentArray[4];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_1:
                                        pay = payMentArray[5];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        break;
                                }
                            }
                            else if( countWild == 1 )
                            {
                                breakLoop = true;

                                if ( pivotElement == pivotElement2 )
                                {
                                    switch ( pivotElement )
                                    {
                                        case ELEMENT_7_BAR:
                                            pay = payMentArray[1];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_CHERRY:
                                            pay = payMentArray[2];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_3:
                                            pay = payMentArray[3];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_2:
                                            pay = payMentArray[4];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_1:
                                            pay = payMentArray[5];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        default:
                                            //pay = tmpPay;
                                            break;
                                    }
                                }
                                else
                                {
                                    if ( ( pivotElement == ELEMENT_7_BAR || pivotElement == ELEMENT_CHERRY ) &&
                                        ( pivotElement2 == ELEMENT_7_BAR || pivotElement2 == ELEMENT_CHERRY ) )
                                    {
                                        pay = payMentArray[6];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if ( ( pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_BAR_3 ) &&
                                        ( pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_BAR_3 ) )
                                    {
                                        pay = payMentArray[7];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                }
                            }
                        }
                        break;

                    case 1: // 7 7 7 bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFreeze[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[1];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                                if ( doAnimate ) {
                                    if ( l < tmpVec.length )
                                    {
                                        e = tmpVec[ l ];
                                    }
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 2: // Triple Cherry
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFreeze[this.resultArray[i][j]] )
                            {
                                case ELEMENT_CHERRY:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[2];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                                if ( doAnimate ) {
                                    if ( l < tmpVec.length )
                                    {
                                        e = tmpVec[ l ];
                                    }
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 3: // 7bar and Cherry Combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFreeze[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_BAR:
                                case ELEMENT_CHERRY:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[6];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                                if ( doAnimate ) {
                                    if ( l < tmpVec.length )
                                    {
                                        e = tmpVec[ l ];
                                    }
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 4: // 3Bar 3Bar 3Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFreeze[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[3];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                                if ( doAnimate ) {
                                    if ( l < tmpVec.length )
                                    {
                                        e = tmpVec[ l ];
                                    }
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 5: // 2Bar 2Bar 2Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFreeze[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[4];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                                if ( doAnimate ) {
                                    if ( l < tmpVec.length )
                                    {
                                        e = tmpVec[ l ];
                                    }
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 6: // Bar Bar Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFreeze[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[5];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                                if ( doAnimate ) {
                                    if ( l < tmpVec.length )
                                    {
                                        e = tmpVec[ l ];
                                    }
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 7: // any Bar combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFreeze[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[7];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( doAnimate ) {
                                    if ( l < tmpVec.length )
                                    {
                                        e = tmpVec[ l ];
                                    }
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

                if ( breakLoop )
                {
                    break;
                }
            }
        }

        if ( !this.fireGenerated && doAnimate )
        {
            for ( var i = 0; i < 3; i++ )
            {
                this.haveFireElementAt[i] = false;
            }
            this.canChangeBet = true;
        }

        if( doAnimate )
        {
            this.currentWin = this.currentBet * pay;
            if ( this.currentWin > 0 )
            {
                this.addWin = this.currentWin / 10;
                this.setCurrentWinAnimation();
            }

            this.checkAndSetAPopUp();
        }
        else
        {
            this.currentWinForcast = this.currentBet * pay;
        }

    },
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSFreeze[i]=this.physicalReelElements[idx];
                i++;
            }
        }
    },
    changeTotalProbabilities : function()
    {
        this.totalProbalities = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            //this.extractArrayFromSring();
            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[this.easyModeCode][i];
            }
        }
        else
        {
            for ( var i = 0; i < 36; i++ )
            {
                this.probabilityArray[this.easyModeCode][i] = reelsRandomizationArrayDJSFreeze[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSFreeze[this.easyModeCode][i];
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
    },

    generateResult : function( scrIndx )
    {
        var wildGeneratedCount = 0;
        for( ; ; ) {
            //generate:
            for (var i = 0; i < this.slots[scrIndx].displayedReels.length; i++) {
                if (!this.haveFireElementAt[i]) {
                    var num = this.prevReelIndex[i];

                    var tmpnum = 0;

                    while (num == this.prevReelIndex[i]) {
                        num = Math.floor(Math.random() * this.totalProbalities);
                    }
                    this.prevReelIndex[i] = num;
                    this.resultArray[0][i] = num;

                    for (var j = 0; j < 36; j++) {
                        tmpnum += this.probabilityArray[this.easyModeCode][j];

                        if (tmpnum >= this.resultArray[0][i]) {
                            this.resultArray[0][i] = j;
                            break;
                        }
                    }
                    //resultArray[0][i] = 17;hack
                     /*this.resultArray[0][0] = 2;
                     this.resultArray[0][1] = 0;
                     this.resultArray[0][2] = 0;*/
                    if (this.reelsArrayDJSFreeze[this.resultArray[0][i]] == ELEMENT_X) {
                        this.fireGenerated = true;
                        wildGeneratedCount++;
                    }
                }
            }

            if (wildGeneratedCount > 1) {
                wildGeneratedCount = 0;
                this.fireGenerated = false;
                //goto
                //generate;
            }
            else
            {
                break;
            }
        }

        /*/HACK
         this.resultArray[0][0] = 2;
         this.resultArray[0][1] = 34;
         this.resultArray[0][2] = 34;
         //*///HACK
        var i;

        for ( i = 0; i < 3; i++ )
        {
            if ( this.haveFireElementAt[i] )
            {
                break;
            }
        }
        if ( i >= 3 )
        {
            this.calculatePayment( false );
            this.checkForFalseWin();
        }

        /*cc.log( "resultArray[0][0] = " + this.resultArray[0][0] );
        cc.log( "resultArray[0][1] = " + this.resultArray[0][1] );
        cc.log( "resultArray[0][2] = " + this.resultArray[0][2] );*/
    },

    removeFlamesAnimation : function()
    {
        for ( var i = FLAME_ANIM_TAG; i < FLAME_ANIM_TAG + 3; i++ )
        {
            while ( this.getChildByTag( i ) )
            {
                this.removeChildByTag( i );
            }
        }
    },

    rollUpdator : function(dt)
    {
        var i;
        for (i = 0; i < this.totalSlots; i++)
        {
            this.slots[i].updateSlot(dt);
        }

        for (i = 0; i < this.totalSlots; i++)
        {
            if ( this.slots[i].isSlotRunning() )
            {
                break;
            }
        }

        if (i >= this.totalSlots)
        {
            this.isRolling = false;
            this.unschedule( this.rollUpdator );
            this.calculatePayment( true );

            if (this.isAutoSpinning)
            {
                if (this.currentWin > 0)
                {
                    this.scheduleOnce( this.autoSpinCB, 1.5);
                }
                else
                {
                    this.scheduleOnce( this.autoSpinCB, 1.0);
                }
            }
        }
    },

    setFalseWin : function()
    {
        if ( !this.canChangeBet )
        {
            return;
        }
        var highWinIndices = [ 0, 18, 30 ];
        var missIndices = [ 2, 12 ];

        var missedIndx = Math.floor( Math.random() * 3 );

        if( missedIndx == 2 )
        {
            if( Math.floor( Math.random() * 10 ) == 3 )
            {
            }
            else
            {
                missedIndx = Math.floor( Math.random() * 2 );
            }
        }

        var sameIndex = Math.floor( Math.random( ) * highWinIndices.length );

        for ( var i = 0; i < 3; i++ )
        {
            if ( i == missedIndx )
            {
                this.resultArray[0][i] = this.getRandomIndex( missIndices[ Math.floor( Math.random( ) * missIndices.length ) ], true );
            }
            else
            {
                this.resultArray[0][i] = this.getRandomIndex( highWinIndices[ sameIndex ], false );
            }
        }
    },

    setFreezeAnimation : function( laneCode )
    {
        if ( this.getChildByTag( FLAME_ANIM_TAG + laneCode ) )
        {
            return;
        }

        var r = this.slots[0].blurReels[ laneCode ];

        var node = r.getParent();

        var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
        diff*=( 2 - laneCode );

        var freezeSprite = null;//Sprite::createWithSpriteFrameName( "freeze.png" );
        if ( laneCode == 0 )
        {
            freezeSprite = new cc.Sprite( "#freeze_side.png" );
        }
        else if ( laneCode == 1 )
        {
            freezeSprite = new cc.Sprite( "#freeze.png" );
        }
        else
        {
            freezeSprite = new cc.Sprite( "#freeze_side.png" );
            freezeSprite.setFlippedX( true );
        }
        freezeSprite.setPosition( node.getPositionX() + this.slots[0].getPositionX() - diff, node.getPositionY() + this.slots[0].getPositionY() );
        this.addChild( freezeSprite, 0, FLAME_ANIM_TAG + laneCode );

        freezeSprite.setOpacity( 0.0 );
        freezeSprite.runAction( new cc.FadeIn( 1.5 ) );

        this.scaleAccordingToSlotScreen( freezeSprite );
        this.delegate.jukeBox( S_ICE_FREEZE );
    },

    setResult : function( indx, lane, sSlot )
    {
        var gap = 387;

        var j = 0;
        var animOriginY;

        var r = this.slots[0].physicalReels[lane];
        var arrSize = r.elementsVec.length;
        var spr;

        var spriteVec = new Array();

        animOriginY = gap * r.direction;
        spr = r.elementsVec[indx];
        if (spr.eCode == ELEMENT_EMPTY)
        {
            gap = 285 * spr.getScale();
        }

        if (indx == 0)
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push(spr);
            if ( !this.haveFireElementAt[lane] )
                spr.setPositionY(animOriginY);

            for (var i = 0; i < 2; i++)
            {
                spr = r.elementsVec[i];
                spriteVec.push(spr);
                if ( !this.haveFireElementAt[lane] )
                    spr.setPositionY(animOriginY + gap * (i + 1) * r.direction);
            }
        }
        else if (indx == arrSize - 1)
        {
            spr = r.elementsVec[0];
            spriteVec.push(spr);
            if ( !this.haveFireElementAt[lane] )
                spr.setPositionY(animOriginY);
            for (var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i--)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                if ( !this.haveFireElementAt[lane] )
                    spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push(spr);
            if ( !this.haveFireElementAt[lane] )
                spr.setPositionY(animOriginY);

            for (var i = indx; i < indx + 2; i++)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                if ( !this.haveFireElementAt[lane] )
                    spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }

        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            gap = -773.5;

            var eee;
            if (i < 2)
            {
                eee = spriteVec[i + 1];
            }
            if (i == 0)
            {
                var ee = spriteVec[i + 1];
                if ( Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) ) != 387 )
                {
                    gap = - Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) * 2 * ee.getScale() );
                }
            }

            gap*=r.direction;
            var ease;

            var time = 0.3;

            if ( this.slots[sSlot].isForcedStop )
            {
                ease = new cc.MoveTo( 0.15, cc.p( 0, e.getPositionY() + gap ) ).easing( cc.easeBackOut() );
                time = 0.15;
            }
            else
            {
                ease = new cc.MoveTo( 0.3, cc.p( 0, e.getPositionY() + gap ) ).easing( cc.easeBackOut() );
            }

            ease.setTag( MOVING_TAG );
            if ( !this.haveFireElementAt[lane] )
                e.runAction(ease);

            if ( this.reelsArrayDJSFreeze[ indx ] == ELEMENT_X )
            {
                var node = new cc.Node();//lambda

                node.runAction( new cc.Sequence( new cc.DelayTime( time ),
                    cc.callFunc( function( ){
                        node.removeFromParent( true );
                        this.setFreezeAnimation( lane );
                    }, this, lane, node ) ) );
                this.addChild( node );
            }
        }

        return spriteVec;
    },

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSFreeze;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    spin : function()
    {

        this.unschedule( this.updateWinCoin );

        this.deductBet( this.currentBet );
        if (this.displayedScore != this.totalScore)
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;
        this.fireGenerated = false;
        this.currentWin = 0;
        this.currentWinForcast = 0;

        this.startLightAnimation();

        var i = 0;
        for( i = 0; i < 3; i++ )
        {
            if ( this.haveFireElementAt[i] )
            {
                break;
            }
        }

        if ( i >= 3 )
        {
            this.removeFlamesAnimation();
        }

        this.startRolling();

        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    },

    setWinAnimation : function( animCode, screen )
    {
        var fire;
        for ( var i = 0; i < this.animNodeVec.length; i++ )
        {
            fire = this.animNodeVec[ i ];
            fire.removeFromParent( true );
        }
        this.animNodeVec = [];


        switch ( animCode )
        {
            case WIN_TAG:
            {
                var r = this.slots[0].blurReels[1];

                var node = r.getParent();

                var bgSprite = new cc.Sprite( "res/DJSFreeze/low_win_bg.png" );
                bgSprite.setPosition( cc.p( this.vSize.width - bgSprite.getContentSize().width / 2, node.getPositionY()  + this.slots[0].getPositionY() ) );

                this.addChild( bgSprite );
                this.animNodeVec.push( bgSprite );

                var fgSprite = new cc.Sprite( "res/DJSFreeze/low_win_bg.png" );
                fgSprite.setPosition( bgSprite.getPosition() );
                fgSprite.setOpacity( 0 );
                fgSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeIn( 0.1 ), new cc.FadeOut( 0.1 ) ) ) );
                this.addChild( fgSprite );
                this.animNodeVec.push( fgSprite );
                this.scaleAccordingToSlotScreen( bgSprite );
                this.scaleAccordingToSlotScreen( fgSprite );

            }
                break;

            case BIG_WIN_TAG:
            {
                var r = this.slots[0].blurReels[1];

                var node = r.getParent();

                var bgSprite = new cc.Sprite( "res/DJSFreeze/glow.png" );
                bgSprite.setPosition( cc.p( this.vSize.width - bgSprite.getContentSize().width / 2, node.getPositionY() + this.slots[0].getPositionY() ) );
                bgSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeOut( 0.5 ), new cc.FadeIn( 0.5 ) ) ) );
                this.addChild( bgSprite );
                this.animNodeVec.push( bgSprite );

                var fgSprite = new cc.Sprite( "res/DJSFreeze/glow1.png" );
                fgSprite.setPosition( bgSprite.getPosition() );
                fgSprite.setOpacity( 0 );
                fgSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeIn( 0.5 ), new cc.FadeOut( 0.5 ) ) ) );
                this.addChild( fgSprite );
                this.animNodeVec.push( fgSprite );

                this.scaleAccordingToSlotScreen( bgSprite );
                this.scaleAccordingToSlotScreen( fgSprite );
            }
                break;

            default:
                break;
        }
    },

    startLightAnimation : function()
    {
        var sprite = this.getChildByTag( LIGHT_TAG );

        if ( !sprite )
        {
            var r = this.slots[0].blurReels[1];

            var node = r.getParent();

            sprite = new cc.Sprite( "res/DJSFreeze/lights.png" );
            sprite.setPosition( node.getPosition() );

            sprite.setTag( LIGHT_TAG );

            this.addChild( sprite, 4 );

        }

        sprite.setOpacity( 0 );
        sprite.runAction( new cc.Sequence( new cc.FadeIn( 0.2 ), new cc.FadeOut( 0.5 ) ) );

    }
});