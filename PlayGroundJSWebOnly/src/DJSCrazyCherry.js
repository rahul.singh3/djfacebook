/**
 * Created by RNF-Mac11 on 2/13/17.
 */


var reelsRandomizationArrayDJSCrazyCherry =
    [
        [// 85% payout
            20, 2, 20, 55, 30, 35,
            40, 60, 30, 10, 20, 50,
            25, 1, 10, 30, 30, 10,
            15, 30, 15, 40, 15, 25,
            10, 25, 10, 20, 30, 3,
            5, 30, 5, 20, 25, 1
        ],
        [//92 %
            20, 2, 20, 55, 30, 35,
            40, 60, 30, 10, 20, 50,
            25, 1, 10, 30, 30, 10,
            15, 30, 15, 50, 15, 35,
            10, 25, 10, 20, 30, 3,
            5, 30, 5, 20, 25, 2
        ],
        [// 96-97% payout
            20, 3, 20, 70, 30, 40,
            40, 60, 30, 10, 20, 63,
            25, 1, 10, 30, 30, 10,
            15, 30, 15, 40, 15, 25,
            10, 25, 10, 20, 30, 5,
            5, 30, 5, 20, 25, 2
        ],
        [//140% payout
            20, 8, 10, 70, 20, 40,
            30, 60, 20, 10, 10, 63,
            25, 3, 5, 30, 20, 10,
            10, 30, 10, 40, 10, 25,
            5, 25, 5, 20, 20, 5,
            3, 30, 3, 20, 25, 2
        ],
        [//300% payout
            20, 8, 10, 70, 20, 40,
            30, 60, 20, 10, 10, 63,
            25, 3, 5, 30, 20, 10,
            10, 30, 10, 40, 10, 25,
            5, 25, 5, 20, 20, 5,
            3, 30, 3, 20, 25, 2
        ]
    ];

var DJSCrazyCherry = GameScene.extend({
    reelsArrayDJSCrazyCherry:[],
    ctor:function ( level_index ) {
        this.reelsArrayDJSCrazyCherry = [
            /*1*/ELEMENT_EMPTY,/*2*/ ELEMENT_2X,/*3*/ ELEMENT_EMPTY,/*4*/ ELEMENT_BAR_3,/*5*/ ELEMENT_EMPTY,
            /*6*/ ELEMENT_BAR_2,/*7*/ELEMENT_EMPTY,/*8*/ ELEMENT_BAR_1,/*9*/ ELEMENT_EMPTY,/*10*/ ELEMENT_CHERRY,
            /*11*/ELEMENT_EMPTY,/*12*/ELEMENT_7_PINK,/*13*/ELEMENT_EMPTY,/*14*/ ELEMENT_2X,/*15*/ELEMENT_EMPTY,
            /*16*/ELEMENT_BAR_1,/*17*/ ELEMENT_EMPTY,/*18*/ ELEMENT_CHERRY,/*19*/ELEMENT_EMPTY,/*20*/ ELEMENT_BAR_2,
            /*21*/ ELEMENT_EMPTY,/*22*/ ELEMENT_BAR_3,/*23*/ELEMENT_EMPTY,/*24*/ELEMENT_7_PINK,/*25*/ELEMENT_EMPTY,
            /*26*/ ELEMENT_BAR_1,/*27*/ ELEMENT_EMPTY,/*28*/ ELEMENT_BAR_2,/*29*/ELEMENT_EMPTY,/*30*/ ELEMENT_CHERRY,
            /*31*/ELEMENT_EMPTY,/*32*/ELEMENT_BAR_2,/*33*/ELEMENT_EMPTY,/*34*/ ELEMENT_BAR_1,/*35*/ ELEMENT_EMPTY,
            /*36*/ ELEMENT_2X
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSCrazyCherry;
        this.physicalArrayCheck= this.reelsArrayDJSCrazyCherry;
        this._super( level_index );

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;


    },

    calculatePayment : function( doAnimate )
    {
        var payMentArray= [

        /*0*/    500,// Triple Wild

        /*1*/    75, // 7 7 7 Red

        /*2*/    35,  // Triple Bar

        /*3*/    25,  // double Bar

        /*4*/    15,  // single Bar

        /*5*/    5,   // Any Bar

        /*6*/    6,   // Triple Cherry

        /*7*/    4,   // Double Cherry

        /*8*/    2   // Single Cherry

    ];

        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        for ( var i = 0; i < this.slots[0].resultReel.length; i++ )
        {
            tmpVector = this.slots[0].resultReel[i];
            tmpVector2.push( tmpVector[1] );
        }

        this.lineVector.push( tmpVector2 );

        var lineLength = 3;
        var winScenario = 7;
        var pay = 0;
        var tmpPay = 1;
        var count2X = 0;
        var countWild = 0;
        var countGeneral = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var i, j, k;
        var e = null;
        var pivotElm = null;
        var pivotElm2 = null;
        var tmpVec;

        for ( i = 0; i < this.totalLines; i++ )
        {
            tmpVec = this.lineVector[i];
            for ( k = 0; k < winScenario; k++ )
            {
                var breakLoop = false;
                countGeneral = 0;
                switch ( k )
                {
                    case 0://for all combination with wild
                        for ( j = 0; j < lineLength; j++ )
                        {
                            if ( this.reelsArrayDJSCrazyCherry[this.resultArray[i][j]] != -1 && this.reelsArrayDJSCrazyCherry[this.resultArray[i][j]] != ELEMENT_EMPTY )
                            {
                                switch ( this.reelsArrayDJSCrazyCherry[this.resultArray[i][j]] )
                                {
                                    case ELEMENT_2X:
                                        count2X++;

                                        countWild++;
                                        if ( doAnimate )
                                        {
                                            e = tmpVec[j];
                                        }
                                        if ( e )
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        switch ( pivotElement )
                                        {
                                            case -1:
                                                pivotElement = this.reelsArrayDJSCrazyCherry[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate )
                                                {
                                                    e = tmpVec[j];
                                                }
                                                if ( e )
                                                {
                                                    pivotElm = e;
                                                }
                                                break;

                                            default:
                                                pivotElement2 = this.reelsArrayDJSCrazyCherry[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate )
                                                {
                                                    e = tmpVec[j];
                                                }
                                                if ( e )
                                                {
                                                    pivotElm2 = e;
                                                }
                                                break;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                var cherCount = 0;
                                var wCount = 0;

                                for (var a = 0; a < lineLength; a++)
                                {
                                    switch ( this.reelsArrayDJSCrazyCherry[this.resultArray[i][a]] )
                                    {
                                        case ELEMENT_2X:
                                            wCount++;

                                            if ( doAnimate )
                                            {
                                                e = tmpVec[a];
                                            }
                                            if ( e )
                                            {
                                                e.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_CHERRY:
                                            cherCount++;
                                            if ( doAnimate )
                                            {
                                                e = tmpVec[a];
                                            }
                                            if ( e )
                                            {
                                                e.aCode = SCALE_TAG;
                                            }
                                            break;
                                    }
                                }

                                if ( ( !wCount && cherCount == 2 ) )
                                {
                                    pay = payMentArray[7];
                                    breakLoop = true;
                                }
                                else if( cherCount && wCount )
                                {
                                    pay = 2 * payMentArray[7];
                                    breakLoop = true;
                                }
                                break;
                            }
                        }

                        if ( j == 3 )
                        {
                            if ( count2X == 3 )
                            {
                                pay = payMentArray[0];
                                breakLoop = true;
                            }
                            else if( countWild >= 2 )
                            {
                                breakLoop = true;
                                tmpPay = 4;
                                switch ( pivotElement )
                                {
                                    case ELEMENT_7_PINK:
                                        pay = tmpPay * payMentArray[1];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_3:
                                        pay = tmpPay * payMentArray[2];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_2:
                                        pay = tmpPay * payMentArray[3];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_1:
                                        pay = tmpPay * payMentArray[4];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_CHERRY:
                                        pay = tmpPay * payMentArray[6];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        pay = tmpPay;
                                        if ( pivotElm )
                                        {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;
                                }
                            }
                            else if( countWild == 1 )
                            {
                                breakLoop = true;
                                tmpPay = 2;
                                if ( pivotElement == pivotElement2 )
                                {
                                    switch ( pivotElement )
                                    {
                                        case ELEMENT_7_PINK:
                                            pay = tmpPay * payMentArray[1];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_3:
                                            pay = tmpPay * payMentArray[2];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_2:
                                            pay = tmpPay * payMentArray[3];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_1:
                                            pay = tmpPay * payMentArray[4];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_CHERRY:
                                            pay = tmpPay * payMentArray[6];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        default:
                                            pay = tmpPay;
                                            break;
                                    }
                                }
                                else
                                {
                                    if( ( pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_BAR_3 ) &&
                                        ( pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_BAR_3 ) )
                                    {
                                        pay = tmpPay * payMentArray[5];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if( pivotElement == ELEMENT_CHERRY )
                                    {
                                        pay = tmpPay * payMentArray[7];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if( pivotElement2 == ELEMENT_CHERRY )
                                    {
                                        pay = tmpPay * payMentArray[7];
                                        if ( doAnimate ) {
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                }
                            }
                            else// no wild element calculation
                            {
                            }
                        }
                        break;

                    case 1: // 7 7 7 Pink
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSCrazyCherry[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_PINK:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[1];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }

                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 2: // 3Bar 3Bar 3Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSCrazyCherry[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[2];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 3: // 2Bar 2Bar 2Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSCrazyCherry[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[3];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 4: // Bar Bar Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSCrazyCherry[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[4];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 5: // any Bar combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSCrazyCherry[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[5];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 6: // Cherry
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSCrazyCherry[this.resultArray[i][j]] )
                            {
                                case ELEMENT_CHERRY:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[6];
                            breakLoop = true;
                        }
                        else if ( countGeneral == 2 )
                        {
                            pay = payMentArray[7];
                            breakLoop = true;
                        }
                        if ( countGeneral == 1 )
                        {
                            pay = payMentArray[8];
                            breakLoop = true;
                        }

                        for ( var l = 0; l < 3; l++ ) {
                        if ( doAnimate ) {
                            e = tmpVec[l];
                        }
                        if ( e && e.eCode == ELEMENT_CHERRY )
                        {
                            e.aCode = SCALE_TAG;
                        }
                    }
                        break;

                    default:
                        break;
                }

                if ( breakLoop )
                {
                    break;
                }
            }
        }

        if ( doAnimate )
        {
            this.currentWin = this.currentBet * pay;
            if ( this.currentWin > 0 )
            {
                this.addWin = this.currentWin / 10;
                this.setCurrentWinAnimation();
            }
            this.checkAndSetAPopUp();
        }
        else
        {
            this.currentWinForcast = this.currentBet * pay;
        }
    },
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSCrazyCherry[i]=this.physicalReelElements[idx];
                i++;
            }
        }
    },
    changeTotalProbabilities : function()
    {
        this.totalProbalities = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            //this.extractArrayFromSring();
            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[this.easyModeCode][i];
            }
        }
        else
        {
            for ( var i = 0; i < 36; i++ )
            {
                this.probabilityArray[this.easyModeCode][i] = reelsRandomizationArrayDJSCrazyCherry[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSCrazyCherry[this.easyModeCode][i];
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
        if ( ( ( this.reelsArrayDJSCrazyCherry[this.resultArray[0][0]] == ELEMENT_7_PINK || this.reelsArrayDJSCrazyCherry[this.resultArray[0][0]] == ELEMENT_2X ) &&
            ( this.reelsArrayDJSCrazyCherry[this.resultArray[0][1]] == ELEMENT_7_PINK || this.reelsArrayDJSCrazyCherry[this.resultArray[0][1]] == ELEMENT_2X ) ) ||
            ( ( this.reelsArrayDJSCrazyCherry[this.resultArray[0][0]] == ELEMENT_BAR_3 || this.reelsArrayDJSCrazyCherry[this.resultArray[0][0]] == ELEMENT_2X ) &&
            ( this.reelsArrayDJSCrazyCherry[this.resultArray[0][1]] == ELEMENT_BAR_3 || this.reelsArrayDJSCrazyCherry[this.resultArray[0][1]] == ELEMENT_2X ) )||
            ( this.reelsArrayDJSCrazyCherry[this.resultArray[0][0]] == ELEMENT_BAR_2 && this.reelsArrayDJSCrazyCherry[this.resultArray[0][1]] == ELEMENT_2X ) ||
            ( this.reelsArrayDJSCrazyCherry[this.resultArray[0][1]] == ELEMENT_BAR_2 && this.reelsArrayDJSCrazyCherry[this.resultArray[0][0]] == ELEMENT_2X ) )
        {
            if ( !this.slots[0].isForcedStop )
            {
                this.schedule( this.reelStopper, 4.8 );

                this.delegate.jukeBox( S_EXCITEMENT );

                var glowSprite = new cc.Sprite( glowStrings[0] );

                if ( glowSprite )
                {
                    var r = this.slots[0].physicalReels[ this.slots[0].physicalReels.length - 1 ];

                    glowSprite.setPosition( r.getParent().getPosition().x + this.slots[0].getPosition().x, r.getParent().getPosition().y + this.slots[0].getPosition().y );

                    //this.scaleAccordingToSlotScreen( glowSprite );

                    this.addChild( glowSprite, 5 );
                    this.animNodeVec.push( glowSprite );

                    glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ) ) );
                }
            }
        }
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
        var tmp = this.slots[0].resultReel[laneIndex];

        var e = tmp[1];

        switch ( e.eCode )
        {
            case ELEMENT_7_RED:
                if ( laneIndex != 0 )
                {
                    break;
                }
            case ELEMENT_2X:
            case ELEMENT_3X:
            case ELEMENT_4X:
            case ELEMENT_5X:
                e.runAction( new cc.Repeat( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ), 2 ) );
                this.delegate.jukeBox( S_FLASH );
                break;

            default:
                break;
        }

    },

    generateResult : function( scrIndx )
    {
        for ( var i = 0; i < this.slots[scrIndx].displayedReels.length; i++ )
        {
            //generate:
            var num = parseInt( this.prevReelIndex[i] );

            var tmpnum = 0;

            while ( num == parseInt( this.prevReelIndex[i] ) )
            {
                num = Math.floor( Math.random() * this.totalProbalities );

            }
            this.prevReelIndex[i] = num;
            this.resultArray[0][i] = num;

            for (var j = 0; j < 36; j++)
            {
                tmpnum+=this.probabilityArray[this.easyModeCode][j];

                if (tmpnum >= this.resultArray[0][i])
                {
                    this.resultArray[0][i] = j;
                    break;
                }
            }
            //console.log( " rand =  " + num + " index = " + this.resultArray[0][i] );
        }

        /*
         int highPay[][3] = {
         {15, 21, 15}, //2x 5x 2x
         {15, 19, 15}, //2x 4x 2x
         {15, 17, 15}, //2x 3x 2x
         {7, 7, 7},    //Triple 7 Red
         {5, 5, 5},    //Triple 7 Yellow
         {29, 29, 29}, //Triple 7 Green
         {1, 1, 1},    //Triple 7 Bar
         {31, 31, 31}, //Triple 3Bar
         {27, 27, 27}, //Triple 2Bar
         {23, 23, 23}, //Triple Bar
         };*/
        /*/HACK
         this.resultArray[0][0] = 0;
         this.resultArray[0][1] = 1;
         this.resultArray[0][2] = 2;
         //*///HACK

        this.calculatePayment( false );
        this.checkForFalseWin();

    },

    rollUpdator : function(dt)
    {
        var i;
        for (i = 0; i < this.totalSlots; i++)
        {
            this.slots[i].updateSlot(dt);
        }

        for (i = 0; i < this.totalSlots; i++)
        {
            if ( this.slots[i].isSlotRunning() )
            {
                break;
            }
        }

        if (i >= this.totalSlots)
        {
            this.isRolling = false;
            this.unschedule( this.rollUpdator );
            this.calculatePayment( true );

            if (this.isAutoSpinning)
            {
                if (this.currentWin > 0)
                {
                    this.scheduleOnce( this.autoSpinCB, 1.5);
                }
                else
                {
                    this.scheduleOnce( this.autoSpinCB, 1.0);
                }
            }
        }
    },

    setFalseWin : function()
    {
        var highWinIndices = [ 1, 3, 5, 11 ];
        var missIndices = [ 1, 3, 5, 11 ];

        var missedIndx = Math.floor( Math.random() * 3 );

        if( missedIndx == 2 )
        {
            if( Math.floor( Math.random() * 10 ) == 3 )
            {
            }
            else
            {
                missedIndx = Math.floor( Math.random() * 2 );
            }
        }

        var sameIndex = Math.floor( Math.random( ) * highWinIndices.length );

        for ( var i = 0; i < 3; i++ )
        {
            if ( i == missedIndx )
            {
                this.resultArray[0][i] = this.getRandomIndex( missIndices[ Math.floor( Math.random( ) * missIndices.length ) ], true );
            }
            else
            {
                this.resultArray[0][i] = this.getRandomIndex( highWinIndices[ sameIndex ], false );
            }
        }
    },

    setResult : function( indx, lane, sSlot )
    {
        var gap = 387.0;

        var j = 0;
        var animOriginY;

        var r = this.slots[0].physicalReels[lane];
        var arrSize = r.elementsVec.length;
        var spr;

        var spriteVec = new Array();

        animOriginY = gap * r.direction;
        spr = r.elementsVec[indx];
        if ( spr.eCode == ELEMENT_EMPTY )
        {
            gap = 285 * spr.getScale();
        }

        if ( indx == 0 )
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push(spr);

            spr.setPositionY( animOriginY );

            for (var i = 0; i < 2; i++)
            {
                spr = r.elementsVec[i];
                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (i + 1) * r.direction);
            }
        }
        else if (indx == arrSize - 1)
        {
            spr = r.elementsVec[0];
            spriteVec.push(spr);

            spr.setPositionY(animOriginY);
            for (var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i--)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction );
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push(spr);
            spr.setPositionY(animOriginY);

            for (var i = indx; i < indx + 2; i++)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }

        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            gap = -773.5;

            var eee;
            if (i < 2)
            {
                eee = spriteVec[i + 1];
            }
            if (i == 0)
            {
                var ee = spriteVec[i + 1];
                if ( Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) ) != 387)
                {
                    gap = -Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) * 2 * ee.getScale() );
                }
            }

            gap*=r.direction;
            var ease;

            if ( this.slots[sSlot].isForcedStop )
            {
                ease =  new cc.MoveTo( 0.15, cc.p( 0, e.getPositionY() + gap) ).easing( cc.easeBackOut() );
            }
            else
            {
                ease = new cc.MoveTo( 0.3, cc.p( 0, e.getPositionY() + gap ) ).easing( cc.easeBackOut() );
            }

            ease.setTag( MOVING_TAG );
            e.runAction(ease);
        }

        return spriteVec;
    },

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSCrazyCherry;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    spin : function()
    {

        this.unschedule( this.updateWinCoin );

        this.deductBet( this.currentBet );
        if (this.displayedScore != this.totalScore)
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;
        this.currentWin = 0;
        this.currentWinForcast = 0;
        this.startRolling();

        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    }

});