PopUps = (function () {
    var instance;
    function init() {
     return{
         listOfPopUps:[],


         showPopUpAccToIndex:function (layerindex) {
             var delegate = AppDelegate.getInstance();
             var scene = cc.director.getRunningScene();
             var layer = scene.getChildByTag( sceneTags.LOBBY_SCENE_TAG );
              if (layer || scene.getTag() == sceneTags.LOBBY_SCENE_TAG) {
                  if (delegate.showingScreen)
                      return;

                  switch (layerindex) {
                      case PopUpTags.STAY_IN_TOUCH_INSTANT_DEAL: {
                          delegate.showingScreen=true;
                          layer.setInstantDealPop(0);
                      }
                          break;
                      case PopUpTags.STAY_TOUCH_BUILD_UPDATE_POP:

                          break;
                      case PopUpTags.STAY_TOUCH_AREARS_POP:
                          layer.setArearsMessage(UserDef.getInstance().getValueForKey(AREARS,0));
                          break;
                      case PopUpTags.STAY_TOUCH_DAILY_BONUS_POP: {
                          layer.setDailyBonusPage();
                      }
                      break;
                      case PopUpTags.STAY_IN_TOUCH_LIKE_PAGE:
                      {
                          layer.setLikePage();
                      }
                      break;
                      case PopUpTags.STAY_TOUCH_ONE_LINK_POP:
                      {
                          if (layer.getChildByTag(PopUpTags.STAY_IN_TOUCH_OFFER_PAGE)){
                              delegate.showingScreen = false;
                              return;
                          }
                          layer.setOfferPopup();
                      }
                          break;
                      case PopUpTags.STAY_IN_TOUCH_ACCEPT_REWARDS:
                      {
                          layer.setFBInvitationAcceptionRewardsPopUp();
                      }
                          break;
                      case PopUpTags.STAY_TOUCH_DEAL_POP:
                      {
                          layer.setDealOffer();
                      }
                          break;
                      case PopUpTags.STAY_TOUCH_HAPPY_HOUR_POP:
                      {
                          layer.setHappyHoursPop();
                      }
                          break;
                      case PopUpTags.STAY_TOUCH_SPECIAL_OFFER_POP:
                      {
                          layer.setSpecialOfferPop();
                      }
                          break;
                      case PopUpTags.STAY_IN_TOUCH_GIFT_POP:
                      {
                          layer.setGiftPage();
                      }
                          break;
                  }
              }else{
                  layer = scene;//.getChildByTag( sceneTags.GAME_SCENE_TAG );
                  if ( layer.getTag() === sceneTags.GAME_SCENE_TAG ) {
                      if (delegate.showingScreen)
                          return;

                      var game = layer;
                      switch (layerindex) {
                          case PopUpTags.STAY_TOUCH_SPECIAL_OFFER_POP:
                          {
                              delegate.showingScreen = true;
                              game.setSpecialOfferPop();
                          }
                              break;
                          case PopUpTags.STAY_TOUCH_ONE_LINK_POP:
                          {
                              delegate.showingScreen = true;

                              game.setOfferPopup();
                          }
                              break;
                          case PopUpTags.STAY_IN_TOUCH_MACHINE_UNLOCK:
                          {
                              delegate.showingScreen = true;

                              game.setMachineUnlockedPopUp();
                          }
                              break;
                          case PopUpTags.STAY_TOUCH_BRIBE_POP:
                          {
                              delegate.showingScreen = true;

                              game.setAppRatingPopUp();
                          }
                              break;

                      }

                  }else {
                      var delegate =AppDelegate.getInstance();
                      delegate.showingScreen = false;
                  }
              }
         },

         addPopUpIndex:function(popIndex)
        {
            if(this.listOfPopUps && this.listOfPopUps.length === 0 && PopUpTags.STAY_IN_TOUCH_INSTANT_DEAL === popIndex) {
                PopUps.getInstance().showPopUpAccToIndex(popIndex);
            }
            this.listOfPopUps.push(popIndex);
        },
         removePopUPIndex:function(popIndex)
        {
            if (this.listOfPopUps && this.listOfPopUps.length>0){
                if (this.listOfPopUps.includes(popIndex)){
                    for( var i = 0; i < this.listOfPopUps.length; i++){ if ( this.listOfPopUps[i] === popIndex) { this.listOfPopUps.splice(i, 1); i--; }}
                }
            }
            if (this.listOfPopUps && this.listOfPopUps.length>0){
                this.showPopUpAccToIndex(this.listOfPopUps[0]);
            }else{
                var delegate =AppDelegate.getInstance();
                delegate.showingScreen = false;
            }
        }
     };

    };
    return {
        getInstance: function () {
            if (!instance) {
                instance = init();
            }
            return instance;
        }
    };
})();
