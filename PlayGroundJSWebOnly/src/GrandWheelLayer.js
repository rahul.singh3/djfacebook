var paidGrandWheelProbArr =
    [
        15/*50k*/, 1/*750k*/, 4/*100k*/, 2/*350k*/, 4/*150k*/, 3/*100k*/, 5/*250k*/, 15/*50k*/, 2/*500k*/, 3/*100k*/, 2/*750k*/, 3/*150k*/, 1/*1Mil*/, 5/*50k*/, 1/*2Mil*/,
    ];

var grandWheelArr =
    [
        [300, 500, 750, 1000, 300, 1200, 1500, 300, 3000, 500, 5000, 300, 7000, 500, 10000],//1062/1000
        [1500, 2500, 3500, 5000, 1500, 6000, 7500, 1500, 15000, 2500, 25000, 1500, 30000, 2500, 40000],//4025/4000
        [3000, 5000, 7500, 10000, 3000, 12000, 15000, 3000, 30000, 5000, 50000, 3000, 70000, 5000, 100000],//10621/10000
        [15000, 25000, 35000, 50000, 15000, 60000, 75000, 15000, 150000, 25000, 250000, 15000, 300000, 25000, 400000],//40259/40000
        [30000, 50000, 75000, 100000, 30000, 120000, 150000, 30000, 300000, 50000, 500000, 30000, 700000, 50000, 1000000],//106214/100000
        [50000, 75000, 100000, 150000, 50000, 150000, 200000, 350000, 450000, 700000, 500000, 350000, 1000000, 70000, 2000000],//194062/200000
    ];

var grandWheelProbArr =
    [
        [25, 10, 10, 10, 10, 8, 6, 10, 4, 10, 5, 12, 3, 15, 2],
        [25, 10, 10, 5, 10, 8, 6, 15, 4, 10, 2, 12, 2, 15, 1],
        [25, 10, 10, 10, 10, 8, 6, 10, 4, 10, 5, 12, 3, 15, 2],
        [25, 10, 10, 5, 10, 8, 6, 15, 4, 10, 2, 12, 2, 15, 1],
        [25, 10, 10, 10, 10, 8, 6, 10, 4, 10, 5, 12, 3, 15, 2],
        [30, 10, 10, 10, 25, 10, 10, 10, 4, 5, 5, 12, 3, 15, 1],
    ];

GrandWheelLayer = cc.Node.extend({

    ctor:function ( w, p ) {
        this._super();

        this.create( w ,p );
        return true;

    },
    curBonusIndx : 0,

    wheelCode : 0,

    payWheelGeneratedIndex : 0,

    paidSpinItem : null,

    payWheelCoins : 0,

    pParent : null,
     spr :null,

    create:function ( w, p ) {

        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 220 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        this.addChild( darkBG );

        this.pParent = p;
        this.wheelCode = w;
        if( this.wheelCode == IN_GAME_WHEEL )
            this.setInGameGrandWheel();
        else
            this.setPaidGrandWheel();
        //setGrandWheel();
        this.setTag( this.wheelCode );
    },
    generatePayWheelIndex : function()
    {
        var totalProb = 0;
        var arrSize = 15;//sizeof(paidGrandWheelProbArr)/sizeof(paidGrandWheelProbArr[0]);
        for( var i = 0; i < arrSize; i++ )
        {
            totalProb+=paidGrandWheelProbArr[i];
        }

        this.payWheelGeneratedIndex = Math.floor( Math.random() * totalProb );
        totalProb = 0;
        for ( var i = 0; i < arrSize; i++ )
        {
            totalProb+=paidGrandWheelProbArr[i];

            if ( totalProb >= this.payWheelGeneratedIndex )
            {
                this.payWheelGeneratedIndex = i;
                break;
            }
        }

        var spinArr = [ 50000, 750000, 100000, 350000, 150000, 100000, 250000, 50000, 500000, 100000, 750000, 150000, 1000000, 50000, 2000000 ];
        this.payWheelCoins = spinArr[this.payWheelGeneratedIndex];
    },
    getMultiplierForInGameWheel : function()
    {
        if( !this.pParent )   return 0;
        var betAmount = UserDef.getInstance().getValueForKey( GRAND_WHEEL_BET_AMOUNT );

        if( betAmount > ONE_MILLION * 10 * this.pParent.grandWheel.factor )
        {
            return 5;
        }
        else if( betAmount > ONE_MILLION * 5 * this.pParent.grandWheel.factor )
        {
            return 4;
        }
        else if( betAmount > ONE_MILLION * 2 * this.pParent.grandWheel.factor )
        {
            return 3;
        }
        else if( betAmount > 500000.0 * this.pParent.grandWheel.factor )
        {
            return 2;
        }
        else if( betAmount > 200000.0 * this.pParent.grandWheel.factor )
        {
            return 1;
        }
        else
        {
            return 0;
        }
    },
    menuCB : function( pSender)
    {
        var delegate = AppDelegate.getInstance();

        var menuItm = pSender;
        var tag = menuItm.getTag();

        switch (tag)
        {
            case BACK_TAG:
                this.removeFromParent( true );
                break;

            case SPIN_BTN_TAG:
                menuItm.setEnabled( false );
                this.startSpin();
                break;

            default:
                break;
        }
        delegate.jukeBox( S_BTN_CLICK );

    },
    payMenuCB : function( pSender)
    {
        var delegate = AppDelegate.getInstance();

        var menuItm = pSender;
        var tag = menuItm.getTag();

        var sprite = ( this.getChildByTag( D_NODE_TAG ) ? this.getChildByTag( D_NODE_TAG ).getChildByTag( WHEEL_TAG ) : this.getChildByTag( LAYER_TAG ).getChildByTag( D_NODE_TAG ).getChildByTag( WHEEL_TAG ) );

        switch (tag)
        {
            case BACK_TAG:
            case CLOSE_BTN_TAG://#Change Dec 13 as per directed by Raghib
                if(this.spr){
                    DPUtils.getInstance().easyBackInAnimToPopup(this.spr, 0.3);
                    this.runAction(new cc.Sequence(new cc.DelayTime(0.3), cc.callFunc(function () {
                        this.removeFromParent( true );
                        delegate.jukeBox( S_STOP_MUSIC );
                        delegate.jukeBox( S_STOP_EFFECTS );
                        delegate.jukeBox( S_LOBBY, true );
                        delegate.showingScreen = false;

                        PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_TOUCH_DAILY_BONUS_POP);
                    }, this)));
                }else{
                    this.removeFromParent( true );
                    delegate.jukeBox( S_STOP_MUSIC );
                    delegate.jukeBox( S_STOP_EFFECTS );
                    delegate.jukeBox( S_LOBBY, true );
                    delegate.showingScreen = false;
                    PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_TOUCH_DAILY_BONUS_POP);
                }
                break;

            case SPIN_BTN_TAG:
                if ( sprite.getRotation() == 0 )
                {
                    if ( cc.director.getRunningScene().getChildByTag(LOADER_TAG))
                        return;

                    this.generatePayWheelIndex();
                    //this.startSpin();//hack
                    IAPHelper.getInstance().purchase( purchaseTags.PURCHASE_01_99, BuyPageTags.PAID_WHEEL_PURCHASE );
                }
                break;

            default:
                break;
        }
        delegate.jukeBox( S_BTN_CLICK );
    },
    setInGameGrandWheel : function()
    {
        var visibleSize = cc.winSize;

        var bgSprite = new cc.Sprite( res.GrandWheelBg );
        bgSprite.setPosition( cc.p( visibleSize.width / 2, visibleSize.height / 2 ) );

        var ab = 1;
        var degree = 40;
        for ( var i = 0; i < 2; i++ )
        {
            var lightSprite = new cc.Sprite( res.Wheel_Light_png );
            lightSprite.setAnchorPoint( cc.p( 0.5, 0 ) );
            lightSprite.setPosition( visibleSize.width / 2, 25 );
            this.addChild( lightSprite, 5 );
            lightSprite.setRotation( degree * ab );
            lightSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.RotateBy( 3.0, -degree * 2 * ab ), new cc.DelayTime( 0.1 ),
                new cc.RotateBy( 3.0, degree * 2 * ab ), new cc.DelayTime( 0.1 ) ) ) );

            lightSprite.setTag( LIGHT_TAG );

            ab = -1;
        }

        bgSprite.setTag( LAYER_TAG );
        DPUtils.getInstance().setTouchSwallowing( null, bgSprite );

        this.addChild( bgSprite, 5 );

        var wheelSprite = new cc.Sprite( "res/GrandWheel/" + this.getMultiplierForInGameWheel() + ".png" );
        wheelSprite.setPosition( cc.p( bgSprite.getContentSize().width / 2, bgSprite.getContentSize().height / 2 + 10 ) );
        wheelSprite.setTag( WHEEL_TAG );
        bgSprite.addChild( wheelSprite );

        var boundrySprite = new cc.Sprite( res.GrandWheelBoundry );
        boundrySprite.setPosition( wheelSprite.getPosition() );
        bgSprite.addChild( boundrySprite );


        //Sprite * spinItm = Sprite( "GrandWheel/btn.png" );
        var spinItm = new cc.MenuItemImage( res.GrandWheelButton, res.GrandWheelButton, this.menuCB, this );

        spinItm.setPosition( bgSprite.getContentSize().width / 2, bgSprite.getContentSize().height / 2 );
        spinItm.setTag( SPIN_BTN_TAG );

        var menu = new cc.Menu( spinItm );
        menu.setPosition( cc.p( 0, 0 ) );
        bgSprite.addChild( menu );

        bgSprite.setTag( D_NODE_TAG );

        DJSResizer.getInstance().setFinalScaling( bgSprite );
    },

    setPaidGrandWheel : function()
    {
        var delegate = AppDelegate.getInstance();
        delegate.jukeBox( S_STOP_MUSIC );
        delegate.jukeBox( S_STOP_EFFECTS );
        delegate.jukeBox( S_PAID_WHEEL_BG, true );

        var visibleSize = cc.winSize;

        this.spr = new cc.Sprite( res.paidGrandWheel_4 );
        this.spr.setPosition( cc.p( visibleSize.width / 2, visibleSize.height / 2 ) );
        this.spr.setTag( LAYER_TAG );

        this.addChild( this.spr, 5 );

        var bgSprite = new cc.Node();
        bgSprite.setPosition( cc.p( visibleSize.width / 2, visibleSize.height / 2 ) );


        //bgSprite.setTag( LAYER_TAG );
        DPUtils.getInstance().setTouchSwallowing( null, bgSprite );

        this.spr.addChild( bgSprite, 5 );



        var wheelSprite = new cc.Sprite( res.paidGrandWheel_wheel );
        wheelSprite.setPosition( cc.p( bgSprite.getContentSize().width / 2, bgSprite.getContentSize().height / 2  ) );
        wheelSprite.setTag( WHEEL_TAG );
        bgSprite.addChild( wheelSprite );

        var boundrySprite = new cc.Sprite( res.paidGrandWheel_boundry );
        boundrySprite.setPosition( wheelSprite.getPosition() );
        bgSprite.addChild( boundrySprite );

        var stopperSprite = new cc.Sprite( res.paidGrandWheel_stopper );
        stopperSprite.setPosition( bgSprite.getContentSize().width / 2, cc.rectGetMaxY(wheelSprite.getBoundingBox()) - 15 );
        stopperSprite.setTag( WHEEL_STOPPER_TAG );
        bgSprite.addChild( stopperSprite );


        this.paidSpinItem = new cc.MenuItemImage( res.paidGrandWheel_btn, res.paidGrandWheel_btn, this.payMenuCB, this );

        this.paidSpinItem.setPosition( wheelSprite.getPosition() );
        this.paidSpinItem.setTag( SPIN_BTN_TAG );

        var paidItemReplacement = new cc.Sprite(res.paidGrandWheel_replacement );
        paidItemReplacement.setPosition( wheelSprite.getPosition() );
        bgSprite.addChild( paidItemReplacement );

        var scaleBy = new cc.ScaleBy( 0.75 , 1.2 );
        this.paidSpinItem.runAction( new cc.RepeatForever( new cc.Sequence( scaleBy, scaleBy.reverse() ) ) );

         this.skipItm = new cc.MenuItemImage( res.Close_Buttpn_png, res.Close_Buttpn_png, this.payMenuCB, this );

        this.skipItm .setPosition( cc.p( visibleSize.width * 0.5 - this.skipItm .getContentSize().width * 0.5, visibleSize.height * 0.5 - this.skipItm .getContentSize().height * 0.5 ) );
        this.skipItm .setTag( CLOSE_BTN_TAG );

        var menu = new cc.Menu( this.paidSpinItem, this.skipItm  );
        menu.setPosition( cc.p( 0, 0 ) );
        bgSprite.addChild( menu, 5 );

        bgSprite.setTag( D_NODE_TAG );

        //DJSResizer.getInstance().setFinalScaling( bgSprite );

        DPUtils.getInstance().easyBackOutAnimToPopup(this.spr,0.5);
    },
     skipItm:null,
    setBuyPopUp : function()
    {
        var visibleSize = cc.winSize;

        var layer = new cc.LayerColor( cc.color( 0, 0, 0, 160 ) );
        this.addChild( layer, 10, PAID_WHEEL );

        DPUtils.getInstance().setTouchSwallowing( null, layer );

        var popUpSprite = new cc.Sprite( res.paidGrandWheel_paidWheelPaymentPopUp );
        popUpSprite.setPosition( cc.p( visibleSize.width * 0.5, visibleSize.height * 0.5 ) );
        layer.addChild( popUpSprite );

        var spinItm = new cc.MenuItemImage( res.paidGrandWheel_pay_button, res.paidGrandWheel_pay_button, this.payMenuCB, this );

        spinItm.setPosition( popUpSprite.getContentSize().width / 2, popUpSprite.getContentSize().height * 0.17 );
        spinItm.setTag( SPIN_BTN_TAG );

        this.skipItm = new cc.MenuItemImage( res.Close_Buttpn_png, res.Close_Buttpn_png, this.payMenuCB, this );

        this.skipItm.setPosition( cc.p( popUpSprite.getContentSize().width  - this.skipItm.getContentSize().width * 0.5, popUpSprite.getContentSize().height - this.skipItm.getContentSize().height * 0.5 ) );
        this.skipItm.setTag( BACK_TAG );

        var menu = new cc.Menu( spinItm, this.skipItm );
        menu.setPosition( cc.p( 0, 0 ) );
        popUpSprite.addChild( menu );

        DJSResizer.getInstance().setFinalScaling( popUpSprite );
    },
    startSpin : function()
    {
        var delegate = AppDelegate.getInstance();

        //var sprite = ( this.getChildByTag( D_NODE_TAG ) ? this.getChildByTag( D_NODE_TAG ).getChildByTag( WHEEL_TAG ) : this.getChildByTag( LAYER_TAG ).getChildByTag( D_NODE_TAG ).getChildByTag( WHEEL_TAG ) );
var sprite = null;
        if( this.getChildByTag( D_NODE_TAG ) )
        {
            sprite = this.getChildByTag( D_NODE_TAG ).getChildByTag( WHEEL_TAG );
        }
        else
        {
            sprite = this.getChildByTag( LAYER_TAG ).getChildByTag( D_NODE_TAG ).getChildByTag( WHEEL_TAG );
        }
        if ( sprite.getRotation() != 0 )
        {
            return;
        }

        var totalProb = 0;
        var arrSize = 15;//sizeof(grandWheelProbArr[0])/sizeof(grandWheelProbArr[0][0]);
        for( var i = 0; i < arrSize; i++ )
        {
            totalProb+=grandWheelProbArr[this.getMultiplierForInGameWheel()][i];
        }

         this.curBonusIndx = Math.floor( Math.random() * totalProb );
        totalProb = 0;
        for ( var i = 0; i < arrSize; i++ )
        {
            totalProb+=grandWheelProbArr[this.getMultiplierForInGameWheel()][i];

            if ( totalProb >= this.curBonusIndx )
            {
                this.curBonusIndx = i;
                break;
            }
        }

        if( this.wheelCode == PAID_WHEEL )
        {
            //var delegate = ( AppDelegate * )Application::getInstance();

            delegate.jukeBox( S_PAID_WHEEL_SPIN );

            this.curBonusIndx = this.payWheelGeneratedIndex;
            arrSize = 15;//sizeof(paidGrandWheelProbArr)/sizeof(paidGrandWheelProbArr[0]);

            /*var walkAnimFrames = [];
            var walkAnim = null;
            var spr = this.getChildByTag( LAYER_TAG );
            for ( var i = 1; i < 5; i++ )
            {
                var string = "res/paidGrandWheel/" + i + ".png";
                var tmp = new cc.SpriteFrame( string, cc.Rect( 0, 0, spr.getContentSize().width, spr.getContentSize().height ) );
                walkAnimFrames.push( tmp );
            }

            walkAnim = new cc.Animation( walkAnimFrames, 0.1 );
            walkAnim.setLoops(-1);
            var tmpAnimate = new cc.Animate( walkAnim );

            spr.runAction( tmpAnimate );*/
            var spr = this.getChildByTag( LAYER_TAG );
            var animFrames = [];
            for (var i = 1; i < 5; i++) {
                var str = "res/paidGrandWheel/" + i + ".png";
                var frame = new cc.SpriteFrame( str, cc.Rect( 0, 0, spr.getContentSize().width, spr.getContentSize().height ) );
                animFrames.push(frame);
            }
//3.create a animation with the spriteframe array along with a period time
            var animation = new cc.Animation(animFrames, 0.1);

            var action = cc.animate( animation );

            spr.runAction( action );

            this.paidSpinItem.setVisible( false );
            this.paidSpinItem.setEnabled( false );
        }
        else
        {
            delegate.jukeBox( S_BONUS_WIN, true );
            //this.pParent.resetGrandWheel();
        }

        //sprite = ( this.getChildByTag( D_NODE_TAG ) ? this.getChildByTag( D_NODE_TAG ).getChildByTag( WHEEL_TAG ) : this.getChildByTag( LAYER_TAG ).getChildByTag( D_NODE_TAG ).getChildByTag( WHEEL_TAG ) );
        if( this.getChildByTag( D_NODE_TAG ) )
            sprite = this.getChildByTag( D_NODE_TAG ).getChildByTag( WHEEL_TAG );
        else
            sprite = this.getChildByTag( LAYER_TAG ).getChildByTag( D_NODE_TAG ).getChildByTag( WHEEL_TAG );


        /*sprite.runAction( new cc.Sequence( new cc.RotateBy( 6, 720 * 2 ), new cc.RotateBy( 1, ( ( ( arrSize - this.curBonusIndx ) * ( 360 / arrSize ) ) ) + 10 ),
            new cc.RotateBy( 0.25, -10 ), new cc.DelayTime( 3 ), cc.callFunc( function(sprite){
        if( this.wheelCode == IN_GAME_WHEEL )
        {
            this.removeChildByTag( D_NODE_TAG );
            while( this.getChildByTag( LIGHT_TAG ) )
                this.removeChildByTag( LIGHT_TAG );
        }
        else
        {
            this.removeChildByTag( LAYER_TAG );
        }
        this.setPayOutPopUp();
    } ), this ) );*/

        sprite.runAction( new cc.Sequence( new cc.RotateBy( 6, 2 * 720 ), new cc.RotateBy( 1, ( ( ( arrSize - this.curBonusIndx ) * ( 360 / arrSize ) ) ) + 10 ),
            new cc.RotateBy( 0.25, -10 ), new cc.DelayTime( 1 ), cc.callFunc( function ( sprite ){
                if( this.wheelCode == IN_GAME_WHEEL )
                {
                    this.removeChildByTag( D_NODE_TAG );
                    while( this.getChildByTag( LIGHT_TAG ) )
                        this.removeChildByTag( LIGHT_TAG );
                    var delegate = AppDelegate.getInstance();
                    delegate.jukeBox( S_STOP_EFFECTS );
                }
                else
                {
                    //this.removeFromParent(true);
                    this.removeChildByTag( LAYER_TAG );
                }
                this.setPayOutPopUp();
            }, this, sprite ) ) );


    },
    setPayOutPopUp : function()
    {
        var visibleSize = cc.winSize;

        var giantWheelPayOutPopUp = new cc.Sprite( res.giantWheelPayOutPopUp );
        giantWheelPayOutPopUp.setPosition( cc.p( visibleSize.width * 0.5, visibleSize.height * 0.5 ) );
        this.addChild( giantWheelPayOutPopUp );

        var delegate = AppDelegate.getInstance();
        delegate.jukeBoxStop( S_BONUS_WIN );
        var total = ( this.wheelCode == PAID_WHEEL ? IAPHelper.getInstance().chipsToCredit : grandWheelArr[this.getMultiplierForInGameWheel()][this.curBonusIndx] );
        var factor = this.wheelCode == PAID_WHEEL ? (total / 151) : (total / 61);
        var currentDisplayed = factor;
        var label = new cc.LabelBMFont( "0", res.BM_FONT_1 );
        label.setScale( 0.35 );
        label.setPosition( giantWheelPayOutPopUp.getContentSize().width * 0.5, giantWheelPayOutPopUp.getContentSize().height * 0.38 );
        giantWheelPayOutPopUp.addChild( label );

        delegate.jukeBox( S_STOP_MUSIC );
        delegate.jukeBox( this.wheelCode == PAID_WHEEL ? S_WHEEL_BONUS : S_WHEEL_BONUS_SHORT );

        giantWheelPayOutPopUp.runAction( new cc.RepeatForever( new cc.Sequence( new cc.DelayTime( 0.08 ), cc.callFunc( function( giantWheelPayOutPopUp ) {//lambda
        currentDisplayed+=factor;
        if( currentDisplayed > total )
        {
            currentDisplayed = total;
            giantWheelPayOutPopUp.stopAllActions();
        }
        label.setString( DPUtils.getInstance().getNumString( currentDisplayed ) );

    },this, currentDisplayed, label, factor, total, delegate ) ) ) );

        var skipItm = new cc.MenuItemImage( res.collectBonus, res.collectBonus, this.collectBonusMenuCB, this );

        skipItm.setPosition( cc.p( giantWheelPayOutPopUp.getContentSize().width * 0.5, skipItm.getContentSize().height * 0.7 ) );
        skipItm.setTag( BACK_TAG );

        var menu = new cc.Menu( skipItm );
        menu.setPosition( cc.p( 0, 0 ) );
        giantWheelPayOutPopUp.addChild( menu );

        /*var fireWorks = Particle2D_On_3DWithTotalParticles( 20 );
        fireWorks.setSpeed( 1000 );
        fireWorks.setGravity( cc.p( fireWorks.getGravity().x, fireWorks.getGravity().y * 8 ) );
        fireWorks.setPosVar( cc.p( visibleSize.width * 0.5, 0 ) );
        fireWorks.setvarRemoveOnFinish( true );
        fireWorks.setPosition( visibleSize.width * 0.5, -100 + origin.y );
        this.addChild( fireWorks );*/

        DJSResizer.getInstance().setFinalScaling( giantWheelPayOutPopUp );
    },
    collectBonusMenuCB : function(pSender)
    {
        var delegate = AppDelegate.getInstance();

        if( PAID_WHEEL === this.wheelCode )
        {
            delegate.jukeBox( S_STOP_MUSIC );
            delegate.jukeBox( S_STOP_EFFECTS );
            delegate.jukeBox( S_LOBBY, true );
            delegate.assignRewards( IAPHelper.getInstance().chipsToCredit );
            delegate.showingScreen = false;

            PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_TOUCH_DAILY_BONUS_POP);

        }
        else
        {
            delegate.jukeBoxStop( S_BONUS_WIN );
            delegate.jukeBox( S_AMBIENCE );
            delegate.assignRewards( grandWheelArr[this.getMultiplierForInGameWheel()][this.curBonusIndx],true );
            this.pParent.resetGrandWheel();
        }

        delegate.jukeBox( S_BTN_CLICK );
        this.removeFromParent( true );
    },
    startPaidRolling : function()
    {
        if (this.skipItm){
            this.skipItm.removeFromParent();
        }
        this.removeChildByTag( PAID_WHEEL );
        //this->getChildByTag( LAYER_TAG )->getChildByTag( D_NODE_TAG )->removeChildByTag( D_NODE_TAG );
        this.startSpin();
    }

});