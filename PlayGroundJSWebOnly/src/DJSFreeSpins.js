/**
 * Created by RNF-Mac11 on 2/15/17.
 */



var reelsRandomizationArrayDJSFreeSpins =
    [
        [// 85% payout
            1, 6500 ,1 , 6000 ,1 , 7000 ,
            1,3000 ,1,9000,1,6000,
            3000,4500,0,800,1,400,
            1500,5000,0,6500,1,1500,
            1,1500,1500,4600,0,2000,
            1,3200,1,1500,2000,4725
        ],
        [//92 %
            1, 7000 ,1 , 6500 ,1 , 7500 ,
            1,3000 ,1,9000,1,6000,
            3000,4500,0,800,1,400,//800
            2000,5000,0,6500,1,1500,
            1,2000,1500,4600,0,2000,
            1,4000,1,1500,3000,4725
        ],
        [// 95-96% payout
            1, 7500 ,1 , 7000 ,1 , 8000 ,
            1,3000 ,1,9000,1,6000,
            3000,5000,0,1000,1,400,
            2000,5000,0,6500,1,1500,
            1,2000,1500,4600,0,2000,
            1,5500,1,1500,3000,4725
        ],
        [// 140% payout
            1, 55500 ,1 , 55000 ,1 , 56000 ,
            0,51000 ,0,75000,0,54000,
            51000,75000,0,49000,1,47800,
            50000,75000,0,54500,1,51500,
            1,50000,49500,75000,0,50000,
            1,53500,1,49500,51000,75000
        ],
        [// 300% payout
            1, 55500 ,1 , 55000 ,1 , 56000 ,
            0,51000 ,0,75000,0,54000,
            51000,75000,0,49000,1,47800,
            50000,75000,0,54500,1,51500,
            1,50000,49500,75000,0,50000,
            1,53500,1,49500,51000,75000
        ]
    ];



var reelsRandomizationArrayWild = [//for 12th machine only
    [// 85% payout
        100, 3000, 100, 3000, 100, 4000,
        100,4000, 500, 3000, 500, 2500,
        1, 200, 1, 1000, 100, 1500,
        100, 2000, 100, 2000, 100, 2500,
        100, 500, 100, 1000, 100, 800,
        100, 300, 100, 500, 100, 500
    ],
    [//92 %
        100, 3000, 100, 3000, 100, 4000,
        100,4000, 500, 3000, 500, 2500,
        1, 200, 1, 1000, 100, 1500,
        100, 2000, 100, 2000, 100, 2500,
        100, 500, 100, 1000, 100, 800,
        100, 300, 100, 500, 100, 500
    ],
    [
        //95% payout
        100, 3000, 100, 3000, 100, 4000,
        100,4000, 500, 3000, 500, 2500,
        1, 200, 1, 1000, 100, 1500,
        100, 2000, 100, 2000, 100, 2500,
        100, 500, 100, 1000, 100, 800,
        100, 300, 100, 500, 100, 500
    ],
    [//140% payout
        1, 3000, 1, 3000, 1, 4000,
        1,4000, 1, 10000, 1, 2500,
        1, 7000, 1, 1000, 1, 1500,
        1, 2000, 1, 2000, 1, 2500,
        1, 1500, 1, 2000, 1, 1800,
        1, 1300, 1, 1500, 1, 1500
    ],
    [//300% payout
        1, 3000, 1, 3000, 1, 4000,
        1,4000, 1, 10000, 1, 2500,
        1, 7000, 1, 1000, 1, 1500,
        1, 2000, 1, 2000, 1, 2500,
        1, 1500, 1, 2000, 1, 1800,
        1, 1300, 1, 1500, 1, 1500
    ]
];
var reelsArrayWild1 = [//for 12th machine only, place the wild such no two wild are visible at a time
    /*1*/ELEMENT_EMPTY,/*2*/ELEMENT_7_RED,/*3*/ELEMENT_EMPTY,/*4*/ELEMENT_BAR_3,/*5*/ELEMENT_EMPTY,
    /*6*/ELEMENT_BAR_2,/*7*/ELEMENT_EMPTY,/*8*/ELEMENT_BAR_1,/*9*/ELEMENT_EMPTY,/*10*/ELEMENT_7_BLUE,
    /*11*/ELEMENT_EMPTY,/*12*/ELEMENT_7_PINK,/*13*/ELEMENT_EMPTY,/*14*/ELEMENT_WILD,/*15*/ELEMENT_EMPTY,
    /*16*/ELEMENT_BAR_3,/*17*/ELEMENT_EMPTY,/*18*/ELEMENT_BAR_2,/*19*/ELEMENT_EMPTY,/*20*/ELEMENT_BAR_1,
    /*21*/ELEMENT_EMPTY,/*22*/ELEMENT_7_PINK,/*23*/ELEMENT_EMPTY,/*24*/ELEMENT_7_RED,/*25*/ELEMENT_EMPTY,
    /*26*/ELEMENT_7_BLUE,/*27*/ELEMENT_EMPTY,/*28*/ELEMENT_BAR_1,/*29*/ELEMENT_EMPTY,/*30*/ELEMENT_BAR_3,
    /*31*/ELEMENT_EMPTY,/*32*/ELEMENT_7_PINK,/*33*/ELEMENT_EMPTY,/*34*/ELEMENT_BAR_3,/*35*/ELEMENT_EMPTY,
    /*36*/ELEMENT_BAR_2
];
var DJSFreeSpins = GameScene.extend({

    calculationsAssigned : false,
    doWildMiraz : false,
    hadTripleWild : false,
    ancientScore : 0,
    currentPayLineAnimIndx : 0,
    totalFreeSpinWin : 0,
    resArr  : null,
    activePayLinesIndx : null,
    prevReelIndexWild : null,
    activePayLines : null,
    payLines : null,
    wildElemnts : null,
    wildLaneCode : null,
    wildIndx : null,
    reelsArrayDJSFreeSpins:[],
    reelsArrayWild:[],
    ctor:function ( level_index ) {
        this.reelsArrayDJSFreeSpins = [
            /*1*/ELEMENT_EMPTY,/*2*/ ELEMENT_7_RED,/*3*/ ELEMENT_EMPTY,/*4*/ ELEMENT_7_BLUE,/*5*/ ELEMENT_EMPTY,
            /*6*/ELEMENT_7_PINK,/*7*/ELEMENT_EMPTY,/*8*/ ELEMENT_BAR_3,/*9*/ ELEMENT_EMPTY,/*10*/ ELEMENT_BAR_2,
            /*11*/ELEMENT_EMPTY,/*12*/ELEMENT_BAR_1,/*13*/ELEMENT_BAR_3,/*14*/ ELEMENT_SPIN,/*15*/ELEMENT_EMPTY,
            /*16*/ELEMENT_7_PINK,/*17*/ ELEMENT_EMPTY,/*18*/ ELEMENT_BAR_3,/*19*/ELEMENT_7_RED,/*20*/ ELEMENT_SPIN,
            /*21*/ ELEMENT_EMPTY,/*22*/ ELEMENT_BAR_2,/*23*/ELEMENT_EMPTY,/*24*/ELEMENT_BAR_1,/*25*/ELEMENT_EMPTY,
            /*26*/ ELEMENT_7_RED,/*27*/ ELEMENT_BAR_1,/*28*/ ELEMENT_SPIN,/*29*/ELEMENT_EMPTY,/*30*/ ELEMENT_BAR_3,
            /*31*/ELEMENT_EMPTY,/*32*/ELEMENT_7_BLUE,/*33*/ ELEMENT_EMPTY,/*34*/ ELEMENT_BAR_1,/*35*/ ELEMENT_7_PINK,
            /*36*/ ELEMENT_SPIN
        ];
        this.reelsArrayWild = [//for 12th machine only, place the wild such no two wild are visible at a time
            /*1*/ELEMENT_EMPTY,/*2*/ELEMENT_7_RED,/*3*/ELEMENT_EMPTY,/*4*/ELEMENT_BAR_3,/*5*/ELEMENT_EMPTY,
            /*6*/ELEMENT_BAR_2,/*7*/ELEMENT_EMPTY,/*8*/ELEMENT_BAR_1,/*9*/ELEMENT_EMPTY,/*10*/ELEMENT_7_BLUE,
            /*11*/ELEMENT_EMPTY,/*12*/ELEMENT_7_PINK,/*13*/ELEMENT_EMPTY,/*14*/ELEMENT_WILD,/*15*/ELEMENT_EMPTY,
            /*16*/ELEMENT_BAR_3,/*17*/ELEMENT_EMPTY,/*18*/ELEMENT_BAR_2,/*19*/ELEMENT_EMPTY,/*20*/ELEMENT_BAR_1,
            /*21*/ELEMENT_EMPTY,/*22*/ELEMENT_7_PINK,/*23*/ELEMENT_EMPTY,/*24*/ELEMENT_7_RED,/*25*/ELEMENT_EMPTY,
            /*26*/ELEMENT_7_BLUE,/*27*/ELEMENT_EMPTY,/*28*/ELEMENT_BAR_1,/*29*/ELEMENT_EMPTY,/*30*/ELEMENT_BAR_3,
            /*31*/ELEMENT_EMPTY,/*32*/ELEMENT_7_PINK,/*33*/ELEMENT_EMPTY,/*34*/ELEMENT_BAR_3,/*35*/ELEMENT_EMPTY,
            /*36*/ELEMENT_BAR_2
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSFreeSpins;
        this.physicalArrayCheck= this.reelsArrayDJSFreeSpins;
        this._super( level_index );

        this.resArr = new Array( );
        for( var i = 0 ; i < 9 ; i++ )
        {
            this.resArr[i] = new Array(3);
        }

        this.activePayLinesIndx = new Array();
        this.prevReelIndexWild = new Array();
        this.activePayLines = new Array();
        this.payLines = new Array();
        this.wildElemnts = new Array();
        this.wildLaneCode = new Array();
        this.wildIndx = new Array();

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;


    },

    animateAllpayLines : function()
    {
        for ( var j = 0; j < this.activePayLines.length; j++ )
        {
            var tmpVec = this.activePayLines[j];

            var e;
            for ( var i = 0; i < tmpVec.length; i++ )
            {
                e = tmpVec[i];
                e.runAction( new cc.Repeat( new cc.Sequence( new cc.ScaleTo( 0.5, 1.1 ), new cc.ScaleTo( 0.5, 1.0 ) ), 2 ) );
            }

            var sprit = this.payLines[this.activePayLinesIndx[j]];

            if ( j < this.activePayLines.length - 1 )
            {
                sprit.runAction( new cc.Repeat( new cc.Sequence( new cc.FadeIn( 0.5 ), new cc.FadeOut( 0.5 ) ), 2 ) );
            }
            else
            {
                sprit.runAction( new cc.Sequence( ( new cc.Repeat( new cc.Sequence( new cc.FadeIn( 0.5 ), new cc.FadeOut( 0.5 ) ), 2 ) ), new cc.callFunc( function(){
                    this.startPayLineAnim();
                }, this ) ) );
            }
        }
    },

    /*animCB : function( obj )
    {
        obj.removeFromParent( true );

        this.startFreeSpin();
    },*/

    calculatePayment : function( doAnimate )
    {
        this.setResultPayLines();

        var payMentArray = [

            /*0*/    800.0,// Triple Wild

            /*1*/    0.0, // 2x 2x 2x

            /*2*/    20.0,  // 7 7 7 Bule

            /*3*/    10.0,  // 7 7 7 Pink

            /*4*/    8.0,  // 7 7 7 Red

            /*5*/    5.0,   // Any 7 combo

            /*6*/    5.0,   // Triple Bar

            /*7*/    4.0,   // Double Bar

            /*8*/    3.0,   // Single Bar

            /*9*/    2.0,   // Triple Bar & 7Red combo

            /*10*/   2.0,   // Double Bar & 7Pink combo

            /*11*/   2.0,   // Single Bar & 7 Blue Combo

            /*12*/   1.0   // Any Bar Combo

        ];


        var totalPay = 0;
        var pay = 0;
        var tmpPay = 1.0;
        var winScenario = 14;
        var countX = 0;
        var countSpin = 0;
        var countWild = 0;
        var countGeneral = 0;
        var count7B = 0;
        var count7R = 0;
        var count7P = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var lineLength = 3;
        var i, j, k;
        var tmpVec = new Array();


        for ( i = 0; i < this.lineVector.length; i++ )
        {
            var breakOuterLoop = false;
            for ( k = 0; k < winScenario; k++ )
            {
                var breakLoop = false;
                countGeneral = 0;
                pay = 0;
                count7B = 0;
                count7P = 0;
                count7R = 0;
                countSpin = 0;
                countX = 0;
                countWild = 0;
                countGeneral = 0;
                pivotElement = -1;
                pivotElement2 = -1;
                tmpPay = 1.0;
                switch ( k )
                {
                    case 0://for all combination with wild
                        for ( j = 0; j < lineLength; j++ )
                        {
                            if ( this.resArr[i][j] != -1 && this.resArr[i][j] != ELEMENT_EMPTY )
                            {
                                switch ( this.resArr[i][j] )
                                {
                                    case ELEMENT_WILD:
                                        countX++;
                                        countWild++;
                                        break;

                                    default:
                                        switch ( pivotElement )
                                        {
                                            case -1:
                                                pivotElement = this.resArr[i][j];
                                                countGeneral++;
                                                break;

                                            default:
                                                pivotElement2 = this.resArr[i][j];
                                                countGeneral++;
                                                break;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }

                        if ( j == 3 )
                        {
                            if ( countX == 3 )
                            {
                                pay = payMentArray[0];
                                breakOuterLoop = true;
                                breakLoop = true;
                            }
                            else if( countWild >= 2 )
                            {
                                breakLoop = true;

                                switch ( pivotElement )
                                {
                                    case ELEMENT_7_RED:
                                        pay = tmpPay * payMentArray[4];
                                        break;

                                    case ELEMENT_7_PINK:
                                        pay = tmpPay * payMentArray[3];
                                        break;

                                    case ELEMENT_7_BLUE:
                                        pay = tmpPay * payMentArray[2];
                                        break;

                                    case ELEMENT_BAR_3:
                                        pay = tmpPay * payMentArray[6];
                                        break;

                                    case ELEMENT_BAR_2:
                                        pay = tmpPay * payMentArray[7];
                                        break;

                                    case ELEMENT_BAR_1:
                                        pay = tmpPay * payMentArray[8];
                                        break;

                                    default:
                                        break;
                                }
                            }
                            else if( countWild == 1 )
                            {
                                breakLoop = true;

                                if ( pivotElement == pivotElement2 )
                                {
                                    switch ( pivotElement )
                                    {
                                        case ELEMENT_7_RED:
                                            pay = tmpPay * payMentArray[4];
                                            break;

                                        case ELEMENT_7_PINK:
                                            pay = tmpPay * payMentArray[3];
                                            break;

                                        case ELEMENT_7_BLUE:
                                            pay = tmpPay * payMentArray[2];
                                            break;

                                        case ELEMENT_BAR_3:
                                            pay = tmpPay * payMentArray[6];
                                            break;

                                        case ELEMENT_BAR_2:
                                            pay = tmpPay * payMentArray[7];
                                            break;

                                        case ELEMENT_BAR_1:
                                            pay = tmpPay * payMentArray[8];
                                            break;

                                        default:
                                            break;
                                    }
                                }
                                else
                                {
                                    if ( pivotElement >= ELEMENT_7_BLUE && pivotElement <= ELEMENT_7_RED &&
                                        pivotElement2 >= ELEMENT_7_BLUE && pivotElement2 <= ELEMENT_7_RED )
                                    {
                                        pay = tmpPay * payMentArray[5];
                                    }
                                    else if ( ( pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_BAR_3 ) &&
                                        ( pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_BAR_3 ) )
                                    {
                                        pay = tmpPay * payMentArray[12];
                                    }
                                    else if( ( pivotElement == ELEMENT_7_RED || pivotElement == ELEMENT_BAR_3 ) &&
                                        ( pivotElement2 == ELEMENT_7_RED || pivotElement2 == ELEMENT_BAR_3 ) )
                                    {
                                        pay = tmpPay * payMentArray[9];
                                    }
                                    else if( ( pivotElement == ELEMENT_7_PINK || pivotElement == ELEMENT_BAR_2 ) &&
                                        ( pivotElement2 == ELEMENT_7_PINK || pivotElement2 == ELEMENT_BAR_2 ) )
                                    {

                                        pay = tmpPay * payMentArray[10];

                                    }
                                    else if ( (pivotElement == ELEMENT_7_BLUE || pivotElement == ELEMENT_BAR_1 ) &&
                                        ( pivotElement2 == ELEMENT_7_BLUE || pivotElement2 == ELEMENT_BAR_1 ) )
                                    {
                                        pay = tmpPay * payMentArray[11];
                                    }
                                }
                            }
                        }
                        break;

                    case 1: // 7 7 7 blue
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.resArr[i][j] )
                            {
                                case ELEMENT_7_BLUE:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[2];
                            breakLoop = true;
                        }
                        break;

                    case 2: // 7 7 7 pink
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.resArr[i][j] )
                            {
                                case ELEMENT_7_PINK:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[3];
                            breakLoop = true;
                        }
                        break;

                    case 3: // 7 7 7 Red
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.resArr[i][j] )
                            {
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[4];
                            breakLoop = true;
                        }
                        break;

                    case 4: // 3Bar 3Bar 3Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.resArr[i][j] )
                            {
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[6];
                            breakLoop = true;
                        }
                        break;

                    case 5: // any 7 combo 2
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.resArr[i][j] )
                            {
                                case ELEMENT_7_BLUE:
                                case ELEMENT_7_PINK:
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[5];
                            breakLoop = true;
                        }
                        break;

                    case 6: // 2Bar 2Bar 2Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.resArr[i][j] )
                            {
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[7];
                            breakLoop = true;
                        }
                        break;

                    case 7: // 7Red and 3bar combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.resArr[i][j] )
                            {
                                case ELEMENT_7_RED:
                                    count7R++;
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 && count7R != 0 )
                        {
                            pay = payMentArray[9];
                            breakLoop = true;
                        }
                        break;

                    case 8: // 7PINK and 2bar combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.resArr[i][j] )
                            {
                                case ELEMENT_7_PINK:
                                    count7P++;
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 && count7P != 0 )
                        {
                            pay = payMentArray[10];
                            breakLoop = true;
                        }
                        break;

                    case 9: // 7Blue and 2bar combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.resArr[i][j] )
                            {
                                case ELEMENT_7_BLUE:
                                    count7B++;
                                    countGeneral++;
                                    break;

                                case ELEMENT_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 && count7B != 0 )
                        {
                            pay = payMentArray[11];
                            breakLoop = true;
                        }
                        break;

                    case 10: // Bar Bar Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.resArr[i][j] )
                            {
                                case ELEMENT_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[8];
                            breakLoop = true;
                        }
                        break;

                    case 11: // any Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.resArr[i][j] )
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[12];
                            breakLoop = true;
                        }
                        break;

                    default:
                        break;
                }

                if ( breakLoop )
                {
                    break;
                }
            }
            totalPay+=pay;
            if ( pay > 0 )
            {
                tmpVec = this.lineVector[i];
                this.activePayLinesIndx[ this.activePayLines.length ] = i;
                this.activePayLines.push( tmpVec );
            }

            if ( breakOuterLoop )
            {
                this.hadTripleWild = true;
                break;
            }
        }


        if ( doAnimate )
        {
            this.currentWin = this.currentBet * totalPay;
            if ( !this.haveFreeSpins )
            {
                if ( this.currentWin > 0 )
                {
                    this.addWin = this.currentWin / 10;

                    this.setCurrentWinAnimation();

                    this.startPayLineAnim();
                }
                this.checkAndSetAPopUp();
            }
            else if( this.haveFreeSpins )
            {
                if ( this.ancientScore == -1 )
                {
                    this.ancientScore = this.currentWin;
                }
                else
                {
                    this.totalFreeSpinWin+=this.currentWin;
                    this.addWin = 0;
                    if ( this.currentWin > 0 )
                    {
                        this.addWin = 1;
                    }

                    this.setCurrentWinAnimation();

                    this.animateAllpayLines();
                }
                var labl = this.getChildByTag( BOTTOM_BAR_TAG ).getChildByTag( 2 );
                labl.setString( "" + this.totalFreeSpinWin );
            }
        }
        else
        {
            this.currentWinForcast = this.currentBet * totalPay;
        }
    },
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSFreeSpins[i]=this.physicalReelElements[idx];
                i++;
            }
        }
        if (this.splPhysicalReelElements && this.splPhysicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.splPhysicalReelElements){
                this.reelsArrayWild[i]=this.splPhysicalReelElements[idx];
                i++;
            }
        }
    },
    changeTotalProbabilities : function()
    {
        this.totalProbalities = 0;
        this.totalProbalitiesWild = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            //this.extractArrayFromSring();
            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[this.easyModeCode][i];
                this.totalProbalitiesWild+=reelsRandomizationArrayWild[this.easyModeCode][i];
            }
        }
        else
        {
            for ( var i = 0; i < 36; i++ )
            {
                this.probabilityArray[this.easyModeCode][i] = reelsRandomizationArrayDJSFreeSpins[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSFreeSpins[this.easyModeCode][i];
                this.totalProbalitiesWild+=reelsRandomizationArrayWild[this.easyModeCode][i];
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
    },

    checkForCombinations : function( veC)
    {
        var vecC = this.setResultPayLines2( veC );

        var pay = 0;
        var tmpPay = 1.0;
        var winScenario = 14;
        var countX = 0;
        var countSpin = 0;
        var countWild = 0;
        var countGeneral = 0;
        var count7B = 0;
        var count7R = 0;
        var count7P = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var lineLength = 3;
        var i, j, k;
        var tmpVec;

        var powerElm;

        for ( i = 0; i < vecC.length; i++ )
        {
            var breakOuterLoop = false;
            for ( k = 0; k < winScenario; k++ )
            {
                var breakLoop = false;
                countGeneral = 0;
                pay = 0;
                count7B = 0;
                count7P = 0;
                count7R = 0;
                countSpin = 0;
                countX = 0;
                countWild = 0;
                countGeneral = 0;
                pivotElement = -1;
                pivotElement2 = -1;
                tmpPay = 1.0;

                switch ( k )
                {
                    case 0://for all combination with wild
                        for ( j = 0; j < lineLength; j++ )
                        {
                            powerElm = vecC[i][j];

                            if ( powerElm.eCode != -1 && powerElm.eCode != ELEMENT_EMPTY )
                            {
                                switch ( powerElm.eCode )
                                {
                                    case ELEMENT_WILD:
                                        countX++;
                                        countWild++;
                                        break;

                                    default:
                                        switch ( pivotElement )
                                        {
                                            case -1:
                                                pivotElement = this.resArr[i][j];
                                                countGeneral++;
                                                break;

                                            default:
                                                pivotElement2 = this.resArr[i][j];
                                                countGeneral++;
                                                break;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }

                        if ( j == 3 )
                        {
                            if ( countX == 3 )
                            {
                                pay = 10;
                                breakOuterLoop = true;
                                breakLoop = true;
                            }
                            else if( countWild >= 2 )
                            {
                                breakLoop = true;

                                switch ( pivotElement )
                                {
                                    case ELEMENT_7_RED:
                                        pay = tmpPay;
                                        break;

                                    case ELEMENT_7_PINK:
                                        pay = tmpPay;
                                        break;

                                    case ELEMENT_7_BLUE:
                                        pay = tmpPay;
                                        break;

                                    case ELEMENT_BAR_3:
                                        pay = tmpPay;
                                        break;

                                    case ELEMENT_BAR_2:
                                        pay = tmpPay;
                                        break;

                                    case ELEMENT_BAR_1:
                                        pay = tmpPay;
                                        break;

                                    default:
                                        break;
                                }
                            }
                            else if( countWild == 1 )
                            {
                                breakLoop = true;

                                if ( pivotElement == pivotElement2 )
                                {
                                    switch ( pivotElement )
                                    {
                                        case ELEMENT_7_RED:
                                            pay = tmpPay;
                                            break;

                                        case ELEMENT_7_PINK:
                                            pay = tmpPay;
                                            break;

                                        case ELEMENT_7_BLUE:
                                            pay = tmpPay;
                                            break;

                                        case ELEMENT_BAR_3:
                                            pay = tmpPay;
                                            break;

                                        case ELEMENT_BAR_2:
                                            pay = tmpPay;
                                            break;

                                        case ELEMENT_BAR_1:
                                            pay = tmpPay;
                                            break;

                                        default:
                                            break;
                                    }
                                }
                                else
                                {
                                    if ( pivotElement >= ELEMENT_7_BLUE && pivotElement <= ELEMENT_7_RED &&
                                        pivotElement2 >= ELEMENT_7_BLUE && pivotElement2 <= ELEMENT_7_RED )
                                    {
                                        pay = tmpPay;
                                    }
                                    else if ( ( pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_BAR_3 ) &&
                                        ( pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_BAR_3 ) )
                                    {
                                        pay = tmpPay;
                                    }
                                    else if( ( pivotElement == ELEMENT_7_RED || pivotElement == ELEMENT_BAR_3 ) &&
                                        ( pivotElement2 == ELEMENT_7_RED || pivotElement2 == ELEMENT_BAR_3 ) )
                                    {
                                        pay = tmpPay;
                                    }
                                    else if( ( pivotElement == ELEMENT_7_PINK || pivotElement == ELEMENT_BAR_2 ) &&
                                        ( pivotElement2 == ELEMENT_7_PINK || pivotElement2 == ELEMENT_BAR_2 ) )
                                    {

                                        pay = tmpPay;

                                    }
                                    else if ( (pivotElement == ELEMENT_7_BLUE || pivotElement == ELEMENT_BAR_1 ) &&
                                        ( pivotElement2 == ELEMENT_7_BLUE || pivotElement2 == ELEMENT_BAR_1 ) )
                                    {
                                        pay = tmpPay;
                                    }
                                }
                            }
                        }
                        break;

                    case 1: // 7 7 7 blue
                        for ( j = 0; j < lineLength; j++ )
                        {
                            powerElm = vecC[i][j];
                            switch ( powerElm.eCode )
                            {
                                case ELEMENT_7_BLUE:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = 10;
                            breakLoop = true;
                        }
                        break;

                    case 2: // 7 7 7 pink
                        for ( j = 0; j < lineLength; j++ )
                        {
                            powerElm = vecC[i][j];
                            switch ( powerElm.eCode )
                            {
                                case ELEMENT_7_PINK:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = 10;
                            breakLoop = true;
                        }
                        break;

                    case 3: // 7 7 7 Red
                        for ( j = 0; j < lineLength; j++ )
                        {
                            powerElm = vecC[i][j];
                            switch ( powerElm.eCode )
                            {
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = 10;
                            breakLoop = true;
                        }
                        break;

                    case 4: // 3Bar 3Bar 3Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            powerElm = vecC[i][j];
                            switch ( powerElm.eCode )
                            {
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = 10;
                            breakLoop = true;
                        }
                        break;

                    case 5: // any 7 combo 2
                        for ( j = 0; j < lineLength; j++ )
                        {
                            powerElm = vecC[i][j];
                            switch ( powerElm.eCode )
                            {
                                case ELEMENT_7_BLUE:
                                case ELEMENT_7_PINK:
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = 10;
                            breakLoop = true;
                        }
                        break;

                    case 6: // 2Bar 2Bar 2Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            powerElm = vecC[i][j];
                            switch ( powerElm.eCode )
                            {
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = 10;
                            breakLoop = true;
                        }
                        break;

                    case 7: // 7Red and 3bar combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            powerElm = vecC[i][j];
                            switch ( powerElm.eCode )
                            {
                                case ELEMENT_7_RED:
                                    count7R++;
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 && count7R != 0 )
                        {
                            pay = 10;
                            breakLoop = true;
                        }
                        break;

                    case 8: // 7PINK and 2bar combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            powerElm = vecC[i][j];
                            switch ( powerElm.eCode )
                            {
                                case ELEMENT_7_PINK:
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = 10;
                            breakLoop = true;
                        }
                        break;

                    case 9: // 7Blue and 2bar combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            powerElm = vecC[i][j];
                            switch ( powerElm.eCode )
                            {
                                case ELEMENT_7_BLUE:
                                    count7B++;
                                    countGeneral++;
                                    break;

                                case ELEMENT_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 && count7B != 0 )
                        {
                            pay = 10;
                            breakLoop = true;
                        }
                        break;

                    case 10: // Bar Bar Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            powerElm = vecC[i][j];
                            switch ( powerElm.eCode )
                            {
                                case ELEMENT_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = 10;
                            breakLoop = true;
                        }
                        break;

                    case 11: // any Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            powerElm = vecC[i][j];
                            switch ( powerElm.eCode )
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = 10;
                            breakLoop = true;
                        }
                        break;

                    default:
                        break;
                }

                if ( breakLoop )
                {
                    break;
                }
            }
            if ( pay > 0 )
            {
                tmpVec = vecC[i];
                this.activePayLinesIndx[ this.activePayLines.length ] = i;
                this.activePayLines.push( tmpVec );
            }

            if ( breakOuterLoop )
            {
                this.hadTripleWild = true;
                break;
            }
        }

        this.startPayLineAnim();
    },

    freeSpinCB : function( dt )
    {
        this.startFreeSpin();
    },

    generateResult : function( scrIndx )
    {
        if ( this.isWildGeneration )
        {
            this.generateResultWild();
        }
        else
        {
            for ( var i = 0; i < this.slots[scrIndx].displayedReels.length; i++ )
            {
                //generate:
                var num = this.prevReelIndex[i];

                var tmpnum = 0;

                while ( num == this.prevReelIndex[i] )
                {
                    num = Math.floor(Math.random() * this.totalProbalities);
                }
                this.prevReelIndex[i] = num;
                this.resultArray[0][i] = num;

                for ( var j = 0; j < 36; j++ )
                {
                    tmpnum+=this.probabilityArray[this.easyModeCode][j];

                    if ( tmpnum >= this.resultArray[0][i] )
                    {
                        this.resultArray[0][i] = j;
                        break;
                    }
                }

                /*/HACK
                 if (i == 0)
                 {
                 resultArray[0][i] = 35;//2;
                 }
                 else if (i == 1)
                 {
                 resultArray[0][i] = 35;//2, 4, 20, 30
                 }
                 else if(i == 2)
                 {
                 resultArray[0][i] = 35;
                 }//*///HACK
            }
        }
    },

    generateResultWild : function()
    {
        for ( var i = 0; i < 3; i++ )
        {
            if ( this.wildLaneCode[i] <= 0 )
            {

                var num = this.prevReelIndexWild[i];

                var tmpnum = 0;

                while ( num == this.prevReelIndexWild[i] )
                {
                    num = Math.floor( Math.random() * this.totalProbalitiesWild );
                }
                this.prevReelIndexWild[i] = num;
                this.resultArray[0][i] = num;

                for ( var j = 0; j < 36; j++ )
                {
                    tmpnum+=reelsRandomizationArrayWild[this.easyModeCode][j];

                    if ( tmpnum >= this.resultArray[0][i] )
                    {
                        this.resultArray[0][i] = j;
                        break;
                    }
                }
            }


            /*/HACK
             if (i == 0)
             {
             resultArray[0][i] = 3;//2;
             }
             else if (i == 1)
             {
             resultArray[0][i] = 13;//2, 4, 20, 30
             }
             else if(i == 2)
             {
             resultArray[0][i] = 13;
             }
             else
             {
             resultArray[0][i] = 0;
             }//*///HACK
        }
    },

    payLineAnimCB : function( obj )
    {
        obj.setOpacity( 0 );
        this.currentPayLineAnimIndx++;

        if ( this.currentPayLineAnimIndx < this.activePayLines.length )
        {
            var tmpVec = this.activePayLines[this.currentPayLineAnimIndx - 1];

            var e;
            for ( var i = 0; i < tmpVec.length; i++ )
            {
                e = tmpVec[i];
                e.stopAllActions();
                e.setLocalZOrder( 0 );
            }

            tmpVec = this.activePayLines[this.currentPayLineAnimIndx];

            for ( var i = 0; i < tmpVec.length; i++ )
            {
                e = tmpVec[i];
                e.aCode = SCALE_TAG;
                e.animateNow();
                e.setLocalZOrder( 2 );
            }

            var sprit = this.payLines[this.activePayLinesIndx[this.currentPayLineAnimIndx]];

            sprit.runAction( new cc.Sequence( new cc.FadeIn( 0.5 ), new cc.DelayTime( 1.0 ), new cc.FadeOut( 0.5 ), new cc.callFunc( this.payLineAnimCB, this ) ) );

            this.setDNodeOnWildMiraj( this.activePayLinesIndx[this.currentPayLineAnimIndx] );
        }
        else
        {
            var tmpVec = this.activePayLines[ this.activePayLines.length - 1 ];

            var e;
            for ( var i = 0; i < tmpVec.length; i++ )
            {
                e = tmpVec[i];
                e.stopAllActions();
                e.setLocalZOrder( 0 );
            }
            this.startPayLineAnim();
        }
    },

    resetWildMiraj : function( lane )
    {
        this.wildLaneCode[lane] = 0;
        var wildSprite = this.slots[0].wildSpriteVec[lane];
        wildSprite.runAction( new cc.Sequence( new cc.ScaleTo( 0.2, 1, 0 ),
            new cc.callFunc( function(){

                if ( wildSprite.getAnchorPoint().y == 0 )
                {
                    wildSprite.setPositionY( wildSprite.getPositionY() + wildSprite.getContentSize().height / 2 );
                }
                else if( wildSprite.getAnchorPoint().y == 1 )
                {
                    wildSprite.setPositionY( wildSprite.getPositionY() - wildSprite.getContentSize().height / 2 );
                }
                wildSprite.setAnchorPoint( cc.p( 0.5, 0.5 ) );
            }, this, wildSprite ) ) );
    },

    rollUpdator : function(dt)
    {
        var i;
        for ( i = 0; i < this.totalSlots; i++ )
        {
            this.slots[i].updateSlot( dt );
        }

        for ( i = 0; i < this.totalSlots; i++ )
        {
            if ( this.slots[i].isSlotRunning() )
            {
                break;
            }
        }

        if ( i >= this.totalSlots )
        {
            this.isRolling = false;
            this.unschedule( this.rollUpdator );

            if ( this.wildElemnts.length >= 3 )
            {
                this.haveFreeSpins = true;
                this.freeSpinCount = 6;
                this.setFreeSpinAnim();
                this.spinItem.loadTextures( "auto_spin.png", "auto_spin_pressed.png", "", ccui.Widget.PLIST_TEXTURE );
                var glowSprite = this.spinItem.getChildByTag( GLOW_SPRITE_TAG );
                glowSprite.setTexture( "res/glow_white.png" );
            }

            if ( !this.doWildMiraz )
            {
                this.calculatePayment( true );
            }

            var time = 1.0;
            if ( this.activePayLines.length )
            {
                time = ( ( 2.0 * ( this.activePayLines.length ) ) + 2.0 );
            }


            if (this.isAutoSpinning && !this.haveFreeSpins /*&& !this.delegate.showLevelUp*/ )
            {
                this.scheduleOnce( this.autoSpinCB, time );
            }
            else
            {
                if ( this.freeSpinCount >= 1 )
                {
                    var wildId = false;
                    for ( var i = 0; i < 3; i++ )
                    {
                        if ( this.wildLaneCode[i] == 2 )
                        {
                            wildId = true;
                            this.calculationsAssigned = false;
                            break;
                        }
                    }

                    if ( this.doWildMiraz && wildId )
                    {
                        var node = new cc.Node();//lambda

                        node.runAction( new cc.Sequence( new cc.DelayTime( 0.5 ),
                            new cc.callFunc( function(){
                                node.removeFromParent( true );
                                for ( var i = 0; i < 3; i++ )
                                {
                                    if ( this.wildLaneCode[i] == 2 )
                                    {
                                        this.setWildMiraj( i, this.wildIndx[i] );
                                    }
                                }

                                var tim = 2.0;
                                if ( this.activePayLines.length )
                                {
                                    tim = ( ( 2.0 * ( this.activePayLines.length ) ) + 2.0 );
                                }

                                if( this.freeSpinCount > 1 )
                                {
                                    this.scheduleOnce( this.freeSpinCB, tim );
                                }
                                else
                                {
                                    var node2 = new cc.Node();

                                    node2.runAction( new cc.Sequence( new cc.DelayTime( tim ),
                                        new cc.callFunc( function(){
                                            node2.removeFromParent( true );
                                            this.setFreeSpinsOver( -1 );
                                        }, this, node2) ) );
                                    this.addChild( node2 );
                                }

                            }, this, node ) ) );

                        this.addChild( node );
                    }
                    else if( this.freeSpinCount > 1 )
                    {
                        this.scheduleOnce( this.freeSpinCB, time );
                    }
                    else
                    {
                        var node2 = new cc.Node();

                        node2.runAction( new cc.Sequence( new cc.DelayTime( time ),
                            new cc.callFunc( function(){
                                node2.removeFromParent( true );
                                this.setFreeSpinsOver( -1 );
                            }, this, node2 ) ) );
                        this.addChild( node2 );
                    }
                }
                else
                {
                    this.freeSpinCount = 0;
                    this.haveFreeSpins = false;
                }
            }
        }
    },

    setDNodeOnWildMiraj : function( code )
    {
        var color =  cc.color( 255.0, 255.0, 255.0, 100.0 );

        var y = 0;
        var height = 0;
        for ( var i = 0; i < this.slots[0].wildSpriteVec.length; i++ )
        {
            var sprite = this.slots[0].wildSpriteVec[i];
            var dNode = sprite.getChildByTag( D_NODE_TAG );
            dNode.clear();

            height = sprite.getContentSize().height / 3;

            if ( sprite.getScaleY() != 0 )
            {
                switch ( code )
                {
                    case 0://_ _ _
                        y = height;
                        dNode.drawRect( cc.p( 0, y ), cc.p( sprite.getContentSize().width, y + height * 2 ), color );
                        break;

                    case 1://- - -
                        dNode.drawRect( cc.p( 0, 0 ), cc.p( sprite.getContentSize().width, height ), color );

                        y = height * 2;
                        dNode.drawRect( cc.p( 0, y ), cc.p( sprite.getContentSize().width, y + height ), color );
                        break;

                    case 2:// ` ` `
                        dNode.drawRect( cc.p( 0, 0 ), cc.p( sprite.getContentSize().width, height * 2 ), color );
                        break;

                    case 3://_ - `
                        if ( i == 0 )
                        {
                            y = height;
                            dNode.drawRect( cc.p( 0, y ), cc.p( sprite.getContentSize().width, y + height * 2 ), color );
                        }
                        else if ( i == 2 )
                        {
                            dNode.drawRect( cc.p( 0, 0 ), cc.p( sprite.getContentSize().width, height * 2 ), color );
                        }
                        else
                        {
                            dNode.drawRect( cc.p( 0, 0 ), cc.p( sprite.getContentSize().width, height ), color );

                            y = height * 2;
                            dNode.drawRect( cc.p( 0, y ), cc.p( sprite.getContentSize().width, y + height ), color );
                        }
                        break;

                    case 4://` - _
                        if ( i == 2 )
                        {
                            y = height;
                            dNode.drawRect( cc.p( 0, y ), cc.p( sprite.getContentSize().width, y + height * 2 ), color );
                        }
                        else if ( i == 0 )
                        {
                            dNode.drawRect( cc.p( 0, 0 ), cc.p( sprite.getContentSize().width, height * 2 ), color );
                        }
                        else
                        {
                            dNode.drawRect( cc.p( 0, 0 ), cc.p( sprite.getContentSize().width, height ), color );

                            y = height * 2;
                            dNode.drawRect( cc.p( 0, y ), cc.p( sprite.getContentSize().width, y + height ), color );
                        }
                        break;

                    case 5:// - _ -
                        if ( i == 1 )
                        {
                            y = height;
                            dNode.drawRect( cc.p( 0, y ), cc.p( sprite.getContentSize().width, y + height * 2 ), color );
                        }
                        else
                        {
                            dNode.drawRect( cc.p( 0, 0 ), cc.p( sprite.getContentSize().width, height ), color );

                            y = height * 2;
                            dNode.drawRect( cc.p( 0, y ), cc.p( sprite.getContentSize().width, y + height ), color );
                        }
                        break;

                    case 6:// - ` -
                        if ( i == 1 )
                        {
                            dNode.drawRect( cc.p( 0, 0 ), cc.p( sprite.getContentSize().width, height * 2 ), color );
                        }
                        else
                        {
                            dNode.drawRect( cc.p( 0, 0 ), cc.p( sprite.getContentSize().width, height ), color );

                            y = height * 2;
                            dNode.drawRect( cc.p( 0, y ), cc.p( sprite.getContentSize().width, y + height ), color );
                        }
                        break;

                    case 7://_ - _
                        if ( i == 1 )
                        {
                            dNode.drawRect( cc.p( 0, 0 ), cc.p( sprite.getContentSize().width, height ), color );

                            y = height * 2;
                            dNode.drawRect( cc.p( 0, y ), cc.p( sprite.getContentSize().width, y + height), color );
                        }
                        else
                        {
                            y = height;
                            dNode.drawRect( cc.p( 0, y ), cc.p( sprite.getContentSize().width, y + height * 2 ), color );
                        }
                        break;

                    case 8://` - `
                        if ( i == 1 )
                        {
                            dNode.drawRect( cc.p( 0, 0 ), cc.p( sprite.getContentSize().width, height ), color );

                            y = height * 2;
                            dNode.drawRect( cc.p( 0, y ), cc.p( sprite.getContentSize().width, y + height ), color );
                        }
                        else
                        {
                            dNode.drawRect( cc.p( 0, 0 ), cc.p( sprite.getContentSize().width, height * 2 ), color );
                        }
                        break;

                    default:
                        break;
                }
            }
        }
    },

    setFinalAnimation : function()
    {
        this.checkForCombinations( this.slots[0].resultReelAncient );
    },

    setFreeSpinAnim : function()
    {
        this.delegate.jukeBox( S_FREESPIN );
        for ( var i = 0; i < this.wildElemnts.length; i++ )
        {
            var e = this.wildElemnts[i];
            var rBy = new cc.RotateBy( 0.5, 360 );
            var sBy = new cc.ScaleBy( 0.5, 0.1 );
            e.runAction( new cc.Sequence( rBy, rBy.reverse() ) );

            e.runAction( new cc.Sequence( sBy, sBy.reverse() ) );
        }

        var bottomBar = this.getChildByTag( BOTTOM_BAR_TAG );

        if ( bottomBar )
        {
            bottomBar.setVisible( true );
            var labl = bottomBar.getChildByTag( 1 );
            labl.setString( "Free Spins  " + (this.freeSpinCount - 1 ) );

            labl = bottomBar.getChildByTag( 2 );
            labl.setString( "" + this.totalFreeSpinWin );
        }
        else
        {
            bottomBar = new cc.Sprite( "#bottom_bar.png" );
            bottomBar.setPosition( cc.p( 209 + bottomBar.getContentSize().width / 2, this.vSize.height - 587 - bottomBar.getContentSize().height / 2 ) );
            bottomBar.setTag( BOTTOM_BAR_TAG );
            this.addChild( bottomBar, 3 );

            var freeSpinLabel = new cc.LabelTTF( "Free Spins  " + ( this.freeSpinCount - 1 ), "HELVETICALTSTD-BOLD", 30 );
            freeSpinLabel.setPosition( 200, bottomBar.getContentSize().height * 0.5 );
            freeSpinLabel.setTag( 1 );
            bottomBar.addChild( freeSpinLabel, 5 );

            var freeSpinWinLabel = new cc.LabelTTF( "" + this.totalFreeSpinWin, "HELVETICALTSTD-BOLD", 30 );
            freeSpinWinLabel.setPosition( 700, bottomBar.getContentSize().height * 0.5 );
            freeSpinWinLabel.setTag( 2 );
            bottomBar.addChild( freeSpinWinLabel, 5 );
        }
    },

    setFreeSpinsOver : function( time )
    {
        var color = cc.color( 0, 0, 0, 150 );
        //this.drawNode.drawRect( cc.p( 0, 0 ), cc.p( this.vSize.width, this.vSize.height ), color, this.vSize.height, color );

        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        this.addChild( darkBG, 7 );

        var node = new cc.Sprite( "#score_bg.png" );

        node.setPosition( cc.p( this.vSize.width / 2, this.vSize.height / 2 ) );

        node.runAction( new cc.Sequence( new cc.DelayTime( 3 ),
            new cc.callFunc( function(){

                darkBG.removeFromParent( true );
                node.removeFromParent( true );

                this.freeSpinCount = 0;
                this.haveFreeSpins = false;

                for ( var i = 0; i < 3; i++ )
                {
                    this.resetWildMiraj( i );

                }

                var bottomBar = this.getChildByTag( BOTTOM_BAR_TAG );
                bottomBar.setVisible( false );


                this.currentWin = this.totalFreeSpinWin + this.ancientScore;
                if ( this.currentWin )
                {
                    this.addWin = this.currentWin / 10;
                    this.setCurrentWinAnimation( );
                }



                if ( this.isAutoSpinning /*&& !this.delegate.showLevelUp*/ )
                {
                    this.scheduleOnce( this.autoSpinCB, 1.0 );
                }
                else
                {
                    this.removeAutoSpinning();
                }

                this.drawNode.clear();

                this.checkAndSetAPopUp();

                /***/
                var veC = this.slots[0].resultReel;
                var vecC;

                for ( var i = 0; i < veC.length; i++ )
                {
                    vecC = veC[i];
                    for ( var j = 0; j < vecC.length; j++ )
                    {
                        var sprite = vecC[j];
                        sprite.runAction( new cc.FadeOut( 0.2 ) );
                    }
                }

                veC = this.slots[0].resultReelAncient;

                for ( var i = 0; i < veC.length; i++ )
                {
                    vecC = veC[i];
                    for ( var j = 0; j < vecC.length; j++ )
                    {
                        var sprite = vecC[j];
                        sprite.runAction( new cc.FadeIn( 0.2 ) );
                        var e = sprite;
                        if ( e.eCode == ELEMENT_SPIN )
                        {
                            sprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.ScaleTo( 0.5, 1.1 ).easing(cc.easeBackOut()), new cc.ScaleTo( 0.5, 1.0 ).easing(cc.easeBackOut()) ) ) );
                        }
                    }
                }

                for ( var i = 0; i < this.payLines.length; i++ )
                {
                    var sprit = this.payLines[i];
                    sprit.stopAllActions();
                    sprit.setOpacity( 0 );
                }
                this.activePayLines = [];

                this.setFinalAnimation();
                /***/

            }, this, node, darkBG ) ) );


        var freeSpinWinLabel = new cc.LabelTTF( "You Won " + DPUtils.getInstance().getNumString( this.totalFreeSpinWin ) + " Credits!", "HELVETICALTSTD-BOLD", 40 );
        freeSpinWinLabel.setPosition( node.getContentSize().width / 2, node.getContentSize().height * 0.5 );
        node.addChild( freeSpinWinLabel, 5 );

        this.addChild( node, 7 );
    },

    setpayLines : function()
    {
        var r = this.slots[0].blurReels[1];

        var payLineBG = new cc.Sprite( "#lines_disabled.png" );
        payLineBG.setPosition( r.getParent().getPosition() );
        payLineBG.setOpacity( 10 );
        this.slots[0].addChild( payLineBG );

        for ( var d = 1; d < 10; d++ )
        {
            var strin = "#pl" + d + ".png";
            var plSprite = new cc.Sprite( strin );
            plSprite.setPosition( r.getParent().getPosition() );
            plSprite.setOpacity( 0 );
            this.payLines.push( plSprite );
            this.slots[0].addChild( plSprite );
            this.scaleAccordingToSlotScreen( plSprite );
        }
    },

    setResultPayLines : function()
    {
        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        var payLines = [
            [0, 0, 0],
            [1, 1, 1],
            [2, 2, 2],
            [0, 1, 2],
            [2, 1, 0],
            [1, 0, 1],
            [1, 2, 1],
            [0, 1, 0],
            [2, 1, 2]
        ];

        for ( var i = 0; i < 9; i++ )
        {
            tmpVector2 = [];
            for ( var j = 0; j < 3; j++ )
            {
                tmpVector = this.slots[0].resultReel[j];
                var e = tmpVector[payLines[i][j]];
                tmpVector2.push( e );
                this.resArr[i][j] = e.eCode;
            }
            this.lineVector.push( tmpVector2 );
        }
    },

    setResultPayLines2 : function( veC )
    {
        var tmpLineVector = new Array();
        var tmpVector;
        var tmpVector2;

        var payLines = [
            [0, 0, 0],
            [1, 1, 1],
            [2, 2, 2],
            [0, 1, 2],
            [2, 1, 0],
            [1, 0, 1],
            [1, 2, 1],
            [0, 1, 0],
            [2, 1, 2]
        ];

        for ( var i = 0; i < 9; i++ )
        {
            tmpVector2 = [];
            for ( var j = 0; j < 3; j++ )
            {
                tmpVector = veC[j];
                var e = tmpVector[payLines[i][j]];
                tmpVector2.push( e );
            }
            tmpLineVector.push( tmpVector2 );
        }

        return tmpLineVector;
    },

    setFalseWin : function()
    {
    },

    setResult : function( indx, lane, sSlot )
    {
        var gap = 125;
        var wildIdx = -1;
        var j = 0;
        var animOriginY;

        var r;
        if ( this.freeSpinCount > 0 )
        {
            r = this.slots[0].wildReel[lane];
        }
        else
        {
            r = this.slots[0].physicalReels[lane];
        }
        var arrSize = r.elementsVec.length;
        var spr;
        var spriteVec = new Array();

        animOriginY = gap * r.direction * 1.5;
        spr = r.elementsVec[indx];

        if ( indx == 0 )
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push( spr );
            if ( spr.eCode == ELEMENT_WILD )
            {
                wildIdx = r.elementsVec.length - 1;
            }

            spr.setPositionY( animOriginY );

            for ( var i = 0; i < 2; i++ )
            {
                spr = r.elementsVec[i];
                spriteVec.push( spr );
                spr.setPositionY( animOriginY + gap * (i + 1) * r.direction );

                if ( spr.eCode == ELEMENT_WILD )
                {
                    wildIdx = i;
                }
            }
        }
        else if ( indx == arrSize - 1 )
        {
            spr = r.elementsVec[0];
            spriteVec.push( spr );

            if ( spr.eCode == ELEMENT_WILD )
            {
                wildIdx = 0;
            }

            spr.setPositionY( animOriginY );
            for ( var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i-- )
            {
                spr = r.elementsVec[i];
                if ( spr.eCode == ELEMENT_WILD )
                {
                    wildIdx = i;
                }
                spriteVec.push( spr );
                spr.setPositionY( animOriginY + gap * ( j + 1 ) * r.direction );
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];
            if ( spr.eCode == ELEMENT_WILD )
            {
                wildIdx = indx - 1;
            }
            spriteVec.push( spr );
            spr.setPositionY( animOriginY );

            for ( var i = indx; i < indx + 2; i++ )
            {
                spr = r.elementsVec[i];
                if ( spr.eCode == ELEMENT_WILD )
                {
                    wildIdx = i;
                }
                spriteVec.push( spr );
                spr.setPositionY( animOriginY + gap * ( j + 1 ) * r.direction );
                j++;
            }
        }

        for ( var i = 0; i < spriteVec.length; i++ )
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );

            gap = -312.5;

            gap*=r.direction;
            var ease;

            if ( this.slots[sSlot].isForcedStop )
            {
                ease = new cc.MoveTo(0.15, cc.p(0, e.getPositionY() + gap)).easing(cc.easeBackOut());
            }
            else
            {
                ease = new cc.MoveTo(0.3, cc.p(0, e.getPositionY() + gap)).easing(cc.easeBackOut());
            }
            ease.setTag( MOVING_TAG );
            e.runAction( ease );

            if ( e.eCode == ELEMENT_SPIN )
            {
                this.wildElemnts.push( e );
                if ( lane == 0 )
                {
                    this.delegate.jukeBox( S_FREESPIN_E );
                    e.runAction( new cc.Sequence( new cc.DelayTime( 0.3 ), new cc.FadeTo( 0.2, 100 ), new cc.FadeTo( 0.2, 255 ) ) );
                }
            }
            else if( this.wildLaneCode[lane] <= 0 )
            {
                if ( e.eCode == ELEMENT_WILD )
                {
                    this.doWildMiraz = true;
                    this.wildLaneCode[lane] = 2;
                    this.wildIndx[lane] = i;
                }
            }
        }

        if ( wildIdx != -1 )
        {
            for ( var i = 0; i < 3; i++ )
            {
                spr = spriteVec[i];
                spr.runAction( new cc.Sequence( new cc.DelayTime( 1 ), new cc.FadeOut( 0.75 ) ) );//17Feb
            }

            spriteVec = [];
            for ( var i = 0; i < 3; i++ )
            {
                spr = r.elementsVec[wildIdx];
                spriteVec.push( spr );
            }
        }

        return spriteVec;
    },

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSFreeSpins;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    setWildMiraj : function( lane, indx )
    {
        var tim = 0.2;

        var wildSprite = this.slots[0].wildSpriteVec[lane];
        wildSprite.setSpriteFrame( "9.png" );
        wildSprite.setScaleY( 0.33 );
        switch ( indx )
        {
            case 0://down
                wildSprite.setAnchorPoint( cc.p( 0.5, 0 ) );
                wildSprite.setPositionY( wildSprite.getPositionY() - wildSprite.getContentSize().height / 2 );
                break;

            case 1://middle
                wildSprite.setAnchorPoint( cc.p( 0.5, 0.5 ) );
                break;

            case 2://up
                wildSprite.setAnchorPoint( cc.p( 0.5, 1 ) );
                wildSprite.setPositionY( wildSprite.getPositionY() + wildSprite.getContentSize().height / 2 );
                break;

            default:
                break;
        }
        wildSprite.runAction( new cc.Sequence( new cc.ScaleTo( tim, 1 ), new cc.callFunc( function(){
            if ( !this.calculationsAssigned )
            {
                this.calculatePayment( true );
            }
            this.calculationsAssigned = true;
        }, this ) ) );

    },

    setWinAnimation : function( animCode, screen )
    {
        var tI = 0.3;

        var fire;

        for ( var i = 0; i < this.animNodeVec.length; i++ )
        {
            fire = this.animNodeVec[i];
            fire.removeFromParent( true );
        }
        this.animNodeVec = [];


        switch ( animCode )
        {
            case WIN_TAG:

                for (var i = 0; i < this.slots[0].blurReels.length; i++)
                {
                    var r = this.slots[0].blurReels[i];

                    var node = r.getParent();

                    var moveW = this.slots[0].reelDimentions.x * this.slots[0].getScale();
                    var moveH = this.slots[0].reelDimentions.y * this.slots[0].getScale();

                    for ( var j = 0; j < 4; j++ )
                    {

                        var fire = new cc.ParticleSun();
                        var pos;

                        fire.texture = cc.textureCache.addImage( res.FireStar_png );


                        this.addChild( fire, 5 );
                        this.animNodeVec.push( fire );


                        var moveBy = null;

                        var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                        diff*=( 2 - i );

                        switch ( j % 4 )
                        {
                            case 0:
                                pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition( pos );
                                moveBy = new cc.MoveBy( tI, cc.p( moveW, 0 ) );
                                break;

                            case 1:
                                pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition( pos );
                                moveBy = new cc.MoveBy( tI, cc.p( 0, moveH ) );
                                break;

                            case 2:
                                pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition( pos );
                                moveBy = new cc.MoveBy( tI, cc.p( -moveW, 0 ) );
                                break;

                            case 3:
                                pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition( pos );
                                moveBy = new cc.MoveBy( tI, cc.p( 0, -moveH ) );
                                break;

                            default:
                                break;
                        }
                        fire.runAction( new cc.RepeatForever( new cc.Sequence( moveBy, moveBy.reverse() ) ) );
                    }
                }

                break;

            case BIG_WIN_TAG:
                for( var i = 0; i < this.slots[0].blurReels.length; i++ )
                {
                    var r = this.slots[0].blurReels[i];

                    var node = r.getParent();

                    var glowSprite;
                    var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                    diff*=( 2 - i );

                    if ( i == 0 )
                    {
                        glowSprite = new cc.Sprite( glowStrings[3] );
                    }
                    else if ( i == 1 )
                    {
                        glowSprite = new cc.Sprite( glowStrings[0] );
                    }
                    else
                    {
                        glowSprite = new cc.Sprite( glowStrings[1] );
                    }

                    if ( glowSprite )
                    {
                        glowSprite.setPosition( node.getPositionX() + this.slots[0].getPositionX() - diff, node.getPositionY() + this.slots[0].getPositionY() );

                        this.addChild( glowSprite, 5 );
                        this.animNodeVec.push( glowSprite );
                        glowSprite.setOpacity( 100 );
                        this.scaleAccordingToSlotScreen( glowSprite );
                        glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.3, 255 ), new cc.DelayTime( 0.3 ), new cc.FadeTo( 0.3, 100 ) ) ) );
                    }
                }
                break;

            default:
                break;
        }
    },

    spin : function()
    {

        for ( var i = 0; i < this.payLines.length; i++ )
        {
            var sprit = this.payLines[i];
            sprit.stopAllActions();
            sprit.setOpacity( 0 );
        }
        this.activePayLines = [];

        for ( var i = 0; i < 3; i++ )
        {
            this.wildLaneCode[i] = 0;
        }
        this.unschedule( this.updateWinCoin );
        this.deductBet( this.currentBet );
        if ( this.displayedScore != this.totalScore )
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        this.doWildMiraz = false;
        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;
        this.hadTripleWild = false;

        this.ancientScore = -1;
        this.totalFreeSpinWin = 0;

        this.wildElemnts = [];

        var veC = this.slots[0].resultReelAncient;
        var vecC;

        for ( var i = 0; i < veC.length; i++ )
        {
            vecC = veC[i];
            for ( var j = 0; j < vecC.length; j++ )
            {
                var sprite = vecC[j];
                sprite.stopAllActions();
                sprite.runAction( new cc.FadeOut( 0.2 ) );
            }
        }

        this.currentWin = 0;
        this.currentWinForcast = 0;
        this.startRolling();


        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    },

    startFreeSpin : function()
    {
        this.freeSpinCount--;
        this.isRolling = true;
        this.doWildMiraz = false;
        this.wildElemnts = [];
        for ( var i = 0; i < this.payLines.length; i++ )
        {
            var sprit = this.payLines[i];
            sprit.stopAllActions();
            sprit.setOpacity( 0 );
        }
        this.activePayLines = [];

        for ( var i = 0; i < this.slots[0].wildSpriteVec.length; i++ )
        {
            var sprite = this.slots[0].wildSpriteVec[i];
            var dNode = sprite.getChildByTag( D_NODE_TAG );
            dNode.clear();
        }


        for ( var i = 0; i < 3; i++ )
        {
            if ( this.hadTripleWild )
            {
                this.resetWildMiraj( i );
            }
            else
            {
                if ( this.wildLaneCode[i] > 0 )
                {
                    this.wildLaneCode[i]--;

                    var sprite = this.slots[0].wildSpriteVec[i];
                    sprite.setSpriteFrame( "11.png" );

                    if ( this.wildLaneCode[i] <= 0 )
                    {
                        this.resetWildMiraj( i );
                    }
                }
            }
        }

        this.hadTripleWild = false;

        this.startRolling( true );

        var labl = this.getChildByTag( BOTTOM_BAR_TAG ).getChildByTag( 1 );
        labl.setString( "Free Spins  " + ( this.freeSpinCount - 1 ) );
    },

    startPayLineAnim : function()
    {
        this.currentPayLineAnimIndx = 0;

        if ( this.activePayLines.length )
        {
            var tmpVec = this.activePayLines[this.currentPayLineAnimIndx];

            var e;
            for ( var i = 0; i < tmpVec.length; i++ )
            {
                e = tmpVec[i];
                e.aCode = SCALE_TAG;
                e.animateNow();
                e.setLocalZOrder( 2 );
            }

            var sprit = this.payLines[this.activePayLinesIndx[this.currentPayLineAnimIndx]];

            sprit.runAction( new cc.Sequence( new cc.FadeIn( 0.5 ), new cc.DelayTime( 1.0 ), new cc.FadeOut( 0.5 ), new cc.callFunc( this.payLineAnimCB, this ) ) );

            this.setDNodeOnWildMiraj( this.activePayLinesIndx[this.currentPayLineAnimIndx] );
        }
    }

});