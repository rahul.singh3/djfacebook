/**
 * Created by RNF-Mac11 on 1/11/17.
 */

var THIS_LEVEL_BET =  "thisLevelBet";
var LEVEL =  "level";
var LEVEL_BAR =  "levelBar";
var BET =  "bet";
//var CHIPS =  "chips";
var BONUS_DAY =  "bonus_day";
var BONUS_HOUR =  "bonus_hour";
var BONUSX =  "bonusX";
var MUSIC =  "music";
var SOUND =  "sound";
var VIP_POINTS =  "vip";
var CURRENT_SCENE_CODE =  "current_scene_code";
var APP_RATE_STARS =  "app_rate_fb_stars";
var LAST_PLAYED_THEME =  "last_played";
var AREARS =  "arear_coins";
var EASY_MODE_BET =  "easyModeBet";
var EASY_MODE_BET_300 =  "easyModeBet_300";
var EASY_MODE_WIN_CHIPS_300 =  "easyModeWinChips_300";
var EASY_MODE_ASSIGNED =  "easy_mode_assigned";
var TOTAL_PLAYED =  "totalplayed";
var RATING_SHOWN_COUNT =  "fb_rating_shown_count";
var IS_BUYER =  "is_buyer";
var SESSION_COUNT =  "session_count";
var APP_VERSION_NAME =  "app_version";
var HAPPY_HOURS =  "happy_hours";
var NEXT_HAPPY_HOUR_IN =  "next_happy_hour_in";
var DOLLARS_SPENT =  "dollars_spent";//todo ask for its use
var MINIMUM_EASY_MODE_BET_LIMIT =  "minimum_easy_mode_bet_limit";
var REAL_MONEY_ADV_CLICKED =  "real_money_adv_clicked";
var REAL_MONEY_ADV_CLICK_COUNT =  "real_money_adv_click_count";//change 12 Dec
var MAX_BET_PLACED =  "maxbetplaced";
var BUY_BUTTON_CLICK_COUNT =  "buybuttonclickcount";
var MAXIMUM_WIN_MACHINE =  "maximumwinmachine";
var MAXIMUM_WIN = "maximum_win";
var PREVIOUS_REQUEST_IDS =  "previous_request_ids";//requested; might be accepted
var ACCEPTED_REQUEST_IDS =  "accepted_request_ids";//accepted friend list; people who have accepted your request
var SERVER_OCCASION = "server_occasion";
var everUsedSpecialPopup = "everUsedSpecialPopup";
var WIN_TOURNAMENT = "win_tournament";
var WIN_10X = "win_10x";
var MACHINE_TUNNING = "machine_tunning";
var FB_REWARDS_ASSIGNMENT_TIME = "fb_rewards_assignment_time";
var INVITE_POP_INVOCATION_COUNT = "invitePopInvocationCount";
var GIFTS_MAP = "gifts_map";


//deal UserDefault start
var DEAL_END_TIME =  "deal_end_time";
var DEAL_START_TIME =  "deal_start_time";
var DEAL_REWARD_CHIPS =  "deal_reward_chips";
var DEAL_PRODUCT_ID =  "deal_product_id";
var DEAL_BACKGROUND =  "deal_background";
var BUYER_DEAL_REFLUSH_TIME =  "buyer_deal_reflush_time";
//deal UserDefault end


var JACKPOT_START_TIME          = "jackpot_start_time";
var JACKPOT_INITIAL_VALUE       = "jackpot_initial_value";
var JACKPOT_FINAL_VALUE         = "jackpot_final_value";
var JACKPOT_INCREMENT_FACTOR    = "jackpot_increment_factor";
var JACKPOT_LAST_WINNING        = "jackpot_last_winning";
var JACKPOT_WIN_COUNT           = "jackpot_win_count";

var GRAND_WHEEL_BET_AMOUNT      = "grand_wheel_bet_amount";
var GRAND_WHEEL_SPIN_COUNT      = "grand_wheel_spin_count";
var GRAND_WHEEL_SPIN_TO_MAKE    = "grand_wheel_spin_to_make";

var SERVER_MACHINE_UNAVAILABLE  = "machine_unavailable";

var STRATEGY_ASSIGNED           = "strategy_assigned";
var IAP_COUNT                   = "iap_count";
var BET_LOWER_LIMIT             = "bet_lower_limit";

var UserDef = (function () {
    // Instance stores a reference to the Singleton
    var instance;

    function init() {
        // Singleton
        // Private methods and variables

        return {
            // Public methods and variables
            /*publicMethod: function () {
             console.log( "The public can see me!" );
             },
             publicProperty: "I am also public",
             getRandomNumber: function() {
             return privateRandomNumber;
             }*/
            storageContainer : null,//Change 16 May
            showSaleViewCount : 0,
            SESSION_BET : 0,

            setValueForKey : function ( key, value )
            {
                /*cc.log( "key = " + key );
                if( key == "1" )
                {
                    cc.log( "Break here" );
                }*/
                this.storageContainer.set( key, value );
                //cc.log( "Arears = " + this.storageContainer.get( AREARS ) + " at " + key );
            },
            getValueForKey :function( key, defValue )
            {
                var value = this.storageContainer.get( key );
                if( value === undefined )
                {
                    if( defValue === undefined )
                    {
                        value = 0;
                        /*if( key == CHIPS )
                        {
                            value = 5000000;
                        }*/
                    }
                    else
                    {
                        value = defValue;
                    }
                    //this.storageContainer.set( key, value );
                }
                return value;
            },
            deleteValueForKey : function ( key )
            {
                this.storageContainer.delete( key );
            }
        };
    };
    return {
        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function () {
            if ( !instance ) {
                instance = init();
                instance.storageContainer = new Map();//change 16 MAy
            }
            return instance;
        }
    };
})();