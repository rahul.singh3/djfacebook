/**
 * Created by RNF-Mac11 on 1/20/17.
 */
CSElemnt = cc.Sprite.extend({

    elementIndex : 0,
    machineCode : 0,
    laneCode : 0,
    eCode : 0,
    aCode : 0,

    ctor:function ( mCode, eCde, eIndex, lane ) {
        this._super();
        this.eCode = eCde;
        this.elementIndex = eIndex;
        this.machineCode = mCode;
        this.laneCode = lane;
        return this.initialize( );
    },
    initialize : function()
    {
        var string = null;
        var str = null;
        var delegate = AppDelegate.getInstance();
        switch ( this.eCode )
        {
            case ELEMENT_X:
                string = "x.png";
                str = "x";
                break;

            case ELEMENT_2X:
                string = "2x.png";
                str = "2x";
                break;

            case ELEMENT_3X:
                string = "3x.png";
                str = "3x";
                break;

            case ELEMENT_4X:
                string = "4x.png";
                str = "4x";
                break;

            case ELEMENT_5X:
                string = "5x.png";
                str = "5x";
                break;

            case ELEMENT_7X:
                string = "7x.png";
                str = "7x";
                break;

            case ELEMENT_10X:
                string = "10x.png";
                str = "10x";
                break;

            case ELEMENT_12X:
                string = "12x.png";
                str = "12x";
                break;

            case ELEMENT_15X:
                string = "15x.png";
                str = "15x";
                break;

            case ELEMENT_20X:
                string = "20x.png";
                str = "20x";
                break;

            case ELEMENT_25X:
                string = "25x.png";
                str = "25x";
                break;

            case ELEMENT_40X:
                string = "40x.png";
                str = "40x";
                break;

            case ELEMENT_50X:
                string = "50x.png";
                str = "50x";
                break;

            case ELEMENT_2XX:
            case ELEMENT_3XX:
                if ( Math.floor( Math.random() * 8487 ) >= 50)
                {
                    string = "243.png";
                }
                else
                {
                    string = "253.png";
                }
                break;

            case ELEMENT_4XX:
                string = "243.png";
                break;

            case ELEMENT_5XX:
                string = "253.png";
                break;

            case ELEMENT_EMPTY:
                string = "res/empty.png";
                break;

            case ELEMENT_BONUS:
                if (this.laneCode == 2)
                {
                    string = ( this.eCode - ELEMENT_7_BAR + 1 ) + ".png";
                    str =  ( this.eCode - ELEMENT_7_BAR + 1 ) + "";
                }
                else
                {
                    string = "17.png";
                    str = "17";
                }
                break;

            case ELEMENT_SPIN_TILL_YOU_WIN:
                if ( this.laneCode == 1 )
                {
                    string = "spinElm.png";
                    str = "spinElm";
                }
                else
                {
                    string = "3x.png";
                }
                break;

            default:
                string = ( this.eCode - ELEMENT_7_BAR + 1 ) + ".png";
                str = ( this.eCode - ELEMENT_7_BAR + 1 ) + "";
                break;
        }

        //PolygonInfo pinfo = AutoPolygon::generatePolygon( string.getCString() );
        //////////////////////////////
        // 1. super init first


        if ( ! DPUtils.getInstance().strcasecmp( string, "res/empty.png" ) )
        {
            if ( !this.initWithFile( string ) )
            {
                return false;
            }
        }
        else
        {
            if ( !this.initWithSpriteFrameName( string ) )
            {
                //return false;
            }
        }

        if ( this.machineCode == DJS_X_MULTIPLIERS )
        {
            this.setScale(0.5);
        }

        if ( ELEMENT_EMPTY == this.eCode)
        {
            this.setVisible(false);
        }

        if ( str )
        {
            var makeGaf = false;
            switch ( this.machineCode ) {
                case DJS_BONUS_MACHINE:
                case DJS_FREEZE:
                case DJS_GOLDEN_SLOT:
                case DJS_MAGNET_SLIDE:
                case DJS_NEON_SLOT:
                case DJS_CRYSTAL_SLOT:
                case DJS_HALLOWEEN_SLOT:
                    makeGaf = true;
                    break;

                case DJS_SPIN_TILL_YOU_WIN:
                    if ( ( this.eCode == ELEMENT_SPIN_TILL_YOU_WIN && this.laneCode == 1 ) || this.eCode == ELEMENT_2X || this.eCode == ELEMENT_3X ) {
                        makeGaf = true;
                    }
                    break;

                case DJS_FIRE:
                    switch ( this.eCode )
                    {
                        case ELEMENT_CHERRY:
                        case ELEMENT_7_BAR:
                            makeGaf = true;
                            break;

                        default:
                            break;
                    }
                    break;

                case DJS_QUINTUPLE_5X:
                    switch ( this.eCode )
                    {
                        case ELEMENT_2X:
                        case ELEMENT_3X:
                        case ELEMENT_5X:
                        case ELEMENT_10X:
                            makeGaf = true;
                            break;

                        default:
                            break;
                    }
                    break;

                default:
                    break;
            }
            if( makeGaf )
            {
                //GAFObject * gafObject = gaf::GAFAsset::create( __String::createWithFormat( "%s%s/%s/%s.gaf", delegate.resourcesPathPrefix.c_str(), machinesString[machineCode - DJS_QUINTUPLE_5X], str.getCString(), str.getCString() ).getCString() ).createObjectAndRun( true );
                var gafObject = gaf.Asset.create( "res/" + machinesString[this.machineCode - DJS_QUINTUPLE_5X] + "/" + str + "/" + str + ".gaf" ).createObjectAndRun( true );
                gafObject.setPosition( cc.p( 0, this.getContentSize().height ) );
                gafObject.setTag( GAF_OBJECT_TAG );

                gafObject.setVisible( false );

                gafObject.stop();

                this.addChild( gafObject );
            }
        }

        return true;
    },

    animateNow : function()
    {
        var gafObject = this.getChildByTag( GAF_OBJECT_TAG );
        //cc.log( "this.elementIndex = " + this.elementIndex );
        if ( gafObject && ( this.aCode == SCALE_TAG || this.aCode == GLOW_TAG ) )
        {
            this.setRotationX( 0 );
            this.setRotationY( 0 );
            gafObject.setVisible( true );
            gafObject.start();
            this.stopAllActions();

            if( this.machineCode == DJS_HALLOWEEN_SLOT )
            {
                var delegate = AppDelegate.getInstance();
                if( this.eCode == ELEMENT_CHERRY )
                {
                    delegate.jukeBox( S_PUMPKIN );
                }
            }

            this.setOpacity( 0 );
            //this.setVisible( false );
        }
        else
        {
            switch (this.aCode)
            {
                case SCALE_TAG:
                    if( this.machineCode == DJS_FREE_SPIN )
                    {
                        this.runAction( new cc.RepeatForever( new cc.Sequence( new cc.ScaleTo(0.5, 1.1), new cc.ScaleTo(0.5, 1) ) ) );
                    }
                    else
                    {
                        if ( this.eCode == ELEMENT_BONUS )
                        {
                            var sprite = new cc.Sprite( res.GlowIconPng );
                            sprite.setPosition( this.getContentSize().width / 2, this.getContentSize().height / 2 );
                            this.addChild(sprite);
                            sprite.setScale(0);
                            sprite.setOpacity(150);
                            sprite.runAction( new cc.Sequence( new cc.ScaleTo( 0.4, 2.5 ), cc.callFunc( function( sprite ){ sprite.removeFromParent() }, this ) ) );
                        }
                        this.runAction( new cc.RepeatForever( new cc.Sequence( new cc.ScaleTo( 0.5, 1.1 ), new cc.ScaleTo( 0.5, 1 ) ) ) );
                    }
                    break;

                case GLOW_TAG:
                {
                    var sprite = new cc.Sprite( res.GlowIconPng );
                    sprite.setPosition( this.getContentSize().width / 2, this.getContentSize().height / 2 );
                    this.addChild( sprite );
                    sprite.setScale( 0 );
                    sprite.setOpacity( 200 );
                    sprite.runAction( new cc.Sequence( new cc.ScaleTo( 0.4, 2.5 ), cc.callFunc( function( ){ sprite.removeFromParent() }, this, sprite ) ) );
                }
                    break;

                default:
                    break;
            }
        }
    }
});

/*
 CSElemnt = cc.Sprite.extend({

 ctor:function ( mCode, eCde, eIndex, lane ) {
 this._super();

 return true;


 },

 });
 */