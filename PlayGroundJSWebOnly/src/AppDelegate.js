/**
 * Created by RNF-Mac11 on 1/13/17.
 */
/*var AppDelegate = (function(){

 var instance;
 var haveHourBonus;
 var appJustLaunched;
 var changingScene;
 var this.sPlayer;
 var trackIds = [];
 function AppDelegate() {
 //do stuff
 //console.log("Let me call once.");
 //alert('Let me call once.');
 this.sPlayer = cc.audioEngine;

 }


 return {
 getInstance: function(){
 if (instance == null) {
 instance = new AppDelegate();
 // Hide the constructor so the returned objected can't be new'd...
 instance.constructor = null;
 }
 return instance;
 }
 };

 return {

 }
 };



 })();*/

var AppDelegate = (function () {
    // Instance stores a reference to the Singleton
    var instance;
    // var sPlayer;

    var trackIds = new Array(TOTAL_SOUNDS);
    function init() {
        // Singleton
        // Private methods and variables
        function privateMethod(){
            //console.log( "I am private" );
        }
        var privateVariable = "Im also private";
        var privateRandomNumber = Math.random();
        return {
            // Public methods and variables
            /*publicMethod: function () {
             console.log( "The public can see me!" );
             },
             publicProperty: "I am also public",
             getRandomNumber: function() {
             return privateRandomNumber;
             }*/
            initApp : function () {
                //var spriteFrameCache = cc.spriteFrameCache;
                //spriteFrameCache.addSpriteFrames( res.Tournament_Lobby_plist, res.Tournament_Lobby_png );

                //this.initiateTournament( 0 );
                this.logInTicker = 0;
                this.sPlayer = cc.audioEngine;
                //this.sPlayer.playMusic(soundFiles.Sound_Ambience_mp3, true);

            },
            stopTournament : true,
            showNewTournament : false,
            dealFlushed : false,
            sPlayer : null,
            onForGround : true,
            tournaments : new Array(),
            logInTicker : 0,
            configInTicker:0,
            spinCount : 0,
            invitation_rewards_assigned : false,
            initiateTournament : function( dt )
            {
                var spriteFrameCache = cc.spriteFrameCache;

                spriteFrameCache.addSpriteFrames( res.Tournament_Lobby_plist, res.Tournament_Lobby_png );


                this.stopTournament = true;
                this.showNewTournament = false;
                for ( var i = 0; i < 3; i++ )
                {
                    this.tournaments[i] = new CSTournament( i + 1 );
                    this.tournaments[i].retain();
                }
            },
            releaseTournament : function()
            {
                if( this.tournaments[0] != null )
                {
                    for ( var i = 0; i < 3; i++ )
                    {
                        this.tournaments[i].removeFromParent( true );
                        this.tournaments[i].release();
                        this.tournaments[i] = null;
                    }
                    this.stopTournament = false;
                    this.showNewTournament = true;
                }
            },
            tournamentToBG : function()
            {
                var time_in_seconds = ServerData.getInstance().current_time_seconds;
                CSUserdefauts.getInstance().setValueForKey( "time_from_background", time_in_seconds );
            },
            stopTournament : true,
            showNewTournament : false,
            tournamentToFG : function()
            {
                var def = CSUserdefauts.getInstance();
                var timeStr = def.getValueForKey( "time_from_background", 0 );
                if ( !timeStr )
                {
                    return;
                }
                var time1 = this.getTime( "time_from_background" );
                var time_in_seconds = ServerData.getInstance().current_time_seconds;
                var resume_seconds = time_in_seconds - time1;

                if ( resume_seconds >= 0 && this.stopTournament && this.tournaments[0] && ( this.tournaments[0].m * 60 + this.tournaments[0].s ) > resume_seconds ) {
                    var min = parseInt( resume_seconds/60 );
                    var sec = resume_seconds%60;
                    if ( this.tournaments[0].s - sec <= 0 ) {
                        min++;
                        sec = sec - this.tournaments[0].s;
                        this.tournaments[0].s = 60;
                        this.tournaments[1].s = 60;
                        this.tournaments[2].s = 60;
                    }
                    this.tournaments[0].m-=min;
                    this.tournaments[0].s-=sec;

                    this.tournaments[1].m-=min;
                    this.tournaments[1].s-=sec;

                    this.tournaments[2].m-=min;
                    this.tournaments[2].s-=sec;

                    while ( resume_seconds > 0 )
                    {
                        if ( resume_seconds > 5 )  //5sec is for tournamentBotUpdateTime
                        {
                            resume_seconds-=5;
                            this.tournaments[0].remainSec-=5;
                            this.tournaments[0]._currentCount+=5;
                            this.tournaments[0].updateBotsPoints(this.tournaments[0].remainSec/5);
                            this.tournaments[1].remainSec-=5;
                            this.tournaments[1]._currentCount+=5;
                            this.tournaments[1].updateBotsPoints(this.tournaments[1].remainSec/5);
                            this.tournaments[2].remainSec-=5;
                            this.tournaments[2]._currentCount+=5;
                            this.tournaments[2].updateBotsPoints(this.tournaments[2].remainSec/5);
                        }
                        else
                        {
                            break;
                        }

                    }

                }
                else
                {
                    this.releaseTournament();
                    var scene = cc.director.getRunningScene();
                    var layer = scene;//.getChildByTag( sceneTags.GAME_SCENE_TAG );
                    if ( layer && layer.getTag() == sceneTags.GAME_SCENE_TAG )//change 12 June
                    {
                        var game = layer;

                        game.tournamentPause = true;
                        if ( game.mCode != DJS_X_MULTIPLIERS )
                        {
                            game.hideWaitingTournament();
                            game.connectingTournament(0);
                        }
                    }
                }

            },
            assignRewards : function( rewards,updateCoins )
            {

                var scene = cc.director.getRunningScene();

                var layer = scene.getChildByTag( sceneTags.LOBBY_SCENE_TAG );

                if ( layer/*.getTag() == sceneTags.LOBBY_SCENE_TAG*/ )
                {
                    var lobby = layer;
                    lobby.displayedScore = lobby.totalScore;
                    lobby.totalScore = lobby.totalScore + rewards;
                    lobby.addScore = ( lobby.totalScore - lobby.displayedScore ) / 10;
                    userDataInfo.getInstance().totalChips = lobby.totalScore;
                    if (updateCoins)
                        ServerData.getInstance().updateData("coins",userDataInfo.getInstance().totalChips);
                    lobby.schedule( lobby.updateWinCoin, 0.08 );
                    lobby.addSurpiseBonusCoinsLbl(rewards);
                }
                else
                {
                    layer = scene;//.getChildByTag( sceneTags.GAME_SCENE_TAG );
                    if ( layer.getTag() == sceneTags.GAME_SCENE_TAG )
                    {
                        var game = layer;
                        game.displayedScore = game.totalScore;
                        game.totalScore = game.totalScore + rewards;
                        game.addScore = ( game.totalScore - game.displayedScore ) / 10;
                        userDataInfo.getInstance().totalChips = game.totalScore;
                        if (updateCoins)
                            ServerData.getInstance().updateData("coins",userDataInfo.getInstance().totalChips);
                        game.schedule( game.updateCoin, 0.08 );
                        game.addSurpiseBonusCoinsLbl(rewards);

                    }
                }
                this.jukeBox( S_COIN );
            },

            jukeBox : function ( trackId, isLooped ) {

                var pSound = true;
                if( isLooped === undefined )        isLooped = false;

                switch (trackId) {
                    case S_REEL_STOP:
                    case S_BTN_CLICK:
                        pSound = CSUserdefauts.getInstance().getValueForKey( SOUND, true );
                        break;

                    default:
                        pSound = CSUserdefauts.getInstance().getValueForKey( MUSIC, true );
                        break;
                }

                if (pSound) {

                    switch (trackId) {
                        case S_AMBIENCE:
                            this.sPlayer.playMusic(soundFiles.Sound_Ambience_back_mp3, true);
                            break;

                        case S_WHEEL_BG:
                            this.sPlayer.playMusic( soundFiles.Sound_Bonus_bg_mp3, true );
                            break;

                        case S_BONUS_POP:
                            this.sPlayer.playMusic( soundFiles.Sound_Bonus_pop_mp3, false );
                            break;

                        case S_WILD:
                            this.sPlayer.playMusic( soundFiles.Sound_Wild_mp3, true );
                            break;

                        case S_LOBBY:
                            this.sPlayer.playMusic(soundFiles.Sound_Ambience_mp3, false);
                            break;

                        case S_LEVEL_UP:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_level_up_mp3 );
                            break;

                        case S_REEL_STOP:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_reel_stop_mp3 );
                            break;

                        case S_EXCITEMENT:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_excitement_mp3 );
                            break;

                        case S_REEL_1:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_reel_1_mp3 );
                            break;

                        case S_REEL_2:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_reel_2_mp3 );
                            break;

                        case S_REEL_3:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_reel_3_mp3 );
                            break;
                        case S_REEL_4:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_reel_4_mp3 );
                            break;

                        case S_REEL_5:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_reel_5_mp3 );
                            break;

                        case S_REEL_6:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_reel_6_mp3 );
                            break;
                        case S_REEL_7:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_reel_7_mp3 );
                            break;

                        case S_REEL_8:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_reel_8_mp3 );
                            break;

                        case S_REEL_9:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_reel_9_mp3 );
                            break;
                        case S_REEL_10:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_reel_10_mp3 );
                            break;

                        case S_REEL_11:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_reel_11_mp3 );
                            break;

                        case S_REEL_12:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_reel_12_mp3 );
                            break;
                        case S_REEL_13:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_reel_13_mp3 );
                            break;

                        case S_REEL_14:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_reel_14_mp3 );
                            break;

                        case S_REEL_15:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_reel_15_mp3 );
                            break;
                        case S_GIFT_BOX_OPEN:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.S_GIFT_BOX_OPEN );
                            break;
                        case S_MISSION_OPEN:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.S_MISSION_OPEN );
                            break;
                        case S_SLIDE_1:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_Slide_1 );
                            break;

                        case S_SLIDE_2:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_Slide_2 );
                            break;

                        case S_SLIDE_3:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_Slide_3 );
                            break;

                        case S_COIN:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_coins_mp3 );
                            break;

                        case S_COIN_MORE:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_coin_more_mp3 );
                            break;

                        case S_BTN_CLICK:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.Sound_btn_click_mp3 );
                            break;

                        case S_FLASH:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_FLASH );
                            break;

                        case S_REVERSAL:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_REVERSAL );
                            break;

                        case S_RESPIN:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_RESPIN );
                            break;

                        case S_RESPIN_ROLL:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_RESPIN_ROLL );
                            break;

                        case S_BONUS_WIN:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_BONUS_WIN, true );
                            break;

                        case S_XX_WILD:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_XX_WILD, true );
                            break;

                        case S_WHEEL:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_WHEEL, true );
                            break;

                        case S_FREESPIN:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_FREESPIN );
                            break;

                        case S_FREESPIN_E:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_FREESPIN_E );
                            break;

                        case S_SPINTILLWIN:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_SPIN_TILL_WIN );
                            break;

                        case S_DJS_FIRE:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_DJS_FIRE );
                            break;

                        case S_ICE_FREEZE:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_ICE_FREEZE );
                            break;

                        case S_MEGA_WIN:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_MEGA_WIN );
                            break;

                        case S_STATIC:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_STATIC );
                            break;

                        case S_STATIC_WIN:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_STATIC_WIN );
                            break;

                        case S_PUMPKIN:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_PUMPKIN );
                            break;

                        case S_WITCH:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_WITCH );
                            break;

                        case S_ZOMBIE:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_ZOMBIE );
                            break;

                        case S_JACKPOT_WIN:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_JACKPOT_WIN );
                            break;

                        case S_BIG_WIN_COINS_TONE:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_BIG_WIN_COINS_TONE );
                            break;

                        case S_HUGE_WIN_COINS:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_HUGE_WIN_COINS );
                            break;

                        case S_BIG_WIN_COINS:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_BIG_WIN_COINS );
                            break;

                        case S_BIG_WIN_SOUND:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_BIG_WIN_SOUND );
                            break;

                        case S_GRAND_WIN_COINS:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_GRAND_WIN_COINS );
                            break;

                        case S_HUGE_WIN_COINS_TONE:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_HUGE_WIN_COINS_TONE );
                            break;

                        /*case S_BIG_WIN_COINS_TONE:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_BIG_WIN_COINS_TONE );
                            break;*/

                        case S_GRAND_WIN_COINS_TONE:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_GRAND_WIN_COINS_TONE );
                            break;

                        case S_PAID_WHEEL_BG:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_PAID_WHEEL_BG );
                            break;

                        case S_PAID_WHEEL_SPIN:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_PAID_WHEEL_SPIN );
                            break;

                        case S_WHEEL_BONUS:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_WHEEL_BONUS );
                            break;

                        case S_WHEEL_BONUS_SHORT:
                            trackIds[trackId - S_AMBIENCE] = this.sPlayer.playEffect( soundFiles.SOUND_WHEEL_BONUS_SHORT );
                            break;

                        case S_STOP_EFFECTS:
                            this.sPlayer.stopAllEffects();
                            break;

                        case S_STOP_MUSIC:
                            this.sPlayer.stopMusic();
                            break;

                        default:
                            break;
                    }
                }
            },
            jukeBoxStop : function ( trackId )
            {
                this.sPlayer.stopEffect( trackIds[trackId - S_AMBIENCE] );
            },
            showingScreen:false,
            marketHitted : false,
            _haveHourBonus : false,
            appJustLaunched : true,
            _dailyBonus : -1,
            _haveDailyBonus : false,
            offerData : false,
            levelUpVIP : 0,
            levelUpScore : 0,
            showLevelUp : true,
            showMachineUnlocked : false,
            unlockedMachineCode: 0,
            _percent : 0,
            invitableFriendsList : new Array(),
            showHappyHoursPop : false,
            appJustLaunchedForInstantDeal:true,
            updateTime : function()
            {
                var totalTime = this.chkForHourlyBonus();
                this.chkForDailyBonus();
                totalTime = 3600 - totalTime;

                var cTime = totalTime;

                var hour = parseInt(totalTime / 3600);
                totalTime%=3600;
                var min = parseInt(totalTime / 60);
                var sec = totalTime % 60;

                var secStr = null;
                var minStr = null;
                var hourStr = null;

                sec < 10 ? secStr = "0" + sec : secStr = "" + sec;


                min < 10 ? minStr = "0" + min : minStr = "" + min;


                hour < 10 ? hourStr = "0" + hour : hourStr = "" + hour;

                var string = hourStr + ":" + minStr + ":" + secStr;

                if ( cTime < 3600 )
                {
                    this._percent = ( ( ( 3600.0 - cTime ) / 3600.0 ) * 100.0 );
                }
                else
                {
                    this._percent = 100 - ( ( ( cTime - 3600.0 ) / cTime ) * 100.0 );
                }
                return string;
            },
            getTime : function( key )
            {
                var time = 0;
                var def = CSUserdefauts.getInstance();
                var t = def.getValueForKey( key, 0 );
                time = t;
                /*for ( var i = 0; i < t.length; i++ )
                 {
                 time = time * 10 + ( t[ i ] - 48 );
                 }*/

                if ( key == HAPPY_HOURS )
                {
                    if ( time <= 0 )
                    {
                        time = -1;
                    }
                }
                return time;
            },
            chkForDailyBonus : function()
            {
                var def = UserDef.getInstance();
                this._dailyBonus = def.getValueForKey( BONUSX, -1 );

                /*if( this._dailyBonus == -1 )//#changed as per discussion with Raghib on Nov 20 2017
                {
                    def.setIntegerForKey( BONUSX, 1 );
                    time_t t= time( 0 );
                    setTime( BONUS_DAY, t - SECONDS_IN_A_DAY );
                }*/

                this._haveDailyBonus = false;
                var t = ServerData.getInstance().current_time_seconds;

                var totalTime = ServerData.getInstance().current_time_seconds;

                if ( this._dailyBonus == -1 )//#changed as per discussion with Raghib on Nov 20 2017
                {
                    def.setValueForKey( BONUSX, 1 );
                     var t = ServerData.getInstance().current_time_seconds;// time( 0 );
                     def.setValueForKey( BONUS_DAY, t - SECONDS_IN_A_DAY );
                }
                else
                {
                    totalTime = t - this.getTime( BONUS_DAY );
                    if ( totalTime >= SECONDS_IN_A_DAY )
                    {
                        this._haveDailyBonus = true;

                        if ( totalTime >= SECONDS_IN_A_DAY * 2 && this._dailyBonus < 25 )
                        {
                            this._dailyBonus = 1;
                        }

                        def.setValueForKey( BONUSX, this._dailyBonus );
                    }
                }
            },
            chkForHourlyBonus : function()
            {
                var def = UserDef.getInstance();
                var dailyBonus = def.getValueForKey(BONUSX, -1);
                this._haveHourBonus = false;
                var t = ServerData.getInstance().current_time_seconds;

                var totalTime = ServerData.getInstance().current_time_seconds;

                if ( dailyBonus == -1 && def.getValueForKey( BONUS_HOUR, 0 ) == 0 )
                {
                    this._haveHourBonus = true;
                }
                else
                {
                    totalTime = ServerData.getInstance().current_time_seconds - def.getValueForKey( BONUS_HOUR, 0 ) ;
                    //def.setValueForKey( BONUS_HOUR, totalTime );

                    if ( totalTime >= SECONDS_IN_AN_HOUR || def.getValueForKey( BONUS_HOUR, 0 ) == 0 )
                    {
                        this._haveHourBonus = true;
                    }
                }

                return totalTime;
            },
            haveHappyHoursRunning:false,
            setLevel : function ( thisLevel, thisLevelBet, layer ) {
              //  console.log("updating Level "+thisLevel+"thisLevelBet"+thisLevelBet);
                var def = UserDef.getInstance();
                var game = null;
                var lBar = layer.getChildByTag( LEVEL_FILL_TAG );

                var prevLevel = thisLevel;

                if ( layer && layer.getTag() == sceneTags.GAME_SCENE_TAG )
                {
                    game = layer;
                    game.prevLevelPercent = CSUserdefauts.getInstance().getValueForKey(LEVEL_BAR, 0);
                    game.prevLevel = thisLevel;
                }

                var levelParLimit = ( 3000 * thisLevel * thisLevel ) + Math.pow( thisLevel, 4 );

                var currentLevelBet = thisLevelBet;

                var thisLevelBar = 0;

                var surplusBet = 0;

                var nextLevelParLimit = ( 3000 * ( thisLevel + 1 ) * ( thisLevel + 1 ) ) + Math.pow( ( thisLevel + 1 ), 4 );

                this.showLevelUp = false;

                this.levelUpVIP = 0;

                this.levelUpScore = 0;
               var previousLevelBar = parseInt(CSUserdefauts.getInstance().getValueForKey(LEVEL_BAR,"0"));
                var levelArr = [0,0,2,10,24,60,120,220,340,470,600,800,1200,1800,2600,3500,4600,5900,8400,10300,12800,15000];
                if(thisLevel < levelArr.length){
                    levelParLimit = levelArr[thisLevel]*100;
                }
                if (thisLevel+1<levelArr.length) {
                    nextLevelParLimit = levelArr[thisLevel+1]*100;
                }
                if( currentLevelBet + levelParLimit >= nextLevelParLimit )
                {
                    while( currentLevelBet + levelParLimit >= nextLevelParLimit )
                    {
                        thisLevel++;

                        surplusBet = ( currentLevelBet + levelParLimit ) - nextLevelParLimit;

                        var  prevLevelParLimit = nextLevelParLimit;

                        levelParLimit = nextLevelParLimit;
                        if(thisLevel+1 < levelArr.length){
                            nextLevelParLimit = levelArr[thisLevel+1]*100;
                        }else{
                            nextLevelParLimit = ( 3000 * ( thisLevel + 1 ) * ( thisLevel + 1 ) ) + Math.pow( ( thisLevel + 1 ), 4 );

                        }

                        currentLevelBet = surplusBet;

                        thisLevelBar = surplusBet * 100 / ( nextLevelParLimit - prevLevelParLimit );

                        this.showLevelUp = true;

                        this.levelUpVIP+=10;

                        this.levelUpScore = this.levelUpScore + 50 * thisLevel;
                    }

                    if ( game )
                    {
                        var s = game.getChildByTag( DIAMOND_SPRITE_TAG );
                        if ( s )
                        {
                            s.runAction( new cc.Repeat( new cc.Sequence( new cc.ScaleTo( 0.1, 1.2 ), new cc.ScaleTo( 0.1, 1.0 ) ), 3 ) );
                        }
                    }
                }
                else
                {
                    surplusBet = currentLevelBet;

                    thisLevelBar = surplusBet * 100 / ( nextLevelParLimit - levelParLimit );
                }
               if (def.getValueForKey(LEVEL,0)<thisLevel){
                   previousLevelBar = thisLevelBar - previousLevelBar;
                   var vip = def.getValueForKey( VIP_POINTS, 0 );
                   def.setValueForKey(VIP_POINTS, vip);

               }
                def.setValueForKey(LEVEL, thisLevel);

                CSUserdefauts.getInstance().setValueForKey(THIS_LEVEL_BET, surplusBet);

                CSUserdefauts.getInstance().setValueForKey(LEVEL_BAR, parseInt(thisLevelBar));

                var vSize = cc.winSize;

                if ( lBar )
                {
                    if ( game )
                    {
                        game.percentAddFector = ( Math.abs( ( thisLevelBar - game.prevLevelPercent ) ) / 10 );
                        lBar.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.5, 150 ), new cc.DelayTime( 0.5 ), new cc.FadeTo( 0.5, 255 ) ) ) );
                        game.schedule( game.updateLevelBar, 0.02 );
                    }
                }
                else
                {
                    lBar = new ccui.LoadingBar(res.Level_Bar_Fill_png);
                    lBar.setBright(true);
                    lBar.setPercent(thisLevelBar);
                    lBar.setTag(LEVEL_FILL_TAG);
                    lBar.setPosition(cc.p(vSize.width * 0.7, vSize.height - lBar.getContentSize().height * 0.875));
                    layer.addChild(lBar, 2);

                }

                var string = "" + parseInt( thisLevel );
                var labl = layer.getChildByTag( LEVEL_LABEL_TAG );

                if ( labl )
                {
                    labl.setString( string );
                }
                else
                {
                    var diamondSprite = new cc.Sprite( res.Level_Diamond_png );
                    diamondSprite.setPosition( cc.p( lBar.getPositionX() - lBar.getContentSize().width * 0.45, lBar.getPositionY() ) );
                    layer.addChild( diamondSprite, 2 );
                    diamondSprite.setTag( DIAMOND_SPRITE_TAG );
                    var ypos = 0;
                    if ( ServerData.getInstance().BROWSER_TYPE === OPERA || ServerData.getInstance().BROWSER_TYPE === CHROME || ServerData.getInstance().BROWSER_TYPE === FIREFOX){
                        ypos = 7;
                    }

                    labl = new CustomLabel( string, "arial_black", 25, cc.size( 100, 30 ) );
                    labl.setColor( cc.color( 255, 255, 255 ) );
                    labl.setPosition(  cc.p(lBar.getPositionX(),lBar.getPositionY()-ypos ));
                    labl.setTag( LEVEL_LABEL_TAG );
                    layer.addChild( labl, 2 );
                }

                if ( this.showLevelUp )
                {
                    ServerData.getInstance().updateData("last_level_up_amount",this.levelUpScore);
                    ServerData.getInstance().updateData("total_level_up_amount",this.levelUpScore,"","singleincrement");
                    //DailyChallenges.getInstance().updateTaskData( C_LEVEL_TO_CROSS, thisLevel - prevLevel );
                    var i = 0;
                    for( i = 19 - 1; i >= 3; i-- )
                    {
                        if( unlockAtLevelArr[i] <= thisLevel )
                        {
                            break;
                        }
                    }
                    if( prevLevel < unlockAtLevelArr[i] )
                    {
                        this.showMachineUnlocked = true;
                        this.unlockedMachineCode = i;
                    }
                   if (!CSUserdefauts.getInstance().getValueForKey( "kFBRatedCurrentVersion", false ) ){

                       var updateLevels = [25, 50, 75, 100, 150, 200, 250,300,350,450,550,600,750 ,850];
                       for (var idx=0; idx<updateLevels.length; idx++) {
                           var currentResetLevel =updateLevels[idx];
                           if (currentResetLevel >= prevLevel && currentResetLevel <= thisLevel) {

                               CSUserdefauts.getInstance().setValueForKey(RATING_SHOWN_COUNT,0);
                               ServerData.getInstance().updateCustomDataOnServer();
                               break;
                           }
                       }
                   }
                    //ServerCommunicator.getInstance().SCUpdateData();
                }
            },

            getVipString : function () {
                var def = UserDef.getInstance();
                var vip = def.getValueForKey( VIP_POINTS, 0 );

                var i;
                for ( i = 6; i >= 0; i-- )
                {
                    if ( vipArray[i] <= vip )
                    {
                        break;
                    }
                }

                var string = "";

                switch ( i )
                {
                    case 0:
                        string = "JUST " + DPUtils.getInstance().getNumString( vipArray[1] - vip ) + " MORE POINTS TO SILVER";
                        break;

                    case 1:
                        string = "JUST " + DPUtils.getInstance().getNumString( vipArray[2] - vip ) + " MORE POINTS TO GOLD";
                        break;

                    case 2:
                        string = "JUST " + DPUtils.getInstance().getNumString( vipArray[3] - vip ) + " MORE POINTS TO SAPPHIRE";
                        break;

                    case 3:
                        string = "JUST " + DPUtils.getInstance().getNumString( vipArray[4] - vip ) + " MORE POINTS TO EMERALD";
                        break;

                    case 4:
                        string = "JUST " + DPUtils.getInstance().getNumString( vipArray[5] - vip ) + " MORE POINTS TO RUBY";
                        break;

                    case 5:
                        string = "JUST " + DPUtils.getInstance().getNumString( vipArray[6] - vip ) + " MORE POINTS TO DIAMOND";
                        break;

                    default:
                        break;
                }

                return string;

            },
            changeBackgroundImage : function( imageName,isDarkImage) {

                var currentContainer = document.getElementById("Cocos2dGameContainer");
                var currentContainerStyle=currentContainer.style;

                currentContainerStyle.background = "url('"+imageName+"')";
                currentContainerStyle.backgroundRepeat='center center fixed';
                currentContainerStyle.webkitBackgroundSize ='cover';
                currentContainerStyle.mozBackgroundSize ='cover';
                currentContainerStyle.oBackgroundSize ='cover';
                currentContainerStyle.backgroundRepeat ='cover';
                currentContainerStyle.backgroundPosition ='center';
            },
            dealInstance : null,
            getDealInstance : function()
            {
                this.dealInstance.canSetDeal = this.dealInstance.getDealStatus();
                return this.dealInstance;
            },
            dealHiding : function( removeOffer )
            {
                var deal = this.dealInstance;

                var gameLayer = cc.director.getRunningScene();//.getChildByTag( sceneTags.GAME_SCENE_TAG );
                var lobbyLayer = null;
                var vec;
                var size;

                if ( gameLayer && gameLayer.getTag() == sceneTags.GAME_SCENE_TAG &&  deal.getDealStatus() )
                {
                    if ( deal.getDealStatus() )
                    {
                        gameLayer.dealBtn.setEnabled( true );
                        gameLayer.dealBtn.setVisible( true );
                        gameLayer.schedule( gameLayer.updateDeal );

                        var normal = gameLayer.dealBtn.getNormalImage();
                        normal.resumeAnimation();

                        normal = gameLayer.dealBtn.getSelectedImage();
                        normal.resumeAnimation();

                        gameLayer.buyBtnHalf.setEnabled( true );
                        gameLayer.buyBtnHalf.setVisible( true );
                        normal = gameLayer.buyBtnHalf.getNormalImage();
                        normal.resumeAnimation();

                        normal = gameLayer.buyBtnHalf.getSelectedImage();
                        normal.resumeAnimation();

                        gameLayer.buyBtn.setEnabled( false );
                        gameLayer.buyBtn.setVisible( false );
                        normal = gameLayer.buyBtn.getNormalImage();
                        normal.pauseAnimation();

                        normal = gameLayer.buyBtn.getSelectedImage();
                        normal.pauseAnimation();
                    }
                    vec = gameLayer.dealBtn.getPosition();
                    size = gameLayer.dealBtn.getContentSize();
                }
                else
                {
                    lobbyLayer = cc.director.getRunningScene().getChildByTag( sceneTags.LOBBY_SCENE_TAG );

                    if ( lobbyLayer )
                    {
                        //lobbyLayer.dealBtn.setEnabled( true );
                        //lobbyLayer.dealBtn.setVisible( true );
                        vec = lobbyLayer.dealBtn.getPosition();
                        size = lobbyLayer.dealBtn.getContentSize();
                    }
                }

                if ( deal.offerSprite )
                {
                    deal.removeFromParent();

                    if ( removeOffer )
                    {
                        deal.offerSprite.removeFromParent( true );
                        deal.offerSprite = null;
                        deal.pDealTexture = null;

                    }
                }

                /*var dispatcher = cc.director.getEventDispatcher();
                 dispatcher.removeEventListenersForTarget( deal );*/
                deal.dealOnScreen = false;
            },
            fbTexture : null,
            remoteSprite: function ( appIconURL, parent, defSpriteURL, isFBImg ){
                //cc.log( "appIconURL = " + appIconURL );
                var sprite = new cc.Sprite( defSpriteURL );

                cc.loader.loadImg(appIconURL, { isCrossOrigin:true },function ( err, texture ) {

                    var texture2d = new cc.Texture2D();
                    texture2d.initWithElement(texture);
                    texture2d.handleLoadedTexture();

                    sprite.setTexture(texture2d);
                    sprite.setScale(0.5, 0.5);

                    if( isFBImg )
                    {
                        AppDelegate.getInstance().fbTexture = texture2d;
                    }

                },sprite, parent, isFBImg );
                return sprite;
            },
            /*remoteSprite: function ( appIconURL, parent, defSpriteURL ){
                var sprite = new cc.Sprite( defSpriteURL );
                cc.loader.loadImg(appIconURL, {isCrossOrigin: true},function (err, texture) {
                    cc.log( "Error in remoteSprite = " + err );
                    var texture2d = new cc.Texture2D();
                    texture2d.initWithElement(texture);
                    texture2d.handleLoadedTexture();
                    var downloadedSprite = new cc.Sprite(texture2d);
                    downloadedSprite.setPosition( sprite.getPosition() );
                    parent.addChild(downloadedSprite,10);
                    downloadedSprite.setScale( sprite.getContentSize().width / downloadedSprite.getContentSize().width );
                    sprite.removeFromParent( true );
                    //texture = cc.textureCache.addImage(texture);
                    //sprite.setTexture( texture );
                },sprite, parent );
                return sprite;
            },*/
            LaunchApp : function()
            {
                if( FacebookObj && typeof ( FacebookObj ) != undefined )
                {
                    FacebookObj.appLaunched = true;
                    AppDelegate.getInstance().invitableFriendsList = FacebookObj.invitableFriendsList;
                }

                cc.director.runScene( new HelloWorldScene() );
                window.setInterval( function(){
                    /// call your function here
                    if (ServerData.getInstance().configCode===1){
                        BonusTimer.getInstance().backgroundTimerHandler(1);
                    }
                    ServerData.getInstance().timeTicker();
                    if( AppDelegate.getInstance().logInTicker === 1 && AppDelegate.getInstance().configInTicker === 1 )
                    {
                        ServerData.getInstance().loginFbChecker();
                       // AppDelegate.getInstance().logInTicker++;
                    }

                }, 1000 );

            }
             /*tournamentToBG : function()
        {
            UserDef.getInstance().setValueForKey("time_from_background", ServerCommunicator.getInstance().current_time_seconds );//Android SetUp            
        },

        tournamentToFG : function()
        {
            var def = UserDef.getInstance();
            var timeStr = def.getValueForKey( "time_from_background", 0 );
            if ( !timeStr )
            {
                return;
            }
            var time1 = this.getTime( "time_from_background" );//std::stol(timeStr);Android SetUp
            
            var resume_seconds= ServerCommunicator.getInstance().current_time_seconds - time1;

            if (resume_seconds >= 0 && stopTournament &&  ( this.tournaments[0].m * 60 + this.tournaments[0].s ) > resume_seconds ) {
                var min=resume_seconds/60;
                var sec=resume_seconds%60;
                if (this.tournaments[0].s-sec<=0) {
                    min++;
                    sec=sec-this.tournaments[0].s;
                    this.tournaments[0].s=60;
                    this.tournaments[1].s=60;
                    this.tournaments[2].s=60;
                }
                this.tournaments[0].m-=min;
                this.tournaments[0].s-=sec;

                this.tournaments[1].m-=min;
                this.tournaments[1].s-=sec;

                this.tournaments[2].m-=min;
                this.tournaments[2].s-=sec;

                while (resume_seconds>0)
                {
                    if (resume_seconds>5)  //5sec is for tournamentBotUpdateTime
                    {
                        resume_seconds-=5;
                        this.tournaments[0].remainSec-=5;
                        this.tournaments[0]._currentCount+=5;
                        this.tournaments[0].updateBotsPoints(this.tournaments[0].remainSec/5);
                        this.tournaments[1].remainSec-=5;
                        this.tournaments[1]._currentCount+=5;
                        this.tournaments[1].updateBotsPoints(this.tournaments[1].remainSec/5);
                        this.tournaments[2].remainSec-=5;
                        this.tournaments[2]._currentCount+=5;
                        this.tournaments[2].updateBotsPoints(this.tournaments[2].remainSec/5);
                    }
                    else
                    {

                        break;
                    }

                }

            }
            else
            {
                this.releaseTournament();
                Scene * scene = Director::getInstance().getRunningScene();
                Layer * layer = ( Layer * )scene.getChildByTag( GAME_SCENE_TAG );
                if ( layer )
                {
                    GameScene * game = ( GameScene * )layer;

                    game.tournamentPause=true;
                    if ( game.mCode != DJS_X_MULTIPLIERS )
                    {
                        game.hideWaitingTournament();
                        game.connectingTournament(0);
                    }
                }
            }

        }*/
        };
    };
    return {
        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function () {
            if ( !instance ) {
                instance = init();
                instance.dealInstance = new DJSDeal();
                instance.dealInstance.retain();
                cc.director.setNotificationNode( new cc.Node() );
                instance.initApp();
            }
            return instance;
        }
    };
})();