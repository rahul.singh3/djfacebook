/**
 * Created by RNF-Mac11 on 2/27/17.
 */

CSPlayerView = cc.Node.extend({

    _img : null,
    _rank : 0,
    _points : 0,

    points : null,
    rank : null,
    circle : null,
    playerImage : null,



    ctor:function (  ) {
        this._super();
        return true;
    },

    setView : function( i, imgIndx, sbox )
    {
        if ( i==0 )
        {
            var url = "https://graph.facebook.com/" + FacebookObj.userFbID + "/picture?width=135&height=135";
            if( hackBuild )
            {
                url = "https://graph.facebook.com/" + 852642308217879 + "/picture?width=135&height=135";
            }

            this.playerImage = AppDelegate.getInstance().remoteSprite( url, this ,"res/tournament/cst_person_male.png", true);
            //cc.log( "URL = " + url );
            //new cc.Sprite( "res/tournament/cst_person_male.png" );

            /*var imgMine = new cc.Image();
            var t = new cocos2d:: Texture2D;
            cocos2d:: Data d=cocos2d:: UserDefault::getInstance().getDataForKey("fbimage");
            if (d.getSize() != 0)
            {
                imgMine.initWithImageData(d.getBytes(), d.getSize());
            }
            else
            {
                imgMine=NULL;
                t=NULL;

            }

            if (imgMine)
            {
                t.initWithImage(imgMine);
                imgMine.release();

            }


            if (t)
            {
                playerImage.setTexture(t);

                t.release();
            }*/


        }
        else
        {
            var data = ServerData.getInstance();
            var url = data.server_cdn_url + imgIndx + ".png";
            this.playerImage = AppDelegate.getInstance().remoteSprite( DPUtils.getInstance().TournamentURLgalaxy( imgIndx ), this,
                "res/tournament/cst_loading_person.png" );//RemoteSprite::createWithURL(cocos2d::StringUtils::format(url.c_str(),imgIndx).c_str(),"tournament/cst_loading_person.png", false );
        }


        this.playerImage.setPosition( cc.p( cc.rectGetMinX( sbox.getBoundingBox() ) + this.playerImage.getContentSize().width * 0.63, cc.rectGetMaxY( sbox.getBoundingBox() ) - this.playerImage.getContentSize().height * 0.75 ) );
        //this.playerImage.setPosition( 500, 500 );
        this.addChild(this.playerImage);
        this._img = this.playerImage;


        this.playerPoints = 0;
        this.playerRank = 100 - i;
        this.points = new cc.LabelTTF( "" +this._points, "TT0504M_", 17.5);
        this.points.setPosition( cc.p( cc.rectGetMaxX(sbox.getBoundingBox() ), cc.rectGetMinY( sbox.getBoundingBox() ) ) );
        this.points.setAnchorPoint( cc.p(1, 0) );

        this.addChild(this.points);


        if( i == 0 )
        {
            var str = "Win\n To\nRank";
            this.rank = new cc.LabelTTF( str, "TT0504M_", 21 );
            //rank.setAlignment(CC_DLL cocos2d::TextHAlignment::CENTER);

            this.rank.setPosition( cc.p( cc.rectGetMaxX( sbox.getBoundingBox() ) - sbox.getContentSize().width * 0.3, cc.rectGetMaxY( sbox.getBoundingBox() ) - sbox.getContentSize().height * 0.4 ) );
            this.addChild( this.rank );
        }
        else
        {
            this.rank = new cc.LabelTTF( "" + this._rank, "TT0504M_", 27.5);
            //this.rank.setAlignment(CC_DLL cocos2d::TextHAlignment::CENTER);
            this.rank.setPosition( cc.p( cc.rectGetMaxX( sbox.getBoundingBox() ) - sbox.getContentSize().width * 0.3, cc.rectGetMaxY( sbox.getBoundingBox() ) - sbox.getContentSize().height * 0.4 ) );
            this.addChild( this.rank );
        }

        this.circle = new cc.Sprite( "#cst_redCircle.png" );
        this.circle.setPosition( this.rank.getPosition() );
        this.addChild( this.circle, -1 );
        this.circle.setVisible( false );
    }


});