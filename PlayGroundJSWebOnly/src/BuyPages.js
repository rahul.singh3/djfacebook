BuyPages = cc.Layer.extend({
    timerLabel : null,
    bgSprite :null,
    offerCode : null,//code should be either 1, 2, 4
    ctor: function (code) {
        this._super();
        //var pRet = new CustomButton();
        //if ( pRet && pRet.init() ){
        this.create(code);
        return true;
        //}
    },
    create: function (code) {


        this.offerCode = code;
        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        this.addChild( darkBG );
        var str = "res/lobby/buyPage_1_19/bg_" + (this.offerCode - BuyPageTags.INSUFFICIENT_CREDITS_1) + ".png";
        this.bgSprite = new cc.Sprite( str );
        this.bgSprite.setPosition( cc.p( this.getContentSize().width / 2, this.getContentSize().height / 2 ) );
        this.addChild( this.bgSprite );
        var buyMenu = new cc.Menu();
        buyMenu.setPosition( cc.p( 0, 0 ) );
        this.bgSprite.addChild( buyMenu );
        var y = 94;
        var h = 80;
        for ( var i = 0; i < 7; i++ )
        {
            var buystr = "res/lobby/buyPage_1_19/" + i + ".png";
            var buyBtn = new cc.MenuItemImage( buystr, buystr, this.menuCB, this );
            var yPos = y + i * h; //- buyBtn.getContentSize().height/2;
            cc.log( "buyBtn yPosition = " + yPos );
            buyBtn.setPosition( cc.p( this.getContentSize().width*0.75, yPos ) );
            buyBtn.setTag( i + purchaseTags.PURCHASE_0_99 );
            buyMenu.addChild( buyBtn );
        }
        if (this.offerCode === BuyPageTags.OFFER_CREDITS){
            var tempTimerLabel = new cc.LabelTTF( "00:00:00", "CarterOne", 28 );
            this.timerLabel = new cc.LabelTTF( "00:00:00", "CarterOne", 28, tempTimerLabel.getContentSize() );
            var ypos = 0.83;
            if (  ServerData.getInstance().BROWSER_TYPE === SAFARI){
                ypos = 0.85;
            }
            this.timerLabel.setPosition( cc.p( this.getContentSize().width / 2 + this.timerLabel.getContentSize().width * 0.4, this.getContentSize().height * ypos ) );
            this.bgSprite.addChild( this.timerLabel );
            this.schedule( this.updator );
        }
        var closestr = "res/lobby/buyPage_1_19/close_btn.png";
        var buycloseItem = new cc.MenuItemImage( closestr, closestr, this.menuCB, this );
        buycloseItem.setPosition(cc.p(this.getContentSize().width*0.88,this.getContentSize().height*0.88));
        buycloseItem.setTag( CLOSE_BTN_TAG );
        buyMenu.addChild( buycloseItem ,2);
        DPUtils.getInstance().easyBackOutAnimToPopup(this.bgSprite,0.3);

    },
    updator : function( dt ) {
        if (this.timerLabel){
        var now = new Date();//ServerData.getInstance().currentTime;
        var days_passed = now.getDay() - 5;
        var min = 59 - now.getMinutes();
        var sec = 59 - now.getSeconds();
        var hour = 0;
        if (days_passed === 0)
            hour = now.getHours() - 18;

        else if (days_passed === 1)
            hour = 24 + now.getHours() - 18;

        else if (days_passed === -5)
            hour = 48 + now.getHours() - 18;

        else if (days_passed === -4)
            hour = 72 + now.getHours() - 18;


        hour = 77 - hour;

        var string = hour < 10 ? "0" + hour : "" + hour;

        min < 10 ? string = string + ":0" + min : string = string + ":" + min;

        sec < 10 ? string = string + ":0" + sec : string = string + ":" + sec;

        this.timerLabel.setString(string);
    }
    },
    menuCB : function( sender ) {

        if ( cc.director.getRunningScene().getChildByTag(LOADER_TAG))
              return;

        var itm = sender;

        var deal = AppDelegate.getInstance().getDealInstance();//DJSDeal.getInstance();

        var def = UserDef.getInstance();

        var delegate = AppDelegate.getInstance();

        switch (itm.getTag()) {
            case CLOSE_BTN_TAG:
            {
                delegate.jukeBox(S_BTN_CLICK);
                if (def.getValueForKey(IS_BUYER,false)){
                    deal.setDealData(5);// set offer deal time
                }else if(!delegate.dealFlushed){
                    deal.setDealData(5);// set offer deal time
                }
                DPUtils.getInstance().easyBackInAnimToPopup(this.bgSprite ,0.3);
                this.runAction(new cc.Sequence( new cc.DelayTime(0.3),cc.callFunc( function ( ){
                if (!def.getValueForKey(IS_BUYER,false)){
                    if (!delegate.dealFlushed && deal.canSetDeal && deal.flushDeal(true)) {
                        DPUtils.getInstance().setTouchSwallowing( this.getParent(), deal );
                        deal.setTag(PopUpTags.STAY_TOUCH_DEAL_POP);
                        this.getParent().addChild( deal, this.getLocalZOrder() );
                        delegate.dealFlushed = true;

                    }
                }else{
                    if (!delegate.dealFlushed && deal.canSetDeal && deal.flushDeal(true)) {
                        DPUtils.getInstance().setTouchSwallowing( this.getParent(), deal );
                        deal.setTag(PopUpTags.STAY_TOUCH_DEAL_POP);
                        this.getParent().addChild( deal, this.getLocalZOrder() );
                        delegate.dealFlushed = true;
                    }
                }
                    this.removeFromParent( true );
                },this)));

            }
                break;
            default:
            {
                IAPHelper.getInstance().purchase( itm.getTag(), this.offerCode );
            }
            break;
        }
    }
});