/**
 * Created by RNF-Mac11 on 2/9/17.
 */



var reelsRandomizationArrayDJSFreeSpinner =
    [
        [// 85% payout
            35, 3, 1, 55, 1, 60,
            1, 64, 20, 4, 1, 90,
            1, 75, 1, 100, 30, 50,
            15, 15, 15, 2, 1, 30,
            1, 35, 1, 20, 1, 10,
            1, 15, 25, 2, 1, 30
        ],
        [//92 %
            35, 3, 1, 70, 1, 70,

            1, 74, 20, 4, 1, 90,

            1, 75, 1, 100, 30, 50,

            15, 15, 15, 3, 1, 30,

            1, 35, 1, 20, 1, 10,


            1, 15, 25, 2, 1, 35
        ],
        [//97 % payout
            30, 3, 1, 75, 1, 80,
            1, 90, 20, 4, 1, 100,
            1, 85, 1, 110, 30, 50,
            15, 20, 15, 2, 1, 30,
            1, 35, 1, 20, 1, 10,
            1, 15, 25, 2, 1, 30

        ],
        [//140 % payout

            30, 10, 1, 160, 1, 200,
            1, 190, 20, 8, 1, 190,
            1, 170, 1, 210, 15, 80,
            1, 60, 15, 5, 1, 70,
            1, 65, 1, 50, 1, 40,
            1, 50, 25, 4, 1, 80

        ],
        [//300 % payout

            30, 10, 1, 160, 1, 200,
            1, 190, 20, 8, 1, 190,
            1, 170, 1, 210, 15, 80,
            1, 60, 15, 5, 1, 70,
            1, 65, 1, 50, 1, 40,
            1, 50, 25, 4, 1, 80

        ]
    ];

var DJSFreeSpinner = GameScene.extend({

    freeSpinEmenentsVec : new Array(),
    reelsArrayDJSFreeSpinner:[],
    ctor:function ( level_index ) {
        this.reelsArrayDJSFreeSpinner = [
            /*1*/ELEMENT_EMPTY,/*2*/ELEMENT_2X,/*3*/ELEMENT_EMPTY,/*4*/ELEMENT_7_BLUE,/*5*/ELEMENT_EMPTY,
            /*6*/ELEMENT_7_PINK,/*7*/ELEMENT_EMPTY,/*8*/ELEMENT_7_RED,/*9*/ELEMENT_EMPTY,/*10*/ELEMENT_3X,
            /*11*/ELEMENT_EMPTY,/*12*/ELEMENT_BAR_3,/*13*/ELEMENT_EMPTY,/*14*/ELEMENT_BAR_2,/*15*/ELEMENT_EMPTY,
            /*16*/ELEMENT_BAR_1,/*17*/ELEMENT_EMPTY,/*18*/ELEMENT_BAR_2,/*19*/ELEMENT_EMPTY,/*20*/ELEMENT_7_RED,
            /*21*/ELEMENT_EMPTY,/*22*/ELEMENT_5X,/*23*/ELEMENT_EMPTY,/*24*/ELEMENT_BAR_1,/*25*/ELEMENT_EMPTY,
            /*26*/ELEMENT_BAR_3,/*27*/ELEMENT_EMPTY,/*28*/ELEMENT_BAR_2,/*29*/ELEMENT_EMPTY,/*30*/ELEMENT_7_BLUE,
            /*31*/ELEMENT_EMPTY,/*32*/ELEMENT_7_PINK,/*33*/ELEMENT_EMPTY,/*34*/ELEMENT_2X,/*35*/ELEMENT_EMPTY,
            /*36*/ELEMENT_BAR_1
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSFreeSpinner;
        this.physicalArrayCheck= this.reelsArrayDJSFreeSpinner;
        this._super( level_index );

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;


    },

    calculatePayment : function( doAnimate )
    {
        var payMentArray = [
        /*0*/   45, // 7 7 7 blue

        /*1*/   20, // 7 7 7 pink

        /*2*/   15, // 7 7 7 red

        /*3*/   12, // any 7 combo

        /*4*/   10, // 3Bar 3Bar 3Bar

        /*5*/   7, // 2Bar 2Bar 2Bar

        /*6*/   4, // Bar Bar Bar

        /*7*/   3 // Any bar combo
    ];

        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        for ( var i = 0; i < this.slots[0].resultReel.length; i++ )
        {
            tmpVector = this.slots[0].resultReel[i];
            tmpVector2.push( tmpVector[1] );
        }

        this.lineVector.push( tmpVector2 );

        var pay = 0;
        var lineLength = 3;
        var countGeneral = 0;
        var i, j, k;
        var e = null;
        var tmpVec;
        this.freeSpinEmenentsVec = [];
        this.haveFreeSpins = false;

        for ( i = 0; i < this.totalLines; i++ )
        {
            tmpVec = this.lineVector[i];
            for ( k = 0; k < 9; k++ )
            {
                var breakLoop = false;
                countGeneral = 0;
                switch ( k )
                {
                    case 0://for all combination with wild
                        for ( j = 0; j < lineLength; j++ )
                        {
                            if ( this.resultArray[0][j] != -1 && doAnimate )
                            {
                                switch ( this.reelsArrayDJSFreeSpinner[this.resultArray[i][j]] )
                                {
                                    case ELEMENT_2X:
                                        if ( this.freeSpinCount < 0 )
                                        {
                                            this.freeSpinCount = 1;
                                        }
                                        else
                                        {
                                            this.freeSpinCount++;
                                        }
                                        breakLoop = true;

                                        if ( j < tmpVec.length ) {
                                            e = tmpVec[ j ];
                                            this.freeSpinEmenentsVec.push( e );
                                        }
                                        break;

                                    case ELEMENT_3X:
                                        if ( this.freeSpinCount < 0 )
                                        {
                                            this.freeSpinCount = 3;
                                        }
                                        else
                                        {
                                            this.freeSpinCount+=3;
                                        }
                                        breakLoop = true;
                                        if ( j < tmpVec.length ) {
                                            e = tmpVec[ j ];
                                            this.freeSpinEmenentsVec.push( e );
                                        }
                                        break;

                                    case ELEMENT_5X:
                                        if ( this.freeSpinCount < 0 )
                                        {
                                            this.freeSpinCount = 5;
                                        }
                                        else
                                        {
                                            this.freeSpinCount+=5;
                                        }
                                        breakLoop = true;

                                        if ( j < tmpVec.length ) {
                                            e = tmpVec[ j ];
                                            this.freeSpinEmenentsVec.push( e );
                                        }
                                        break;

                                    default:
                                        break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                        break;

                    case 1: // 7 7 7 Blue
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFreeSpinner[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_BLUE:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[0];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {
                            if ( l < tmpVec.length ) {
                                e = tmpVec[ l ];
                            }

                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 2: // 7 7 7 Pink
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFreeSpinner[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_PINK:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[1];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {
                            if ( l < tmpVec.length ) {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 3: // 7 7 7 Red
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFreeSpinner[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[2];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {
                            if ( l < tmpVec.length ) {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 4: // Any 7 Combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFreeSpinner[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_BLUE:
                                case ELEMENT_7_PINK:
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[3];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {
                            if ( l < tmpVec.length ) {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 5: // 3Bar 3Bar 3Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFreeSpinner[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[4];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {
                            if ( l < tmpVec.length ) {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 6: // 2Bar 2Bar 2Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFreeSpinner[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[5];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {
                            if ( l < tmpVec.length ) {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 7: // Bar Bar Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFreeSpinner[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[6];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {
                            if ( l < tmpVec.length ) {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 8: // any Bar combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSFreeSpinner[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[7];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( l < tmpVec.length ) {
                                    e = tmpVec[ l ];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

                if ( breakLoop )
                {
                    break;
                }
            }
        }

        if ( doAnimate )
        {
            this.currentWin = this.currentBet * pay;
        }
        else
        {
            this.currentWinForcast = this.currentBet * pay;
        }

        if ( doAnimate )
        {
            var time = 0.5;
            if ( this.currentWin > 0 )
            {
                this.addWin = this.currentWin / 10;
                this.setCurrentWinAnimation();
                time = 1.0;
            }

            if ( this.freeSpinCount > 0 )
            {
                this.haveFreeSpins = true;
                this.freeSpinCount--;
                if ( this.freeSpinEmenentsVec.length )
                {
                    for ( var a = 0; a < this.freeSpinEmenentsVec.length; a++ )
                    {
                        e = this.freeSpinEmenentsVec[a];

                        if ( a == this.freeSpinEmenentsVec.length - 1 )
                        {
                            e.runAction( new cc.Sequence( new cc.RotateBy( time, 720 * time, 0, 0 ), cc.callFunc( function() {
                            this.spin();

                                this.spinItem.loadTextures( "auto_spin.png", "auto_spin_pressed.png", "", ccui.Widget.PLIST_TEXTURE );
                            var glowSprite = this.spinItem.getChildByTag( GLOW_SPRITE_TAG );
                            glowSprite.setTexture( res.glow_white_png );

                        }, this ) ) );
                        }
                        else
                        {
                            e.runAction( new cc.Sequence( new cc.RotateBy( time, 720 * time, 0, 0 ) ) );
                        }
                        this.delegate.jukeBox( S_SPINTILLWIN );
                    }
                }
                else
                {
                    var node = new cc.Node();
                    this.addChild( node );
                    node.runAction( new cc.Sequence( new cc.DelayTime( time ), new cc.callFunc( function() {
                    node.removeFromParent( true );
                    this.spin();
                        this.spinItem.loadTextures( "auto_spin.png", "auto_spin_pressed.png", "", ccui.Widget.PLIST_TEXTURE );
                    var glowSprite = this.spinItem.getChildByTag( GLOW_SPRITE_TAG );
                    glowSprite.setTexture( res.glow_white_png );

                }, this, node ) ) );
                    this.delegate.jukeBox( S_SPINTILLWIN );
                }
            }
            else
            {
                this.checkAndSetAPopUp();

                if ( !this.isAutoSpinning )
                {
                    this.spinItem.loadTextures( "spin.png", "spin_pressed.png", "", ccui.Widget.PLIST_TEXTURE );
                    var glowSprite = this.spinItem.getChildByTag( GLOW_SPRITE_TAG );
                    glowSprite.setTexture( res.glow_white_png );
                }
            }
        }
    },
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSFreeSpinner[i]=this.physicalReelElements[idx];
                i++;
            }
        }
    },
    changeTotalProbabilities : function()
    {
        this.totalProbalities = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            //this.extractArrayFromSring();
            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[this.easyModeCode][i];
            }
        }
        else
        {
            for ( var i = 0; i < 36; i++ )
            {
                this.probabilityArray[this.easyModeCode][i] = reelsRandomizationArrayDJSFreeSpinner[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSFreeSpinner[this.easyModeCode][i];
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
        if( (this.reelsArrayDJSFreeSpinner[this.resultArray[0][0]] == ELEMENT_7_BLUE && this.reelsArrayDJSFreeSpinner[this.resultArray[0][1]] == ELEMENT_7_BLUE ) ||
            (this.reelsArrayDJSFreeSpinner[this.resultArray[0][0]] == ELEMENT_7_PINK && this.reelsArrayDJSFreeSpinner[this.resultArray[0][1]] == ELEMENT_7_PINK ) ||
            (this.reelsArrayDJSFreeSpinner[this.resultArray[0][0]] == ELEMENT_7_RED && this.reelsArrayDJSFreeSpinner[this.resultArray[0][1]] == ELEMENT_7_RED ) )
        {
            if ( !this.slots[0].isForcedStop )
            {
                this.schedule( this.reelStopper, 4.8 );

                this.delegate.jukeBox( S_EXCITEMENT );

                var glowSprite = new cc.Sprite( glowStrings[0] );

                if ( glowSprite )
                {
                    var r = this.slots[0].physicalReels[ this.slots[0].physicalReels.length - 1 ];

                    glowSprite.setPosition( r.getParent().getPosition().x + this.slots[0].getPosition().x, r.getParent().getPosition().y + this.slots[0].getPosition().y );

                    //this.scaleAccordingToSlotScreen( glowSprite );

                    this.addChild( glowSprite, 5 );
                    this.animNodeVec.push( glowSprite );

                    glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ) ) );
                }
            }
        }
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
        var tmp = this.slots[0].resultReel[laneIndex];

        var e = tmp[1];

        switch ( e.eCode )
        {
            case ELEMENT_7_RED:
                if (laneIndex != 0)
                {
                    break;
                }
            case ELEMENT_2X:
            case ELEMENT_3X:
            case ELEMENT_4X:
            case ELEMENT_5X:
                e.runAction( new cc.Repeat( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ), 2 ) );
                this.delegate.jukeBox( S_FLASH );
                break;

            default:
                break;
        }

    },

    generateResult : function( scrIndx )
    {
        for ( var i = 0; i < this.slots[scrIndx].displayedReels.length; i++ )
        {
            var num = this.prevReelIndex[i];

            var tmpnum = 0;

            while (num == this.prevReelIndex[i])
            {
                num = Math.floor( Math.random() * this.totalProbalities );
            }
            this.prevReelIndex[i] = num;
            this.resultArray[0][i] = num;

            for (var j = 0; j < 36; j++)
            {
                tmpnum+=this.probabilityArray[this.easyModeCode][j];

                if (tmpnum >= this.resultArray[0][i])
                {
                    this.resultArray[0][i] = j;
                    break;
                }
            }


            /*/HACK
             if ( this.freeSpinCount <= 0 )
             {
             if (i == 0)
             {
             this.resultArray[0][i] = 0;//2;
             }
             else if (i == 1)
             {
                 this.resultArray[0][i] = 1;//2, 4, 20, 30
             }
             else if(i == 2)
             {
                 this.resultArray[0][i] = 0;
             }
             else
             {
                 this.resultArray[0][i] = 0;
             }
             }
             //*///HACK
        }

        this.calculatePayment( false );
        if ( this.freeSpinCount <= 0 )
        {
            //this.calculatePayment( false );//Change 8 June
            this.checkForFalseWin();
        }

    },

    rollUpdator : function(dt)
    {
        var i;
        for (i = 0; i < this.totalSlots; i++)
        {
            this.slots[i].updateSlot(dt);
        }

        for (i = 0; i < this.totalSlots; i++)
        {
            if ( this.slots[i].isSlotRunning() )
            {
                break;
            }
        }

        if (i >= this.totalSlots)
        {
            this.isRolling = false;
            this.unschedule( this.rollUpdator );
            this.calculatePayment( true );

            if (this.isAutoSpinning)
            {
                if (this.currentWin > 0)
                {
                    this.scheduleOnce( this.autoSpinCB, 1.5);
                }
                else
                {
                    this.scheduleOnce( this.autoSpinCB, 1.0);
                }
            }
        }
    },

    setFalseWin : function()
    {
        var highWinIndices = [ 19, 29 ];
        var missIndices = [ 19, 29 ];

        var missedIndx = Math.floor( Math.random() * 3 );

        if( missedIndx == 2 )
        {
            if( Math.floor( Math.random() * 10 ) == 3 )
            {
            }
            else
            {
                missedIndx = Math.floor( Math.random() * 2 );
            }
        }

        var sameIndex = Math.floor( Math.random( ) * highWinIndices.length );

        for ( var i = 0; i < 3; i++ )
        {
            if ( i == missedIndx )
            {
                this.resultArray[0][i] = this.getRandomIndex( missIndices[ Math.floor( Math.random( ) * missIndices.length ) ], true );
            }
            else
            {
                this.resultArray[0][i] = this.getRandomIndex( highWinIndices[ sameIndex ], false );
            }
        }
    },

    setResult : function( indx, lane, sSlot )
    {
        var gap = 387.0;

        var j = 0;
        var animOriginY;

        var r = this.slots[0].physicalReels[lane];
        var arrSize = r.elementsVec.length;
        var spr;

        var spriteVec = new Array();

        animOriginY = gap * r.direction;
        spr = r.elementsVec[indx];
        if ( spr.eCode == ELEMENT_EMPTY )
        {
            gap = 285 * spr.getScale();
        }

        if ( indx == 0 )
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push(spr);

            spr.setPositionY( animOriginY );

            for (var i = 0; i < 2; i++)
            {
                spr = r.elementsVec[i];
                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (i + 1) * r.direction);
            }
        }
        else if (indx == arrSize - 1)
        {
            spr = r.elementsVec[0];
            spriteVec.push(spr);

            spr.setPositionY(animOriginY);
            for (var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i--)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction );
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push(spr);
            spr.setPositionY(animOriginY);

            for (var i = indx; i < indx + 2; i++)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }

        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            gap = -773.5;

            var eee;
            if (i < 2)
            {
                eee = spriteVec[i + 1];
            }
            if (i == 0)
            {
                var ee = spriteVec[i + 1];
                if ( Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) ) != 387)
                {
                    gap = -Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) * 2 * ee.getScale() );
                }
            }

            gap*=r.direction;
            var ease;

            if ( this.slots[sSlot].isForcedStop )
            {
                ease =  new cc.MoveTo( 0.15, cc.p( 0, e.getPositionY() + gap) ).easing( cc.easeBackOut() );
            }
            else
            {
                ease = new cc.MoveTo( 0.3, cc.p( 0, e.getPositionY() + gap ) ).easing( cc.easeBackOut() );
            }

            ease.setTag( MOVING_TAG );
            e.runAction(ease);
        }

        return spriteVec;
    },

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSFreeSpinner;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    setWinAnimation : function( animCode, screen )
{
    var tI = 0.3;

    var fire;
    for ( var i = 0; i < this.animNodeVec.length; i++ )
    {
        fire = this.animNodeVec[i];
        fire.removeFromParent( true );
    }
    this.animNodeVec = [];


    switch ( animCode )
    {
        case WIN_TAG:

            for (var i = 0; i < this.slots[0].blurReels.length; i++)
        {
            var r = this.slots[0].blurReels[i];

            var node = r.getParent();

            var moveW = this.slots[0].reelDimentions.x * this.slots[0].getScale();
            var moveH = this.slots[0].reelDimentions.y * this.slots[0].getScale();

            for ( var j = 0; j < 4; j++ )
            {

                var fire = new cc.ParticleSun();
                fire.texture = cc.textureCache.addImage( res.FireStar_png );
                fire.setStartColor( cc.color( 0, 255, 0 ) );

                var pos;

                this.addChild( fire, 5 );
                this.animNodeVec.push( fire );

                var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                diff*=( 2 - i );
                var moveBy = null;

                switch ( j % 4 )
                {
                    case 0:
                        pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY());
                        fire.setPosition( pos );
                        moveBy = new cc.MoveBy( tI, cc.p( moveW, 0 ) );
                        break;

                    case 1:
                        pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY());
                        fire.setPosition( pos );
                        moveBy = new cc.MoveBy( tI, cc.p( 0, moveH ) );
                        break;

                    case 2:
                        pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                        fire.setPosition( pos );
                        moveBy = new cc.MoveBy( tI, cc.p( -moveW, 0 ) );
                        break;

                    case 3:
                        pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                        fire.setPosition( pos );
                        moveBy = new cc.MoveBy( tI, cc.p( 0, -moveH ) );
                        break;

                    default:
                        break;
                }
                fire.runAction( new cc.RepeatForever( new cc.Sequence( moveBy, moveBy.reverse() ) ) );
            }
        }

            break;

        case BIG_WIN_TAG:
            for( var i = 0; i < this.slots[0].blurReels.length; i++ )
        {
            var r = this.slots[0].blurReels[i];

            var node = r.getParent();

            var glowSprite;

            var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
            diff*=( 2 - i );


            if ( i == 0 )
            {
                glowSprite = new cc.Sprite( glowStrings[0] );
            }
            else if ( i == 1 )
            {
                glowSprite = new cc.Sprite( glowStrings[1] );
            }
            else
            {
                glowSprite = new cc.Sprite( glowStrings[0] );
            }

            if ( glowSprite )
            {
                glowSprite.setPosition( node.getPositionX() + this.slots[0].getPositionX() - diff, node.getPositionY() + this.slots[0].getPositionY() );

                this.addChild( glowSprite, 5 );
                this.animNodeVec.push( glowSprite );
                glowSprite.setOpacity( 100 );
                this.scaleAccordingToSlotScreen( glowSprite );
                glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.3, 255 ), new cc.DelayTime( 0.3 ), new cc.FadeTo( 0.3, 100 ) ) ) );
            }
        }
            break;

        default:
            break;
    }
},

    spin : function()
    {
        this.unschedule( this.updateWinCoin );

        if ( !this.haveFreeSpins )
        {
            this.deductBet( this.currentBet );
        }

        if (this.displayedScore != this.totalScore)
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;

        this.currentWin = 0;
        this.currentWinForcast = 0;
        this.startRolling();

        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    }

});