/**
 * Created by RNF-Mac11 on 1/12/17.
 */

var DPUtils = (function () {
    // Instance stores a reference to the Singleton
    var instance;

    function init() {
        // Singleton
        // Private methods and variables
        function privateMethod(){
            //console.log( "I am private" );
        }
        var privateVariable = "Im also private";
        var privateRandomNumber = Math.random();
        return {

            strcasecmp: function( fString1, fString2 ){
                var string1 = (fString1 + '').toLowerCase();
                var string2 = (fString2 + '').toLowerCase();

                if (string1 > string2) {
                    return 1;
                } else if (string1 == string2) {
                    return 0;
                }
                return -1;
            },
            abbreviateNumber:function (value) {
                var newValue = value;
                if (value >= 1000) {
                    var suffixes = ["", "k", "m", "b","t"];
                    var suffixNum = Math.floor( (""+value).length/3 );
                    var shortValue = '';
                    for (var precision = 2; precision >= 1; precision--) {
                        shortValue = parseFloat( (suffixNum != 0 ? (value / Math.pow(1000,suffixNum) ) : value).toPrecision(precision));
                        var dotLessShortValue = (shortValue + '').replace(/[^a-zA-Z 0-9]+/g,'');
                        if (dotLessShortValue.length <= 2) { break; }
                    }
                    if (shortValue % 1 != 0)  shortValue = shortValue.toFixed(1);
                    newValue = shortValue+suffixes[suffixNum];
                }
                return newValue;
            },
            getNumString : function ( num ) {
                var string = null;

                var no = num;
                var revNo = 0;
                var noLength = 0;
                var length = 0;
                var i = 0;
                while (  parseInt( no )  > 0 )
                {
                    revNo = revNo * 10 + parseInt( no ) % 10;
                    no /= 10;
                    noLength++;
                }
                length = noLength;

                if ( length == 0 )
                {
                    string = "0";
                }
                else
                {
                    while ( noLength > 0 )
                    {
                        no = revNo % 10;
                        revNo/=10;
                        revNo = parseInt( revNo );
                        if ( string == null )
                        {
                            string = "" + parseInt( no );
                        }
                        else
                        {
                            switch ( length )
                            {
                                case 1:
                                case 2:
                                case 3:
                                    string = string + "" + parseInt( no );
                                    break;

                                default:
                                    if ( i != 0 && ( length  - i ) % 3 == 0 )
                                    {
                                        string = string + "," + parseInt( no );
                                    }
                                    else
                                    {
                                        string = string + "" + parseInt( no );
                                    }
                                    break;
                            }
                        }
                        i++;
                        noLength--;
                    }
                }
                return string;
            },
            scrolledAlready : false,
            keyAlreadyPressed : false,
            countryCode : "",
            //invitePopInvocationCount : 0,//change 15 June
            checkAndSetInvitePopUp : function()
            {
                //cc.log( "invocation count = " + UserDef.getInstance().getValueForKey( INVITE_POP_INVOCATION_COUNT, 0 ));
                if( !UserDef.getInstance().getValueForKey( IS_BUYER, false ) && CSUserdefauts.getInstance().getValueForKey( INVITE_POP_INVOCATION_COUNT, 0 ) < 3
                    && cc.director.getRunningScene().getTag() === sceneTags.GAME_SCENE_TAG )
                {
                    var pop = new FBInvitePopUp();
                    cc.director.getRunningScene().addChild(pop, 10, INVITE_FB_FRIENDS_LAYER_TAG);
                    //instance.invitePopInvocationCount++;
                }
            },
            extractVectorFromSring : function( arrayString ) {
                if (arrayString && arrayString.length > 0) {

                    var numString = "";
                    var returningVec = [];

                    var i = 0;

                    //int a = 0;

                    while (1) {
                        if (i >= arrayString.length || arrayString[i] == ',') {
                            if (numString.length) {
                                var id = parseInt(numString);
                                returningVec.push(id);
                            }
                            numString = "";
                            //a++;
                        } else {
                            numString = numString + arrayString[i];
                        }

                        if (i >= arrayString.length) {
                            break;
                        }
                        i++;
                    }

                    return returningVec;
                }else {
                    return [];
                }
            },
            easyBackInAnimToPopup:function(layer,time){
                layer.runAction( new cc.ScaleTo(time , 0.5 ).easing( cc.easeBackIn() ));

            },
            assignDataFromJSON : function ( json ) {
                if (json ) {
                    var jsonObj = JSON.stringify(json);
                    var jObj = JSON.parse(jsonObj);
                    for (var key in jObj) {
                        if (jObj.hasOwnProperty(key)) {
                            if (key === "thisLevelBet" || key === "levelBar"){
                                CSUserdefauts.getInstance().setValueForKey(key, parseInt(jObj[key]));

                            }else{
                                CSUserdefauts.getInstance().setValueForKey(key, jObj[key]);

                            }
                        }
                    }
                }else {
                    CSUserdefauts.getInstance().setValueForKey(MUSIC, true);
                    CSUserdefauts.getInstance().setValueForKey(SOUND, true);


                }
            },
            easyBackOutAnimToPopup:function(layer,time){
                var scaleP = layer.getScale();
                layer.scale =0;
                layer.runAction( new cc.ScaleTo(time , scaleP ).easing( cc.easeBackOut() ) );

            },
            detectClient : function( window )
            {
                /*
                 * JavaScript Client Detection
                 * (C) viazenetti GmbH (Christian Ludwig)
                 */

                var unknown = '-';

                // browser
                var nVer = navigator.appVersion;
                var nAgt = navigator.userAgent;
                var browser = navigator.appName;
                var version = '' + parseFloat(navigator.appVersion);
                var majorVersion = parseInt(navigator.appVersion, 10);
                var nameOffset, verOffset, ix;

                // Opera
                if ((verOffset = nAgt.indexOf('Opera')) != -1) {
                    browser = 'Opera';
                    ServerData.getInstance().BROWSER_TYPE =OPERA;

                    version = nAgt.substring(verOffset + 6);
                    if ((verOffset = nAgt.indexOf('Version')) != -1) {
                        version = nAgt.substring(verOffset + 8);
                    }
                }
                // Opera Next
                if ((verOffset = nAgt.indexOf('OPR')) != -1) {
                    browser = 'Opera';
                    ServerData.getInstance().BROWSER_TYPE =OPERA;

                    version = nAgt.substring(verOffset + 4);
                }
                // Edge
                else if ((verOffset = nAgt.indexOf('Edge')) != -1) {
                    browser = 'Microsoft Edge';
                    version = nAgt.substring(verOffset + 5);
                }
                // MSIE
                else if ((verOffset = nAgt.indexOf('MSIE')) != -1) {
                    browser = 'Microsoft Internet Explorer';
                    version = nAgt.substring(verOffset + 5);
                }
                // Chrome
                else if ((verOffset = nAgt.indexOf('Chrome')) != -1) {
                    ServerData.getInstance().BROWSER_TYPE =CHROME;
                    browser = 'Chrome';
                    version = nAgt.substring(verOffset + 7);
                }
                // Safari
                else if ((verOffset = nAgt.indexOf('Safari')) != -1) {
                    browser = 'Safari';
                    ServerData.getInstance().BROWSER_TYPE =SAFARI;

                    version = nAgt.substring(verOffset + 7);
                    if ((verOffset = nAgt.indexOf('Version')) != -1) {
                        version = nAgt.substring(verOffset + 8);
                    }
                }
                // Firefox
                else if ((verOffset = nAgt.indexOf('Firefox')) != -1) {
                    browser = 'Firefox';
                    ServerData.getInstance().BROWSER_TYPE = FIREFOX;
                    version = nAgt.substring(verOffset + 8);
                }
                // MSIE 11+
                else if (nAgt.indexOf('Trident/') != -1) {
                    browser = 'Microsoft Internet Explorer';
                    version = nAgt.substring(nAgt.indexOf('rv:') + 3);
                }
                // Other browsers
                else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
                    browser = nAgt.substring(nameOffset, verOffset);
                    version = nAgt.substring(verOffset + 1);
                    if (browser.toLowerCase() == browser.toUpperCase()) {
                        browser = navigator.appName;
                    }
                }
                // trim the version string
                if ((ix = version.indexOf(';')) != -1) version = version.substring(0, ix);
                if ((ix = version.indexOf(' ')) != -1) version = version.substring(0, ix);
                if ((ix = version.indexOf(')')) != -1) version = version.substring(0, ix);

                majorVersion = parseInt('' + version, 10);
                if (isNaN(majorVersion)) {
                    version = '' + parseFloat(navigator.appVersion);
                    majorVersion = parseInt(navigator.appVersion, 10);
                }

                // mobile version
                var mobile = /Mobile|mini|Fennec|Android|iP(ad|od|hone)/.test(nVer);

                // cookie
                var cookieEnabled = (navigator.cookieEnabled) ? true : false;

                if (typeof navigator.cookieEnabled == 'undefined' && !cookieEnabled) {
                    document.cookie = 'testcookie';
                    cookieEnabled = (document.cookie.indexOf('testcookie') != -1) ? true : false;
                }

                // system
                var os = unknown;
                var clientStrings = [
                    {s:'Windows 10', r:/(Windows 10.0|Windows NT 10.0)/},
                    {s:'Windows 8.1', r:/(Windows 8.1|Windows NT 6.3)/},
                    {s:'Windows 8', r:/(Windows 8|Windows NT 6.2)/},
                    {s:'Windows 7', r:/(Windows 7|Windows NT 6.1)/},
                    {s:'Windows Vista', r:/Windows NT 6.0/},
                    {s:'Windows Server 2003', r:/Windows NT 5.2/},
                    {s:'Windows XP', r:/(Windows NT 5.1|Windows XP)/},
                    {s:'Windows 2000', r:/(Windows NT 5.0|Windows 2000)/},
                    {s:'Windows ME', r:/(Win 9x 4.90|Windows ME)/},
                    {s:'Windows 98', r:/(Windows 98|Win98)/},
                    {s:'Windows 95', r:/(Windows 95|Win95|Windows_95)/},
                    {s:'Windows NT 4.0', r:/(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/},
                    {s:'Windows CE', r:/Windows CE/},
                    {s:'Windows 3.11', r:/Win16/},
                    {s:'Android', r:/Android/},
                    {s:'Open BSD', r:/OpenBSD/},
                    {s:'Sun OS', r:/SunOS/},
                    {s:'Linux', r:/(Linux|X11)/},
                    {s:'iOS', r:/(iPhone|iPad|iPod)/},
                    {s:'Mac OS X', r:/Mac OS X/},
                    {s:'Mac OS', r:/(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/},
                    {s:'QNX', r:/QNX/},
                    {s:'UNIX', r:/UNIX/},
                    {s:'BeOS', r:/BeOS/},
                    {s:'OS/2', r:/OS\/2/},
                    {s:'Search Bot', r:/(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/}
                ];
                for (var id in clientStrings) {
                    var cs = clientStrings[id];
                    if (cs.r.test(nAgt)) {
                        os = cs.s;
                        break;
                    }
                }

                var osVersion = unknown;

                if (/Windows/.test(os)) {
                    osVersion = /Windows (.*)/.exec(os)[1];
                    os = 'Windows';
                }

                switch (os) {
                    case 'Mac OS X':
                        osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
                        //isMAC = true;
                        break;

                    case 'Android':
                        osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
                        break;

                    case 'iOS':
                        osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
                        osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);
                        break;
                }

                var finalStr = browser + " " + version + " on " + os + " " + osVersion;
                ServerData.getInstance().device_model= browser + " " + version;
                ServerData.getInstance().device_version = os + " " + osVersion;
                ServerData.getInstance().device_name= browser;

                //console.log(browser+ServerData.getInstance().device_model+ServerData.getInstance().device_version);
              //  console.log(ServerData.getInstance().BROWSER_TYPE);
                return finalStr;
            },
            setTouchSwallingOnPayment:function(parent , node){
                var listener = cc.EventListener.create({
                    event: cc.EventListener.TOUCH_ONE_BY_ONE,
                    swallowTouches: false,
                    onTouchBegan: function (touch, event) {
                        return true;
                    }
                });
                cc.eventManager.addListener( listener, node );
            },
            setTouchSwallowing : function ( parent, node )
            {
                var listener = cc.EventListener.create({
                    event: cc.EventListener.TOUCH_ONE_BY_ONE,
                    swallowTouches: true,
                    onTouchBegan: function (touch, event) {
                        return true;
                    }
                });
                cc.eventManager.addListener( listener, node );

                if( parent == 100 )
                {
                    var listener2 = cc.EventListener.create({
                        event: cc.EventListener.MOUSE,
                        onMouseScroll : function (event) {
                            node.handleScroll( event );

                            /*if( DailyChallenges.getInstance().displayingStatus )
                            {
                                DailyChallenges.getInstance().handleScroll( event );
                            }
                            else
                            {
                                node.handleScroll( event );
                            }*/
                        }
                    });
                    cc.eventManager.addListener( listener2, node );
                }

                var listener3 = cc.EventListener.create({
                    event: cc.EventListener.KEYBOARD,
                    onKeyPressed: function (key, event) {
                        //cc.log( "in DPUtils onKeyPressed" );
                        instance.keyAlreadyPressed = true;
                    },
                    onKeyReleased: function (key, event) {
                        //cc.log( "in DPUtils onKeyReleased" );
                        instance.keyAlreadyPressed = false;
                    }
                });
                cc.eventManager.addListener( listener3, node );
            },
            shuffle: function (array,strt,end) {
                var currentIndex = end, temporaryValue, randomIndex;

                // While there remain elements to shuffle...
                while (strt != currentIndex) {

                    // Pick a remaining element...
                    randomIndex = Math.floor(Math.random() * currentIndex);
                    currentIndex -= 1;

                    // And swap it with the current element.
                    temporaryValue = array[currentIndex];
                    array[currentIndex] = array[randomIndex];
                    array[randomIndex] = temporaryValue;
                }

                return array;
            },
            toggleFullScreen : function( pSender, isForceFull )
            {
                FULLSCREENCHECKED = true;
                if ( cc.screen.fullScreen() ) {
                    cc.screen.exitFullScreen();
                    pSender.getNormalImage().setTexture( res.Full_screen_button_png );
                    pSender.getSelectedImage().setTexture( res.Full_screen_button_png );
                } else if( isForceFull ) {
                    cc.screen.requestFullScreen();
                    pSender.getNormalImage().setTexture( res.Normal_screen_button_png );
                    pSender.getSelectedImage().setTexture( res.Normal_screen_button_png );
                }
            },
            getAllUrlParams : function(url) {

                if( hackBuild )
                {
                   // url = "https://apps.facebook.com/911544245891110?offer_id=DJ1586699070061";
                }
                // get query string from url (optional) or window
                var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

                // we'll store the parameters here
                var obj = {};

                // if query string exists
                if (queryString) {

                    // stuff after # is not part of query string, so get rid of it
                    queryString = queryString.split('#')[0];

                    // split our query string into its component parts
                    var arr = queryString.split('&');

                    for (var i=0; i<arr.length; i++) {
                        // separate the keys and the values
                        var a = arr[i].split('=');

                        // in case params look like: list[]=thing1&list[]=thing2
                        var paramNum = undefined;
                        var paramName = a[0].replace(/\[\d*\]/, function(v) {
                            paramNum = v.slice(1,-1);
                            return '';
                        });

                        // set parameter value (use 'true' if empty)
                        var paramValue = typeof(a[1])==='undefined' ? true : a[1];

                        // (optional) keep case consistent
                        paramName = paramName.toLowerCase();
                        paramValue = paramValue.toLowerCase();

                        //cc.log( "paramName = " + paramName + " paramValue = " + paramValue );

                        // if parameter name already exists
                        if (obj[paramName]) {
                            // convert value to array (if still string)
                            if (typeof obj[paramName] === 'string') {
                                obj[paramName] = [obj[paramName]];
                            }
                            // if no array index number specified...
                            if (typeof paramNum === 'undefined') {
                                // put the value on the end of the array
                                obj[paramName].push(paramValue);
                            }
                            // if array index number specified...
                            else {
                                // put the value at that index number
                                obj[paramName][paramNum] = paramValue;
                            }
                        }
                        // if param name doesn't exist yet, set it
                        else {
                            obj[paramName] = paramValue;
                        }
                    }
                }
                return obj;
            },
            ImageLoadr: function (path, cb) {
                //cc.log( "path called = " + path );
                cc.loader.loadImg( path, true, cb === undefined ?function(err, img){

                    var texture2d = new cc.Texture2D();
                    texture2d.initWithElement(img);
                    texture2d.handleLoadedTexture();
//cc.log( "path stored = " + path );
                    cc.textureCache.cacheImage( path, img );
                } : cb );
            },

        TournamentURLgalaxy:function(idx){
            var url;
            if(ServerData.getInstance().server_cdn_url!=="")
            {
                url= ServerData.getInstance().server_cdn_url+idx+".png";
                return url;
            }
            else {
                if (FB_TOURNAMENT_PICS[idx] !== FacebookObj.userFbID) {
                    url = "https://graph.facebook.com/" + FB_TOURNAMENT_PICS[idx] + "/picture?width=135&height=135";
                    return url;
                }
                else {
                    var id2 = parseInt(cc.rand()) % 2000;
                    return this.TournamentURLgalaxy(id2);
                }
            }

        },
            extractIntArrayFromSring : function( str )
            {
                var arrayString = str;

                var numString = "";

                var i = 0;

                var a = 0;

                var returningArray = [];

                while ( 1 )
                {
                    if ( arrayString[i] == ',' || i >= arrayString.length )
                    {
                        if ( numString.length )
                        {
                            returningArray.push( parseInt( numString ) );
                        }
                        else
                        {
                            break;
                        }
                        numString = "";
                        a++;
                    }
                    else
                    {
                        numString = numString + arrayString[i];
                    }

                    if ( i >= arrayString.length )
                    {
                        break;
                    }
                    i++;
                }
                return returningArray;
            }
//serverconfiguration
        };
    };
    return {
        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function () {
            if ( !instance ) {
                instance = init();
            }
            return instance;
        }
    };
})();