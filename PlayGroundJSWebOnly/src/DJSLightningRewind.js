/**
 * Created by RNF-Mac11 on 2/8/17.
 */



var reelsRandomizationArrayDJSLightningRewind =
    [
        [// 85% payout
            30, 90, 30, 85, 10, 100,
            30, 90, 30, 100, 25,90,
            100, 7, 10,30 , 10, 50,
            10, 50, 5, 50, 10, 70 ,
            10, 115, 10, 50, 10, 50,
            10, 40, 60, 10,10 , 60
        ],
        [//92 %
            30, 90, 20, 85, 10, 100,

            30, 90, 30, 100, 25,90,

            100, 10, 10,30 , 10, 50,

            10, 50, 5, 50, 10, 70 ,

            10, 115, 10, 50, 10, 50,

            10, 40, 60, 10,10 , 70
        ],
        [// 97 %
            20, 90, 20, 85, 5, 100,

            20, 90, 20, 100, 15,90,

            100, 7, 10,30 , 10, 50,

            10, 50, 5, 50, 10, 70 ,

            10, 125, 10, 50, 10, 50,

            10, 40, 60, 10,10 , 60
        ],
        [// 140% payout

            20, 100, 20, 150, 15, 120,
            20, 150, 20, 150, 15, 100,
            100, 30, 10, 50, 10, 50,
            10, 50, 10, 100, 10, 100,
            10, 100, 10, 55, 10, 40,
            10, 70, 10, 70,10 , 100
        ],
        [// 300% payout
            20, 100, 20, 150, 15, 120,
            20, 150, 20, 150, 15, 100,
            100, 30, 10, 50, 10, 50,
            10, 50, 10, 100, 10, 100,
            10, 100, 10, 55, 10, 40,
            10, 70, 10, 70,10 , 100
        ]
    ];

var DJSLightningRewind = GameScene.extend({
    reelsArrayDJSLightningRewind:[],
    ctor:function ( level_index ) {
        this.reelsArrayDJSLightningRewind = [
            /*1*/ELEMENT_EMPTY,/*2*/ELEMENT_7_BAR,/*3*/ ELEMENT_EMPTY,/*4*/ ELEMENT_7_BLUE,/*5*/ ELEMENT_EMPTY,
            /*6*/ ELEMENT_CHERRY,/*7*/ELEMENT_EMPTY,/*8*/ELEMENT_BAR_3,/*9*/ ELEMENT_EMPTY,/*10*/ ELEMENT_BAR_2,
            /*11*/ELEMENT_EMPTY,/*12*/ELEMENT_BAR_1,/*13*/ELEMENT_EMPTY,/*14*/ELEMENT_2X,/*15*/ ELEMENT_EMPTY,
            /*16*/ELEMENT_7_BAR,/*17*/ELEMENT_EMPTY,/*18*/ ELEMENT_BAR_1,/*19*/ELEMENT_EMPTY,/*20*/ELEMENT_7_BLUE,
            /*21*/ ELEMENT_EMPTY,/*22*/ ELEMENT_CHERRY,/*23*/ ELEMENT_EMPTY,/*24*/ELEMENT_BAR_2,/*25*/ELEMENT_EMPTY,
            /*26*/ELEMENT_BAR_3,/*27*/ ELEMENT_EMPTY,/*28*/ ELEMENT_7_BLUE,/*29*/ ELEMENT_EMPTY,/*30*/ELEMENT_CHERRY,
            /*31*/ ELEMENT_EMPTY,/*32*/ELEMENT_7_BAR,/*33*/ELEMENT_EMPTY,/*34*/ ELEMENT_BAR_1,/*35*/ ELEMENT_EMPTY,
            /*36*/ ELEMENT_BAR_2
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSLightningRewind;
        this.physicalArrayCheck= this.reelsArrayDJSLightningRewind;
        this._super( level_index );

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;
    },

    haveExcitement : false,

 reverseCount : 0,

    calculatePayment : function( doAnimate )
    {
        var payMentArray = [
        /*0*/    500, // 2x 2x 2x

        /*1*/    30,  // 7 7 7 Blue

        /*2*/    15,  // triple Cherry

        /*3*/    12,  // 7Bar 7Bar 7Bar

        /*4*/    10,   // 3Bar 3Bar 3Bar

        /*5*/    7,   // 2Bar 2Bar 2Bar

        /*6*/    5,   // Bar Bar Bar

        /*7*/    5,   // Any Bar 7Bar Combo

        /*8*/    1   // Double Cherry
    ];

        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        for ( var i = 0; i < this.slots[0].resultReel.length; i++ )
        {
            tmpVector = this.slots[0].resultReel[i];
            tmpVector2.push( tmpVector[1] );
        }

        this.lineVector.push( tmpVector2 );


        var pay = 0;
        var tmpPay = 1;
        var count2X = 0;
        var countWild = 0;
        var lineLength = 3;
        var countGeneral = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var i, j, k;

        var e = null;
        var pivotElm = null;
        var pivotElm2 = null;
        var tmpVec;

        for ( i = 0; i < this.totalLines; i++ )
        {
            tmpVec = this.lineVector[i];
            for ( k = 0; k < 9; k++ )
            {
                var breakLoop = false;
                countGeneral = 0;
                switch ( k )
                {
                    case 0://for all combination with wild
                        for ( j = 0; j < lineLength; j++ )
                        {
                            if ( this.reelsArrayDJSLightningRewind[this.resultArray[i][j]] != -1 && this.reelsArrayDJSLightningRewind[this.resultArray[i][j]] != ELEMENT_EMPTY )
                            {
                                switch ( this.reelsArrayDJSLightningRewind[this.resultArray[i][j]] )
                                {
                                    case ELEMENT_2X:
                                        count2X++;
                                        tmpPay*=2;
                                        countWild++;
                                        if ( doAnimate ) {
                                            e = tmpVec[ j ];
                                        }

                                        if ( e )
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        switch ( pivotElement )
                                        {
                                            case -1:
                                                pivotElement = this.reelsArrayDJSLightningRewind[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate ) {
                                                    e = tmpVec[ j ];
                                                }
                                                if ( e )
                                                {
                                                    pivotElm = e;
                                                }
                                                break;

                                            default:
                                                pivotElement2 = this.reelsArrayDJSLightningRewind[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate ) {
                                                    e = tmpVec[ j ];
                                                }
                                                if ( e )
                                                {
                                                    pivotElm2 = e;
                                                }
                                                break;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                var cherCount = 0;
                                var wCount = 0;

                                for (var a = 0; a < lineLength; a++)
                                {
                                    switch ( this.reelsArrayDJSLightningRewind[this.resultArray[i][a]] )
                                    {
                                        case ELEMENT_2X:
                                            wCount++;

                                            if ( doAnimate ) {
                                                e = tmpVec[ a ];
                                            }
                                            if ( e )
                                            {
                                                e.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_CHERRY:
                                            cherCount++;
                                            if ( doAnimate ) {
                                                e = tmpVec[ a ];
                                            }
                                            if ( e )
                                            {
                                                e.aCode = SCALE_TAG;
                                            }
                                            break;
                                    }
                                }

                                if ( ( !wCount && cherCount == 2 ) )
                                {
                                    pay = payMentArray[8];
                                    breakLoop = true;
                                }
                                else if( cherCount && wCount )
                                {
                                    pay = 2 * payMentArray[8];
                                    breakLoop = true;
                                }
                                break;
                            }
                        }

                        if ( j == 3 )
                        {
                            if ( count2X == 3 )
                            {
                                pay = payMentArray[0];
                                breakLoop = true;
                            }
                            else if( countWild >= 2 )
                            {
                                breakLoop = true;

                                switch ( pivotElement )
                                {
                                    case ELEMENT_7_BLUE:
                                        pay = tmpPay * payMentArray[1];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }

                                        break;

                                    case ELEMENT_CHERRY:
                                        pay = tmpPay * payMentArray[2];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_7_BAR:
                                        pay = tmpPay * payMentArray[3];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_3:
                                        pay = tmpPay * payMentArray[4];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_2:
                                        pay = tmpPay * payMentArray[5];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_1:
                                        pay = tmpPay * payMentArray[6];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        pay = tmpPay;
                                        if ( pivotElm )
                                        {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;
                                }
                            }
                            else if( countWild == 1 )
                            {
                                breakLoop = true;

                                if ( pivotElement == pivotElement2 )
                                {
                                    switch ( pivotElement )
                                    {
                                        case ELEMENT_7_BLUE:
                                            pay = tmpPay * payMentArray[1];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }

                                            break;

                                        case ELEMENT_CHERRY:
                                            pay = tmpPay * payMentArray[2];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_7_BAR:
                                            pay = tmpPay * payMentArray[3];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_3:
                                            pay = tmpPay * payMentArray[4];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_2:
                                            pay = tmpPay * payMentArray[5];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_1:
                                            pay = tmpPay * payMentArray[6];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        default:
                                            break;
                                    }
                                }
                                else
                                {
                                    if((pivotElement == ELEMENT_BAR_1 || pivotElement == ELEMENT_7_BAR) &&
                                        (pivotElement2 == ELEMENT_BAR_1 || pivotElement2 == ELEMENT_7_BAR))
                                    {
                                        pay = tmpPay * payMentArray[6];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if( ( pivotElement == ELEMENT_7_BAR || ( pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_BAR_3 ) ) &&
                                        ( pivotElement2 == ELEMENT_7_BAR || ( pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_BAR_3 ) ) )
                                    {
                                        pay = tmpPay * payMentArray[7];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if( pivotElement == ELEMENT_CHERRY )
                                    {
                                        pay = tmpPay * payMentArray[8];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if( pivotElement2 == ELEMENT_CHERRY )
                                    {
                                        pay = tmpPay * payMentArray[8];
                                        if ( doAnimate ) {
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                }
                            }
                        }
                        break;

                    case 1: // 7 7 7 BLUe
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSLightningRewind[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_BLUE:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[1];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }

                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 2: // triple cherry
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSLightningRewind[this.resultArray[i][j]] )
                            {
                                case ELEMENT_CHERRY:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[2];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        else if ( countGeneral == 2 )//double cherry
                        {
                            pay = payMentArray[8];
                            breakLoop = true;
                            for ( var l = 0; l < 3; l++ )
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if ( e && e.eCode == ELEMENT_CHERRY )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 3: // 7 7 7 bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSLightningRewind[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[3];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 4: // 3Bar 3Bar 3Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSLightningRewind[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[4];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 5:
                        //double cherry
                        break;

                    case 6: // 2Bar 2Bar 2Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSLightningRewind[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[5];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 7: // Bar Bar Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSLightningRewind[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[6];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 8: // any Bar combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSLightningRewind[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[7];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

                if ( breakLoop )
                {
                    break;
                }
            }
        }

        if ( doAnimate )
        {
            this.currentWin = this.currentBet * pay;
            if (this.currentWin > 0)
            {
                this.addWin = this.currentWin / 10;
                this.setCurrentWinAnimation();
            }

            this.checkAndSetAPopUp();
        }
        else
        {
            this.currentWinForcast = this.currentBet * pay;
        }
    },
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSLightningRewind[i]=this.physicalReelElements[idx];
                i++;
            }
        }
    },
    changeTotalProbabilities : function()
    {
        this.totalProbalities = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            //this.extractArrayFromSring();
            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[this.easyModeCode][i];
            }
        }
        else
        {
            for ( var i = 0; i < 36; i++ )
            {
                this.probabilityArray[this.easyModeCode][i] = reelsRandomizationArrayDJSLightningRewind[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSLightningRewind[this.easyModeCode][i];
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
        if ( this.haveReversal )
        {
            return;
        }
        if( ( this.reelsArrayDJSLightningRewind[this.resultArray[0][0]] == ELEMENT_7_PINK && this.reelsArrayDJSLightningRewind[this.resultArray[0][1]] == ELEMENT_2X ) ||
            ( this.reelsArrayDJSLightningRewind[this.resultArray[0][1]] == ELEMENT_7_PINK && this.reelsArrayDJSLightningRewind[this.resultArray[0][0]] == ELEMENT_2X ) ||
            ( this.reelsArrayDJSLightningRewind[this.resultArray[0][0]] == ELEMENT_7_PINK && this.reelsArrayDJSLightningRewind[this.resultArray[0][1]] == ELEMENT_7_PINK ) ||
            ( this.reelsArrayDJSLightningRewind[this.resultArray[0][0]] == ELEMENT_2X && this.reelsArrayDJSLightningRewind[this.resultArray[0][1]] == ELEMENT_2X ) ||
            ( this.reelsArrayDJSLightningRewind[this.resultArray[0][0]] == ELEMENT_CHERRY && this.reelsArrayDJSLightningRewind[this.resultArray[0][1]] == ELEMENT_2X ) ||
            ( this.reelsArrayDJSLightningRewind[this.resultArray[0][1]] == ELEMENT_CHERRY && this.reelsArrayDJSLightningRewind[this.resultArray[0][0]] == ELEMENT_2X ) ||
            ( this.reelsArrayDJSLightningRewind[this.resultArray[0][0]] == ELEMENT_7_BAR && this.reelsArrayDJSLightningRewind[this.resultArray[0][1]] == ELEMENT_2X ) ||
            ( this.reelsArrayDJSLightningRewind[this.resultArray[0][1]] == ELEMENT_7_BAR && this.reelsArrayDJSLightningRewind[this.resultArray[0][0]] == ELEMENT_2X ) ||
            ( this.reelsArrayDJSLightningRewind[this.resultArray[0][0]] == ELEMENT_7_BLUE && this.reelsArrayDJSLightningRewind[this.resultArray[0][1]] == ELEMENT_2X ) ||
            ( this.reelsArrayDJSLightningRewind[this.resultArray[0][1]] == ELEMENT_7_BLUE && this.reelsArrayDJSLightningRewind[this.resultArray[0][0]] == ELEMENT_2X ) ||
            ( this.reelsArrayDJSLightningRewind[this.resultArray[0][0]] == ELEMENT_7_BLUE && this.reelsArrayDJSLightningRewind[this.resultArray[0][1]] == ELEMENT_7_BLUE ) )
        {
            if ( !this.slots[0].isForcedStop )
            {
                this.schedule( this.reelStopper, 4.8 );

                this.delegate.jukeBox( S_EXCITEMENT );

                var glowSprite = new cc.Sprite( glowStrings[0] );

                if ( glowSprite )
                {
                    var r = this.slots[0].physicalReels[this.slots[0].physicalReels.length - 1];

                    glowSprite.setPosition( cc.p( r.getParent().getPosition().x + this.slots[0].getPosition().x, r.getParent().getPosition().y + this.slots[0].getPosition().y ) );

                    this.scaleAccordingToSlotScreen( glowSprite );

                    this.addChild( glowSprite, 5 );
                    this.animNodeVec.push( glowSprite );

                    glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ) ) );
                }
            }
            this.haveExcitement = true;
        }
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
    },

    generateResult : function( scrIndx )
    {
        for ( var i = 0; i < this.slots[scrIndx].displayedReels.length; i++ )
        {
            var num = this.prevReelIndex[i];

            var tmpnum = 0;

            while ( num == this.prevReelIndex[i] )
            {
                num = Math.floor(Math.random() * this.totalProbalities);
            }
            this.prevReelIndex[i] = num;
            this.resultArray[0][i] = num;

            for ( var j = 0; j < 36; j++ )
            {
                tmpnum+=this.probabilityArray[this.easyModeCode][j];

                if ( tmpnum >= this.resultArray[0][i] )
                {
                    this.resultArray[0][i] = j;
                    break;
                }
            }

            /*/HACK
             if (i == 0)
             {
             resultArray[0][i] = 0;
             }
             else if (i == 1)
             {
             resultArray[0][i] = 13;
             }
             else if(i == 2)
             {
             resultArray[0][i] = 13;
             }
             else
             {
             resultArray[0][i] = 0;
             }//*///HACK
        }

        this.calculatePayment( false );
        this.checkForFalseWin();

    },

    reversalAnimCB : function( obj )
{
    this.slots[0].setReelsDirection( -1 );
    this.spin();

    obj.removeFromParent( true );

    this.removeChildByTag( D_NODE_TAG );
},

    rollUpdator : function(dt)
    {
        var i;
        for (i = 0; i < this.totalSlots; i++)
        {
            this.slots[i].updateSlot(dt);
        }

        for (i = 0; i < this.totalSlots; i++)
        {
            if ( this.slots[i].isSlotRunning() )
            {
                break;
            }
        }

        if (i >= this.totalSlots)
        {
            this.isRolling = false;
            this.unschedule( this.rollUpdator );
            this.calculatePayment( true );

            if (this.isAutoSpinning)
            {
                if (this.currentWin > 0)
                {
                    this.scheduleOnce( this.autoSpinCB, 1.5);
                }
                else
                {
                    this.scheduleOnce( this.autoSpinCB, 1.0);
                }
            }
        }
    },

    setFalseWin : function()
    {
        var highWinIndices = [ 13, 1, 3 ];
        var missIndices = [ 13, 1, 3 ];

        var missedIndx = Math.floor( Math.random() * 3 );

        if( missedIndx == 2 )
        {
            if( Math.floor( Math.random() * 10 ) == 3 )
            {
            }
            else
            {
                missedIndx = Math.floor( Math.random() * 2 );
            }
        }

        var sameIndex = Math.floor( Math.random( ) * highWinIndices.length );

        for ( var i = 0; i < 3; i++ )
        {
            if ( i == missedIndx )
            {
                this.resultArray[0][i] = this.getRandomIndex( missIndices[ Math.floor( Math.random( ) * missIndices.length ) ], true );
            }
            else
            {
                this.resultArray[0][i] = this.getRandomIndex( highWinIndices[ sameIndex ], false );
            }
        }
    },

    setResult : function( indx, lane, sSlot )
    {
        var gap = 387;

        var j = 0;
        var animOriginY;
        var direction = 1;

        if ( this.haveReversal )
        {
            direction = -1;
        }


        var r = this.slots[0].physicalReels[lane];
        var arrSize = r.elementsVec.length;
        var spr;
        var spriteVec = new Array();

        animOriginY = gap * direction;
        spr = r.elementsVec[indx];
        if ( spr.eCode == ELEMENT_EMPTY )
        {
            gap = 285 * spr.getScale();
        }

        if ( indx == 0 )
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push( spr );

            spr.setPositionY( animOriginY );

            for ( var i = 0; i < 2; i++ )
            {
                spr = r.elementsVec[i];
                spriteVec.push( spr );
                spr.setPositionY( animOriginY + gap * ( i + 1 ) * direction );
            }
        }
        else if ( indx == arrSize - 1 )
        {
            spr = r.elementsVec[0];
            spriteVec.push( spr );

            spr.setPositionY( animOriginY );
            for ( var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i-- )
            {
                spr = r.elementsVec[i];

                spriteVec.push( spr );
                spr.setPositionY( animOriginY + gap * ( j + 1 ) * direction );
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push( spr );
            spr.setPositionY( animOriginY );

            for ( var i = indx; i < indx + 2; i++ )
            {
                spr = r.elementsVec[i];

                spriteVec.push( spr );
                spr.setPositionY( animOriginY + gap * ( j + 1 ) * direction );
                j++;
            }
        }


        for ( var i = 0; i < spriteVec.length; i++ )
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            gap = -773.5;

            var eee;
            if ( i < 2 )
            {
                eee = spriteVec[i + 1];
            }
            if ( i == 0 )
            {
                var ee = spriteVec[i + 1];
                if ( Math.abs( e.getPositionY() - ee.getPositionY() ) != 387 )
                {
                    gap = -( Math.abs( e.getPositionY() - ee.getPositionY() ) * 2 * ee.getScale() );
                }
            }


            gap*=direction;


            var ease;

            if ( this.slots[sSlot].isForcedStop )
            {
                ease = new cc.MoveTo(0.15, cc.p(0, e.getPositionY() + gap)).easing( cc.easeBackOut() );
            }
            else
            {
                ease = new cc.MoveTo(0.3, cc.p(0, e.getPositionY() + gap)).easing( cc.easeBackOut() );
            }
            ease.setTag( MOVING_TAG );
            e.runAction( ease );
        }

        return spriteVec;
    },

    setReversalAnim : function()
{
    this.delegate.jukeBoxStop( S_COIN );
    this.delegate.jukeBoxStop( S_COIN_MORE );
    this.delegate.jukeBox( S_REVERSAL );

    //var vSize = Director::getInstance().getVisibleSize();

    var sprite = new cc.Sprite( "res/DJSLightningRewind/black_bg.png" );
    sprite.setTag( D_NODE_TAG );
    sprite.setOpacity( 150 );
    sprite.setScale( 2.1 );
    sprite.setPosition( cc.p( this.vSize.width - sprite.getContentSize().width / 2, this.vSize.height / 2 + 60 ) );
    this.addChild( sprite, 4 );

    var r = this.slots[0].physicalReels[1];
    var node = r.getParent();

    var revSprite = new cc.Sprite( "#rev.png" );
    revSprite.setTag( REVERSE_TAG );
    revSprite.setPosition( cc.p(node.getPosition().x + this.slots[0].getPosition().x, node.getPosition().y + this.slots[0].getPosition().y ) );
    //cc.log( 'revSpritePosX = ' + revSprite.getPosition().x + 'revSpritePosY = ' + revSprite.getPosition().y );
    this.addChild( revSprite, 5 );
    var rBy = new cc.RotateBy( 1, 720 );
    var rByRev = rBy.reverse();
    revSprite.setScale( 0.25 );
    revSprite.runAction( new cc.Sequence( new cc.ScaleTo( 0.5, 1 ), rBy.easing( cc.easeBackInOut() ), rByRev.easing( cc.easeBackInOut() ), new cc.ScaleBy( 0.5, 0.25 ), cc.callFunc( this.reversalAnimCB, this ) ) );

    var particle = new cc.ParticleSun();
    particle.texture = cc.textureCache.addImage( res.FireSnow_png );

    particle.setPosition( revSprite.getContentSize().width * 0.75, revSprite.getContentSize().height * 0.75 );
    revSprite.addChild( particle );
},

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSLightningRewind;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    setWinAnimation : function( animCode, screen )
{
    var tI = 0.3;

    var fire;
    for ( var i = 0; i < this.animNodeVec.length; i++ )
    {
        fire = this.animNodeVec[i];
        fire.removeFromParent( true );
    }
    this.animNodeVec = [];


    switch ( animCode )
    {
        case WIN_TAG:

            for ( var i = 0; i < this.slots[0].blurReels.length; i++ )
        {
            var r = this.slots[0].blurReels[i];

            var node = r.getParent();

            var moveW = this.slots[0].reelDimentions.x * this.slots[0].getScale();
            var moveH = this.slots[0].reelDimentions.y * this.slots[0].getScale();

            for ( var j = 0; j < 4; j++ )
            {

                var fire = new cc.ParticleSun();
                fire.texture = cc.textureCache.addImage( res.FireSnow_png );
                var pos;

                this.addChild( fire, 5 );
                this.animNodeVec.push( fire );

                var moveBy = null;

                var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                diff*=( 2 - i );

                switch ( j % 4 )
                {
                    case 0:
                        pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY());
                        fire.setPosition( pos );
                        moveBy = new cc.MoveBy( tI, cc.p( moveW, 0 ) );
                        break;

                    case 1:
                        pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY());
                        fire.setPosition( pos );
                        moveBy = new cc.MoveBy( tI, cc.p( 0, moveH ) );
                        break;

                    case 2:
                        pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                        fire.setPosition( pos );
                        moveBy = new cc.MoveBy( tI, cc.p( -moveW, 0 ) );
                        break;

                    case 3:
                        pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                        fire.setPosition( pos );
                        moveBy = new cc.MoveBy( tI, cc.p( 0, -moveH ) );
                        break;

                    default:
                        break;
                }
                fire.runAction( new cc.RepeatForever( new cc.Sequence( moveBy, moveBy.reverse() ) ) );
            }
        }

            break;

        case BIG_WIN_TAG:
            for( var i = 0; i < this.slots[0].blurReels.length; i++ )
        {
            var r = this.slots[0].blurReels[i];

            var node = r.getParent();

            var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
            diff*=( 2 - i );

            var glowSprite = new cc.Sprite( glowStrings[1] );

            if ( glowSprite )
            {
                glowSprite.setPosition( node.getPositionX() + this.slots[0].getPositionX() - diff, node.getPositionY() + this.slots[0].getPositionY() );

                this.addChild( glowSprite, 5 );
                this.animNodeVec.push( glowSprite );
                this.scaleAccordingToSlotScreen( glowSprite );
                glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ) ) );
            }
        }
            break;

        default:
            break;
    }

    if ( !this.haveExcitement )
    {
        var tmpNo = Math.floor(Math.random() * 100);
        if ( this.reverseCount > 0 )
        {
            this.reverseCount--;
            if ( this.reverseCount <= 0 )
            {
                this.haveReversal = false;
                for ( var i = 0; i < this.slots[0].blurReels.length; i++ )
                {
                    this.slots[0].blurReels[i].resetPosition( 1 );
                }

                this.slots[0].setReelsDirection( 1 );
            }
            else
            {
                this.setReversalAnim();
            }
        }
        else if ( tmpNo % 9 == 0 )
        {
            this.reverseCount = Math.floor( Math.random() * 3 ) + 1;
            this.haveReversal = true;
            this.setReversalAnim();

            this.touchPressed = false;

            for ( var i = 0; i < this.slots[0].blurReels.length; i++ )
            {
                this.slots[0].blurReels[i].resetPosition( -1 );
            }
        }
    }
},

    spin : function()
    {

        this.unschedule( this.updateWinCoin );
        if ( !this.haveReversal )
        {
            this.deductBet( this.currentBet );
        }

        if ( this.displayedScore != this.totalScore )
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;
        this.haveExcitement = false;

        this.currentWin = 0;
        this.currentWinForcast = 0;
        this.startRolling();


        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    }

});