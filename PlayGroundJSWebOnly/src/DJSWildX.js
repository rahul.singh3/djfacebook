/**
 * Created by RNF-Mac11 on 2/9/17.
 */



var reelsRandomizationArrayDJSWildX =
    [
        [// 85% payout
            20, 1, 5, 50, 15, 60,

            15, 50, 5, 50, 5, 32,

            5,20, 5, 30, 10, 30,

            5, 5, 8, 3,5 , 3,

            5, 2, 5, 20, 5, 25,

            5, 10, 20, 3, 5, 10
        ],
        [// 92 %
            20, 2, 5, 50, 15, 60,

            15, 50, 5, 50, 5, 35,

            5,20, 5, 30, 10, 30,

            5, 5, 8, 3,5 , 3,

            5, 2, 5, 20, 5, 25,

            5, 10, 20, 3, 5, 10
        ],
        [
            // 94.5 - 98% payout
            20, 3, 5, 50, 15, 60,
            15, 50, 5, 50, 5, 30,
            5,20, 5, 30, 10, 30,
            5, 8, 8, 3,5 , 5,
            5, 2, 5, 20, 5, 25,
            5, 10, 20, 3, 5, 10
        ],
        [

            // 140% + payout
            10, 5, 5, 60, 10, 70,
            10, 60, 5, 60, 5, 40,
            5,30, 5, 40, 5, 40,
            5, 15, 5, 5,5 , 10,
            5, 3, 5, 30, 5, 30,
            5, 10, 10, 5, 5, 20
        ],
        [
            // 300% + payout
            10, 5, 5, 60, 10, 70,
            10, 60, 5, 60, 5, 40,
            5,30, 5, 40, 5, 40,
            5, 15, 5, 5,5 , 10,
            5, 3, 5, 30, 5, 30,
            5, 10, 10, 5, 5, 20
        ]
    ];

var wildArr = [
    ELEMENT_25X, ELEMENT_20X, ELEMENT_15X, ELEMENT_10X, ELEMENT_5X, ELEMENT_3X, ELEMENT_2X
];

var wildRandArr = [
    1,5,7,10,15,25,60
];

var DJSWildX = GameScene.extend({
    wildElms : null,

    timeElapsedWild : 0,

    wildRotationCount : 0,

    wildIndx : null,

    wildActivated : false,

    payOutVec : null,

    currentWildIndx : 0,

    totalWildProbabilities : 0,

    isScheduled : false,
    reelsArrayDJSWildX:[],
    ctor:function ( level_index ) {
        this.reelsArrayDJSWildX = [
            /*1*/ELEMENT_EMPTY,/*2*/ ELEMENT_X,/*3*/ ELEMENT_EMPTY,/*4*/ ELEMENT_BAR_3,/*5*/ ELEMENT_EMPTY,
            /*6*/ELEMENT_BAR_2,/*7*/ELEMENT_EMPTY,/*8*/ ELEMENT_BAR_1,/*9*/ ELEMENT_EMPTY,/*10*/ ELEMENT_7_PINK,
            /*11*/ELEMENT_EMPTY,/*12*/ELEMENT_CHERRY,/*13*/ELEMENT_EMPTY,/*14*/ ELEMENT_BAR_3,/*15*/ELEMENT_EMPTY,
            /*16*/ELEMENT_7_PINK,/*17*/ ELEMENT_EMPTY,/*18*/ ELEMENT_BAR_1,/*19*/ELEMENT_EMPTY,/*20*/ ELEMENT_CHERRY,
            /*21*/ ELEMENT_EMPTY,/*22*/ ELEMENT_X,/*23*/ELEMENT_EMPTY,/*24*/ELEMENT_CHERRY,/*25*/ELEMENT_EMPTY,
            /*26*/ ELEMENT_X,/*27*/ ELEMENT_EMPTY,/*28*/ ELEMENT_BAR_2,/*29*/ELEMENT_EMPTY,/*30*/ ELEMENT_BAR_1,
            /*31*/ELEMENT_EMPTY,/*32*/ELEMENT_BAR_3,/*33*/ ELEMENT_EMPTY,/*34*/ ELEMENT_X,/*35*/ ELEMENT_EMPTY,
            /*36*/ELEMENT_BAR_2
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSWildX;
        this.physicalArrayCheck= this.reelsArrayDJSWildX;
        this._super( level_index );

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.wildElms = new Array(3);

        this.wildIndx = new Array(3);

        this.changeTotalProbabilities();

        this.payOutVec = [
            /*0*/ 5000,//Wild Wild Wild

            /*1*/ 30,  //7Pink 7Pink 7Pink

            /*2*/ 10,  //Triple Cherry

            /*3*/ 2,  //Double Cherry

            /*4*/ 7,  //Triple Bar

            /*5*/ 5,   //Double Bar

            /*6*/ 3,   //Single Bar

            /*7*/ 1  //Any Bar
        ];

        return true;


    },

    calculatePayment : function( doAnimate )
    {
        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        for (var i = 0; i < this.slots[0].resultReel.length; i++)
        {
            tmpVector = this.slots[0].resultReel[i];
            tmpVector2.push( tmpVector[1] );
        }

        this.lineVector.push(tmpVector2);

        var lineLength = 3;
        var winScenario = 8;
        var pay = 0;
        var tmpPay = 1;
        var countX = 0;
        var countWild = 0;
        var countGeneral = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var i, j, k;
        var e = null;
        var pivotElm = null;
        var pivotElm2 = null;
        var tmpVec;


        for (i = 0; i < this.totalLines; i++)
        {
            tmpVec = this.lineVector[i];
            for (k = 0; k < winScenario; k++)
            {
                var breakLoop = false;
                countGeneral = 0;
                switch (k)
                {
                    case 0://for all combination with wild
                        for (j = 0; j < lineLength; j++)
                        {
                            if (this.reelsArrayDJSWildX[this.resultArray[i][j]] != -1 && this.reelsArrayDJSWildX[this.resultArray[i][j]] != ELEMENT_EMPTY)
                            {
                                switch (this.reelsArrayDJSWildX[this.resultArray[i][j]])
                                {
                                    case ELEMENT_X:
                                        countX++;
                                        countWild++;
                                        if ( doAnimate ) {
                                            e = tmpVec[j];
                                        }
                                        if (e)
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        switch (pivotElement)
                                        {
                                            case -1:
                                                pivotElement = this.reelsArrayDJSWildX[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate ) {
                                                    e = tmpVec[j];
                                                }
                                                if (e)
                                                {
                                                    pivotElm = e;
                                                }
                                                break;

                                            default:
                                                pivotElement2 = this.reelsArrayDJSWildX[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate ) {
                                                    e = tmpVec[j];
                                                }
                                                if (e)
                                                {
                                                    pivotElm2 = e;
                                                }
                                                break;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                var cherCount = 0;
                                var wCount = 0;

                                for (var a = 0; a < lineLength; a++)
                                {
                                    switch ( this.reelsArrayDJSWildX[this.resultArray[i][a]] )
                                    {
                                        case ELEMENT_X:
                                            wCount++;

                                            if ( doAnimate ) {
                                                e = tmpVec[ a ];
                                            }
                                            if ( e )
                                            {
                                                e.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_CHERRY:
                                            cherCount++;
                                            if ( doAnimate ) {
                                                e = tmpVec[ a ];
                                            }
                                            if ( e )
                                            {
                                                e.aCode = SCALE_TAG;
                                            }
                                            break;
                                    }
                                }

                                if ( ( !wCount && cherCount == 2 ) )
                                {
                                    pay = this.payOutVec[ 3 ];
                                    breakLoop = true;
                                }
                                else if( cherCount && wCount )
                                {
                                    pay = this.payOutVec[ 3 ];
                                    breakLoop = true;
                                }
                                break;
                            }
                        }

                        if (j == 3)
                        {
                            if(countWild >= 2)
                            {
                                breakLoop = true;
                                switch (pivotElement)
                                {
                                    case ELEMENT_7_PINK:
                                        pay = tmpPay * this.payOutVec[ 1 ];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }

                                        break;

                                    case ELEMENT_CHERRY:
                                        pay = tmpPay * this.payOutVec[ 2 ];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_3:
                                        pay = tmpPay * this.payOutVec[ 4 ];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_2:
                                        pay = tmpPay * this.payOutVec[ 5 ];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_1:
                                        pay = tmpPay * this.payOutVec[ 6 ];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        pay = tmpPay;
                                        if (pivotElm)
                                        {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;
                                }
                            }
                            else if(countWild == 1)
                            {
                                breakLoop = true;
                                if (pivotElement == pivotElement2)
                                {
                                    switch (pivotElement)
                                    {
                                        case ELEMENT_7_PINK:
                                            pay = tmpPay * this.payOutVec[ 1 ];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }

                                            break;

                                        case ELEMENT_CHERRY:
                                            pay = tmpPay * this.payOutVec[ 2 ];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_3:
                                            pay = tmpPay * this.payOutVec[ 4 ];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_2:
                                            pay = tmpPay * this.payOutVec[ 5 ];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_1:
                                            pay = tmpPay * this.payOutVec[ 6 ];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        default:
                                            pay = tmpPay;
                                            break;
                                    }
                                }
                                else
                                {
                                    if((pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_BAR_3) &&
                                        (pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_BAR_3))
                                    {
                                        pay = tmpPay * this.payOutVec[ 7 ];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if( pivotElement == ELEMENT_CHERRY )
                                    {
                                        pay = tmpPay * this.payOutVec[ 3 ];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if(pivotElement2 == ELEMENT_CHERRY )
                                    {
                                        pay = tmpPay * this.payOutVec[ 3 ];
                                        if ( doAnimate ) {
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                }
                            }
                        }
                        break;

                    case 1: // 7 7 7 Pink
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSWildX[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_PINK:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = this.payOutVec[ 1 ];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }

                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 2: // 3Bar 3Bar 3Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSWildX[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = this.payOutVec[ 4 ];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 3: // 2Bar 2Bar 2Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSWildX[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = this.payOutVec[ 5 ];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 4: // Bar Bar Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSWildX[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = this.payOutVec[ 6 ];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if (e)
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 5: // any Bar combo
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSWildX[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = this.payOutVec[ 7 ];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++)
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 6: // Cherry
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSWildX[this.resultArray[i][j]])
                            {
                                case ELEMENT_CHERRY:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = this.payOutVec[ 2 ];
                            breakLoop = true;
                        }
                        else if (countGeneral == 2)
                        {
                            pay = this.payOutVec[ 3 ];
                            breakLoop = true;
                        }

                        for (var l = 0; l < 3; l++)
                    {
                        if ( doAnimate ) {
                            e = tmpVec[l];
                        }
                        if ( e && e.eCode == ELEMENT_CHERRY )
                        {
                            e.aCode = SCALE_TAG;
                        }
                    }
                        break;

                    default:
                        break;
                }

                if (breakLoop)
                {
                    break;
                }
            }
        }

        if ( doAnimate )
        {
            this.currentWin = this.currentBet * pay;
            if (this.currentWin > 0)
            {
                if (this.haveWild)
                {
                    this.startWildRolling();
                }
                else
                {
                    this.addWin = this.currentWin / 10;
                    this.setCurrentWinAnimation();
                }
            }

            if ( !this.haveWild )
            {
                this.checkAndSetAPopUp();
            }
        }
        else
        {
            this.currentWinForcast = this.currentBet * pay;
        }
    },
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSWildX[i]=this.physicalReelElements[idx];
                i++;
            }
        }
    },
    changeTotalProbabilities : function()
    {
        this.totalProbalities = 0;
        this.totalWildProbabilities = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            //this.extractArrayFromSring();
            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[this.easyModeCode][i];
                if (i < 7)
                {
                    this.totalWildProbabilities+=wildRandArr[i];
                }
            }
        }
        else
        {
            for (var i = 0; i < 36; i++)
            {
                this.probabilityArray[this.easyModeCode][i] = reelsRandomizationArrayDJSWildX[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSWildX[this.easyModeCode][i];

                if (i < 7)
                {
                    this.totalWildProbabilities+=wildRandArr[i];
                }
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
    },

    generateResult : function( scrIndx )
    {
        for (var i = 0; i < this.slots[scrIndx].displayedReels.length; i++)
        {
            var num = this.prevReelIndex[i];

            var tmpnum = 0;

            while (num == this.prevReelIndex[i])
            {
                num = Math.floor(Math.random() * this.totalProbalities);
            }
            this.prevReelIndex[i] = num;
            this.resultArray[0][i] = num;

            for (var j = 0; j < 36; j++)
            {
                tmpnum+=this.probabilityArray[this.easyModeCode][j];

                if (tmpnum >= this.resultArray[0][i])
                {
                    this.resultArray[0][i] = j;
                    break;
                }
            }


            /*/HACK
             if (i == 0)
             {
             resultArray[0][i] = 1;//2;
             }
             else if (i == 1)
             {
             resultArray[0][i] = 7;//2, 4, 20, 30
             }
             else if(i == 2)
             {
             resultArray[0][i] = 23;
             }
             else
             {
             resultArray[0][i] = 0;
             }//*///HACK

            if (this.reelsArrayDJSWildX[this.resultArray[0][i]] == ELEMENT_X)
            {
                this.haveWild = true;
                this.wildIndx[i] = this.generateWildResult();
                var r = this.slots[0].wildReel[i];
                this.wildElms[i] = r.elementsVec[this.wildIndx[i]];
            }
        }

        this.calculatePayment( false );
        this.checkForFalseWin();

    },

    generateWildResult : function()
    {
        var indx = Math.floor(Math.random() * this.totalWildProbabilities);
        var tmpNum = 0;
        for (var i = 0; i < 7; i++)
        {
            tmpNum+=wildRandArr[i];
            if (tmpNum >= indx)
            {
                indx = i;
                break;
            }
        }
        return indx;
    },

    rollUpdator : function(dt)
    {
        var i;
        for (i = 0; i < this.totalSlots; i++)
        {
            this.slots[i].updateSlot(dt);
        }

        for (i = 0; i < this.totalSlots; i++)
        {
            if ( this.slots[i].isSlotRunning() )
            {
                break;
            }
        }

        if (i >= this.totalSlots)
        {
            this.isRolling = false;
            this.unschedule( this.rollUpdator );
            this.calculatePayment( true );

            if (this.isAutoSpinning && !this.isRolling )
            {
                if (this.currentWin > 0)
                {
                    this.scheduleOnce( this.autoSpinCB, 1.5);
                }
                else
                {
                    this.scheduleOnce( this.autoSpinCB, 1.0);
                }
            }
        }
    },

    rollUpdatorWild : function( dt )
{
    this.slots[0].updateSlotWild(dt);
},

    setFalseWin : function()
    {
        var highWinIndices = [ 1, 9 ];
        var missIndices = [ 1, 9, 19 ];

        var missedIndx = Math.floor( Math.random() * 3 );

        if( missedIndx == 2 )
        {
            if( Math.floor( Math.random() * 10 ) == 3 )
            {
            }
            else
            {
                missedIndx = Math.floor( Math.random() * 2 );
            }
        }

        var sameIndex = Math.floor( Math.random( ) * highWinIndices.length );

        for ( var i = 0; i < 3; i++ )
        {
            if ( i == missedIndx )
            {
                this.resultArray[0][i] = this.getRandomIndex( missIndices[ Math.floor( Math.random( ) * missIndices.length ) ], true );
            }
            else
            {
                this.resultArray[0][i] = this.getRandomIndex( highWinIndices[ sameIndex ], false );
            }
        }
    },

    setResult : function( indx, lane, sSlot )
    {
        var gap = 387.0;

        var j = 0;
        var animOriginY;

        var r = this.slots[0].physicalReels[lane];
        var arrSize = r.elementsVec.length;
        var spr;

        var spriteVec = new Array();

        animOriginY = gap * r.direction;
        spr = r.elementsVec[indx];
        if ( spr.eCode == ELEMENT_EMPTY )
        {
            gap = 285 * spr.getScale();
        }

        if ( indx == 0 )
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push(spr);

            spr.setPositionY( animOriginY );

            for (var i = 0; i < 2; i++)
            {
                spr = r.elementsVec[i];
                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (i + 1) * r.direction);
            }
        }
        else if (indx == arrSize - 1)
        {
            spr = r.elementsVec[0];
            spriteVec.push(spr);

            spr.setPositionY(animOriginY);
            for (var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i--)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction );
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push(spr);
            spr.setPositionY(animOriginY);

            for (var i = indx; i < indx + 2; i++)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }

        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            gap = -773.5;

            var eee;
            if (i < 2)
            {
                eee = spriteVec[i + 1];
            }
            if (i == 0)
            {
                var ee = spriteVec[i + 1];
                if ( Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) ) != 387)
                {
                    gap = -Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) * 2 * ee.getScale() );
                }
            }

            gap*=r.direction;
            var ease;

            if ( this.slots[sSlot].isForcedStop )
            {
                ease =  new cc.MoveTo( 0.15, cc.p( 0, e.getPositionY() + gap) ).easing( cc.easeBackOut() );
            }
            else
            {
                ease = new cc.MoveTo( 0.3, cc.p( 0, e.getPositionY() + gap ) ).easing( cc.easeBackOut() );
            }

            ease.setTag( MOVING_TAG );
            e.runAction(ease);
        }

        return spriteVec;
    },

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSWildX;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    spin : function()
    {
        this.unschedule(this.updateWinCoin);

        this.deductBet( this.currentBet );
        if (this.displayedScore != this.totalScore)
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;
        this.haveWild = false;
        this.wildActivated = false;

        this.currentWildIndx = -1;

        for (var i = 0; i < 3; i++)
        {
            this.wildIndx[i] = -1;
            this.wildElms[i] = null;
        }

        this.currentWin = 0;
        this.currentWinForcast = 0;
        this.startRolling();

        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    },

    startWildRolling : function()
    {
        this.delegate.jukeBox(S_WILD);
        this.isRolling = true;
        var i;
        var reelVec = this.slots[0].resultReel;

        for (i = 0; i < reelVec.length; i++)
        {
            if (this.wildIndx[i] != -1 && this.currentWildIndx < i)
            {
                var r = reelVec[i];

                var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                diff*=( 2 - i );

                for (var j = 0; j < r.length; j++)
                {
                    var s = r[j];
                    s.runAction( new cc.FadeOut( 0.2 ) );
                }
                this.slots[0].wildReel[i].setFadeIn();

                this.currentWildIndx = i;

                this.wildRotationCount = 0;

                this.timeElapsedWild = 0;

                this.isScheduled = true;

                this.schedule(this.rollUpdatorWild);

                var sprite = new cc.Sprite( glowStrings[3] );

                sprite.setTag(WILD_ANIM_TAG);

                var R = this.slots[0].wildReel[i];

                sprite.setPosition( R.getParent().getPositionX() + this.slots[0].getPositionX() - diff, R.getParent().getPositionY() + this.slots[0].getPositionY() );
                sprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.5, 150 ), new cc.FadeTo( 0.5, 255 ) ) ) );
                this.addChild(sprite, 5);

                this.scaleAccordingToSlotScreen( sprite );

                break;
            }
        }
    }

});