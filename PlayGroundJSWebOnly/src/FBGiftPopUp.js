/**
 * Created by RNF-Mac11 on 6/16/17.
 */


FBGiftPopUp = cc.Layer.extend({

    bgSprite : null,

    scroll : null,

    selectedFriendsCountLabel : null,

    friendsVector : new Array(),

    selectedFriendsVector : new Array(),

    buttonsVector : new Array(),

    giveRewardsForInviting : false,//for assigning rewards on just inviting more than 10 Poeple

    touchEvent : function( pSender, type )
    {
        var button = pSender;
        if( this.getLocalZOrder() === 0 )
        {
            return;
        }

        switch ( type )
        {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                switch ( button.getTag() )
                {
                    case CLOSE_BTN_TAG:
                        this.getParent().getChildByTag( 2 ).setLocalZOrder( 1 );
                        this.setLocalZOrder( 0 );
                        break;

                    case INVITE_BTN_TAG:
                        for( var i = 0; i < FacebookObj.appfriendList.length; i++ )
                        {
                            this.selectedFriendsVector[i] = FacebookObj.appfriendList[i];
                        }

                        /*for( var j = 0; j < this.buttonsVector.length; j++ )
                        {
                            var obj = this.buttonsVector[j];
                            obj.getChildByTag( TICK_IMAGE_TAG ).setEnabled( false );
                        }*/
                        this.sendInvitations();
                        break;

                    case GIFT_ALL_BTN_TAG:
                        var parent = this.getParent();
                        var grandParent = parent.getParent();
                        parent.getParent().showingScreen = false;
                        parent.removeFromParent( true );
                        var pop = new FBInvitePopUp();
                        grandParent.addChild(pop, 10, INVITE_FB_FRIENDS_LAYER_TAG);
                        DPUtils.getInstance().setTouchSwallowing(100, pop);
                        break;

                    default:
                        this.sendGift( button );
                        break;
                }

                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;

            default:
                break;
        }
    },

    editBoxEditingDidBegin : function( editBox )
    {
        editBox.setFont( "HELVETICALTSTD-BOLD", 30 );
    },
    editBoxEditingDidEnd : function( editBox )
    {
        editBox.setFont( "HELVETICALTSTD-BOLD", 30 );
    },
    editBoxTextChanged : function( editBox, text )
    {
        editBox.setFont( "HELVETICALTSTD-BOLD", 30 );
        this.setScrollViewAccordingToWord( text );
    },
    editBoxReturn : function( editBox )
    {
    },

    setScrollViewAccordingToWord : function( text )
    {
        //cc.log( "text to search = " + text );
        var height = ( this.buttonsVector.length + 1.5 ) * 30 * 1.2;

        var scollFrameSize = cc.size( this.bgSprite.getContentSize().width, this.bgSprite.getContentSize().height * 0.5 );

        if( height < scollFrameSize.height )
        {
            height = scollFrameSize.height;
        }

        var elmCount = 0;

        var isLeft = true;

        for ( var i = 0; i < this.buttonsVector.length; i++ )
        {
            var item = this.buttonsVector[i];

            var label = item.getChildByTag( NAME_LABEL_TAG );
            var userNameTxt = label.getString().toLowerCase();
            //cc.log( "userNameTxt to search = " + userNameTxt );
            if(text.length && userNameTxt.indexOf(text.toLowerCase()) == -1)
            {
                item.setVisible( false );
            }
            else
            {
                //cc.log( "userNameTxt.indexOf(text.toLowerCase()) = " + userNameTxt.indexOf(text.toLowerCase()) );
                item.setVisible( true );
                elmCount++;
            }
        }

        height = ( elmCount / 2 + 1.5 ) * new cc.Sprite( "#inviteTab.png" ).getContentSize().height * 1.2;

        if( height < scollFrameSize.height )
        {
            height = scollFrameSize.height;
        }

        this.scroll.setInnerContainerSize( cc.size( scollFrameSize.width, height ) );

        var pos = height - new cc.Sprite( "#inviteTab.png" ).getContentSize().height * 0.5;

        for ( var i = 0; i < this.buttonsVector.length; i++ )
        {
            var button =  this.buttonsVector[ i ];

            if ( button.isVisible() )
            {
                if ( isLeft )
                {
                    button.setPosition( cc.p( this.bgSprite.getContentSize().width / 2 - button.getContentSize().width * 0.55, pos ) );
                }
                else
                {
                    button.setPosition( cc.p( this.bgSprite.getContentSize().width / 2 + button.getContentSize().width * 0.55, pos ) );
                    pos-=button.getContentSize().height * 1.2;
                }
                isLeft = !isLeft;
            }
        }
    },

    create : function( )
    {},

    hitForFriends : function( haveNet )
    {},
    selectAllItem : null,
    makeScrollVIew : function( data )
    {
        this.scroll = new ccui.ScrollView();

        var scollFrameSize = cc.size( this.bgSprite.getContentSize().width, this.bgSprite.getContentSize().height * 0.45 );
        this.scroll.setDirection( ccui.ScrollView.DIR_VERTICAL );
        this.scroll.setContentSize( scollFrameSize );
        this.scroll.setScrollBarAutoHideEnabled( false )
        this.scroll.setScrollBarOpacity(255);
        this.scroll.setScrollBarColor( cc.color( 255, 255, 255, 255 ) );
        this.scroll.setBounceEnabled( true );
        this.scroll.setPosition( cc.p( 0, this.bgSprite.getContentSize().height * 0.2 ) );

        var height = ( data.length + 1.5 ) * new cc.Sprite( "#giftPop/bar.png" ).getContentSize().height;

        if( height < scollFrameSize.height )
        {
            height = scollFrameSize.height;
        }
        this.scroll.setInnerContainerSize( cc.size( scollFrameSize.width, height ) );

        this.bgSprite.addChild( this.scroll );

        //var obj;
        //var isLeft = true;
        var pos = height - new cc.Sprite( "#giftPop/bar.png" ).getContentSize().height * 0.5;
        var tag = 0;

        //for (var obj in data)
        //cc.log( "data.length = " + data.length );
        for( var ii = 0; ii < data.length; ii++)
        {
            var obj = data[ii];
            this.friendsVector.push( obj );
            if ( obj )
            {
                var sprite = new cc.Sprite( "#giftPop/bar.png" );

                sprite.setTag( tag );
                tag++;

                this.buttonsVector.push( sprite );

                sprite.setPosition( cc.p( this.bgSprite.getContentSize().width / 2, pos ) );
                pos-=sprite.getContentSize().height * 1.05;

                var first_name = obj[ "name" ];
                var heading = new CustomLabel( first_name, "GEORGIAB", 30, cc.size( 300, 20 ) );
                heading.setPosition( sprite.getContentSize().width * 0.4, sprite.getContentSize().height * 0.5 );
                //heading.setColor( cc.color( 255, 255, 0, 255 ) );
                heading.setTag( NAME_LABEL_TAG );
                sprite.addChild( heading, 2 );

                var url = "https://graph.facebook.com/" + obj["id"] + "/picture?width=55&height=55";

                this.addImageOnParent( url, sprite );

                var tickSprite = new ccui.Button( "giftPop/send_button.png", "giftPop/send_button.png", "", ccui.Widget.PLIST_TEXTURE );//new cc.Sprite( "#giftPop/send_button.png.png" );
                tickSprite.setPosition( cc.p( sprite.getContentSize().width * 0.85, sprite.getContentSize().height / 2 ) );
                sprite.addChild( tickSprite, 0, TICK_IMAGE_TAG );

                tickSprite.addTouchEventListener( this.touchEvent, this );
                this.scroll.addChild( sprite );
            }
        }

        this.selectedFriendsCountLabel = new cc.LabelTTF( this.selectedFriendsVector.length + "/" + data.length, "GEORGIAB", 40 );
        this.selectedFriendsCountLabel.setPosition( cc.p( this.bgSprite.getContentSize().width * 0.5, this.bgSprite.getContentSize().height * 0.7 ) );
        this.bgSprite.addChild( this.selectedFriendsCountLabel );

        this.selectedFriendsCountLabel.setVisible( false );


        var giftItem = new ccui.Button( "giftPop/gift_all.png", "giftPop/gift_all.png", "", ccui.Widget.PLIST_TEXTURE );
        giftItem.setPosition( this.bgSprite.getContentSize().width * 0.5, giftItem.getContentSize().height * 0.6 );
        giftItem.setTag( INVITE_BTN_TAG );
        giftItem.addTouchEventListener( this.touchEvent, this );
        this.bgSprite.addChild( giftItem );

        var inviteItem = new ccui.Button( "giftPop/invite_button.png", "giftPop/invite_button.png", "", ccui.Widget.PLIST_TEXTURE );
        inviteItem.setPosition( giftItem.getPositionX() + giftItem.getContentSize().width * 0.5 + inviteItem.getContentSize().width * 0.6, giftItem.getPositionY() );
        inviteItem.setTag( GIFT_ALL_BTN_TAG );//mingled with gift all button be careful

        inviteItem.addTouchEventListener( this.touchEvent, this );
        this.bgSprite.addChild( inviteItem );

        this.bgSprite.removeChildByTag( LOADER_TAG );

        var termText = new cc.LabelTTF( "   Only one gift\nper friend per day.", "arial", 20 );
         termText.setPosition( cc.p( this.bgSprite.getContentSize().width * 0.15, giftItem.getPositionY() ) );
        this.bgSprite.addChild( termText );
    },

    addImageOnParent: function (appIconURL,parentSprite){
        cc.loader.loadImg(appIconURL, {isCrossOrigin: true},function (err, texture) {
            var sprite = new cc.Sprite(texture);
            sprite.setPosition( cc.p( parentSprite.getContentSize().width * 0.11, parentSprite.getContentSize().height * 0.5 + 1.5 ) );
            parentSprite.addChild(sprite,10);
        },parentSprite);
    },

    makeEntriesOfSentGifts : function( data )
    {
        var giftsMap = UserDef.getInstance().getValueForKey( GIFTS_MAP, null );
        //cc.log( "object value = " + JSON.toString( giftsMap ) );
        if( giftsMap == null )
        {
            giftsMap = {};
        }

        var totalGifts = 0;
        for ( var i = 0; i < data.length; i++ )
        {
            var obj = "" + data[i];
            giftsMap[obj] = ServerData.getInstance().current_time_seconds;
            for( var j in FacebookObj.appFriendListForGift )
            {
                if( obj == FacebookObj.appFriendListForGift[j].id )
                {
                    for( var k = 0; k < this.buttonsVector.length; k++ )
                    {
                        var button = this.buttonsVector[k];
                        if( button.getChildByTag( NAME_LABEL_TAG ).getString() == FacebookObj.appFriendListForGift[j].name )
                            button.getChildByTag( TICK_IMAGE_TAG ).setEnabled( false );
                    }

                    FacebookObj.appFriendListForGift.splice( j, 1 );
                    //i--;
                    totalGifts++;
                }
            }
        }
        //var jsonVar = ServerCommunicator.getInstance().map_to_object( giftsMap );
        AppDelegate.getInstance().assignRewards( totalGifts * FB_GIFT_REWARDS_POINTS,true );

        UserDef.getInstance().setValueForKey( GIFTS_MAP, giftsMap );
        //ServerCommunicator.getInstance().SCUpdateData( );
    },

    sendGift : function( button )
    {
        var tmpVec = [];
        if( FacebookObj.appfriendList )
        {
            for( var i = 0; i < FacebookObj.appfriendList.length; i++ )
            {
                tmpVec[i] = FacebookObj.appfriendList[i];
            }
        }
        var index = 0;
        for( index = 0; index < tmpVec.length; index++ )
        {
            if( tmpVec[index].name === button.getParent().getChildByTag( NAME_LABEL_TAG ).getString() )
            {
                break;
            }
        }
        var obj = tmpVec[index];

        FacebookObj.sendGiftsToFewUsers(obj.id, "Free Coins For You", 1);
        //button.setEnabled( false );
    },

    sendInvitations : function()
    {
        var valueMapOfFriendIds = new Map();
        var ids = "";
        //cc.log( "this.selectedFriendsVector.length = " + this.selectedFriendsVector.length );
        if ( this.selectedFriendsVector.length )
        {
            var a = 0;
            for ( var i = 0; i < this.selectedFriendsVector.length; i++ )
            {
                //cc.log( "i = " + i );
                var sdata = this.selectedFriendsVector[i];
                var idString = sdata["id"];

                valueMapOfFriendIds.set( ("" + a ), idString );
                ids = ids + idString + ",";
                this.selectedFriendsVector.splice( i , 1 );
                i--;
                a++;
                if( a >= FB_MAX_REQUESTS - 1 )
                {
                    //cc.log( "FB_MAX_REQUESTS = " + a );
                    break;
                }
            }

            if( ServerData.getInstance().current_time_seconds >= UserDef.getInstance().getValueForKey( FB_REWARDS_ASSIGNMENT_TIME, 0 ) )
            {
                UserDef.getInstance().setValueForKey( FB_REWARDS_ASSIGNMENT_TIME, 0 );
            }

            //if (valueMapOfFriendIds.length > 0) {
            FacebookObj.sendGiftsToFewUsers(ids, "Free Coins For You", 1);
            //cc.log( "ids.length = " + ids.length );
            if( ids.length == 0 )
            {
                this.removeFromParent(true);
            }
            //}

        }
        else
        {
            //cc.log( "in here" );
            FacebookObj.requestedOnce = false;
            //this.removeFromParent( true );
            //ServerCommunicator.getInstance().SCUpdateData();
        }
    },

    ctor:function ( code ) {
        this._super( );

        var vSize = cc.winSize;

        this.bgSprite = new cc.Sprite( "#giftPop/bg.png" );
        this.bgSprite.setPosition( vSize.width / 2, vSize.height / 2 );
        //this.bgSprite.retain();
        this.addChild( this.bgSprite );

        var tabSprite = new cc.Sprite( "#giftPop/send_gifts.png" );
        tabSprite.setPosition( this.bgSprite.getContentSize().width / 2, this.bgSprite.getContentSize().height * 0.55 );
        this.bgSprite.addChild( tabSprite );

        var displayText = new cc.LabelTTF( "Get                 for sending gift to each of your friend.", "ARIAL", 25 );
        displayText.setPosition( cc.p( this.bgSprite.getContentSize().width * 0.5, this.bgSprite.getContentSize().height * 0.75 + 3 ) );
        this.bgSprite.addChild( displayText );

        var displayedText = new cc.LabelTTF( "   " + FB_GIFT_REWARDS_POINTS + " coins", "ARIAL", 25 );
        displayedText.setPosition( cc.p( cc.rectGetMinX( displayText.getBoundingBox() ) + new cc.LabelTTF( FB_GIFT_REWARDS_POINTS + "Get ", "ARIAL", 25 ).getContentSize().width,
            displayText.getPositionY() ) );
        this.bgSprite.addChild( displayedText );
        displayedText.setFontFillColor( new cc.Color( 255, 0, 0 ) );


        if( FacebookObj.appFriendListForGift && FacebookObj.appFriendListForGift.length )
        {
            this.makeScrollVIew( FacebookObj.appFriendListForGift );
        }
        else
        {
            var messageTest = new cc.LabelTTF( "\nYou have sent free gifts to all your\n \t \t \t \t \t \t \t \t friends today." +
                "\n\nInvite more friends or come back \n\t \t \t \t \t \t \t \t \t \t tomorrow.", "ARIAL", 40 );
            messageTest.setPosition( cc.p( this.bgSprite.getContentSize().width * 0.5, this.bgSprite.getContentSize().height * 0.5 ) );
            this.bgSprite.addChild( messageTest );

            var giftItem = new ccui.Button( "giftPop/gift_all.png", "giftPop/gift_all.png", "", ccui.Widget.PLIST_TEXTURE );
            giftItem.setPosition( this.bgSprite.getContentSize().width * 0.5, giftItem.getContentSize().height * 0.6 );

            var inviteItem = new ccui.Button( "giftPop/invite_button.png", "giftPop/invite_button.png", "", ccui.Widget.PLIST_TEXTURE );
            inviteItem.setPosition( giftItem.getPositionX() + giftItem.getContentSize().width * 0.5 + inviteItem.getContentSize().width * 0.6, giftItem.getPositionY() );
            inviteItem.setTag( GIFT_ALL_BTN_TAG );//mingled with gift all button be careful

            inviteItem.addTouchEventListener( this.touchEvent, this );
            this.bgSprite.addChild( inviteItem );
        }


        var headingSprite = new cc.Sprite( "#giftPop/send_gift.png" );
        headingSprite.setPosition( cc.p( headingSprite.getContentSize().width * 0.65, this.bgSprite.getContentSize().height - headingSprite.getContentSize().height * 1.08 ) );
        this.bgSprite.addChild( headingSprite );

        var closeItem = new ccui.Button( "giftPop/collect_gift.png", "giftPop/collect_gift.png", "", ccui.Widget.PLIST_TEXTURE );

        closeItem.setPosition( this.bgSprite.getContentSize().width * 0.56, headingSprite.getPositionY() );
        closeItem.setTag( CLOSE_BTN_TAG );

        closeItem.addTouchEventListener( this.touchEvent, this );
        //closeItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );

        this.bgSprite.addChild( closeItem );

        //this.bgSprite.release();

        return true;
    },

    handleScroll : function( event )
    {
        DPUtils.getInstance().scrolledAlready = true;

        var self = event.getCurrentTarget();

        if( !self.getChildByTag( 1 ).scroll )
        {
            return;
        }

        var nMoveY = event.getScrollY() * 0.5;

        nMoveY = -nMoveY;

        //cc.log( str );

        var inner = self.getChildByTag( 1 ).scroll.getInnerContainer();
        var curPos  = inner.getPosition();
        var nextPos = cc.p(curPos.x, curPos.y + nMoveY);

        // prevent scrolling past beginning
        if (nextPos.y > 0)
        {
            //inner.setPosition(cc.p(0, nextPos.y));
            self.getChildByTag( 1 ).scroll._startAutoScrollToDestination(cc.p(nextPos.x, 0), 0.5, true);
            return;
        }

        //auto ws = Director::getInstance()->getWinSize();
        var size = self.getChildByTag( 1 ).scroll.getContentSize();
        var innerSize = inner.getContentSize();
        var topMostY = size.height - innerSize.height;

        // prevent scroll past end
        if (nextPos.y < topMostY)
        {
            self.getChildByTag( 1 ).scroll._startAutoScrollToDestination(cc.p(nextPos.x, topMostY), 0.5, true);
            return;
        }

        self.getChildByTag( 1 ).scroll._startAutoScrollToDestination(nextPos, 0.5, true);
    }

});