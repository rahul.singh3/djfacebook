/**
 * Created by RNF-Mac11 on 1/23/17.
 */



var reelsRandomizationArrayDJSQuintuple5x =
    [
        [// 85% payout
            30, 70, 30, 90, 30, 30,
            25, 95, 20, 50, 20, 95,
            20, 90, 10, 3, 10, 5,
            8, 1, 8, 3, 10, 10,
            12, 3, 25, 45, 30, 25,
            28, 35, 30, 25, 10, 40
        ],
        [//92 %
            30, 70, 24, 90, 20, 30,

            20, 95, 20, 50, 20, 95,

            20, 90, 10, 4, 10, 5,

            8, 1, 8, 3, 10, 10,

            12, 3, 25, 45, 30, 25,

            28, 35, 30, 25, 10, 40
        ],
        [//97 %
            20, 95, 20, 100, 20, 25,
            20, 105, 20, 50, 20, 95,
            20, 90, 10, 4, 15, 5,
            10, 1, 8, 3, 10, 10,
            15, 5, 30, 45, 30, 25,
            30, 35, 30, 25, 30, 5
        ],
        [//140 %
            1, 70, 1, 130, 1, 60,
            1, 130, 1, 50, 1, 100,
            1, 100, 20, 5, 25, 5,
            40, 2, 30, 3, 1, 10,
            25, 2, 1, 55, 1, 30,
            1, 45, 1, 40, 1, 15
        ],
        [//300 %
            1, 70, 1, 130, 1, 60,
            1, 130, 1, 50, 1, 100,
            1, 100, 18, 8, 18, 8,
            25, 5, 15, 7, 1, 10,
            12, 1, 1, 75, 1, 30,
            1, 50, 1, 50, 1, 45
        ]
    ];

var DJSQuintuple5x = GameScene.extend({
     reelsArrayDJSQuintuple5x :[],

    ctor:function ( level_index ) {

         this.reelsArrayDJSQuintuple5x = [
            /*1*/ELEMENT_EMPTY,/*2*/ELEMENT_7_BAR,/*3*/ELEMENT_EMPTY,/*4*/ELEMENT_7_BLUE,/*5*/ELEMENT_EMPTY,
            /*6*/ELEMENT_7_PINK,/*7*/ELEMENT_EMPTY,/*8*/ELEMENT_7_RED,/*9*/ELEMENT_EMPTY,/*10*/ELEMENT_BAR_1,
            /*11*/ELEMENT_EMPTY,/*12*/ELEMENT_BAR_2,/*13*/ELEMENT_EMPTY,/*14*/ELEMENT_BAR_3,/*15*/ELEMENT_EMPTY,
            /*16*/ELEMENT_2X,/*17*/ELEMENT_EMPTY,/*18*/ELEMENT_3X,/*19*/ELEMENT_EMPTY,/*20*/ELEMENT_10X,
            /*21*/ELEMENT_EMPTY,/*22*/ELEMENT_5X,/*23*/ELEMENT_EMPTY,/*24*/ELEMENT_BAR_1,/*25*/ELEMENT_EMPTY,
            /*26*/ELEMENT_2X,/*27*/ELEMENT_EMPTY,/*28*/ELEMENT_BAR_2,/*29*/ELEMENT_EMPTY,/*30*/ELEMENT_7_BLUE,
            /*31*/ELEMENT_EMPTY,/*32*/ELEMENT_BAR_3,/*33*/ELEMENT_EMPTY,/*34*/ELEMENT_7_BAR,/*35*/ELEMENT_EMPTY,
            /*36*/ELEMENT_BAR_1
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSQuintuple5x;
        this.physicalArrayCheck= this.reelsArrayDJSQuintuple5x;
        this._super( level_index );

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;


    },

    calculatePayment : function( doAnimate )
    {
        var payMentArray = [
            /*0*/    2000, // 10x 10x 10x

            /*1*/     500, // 5x 5x 5x

            /*2*/     300, // 2x 3x 2x

            /*3*/     25,  // 7 7 7 Red

            /*4*/     20,  // 7 7 7 Yellow

            /*5*/     15,  // 7 7 7 Green

            /*6*/     12,  // 7Bar 7Bar 7Bar

            /*7*/     10,   // 3Bar 3Bar 3Bar

            /*8*/     7,   // 2Bar 2Bar 2Bar

            /*9*/     5,   // Bar Bar Bar

            /*10*/    5,   // Any 7 Combo

            /*11*/    3   // Any Bar Combo
        ];

        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        for ( var i = 0; i < this.slots[0].resultReel.length; i++ )
        {
            tmpVector = this.slots[0].resultReel[ i ];
            tmpVector2.push( tmpVector[ 1 ] );
        }

        this.lineVector.push( tmpVector2 );

        var pay = 0;
        var tmpPay = 1;
        var lineLength = 3;
        var count2X = 0;
        var count3X = 0;
        var count10X = 0;
        var count5X = 0;
        var countP = 0;
        var countWild = 0;
        var countGeneral = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var i, j, k;
        var e = null;
        var pivotElm = null;
        var pivotElm2 = null;
        var tmpVec;

        for ( i = 0; i < this.totalLines; i++ )
        {
            tmpVec = this.lineVector[ i ];
            for ( k = 0; k < 11; k++ )
            {
                var breakLoop = false;
                countGeneral = 0;
                switch ( k )
                {
                    case 0://for all combination with wild
                        for ( j = 0; j < lineLength; j++ )
                        {
                            if ( this.resultArray[0][j] != -1 )
                            {
                                switch ( this.reelsArrayDJSQuintuple5x[this.resultArray[i][j]] )
                                {
                                    case ELEMENT_2X:
                                        count2X++;
                                        tmpPay*=2;
                                        countWild++;
                                        if ( doAnimate ) {
                                            e = tmpVec[ j ];
                                        }

                                        if ( e )
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_3X:
                                        count3X++;
                                        tmpPay*=3;
                                        countWild++;

                                        if ( doAnimate ) {
                                            e = tmpVec[ j ];
                                        }
                                        if ( e )
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_10X:
                                        count10X++;
                                        tmpPay*=10;
                                        countWild++;
                                        if ( doAnimate ) {
                                            e = tmpVec[ j ];
                                        }
                                        if ( e )
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_5X:
                                        count5X++;
                                        tmpPay*=5;
                                        countWild++;
                                        if ( doAnimate ) {
                                            e = tmpVec[ j ];
                                        }
                                        if ( e )
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        switch ( pivotElement )
                                        {
                                            case -1:
                                                pivotElement = this.reelsArrayDJSQuintuple5x[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate ) {
                                                    e = tmpVec[ j ];
                                                }
                                                if ( e )
                                                {
                                                    pivotElm = e;
                                                }
                                                break;

                                            default:
                                                pivotElement2 = this.reelsArrayDJSQuintuple5x[this.resultArray[i][j]];
                                                countGeneral++;

                                                if ( doAnimate ) {
                                                    e = tmpVec[ j ];
                                                }
                                                if ( e )
                                                {
                                                    pivotElm2 = e;
                                                }
                                                break;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }

                        if ( j == 3 )
                        {
                            if ( count10X == 3 )
                            {
                                pay = payMentArray[0];
                                breakLoop = true;
                            }
                            else if ( count5X == 3 )
                            {
                                pay = payMentArray[1];
                                breakLoop = true;
                            }
                            else if ( count2X == 2 && count3X == 1 )
                            {
                                pay = payMentArray[2];
                                breakLoop = true;
                            }
                            else if( countWild >= 2 )
                            {
                                breakLoop = true;

                                switch ( pivotElement )
                                {
                                    case ELEMENT_7_RED:
                                        pay = tmpPay * payMentArray[3];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_7_PINK:
                                        pay = tmpPay * payMentArray[4];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_7_BLUE:
                                        pay = tmpPay * payMentArray[5];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_7_BAR:
                                        pay = tmpPay * payMentArray[6];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_3:
                                        pay = tmpPay * payMentArray[7];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_2:
                                        pay = tmpPay * payMentArray[8];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_1:
                                        pay = tmpPay * payMentArray[9];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        pay = tmpPay;
                                        if ( pivotElm )
                                        {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;
                                }
                            }
                            else if( countWild == 1 )
                            {
                                breakLoop = true;

                                if ( pivotElement == pivotElement2 )
                                {
                                    switch ( pivotElement )
                                    {
                                        case ELEMENT_7_RED:
                                            pay = tmpPay * payMentArray[3];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }

                                            break;

                                        case ELEMENT_7_PINK:
                                            pay = tmpPay * payMentArray[4];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_7_BLUE:
                                            pay = tmpPay * payMentArray[5];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_7_BAR:
                                            pay = tmpPay * payMentArray[6];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_3:
                                            pay = tmpPay * payMentArray[7];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_2:
                                            pay = tmpPay * payMentArray[8];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_1:
                                            pay = tmpPay * payMentArray[9];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        default:
                                            pay = tmpPay;
                                            break;
                                    }
                                }
                                else
                                {
                                    if ( ( pivotElement == ELEMENT_7_BAR || pivotElement == ELEMENT_7_PINK ) &&
                                        ( pivotElement2 == ELEMENT_7_BAR || pivotElement2 == ELEMENT_7_PINK ) )
                                    {
                                        pay = tmpPay * payMentArray[4];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if ( pivotElement >= ELEMENT_7_BAR && pivotElement <= ELEMENT_7_RED &&
                                        pivotElement2 >= ELEMENT_7_BAR && pivotElement2 <= ELEMENT_7_RED )
                                    {
                                        pay = tmpPay * payMentArray[10];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if ( ( pivotElement == ELEMENT_7_BAR || pivotElement == ELEMENT_BAR_1 ) &&
                                        ( pivotElement2 == ELEMENT_7_BAR || pivotElement2 == ELEMENT_BAR_1 ) )
                                    {
                                        pay = tmpPay * payMentArray[9];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if ( ( ( pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_BAR_3 ) || pivotElement == ELEMENT_7_BAR ) &&
                                        ( ( pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_BAR_3 ) || pivotElement2 == ELEMENT_7_BAR ) )
                                    {
                                        pay = tmpPay * payMentArray[11];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else
                                    {
                                        pay = tmpPay;
                                    }
                                }
                            }
                        }
                        break;

                    case 1: // 7 7 7 red
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSQuintuple5x[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[3];
                            breakLoop = true;

                            for ( var l = 0; l < countGeneral; l++ ) {

                            if ( doAnimate && tmpVec ) {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 2: // 7 7 7 white
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSQuintuple5x[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_PINK:
                                    countP++;
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 && countP )
                        {
                            pay = payMentArray[4];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                            if ( doAnimate && tmpVec ) {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 3: // 7 7 7 Blue
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSQuintuple5x[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_BLUE:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[5];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                            if ( doAnimate && tmpVec ) {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 4: // 7 7 7 bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSQuintuple5x[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[6];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                            if ( doAnimate && tmpVec ) {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 5: // 3Bar 3Bar 3Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSQuintuple5x[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[7];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                            if ( doAnimate && tmpVec ) {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 6: // 2Bar 2Bar 2Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSQuintuple5x[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[8];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                            if ( doAnimate && tmpVec ) {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 7: // Bar Bar Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSQuintuple5x[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[9];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                            if ( doAnimate && tmpVec ) {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 8: // any 7 combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSQuintuple5x[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_BAR:
                                case ELEMENT_7_BLUE:
                                case ELEMENT_7_PINK:
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[10];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {
                            if ( doAnimate && tmpVec ) {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 9: // any Bar combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSQuintuple5x[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[11];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( doAnimate && tmpVec ) {
                                    e = tmpVec[ l ];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

                if ( breakLoop )
                {
                    break;
                }
            }
        }



        if( doAnimate )
        {
            this.currentWin = this.currentBet * pay;
            if ( this.currentWin > 0 )
            {
                this.addWin = this.currentWin / 10;
                this.setCurrentWinAnimation( 0 );
            }

            this.checkAndSetAPopUp();
        }
        else
        {
            this.currentWinForcast = this.currentBet * pay;
        }
    },
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSQuintuple5x[i]=this.physicalReelElements[idx];
                i++;
            }
        }
    },
    changeTotalProbabilities : function()
    {
        this.totalProbalities = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            //this.extractArrayFromSring();
            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[this.easyModeCode][i];
            }
        }
        else
        {
            for ( var i = 0; i < 36; i++ )
            {
                this.probabilityArray[this.easyModeCode][i] = reelsRandomizationArrayDJSQuintuple5x[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSQuintuple5x[this.easyModeCode][i];
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
        if( ( this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] == ELEMENT_7_RED && this.reelsArrayDJSQuintuple5x[this.resultArray[0][1]] == ELEMENT_7_RED )  ||
            ( this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] == ELEMENT_7_BLUE && this.reelsArrayDJSQuintuple5x[this.resultArray[0][1]] == ELEMENT_7_BLUE ) ||
            ( this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] == ELEMENT_7_PINK && this.reelsArrayDJSQuintuple5x[this.resultArray[0][1]] == ELEMENT_7_PINK ) ||
            ( ( this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] <= ELEMENT_10X ) &&
            ( this.reelsArrayDJSQuintuple5x[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSQuintuple5x[this.resultArray[0][1]] <= ELEMENT_10X ) ) ||
            ( this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] == ELEMENT_7_RED && ((this.reelsArrayDJSQuintuple5x[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSQuintuple5x[this.resultArray[0][1]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSQuintuple5x[this.resultArray[0][1]] == ELEMENT_7_RED && ((this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] == ELEMENT_7_BLUE && ((this.reelsArrayDJSQuintuple5x[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSQuintuple5x[this.resultArray[0][1]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSQuintuple5x[this.resultArray[0][1]] == ELEMENT_7_BLUE && ((this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] == ELEMENT_7_PINK && ((this.reelsArrayDJSQuintuple5x[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSQuintuple5x[this.resultArray[0][1]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSQuintuple5x[this.resultArray[0][1]] == ELEMENT_7_PINK && ((this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] == ELEMENT_7_BAR && ((this.reelsArrayDJSQuintuple5x[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSQuintuple5x[this.resultArray[0][1]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSQuintuple5x[this.resultArray[0][1]] == ELEMENT_7_BAR && ((this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] == ELEMENT_BAR_3 && ((this.reelsArrayDJSQuintuple5x[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSQuintuple5x[this.resultArray[0][1]] <= ELEMENT_10X ) ) ) ||
            (this. reelsArrayDJSQuintuple5x[this.resultArray[0][1]] == ELEMENT_BAR_3 && ((this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSQuintuple5x[this.resultArray[0][0]] <= ELEMENT_10X ) ) ) )
        {
            if ( !this.slots[0].isForcedStop )
            {
                this.schedule( this.reelStopper, 4.8 );

                this.delegate.jukeBox( S_EXCITEMENT );

                var glowSprite = new cc.Sprite( glowStrings[0] );

                if ( glowSprite )
                {
                    var r = this.slots[0].physicalReels[ this.slots[0].physicalReels.length - 1 ];

                    glowSprite.setPosition( r.getParent().getPosition().x + this.slots[0].getPosition().x, r.getParent().getPosition().y + this.slots[0].getPosition().y );

                    //this.scaleAccordingToSlotScreen( glowSprite );

                    this.addChild( glowSprite, 5 );
                    this.animNodeVec.push( glowSprite );

                    glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ) ) );
                }
            }
        }
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
        var tmp = this.slots[0].resultReel[laneIndex];

        var e = tmp[1];

        switch ( e.eCode )
        {
            case ELEMENT_7_RED:
                if ( laneIndex != 0 )
                {
                    break;
                }
            case ELEMENT_2X:
            case ELEMENT_3X:
            case ELEMENT_4X:
            case ELEMENT_5X:
                e.runAction( new cc.Repeat( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ), 2 ) );
                this.delegate.jukeBox( S_FLASH );
                break;

            default:
                break;
        }

    },

    generateResult : function( scrIndx )
    {
        for ( var i = 0; i < this.slots[scrIndx].displayedReels.length; i++ )
        {
            //generate:
            var num = parseInt( this.prevReelIndex[i] );

            var tmpnum = 0;

            while ( num == parseInt( this.prevReelIndex[i] ) )
            {
                num = Math.floor( Math.random() * this.totalProbalities );

            }
            this.prevReelIndex[i] = num;
            this.resultArray[0][i] = num;

            for (var j = 0; j < 36; j++)
            {
                tmpnum+=this.probabilityArray[this.easyModeCode][j];

                if (tmpnum >= this.resultArray[0][i])
                {
                    this.resultArray[0][i] = j;
                    break;
                }
            }
            //console.log( " rand =  " + num + " index = " + this.resultArray[0][i] );
        }

        /*cc.log( "this.resultArray[0][0] = " + this.resultArray[0][0] );
        cc.log( "this.resultArray[0][1] = " + this.resultArray[0][1] );
        cc.log( "this.resultArray[0][2] = " + this.resultArray[0][2] );*/

        /*
         int highPay[][3] = {
         {15, 21, 15}, //2x 5x 2x
         {15, 19, 15}, //2x 4x 2x
         {15, 17, 15}, //2x 3x 2x
         {7, 7, 7},    //Triple 7 Red
         {5, 5, 5},    //Triple 7 Yellow
         {29, 29, 29}, //Triple 7 Green
         {1, 1, 1},    //Triple 7 Bar
         {31, 31, 31}, //Triple 3Bar
         {27, 27, 27}, //Triple 2Bar
         {23, 23, 23}, //Triple Bar
         };*/
        //HACK
        /*this.resultArray[0][0] = 11;
        this.resultArray[0][1] = 11;
        this.resultArray[0][2] = 11;
        //*///HACK
        //if( hackBuild )
        /*{
            if( this.tmpp == 0 )
            {
                this.resultArray[0][0] = 21;
                this.resultArray[0][1] = 21;
                this.resultArray[0][2] = 21;
            }
            else if( this.tmpp == 1 )
            {
                this.resultArray[0][0] = 7;
                this.resultArray[0][1] = 7;
                this.resultArray[0][2] = 15;
            }
            else if( this.tmpp == 2 )
            {
                this.resultArray[0][0] = 7;
                this.resultArray[0][1] = 7;
                this.resultArray[0][2] = 7;
            }
            else if( this.tmpp == 3 )
            {
                this.resultArray[0][0] = 19;
                this.resultArray[0][1] = 0;
                this.resultArray[0][2] = 0;
            }
            else if( this.tmpp == 4 )
            {
                this.resultArray[0][0] = 11;
                this.resultArray[0][1] = 11;
                this.resultArray[0][2] = 11;
                this.tmpp = -1;
            }
            this.tmpp++;
        }*/

        this.calculatePayment( false );
        this.checkForFalseWin();
        if( this.applyStrategy )
        {
            if( this.currentBet <= 100 )
            {
                this.applyStrategy = false;
                CSUserdefauts.getInstance().setValueForKey( STRATEGY_ASSIGNED, true );
                var highPay = [
                [15, 19, 21],//2x 10x 5x
                [31, 19, 31],//3Bar 10x 3Bar
                [7, 15, 15]//7Red 2x 2x
            ];

                var missedIndx = Math.floor( Math.random() * 3 );

                for ( var i = 0; i < 3; i++ )
                {
                    this.resultArray[0][i] = highPay[missedIndx][i];
                }
            }
            else if( this.currentBet <= 1000 )
            {
                this.applyStrategy = false;
                CSUserdefauts.getInstance().setValueForKey( STRATEGY_ASSIGNED, true );
                var highPay = [
                [3, 3, 21],//7Green 7Green 5x
                [5, 5, 17],//7White 7Whits 3x
                [7, 7, 15]//7Red 7Red 2x
            ];

                var missedIndx = Math.floor( Math.random() * 3 );

                for ( var i = 0; i < 3; i++ )
                {
                    this.resultArray[0][i] = highPay[missedIndx][i];
                }
            }
        }
    },
    tmpp : 0,
    rollUpdator : function(dt)
    {
        var i;
        for (i = 0; i < this.totalSlots; i++)
        {
            this.slots[i].updateSlot(dt);
        }

        for (i = 0; i < this.totalSlots; i++)
        {
            if ( this.slots[i].isSlotRunning() )
            {
                break;
            }
        }

        if (i >= this.totalSlots)
        {
            this.isRolling = false;
            this.unschedule( this.rollUpdator );
            this.calculatePayment( true );

            if (this.isAutoSpinning)
            {
                if (this.currentWin > 0)
                {
                    this.scheduleOnce( this.autoSpinCB, 1.5);
                }
                else
                {
                    this.scheduleOnce( this.autoSpinCB, 1.0);
                }
            }
        }
    },

    setFalseWin : function()
    {
        var highWinIndices = [ 3, 5, 7 ];
        var missIndices = [ 3, 5, 7, 15, 17, 19, 21 ];

        var missedIndx = Math.floor( Math.random() * 3 );

        if( missedIndx == 2 )
        {
            if( Math.floor( Math.random() * 10 ) == 3 )
            {
            }
            else
            {
                missedIndx = Math.floor( Math.random() * 2 );
            }
        }

        var sameIndex = Math.floor( Math.random( ) * highWinIndices.length );

        for ( var i = 0; i < 3; i++ )
        {
            if ( i == missedIndx )
            {
                this.resultArray[0][i] = this.getRandomIndex( missIndices[ Math.floor( Math.random( ) * missIndices.length ) ], true );
            }
            else
            {
                this.resultArray[0][i] = this.getRandomIndex( highWinIndices[ sameIndex ], false );
            }
        }
    },

    setResult : function( indx, lane, sSlot )
    {
        //cc.log( "indx = " + indx + ", lane = " + lane + ", sSlot = " + sSlot );
        var gap = 387.0;

        var j = 0;
        var animOriginY;

        var r = this.slots[0].physicalReels[lane];
        var arrSize = r.elementsVec.length;
        var spr;

        var spriteVec = [];

        animOriginY = gap * r.direction;
        spr = r.elementsVec[indx];
        if ( spr.eCode === ELEMENT_EMPTY )
        {
            gap = 285 * spr.getScale();
        }

        if ( indx == 0 )
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push(spr);

            spr.setPositionY( animOriginY );

            for (var i = 0; i < 2; i++)
            {
                spr = r.elementsVec[i];
                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (i + 1) * r.direction);
            }
        }
        else if (indx == arrSize - 1)
        {
            spr = r.elementsVec[0];
            spriteVec.push(spr);

            spr.setPositionY(animOriginY);
            for (var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i--)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction );
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push(spr);
            spr.setPositionY(animOriginY);

            for (var i = indx; i < indx + 2; i++)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }

        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            gap = -773.5;

            var eee;
            if (i < 2)
            {
                eee = spriteVec[i + 1];
            }
            if (i == 0)
            {
                var ee = spriteVec[i + 1];
                if ( Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) ) != 387)
                {
                    gap = -Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) * 2 * ee.getScale() );
                }
            }

            gap*=r.direction;
            var ease;

            if ( this.slots[sSlot].isForcedStop )
            {
                ease =  new cc.MoveTo( 0.15, cc.p( 0, e.getPositionY() + gap) ).easing( cc.easeBackOut() );
            }
            else
            {
                ease = new cc.MoveTo( 0.3, cc.p( 0, e.getPositionY() + gap ) ).easing( cc.easeBackOut() );
            }

            ease.setTag( MOVING_TAG );
            e.runAction(ease);
        }

        return spriteVec;
    },

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSQuintuple5x;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    spin : function()
    {

        this.unschedule( this.updateWinCoin );

        this.deductBet( this.currentBet );
        if (this.displayedScore != this.totalScore)
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;
        this.currentWin = 0;
        this.currentWinForcast = 0;
        this.startRolling();

        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    }

});


/*var DJSQ = cc.Scene.extend( {
 onEnter:function () {
 this._super();
 var layer = new DJSQuintuple5x( lvl );
 this.addChild(layer);
 }
 });*/