/**
 * Created by RNF-Mac11 on 1/13/17.
 */

var DJSDeal  = cc.Layer.extend({



    ctor:function ( ) {
        this._super( );
        return true;
        //}
    },
    /*getInstance : function () {
      return AppDelegate.getInstance().dealInstance;
    },*/

    canSetDeal : false,
    def : UserDef.getInstance(),
    checkAndSetBuyersDeal : function () {
        this.canSetDeal = this.getDealStatus();
    },
    offerSessionCount : 0,
    offerFrequency : 1,
    offerSprite : null,
    pDealTexture:null,
    dealOnScreen : false,
    flushDeal : function ( forceFlush ) {
        this.def = UserDef.getInstance();
        var dealFlushed = false;
        if ( this.offerSessionCount < this.offerFrequency || forceFlush )
        {
            if ( ! this.offerSprite )
            {
                var d = this.def.getValueForKey( DEAL_BACKGROUND, 0 );

                if ( this.pDealTexture )
                {
                    this.offerSprite = new cc.Sprite();
                    this.offerSprite.setTexture(this.pDealTexture);
                   // this.offerSprite.setScale(0.6);
                    /*var img = new Image;
                     if ( img.initWithImageData( d.getBytes(), d.getSize() ) == false )
                     {
                     return dealFlushed;
                     }


                     auto textureCache = cocos2d::Director::getInstance().getTextureCache();

                     auto previousTexture = textureCache.getTextureForKey( "img" );
                     if (previousTexture)
                     {
                     textureCache.removeTextureForKey( "img" );
                     }

                     // Add the new image into the Texture cache using the URL as the key
                     cocos2d::Texture2D* texture = textureCache.addImage( img, "img" );
                     if ( texture )
                     {
                     textureVec.pushBack( texture );
                     }
                     img.release();

                     offerSprite = Sprite::createWithTexture( texture );*/
                }
                else
                {
                    if ( this.def.getValueForKey( IS_BUYER, false ) )
                    {
                        this.offerSprite = new cc.Sprite( res.Deal_Default_Bg_Buyer_png );
                    }
                    else
                    {
                        this.offerSprite = new cc.Sprite( res.Deal_Default_Bg_NonBuyer_png );
                    }
                }

                var visibleSize = cc.winSize;
                var darkBG = new cc.Sprite( res.Dark_BG_png );
                darkBG.setScale( 2 );
                darkBG.setOpacity( 169 );
                darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
                darkBG.setTag( 2002 );
                this.addChild( darkBG );


                this.offerSprite.setPosition( cc.p( visibleSize.width / 2, visibleSize.height / 2 ) );
                this.addChild( this.offerSprite );

                this.schedule( this.updator );


                var closeItem = new cc.MenuItemImage( res.Close_Button_2_png, res.Close_Button_2_png, this.menuCB, this );

                closeItem.setPosition( cc.p( this.offerSprite.getContentSize().width - 20 - closeItem.getContentSize().width / 2, this.offerSprite.getContentSize().height - closeItem.getContentSize().width *0.8 ) );

                closeItem.setTag( CLOSE_BTN_TAG );

                closeItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );

                var buyItem = new cc.MenuItemImage( res.deal_buy_btn_png, res.deal_buy_btn_png, this.menuCB, this );

                buyItem.setPosition( cc.p( this.offerSprite.getContentSize().width*0.52,  buyItem.getContentSize().height*1.5 ) );

                buyItem.setTag( BUY_BTN_TAG );

                buyItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );

                var tempTimerLabel = new cc.LabelTTF( "00:00:00", "fonts/CarterOne.ttf", 35 );
                this.timerLabel = new cc.LabelTTF( "00:00:00", "fonts/CarterOne.ttf", 35, tempTimerLabel.getContentSize() );

                var ypos = 2.13;
                if ( ServerData.getInstance().BROWSER_TYPE == SAFARI){
                    ypos = 2.2;
                }

                this.timerLabel.setPosition( cc.p( this.offerSprite.getContentSize().width*0.56, buyItem.getContentSize().height*ypos) );
                this.offerSprite.addChild( this.timerLabel );


                var menu = new cc.Menu();
                menu.addChild( closeItem );
                menu.addChild( buyItem );
                menu.setPosition( cc.p( 0, 0 ) );
                this.offerSprite.addChild( menu );

                //DJSResizer.getInstance().setFinalScaling( this.offerSprite );
                DPUtils.getInstance().easyBackOutAnimToPopup(this.offerSprite,0.3);
                AppDelegate.getInstance().dealFlushed = true;

                /*
                  if ( this.getPosition() != cc.p( 0, 0 ) )
                  {
                      var time = 0.2;
                      this.runAction( new cc.ScaleTo( time, 1 ) );
                      this.offerSprite.runAction( new cc.FadeTo( time, 255 ) );
                      this.runAction( new cc.Sequence( new cc.MoveTo( time, cc.p( 0, 0 ) ), cc.callFunc( function( ) {
                          this.setOpacity( 160 );
                      }, this ) ) );
                  }*/
            }
            else
            {
                AppDelegate.getInstance().dealFlushed = true;
                DPUtils.getInstance().easyBackOutAnimToPopup(this.offerSprite,0.3);

/* var time = 0.2;
 this.runAction( new cc.ScaleTo( time, 1 ) );
 this.offerSprite.runAction( new cc.FadeTo( time, 255 ) );
 this.runAction( new cc.Sequence( new cc.MoveTo( time, cc.p( 0, 0 ) ), cc.callFunc( function( ){
     this.setOpacity( 160 );
 }, this ) ) );*/
}

this.unschedule( this.updator );
this.schedule( this.updator );


if ( !forceFlush )
{
 this.offerSessionCount++;
}

dealFlushed = true;
}
this.dealOnScreen = dealFlushed;
return dealFlushed;
},
getDealStatus : function ( ) {
var str = null;
var totalTime = userDataInfo.getInstance().deal_remaining_seconds;
this.def = UserDef.getInstance();
if (totalTime>0 && totalTime <=SECONDS_IN_AN_HOUR/2){
str = this.getTimeString( totalTime );
}
if ( totalTime == 0 ){
str = null;
userDataInfo.getInstance().deal_remaining_seconds = 0;
}

return str;
},
getTimeString : function( totalTime )
{
var hour = totalTime / 3600;
totalTime%=3600;
var min = totalTime / 60;
var sec = totalTime % 60;

var secStr = null;
var minStr = null;
var hourStr = null;

parseInt(sec) < 10 ? secStr = "0" + parseInt(sec) : secStr = "" + parseInt(sec);


parseInt(min) < 10 ? minStr = "0" + parseInt(min) : minStr = "" + parseInt(min);


hour < 10 ? hourStr = "0" + hour : hourStr = "" + hour;

var str = "" + minStr + ":" +  secStr;

return str;
},

getPriceString : function()
{
this.def = UserDef.getInstance();
var pId = this.def.getValueForKey( DEAL_PRODUCT_ID, 0 );

switch ( pId )
{
case 0:
 return "For $ 0.99";
 break;

case 1:
 return "For $ 4.99";
 break;

case 2:
 return "For $ 9.99";
 break;

case 3:
 return "For $ 19.99";
 break;

case 4:
 return "For $ 49.99";
 break;

case 5:
 return "For $ 99.99";
 break;

default:
 return "For $ 0.99";
 break;
}
},
menuCB : function( sender )
{
var itm = sender;
var delegate = AppDelegate.getInstance();

var scene = cc.director.getRunningScene();

var layer = scene.getChildByTag( sceneTags.LOBBY_SCENE_TAG );

switch ( itm.getTag() )
{
case CLOSE_BTN_TAG: {
    delegate.dealHiding(false);
    if (layer) {
        lobby = layer;
        lobby.showingScreen = false;
        lobby.isDealLaunch = false;
        delegate.showingScreen = false;
        PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_TOUCH_DEAL_POP);

    } else {
        DPUtils.getInstance().checkAndSetInvitePopUp();
    }

}
 break;

case BUY_BTN_TAG:
 //if ( this.getParent() && this.getParent().getParent() && !this.getParent().getParent().getChildByTag( LOADER_TAG ) )
{
 var pId = parseInt( this.def.getValueForKey( DEAL_PRODUCT_ID, "0" ) );

 IAPHelper.getInstance().purchase( pId+purchaseTags.PURCHASE_0_99 , BuyPageTags.DEAL_OFFER );//todo
}
 break;
}

delegate.jukeBox( S_BTN_CLICK );
},
updator : function( dt )
{
var string = this.getDealStatus();

if ( string )
{
this.timerLabel.setString( string );
}
else
{
this.resetDealData();
}
},
resetDealData : function()
{
var delegate = AppDelegate.getInstance();
if ( !this.dealOnScreen )
{
userDataInfo.getInstance().deal_remaining_seconds = 0; // disscussion with zikar update time for deal
this.canSetDeal = false;
this.offerSessionCount = 0;
this.unschedule( this.updator );
}
delegate.dealHiding(true);

},
setDealData : function( offerCode )
{
var delegate = AppDelegate.getInstance();
var serverDataObj = ServerData.getInstance();
var userdataObject = userDataInfo.getInstance();
    console.log("deal_reflush_remaining_seconds "+userdataObject.deal_reflush_remaining_seconds);
if (offerCode===5){
if ( !this.def.getValueForKey( IS_BUYER, false ) && userdataObject.deal_remaining_seconds<=0){
 userdataObject.deal_remaining_seconds = SECONDS_IN_AN_HOUR / 2;
 userdataObject.deal_reflush_remaining_seconds = -1;
 this.canSetDeal = this.getDealStatus();
 this.offerSessionCount = 0;
 serverDataObj.updateDealActive(userdataObject.deal_remaining_seconds,this.def.getValueForKey( DEAL_PRODUCT_ID, 0 ),this.def.getValueForKey( DEAL_REWARD_CHIPS, NON_BUYER_DEFAULT_CHIPS),1); //DEAL_REWARD_CHIPS
}else if(this.def.getValueForKey( IS_BUYER, false ) && userdataObject.deal_reflush_remaining_seconds > 0 && userdataObject.deal_remaining_seconds <= 0) {
 userdataObject.deal_remaining_seconds = SECONDS_IN_AN_HOUR / 2;
 userdataObject.deal_reflush_remaining_seconds = -1;
 this.canSetDeal = this.getDealStatus();
 serverDataObj.updateDealActive(userdataObject.deal_remaining_seconds, this.def.getValueForKey(DEAL_PRODUCT_ID, 0), this.def.getValueForKey(DEAL_REWARD_CHIPS, BUYER_DEFAULT_CHIPS),1); //DEAL_REWARD_CHIPS
 delegate.dealFlushed = false;
 this.offerSessionCount = 0;
}else if(this.def.getValueForKey( IS_BUYER, false ) && userdataObject.deal_reflush_remaining_seconds === -1 && userdataObject.deal_remaining_seconds > 0) {


}else{
 delegate.dealFlushed= true;
}
return;
}
if ( ! this.offerSprite )
{
if(this.def.getValueForKey( IS_BUYER, false )){
 var productId = serverDataObj.server_deal_buyer_image.length>1 ? serverDataObj.server_deal_buyer_product : BUYER_DEFAULT_PRODUCT_ID -purchaseTags.PURCHASE_0_99;
 var reward_chips = serverDataObj.server_deal_buyer_image.length>1? serverDataObj.server_deal_buyer_coins : BUYER_DEFAULT_CHIPS ;
 this.def.setValueForKey( DEAL_PRODUCT_ID, productId );
 this.def.setValueForKey( DEAL_REWARD_CHIPS, reward_chips );
 this.callBGImage( serverDataObj.server_deal_buyer_image);
}else{
 var productId = serverDataObj.server_deal_nonbuyer_product.length>1 ? serverDataObj.server_deal_nonbuyer_product : NON_BUYER_DEFAULT_PRODUCT_ID-purchaseTags.PURCHASE_0_99 ;
 var reward_chips = serverDataObj.server_deal_nonbuyer_image.length>1 ? serverDataObj.server_deal_nonbuyer_coins : NON_BUYER_DEFAULT_CHIPS ;
 this.def.setValueForKey( DEAL_PRODUCT_ID, productId );
 this.def.setValueForKey( DEAL_REWARD_CHIPS, reward_chips );
 this.callBGImage( serverDataObj.server_deal_nonbuyer_image );
}

}
this.offerSessionCount = 0;
this.canSetDeal = this.getDealStatus();
},
callBGImage:function(imagePath){
        //imagePath = "http://dhr6hrqv9tj0z.cloudfront.net/deal-images/Production/doublejackpot-slots/1st-purchase-special-pop-up_bg.png";
     // imagePath = "https://dhr6hrqv9tj0z.cloudfront.net/instant-deals/qa/1068193972/1565.png";

    if (imagePath && imagePath.length>0){
        cc.loader.loadImg(imagePath, { isCrossOrigin:true },function ( err, texture ) {
            if (texture && err==null) {
                var texture2d = new cc.Texture2D();
                texture2d.initWithElement(texture);
                texture2d.handleLoadedTexture();
                AppDelegate.getInstance().getDealInstance().pDealTexture = texture2d;
            }else{
                AppDelegate.getInstance().getDealInstance().setAgainDealData();
            }
        });

    }
},
 setAgainDealData:function () {
this.pDealTexture = null;
     if(this.def.getValueForKey( IS_BUYER, false )) {
         this.def.setValueForKey( DEAL_PRODUCT_ID, BUYER_DEFAULT_PRODUCT_ID -purchaseTags.PURCHASE_0_99 );
         this.def.setValueForKey( DEAL_REWARD_CHIPS, BUYER_DEFAULT_CHIPS );
     }else {
         this.def.setValueForKey( DEAL_PRODUCT_ID, NON_BUYER_DEFAULT_PRODUCT_ID-purchaseTags.PURCHASE_0_99 );
         this.def.setValueForKey( DEAL_REWARD_CHIPS, NON_BUYER_DEFAULT_CHIPS );
     }
 }

});