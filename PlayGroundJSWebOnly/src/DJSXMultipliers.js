/**
 * Created by RNF-Mac11 on 2/10/17.
 */



var reelsRandomizationArrayDJSXMultipliers =
    [
        [// 85% payout
            50, 90, 50, 80, 60, 35,
            50, 90, 55, 50, 50, 100,
            55, 100, 10, 5, 15, 4,
            12, 3, 12, 5, 25, 5,
            3, 5, 10, 10, 45, 20,
            10, 10, 10, 7, 10, 3
        ],
        [//92 %
            50, 90, 50, 80, 60, 40,

            50, 90, 55, 50, 50, 100,

            55, 100, 10, 5, 20, 3,

            12, 4, 12, 5, 25, 5,

            3, 5, 10, 10, 45, 20,

            10, 10, 10, 7, 10, 3
        ],
        [//95-96 % payout
            40, 90, 40, 80, 50, 35,
            40, 90, 45, 50, 45, 100,
            45, 100, 10, 5, 15, 3,
            12, 3, 12, 5, 25, 5,
            3, 5, 10, 10, 45, 20,
            10, 10, 10, 5, 10, 3
        ],
        [//140% payout
            30, 150, 30, 140, 30, 100,
            30, 150, 30, 100, 30, 100,
            30, 175, 110, 12, 100, 10,
            120, 8, 120, 12, 20, 30,
            20, 14, 20, 75, 20, 40,
            20, 30, 20, 20, 20, 8
        ],
        [//300% payout
            30, 150, 30, 140, 30, 100,
            30, 150, 30, 100, 30, 100,
            30, 175, 110, 12, 100, 10,
            120, 8, 120, 12, 20, 30,
            20, 14, 20, 75, 20, 40,
            20, 30, 20, 20, 20, 8
        ]
    ];

var DJSXMultipliers = GameScene.extend({

    animAssigned : false,

    sCode : 0,
    reelsArrayDJSXMultipliers:[],
    ctor:function ( level_index ) {
        this.reelsArrayDJSXMultipliers = [
            /*1*/ELEMENT_EMPTY,/*2*/ELEMENT_7_BAR,/*3*/ELEMENT_EMPTY,/*4*/ELEMENT_7_BLUE,/*5*/ELEMENT_EMPTY,
            /*6*/ELEMENT_7_PINK,/*7*/ELEMENT_EMPTY,/*8*/ELEMENT_7_RED,/*9*/ELEMENT_EMPTY,/*10*/ELEMENT_BAR_1,
            /*11*/ELEMENT_EMPTY,/*12*/ELEMENT_BAR_2,/*13*/ELEMENT_EMPTY,/*14*/ELEMENT_BAR_3,/*15*/ELEMENT_EMPTY,
            /*16*/ELEMENT_2X,/*17*/ELEMENT_EMPTY,/*18*/ELEMENT_3X,/*19*/ELEMENT_EMPTY,/*20*/ELEMENT_10X,
            /*21*/ELEMENT_EMPTY,/*22*/ELEMENT_5X,/*23*/ELEMENT_EMPTY,/*24*/ELEMENT_BAR_1,/*25*/ELEMENT_EMPTY,
            /*26*/ELEMENT_2X,/*27*/ELEMENT_EMPTY,/*28*/ELEMENT_BAR_2,/*29*/ELEMENT_EMPTY,/*30*/ELEMENT_7_BLUE,
            /*31*/ELEMENT_EMPTY,/*32*/ELEMENT_BAR_3,/*33*/ELEMENT_EMPTY,/*34*/ELEMENT_7_BAR,/*35*/ELEMENT_EMPTY,
            /*36*/ELEMENT_3X
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSXMultipliers;
        this.physicalArrayCheck= this.reelsArrayDJSXMultipliers;
        this._super( level_index );

        this.totalSlots = 4;

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;


    },

    calculatePayment : function( doAnimate )
    {
        var payMentArray = [
            /*0*/   2000,  // 10x 10x 10x

            /*1*/     500, // 5x 5x 5x

            /*2*/     300, // 2x 3x 2x

            /*3*/     25,  // 7 7 7 Red

            /*4*/     20,  // 7 7 7 white

            /*5*/     15,  // 7 7 7 Green

            /*6*/     12,  // 7Bar 7Bar 7Bar

            /*7*/     10,   // 3Bar 3Bar 3Bar

            /*8*/     7,   // 2Bar 2Bar 2Bar

            /*9*/     5,   // Bar Bar Bar

            /*10*/    5,   // Any 7 Combo

            /*11*/    3   // Any Bar Combo
        ];


        this.lineVector = [];
        var tmpVector = new Array();
        var tmpVector2 = new Array();

        for (var j = 0; j < 4; j++)
        {
            tmpVector = [];
            tmpVector2 = [];
            for (var i = 0; i < this.slots[j].resultReel.length; i++)
            {
                tmpVector = this.slots[j].resultReel[i];
                tmpVector2.push(tmpVector[1]);
            }

            this.lineVector.push(tmpVector2);
        }

        var totalPay = 0;
        var pay = 0;
        var tmpPay = 1;
        var lineLength = 3;
        var count2X = 0;
        var count3X = 0;
        var count10X = 0;
        var count5X = 0;
        var count7P = 0;
        var countWild = 0;
        var countGeneral = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var i, j, k;
        var e = null;
        var pivotElm = null;
        var pivotElm2 = null;
        var tmpVec = new Array();


        for (i = 0; i < 4; i++)
        {
            tmpVec = [];
            tmpVec = this.lineVector[i];


            for (k = 0; k < 10; k++)
            {
                var breakLoop = false;
                countGeneral = 0;
                pay = 0;
                tmpPay = 1;
                count2X = 0;
                count3X = 0;
                count10X = 0;
                count5X = 0;
                count7P = 0;
                countWild = 0;
                countGeneral = 0;
                pivotElement = -1;
                pivotElement2 = -1;
                e = null;
                pivotElm = null;
                pivotElm2 = null;
                this.animAssigned = false;

                switch (k)
                {
                    case 0://for all combination with wild
                        for (j = 0; j < lineLength; j++)
                        {
                            if (this.resultArray[i][j] != -1 )
                            {
                                switch (this.reelsArrayDJSXMultipliers[this.resultArray[i][j]])
                                {
                                    case ELEMENT_2X:
                                        count2X++;
                                        tmpPay*=2;
                                        countWild++;
                                        if ( doAnimate )
                                        {
                                            e = tmpVec[j];
                                        }
                                        if (e)
                                        {
                                            e.aCode = GLOW_TAG;
                                        }
                                        break;

                                    case ELEMENT_3X:
                                        count3X++;
                                        tmpPay*=3;
                                        countWild++;
                                        if ( doAnimate )
                                        {
                                            e = tmpVec[j];
                                        }
                                        if (e)
                                        {
                                            e.aCode = GLOW_TAG;
                                        }
                                        break;

                                    case ELEMENT_10X:
                                        count10X++;
                                        tmpPay*=10;
                                        countWild++;
                                        if ( doAnimate )
                                        {
                                            e = tmpVec[j];
                                        }
                                        if (e)
                                        {
                                            e.aCode = GLOW_TAG;
                                        }
                                        break;

                                    case ELEMENT_5X:
                                        count5X++;
                                        tmpPay*=5;
                                        countWild++;
                                        if ( doAnimate )
                                        {
                                            e = tmpVec[j];
                                        }
                                        if (e)
                                        {
                                            e.aCode = GLOW_TAG;
                                        }
                                        break;

                                    default:
                                        switch (pivotElement)
                                        {
                                            case -1:
                                                pivotElement = this.reelsArrayDJSXMultipliers[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate )
                                                {
                                                    e = tmpVec[j];
                                                }
                                                if (e)
                                                {
                                                    pivotElm = e;
                                                }
                                                break;

                                            default:
                                                pivotElement2 = this.reelsArrayDJSXMultipliers[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate )
                                                {
                                                    e = tmpVec[j];
                                                }
                                                if (e)
                                                {
                                                    pivotElm2 = e;
                                                }
                                                break;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }

                        if (j == 3)
                        {
                            if ( count10X == 3 )
                            {
                                pay = payMentArray[0];
                                breakLoop = true;
                            }
                            else if ( count5X == 3 )
                            {
                                pay = payMentArray[1];
                                breakLoop = true;
                            }
                            else if (count2X == 2 && count3X == 1)
                            {
                                pay = payMentArray[2];
                                breakLoop = true;
                            }
                            else if(countWild >= 2)
                            {
                                breakLoop = true;

                                switch (pivotElement)
                                {
                                    case ELEMENT_7_RED:
                                        pay = tmpPay * payMentArray[3];
                                        if ( doAnimate )
                                        {
                                            pivotElm.aCode = GLOW_TAG;
                                        }

                                        break;

                                    case ELEMENT_7_PINK:
                                        pay = tmpPay * payMentArray[4];
                                        if ( doAnimate )
                                        {
                                            pivotElm.aCode = GLOW_TAG;
                                        }
                                        break;

                                    case ELEMENT_7_BLUE:
                                        pay = tmpPay * payMentArray[5];
                                        if ( doAnimate )
                                        {
                                            pivotElm.aCode = GLOW_TAG;
                                        }
                                        break;

                                    case ELEMENT_7_BAR:
                                        pay = tmpPay * payMentArray[6];
                                        if ( doAnimate )
                                        {
                                            pivotElm.aCode = GLOW_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_3:
                                        pay = tmpPay * payMentArray[7];
                                        if ( doAnimate )
                                        {
                                            pivotElm.aCode = GLOW_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_2:
                                        pay = tmpPay * payMentArray[8];
                                        if ( doAnimate )
                                        {
                                            pivotElm.aCode = GLOW_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_1:
                                        pay = tmpPay * payMentArray[9];
                                        if ( doAnimate )
                                        {
                                            pivotElm.aCode = GLOW_TAG;
                                        }
                                        break;

                                    default:
                                        pay = tmpPay;
                                        if (pivotElm)
                                        {
                                            pivotElm.aCode = GLOW_TAG;
                                        }
                                        break;
                                }
                            }
                            else if(countWild == 1)
                            {
                                breakLoop = true;

                                if (pivotElement == pivotElement2)
                                {
                                    switch (pivotElement)
                                    {
                                        case ELEMENT_7_RED:
                                            pay = tmpPay * payMentArray[3];
                                            if ( doAnimate )
                                            {
                                                pivotElm.aCode = GLOW_TAG;
                                                pivotElm2.aCode = GLOW_TAG;
                                            }

                                            break;

                                        case ELEMENT_7_PINK:
                                            pay = tmpPay * payMentArray[4];
                                            if ( doAnimate )
                                            {
                                                pivotElm.aCode = GLOW_TAG;
                                                pivotElm2.aCode = GLOW_TAG;
                                            }
                                            break;

                                        case ELEMENT_7_BLUE:
                                            pay = tmpPay * payMentArray[5];
                                            if ( doAnimate )
                                            {
                                                pivotElm.aCode = GLOW_TAG;
                                                pivotElm2.aCode = GLOW_TAG;
                                            }
                                            break;

                                        case ELEMENT_7_BAR:
                                            pay = tmpPay * payMentArray[6];
                                            if ( doAnimate )
                                            {
                                                pivotElm.aCode = GLOW_TAG;
                                                pivotElm2.aCode = GLOW_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_3:
                                            pay = tmpPay * payMentArray[7];
                                            if ( doAnimate )
                                            {
                                                pivotElm.aCode = GLOW_TAG;
                                                pivotElm2.aCode = GLOW_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_2:
                                            pay = tmpPay * payMentArray[8];
                                            if ( doAnimate )
                                            {
                                                pivotElm.aCode = GLOW_TAG;
                                                pivotElm2.aCode = GLOW_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_1:
                                            pay = tmpPay * payMentArray[9];
                                            if ( doAnimate )
                                            {
                                                pivotElm.aCode = GLOW_TAG;
                                                pivotElm2.aCode = GLOW_TAG;
                                            }
                                            break;

                                        default:
                                            pay = tmpPay;
                                            break;
                                    }
                                }
                                else
                                {
                                    if ( ( pivotElement == ELEMENT_7_BAR || pivotElement == ELEMENT_7_PINK ) &&
                                        ( pivotElement2 == ELEMENT_7_BAR || pivotElement2 == ELEMENT_7_PINK ) )
                                    {
                                        pay = tmpPay * payMentArray[4];
                                        if ( doAnimate )
                                        {
                                            pivotElm.aCode = GLOW_TAG;
                                            pivotElm2.aCode = GLOW_TAG;
                                        }
                                    }
                                    else
                                    if (pivotElement >= ELEMENT_7_BAR && pivotElement <= ELEMENT_7_RED &&
                                        pivotElement2 >= ELEMENT_7_BAR && pivotElement2 <= ELEMENT_7_RED)
                                    {
                                        pay = tmpPay * payMentArray[10];
                                        if ( doAnimate )
                                        {
                                            pivotElm.aCode = GLOW_TAG;
                                            pivotElm2.aCode = GLOW_TAG;
                                        }
                                    }
                                    else if ( ( pivotElement == ELEMENT_7_BAR && pivotElement2 == ELEMENT_BAR_1 ) ||
                                        ( pivotElement2 == ELEMENT_7_BAR && pivotElement == ELEMENT_BAR_1 ) )
                                    {
                                        pay = tmpPay * payMentArray[9];
                                        if ( doAnimate )
                                        {
                                            pivotElm.aCode = GLOW_TAG;
                                            pivotElm2.aCode = GLOW_TAG;
                                        }
                                    }
                                    else if ( ( ( pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_BAR_3 ) || pivotElement == ELEMENT_7_BAR ) &&
                                        ( ( pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_BAR_3 ) || pivotElement2 == ELEMENT_7_BAR ) )
                                    {
                                        pay = tmpPay * payMentArray[11];
                                        if ( doAnimate )
                                        {
                                            pivotElm.aCode = GLOW_TAG;
                                            pivotElm2.aCode = GLOW_TAG;
                                        }
                                    }
                                    else
                                    {
                                        pay = tmpPay;
                                    }
                                }
                            }
                        }
                        break;

                    case 1: // 7 7 7 red
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSXMultipliers[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[3];
                            breakLoop = true;

                            for (var l = 0; l < countGeneral; l++) {
                                if ( doAnimate )
                                {
                                    e = tmpVec[l];
                                }

                                if (e)
                                {
                                    e.aCode = GLOW_TAG;
                                }
                            }
                        }
                        break;

                    case 2: // 7 7 7 white
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSXMultipliers[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_PINK:
                                    count7P++;
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 && count7P )
                        {
                            pay = payMentArray[4];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                                if ( doAnimate )
                                {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = GLOW_TAG;
                                }
                            }
                        }
                        break;

                    case 3: // 7 7 7 Blue
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSXMultipliers[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_BLUE:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[5];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                                if ( doAnimate )
                                {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = GLOW_TAG;
                                }
                            }
                        }
                        break;

                    case 4: // 7 7 7 bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSXMultipliers[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[6];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                                if ( doAnimate )
                                {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = GLOW_TAG;
                                }
                            }
                        }
                        break;

                    case 5: // 3Bar 3Bar 3Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSXMultipliers[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[7];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                                if ( doAnimate )
                                {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = GLOW_TAG;
                                }
                            }
                        }
                        break;

                    case 6: // 2Bar 2Bar 2Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSXMultipliers[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[8];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                                if ( doAnimate )
                                {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = GLOW_TAG;
                                }
                            }
                        }
                        break;

                    case 7: // Bar Bar Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSXMultipliers[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[9];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                                if ( doAnimate )
                                {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = GLOW_TAG;
                                }
                            }
                        }
                        break;

                    case 8: // any 7 combo
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSXMultipliers[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_BAR:
                                case ELEMENT_7_BLUE:
                                case ELEMENT_7_PINK:
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[10];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                                if ( doAnimate )
                                {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = GLOW_TAG;
                                }
                            }
                        }
                        break;

                    case 9: // any Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSXMultipliers[this.resultArray[i][j]])
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                case ELEMENT_7_BAR:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[11];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++)
                            {
                                if ( doAnimate )
                                {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = GLOW_TAG;
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

                if (breakLoop)
                {
                    break;
                }
            }

            totalPay+=pay;

            if ( doAnimate )
            {
                this.addWin = 0;
                if (pay > 0)
                {

                    this.addWin = pay;
                    this.setCurrentWinAnimation(i);
                }
                else
                {
                    this.setLightsOff(i);
                }
            }
        }

        if ( doAnimate )
        {
            this.currentWin = this.currentBet * totalPay;
            if (this.currentWin > 0)
            {
                this.updateDailyChallengesData(this.currentWin,this.currentBet*4);

                this.addWin = this.currentWin / 10;

                this.totalScore+=this.currentWin;
                userDataInfo.getInstance().totalChips = this.totalScore;
               // this.def.setValueForKey(this.totalScore);
                this.addScore = (this.totalScore - this.displayedScore) / 30;
                if (this.addScore > 0 && !this.haveFreeSpins)
                {
                    this.schedule(this.updateCoin, 0.1);
                }
                this.runAction(new cc.Sequence(new cc.DelayTime(this.surpriseTimeDealy),new cc.CallFunc(function () {
                    this.addSurpiseBonusCoinsLbl(this.currentWin);
                    this.isWinningOccured = false;

                },this)));
            }
            this.checkAndSetAPopUp();
        }
        else
        {
            this.currentWinForcast = this.currentBet * totalPay;
        }
    },
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSXMultipliers[i]=this.physicalReelElements[idx];
                i++;
            }
        }
    },
    changeTotalProbabilities : function()
    {
        this.totalProbalities = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            //this.extractArrayFromSring();
            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[this.easyModeCode][i];
            }
        }
        else
        {
            for ( var i = 0; i < 36; i++ )
            {
                this.probabilityArray[this.easyModeCode][i] = reelsRandomizationArrayDJSXMultipliers[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSXMultipliers[this.easyModeCode][i];
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
    },

    generateResult : function( scrIndx )
    {
        for ( var i = 0; i < this.slots[scrIndx].displayedReels.length; i++ )
        {
            //generate:
            var num = parseInt( this.prevReelIndex[i] );

            var tmpnum = 0;

            while ( num == parseInt( this.prevReelIndex[i] ) )
            {
                num = Math.floor( Math.random() * this.totalProbalities );

            }
            this.prevReelIndex[i] = num;
            this.resultArray[scrIndx][i] = num;

            for (var j = 0; j < 36; j++)
            {
                tmpnum+=this.probabilityArray[this.easyModeCode][j];

                if (tmpnum >= this.resultArray[scrIndx][i])
                {
                    this.resultArray[scrIndx][i] = j;
                    break;
                }
            }
            //console.log( " rand =  " + num + " index = " + this.resultArray[0][i] );
        }

        /*
         int highPay[][3] = {
         {15, 21, 15}, //2x 5x 2x
         {15, 19, 15}, //2x 4x 2x
         {15, 17, 15}, //2x 3x 2x
         {7, 7, 7},    //Triple 7 Red
         {5, 5, 5},    //Triple 7 Yellow
         {29, 29, 29}, //Triple 7 Green
         {1, 1, 1},    //Triple 7 Bar
         {31, 31, 31}, //Triple 3Bar
         {27, 27, 27}, //Triple 2Bar
         {23, 23, 23}, //Triple Bar
         };*/
        /*/HACK
         this.resultArray[0][0] = 0;
         this.resultArray[0][1] = 1;
         this.resultArray[0][2] = 2;
         //*///HACK

        this.calculatePayment( false );
        this.checkForFalseWin();

    },
    rollUpdator : function(dt)
    {
        var i;
        for (i = 0; i < this.totalSlots; i++)
        {
            this.slots[i].updateSlot(dt);
        }

        for (i = 0; i < this.totalSlots; i++)
        {
            if ( this.slots[i].isSlotRunning() )
            {
                break;
            }
        }

        if (i >= this.totalSlots)
        {
            this.isRolling = false;
            this.unschedule( this.rollUpdator );
            this.calculatePayment( true );

            if (this.isAutoSpinning)
            {
                if (this.currentWin > 0)
                {
                    this.scheduleOnce( this.autoSpinCB, 1.5);
                }
                else
                {
                    this.scheduleOnce( this.autoSpinCB, 1.0);
                }
            }
        }
    },
    setFalseWin : function()
    {
    },
    setGameBG : function(i)
    {
        var sprite = new cc.Sprite( "res/DJSXMultipliers/bg.png" );
        if (sprite )
        {
            sprite.setPosition( cc.p( this.vSize.width * 0.5, this.vSize.height * 0.5 ) );
            this.addChild(sprite);
        }

        //break;

        var topSprite = new cc.Sprite( res.Top_Bar_Lobby_png );
        topSprite.setPosition( cc.p( this.vSize.width * 0.5, this.vSize.height - topSprite.getContentSize().height * 0.5 ) );
        this.addChild(topSprite, 1);

        var topBarBack = new cc.Sprite( res.Top_Bar_Back_png );
        this.resizer.setFinalScaling( topBarBack );
        topBarBack.setPosition( cc.p( topSprite.getPositionX(), this.vSize.height - topBarBack.getContentSize().height * 0.5 * topBarBack.getScaleY() ) );
        this.addChild( topBarBack, 1 );

        var bottomBar = new cc.Sprite( "#bottom_bar.png" );
        bottomBar.setPosition( cc.p( this.vSize.width / 2, this.vSize.height - 587 - bottomBar.getContentSize().height / 2 ) );

        this.addChild(bottomBar, 1);
    },
    setLightsOff : function( scrCode)
    {
        this.slots[scrCode].lightsOff();
    },
    setResult : function( indx, lane, sSlot )
    {
        var gap = 387;

        var j = 0;
        var animOriginY;

        var r = this.slots[sSlot].physicalReels[lane];

        var arrSize = r.elementsVec.length;
        var spr;
        var spriteVec = new Array();

        animOriginY = gap * r.direction;
        spr = r.elementsVec[indx];
        if (spr.eCode == ELEMENT_EMPTY)
        {
            gap = 285 * spr.getScale();
        }

        if (indx == 0)
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push(spr);

            spr.setPositionY(animOriginY);

            for (var i = 0; i < 2; i++)
            {
                spr = r.elementsVec[i];
                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (i + 1) * r.direction);
            }
        }
        else if (indx == arrSize - 1)
        {
            spr = r.elementsVec[0];
            spriteVec.push(spr);

            spr.setPositionY(animOriginY);
            for (var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i--)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push(spr);
            spr.setPositionY(animOriginY);

            for (var i = indx; i < indx + 2; i++)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }

        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );

            gap = -773.5;

            var eee;
            if (i < 2)
            {
                eee = spriteVec[i + 1];
            }
            if (i == 0)
            {
                var ee = spriteVec[i + 1];
                if (Math.abs(e.getPositionY() - ee.getPositionY()) != 387)
                {
                    gap = -(Math.abs(e.getPositionY() - ee.getPositionY()) * 2.05);
                }
            }

            gap*=r.direction;
            var ease = new cc.MoveTo(0.3, cc.p(0, e.getPositionY() + gap) ).easing( cc.easeBackOut() );

            ease.setTag(MOVING_TAG);
            e.runAction(ease);
        }
        return spriteVec;
    },
    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSXMultipliers;

        if ( this.sCode < this.totalSlots && !this.slots[this.sCode])
        {
            this.slots[this.sCode] = new SlotScreen(tmp, this.mCode, this.sCode);
            this.addChild(this.slots[this.sCode]);
            this.sCode++;
        }
    },
    setWinAnimation : function(animCode, screen)
    {
        if (this.animAssigned)
        {
            return;
        }
        this.animAssigned = true;

        var fire;

        if (screen == 0)
        {
            for (var i = 0; i < this.animNodeVec.length; i++)
            {
                fire = this.animNodeVec[i];
                fire.removeFromParent(true);
            }
            this.animNodeVec = [];
        }

        var r = this.slots[screen].blurReels[1];
        var pos = r.getParent().getPosition();

        {
            var node1 = new cc.Sprite("#glow.png");
            node1.setPosition(pos);
            node1.setOpacity(100);
            this.addChild(node1, 5);

            this.animNodeVec.push(node1);
        }
        {
            var node = new cc.Sprite("#glow_bg.png");

            node.setPosition(pos);
            node.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.5, 150 ), new cc.FadeTo( 0.5, 255 ) ) ) );
            this.addChild(node, 5);
            this.animNodeVec.push(node);
        }

        //DailyChallenges.getInstance().updateTaskData( C_SCORE, this.currentWin );

       // DailyChallenges.getInstance().updateTaskData( C_WIN_COUNT, 1 );

        if ( this.currentWin >= this.currentBet * 50 )
        {
           // DailyChallenges.getInstance().updateTaskData( C_HUMONGOUS_WIN_COUNT, 1 );
           // DailyChallenges.getInstance().updateTaskData( C_HUGE_WIN_COUNT, 1 );
           // DailyChallenges.getInstance().updateTaskData( C_BIG_WIN_COUNT, 1 );
        }
        else if ( this.currentWin >= this.currentBet * 25 )
        {
           // DailyChallenges.getInstance().updateTaskData( C_HUGE_WIN_COUNT, 1 );
           // DailyChallenges.getInstance().updateTaskData( C_BIG_WIN_COUNT, 1 );
        }
        else if ( this.currentWin >= this.currentBet * 10 )
        {
            //DailyChallenges.getInstance().updateTaskData( C_BIG_WIN_COUNT, 1 );
        }
    },
    spin : function()
    {
        var betAmt = this.currentBet;

        betAmt*=4;

        this.unschedule( this.updateWinCoin );
        this.deductBet( betAmt );
        if (this.displayedScore != this.totalScore)
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;

        this.currentWin = 0;
        this.currentWinForcast = 0;
        this.startRolling();

        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    }
});


