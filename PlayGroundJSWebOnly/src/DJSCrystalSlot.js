
var reelsArrayDJSCrystalSlotWild1 = [
    /*1*/ELEMENT_X,/*2*/ELEMENT_EMPTY,/*3*/ELEMENT_3X,/*4*/ELEMENT_EMPTY,/*5*/ELEMENT_2X,
    /*6*/ELEMENT_EMPTY,/*7*/ELEMENT_4X,/*8*/ELEMENT_EMPTY,/*9*/ELEMENT_X,/*10*/ELEMENT_EMPTY,
    /*11*/ELEMENT_3X,/*12*/ELEMENT_EMPTY,/*13*/ELEMENT_2X,/*14*/ELEMENT_EMPTY,/*15*/ELEMENT_4X,
    /*16*/ELEMENT_EMPTY,/*17*/ELEMENT_X,/*18*/ELEMENT_EMPTY,/*19*/ELEMENT_3X,/*20*/ELEMENT_EMPTY,
    /*21*/ELEMENT_2X,/*22*/ELEMENT_EMPTY,/*23*/ELEMENT_5X,/*24*/ELEMENT_EMPTY,/*25*/ELEMENT_X,
    /*26*/ELEMENT_EMPTY,/*27*/ELEMENT_3X,/*28*/ELEMENT_EMPTY,/*29*/ELEMENT_2X,/*30*/ELEMENT_EMPTY,
    /*31*/ELEMENT_X,/*32*/ELEMENT_EMPTY,/*33*/ELEMENT_4X,/*34*/ELEMENT_EMPTY,/*35*/ELEMENT_5X,
    /*36*/ELEMENT_EMPTY,
];

var reelsRandomizationArrayDJSCrystalSlot =
    [
[// 86% payout
    1500,55, 750, 150, 1500, 100, 2000, 50, 100, 50, 800, 40, 1000, 50, 250, 100, 1100, 50, 80, 80, 1000, 200, 1600, 50, 1250, 500, 10, 50, 1200, 170, 1200, 200, 700, 500, 15, 50,
],
[//92 %
    2200, 55, 750, 150, 1500, 100, 2000, 50, 100, 50, 800, 40, 1700, 50, 250, 100, 1100, 50, 80, 80, 1000, 200, 1600, 50, 1250, 500, 10, 50, 1200, 170, 1200, 200, 700, 500, 15, 50,
],
[//96.5 %
    2200, 55, 750, 150, 1500, 100, 2000, 50, 100, 50, 800, 40, 2100, 50, 250, 100, 1100, 50, 80, 80, 1500, 200, 1600, 50, 1250, 500, 10, 50, 1200, 170, 1200, 200, 700, 500, 15, 50,
],
[//160 %
    2200, 55, 750, 150, 1500, 100, 2000, 50, 100, 50, 800, 40, 2100, 50, 250, 100, 1100, 50, 80, 80, 1500, 200, 1600, 50, 1250, 500, 10, 50, 1200, 170, 1200, 200, 700, 500, 15, 50,
],
[//300 %
    3200, 55, 750, 150, 2500, 100, 2000, 50, 300, 50, 800, 40, 2300, 50, 250, 100, 1100, 50, 160, 80, 1500, 50, 1600, 50, 1250, 50, 100, 50, 2200, 30, 1200, 200, 700, 40, 15, 50,
],
/*{//96.5 %
    2200, 55, 750, 150, 1500, 100, 2000, 50, 100, 50, 800, 40, 2100, 50, 250, 100, 1100, 50, 80, 80, 1500, 200, 1600, 50, 1250, 500, 10, 50, 1200, 170, 1200, 200, 700, 500, 15, 50,
},
{//96.5 %
    2200, 55, 750, 150, 1500, 100, 2000, 50, 100, 50, 800, 40, 2100, 50, 250, 100, 1100, 50, 80, 80, 1500, 200, 1600, 50, 1250, 500, 10, 50, 1200, 170, 1200, 200, 700, 500, 15, 50,
},
{//96.5 %
    2200, 55, 750, 150, 1500, 100, 2000, 50, 100, 50, 800, 40, 2100, 50, 250, 100, 1100, 50, 80, 80, 1500, 200, 1600, 50, 1250, 500, 10, 50, 1200, 170, 1200, 200, 700, 500, 15, 50,
},
{//96.5 %
    2200, 55, 750, 150, 1500, 100, 2000, 50, 100, 50, 800, 40, 2100, 50, 250, 100, 1100, 50, 80, 80, 1500, 200, 1600, 50, 1250, 500, 10, 50, 1200, 170, 1200, 200, 700, 500, 15, 50,
},
{//96.5 %
    2200, 55, 750, 150, 1500, 100, 2000, 50, 100, 50, 800, 40, 2100, 50, 250, 100, 1100, 50, 80, 80, 1500, 200, 1600, 50, 1250, 500, 10, 50, 1200, 170, 1200, 200, 700, 500, 15, 50,
},*/
];



var reelsRandomizationArrayDJSCrystalSlotWild =
    [
        [// 86% payout
            1000, 500, 70, 150, 170, 250, 100, 60, 125, 500, 150, 150, 40, 20, 30, 100, 60, 50, 100, 80, 35, 150, 65, 900, 225, 600, 50, 250, 200, 150, 500, 200, 180, 150, 30, 600,
        ],
        [//92 %
            1000, 500, 70, 150, 170, 250, 100, 60, 125, 500, 150, 150, 40, 20, 50, 100, 60, 50, 100, 80, 35, 150, 65, 900, 225, 600, 50, 250, 200, 150, 500, 200, 180, 150, 30, 600,
        ],
        [//96.5 %
            1000, 500, 70, 150, 170, 250, 100, 60, 125, 500, 150, 150, 40, 20, 50, 100, 60, 50, 100, 80, 35, 150, 65, 900, 225, 600, 50, 250, 200, 150, 500, 200, 180, 150, 30, 600,
        ],
        [//160 %
            1000, 500, 70, 150, 170, 250, 250, 60, 125, 500, 150, 150, 40, 20, 220, 100, 60, 50, 100, 80, 35, 150, 220, 900, 225, 600, 50, 250, 200, 150, 500, 200, 720, 150, 200, 350,
        ],
        [//300 %
            1000, 500, 400, 150, 170, 250, 500, 60, 125, 500, 150, 150, 40, 20, 320, 100, 60, 50, 100, 80, 35, 150, 450, 50, 225, 600, 50, 250, 200, 150, 500, 200, 720, 150, 340, 50,
        ],
        /*{//96.5 %
            1000, 500, 70, 150, 170, 250, 100, 60, 125, 500, 150, 150, 40, 20, 80, 100, 60, 50, 100, 80, 35, 150, 65, 900, 225, 600, 50, 250, 200, 150, 500, 200, 180, 150, 30, 600,
        },
        {//96.5 %
            1000, 500, 70, 150, 170, 250, 100, 60, 125, 500, 150, 150, 40, 20, 80, 100, 60, 50, 100, 80, 35, 150, 65, 900, 225, 600, 50, 250, 200, 150, 500, 200, 180, 150, 30, 600,
        },
        {//96.5 %
            1000, 500, 70, 150, 170, 250, 100, 60, 125, 500, 150, 150, 40, 20, 80, 100, 60, 50, 100, 80, 35, 150, 65, 900, 225, 600, 50, 250, 200, 150, 500, 200, 180, 150, 30, 600,
        },
        {//96.5 %
            1000, 500, 70, 150, 170, 250, 100, 60, 125, 500, 150, 150, 40, 20, 80, 100, 60, 50, 100, 80, 35, 150, 65, 900, 225, 600, 50, 250, 200, 150, 500, 200, 180, 150, 30, 600,
        },
        {//96.5 %
            1000, 500, 70, 150, 170, 250, 100, 60, 125, 500, 150, 150, 40, 20, 80, 100, 60, 50, 100, 80, 35, 150, 65, 900, 225, 600, 50, 250, 200, 150, 500, 200, 180, 150, 30, 600,
        },*/
    ];

var DJSCrystalSlot = GameScene.extend({
    reelsArrayDJSCrystalSlot:[],
    reelsArrayDJSCrystalSlotWild:[],
    ctor:function ( level_index ) {
        this.reelsArrayDJSCrystalSlot = [
            /*1*/ELEMENT_BAR_3,/*2*/ELEMENT_EMPTY,/*3*/ELEMENT_7_BLUE,/*4*/ELEMENT_EMPTY,/*5*/ELEMENT_BAR_2,
            /*6*/ELEMENT_EMPTY,/*7*/ELEMENT_BAR_1,/*8*/ELEMENT_EMPTY,/*9*/ELEMENT_WILD,/*10*/ELEMENT_EMPTY,
            /*11*/ELEMENT_BAR_3,/*12*/ELEMENT_EMPTY,/*13*/ELEMENT_7_BLUE,/*14*/ELEMENT_EMPTY,/*15*/ELEMENT_BAR_2,
            /*16*/ELEMENT_EMPTY,/*17*/ELEMENT_BAR_1,/*18*/ELEMENT_EMPTY,/*19*/ELEMENT_WILD,/*20*/ELEMENT_EMPTY,
            /*21*/ELEMENT_BAR_3,/*22*/ELEMENT_EMPTY,/*23*/ELEMENT_BAR_2,/*24*/ELEMENT_EMPTY,/*25*/ELEMENT_BAR_1,
            /*26*/ELEMENT_EMPTY,/*27*/ELEMENT_WILD,/*28*/ELEMENT_EMPTY,/*29*/ELEMENT_7_BLUE,/*30*/ELEMENT_EMPTY,
            /*31*/ELEMENT_BAR_2,/*32*/ELEMENT_EMPTY,/*33*/ELEMENT_BAR_1,/*34*/ELEMENT_EMPTY,/*35*/ELEMENT_WILD,
            /*36*/ELEMENT_EMPTY,
        ];
        this.reelsArrayDJSCrystalSlotWild = [
            /*1*/ELEMENT_X,/*2*/ELEMENT_EMPTY,/*3*/ELEMENT_3X,/*4*/ELEMENT_EMPTY,/*5*/ELEMENT_2X,
            /*6*/ELEMENT_EMPTY,/*7*/ELEMENT_4X,/*8*/ELEMENT_EMPTY,/*9*/ELEMENT_X,/*10*/ELEMENT_EMPTY,
            /*11*/ELEMENT_3X,/*12*/ELEMENT_EMPTY,/*13*/ELEMENT_2X,/*14*/ELEMENT_EMPTY,/*15*/ELEMENT_4X,
            /*16*/ELEMENT_EMPTY,/*17*/ELEMENT_X,/*18*/ELEMENT_EMPTY,/*19*/ELEMENT_3X,/*20*/ELEMENT_EMPTY,
            /*21*/ELEMENT_2X,/*22*/ELEMENT_EMPTY,/*23*/ELEMENT_5X,/*24*/ELEMENT_EMPTY,/*25*/ELEMENT_X,
            /*26*/ELEMENT_EMPTY,/*27*/ELEMENT_3X,/*28*/ELEMENT_EMPTY,/*29*/ELEMENT_2X,/*30*/ELEMENT_EMPTY,
            /*31*/ELEMENT_X,/*32*/ELEMENT_EMPTY,/*33*/ELEMENT_4X,/*34*/ELEMENT_EMPTY,/*35*/ELEMENT_5X,
            /*36*/ELEMENT_EMPTY,
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSCrystalSlot;
        this.physicalArrayCheck= this.reelsArrayDJSCrystalSlot;
        this._super( level_index );

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;
    },


    calculatePayment : function( doAnimate )
    {
        var payMentArray = [
        /*0*/   1000, // wild wild 5x

        /*1*/   600, // wild wild 3x

        /*2*/   300, // wild wild 3x

        /*3*/   200, // wild wild 5x

        /*4*/   100, // wild wild 1x

        /*5*/   10, // 7Bule 7Bule 7Bule

        /*6*/   6, // 3Bar 3Bar 3Bar

        /*7*/   3, // 2Bar 2Bar 2Bar

        /*8*/   2, // Bar Bar Bar

        /*9*/   1, // Any bar combo
    ];

        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        for ( var i = 0; i < this.slots[0].resultReel.length; i++ )
        {
            tmpVector = this.slots[0].resultReel[i];
            tmpVector2.push( tmpVector[1] );
        }

        this.lineVector.push( tmpVector2 );

        var pay = 0;
        var lineLength = 3;
        var countGeneral = 0;
        var tmpPay = 0;
        var countWild = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var i, j;
        var e = null;
        var pivotElm = null;
        var pivotElm2 = null;
        var tmpVec;
        //haveFreeSpins = false;

        for ( i = 0; i < this.totalLines; i++ )
        {
            tmpVec = this.lineVector[i];

            var breakLoop = false;
            countGeneral = 0;
            //switch ( k )
            {
                //case 0://for all combination with wild
                for ( j = 0; j < lineLength; j++ )
                {
                    if ( this.resultArray[0][j] != -1 && this.resultArray[0][j] != ELEMENT_EMPTY )
                    {
                        switch ( ( ( j == 2 ) ? this.reelsArrayDJSCrystalSlotWild[this.resultArray[i][j]] : this.reelsArrayDJSCrystalSlot[this.resultArray[i][j]] ) )
                        {
                            case ELEMENT_WILD:
                                countWild++;
                                if ( doAnimate ) {
                                    if ( j < tmpVec.length )
                                    {
                                        e = tmpVec[j];
                                    }
                                    if (e)
                                    {
                                        e.aCode = SCALE_TAG;
                                    }
                                }
                                break;

                            case ELEMENT_X:
                                tmpPay = 1;
                                if ( doAnimate ) {
                                    if ( j < tmpVec.length )
                                    {
                                        e = tmpVec[j];
                                    }
                                    if (e)
                                    {
                                        e.aCode = SCALE_TAG;
                                    }
                                }
                                break;

                            case ELEMENT_2X:
                                tmpPay = 2;
                                if ( doAnimate ) {
                                    if ( j < tmpVec.length )
                                    {
                                        e = tmpVec[j];
                                    }
                                    if (e)
                                    {
                                        e.aCode = SCALE_TAG;
                                    }
                                }
                                break;

                            case ELEMENT_3X:
                                tmpPay = 3;
                                if ( doAnimate ) {
                                    if ( j < tmpVec.length )
                                    {
                                        e = tmpVec[j];
                                    }
                                    if (e)
                                    {
                                        e.aCode = SCALE_TAG;
                                    }
                                }
                                break;

                            case ELEMENT_4X:
                                tmpPay = 4;
                                if ( doAnimate ) {
                                    if ( j < tmpVec.length )
                                    {
                                        e = tmpVec[j];
                                    }
                                    if (e)
                                    {
                                        e.aCode = SCALE_TAG;
                                    }
                                }
                                break;

                            case ELEMENT_5X:
                                tmpPay = 5;
                                if ( doAnimate ) {
                                    if ( j < tmpVec.length )
                                    {
                                        e = tmpVec[j];
                                    }
                                    if (e)
                                    {
                                        e.aCode = SCALE_TAG;
                                    }
                                }
                                break;

                            default:
                                switch (pivotElement)
                                {
                                    case -1:
                                        pivotElement = this.reelsArrayDJSCrystalSlot[this.resultArray[i][j]];
                                        countGeneral++;
                                        if ( doAnimate ) {
                                            if ( j < tmpVec.length )
                                            {
                                                e = tmpVec[j];
                                            }
                                            if (e)
                                            {
                                                pivotElm = e;
                                            }
                                        }

                                        break;

                                    default:
                                        pivotElement2 = this.reelsArrayDJSCrystalSlot[this.resultArray[i][j]];
                                        countGeneral++;
                                        if ( doAnimate ) {
                                            if ( j < tmpVec.length )
                                            {
                                                e = tmpVec[j];
                                            }
                                            if (e)
                                            {
                                                pivotElm2 = e;
                                            }
                                        }

                                        break;
                                }
                                break;
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                if (j == 3)
                {
                    if( countWild == 2 )
                    {
                        switch ( tmpPay ) {
                            case 1:
                                pay = 100;
                                break;

                            case 2:
                                pay = 200;
                                break;

                            case 3:
                                pay = 300;
                                break;

                            case 4:
                                pay = 600;
                                break;

                            case 5:
                                pay = 1000;
                                break;

                            default:
                                break;
                        }
                    }
                    else if( countWild == 1 )
                    {
                        switch ( pivotElement ) {
                            case ELEMENT_7_BLUE:
                                pay = payMentArray[5] * tmpPay;
                                break;

                            case ELEMENT_BAR_3:
                                pay = payMentArray[6] * tmpPay;
                                break;

                            case ELEMENT_BAR_2:
                                pay = payMentArray[7] * tmpPay;
                                break;

                            case ELEMENT_BAR_1:
                                pay = payMentArray[8] * tmpPay;
                                break;

                            default:
                                break;
                        }
                    }
                    else if( pivotElement == pivotElement2 )
                    {
                        switch ( pivotElement ) {
                            case ELEMENT_7_BLUE:
                                pay = payMentArray[5] * tmpPay;
                                break;

                            case ELEMENT_BAR_3:
                                pay = payMentArray[6] * tmpPay;
                                break;

                            case ELEMENT_BAR_2:
                                pay = payMentArray[7] * tmpPay;
                                break;

                            case ELEMENT_BAR_1:
                                pay = payMentArray[8] * tmpPay;
                                break;

                            default:
                                break;
                        }
                    }
                    else
                    {
                        var barCount = 0;
                        switch ( pivotElement ) {
                            case ELEMENT_BAR_1:
                            case ELEMENT_BAR_2:
                            case ELEMENT_BAR_3:
                                barCount++;
                                break;

                            default:
                                break;
                        }

                        switch ( pivotElement2 ) {
                            case ELEMENT_BAR_1:
                            case ELEMENT_BAR_2:
                            case ELEMENT_BAR_3:
                                barCount++;
                                break;

                            default:
                                break;
                        }

                        if( barCount == 2 )
                        {
                            pay = tmpPay * payMentArray[9];
                        }
                    }

                    if( pay > 0 )
                    {
                        if( pivotElm ) pivotElm.aCode = SCALE_TAG;
                        if( pivotElm2 ) pivotElm2.aCode = SCALE_TAG;
                    }
                    breakLoop = true;
                }
            }

        }

        if ( doAnimate )
        {
            this.currentWin = this.currentBet * pay;
            //bonusWin+=( currentBet * pay );
        }
        else
        {
            this.currentWinForcast = this.currentBet * pay;
        }

        if ( doAnimate )
        {
            if ( this.currentWin > 0 )
            {
                this.addWin = this.currentWin / 10;
                this.setCurrentWinAnimation();
            }

            this.checkAndSetAPopUp();
        }
    },
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSCrystalSlot[i]=this.physicalReelElements[idx];
                i++;
            }
        }

        if (this.splPhysicalReelElements && this.splPhysicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.splPhysicalReelElements){
                this.reelsArrayDJSCrystalSlotWild[i]= this.splPhysicalReelElements[idx] ;
                i++;
            }
        }
        this.probabilityArray = this.probabilityArray[this.easyModeCode];

    },
    changeTotalProbabilities : function()
    {

        this.totalProbalities = 0;
        this.totalProbalitiesWild = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
              this.probabilityArray = this.probabilityArrayServer[this.easyModeCode];
            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[i];
                this.totalProbalitiesWild+=reelsRandomizationArrayDJSCrystalSlotWild[this.easyModeCode][i];

            }
        }
        else
        {
            for ( var i = 0; i < 36; i++ )
            {
                this.probabilityArray[i] = reelsRandomizationArrayDJSCrystalSlot[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSCrystalSlot[this.easyModeCode][i];
                this.totalProbalitiesWild+=reelsRandomizationArrayDJSCrystalSlotWild[this.easyModeCode][i];
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
        var tmp = this.slots[0].resultReel[laneIndex];

        var e = tmp[1];

        switch (e.eCode)
        {
            case ELEMENT_7_RED:
                if (laneIndex != 0)
                {
                    break;
                }
            case ELEMENT_2X:
            case ELEMENT_3X:
            case ELEMENT_4X:
            case ELEMENT_5X:
                e.runAction(new cc.Repeat( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ), 2 ) );
                this.delegate.jukeBox(S_FLASH);
                break;

            default:
                break;
        }
    },

    generateResult : function( scrIndx )
    {
        for ( var i = 0; i < this.slots[scrIndx].displayedReels.length; i++ )
        {
            //generate:
            var num = parseInt(this.prevReelIndex[i]);

            var tmpnum = 0;

            while (num == parseInt(this.prevReelIndex[i]))
            {
                num = Math.floor( Math.random() * ( ( i == 2 ) ? this.totalProbalitiesWild : this.totalProbalities ) );
            }
            this.prevReelIndex[i] = num;
            this.resultArray[0][i] = num;

            for (var j = 0; j < 36; j++)
            {
                tmpnum+=( ( i == 2 ) ? reelsRandomizationArrayDJSCrystalSlotWild[this.easyModeCode][j] : this.probabilityArray[j] );

                if (tmpnum >= this.resultArray[0][i])
                {
                    this.resultArray[0][i] = j;
                    break;
                }
            }


        }
        /*if( hackBuild ) {
            this.resultArray[0][0] = 0;
            this.resultArray[0][1] = 8;
            this.resultArray[0][2] = 0;
        }*/
    },

    rollUpdator : function(dt)
    {
        var i;
        for (i = 0; i < this.totalSlots; i++)
        {
            this.slots[i].updateSlot(dt);
        }

        for (i = 0; i < this.totalSlots; i++)
        {
            if ( this.slots[i].isSlotRunning() )
            {
                break;
            }
        }

        if (i >= this.totalSlots)
        {
            this.isRolling = false;
            this.unschedule( this.rollUpdator );
            this.calculatePayment( true );

            if (this.isAutoSpinning)
            {
                if (this.currentWin > 0)
                {
                    this.scheduleOnce( this.autoSpinCB, 1.5);
                }
                else
                {
                    this.scheduleOnce( this.autoSpinCB, 1.0);
                }
            }
        }
    },

    setFalseWin : function()
    {
        /*var highWinIndices = [ 1, 3, 19, 25 ];
        var missIndices = [ 1, 3, 7, 19, 25 ];

        var missedIndx = Math.floor( Math.random() * 3 );
        if( missedIndx == 2 )
        {
            if( Math.floor( Math.random() * 10 ) == 3 )
            {
            }
            else
            {
                missedIndx = Math.floor( Math.random() * 2 );
            }
        }

        var sameIndex = Math.floor( Math.random() * highWinIndices.length );

        for ( var i = 0; i < 3; i++ )
        {
            if ( i == missedIndx )
            {
                this.resultArray[0][i] = this.getRandomIndex( missIndices[ Math.floor( Math.random() * missIndices.length ) ], true );
            }
            else
            {
                this.resultArray[0][i] = this.getRandomIndex( highWinIndices[ sameIndex ], false );
            }
        }*/
    },

    setResult : function( indx, lane, sSlot )
    {
        var gap = 387.0;

        var j = 0;
        var animOriginY;

        var r = this.slots[0].physicalReels[lane];
        var arrSize = r.elementsVec.length;
        var spr;

        var spriteVec = [];

        animOriginY = gap * r.direction;
        spr = r.elementsVec[indx];
        if ( spr.eCode == ELEMENT_EMPTY )
        {
            gap = 285 * spr.getScale();
        }

        if ( indx == 0 )
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push(spr);

            spr.setPositionY( animOriginY );

            for (var i = 0; i < 2; i++)
            {
                spr = r.elementsVec[i];
                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (i + 1) * r.direction);
            }
        }
        else if (indx == arrSize - 1)
        {
            spr = r.elementsVec[0];
            spriteVec.push(spr);

            spr.setPositionY(animOriginY);
            for (var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i--)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction );
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push(spr);
            spr.setPositionY(animOriginY);

            for (var i = indx; i < indx + 2; i++)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }

        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            gap = -773.5;

            var eee;
            if (i < 2)
            {
                eee = spriteVec[i + 1];
            }
            if (i == 0)
            {
                var ee = spriteVec[i + 1];
                if ( Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) ) != 387)
                {
                    gap = -Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) * 2 * ee.getScale() );
                }
            }

            gap*=r.direction;
            var ease;

            if ( this.slots[sSlot].isForcedStop )
            {
                ease =  new cc.MoveTo( 0.15, cc.p( 0, e.getPositionY() + gap) ).easing( cc.easeBackOut() );
            }
            else
            {
                ease = new cc.MoveTo( 0.3, cc.p( 0, e.getPositionY() + gap ) ).easing( cc.easeBackOut() );
            }

            ease.setTag( MOVING_TAG );
            e.runAction(ease);
        }

        return spriteVec;
    },

    setWinAnimation : function( animCode, screen )
    {
        var tI = 0.3;

        var fire;
        for (var i = 0; i < this.animNodeVec.length; i++)
        {
            fire = this.animNodeVec[i];
            fire.removeFromParent(true);
        }
        this.animNodeVec = [];


        switch ( animCode )
        {
            case WIN_TAG:

                for (var i = 0; i < this.slots[0].blurReels.length; i++)
                {
                    var r = this.slots[0].blurReels[i];

                    var node = r.getParent();

                    var moveW = this.slots[0].reelDimentions.x * this.slots[0].getScale();
                    var moveH = this.slots[0].reelDimentions.y * this.slots[0].getScale();

                    for (var j = 0; j < 2; j++)
                    {

                        var fire = new cc.ParticleSun();
                        fire.texture = cc.textureCache.addImage( res.FireStar_png );
                        var pos;

                        this.addChild(fire, 1);
                        this.animNodeVec.push(fire);

                        var moveBy = null;

                        var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                        diff*=( 2 - i );

                        switch (j % 4)
                        {
                            case 0:
                                pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY() );
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(moveW, 0));
                                break;

                            case 1:
                                pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(0, moveH));
                                break;

                            /*case 2:
                                pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(-moveW, 0));
                                break;

                            case 3:
                                pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(0, -moveH));
                                break;*/

                            default:
                                break;
                        }
                        fire.runAction( new cc.RepeatForever( new cc.Sequence( moveBy, moveBy.reverse() ) ) );
                    }
                }

                break;

            case BIG_WIN_TAG:
                for(var i = 0; i < this.slots[0].blurReels.length; i++)
                {
                    var r = this.slots[0].blurReels[i];

                    var node = r.getParent();

                    var walkAnimFrames = [];
                    var walkAnim = null;

                    var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                    diff*=( 2 - i );

                    var glowSprite = new cc.Sprite( glowStrings[1] );
                    {
                        var tmp = new cc.SpriteFrame( glowStrings[1], cc.rect(0, 0, glowSprite.getContentSize().width, glowSprite.getContentSize().height));
                        walkAnimFrames.push(tmp);
                    }
                    {
                        var tmp = new cc.SpriteFrame( glowStrings[3], cc.rect(0, 0, glowSprite.getContentSize().width, glowSprite.getContentSize().height));
                        walkAnimFrames.push(tmp);
                    }
                    {
                        var tmp = new cc.SpriteFrame( glowStrings[5], cc.rect(0, 0, glowSprite.getContentSize().width, glowSprite.getContentSize().height));
                        walkAnimFrames.push(tmp);
                    }

                    walkAnim = new cc.Animation( walkAnimFrames, 0.75 );

                    var tmpAnimate = new cc.Animate(walkAnim);
                    glowSprite.runAction( tmpAnimate.repeatForever() );

                    if (glowSprite)
                    {
                        glowSprite.setPosition( node.getPositionX() + this.slots[0].getPositionX() - diff, node.getPositionY() + this.slots[0].getPositionY() );


                        this.addChild(glowSprite, 1);
                        this.animNodeVec.push(glowSprite);

                        this.scaleAccordingToSlotScreen( glowSprite );

                        glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo(0.25, 100), new cc.FadeTo(0.25, 255) ) ) );
                    }
                }
                break;

            default:
                break;
        }
    },

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSCrystalSlot;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    spin : function()
    {
        this.unschedule(this.updateWinCoin);

        this.deductBet( this.currentBet );
        if (this.displayedScore != this.totalScore)
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;
        this.currentWin = 0;
        this.currentWinForcast = 0;
        this.startRolling();

        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    }

});