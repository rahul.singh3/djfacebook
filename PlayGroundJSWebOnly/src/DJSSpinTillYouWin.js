/**
 * Created by RNF-Mac11 on 2/7/17.
 */

var reelsRandomizationArrayDJSSpinTillYouWin =
    [
        [// 85% payout
            1, 40, 1, 40, 1, 40,
            10, 5, 1, 40, 1, 65,
            1,60, 30, 2, 10, 5,
            30, 3, 1, 40, 1, 15,
            30, 3, 1,15, 1,15,
            8, 5, 1, 10, 1,20
        ],
        [//92 %
            1, 45, 1, 40, 1, 40,

            10, 5, 1, 40, 1, 65,

            1,60, 30, 3, 10, 4,

            30, 3, 1, 40, 1, 15,

            30, 5, 1,15, 1,15,


            8, 5, 1, 10, 1,20
        ],
        [//97 % payout
            1, 50, 1, 50, 1, 50,
            8, 5, 1, 40, 1, 65,
            1,60, 30, 2, 8, 5,
            30, 3, 1, 40, 1, 15,
            30, 3, 1,15, 1,15,
            8, 5, 1, 10, 1,20

        ],
        [//140 %

            1, 65, 1, 65, 1, 65,

            8, 5, 1, 45, 1, 75,

            1,70, 15, 5, 7, 7,

            10, 5, 1, 40, 1, 15,

            20, 5, 1,15, 1,15,


            7, 7, 1, 10, 1,20
        ],
        [//300 %
            1, 485, 1, 285, 1, 285,
            1, 45, 1, 265, 1, 295,
            1,290, 1, 55, 1, 47,
            1, 55, 1, 260, 1, 235,
            1, 55, 1,235, 1,235,
            1, 40, 1, 230, 1,250
        ]
    ];

var DJSSpinTillYouWin = GameScene.extend({

    freeSpinJustActivated : false,

    spinTElemnt : null,//the element to show spinTillYouWin animation

    spinTillYouWinActive : 0,
    reelsArrayDJSSpinTillYouWin:[],
    ctor:function ( level_index ) {
        this.reelsArrayDJSSpinTillYouWin = [
            // IMP "ELEMENT_SPIN_TILL_YOU_WIN" should not be the second element and should not pe placed after adjecent empty elements

            /*1*/ELEMENT_EMPTY,/*2*/ELEMENT_7_RED,/*3*/ELEMENT_EMPTY,/*4*/ELEMENT_7_BLUE,/*5*/ELEMENT_EMPTY,
            /*6*/ELEMENT_7_PINK,/*7*/ELEMENT_EMPTY,/*8*/ELEMENT_SPIN_TILL_YOU_WIN,/*9*/ELEMENT_EMPTY,/*10*/ELEMENT_BAR_1,
            /*11*/ELEMENT_EMPTY,/*12*/ELEMENT_BAR_2,/*13*/ELEMENT_EMPTY,/*14*/ELEMENT_BAR_3,/*15*/ELEMENT_EMPTY,
            /*16*/ELEMENT_2X,/*17*/ELEMENT_EMPTY,/*18*/ELEMENT_SPIN_TILL_YOU_WIN,/*19*/ELEMENT_EMPTY,/*20*/ELEMENT_3X,
            /*21*/ELEMENT_EMPTY,/*22*/ELEMENT_BAR_1,/*23*/ELEMENT_EMPTY,/*24*/ELEMENT_BAR_2,/*25*/ELEMENT_EMPTY,
            /*26*/ELEMENT_2X,/*27*/ELEMENT_EMPTY,/*28*/ELEMENT_BAR_3,/*29*/ELEMENT_EMPTY,/*30*/ELEMENT_7_PINK,
            /*31*/ELEMENT_EMPTY,/*32*/ELEMENT_SPIN_TILL_YOU_WIN,/*33*/ELEMENT_EMPTY,/*34*/ELEMENT_7_BLUE,/*35*/ELEMENT_EMPTY,
            /*36*/ELEMENT_BAR_1
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSSpinTillYouWin;
        this.physicalArrayCheck= this.reelsArrayDJSSpinTillYouWin;
        this._super( level_index );

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;
    },

    /*animCB : function(obj)
{
    obj.removeFromParent(true);

    this.isRolling = true;
    this.startRolling(true);
},*/

    calculatePayment : function( doAnimate )
    {
        var payMentArray = [
        /*0*/     20,  // 7 7 7 Red

        /*1*/     15,  // 7 7 7 BLUE

        /*2*/     12,  // 7 7 7 pink

        /*3*/     10,  // Any 7 Combo

        /*4*/     10,   // 3Bar 3Bar 3Bar

        /*5*/     5,   // 2Bar 2Bar 2Bar

        /*6*/     3,   // Bar Bar Bar

        /*7*/    2   // Any Bar Combo
    ];

        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        for ( var i = 0; i < this.slots[0].resultReel.length; i++ )
        {
            tmpVector = this.slots[0].resultReel[ i ];
            tmpVector2.push( tmpVector[ 1 ] );
        }

        this.lineVector.push( tmpVector2 );

        var pay = 0;
        var tmpPay = 1;
        var lineLength = 3;
        var count2X = 0;
        var count3X = 0;
        var countWild = 0;
        var countGeneral = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var i, j, k;
        var e = null;
        var pivotElm = null;
        var pivotElm2 = null;
        var tmpVec;


        for ( i = 0; i < this.totalLines; i++ )
        {
            tmpVec = this.lineVector[ i ];
            for ( k = 0; k < 9; k++ )
            {
                var breakLoop = false;
                countGeneral = 0;
                switch ( k )
                {
                    case 0://for all combination with wild
                        for ( j = 0; j < lineLength; j++ )
                        {
                            if ( this.resultArray[0][j] != -1 && this.reelsArrayDJSSpinTillYouWin[this.resultArray[i][j]]!= ELEMENT_EMPTY )
                            {
                                switch ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[i][j]] )
                                {
                                    case ELEMENT_2X:
                                        count2X++;
                                        tmpPay*=2;
                                        countWild++;
                                        if ( j < tmpVec.length ) {
                                            e = tmpVec[ j ];
                                        }

                                        if ( e )
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_3X:
                                        count3X++;
                                        tmpPay*=3;
                                        countWild++;
                                        if ( j < tmpVec.length ) {
                                            e = tmpVec[ j ];
                                        }
                                        if ( e )
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_SPIN_TILL_YOU_WIN:
                                        countWild++;
                                        if ( j < tmpVec.length ) {
                                            e = tmpVec[ j ];
                                        }
                                        if ( e )
                                        {
                                            e.aCode = SCALE_TAG;
                                        }

                                        this.spinTElemnt = e;

                                        if ( this.spinTillYouWinActive == 0/* && doAnimate*/ )
                                        {
                                            this.spinTillYouWinActive = 1;
                                            this.freeSpinJustActivated = true;
                                        }
                                        break;

                                    default:
                                        switch ( pivotElement )
                                        {
                                            case -1:
                                                pivotElement = this.reelsArrayDJSSpinTillYouWin[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( j < tmpVec.length ) {
                                                    e = tmpVec[ j ];
                                                }
                                                if ( e )
                                                {
                                                    pivotElm = e;
                                                }
                                                break;

                                            default:
                                                pivotElement2 = this.reelsArrayDJSSpinTillYouWin[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( j < tmpVec.length ) {
                                                    e = tmpVec[ j ];
                                                }
                                                if ( e )
                                                {
                                                    pivotElm2 = e;
                                                }
                                                break;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                for ( var a = j; a < lineLength; a++ )
                                {
                                    if ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[i][a]] == ELEMENT_SPIN_TILL_YOU_WIN )
                                    {
                                        countWild++;
                                        if ( a < tmpVec.length ) {
                                            e = tmpVec[ a ];
                                        }
                                        if ( e )
                                        {
                                            e.aCode = SCALE_TAG;
                                        }

                                        this.spinTElemnt = e;

                                        if ( this.spinTillYouWinActive == 0 /*&& doAnimate*/ )
                                        {
                                            this.spinTillYouWinActive = 1;
                                            this.freeSpinJustActivated = true;
                                        }
                                    }
                                }
                                break;
                            }
                        }

                        if ( j == 3 )
                        {
                            if( countWild >= 2 )
                            {
                                breakLoop = true;

                                switch ( pivotElement )
                                {
                                    case ELEMENT_7_RED:
                                        pay = tmpPay * payMentArray[0];
                                        if ( pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_7_PINK:
                                        pay = tmpPay * payMentArray[2];
                                        if ( pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_7_BLUE:
                                        pay = tmpPay * payMentArray[1];
                                        if ( pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_3:
                                        pay = tmpPay * payMentArray[4];
                                        if ( pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_2:
                                        pay = tmpPay * payMentArray[5];
                                        if ( pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_1:
                                        pay = tmpPay * payMentArray[6];
                                        if ( pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        pay = tmpPay;
                                        if ( pivotElm )
                                        {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;
                                }
                            }
                            else if( countWild == 1 )
                            {
                                breakLoop = true;

                                if ( pivotElement == pivotElement2 )
                                {
                                    switch ( pivotElement )
                                    {
                                        case ELEMENT_7_RED:
                                            pay = tmpPay * payMentArray[0];
                                            if ( pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_7_PINK:
                                            pay = tmpPay * payMentArray[2];
                                            if ( pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_7_BLUE:
                                            pay = tmpPay * payMentArray[1];
                                            if ( pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_3:
                                            pay = tmpPay * payMentArray[4];
                                            if ( pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_2:
                                            pay = tmpPay * payMentArray[5];
                                            if ( pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_1:
                                            pay = tmpPay * payMentArray[6];
                                            if ( pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        default:
                                            break;
                                    }
                                }
                                else
                                {
                                    if ( pivotElement >= ELEMENT_7_BAR && pivotElement <= ELEMENT_7_RED &&
                                        pivotElement2 >= ELEMENT_7_BAR && pivotElement2 <= ELEMENT_7_RED )
                                    {
                                        pay = tmpPay * payMentArray[3];
                                        if ( pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if ( ( pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_BAR_3 ) &&
                                        ( pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_BAR_3 ) )
                                    {
                                        pay = tmpPay * payMentArray[7];
                                        if ( pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                }
                            }
                        }
                        break;

                    case 1: // 7 7 7 red
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[0];
                            breakLoop = true;

                            for ( var l = 0; l < countGeneral; l++ ) {
                            if ( l < tmpVec.length )
                            {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 2: // 7 7 7 pink
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_PINK:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[2];
                            breakLoop = true;
                            for ( l = 0; l < countGeneral; l++ ) {
                            if ( l < tmpVec.length )
                            {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 3: // 7 7 7 Blue
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_BLUE:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[1];
                            breakLoop = true;
                            for ( l = 0; l < countGeneral; l++ ) {
                            if ( l < tmpVec.length )
                            {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 4: // 3Bar 3Bar 3Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[4];
                            breakLoop = true;
                            for ( l = 0; l < countGeneral; l++ ) {
                            if ( l < tmpVec.length )
                            {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 5: // 2Bar 2Bar 2Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[5];
                            breakLoop = true;
                            for ( l = 0; l < countGeneral; l++ ) {
                            if ( l < tmpVec.length )
                            {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 6: // Bar Bar Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[6];
                            breakLoop = true;
                            for ( l = 0; l < countGeneral; l++ ) {
                            if ( l < tmpVec.length )
                            {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 7: // any 7 combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_BLUE:
                                case ELEMENT_7_PINK:
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[3];
                            breakLoop = true;
                            for ( l = 0; l < countGeneral; l++ ) {
                            if ( l < tmpVec.length )
                            {
                                e = tmpVec[ l ];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 8: // any Bar combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[7];
                            breakLoop = true;
                            for ( l = 0; l < countGeneral; l++ )
                            {
                                if ( l < tmpVec.length )
                                {
                                    e = tmpVec[ l ];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

                if ( breakLoop )
                {
                    break;
                }
            }
        }

        if ( doAnimate )
        {
            this.currentWin = this.currentBet * pay;
        }
        else
        {
            this.currentWinForcast = this.currentBet * pay;
        }


        if ( doAnimate )
        {
            var time = 0.5;
            if ( this.currentWin > 0 )
            {
                this.addWin = this.currentWin / 10;
                this.setCurrentWinAnimation(0);

                if ( !this.freeSpinJustActivated )
                {
                    this.spinTillYouWinActive--;
                    if ( this.spinTillYouWinActive <= 0 )
                    {
                        this.spinTillYouWinActive = 0;
                        this.haveFreeSpins = false;
                    }
                }
                time = 1.0;
            }

            this.freeSpinJustActivated = false;

            if ( this.spinTillYouWinActive > 0 )
            {
                this.haveFreeSpins = true;

                this.delegate.jukeBox( S_SPINTILLWIN );

                this.spinTElemnt.runAction( new cc.Sequence( new cc.RotateBy( time, 0, 720 * time, 0 ), new cc.callFunc( function() {
                this.spin();

                this.spinItem.loadTextures( "auto_spin.png", "auto_spin_pressed.png", "", ccui.Widget.PLIST_TEXTURE );
                var glowSprite = this.spinItem.getChildByTag( GLOW_SPRITE_TAG );
                glowSprite.setTexture( res.glow_white_png );

            }, this ) ) );
            }
            else
            {
                this.checkAndSetAPopUp();

                if ( !this.isAutoSpinning )
                {
                    this.spinItem.loadTextures( "spin.png", "spin_pressed.png", "", ccui.Widget.PLIST_TEXTURE );
                    var glowSprite = this.spinItem.getChildByTag( GLOW_SPRITE_TAG );
                    glowSprite.setTexture( res.glow_white_png );
                }

            }
        }

    },
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSSpinTillYouWin[i]=this.physicalReelElements[idx];
                i++;
            }
        }
        this.probabilityArray = this.probabilityArray[this.easyModeCode];

    },
    changeTotalProbabilities : function()
    {
        this.totalProbalities = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            this.probabilityArray = this.probabilityArrayServer[this.easyModeCode];
            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[i];
            }
        }
        else
        {
            for ( var i = 0; i < 36; i++ )
            {
                this.probabilityArray[i] = reelsRandomizationArrayDJSSpinTillYouWin[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSSpinTillYouWin[this.easyModeCode][i];
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;

        if (this.slots[0].isForcedStop || this.wildAnimating)
        {
            return;
        }
        else
        {
            if( ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][0]] == ELEMENT_7_RED && this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][1]] == ELEMENT_7_RED )  ||
                ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][0]] == ELEMENT_7_BLUE && this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][1]] == ELEMENT_7_BLUE ) ||
                ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][0]] == ELEMENT_7_PINK && this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][1]] == ELEMENT_7_PINK ) ||
                ( ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][0]] <= ELEMENT_3X ) &&
                ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][1]] <= ELEMENT_3X ) ) ||
                ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][0]] == ELEMENT_7_RED && ((this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][1]] <= ELEMENT_3X ) ) ) ||
                ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][1]] == ELEMENT_7_RED && ((this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][0]] <= ELEMENT_3X ) ) ) ||
                ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][0]] == ELEMENT_7_BLUE && ((this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][1]] <= ELEMENT_3X ) ) ) ||
                ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][1]] == ELEMENT_7_BLUE && ((this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][0]] <= ELEMENT_3X ) ) ) ||
                ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][0]] == ELEMENT_7_PINK && ((this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][1]] <= ELEMENT_3X ) ) ) ||
                ( this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][1]] == ELEMENT_7_PINK && ((this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][0]] <= ELEMENT_3X ) ) ) )
            {
                if ( !this.slots[0].isForcedStop )
                {
                    this.schedule( this.reelStopper, 4.8 );

                    this.delegate.jukeBox( S_EXCITEMENT );

                    var glowSprite = new cc.Sprite( glowStrings[0] );

                    if ( glowSprite )
                    {
                        var r = this.slots[0].physicalReels[ this.slots[0].physicalReels.length - 1 ];

                        glowSprite.setPosition( r.getParent().getPosition().x + this.slots[0].getPosition().x, r.getParent().getPosition().y + this.slots[0].getPosition().y );

                        this.scaleAccordingToSlotScreen( glowSprite );

                        this.addChild( glowSprite, 5 );
                        this.animNodeVec.push( glowSprite );

                        glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ) ) );
                    }
                    else
                    {
                        //cc.log( 'Break here' );
                    }
                }
            }
        }
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
    },

    generateResult : function( scrIndx )
    {
        var havePayment = false;
        for (var i = 0; i < this.slots[scrIndx].displayedReels.length; i++)
        {
            if ( this.spinTillYouWinActive > 0 && i == 1 )
            {
            }
            else
            {
                for ( ; ; ) {
                    //generate:
                        var num = this.prevReelIndex[i];

                    var tmpnum = 0;

                    while (num == this.prevReelIndex[i]) {
                        num = Math.floor(Math.random() * this.totalProbalities);
                    }
                    this.prevReelIndex[i] = num;
                    this.resultArray[0][i] = num;

                    for (var j = 0; j < TOTAL_REEL_ELEMNTS; j++) {
                        tmpnum += this.probabilityArray[j];

                        if (tmpnum >= this.resultArray[0][i]) {
                            this.resultArray[0][i] = j;
                            break;
                        }
                    }

                    if (this.reelsArrayDJSSpinTillYouWin[this.resultArray[0][i]] == ELEMENT_SPIN_TILL_YOU_WIN) {
                        if (i != 1) {
                            //goto
                            //generate;
                        }
                        else {
                            havePayment = true;
                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        if ( !havePayment && this.spinTillYouWinActive <= 0 )
        {
            this.calculatePayment( false );
            this.checkForFalseWin();

            if( this.applyStrategy )
            {
                if( this.currentBet <= 100 )
                {
                    this.applyStrategy = false;
                    CSUserdefauts.getInstance().setValueForKey( STRATEGY_ASSIGNED, true );
                    var highPay = [1, 19, 25];//7Pink 2x 3x

                    for ( var i = 0; i < 3; i++ )
                    {
                        this.resultArray[0][i] = highPay[i];
                    }
                }
                else if( this.currentBet <= 1000 )
                {
                    this.applyStrategy = false;
                    CSUserdefauts.getInstance().setValueForKey( STRATEGY_ASSIGNED, true );
                    var highPay = [
                    [5, 25, 19],//7orange 2x 3x
                    [15, 15, 33],//7Green 7Green 2x
                ];

                    var missedIndx = Math.floor( Math.random() * 2 );

                    for ( var i = 0; i < 3; i++ )
                    {
                        this.resultArray[0][i] = highPay[missedIndx][i];
                    }
                }
            }
        }

        //HACK
         //this.resultArray[0][0] = 15;
         //this.resultArray[0][1] = 7;
         //this.resultArray[0][2] = 1;
         //*///HACK

    },

    rollUpdator : function(dt)
    {
        var i;
        for (i = 0; i < this.totalSlots; i++)
        {
            this.slots[i].updateSlot(dt);
        }

        for (i = 0; i < this.totalSlots; i++)
        {
            if ( this.slots[i].isSlotRunning() )
            {
                break;
            }
        }

        if (i >= this.totalSlots)
        {
            this.isRolling = false;
            this.unschedule( this.rollUpdator );
            this.calculatePayment( true );

            if (this.isAutoSpinning)
            {
                if (this.currentWin > 0)
                {
                    this.scheduleOnce( this.autoSpinCB, 1.5);
                }
                else
                {
                    this.scheduleOnce( this.autoSpinCB, 1.0);
                }
            }
        }
    },

    setFalseWin : function()
    {
        var highWinIndices = [ 1, 3, 19, 25 ];
        var missIndices = [ 1, 3, 7, 19, 25 ];

        var missedIndx = Math.floor( Math.random() * 3 );
        if( missedIndx == 2 )
        {
            if( Math.floor( Math.random() * 10 ) == 3 )
            {
            }
            else
            {
                missedIndx = Math.floor( Math.random() * 2 );
            }
        }

        var sameIndex = Math.floor( Math.random() * highWinIndices.length );

        for ( var i = 0; i < 3; i++ )
        {
            if ( i == missedIndx )
            {
                this.resultArray[0][i] = this.getRandomIndex( missIndices[ Math.floor( Math.random() * missIndices.length ) ], true );
            }
            else
            {
                this.resultArray[0][i] = this.getRandomIndex( highWinIndices[ sameIndex ], false );
            }
        }
    },

    setResult : function( indx, lane, sSlot )
    {
        var gap = 387;

        var j = 0;
        var animOriginY;

        var r = this.slots[0].physicalReels[lane];
        var arrSize = r.elementsVec.length;
        var spr;

        var spriteVec = new Array();

        animOriginY = gap * r.direction;
        spr = r.elementsVec[indx];
        if (spr.eCode == ELEMENT_EMPTY)
        {
            gap = 285 * spr.getScale();
        }

        if (indx == 0)
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push(spr);

            spr.setPositionY(animOriginY);

            for (var i = 0; i < 2; i++)
            {
                spr = r.elementsVec[i];
                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (i + 1) * r.direction);
            }
        }
        else if (indx == arrSize - 1)
        {
            spr = r.elementsVec[0];
            spriteVec.push(spr);

            spr.setPositionY(animOriginY);
            for (var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i--)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push(spr);

            spr.setPositionY(animOriginY);

            for (var i = indx; i < indx + 2; i++)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                if ( this.spinTillYouWinActive > 0 && lane == 1 )
                {
                }
                else
                {
                    spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                }
                j++;
            }

        }

        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            gap = -773.5;

            var eee;
            if (i < 2)
            {
                eee = spriteVec[i + 1];
            }
            if (i == 0)
            {
                var ee = spriteVec[i + 1];
                if ( Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) ) != 387 )
                {
                    gap = - Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) * 2 * ee.getScale() );
                }
            }

            gap*=r.direction;
            var ease = null;

            if ( this.slots[sSlot].isForceStop )
            {
                ease = new cc.MoveTo( 0.15, cc.p( 0, e.getPositionY() + gap ) ).easing( cc.easeBackOut() );
            }
            else if ( this.spinTillYouWinActive > 0 && lane == 1 )
            {
            }
            else
            {
                ease = new cc.MoveTo( 0.3, cc.p( 0, e.getPositionY() + gap ) ).easing( cc.easeBackOut() );
            }

            if ( ease )
            {
                ease.setTag( MOVING_TAG );
                e.runAction( ease );
            }
        }

        return spriteVec;
    },

    setWinAnimation : function( animCode, screen )
{
    var tI = 0.3;

    var fire;
    for (var i = 0; i < this.animNodeVec.length; i++)
    {
        fire = this.animNodeVec[i];
        fire.removeFromParent(true);
    }
    this.animNodeVec = [];


    switch ( animCode )
    {
        case WIN_TAG:

            for (var i = 0; i < this.slots[0].blurReels.length; i++)
        {
            var r = this.slots[0].blurReels[i];

            var node = r.getParent();

            var moveW = this.slots[0].reelDimentions.x * this.slots[0].getScale();
            var moveH = this.slots[0].reelDimentions.y * this.slots[0].getScale();

            for (var j = 0; j < 4; j++)
            {

                var fire = new cc.ParticleSun();
                fire.texture = cc.textureCache.addImage( res.FireStar_png );
                var pos;
                
                this.addChild(fire, 5);
                this.animNodeVec.push(fire);
                
                var moveBy = null;

                var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                diff*=( 2 - i );

                switch (j % 4)
                {
                    case 0:
                        pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY() );
                        fire.setPosition(pos);
                        moveBy = new cc.MoveBy(tI, cc.p(moveW, 0));
                        break;

                    case 1:
                        pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY());
                        fire.setPosition(pos);
                        moveBy = new cc.MoveBy(tI, cc.p(0, moveH));
                        break;

                    case 2:
                        pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                        fire.setPosition(pos);
                        moveBy = new cc.MoveBy(tI, cc.p(-moveW, 0));
                        break;

                    case 3:
                        pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                        fire.setPosition(pos);
                        moveBy = new cc.MoveBy(tI, cc.p(0, -moveH));
                        break;

                    default:
                        break;
                }
                fire.runAction( new cc.RepeatForever( new cc.Sequence( moveBy, moveBy.reverse() ) ) );
            }
        }

            break;

        case BIG_WIN_TAG:
            for(var i = 0; i < this.slots[0].blurReels.length; i++)
        {
            var r = this.slots[0].blurReels[i];

            var node = r.getParent();

            var walkAnimFrames = [];
            var walkAnim = null;

            var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
            diff*=( 2 - i );

            var glowSprite = new cc.Sprite( glowStrings[1] );
            {
                var tmp = new cc.SpriteFrame( glowStrings[1], cc.rect(0, 0, glowSprite.getContentSize().width, glowSprite.getContentSize().height));
                walkAnimFrames.push(tmp);
            }
            {
                var tmp = new cc.SpriteFrame( glowStrings[3], cc.rect(0, 0, glowSprite.getContentSize().width, glowSprite.getContentSize().height));
                walkAnimFrames.push(tmp);
            }
            {
                var tmp = new cc.SpriteFrame( glowStrings[5], cc.rect(0, 0, glowSprite.getContentSize().width, glowSprite.getContentSize().height));
                walkAnimFrames.push(tmp);
            }

            walkAnim = new cc.Animation( walkAnimFrames, 0.75 );

            var tmpAnimate = new cc.Animate(walkAnim);
            glowSprite.runAction( tmpAnimate.repeatForever() );

            if (glowSprite)
            {
                glowSprite.setPosition( node.getPositionX() + this.slots[0].getPositionX() - diff, node.getPositionY() + this.slots[0].getPositionY() );


                this.addChild(glowSprite, 5);
                this.animNodeVec.push(glowSprite);

                this.scaleAccordingToSlotScreen( glowSprite );

                glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo(0.25, 100), new cc.FadeTo(0.25, 255) ) ) );
            }
        }
            break;

        default:
            break;
    }
},

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSSpinTillYouWin;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    spin : function()
    {

        this.unschedule( this.updateWinCoin );

        if ( this.spinTillYouWinActive <= 0 )
        {
            this.deductBet( this.currentBet );
        }

        if ( this.displayedScore != this.totalScore )
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;
        this.wildAnimating = false;

        this.currentWin = 0;
        this.currentWinForcast = 0;
        this.startRolling();

        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    }

});