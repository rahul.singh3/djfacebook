/**
 * Created by RNF-Mac11 on 2/9/17.
 */

var reelsRandomizationArrayDJSBonusMachine =
    [
        [// 85% payout

            5, 100, 5, 100, 5, 80,
            5, 60, 5, 130, 5, 130,
            35, 5, 55, 4, 5, 75,
            5, 50, 5, 60, 5, 50,
            5, 75, 5, 60, 5, 50,
            5, 40, 5, 50, 5, 40
        ],
        [//92 %

            5, 120, 5, 120, 5, 125,
            5, 75, 5, 130, 5, 130,
            35, 5, 45, 4, 5, 75,
            5, 50, 5, 60, 5, 50,
            5, 75, 5, 60, 5, 50,
            5, 40, 5, 50, 5, 50
        ],
        [//97 % payout

            5, 120, 5, 120, 5, 125,
            5, 75, 5, 130, 5, 130,
            27, 5, 35, 4, 5, 75,
            5, 50, 5, 60, 5, 50,
            5, 75, 5, 60, 5, 50,
            5, 40, 5, 50, 5, 50
        ],
        [//140 % payout

            5, 120, 5, 120, 5, 125,
            5, 75, 5, 130, 5, 130,
            27, 15, 35, 10, 5, 75,
            5, 50, 5, 60, 5, 50,
            5, 75, 5, 60, 5, 50,
            5, 40, 5, 50, 5, 50
        ],
        [//300 % payout

            5, 120, 5, 120, 5, 125,
            5, 75, 5, 130, 5, 130,
            27, 15, 35, 10, 5, 75,
            5, 50, 5, 60, 5, 50,
            5, 75, 5, 60, 5, 50,
            5, 40, 5, 50, 5, 50
        ]
    ];

var DJSBonusMachine = GameScene.extend({

    tmpVariable : false,
    reelsArrayDJSBonusMachine:[],
    bonusWin : 0,

    ctor:function ( level_index ) {
        this.reelsArrayDJSBonusMachine = [
            /*1*/ELEMENT_EMPTY,/*2*/ELEMENT_7_RED,/*3*/ELEMENT_EMPTY,/*4*/ELEMENT_BAR_2,/*5*/ELEMENT_EMPTY,
            /*6*/ELEMENT_7_PINK,/*7*/ELEMENT_EMPTY,/*8*/ELEMENT_X,/*9*/ELEMENT_EMPTY,/*10*/ELEMENT_BAR_1,
            /*11*/ELEMENT_EMPTY,/*12*/ELEMENT_BAR_3,/*13*/ELEMENT_EMPTY,/*14*/ELEMENT_3X,/*15*/ELEMENT_EMPTY,
            /*16*/ELEMENT_5X,/*17*/ELEMENT_EMPTY,/*18*/ELEMENT_X,/*19*/ELEMENT_EMPTY,/*20*/ELEMENT_7_RED,
            /*21*/ELEMENT_EMPTY,/*22*/ELEMENT_BAR_1,/*23*/ELEMENT_EMPTY,/*24*/ELEMENT_BAR_2,/*25*/ELEMENT_EMPTY,
            /*26*/ELEMENT_X,/*27*/ELEMENT_EMPTY,/*28*/ELEMENT_BAR_3,/*29*/ELEMENT_EMPTY,/*30*/ELEMENT_7_PINK,
            /*31*/ELEMENT_EMPTY,/*32*/ELEMENT_BAR_2,/*33*/ELEMENT_EMPTY,/*34*/ELEMENT_7_RED,/*35*/ELEMENT_EMPTY,
            /*36*/ELEMENT_BAR_1
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSBonusMachine;
        this.physicalArrayCheck= this.reelsArrayDJSBonusMachine;
        this._super( level_index );

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;


    },

    calculatePayment : function( doAnimate )
    {
        var payMentArray = [
            /*0*/   200, // 5x 5x 5x

            /*1*/   100, // 3x 3x 3x

            /*2*/   75, // 5x 5x 3x

            /*3*/   50, // 3x 3x 5x

            /*4*/   25, // 7Green 7Green 7Green

            /*5*/   15, // 7Red 7 Red 7Red

            /*6*/   10, // 3Bar 3Bar 3Bar

            /*7*/   5, // 2Bar 2Bar 2Bar

            /*8*/   3, // Bar Bar Bar

            /*9*/   2, // Any bar combo

            /*10*/  5 // any 7 Combo
        ];

        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        for ( var i = 0; i < this.slots[0].resultReel.length; i++ )
        {
            tmpVector = this.slots[0].resultReel[ i ];
            tmpVector2.push( tmpVector[ 1 ] );
        }

        this.lineVector.push( tmpVector2 );

        var pay = 0;
        var lineLength = 3;
        var countGeneral = 0;
        var elementXCount = 0;
        var tmpPay = 1;
        var count3X = 0;
        var count5X = 0;
        var countWild = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var i, j, k;
        var e = null;
        var pivotElm = null;
        var pivotElm2 = null;
        var tmpVec;

        for ( i = 0; i < this.totalLines; i++ )
        {
            tmpVec = this.lineVector[i];
            for ( k = 0; k < 8; k++ )
            {
                var breakLoop = false;
                countGeneral = 0;
                switch ( k )
                {
                    case 0://for all combination with wild
                        for ( j = 0; j < lineLength; j++ )
                        {
                            if ( this.resultArray[0][j] != -1 && doAnimate && this.resultArray[0][j] != ELEMENT_EMPTY )
                            {
                                switch ( this.reelsArrayDJSBonusMachine[this.resultArray[i][j]] )
                                {
                                    case ELEMENT_X:
                                        elementXCount++;
                                        break;

                                    case ELEMENT_3X:
                                        //breakLoop = true;
                                        count3X++;
                                        tmpPay*=3;
                                        countWild++;
                                        if ( doAnimate ) {
                                            if ( j < tmpVec.length )
                                            {
                                                e = tmpVec[ j ];
                                            }
                                            if (e)
                                            {
                                                e.aCode = SCALE_TAG;
                                            }
                                        }
                                        break;

                                    case ELEMENT_5X:
                                        count5X++;
                                        tmpPay*=5;
                                        countWild++;
                                        if ( doAnimate ) {
                                            if ( j < tmpVec.length )
                                            {
                                                e = tmpVec[ j ];
                                            }
                                            if (e)
                                            {
                                                e.aCode = SCALE_TAG;
                                            }
                                        }
                                        break;

                                    default:
                                        switch (pivotElement)
                                        {
                                            case -1:
                                                pivotElement = this.reelsArrayDJSBonusMachine[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate ) {
                                                    if ( j < tmpVec.length )
                                                    {
                                                        e = tmpVec[ j ];
                                                    }
                                                    if (e)
                                                    {
                                                        pivotElm = e;
                                                    }
                                                }

                                                break;

                                            default:
                                                pivotElement2 = this.reelsArrayDJSBonusMachine[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate ) {
                                                    if ( j < tmpVec.length )
                                                    {
                                                        e = tmpVec[ j ];
                                                    }
                                                    if (e)
                                                    {
                                                        pivotElm2 = e;
                                                    }
                                                }

                                                break;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }

                        if (j == 3)
                        {
                            if (count5X == 3)
                            {
                                pay = payMentArray[0];
                                breakLoop = true;
                            }
                            else if (count3X == 3)
                            {
                                pay = payMentArray[1];
                                breakLoop = true;
                            }
                            else if (count5X == 2 && count3X == 1)
                            {
                                pay = payMentArray[2];
                                breakLoop = true;
                            }
                            else if (count5X == 1 && count3X == 2)
                            {
                                pay = payMentArray[3];
                                breakLoop = true;
                            }
                            else if(countWild >= 2)
                            {
                                breakLoop = true;

                                switch (pivotElement)
                                {
                                    case ELEMENT_7_RED:
                                        pay = tmpPay * payMentArray[5];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_7_PINK:
                                        pay = tmpPay * payMentArray[4];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_3:
                                        pay = tmpPay * payMentArray[6];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_2:
                                        pay = tmpPay * payMentArray[7];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_1:
                                        pay = tmpPay * payMentArray[8];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        break;
                                }
                            }
                            else if(countWild == 1)
                            {
                                breakLoop = true;

                                if (pivotElement == pivotElement2)
                                {
                                    switch (pivotElement)
                                    {
                                        case ELEMENT_7_RED:
                                            pay = tmpPay * payMentArray[5];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_7_PINK:
                                            pay = tmpPay * payMentArray[4];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_3:
                                            pay = tmpPay * payMentArray[6];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_2:
                                            pay = tmpPay * payMentArray[7];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_1:
                                            pay = tmpPay * payMentArray[8];
                                            if ( doAnimate && pivotElm ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        default:
                                            //pay = tmpPay;
                                            break;
                                    }
                                }
                                else
                                {
                                    if (pivotElement >= ELEMENT_7_PINK && pivotElement <= ELEMENT_7_RED &&
                                        pivotElement2 >= ELEMENT_7_PINK && pivotElement2 <= ELEMENT_7_RED)
                                    {
                                        pay = tmpPay * payMentArray[10];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if ((pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_BAR_3) &&
                                        (pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_BAR_3))
                                    {
                                        pay = tmpPay * payMentArray[9];
                                        if ( doAnimate && pivotElm ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                }
                            }
                        }
                        break;

                    case 1: // 7 7 7 Pink
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSBonusMachine[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_PINK:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[4];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {
                                if ( l < tmpVec.length ) {
                                    e = tmpVec[ l ];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 2: // 7 7 7 Red
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSBonusMachine[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[5];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {
                                if ( l < tmpVec.length ) {
                                    e = tmpVec[ l ];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 3: // Any 7 Combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSBonusMachine[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_PINK:
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[10];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {
                                if ( l < tmpVec.length ) {
                                    e = tmpVec[ l ];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 4: // 3Bar 3Bar 3Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSBonusMachine[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[6];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {
                                if ( l < tmpVec.length ) {
                                    e = tmpVec[ l ];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 5: // 2Bar 2Bar 2Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSBonusMachine[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[7];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {
                                if ( l < tmpVec.length ) {
                                    e = tmpVec[ l ];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 6: // Bar Bar Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSBonusMachine[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[8];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {
                                if ( l < tmpVec.length ) {
                                    e = tmpVec[ l ];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 7: // any Bar combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSBonusMachine[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[9];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( l < tmpVec.length ) {
                                    e = tmpVec[ l ];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

                if ( breakLoop )
                {
                    break;
                }
            }
        }

        if ( doAnimate )
        {
            this.currentWin = this.currentBet * pay;
            //bonusWin+=( currentBet * pay );
        }
        else
        {
            this.currentWinForcast = this.currentBet * pay;
        }

        var time = 0.5;

        if ( elementXCount >= 2 )
        {
            this.freeSpinCount = 6;

            time = this.setFreeSpinsAnimation( );
        }

        var multiplyFactor = 1;

        if( this.haveFreeSpins )
        {
            switch ( this.freeSpinCount )
            {
                case 4:
                case 3:
                    multiplyFactor = 2;
                    break;

                case 2:
                case 1:
                case 0:
                    multiplyFactor = 3;
                    break;

                default:
                    break;
            }
        }

        if ( doAnimate )
        {
            this.currentWin*=multiplyFactor;
            if ( this.currentWin > 0 )
            {
                this.addWin = this.currentWin / 10;
                this.setCurrentWinAnimation();
                time = 2.0;
            }


            if ( this.freeSpinCount > 0 )
            {
                this.haveFreeSpins = true;
                this.freeSpinCount--;

                //setMultiplierAnimation( freeSpinCount );

                var node = new cc.Node();
                this.addChild( node );
                node.runAction( new cc.Sequence( new cc.DelayTime( time ), cc.callFunc( function() {
                    node.removeFromParentAndCleanup( true );
                    this.removeMegaWinAnimation();
                    this.spin();
                    this.setMultiplierAnimation( this.freeSpinCount );
                    this.spinItem.loadTextures( "auto_spin.png", "auto_spin_pressed.png", "", ccui.Widget.PLIST_TEXTURE );
                    var glowSprite = this.spinItem.getChildByTag( GLOW_SPRITE_TAG );
                    glowSprite.setTexture( res.glow_white_png );

                    var label = this.getChildByTag( 122 ).getChildByTag( 122 );
                    label.setString( this.freeSpinCount + " left" );

                }, this, node ) ) );
                this.delegate.jukeBox( S_SPINTILLWIN );
            }
            else
            {
                this.checkAndSetAPopUp();
                this.removeChildByTag( FLOWER_TAG );
                this.removeChildByTag( 121 );
                this.removeChildByTag( 122 );
                if ( !this.isAutoSpinning )
                {
                    this.spinItem.loadTextures( "spin.png", "spin_pressed.png", "", ccui.Widget.PLIST_TEXTURE );
                    var glowSprite = this.spinItem.getChildByTag( GLOW_SPRITE_TAG );
                    glowSprite.setTexture( res.glow_white_png );

                }
                this.haveFreeSpins = false;
            }
        }
        else//Change 8 June
        {
            this.currentWinForcast*=multiplyFactor;
        }
    },
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSBonusMachine[i]=this.physicalReelElements[idx];
                i++;
            }
        }
    },

    changeTotalProbabilities : function()
    {
        this.totalProbalities = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            //this.extractArrayFromSring();
            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[this.easyModeCode][i];

            }
        }
        else
        {
            for ( var i = 0; i < 36; i++ )
            {
                this.probabilityArray[this.easyModeCode][i] = reelsRandomizationArrayDJSBonusMachine[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSBonusMachine[this.easyModeCode][i];
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
        if( (this.reelsArrayDJSBonusMachine[this.resultArray[0][0]] == ELEMENT_7_BLUE && this.reelsArrayDJSBonusMachine[this.resultArray[0][1]] == ELEMENT_7_BLUE ) ||
            (this.reelsArrayDJSBonusMachine[this.resultArray[0][0]] == ELEMENT_7_PINK && this.reelsArrayDJSBonusMachine[this.resultArray[0][1]] == ELEMENT_7_PINK ) ||
            (this.reelsArrayDJSBonusMachine[this.resultArray[0][0]] == ELEMENT_7_RED && this.reelsArrayDJSBonusMachine[this.resultArray[0][1]] == ELEMENT_7_RED ) )
        {
            if ( !this.slots[0].isForcedStop )
            {
                this.schedule( this.reelStopper, 4.8 );

                this.delegate.jukeBox( S_EXCITEMENT );

                var glowSprite = new cc.Sprite( glowStrings[0] );

                if ( glowSprite )
                {
                    var r = this.slots[0].physicalReels[ this.slots[0].physicalReels.length - 1 ];

                    glowSprite.setPosition( r.getParent().getPosition().x + this.slots[0].getPosition().x, r.getParent().getPosition().y + this.slots[0].getPosition().y );

                    //this.scaleAccordingToSlotScreen( glowSprite );

                    this.addChild( glowSprite, 5 );
                    this.animNodeVec.push( glowSprite );

                    glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ) ) );
                }
            }
        }
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
    },

    generateResult : function( scrIndx )
    {
        for ( var i = 0; i < this.slots[scrIndx].displayedReels.length; i++ )
        {
            for( ; ; ) {
                //generate:

                var num = this.prevReelIndex[i];


                var tmpnum = 0;

                while (num == this.prevReelIndex[i]) {
                    num = Math.floor(Math.random() * this.totalProbalities);
                }
                this.prevReelIndex[i] = num;
                this.resultArray[0][i] = num;

                for (var j = 0; j < 36; j++ )
                {
                    tmpnum += this.probabilityArray[this.easyModeCode][j];

                    if (tmpnum >= this.resultArray[0][i]) {
                        this.resultArray[0][i] = j;
                        break;
                    }
                }

                //HACK

                /*if( !this.tmpVariable )
                {
                    if ( this.freeSpinCount <= 0 )
                    {
                        if (i == 0)
                        {
                            this.resultArray[0][i] = 7;//2;
                        }
                        else if (i == 1)
                        {
                            this.resultArray[0][i] = 0;//2, 4, 20, 30
                        }
                        else if(i == 2)
                        {
                            this.resultArray[0][i] = 7;
                        }
                    }
                }
                else
                {
                    /*if (i == 0)
                    {
                        this.resultArray[0][i] = 13;//2;
                    }
                    else if (i == 1)
                    {
                        this.resultArray[0][i] = 15;//2, 4, 20, 30
                    }
                    else if(i == 2)
                    {
                        this.resultArray[0][i] = 13;
                    }*
                }
                //*///HACK

                /*/HACK
                 this.resultArray[0][0] = 1;
                 this.resultArray[0][1] = 5;
                 this.resultArray[0][2] = 1;
                 //*///HACK

                if (( i == 1 && this.reelsArrayDJSBonusMachine[this.resultArray[0][i]] == ELEMENT_X ) || ( this.haveFreeSpins && this.reelsArrayDJSBonusMachine[this.resultArray[0][i]] == ELEMENT_X )) {

                }
                else {
                    break;
                }
            }
        }
        //this.tmpVariable = true;
        if ( this.freeSpinCount <= 0 )
        {
            this.calculatePayment( false );
            this.checkForFalseWin();
        }

    },

    rollUpdator : function(dt)
    {
        var i;
        for (i = 0; i < this.totalSlots; i++)
        {
            this.slots[i].updateSlot(dt);
        }

        for (i = 0; i < this.totalSlots; i++)
        {
            if ( this.slots[i].isSlotRunning() )
            {
                break;
            }
        }

        if (i >= this.totalSlots)
        {
            this.isRolling = false;
            this.unschedule( this.rollUpdator );
            this.calculatePayment( true );

            if (this.isAutoSpinning)
            {
                if (this.currentWin > 0)
                {
                    this.scheduleOnce( this.autoSpinCB, 1.5);
                }
                else
                {
                    this.scheduleOnce( this.autoSpinCB, 1.0);
                }
            }
        }
    },

    setFalseWin : function()
    {
        var highWinIndices = [ 19, 29 ];
        var missIndices = [ 19, 29, 13, 15 ];

        var missedIndx = Math.floor( Math.random() * 3 );

        if( missedIndx == 2 )
        {
            if( Math.floor( Math.random() * 10 ) == 3 )
            {
            }
            else
            {
                missedIndx = Math.floor( Math.random() * 2 );
            }
        }

        var sameIndex = Math.floor( Math.random( ) * highWinIndices.length );

        for ( var i = 0; i < 3; i++ )
        {
            if ( i == missedIndx )
            {
                this.resultArray[0][i] = this.getRandomIndex( missIndices[ Math.floor( Math.random( ) * missIndices.length ) ], true );
            }
            else
            {
                this.resultArray[0][i] = this.getRandomIndex( highWinIndices[ sameIndex ], false );
            }
        }
    },

    setMultiplierAnimation : function( code )
    {
        var sprite = this.getChildByTag( FLOWER_TAG );

        if ( !sprite )
        {
            var r = this.slots[0].blurReels[0];

            var node = r.getParent();

            var moveW = this.slots[0].reelDimentions.x/2, moveH = this.slots[0].reelDimentions.y/2;

            sprite = new cc.Sprite( "#multiplier_1.png" );
            var posX = ( ( node.getPositionX() - moveW ) * this.slots[0].getScale() ) + this.slots[0].getPositionX();
            var posY = ( ( node.getPositionY() - moveH ) * this.slots[0].getScale() ) + this.slots[0].getPositionY();
            sprite.setPosition( cc.p( posX, posY ) );

            sprite.setTag( FLOWER_TAG );

            this.addChild( sprite, 10 );
        }
        switch ( code )
        {
            case 4:
                sprite.setSpriteFrame( "multiplier_2.png" );
            case 3:
                break;

            case 2:
                sprite.setSpriteFrame( "multiplier_3.png" );
            case 1:
            case 0:
                break;

            default:
                break;
        }

        sprite.setScale( 0 );

        var scaleTo = new cc.ScaleTo( 0.5, 1 );
        var rotateBy = new cc.RotateBy( 0.5, 360 );
        var spawn = new cc.Spawn( scaleTo, rotateBy );
        sprite.runAction( spawn );
    },

    setResult : function( indx, lane, sSlot )
    {
        var gap = 387.0;

        var j = 0;
        var animOriginY;

        var r = this.slots[0].physicalReels[lane];
        var arrSize = r.elementsVec.length;
        var spr;

        var spriteVec = new Array();

        animOriginY = gap * r.direction;
        spr = r.elementsVec[indx];
        if ( spr.eCode == ELEMENT_EMPTY )
        {
            gap = 285 * spr.getScale();
        }

        if ( indx == 0 )
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push(spr);

            spr.setPositionY( animOriginY );

            for (var i = 0; i < 2; i++)
            {
                spr = r.elementsVec[i];
                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (i + 1) * r.direction);
            }
        }
        else if (indx == arrSize - 1)
        {
            spr = r.elementsVec[0];
            spriteVec.push(spr);

            spr.setPositionY(animOriginY);
            for (var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i--)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction );
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push(spr);
            spr.setPositionY(animOriginY);

            for (var i = indx; i < indx + 2; i++)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }

        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            gap = -773.5;

            var eee;
            if (i < 2)
            {
                eee = spriteVec[i + 1];
            }
            if (i == 0)
            {
                var ee = spriteVec[i + 1];
                if ( Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) ) != 387)
                {
                    gap = -Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) * 2 * ee.getScale() );
                }
            }

            gap*=r.direction;
            var ease;

            if ( this.slots[sSlot].isForcedStop )
            {
                ease =  new cc.MoveTo( 0.15, cc.p( 0, e.getPositionY() + gap) ).easing( cc.easeBackOut() );
            }
            else
            {
                ease = new cc.MoveTo( 0.3, cc.p( 0, e.getPositionY() + gap ) ).easing( cc.easeBackOut() );
            }

            ease.setTag( MOVING_TAG );
            e.runAction(ease);
        }

        return spriteVec;
    },

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSBonusMachine;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    spin : function()
    {
        this.unschedule( this.updateWinCoin );

        if ( !this.haveFreeSpins )
        {
            this.deductBet( this.currentBet );
        }

        if (this.displayedScore != this.totalScore)
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;

        this.currentWin = 0;
        this.currentWinForcast = 0;

        this.startLightAnimation();

        this.startRolling();

        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    },

    setWinAnimation : function( animCode, screen )
    {
        var fire;
        for ( var i = 0; i < this.animNodeVec.length; i++ )
        {
            fire = this.animNodeVec[i];
            fire.removeFromParent( true );
        }
        this.animNodeVec = [];


        switch ( animCode )
        {
            case WIN_TAG:
            {
                var r = this.slots[0].blurReels[1];

                var node = r.getParent();

                var fgSprite = new cc.Sprite( "res/DJSBonusMachine/glow_low_winning.png" );
                fgSprite.setPosition( cc.p( this.vSize.width - fgSprite.getContentSize().width / 2, node.getPositionY() + this.slots[0].getPositionY() ) );
                this.scaleAccordingToSlotScreen( fgSprite );
                fgSprite.setOpacity( 0 );
                fgSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeIn( 0.25 ), new cc.FadeOut( 0.25 ) ) ) );
                this.addChild( fgSprite );
                this.animNodeVec.push( fgSprite );
            }
                break;

            case BIG_WIN_TAG:
            {
                var r = this.slots[0].blurReels[1];

                var node = r.getParent();

                var bgSprite = new cc.Sprite( "#glow_bg.png" );
                bgSprite.setPosition( cc.p( this.vSize.width - bgSprite.getContentSize().width / 2, node.getPositionY() + this.slots[0].getPositionY() ) );
                this.addChild( bgSprite );
                this.animNodeVec.push( bgSprite );


                var fgSprite = new cc.Sprite( "#glow_fg.png" );
                fgSprite.setPosition( bgSprite.getPosition() );
                fgSprite.setOpacity( 0 );
                fgSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeIn( 0.25 ), new cc.FadeOut( 0.25 ) ) ) );
                this.addChild( fgSprite );
                this.animNodeVec.push( fgSprite );

                var fgSprite2 = new cc.Sprite( "res/DJSBonusMachine/glow_fg1.png" );
                fgSprite2.setPosition( cc.p( this.vSize.width - fgSprite2.getContentSize().width / 2, node.getPositionY() + this.slots[0].getPositionY() ) );
                this.addChild( fgSprite2 );
                this.animNodeVec.push( fgSprite2 );

                this.scaleAccordingToSlotScreen( bgSprite );
                this.scaleAccordingToSlotScreen( fgSprite );
                this.scaleAccordingToSlotScreen( fgSprite2 );
            }
                break;

            default:
                break;
        }
    },
    startLightAnimation : function()
    {
        var sprite = this.getChildByTag( LIGHT_TAG );

        if ( !sprite )
        {
            for ( var i = 0; i < 3; i++ )
            {
                var r = this.slots[0].blurReels[i];

                var node = r.getParent();

                var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                diff*=( 2 - i );

                sprite = new cc.Sprite( "#light.png" );
                sprite.setPosition( cc.p( node.getPositionX() + this.slots[0].getPositionX() - diff, this.slots[0].getPositionY() + this.slots[0].reelDimentions.y * 0.5 * node.getScaleY() - sprite.getContentSize().height * 0.1 ) );

                sprite.setTag( LIGHT_TAG + i );
                this.addChild( sprite, 4 );
            }
        }

        for ( var i = 0; i < 3; i++ )
        {
            sprite = this.getChildByTag( LIGHT_TAG + i );
            sprite.setOpacity( 0 );
            sprite.runAction( new cc.Sequence( new cc.FadeIn( 0.2 ), new cc.FadeOut( 0.5 ) ) );
        }
    },

    setFreeSpinsAnimation : function(  )
    {
        var moveToTime = 0.75;
        var firstAnimTime = 0.5;
        var haltTime = 0.5;
        var sprite = new cc.Sprite( "res/DJSBonusMachine/FreeSpin_text.png" );

        var r = this.slots[0].blurReels[1];

        var node = r.getParent();

        sprite.setPosition( node.getPosition().x + this.slots[0].getPosition().x, node.getPosition().y + this.slots[0].getPosition().y );
        sprite.setScale( 0.0 );
        sprite.setOpacity( 0.0 );

        sprite.runAction( new cc.Sequence( new cc.Spawn( new cc.FadeIn( firstAnimTime ), new cc.ScaleTo( firstAnimTime, 1.0 ) ), new cc.DelayTime( haltTime ), new cc.callFunc( function(){//lambda

            var bottomBar = new cc.Sprite( "#bottom_bar.png" );
            bottomBar.setPosition(cc.p(209 + bottomBar.getContentSize().width / 2, this.vSize.height - 587 - bottomBar.getContentSize().height / 2));
            this.addChild( bottomBar, 2, 122 );


            var label = new cc.LabelTTF( this.freeSpinCount + " left", "Arimo-Bold", 25 );

            label.setColor( cc.color( 255, 255, 255 ) );
            label.setPosition( cc.p( bottomBar.getContentSize().width * 0.35, bottomBar.getContentSize().height * 0.5 ) );
            bottomBar.addChild( label, 1, bottomBar.getTag() );

            sprite.runAction( new cc.Sequence( new cc.Spawn( new cc.FadeOut( moveToTime ), new cc.ScaleTo( moveToTime, 2.0 ) ), new cc.callFunc( function(){//lambda
                sprite.setPosition( cc.p( cc.rectGetMinX( bottomBar.getBoundingBox() ) + sprite.getContentSize().width * 0.5 * 0.4, bottomBar.getPositionY() ) );
                sprite.setScale( 0.0 );
                sprite.runAction( new cc.Spawn( new cc.FadeIn( 0.5 ), new cc.ScaleTo( 0.5, 0.4 ) ) );

            }, this, sprite, bottomBar ) ) );

        }, this, sprite, moveToTime) ) );

        this.addChild( sprite, 6, 121 );
        return ( moveToTime + haltTime + firstAnimTime );
    }

});