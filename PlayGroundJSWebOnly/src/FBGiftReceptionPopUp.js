/**
 * Created by RNF-Mac11 on 6/19/17.
 */


FBGiftReceptionPopUp = cc.Layer.extend({

    bgSprite : null,

    scroll : null,

    selectedFriendsCountLabel : null,

    friendsVector : new Array(),

    selectedFriendsVector : new Array(),

    buttonsVector : new Array(),

    menuCB : function( pSender )
    {
        if( this.getLocalZOrder() == 0 )
        {
            return;
        }
        var delegate = AppDelegate.getInstance();

        var tickSprite = pSender.getChildByTag( TICK_IMAGE_TAG );

        switch ( pSender.getTag() )
        {
            case CLOSE_BTN_TAG:
                //this.removeFromParent( true );
                this.getParent().getChildByTag( 1 ).setLocalZOrder( 1 );
                //DPUtils.getInstance().setTouchSwallowing( 100, this.getParent().getChildByTag( 1 ).bgSprite.setLocalZOrder( 1 ) );
                this.setLocalZOrder( 0 );
                break;

            case SELECT_ALL_BTN:
                tickSprite.setVisible( !tickSprite.isVisible() );
                break;

            case INVITE_BTN_TAG:
                //this.sendInvitations();
                this.collectAllGifts();
                break;

            default:
                break;
        }
        delegate.jukeBox( S_BTN_CLICK );
    },
    collectAllGifts : function()
    {
        this.inviteItem.setVisible( false );
        if( this.friendsVector.length == 0 )
        {
            this.inviteItem.setEnabled( false );
            return;
        }
        if( !this.selectAllItem.getChildByTag( TICK_IMAGE_TAG ).isVisible() )
        {
            var length = this.friendsVector.length;

            for( var i = 0; i < this.buttonsVector.length; i++ )
            {
                var button = this.buttonsVector[i];
                button.setEnabled( false );
            }

            var index = 0;
            AppDelegate.getInstance().assignRewards( FB_GIFT_REWARDS_POINTS * length ,true);

            for ( var i = 0; i < this.friendsVector.length; i++ )
            {
                FacebookObj.deleteRequest( this.friendsVector[ i ].id );
                FacebookObj.receivedGiftsDetails.data.splice( i, 1 );
                this.friendsVector.splice( i, 1 );
                i--;
            }
        }
        else
        {
            this.selectedAll = true;

            var index = 0;
            //AppDelegate.getInstance().assignRewards( FB_GIFT_REWARDS_POINTS * this.friendsVector.length );
            var ids = "";
            var a = 0;
            for ( var i = 0; i < this.friendsVector.length; i++ )
            {
                var idString = this.friendsVector[ i ].from.id;

                if( this.selectAllItem.getChildByTag( TICK_IMAGE_TAG ).isVisible() && this.friendsVector[ index ].data != 0 ) {
                    ids = ids + idString + ",";
                }
                a++;

                if( a >= FB_MAX_REQUESTS - 1 )
                {
                    break;
                }

            }

            if( ids.length )
            {
                //cc.log( "#Dp id to send Return Gift are = " + ids );
                FacebookObj.sendGiftsToFewUsers( ids, "Return Gift", 0 );
            }
            else
            {
                var length = this.friendsVector.length;
                AppDelegate.getInstance().assignRewards( FB_GIFT_REWARDS_POINTS * length ,true);
                for( var i = 0; i < this.buttonsVector.length; i++ )
                {
                    var button = this.buttonsVector[i];
                    if( button.isEnabled() )
                    {
                        for( var j = 0; j < this.friendsVector.length; j++ )
                        {
                            if( this.friendsVector[j].from.name === button.getParent().getChildByTag( NAME_LABEL_TAG ).getString() )
                            {
                                button.setEnabled( false );
                                FacebookObj.deleteRequest( this.friendsVector[ j ].id );
                                FacebookObj.receivedGiftsDetails.data.splice( j, 1 );
                                this.friendsVector.splice( j, 1 );
                            }
                        }
                    }

                }
            }
        }
    },
    giveRewardsForInviting : false,//for assigning rewards on just inviting more than 10 Poeple

    touchEvent : function( pSender, type )
    {
        if( this.getLocalZOrder() == 0 )
        {
            return;
        }
        var button = pSender;

        switch ( type )
        {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                button.setEnabled( false );

                var index = 0;
                for( index = 0; index < this.friendsVector.length; index++ )
                {
                    if( this.friendsVector[index].from.name === button.getParent().getChildByTag( NAME_LABEL_TAG ).getString() )
                    {
                        break;
                    }
                }

                if( this.selectAllItem.getChildByTag( TICK_IMAGE_TAG ).isVisible() && this.friendsVector[ index ].data != 0 )
                {
                    FacebookObj.sendGiftsToFewUsers( this.friendsVector[ index ].from.id, "Return Gift", 0 );
                }



                AppDelegate.getInstance().assignRewards( FB_GIFT_REWARDS_POINTS,true );
                FacebookObj.deleteRequest( this.friendsVector[ index ].id );
                FacebookObj.receivedGiftsDetails.data.splice( index, 1 );
                this.friendsVector.splice( index, 1 );
                if( this.friendsVector.length == 0 )
                {
                    this.inviteItem.setVisible( false );
                    this.inviteItem.setEnabled( false );
                }
                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;

            default:
                break;
        }
    },
    selectedAll : false,
    giftCollectionRewards : function()
    {
        var length = FB_MAX_REQUESTS;
        if( this.friendsVector.length <= FB_MAX_REQUESTS )
        {
            length = this.friendsVector.length;
        }
        if( this.selectedAll )
        {
            for( var i = 0; i < this.buttonsVector.length; i++ )
            {
                var button = this.buttonsVector[i];
                button.setEnabled( false );
            }

            var index = 0;
            AppDelegate.getInstance().assignRewards( FB_GIFT_REWARDS_POINTS * length,true );
            var ids = "";
            var a = 0;
            for ( var i = 0; i < this.friendsVector.length; i++ )
            {
                var idString = this.friendsVector[ i ].from.id;

                if( this.selectAllItem.getChildByTag( TICK_IMAGE_TAG ).isVisible() && this.friendsVector[ index ].data != 0 ) {
                    ids = ids + idString + ",";
                }
                a++;
                FacebookObj.deleteRequest( this.friendsVector[ i ].id );
                FacebookObj.receivedGiftsDetails.data.splice( i, 1 );
                this.friendsVector.splice( i, 1 );
                if( a >= FB_MAX_REQUESTS - 1 )
                {
                    //cc.log( "FB_MAX_REQUESTS = " + a );
                    break;
                }
                i--;
            }
        }
        else
        {

        }
    },
    selectAllItem : null,
    inviteItem : null,
    makeScrollVIew : function( data )
    {
        this.scroll = new ccui.ScrollView();

        var scollFrameSize = cc.size( this.bgSprite.getContentSize().width, this.bgSprite.getContentSize().height * 0.5 );
        this.scroll.setDirection( ccui.ScrollView.DIR_VERTICAL );
        this.scroll.setContentSize( scollFrameSize );
        this.scroll.setScrollBarAutoHideEnabled( false )
        this.scroll.setScrollBarOpacity(255);
        this.scroll.setScrollBarColor( cc.color( 255, 255, 255, 255 ) );
        this.scroll.setBounceEnabled( true );
        this.scroll.setPosition( cc.p( 0, this.bgSprite.getContentSize().height * 0.2 ) );

        var height = ( data.data.length + 1.5 ) * new cc.Sprite( "#giftPop/bar.png" ).getContentSize().height;

        if( height < scollFrameSize.height )
        {
            height = scollFrameSize.height;
        }
        this.scroll.setInnerContainerSize( cc.size( scollFrameSize.width, height ) );

        this.bgSprite.addChild( this.scroll );

        var pos = height - new cc.Sprite( "#giftPop/bar.png" ).getContentSize().height * 0.5;
        var tag = 0;

        //for (var obj in data)
        //cc.log( "#Dp data.length = " + data.data.length );
        //cc.log( "#Dp data = " + JSON.stringify(data) );
        for( var ii = 0; ii < data.data.length; ii++)
        {
            var obj = data.data[ii];
            this.friendsVector.push( obj );
            if ( obj )
            {
                var sprite = new cc.Sprite( "#giftPop/bar.png" );

                sprite.setTag( tag );
                tag++;


                sprite.setPosition( cc.p( this.bgSprite.getContentSize().width / 2, pos ) );
                pos-=sprite.getContentSize().height * 1.05;


                var first_name = obj.from[ "name" ];
                var heading = new CustomLabel( first_name, "GEORGIAB", 30, cc.size( 300, 20 ) );
                heading.setPosition( sprite.getContentSize().width * 0.5, sprite.getContentSize().height * 0.5 );
                //heading.setColor( cc.color( 255, 255, 0, 255 ) );
                heading.setTag( NAME_LABEL_TAG );
                sprite.addChild( heading, 2 );

                var url = "https://graph.facebook.com/" + obj.from["id"] + "/picture?width=55&height=55";

                this.addImageOnParent( url, sprite );

                var tickSprite = new ccui.Button( "giftPop/collect_button.png", "giftPop/collect_button.png", "", ccui.Widget.PLIST_TEXTURE );//new cc.Sprite( "#giftPop/send_button.png.png" );
                tickSprite.setPosition( cc.p( sprite.getContentSize().width * 0.85, sprite.getContentSize().height / 2 ) );
                sprite.addChild( tickSprite, 0, TICK_IMAGE_TAG );
                this.buttonsVector.push( tickSprite );
                tickSprite.addTouchEventListener( this.touchEvent, this );
                this.scroll.addChild( sprite );
            }
        }

        this.selectedFriendsCountLabel = new cc.LabelTTF( this.selectedFriendsVector.length + "/" + data.length, "GEORGIAB", 40 );
        this.selectedFriendsCountLabel.setPosition( cc.p( this.bgSprite.getContentSize().width * 0.5, this.bgSprite.getContentSize().height * 0.7 ) );
        this.bgSprite.addChild( this.selectedFriendsCountLabel );

        this.selectedFriendsCountLabel.setVisible( false );

        this.selectAllItem = new cc.MenuItemSprite( new cc.Sprite( "#giftPop/check_box_bg.png" ), new cc.Sprite( "#giftPop/check_box_bg.png" ), this.menuCB, this );

        this.selectAllItem.setPosition( this.bgSprite.getContentSize().width * 0.2, this.bgSprite.getContentSize().height * 0.08 - 1 );
        this.selectAllItem.setTag( SELECT_ALL_BTN );

        this.selectAllItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );

        var tickSprite = new cc.Sprite( "#giftPop/check.png" );
        tickSprite.setPosition( cc.p( this.selectAllItem.getContentSize().width * 0.1, this.selectAllItem.getContentSize().height / 2 ) );
        tickSprite.setTag( TICK_IMAGE_TAG );
        this.selectAllItem.addChild( tickSprite );


        this.inviteItem = new cc.MenuItemSprite( new cc.Sprite( "#giftPop/collect_all.png" ), new cc.Sprite( "#giftPop/collect_all.png" ), this.menuCB, this );

        this.inviteItem.setPosition( this.bgSprite.getContentSize().width * 0.75, this.selectAllItem.getPositionY() );
        this.inviteItem.setTag( INVITE_BTN_TAG );

        this.inviteItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );

        var menu = new cc.Menu( this.selectAllItem, this.inviteItem, null );

        menu.setPosition( 0, 0 );
        this.bgSprite.addChild( menu );

        this.bgSprite.removeChildByTag( LOADER_TAG );

        /*var termText = new cc.LabelTTF( "* You will receive " + FB_INVITATION_REWARDS + " credits for every friend who successfully joins Double Jackpot Slots!", "GEORGIAB", 15 );
         termText.setPosition( cc.p( this.bgSprite.getContentSize().width / 2, cc.rectGetMinY(this.selectAllItem.getBoundingBox()) - termText.getContentSize().height * 0.75 ) );
         this.bgSprite.addChild( termText );*/

        //DPUtils.getInstance().setTouchSwallowing( 100, this );
    },

    addImageOnParent: function (appIconURL,parentSprite){
        cc.loader.loadImg(appIconURL, {isCrossOrigin: true},function (err, texture) {
            var sprite = new cc.Sprite(texture);
            sprite.setPosition( cc.p( parentSprite.getContentSize().width * 0.11, parentSprite.getContentSize().height * 0.5 ) );
            parentSprite.addChild(sprite,10);
        },parentSprite);
    },

    sendInvitations : function()
    {
        var valueMapOfFriendIds = new Map();
        var ids = "";
        if ( this.selectedFriendsVector.length )
        {
            var a = 0;
            for ( var i = 0; i < this.selectedFriendsVector.length; i++ )
            {
                //cc.log( "i = " + i );
                var sdata = this.selectedFriendsVector[i];
                var idString = sdata["id"];

                valueMapOfFriendIds.set( ("" + a ), idString );
                ids = ids + idString + ",";
                this.selectedFriendsVector.splice( i , 1 );
                i--;
                a++;
                if( a >= FB_MAX_REQUESTS - 1 )
                {
                    //cc.log( "FB_MAX_REQUESTS = " + a );
                    break;
                }
            }

            //if (valueMapOfFriendIds.length > 0) {
            //FacebookObj.sendGiftsToFewUsers(ids, "Gifts form Dp", 10);
            //cc.log( "ids.length = " + ids.length );
            if( ids.length == 0 )
            {
                //this.removeFromParent(true);
            }
            //}

        }
        else
        {
            //cc.log( "in here" );
            FacebookObj.requestedOnce = false;
            //this.removeFromParent( true );
            //ServerCommunicator.getInstance().SCUpdateData();
        }
    },

    ctor:function ( code ) {
        this._super( );

        var vSize = cc.winSize;

        this.bgSprite = new cc.Sprite( "#giftPop/bg.png" );
        this.bgSprite.setPosition( vSize.width / 2, vSize.height / 2 );
        //this.bgSprite.retain();
        this.addChild( this.bgSprite );

        var tabSprite = new cc.Sprite( "#giftPop/collect_gifts.png" );
        tabSprite.setPosition( this.bgSprite.getContentSize().width / 2, this.bgSprite.getContentSize().height * 0.55 );
        this.bgSprite.addChild( tabSprite );

        var delegate = AppDelegate.getInstance();

        if( FacebookObj.receivedGiftsDetails && FacebookObj.receivedGiftsDetails.data && FacebookObj.receivedGiftsDetails.data.length )
        {
            this.makeScrollVIew( FacebookObj.receivedGiftsDetails );
        }
        else
        {
            var messageTest = new cc.Sprite( "#giftPop/no_gift.png" );
            messageTest.setPosition( cc.p( this.bgSprite.getContentSize().width * 0.5, this.bgSprite.getContentSize().height * 0.5 ) );
            this.bgSprite.addChild( messageTest );
        }

        var headingSprite = new ccui.Button( "giftPop/send_gift.png", "giftPop/send_gift.png", "", ccui.Widget.PLIST_TEXTURE );
        headingSprite.setPosition( cc.p( headingSprite.getContentSize().width * 0.65, this.bgSprite.getContentSize().height - headingSprite.getContentSize().height * 1.08 ) );
        this.bgSprite.addChild( headingSprite );
        headingSprite.addTouchEventListener( this.touchEvent2, this );

        var closeItem = new cc.Sprite( "#giftPop/collect_gift.png" );//new cc.MenuItemImage( "#giftPop/collect_gift.png", "res/lobby/buyPage/close_btn.png", this.menuCB, this );

        closeItem.setPosition( this.bgSprite.getContentSize().width * 0.56, headingSprite.getPositionY() );
        closeItem.setTag( CLOSE_BTN_TAG );

        this.bgSprite.addChild( closeItem );

        //this.bgSprite.release();

        return true;
    },
    touchEvent2 : function( pSender, type )
    {
        var button = pSender;

        switch ( type )
        {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                this.getParent().getChildByTag( 1 ).setLocalZOrder( 1 );
                this.setLocalZOrder( 0 );
                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;

            default:
                break;
        }
    },
    handleScroll : function( event )
    {
        DPUtils.getInstance().scrolledAlready = true;

        var self = event.getCurrentTarget();

        if( !self.getChildByTag( 2 ).scroll )
        {
            return;
        }

        var nMoveY = event.getScrollY() * 0.5;
        //if( !isMAC )//Change 17 May
        {
            nMoveY = -nMoveY;
        }
        //cc.log( str );

        var inner = self.getChildByTag( 2 ).scroll.getInnerContainer();
        var curPos  = inner.getPosition();
        var nextPos = cc.p(curPos.x, curPos.y + nMoveY);

        // prevent scrolling past beginning
        if (nextPos.y > 0)
        {
            //inner.setPosition(cc.p(0, nextPos.y));
            self.getChildByTag( 2 ).scroll._startAutoScrollToDestination(cc.p(nextPos.x, 0), 0.5, true);
            return;
        }

        //auto ws = Director::getInstance()->getWinSize();
        var size = self.getChildByTag( 2 ).scroll.getContentSize();
        var innerSize = inner.getContentSize();
        var topMostY = size.height - innerSize.height;

        // prevent scroll past end
        if (nextPos.y < topMostY)
        {
            self.getChildByTag( 2 ).scroll._startAutoScrollToDestination(cc.p(nextPos.x, topMostY), 0.5, true);
            return;
        }

        self.getChildByTag( 2 ).scroll._startAutoScrollToDestination(nextPos, 0.5, true);
    }

});