/**
 * Created by RNF-Mac11 on 2/13/17.
 */



var reelsRandomizationArrayDJSDiamondsForever =
    [
        [// 85% payout
            70, 20,65,25 , 20, 20,
            50, 1, 50, 100,30, 85,
            30, 80, 70 ,2, 25,150,
            30, 50, 70, 3, 30, 60,
            30, 70, 50, 5, 10,20,
            50, 5, 5, 40, 10, 5
        ],
        [//92 %
            70, 20,65,25 , 20, 20,
            50, 2, 50, 100,30, 85,
            30, 80, 75 ,3, 25,150,
            30, 50, 70, 3, 30, 60,
            30, 70, 50, 5, 10,20,
            50, 5, 5, 42, 10, 4
        ],
        [// 95-97% payout
            70, 20,65,25 , 20, 20,
            50, 4, 50, 100,30, 85,
            30, 80, 70 ,3, 25,150,
            30, 50, 70, 3, 30, 60,
            30, 70, 50, 5, 10,20,
            50, 5, 5, 40, 10, 5
        ],
        [//140% payout
            100, 20, 100,25 , 20, 20,
            100, 10, 30, 150, 30, 135,
            30, 135, 100 ,7, 30, 200,
            30, 50, 100, 7, 30, 60,
            30, 70, 30, 5, 20,20,
            100, 8, 10, 50, 20, 5
        ],
        [//300% payout
            100, 20, 100,25 , 20, 20,
            100, 10, 30, 150, 30, 135,
            30, 135, 100 ,7, 30, 200,
            30, 50, 100, 7, 30, 60,
            30, 70, 30, 5, 20,20,
            100, 8, 10, 50, 20, 5
        ]
    ];

var DJSDiamondsForever = GameScene.extend({

    lastSlidingLane : 0,
    reelsArrayDJSDiamondsForever:[],
    ctor:function ( level_index ) {
        this.reelsArrayDJSDiamondsForever = [
            /*1*/ELEMENT_EMPTY,/*2*/ELEMENT_D_BAR_1,/*3*/ELEMENT_EMPTY,/*4*/ELEMENT_D_BAR_3,/*5*/ELEMENT_EMPTY,
            /*6*/ELEMENT_I_D_BAR_2,/*7*/ELEMENT_EMPTY,/*8*/ELEMENT_TRIPLE_D,/*9*/ELEMENT_EMPTY,/*10*/ELEMENT_BAR_1,
            /*11*/ELEMENT_EMPTY,/*12*/ELEMENT_BAR_2,/*13*/ELEMENT_EMPTY,/*14*/ELEMENT_BAR_3,/*15*/ELEMENT_EMPTY,
            /*16*/ELEMENT_PENTA_D,/*17*/ELEMENT_EMPTY,/*18*/ELEMENT_7_PINK,/*19*/ELEMENT_EMPTY,/*20*/ELEMENT_BAR_3,
            /*21*/ELEMENT_EMPTY,/*22*/ELEMENT_PENTA_D,/*23*/ELEMENT_EMPTY,/*24*/ELEMENT_BAR_2,/*25*/ELEMENT_EMPTY,
            /*26*/ELEMENT_BAR_1,/*27*/ELEMENT_EMPTY,/*28*/ELEMENT_I_D_BAR_2,/*29*/ELEMENT_EMPTY,/*30*/ ELEMENT_7_PINK,
            /*31*/ELEMENT_EMPTY,/*32*/ELEMENT_TRIPLE_D,/*33*/ ELEMENT_EMPTY,/*34*/ ELEMENT_7_PINK,/*35*/ELEMENT_EMPTY,
            /*36*/ELEMENT_D_BAR_1
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSDiamondsForever;
        this.physicalArrayCheck= this.reelsArrayDJSDiamondsForever;
        this._super( level_index );

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;


    },

    calculatePayment : function( doAnimate )
    {
        var payMentArray = [
            /*0*/    1500,// 5x 5x 5x

            /*1*/    300, // 3x 3x 3x

            /*2*/    40,  // 7 7 7 Pink

            /*3*/    30,  // triple Bar with any diamond

            /*4*/    20,  // double Bar with any diamond

            /*5*/    10,   // single Bar with any diamond

            /*6*/    1   // any bar with any diamond
        ];

        this.changeResults();
        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        for (var i = 0; i < this.slots[0].resultReel.length; i++)
        {
            tmpVector = this.slots[0].resultReel[i];
            if (tmpVector.length == 3)
            {
                tmpVector2.push(tmpVector[1]);
            }
            else
            {
                tmpVector2.push(tmpVector[0]);
            }
        }

        this.lineVector.push(tmpVector2);

        var winScenario = 6;
        var lineLength = 3;
        var pay = 0;
        var tmpPay = 1;
        var count5X = 0;
        var count3X = 0;
        var countWild = 0;
        var countGeneral = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var i, j, k;
        var e = null;
        var pivotElm = null;
        var pivotElm2 = null;
        var tmpVec;


        for (i = 0; i < this.totalLines; i++)
        {
            tmpVec = this.lineVector[i];

            for (k = 0; k < winScenario; k++)
            {
                var breakLoop = false;
                countGeneral = 0;
                switch (k)
                {
                    case 0://for all combination with wild
                        for (j = 0; j < lineLength; j++)
                        {
                            if (this.reelsArrayDJSDiamondsForever[this.resultArray[i][j]] != -1)
                            {

                                switch (this.reelsArrayDJSDiamondsForever[this.resultArray[i][j]])
                                {
                                    case ELEMENT_PENTA_D:
                                        count5X++;
                                        tmpPay*=5;
                                        countWild++;
                                        if ( doAnimate ) {
                                            e = tmpVec[j];
                                        }

                                        if (e)
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_TRIPLE_D:
                                        count3X++;
                                        tmpPay*=3;
                                        countWild++;
                                        if ( doAnimate ) {
                                            e = tmpVec[j];
                                        }
                                        if (e)
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        switch (pivotElement)
                                        {
                                            case -1:
                                                pivotElement = this.reelsArrayDJSDiamondsForever[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate ) {
                                                    e = tmpVec[j];
                                                }
                                                if (e)
                                                {
                                                    pivotElm = e;
                                                }
                                                break;

                                            default:
                                                pivotElement2 = this.reelsArrayDJSDiamondsForever[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate ) {
                                                    e = tmpVec[j];
                                                }
                                                if (e)
                                                {
                                                    pivotElm2 = e;
                                                }
                                                break;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }

                        if (j == 3)
                        {
                            if (count5X == 3)
                            {
                                pay = payMentArray[0];
                                breakLoop = true;
                            }
                            else if (count3X == 3)
                            {
                                pay = payMentArray[1];
                                breakLoop = true;
                            }
                            else if(countWild >= 2)
                            {
                                breakLoop = true;

                                switch (pivotElement)
                                {
                                    case ELEMENT_7_PINK:
                                        pay = tmpPay * payMentArray[2];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }

                                        break;

                                    case ELEMENT_I_D_BAR_3:
                                    case ELEMENT_D_BAR_3:
                                    case ELEMENT_BAR_3:
                                        pay = tmpPay * payMentArray[3];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_I_D_BAR_2:
                                    case ELEMENT_D_BAR_2:
                                    case ELEMENT_BAR_2:
                                        pay = tmpPay * payMentArray[4];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_I_D_BAR_1:
                                    case ELEMENT_D_BAR_1:
                                    case ELEMENT_BAR_1:
                                        pay = tmpPay * payMentArray[5];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        pay = tmpPay;
                                        if (pivotElm)
                                        {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;
                                }
                            }
                            else if(countWild == 1)
                            {
                                breakLoop = true;

                                if (pivotElement == pivotElement2)
                                {
                                    switch (pivotElement)
                                    {
                                        case ELEMENT_7_PINK:
                                            pay = tmpPay * payMentArray[2];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }

                                            break;

                                        case ELEMENT_I_D_BAR_3:
                                        case ELEMENT_D_BAR_3:
                                        case ELEMENT_BAR_3:
                                            pay = tmpPay * payMentArray[3];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_I_D_BAR_2:
                                        case ELEMENT_D_BAR_2:
                                        case ELEMENT_BAR_2:
                                            pay = tmpPay * payMentArray[4];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_I_D_BAR_1:
                                        case ELEMENT_D_BAR_1:
                                        case ELEMENT_BAR_1:
                                            pay = tmpPay * payMentArray[5];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        default:
                                            pay = tmpPay;
                                            break;
                                    }
                                }
                                else
                                {
                                    if ((pivotElement == ELEMENT_I_D_BAR_1 || pivotElement == ELEMENT_D_BAR_1 || pivotElement == ELEMENT_BAR_1) && (pivotElement2 == ELEMENT_I_D_BAR_1 || pivotElement2 == ELEMENT_D_BAR_1 || pivotElement2 == ELEMENT_BAR_1))
                                    {
                                        pay = tmpPay * payMentArray[5];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if ((pivotElement == ELEMENT_I_D_BAR_2 || pivotElement == ELEMENT_D_BAR_2 || pivotElement == ELEMENT_BAR_2) && (pivotElement2 == ELEMENT_I_D_BAR_2 || pivotElement2 == ELEMENT_D_BAR_2 || pivotElement2 == ELEMENT_BAR_2))
                                    {
                                        pay = tmpPay * payMentArray[4];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if ((pivotElement == ELEMENT_I_D_BAR_3 || pivotElement == ELEMENT_D_BAR_3 || pivotElement == ELEMENT_BAR_3) && (pivotElement2 == ELEMENT_I_D_BAR_3 || pivotElement2 == ELEMENT_D_BAR_3 || pivotElement2 == ELEMENT_BAR_3))
                                    {
                                        pay = tmpPay * payMentArray[3];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if((pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_BAR_3) &&
                                        (pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_BAR_3))
                                    {
                                        pay = tmpPay * payMentArray[6];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if((pivotElement >= ELEMENT_D_BAR_1 && pivotElement <= ELEMENT_D_BAR_3) &&
                                        (pivotElement2 >= ELEMENT_D_BAR_1 && pivotElement2 <= ELEMENT_D_BAR_3))
                                    {
                                        pay = tmpPay * payMentArray[6];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if((pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_I_D_BAR_3) &&
                                        (pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_I_D_BAR_3))
                                    {
                                        pay = tmpPay * payMentArray[6];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else
                                    {
                                        pay = tmpPay;
                                    }
                                }
                            }
                        }
                        break;

                    case 1: // 7 7 7 pink
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSDiamondsForever[this.resultArray[i][j]])
                            {
                                case ELEMENT_7_PINK:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[2];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }

                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 2: // 3Bar 3Bar 3Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSDiamondsForever[this.resultArray[i][j]])
                            {
                                case ELEMENT_I_D_BAR_3:
                                case ELEMENT_D_BAR_3:
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[3];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 3: // 2Bar 2Bar 2Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSDiamondsForever[this.resultArray[i][j]])
                            {
                                case ELEMENT_I_D_BAR_2:
                                case ELEMENT_D_BAR_2:
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[4];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 4: // Bar Bar Bar
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSDiamondsForever[this.resultArray[i][j]])
                            {
                                case ELEMENT_I_D_BAR_1:
                                case ELEMENT_D_BAR_1:
                                case ELEMENT_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[5];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++) {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    case 5: // any Bar and Diamond Bar combo
                        for (j = 0; j < lineLength; j++)
                        {
                            switch (this.reelsArrayDJSDiamondsForever[this.resultArray[i][j]])
                            {
                                case ELEMENT_I_D_BAR_1:
                                case ELEMENT_I_D_BAR_2:
                                case ELEMENT_I_D_BAR_3:
                                case ELEMENT_D_BAR_1:
                                case ELEMENT_D_BAR_2:
                                case ELEMENT_D_BAR_3:
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (countGeneral == 3)
                        {
                            pay = payMentArray[6];
                            breakLoop = true;
                            for (var l = 0; l < countGeneral; l++)
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if (e)
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

                if (breakLoop)
                {
                    break;
                }
            }
        }

        if ( doAnimate )
        {
            this.currentWin = this.currentBet * pay;
            if (this.currentWin > 0)
            {
                this.addWin = this.currentWin / 10;
                this.setCurrentWinAnimation();
            }

            this.checkAndSetAPopUp();
        }
        else
        {
            this.currentWinForcast = this.currentBet * pay;
        }
    },

    changeResults : function()
    {
        var e = null;
        var uE = null;
        var lE = null;

        var tmpVec = this.slots[0].resultReel;
        var tmpVec3 = [];


        for (var i = 0; i < tmpVec.length; i++)
        {
            var spriteVec = tmpVec[i];
            e = spriteVec[1];

            var tmpVec2 = spriteVec;

            if (e.eCode == ELEMENT_EMPTY)
            {
                uE = spriteVec[0];
                lE = spriteVec[2];


                if (uE.eCode == ELEMENT_I_D_BAR_1 || uE.eCode == ELEMENT_I_D_BAR_2 || uE.eCode == ELEMENT_I_D_BAR_3 || uE.eCode == ELEMENT_3X || uE.eCode == ELEMENT_2X)
                {
                    e = uE;
                    this.resultArray[0][i] = e.elementIndex;
                    tmpVec2 = [];
                    tmpVec2.push(e);
                }
                else if (lE.eCode == ELEMENT_D_BAR_1 || lE.eCode == ELEMENT_D_BAR_2 || lE.eCode == ELEMENT_D_BAR_3 || lE.eCode == ELEMENT_3X || lE.eCode == ELEMENT_2X)
                {
                    e = lE;
                    this.resultArray[0][i] = e.elementIndex;
                    tmpVec2 = [];
                    tmpVec2.push(e);
                }
            }

            tmpVec3.push(tmpVec2);
        }
        this.slots[0].resultReel = tmpVec3;
    },
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSDiamondsForever[i]=this.physicalReelElements[idx];
                i++;
            }
        }
    },
    changeTotalProbabilities : function()
    {
        this.totalProbalities = 0;
        if ( ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            //this.extractArrayFromSring();
            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[this.easyModeCode][i];
            }
        }
        else
        {
            for ( var i = 0; i < 36; i++ )
            {
                this.probabilityArray[this.easyModeCode][i] = reelsRandomizationArrayDJSDiamondsForever[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSDiamondsForever[this.easyModeCode][i];
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
        if (((this.reelsArrayDJSDiamondsForever[this.resultArray[0][0]] == ELEMENT_PENTA_D || this.reelsArrayDJSDiamondsForever[this.resultArray[0][0]] == ELEMENT_TRIPLE_D) &&
            (this.reelsArrayDJSDiamondsForever[this.resultArray[0][1]] == ELEMENT_PENTA_D || this.reelsArrayDJSDiamondsForever[this.resultArray[0][1]] == ELEMENT_TRIPLE_D)) ||
            ((this.reelsArrayDJSDiamondsForever[this.resultArray[0][0]] == ELEMENT_PENTA_D || this.reelsArrayDJSDiamondsForever[this.resultArray[0][0]] == ELEMENT_TRIPLE_D) &&
            (this.reelsArrayDJSDiamondsForever[this.resultArray[0][1]] == ELEMENT_7_PINK )) ||
            ((this.reelsArrayDJSDiamondsForever[this.resultArray[0][1]] == ELEMENT_PENTA_D || this.reelsArrayDJSDiamondsForever[this.resultArray[0][1]] == ELEMENT_TRIPLE_D) &&
            (this.reelsArrayDJSDiamondsForever[this.resultArray[0][0]] == ELEMENT_7_PINK )))
        {
            if ( !this.slots[0].isForcedStop )
            {
                this.schedule( this.reelStopper, 4.8 );

                this.delegate.jukeBox( S_EXCITEMENT );

                var glowSprite = new cc.Sprite( glowStrings[0] );

                if ( glowSprite )
                {
                    var r = this.slots[0].physicalReels[ this.slots[0].physicalReels.length - 1 ];

                    glowSprite.setPosition( r.getParent().getPosition().x + this.slots[0].getPosition().x, r.getParent().getPosition().y + this.slots[0].getPosition().y );

                    //this.scaleAccordingToSlotScreen( glowSprite );

                    this.addChild( glowSprite, 5 );
                    this.animNodeVec.push( glowSprite );

                    glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ) ) );
                }
            }
        }
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
    },

    generateResult : function( scrIndx )
    {
        for ( var i = 0; i < this.slots[scrIndx].displayedReels.length; i++ )
        {
            //generate:
            var num = parseInt( this.prevReelIndex[i] );

            var tmpnum = 0;

            while ( num == parseInt( this.prevReelIndex[i] ) )
            {
                num = Math.floor( Math.random() * this.totalProbalities );

            }
            this.prevReelIndex[i] = num;
            this.resultArray[0][i] = num;

            for (var j = 0; j < 36; j++)
            {
                tmpnum+=this.probabilityArray[this.easyModeCode][j];

                if (tmpnum >= this.resultArray[0][i])
                {
                    this.resultArray[0][i] = j;
                    break;
                }
            }
            //console.log( " rand =  " + num + " index = " + this.resultArray[0][i] );
        }

        /*
         int highPay[][3] = {
         {15, 21, 15}, //2x 5x 2x
         {15, 19, 15}, //2x 4x 2x
         {15, 17, 15}, //2x 3x 2x
         {7, 7, 7},    //Triple 7 Red
         {5, 5, 5},    //Triple 7 Yellow
         {29, 29, 29}, //Triple 7 Green
         {1, 1, 1},    //Triple 7 Bar
         {31, 31, 31}, //Triple 3Bar
         {27, 27, 27}, //Triple 2Bar
         {23, 23, 23}, //Triple Bar
         };*/
        /*/HACK
         this.resultArray[0][0] = 0;
         this.resultArray[0][1] = 1;
         this.resultArray[0][2] = 2;
         //*///HACK

        this.calculatePayment( false );
        this.checkForFalseWin();

    },

    getPostStopSliding : function()
    {
        var isSliding = false;
        var indx = 0;

        for (var i = 0; i < this.slots[0].resultReel.length; i++)
        {
            var spriteVec = this.slots[0].resultReel[i];
            var E = spriteVec[1];

            if (E.eCode == ELEMENT_EMPTY)
            {
                var uE = spriteVec[0];
                var lE = spriteVec[2];

                if (uE.eCode == ELEMENT_I_D_BAR_1 || uE.eCode == ELEMENT_I_D_BAR_2 || uE.eCode == ELEMENT_I_D_BAR_3  || uE.eCode == ELEMENT_3X || uE.eCode == ELEMENT_2X)//moving up
                {
                    this.isRolling = true;
                    isSliding = true;
                    this.setPostStopSliding(true, i, indx);
                    indx++;
                }
                else if (lE.eCode == ELEMENT_D_BAR_1 || lE.eCode == ELEMENT_D_BAR_2 || lE.eCode == ELEMENT_D_BAR_3 || lE.eCode == ELEMENT_3X || lE.eCode == ELEMENT_2X)
                {
                    this.isRolling = true;
                    isSliding = true;
                    this.setPostStopSliding(false, i, indx);
                    indx++;
                }
            }
        }

        return isSliding;
    },

    postStopAnimCB : function(obj)
    {
        if (obj.getTag() == this.lastSlidingLane + 2)
        {
            this.isRolling = false;
            this.calculatePayment( true );

            if (this.isAutoSpinning)
            {
                if (this.currentWin > 0)
                {
                    this.scheduleOnce(this.autoSpinCB, 1.5);
                }
                else
                {
                    this.scheduleOnce(this.autoSpinCB, 1.0);
                }
            }
        }
        obj.setTag(-1);

    },

    postStopSoundCB : function(obj)
    {
        this.delegate.jukeBox(S_SLIDE_1 + obj.getTag());
        obj.removeFromParent(true);
    },

    rollUpdator : function(dt)
    {
        var i;
        for (i = 0; i < this.totalSlots; i++)
        {
            this.slots[i].updateSlot(dt);
        }

        for (i = 0; i < this.totalSlots; i++)
        {
            if (this.slots[i].isSlotRunning())
            {
                break;
            }
        }

        if (i >= this.totalSlots)
        {
            this.isRolling = false;
            this.unschedule(this.rollUpdator);

            if (!this.getPostStopSliding())
            {
                this.calculatePayment( true );
                if (this.isAutoSpinning)
                {
                    if (this.currentWin > 0)
                    {
                        this.scheduleOnce(this.autoSpinCB, 1.5);
                    }
                    else
                    {
                        this.scheduleOnce(this.autoSpinCB, 1.0);
                    }
                }
            }
        }
    },

    setFalseWin : function()
    {
        var highWinIndices = [ 3, 13 ];
        var missIndices = [ 13, 15, 31 ];

        var missedIndx = Math.floor( Math.random() * 3 );

        if( missedIndx == 2 )
        {
            if( Math.floor( Math.random() * 10 ) == 3 )
            {
            }
            else
            {
                missedIndx = Math.floor( Math.random() * 2 );
            }
        }

        var sameIndex = Math.floor( Math.random( ) * highWinIndices.length );

        for ( var i = 0; i < 3; i++ )
        {
            if ( i == missedIndx )
            {
                this.resultArray[0][i] = this.getRandomIndex( missIndices[ Math.floor( Math.random( ) * missIndices.length ) ], true );
            }
            else
            {
                this.resultArray[0][i] = this.getRandomIndex( highWinIndices[ sameIndex ], false );
            }
        }
    },

    setPostStopSliding : function(isUpward, lane, indx)
{
    var time = 0.75;
    var waitTime = 0.5;

    if (indx > 0)
    {
        waitTime = (waitTime + time) * indx;

        var node = new cc.Node();
        node.setTag(lane);
        node.runAction( new cc.Sequence( new cc.DelayTime(waitTime - 0.5), new cc.callFunc( this.postStopSoundCB, this) ));
        this.addChild(node);
    }

    this.lastSlidingLane = lane;

    var spriteVec = this.slots[0].resultReel[lane];

    if (isUpward)
    {
        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction(new cc.FadeIn(0.2));
            var gap = 1547 * 0.275 * 0.5;
            if (i == 0)
            {
                var ee = spriteVec[i + 1];
                if (Math.abs(e.getPositionY() - ee.getPositionY()) != 387)
                {
                    gap = (Math.abs(e.getPositionY() - ee.getPositionY()) * ee.getScale() ) + 20;
                }
            }
            e.runAction( new cc.Sequence( new cc.DelayTime(waitTime), new cc.MoveBy(time, cc.p(0, 183)).easing(cc.easeBackOut()), new cc.callFunc(this.postStopAnimCB, this) ) );
            e.setTag(lane + i);
        }
        if (indx == 0)
            this.delegate.jukeBox(S_SLIDE_1 + lane);
    }
    else
    {
        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            var gap = -1547 * 0.237 * 0.5;

            e.runAction(new cc.Sequence(new cc.DelayTime(waitTime), new cc.MoveBy(time, cc.p(0, gap)).easing(cc.easeBackOut()), new cc.callFunc(this.postStopAnimCB, this)));
            e.setTag(lane + i);

        }
        if (indx == 0)
            this.delegate.jukeBox(S_SLIDE_1 + lane);
    }


},

    setResult : function( indx, lane, sSlot )
    {
        var gap = 387.0;

        var j = 0;
        var animOriginY;

        var r = this.slots[0].physicalReels[lane];
        var arrSize = r.elementsVec.length;
        var spr;

        var spriteVec = new Array();

        animOriginY = gap * r.direction;
        spr = r.elementsVec[indx];
        if ( spr.eCode == ELEMENT_EMPTY )
        {
            gap = 285 * spr.getScale();
        }

        if ( indx == 0 )
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push(spr);

            spr.setPositionY( animOriginY );

            for (var i = 0; i < 2; i++)
            {
                spr = r.elementsVec[i];
                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (i + 1) * r.direction);
            }
        }
        else if (indx == arrSize - 1)
        {
            spr = r.elementsVec[0];
            spriteVec.push(spr);

            spr.setPositionY(animOriginY);
            for (var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i--)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction );
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push(spr);
            spr.setPositionY(animOriginY);

            for (var i = indx; i < indx + 2; i++)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }

        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            gap = -773.5;

            var eee;
            if (i < 2)
            {
                eee = spriteVec[i + 1];
            }
            if (i == 0)
            {
                var ee = spriteVec[i + 1];
                if ( Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) ) != 387)
                {
                    gap = -Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) * 2 * ee.getScale() );
                }
            }

            gap*=r.direction;
            var ease;

            if ( this.slots[sSlot].isForcedStop )
            {
                ease =  new cc.MoveTo( 0.15, cc.p( 0, e.getPositionY() + gap) ).easing( cc.easeBackOut() );
            }
            else
            {
                ease = new cc.MoveTo( 0.3, cc.p( 0, e.getPositionY() + gap ) ).easing( cc.easeBackOut() );
            }

            ease.setTag( MOVING_TAG );
            e.runAction(ease);
        }

        return spriteVec;
    },

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSDiamondsForever;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    spin : function()
    {
        this.unschedule(this.updateWinCoin);
        this.deductBet( this.currentBet );
        if (this.displayedScore != this.totalScore)
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;

        this.currentWin = 0;
        this.currentWinForcast = 0;
        this.startRolling();


        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    }

});