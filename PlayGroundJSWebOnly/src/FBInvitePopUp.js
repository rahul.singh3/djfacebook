/**
 * Created by RNF-Mac11 on 3/9/17.
 */

var TICK_IMAGE_TAG  = 22;
var INVITED_FRIENDS = "invited_friends";
var INSTANT_REWARDS = 2000;
var FB_INVITATION_REWARDS = 2000;
var FB_MAX_REQUESTS = 50;

FBInvitePopUp = cc.Layer.extend({

    bgSprite : null,

    scroll : null,

    selectedFriendsCountLabel : null,

    friendsVector : new Array(),

    selectedFriendsVector : new Array(),

    buttonsVector : new Array(),


    setLabelText : function()
    {
        var str = this.selectedFriendsVector.length + "/" + this.friendsVector.length;
        this.selectedFriendsCountLabel.setString( str );
    },

    menuCB : function( pSender )
    {
        var delegate = AppDelegate.getInstance();

        var tickSprite = pSender.getChildByTag( TICK_IMAGE_TAG );

        switch ( pSender.getTag() )
        {
            case CLOSE_BTN_TAG:
                this.removeFromParent( true );
                FacebookObj.requestedOnce = false;
                delegate.showingScreen = false;
                PopUps.getInstance().removePopUPIndex(PopUpTags.STAY_TOUCH_DEAL_POP);
                //cc.log( "FacebookObj.requestedOnce in pop = " + FacebookObj.requestedOnce );
                break;

            case SELECT_ALL_BTN:
                tickSprite.setVisible( !tickSprite.isVisible() );
                this.selectAllButtons( tickSprite.isVisible() );
                break;

            case INVITE_BTN_TAG:
                if( this.selectedFriendsVector.length >= 10 )
                {
                    //cc.log( "int INVITE_BTN_TAG this.giveRewardsForInviting = true" );
                    this.giveRewardsForInviting = true;
                }
                this.sendInvitations();
                break;

            default:
                break;
        }
        delegate.jukeBox( S_BTN_CLICK );
    },
    giveRewardsForInviting : false,//for assigning rewards on just inviting more than 10 Poeple

    selectAllButtons : function( isVIsible )
    {
        for(var i = 0; i < this.buttonsVector.length; i++)
        {
            var button = this.buttonsVector[ i ];
            button.getChildByTag( TICK_IMAGE_TAG ).setVisible( isVIsible );
        }

        if ( isVIsible ) {
            this.selectedFriendsVector = [];
            for( var i = 0; i < this.friendsVector.length; i++ )
            {
                this.selectedFriendsVector[i] = this.friendsVector[i];
            }
        }
        else
        {
            this.selectedFriendsVector = [];
        }
        this.setLabelText();
    },

    touchEvent : function( pSender, type )
    {
        var button = pSender;

        switch ( type )
        {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                button.getChildByTag( TICK_IMAGE_TAG ).setVisible( !button.getChildByTag( TICK_IMAGE_TAG ).isVisible() );
                if( button.getChildByTag( TICK_IMAGE_TAG ).isVisible() )
                {
                    this.selectedFriendsVector.push( this.friendsVector[ button.getTag() ] );
                    if( this.selectedFriendsVector.length == this.friendsVector.length )
                    {
                        this.selectAllItem.getChildByTag( TICK_IMAGE_TAG ).setVisible(true);
                    }
                }
                else
                {
                    var index = 0;
                    for( index = 0; index < this.selectedFriendsVector.length; index++ )
                    {
                        if( this.selectedFriendsVector[index].name === button.getChildByTag( NAME_LABEL_TAG ).getString() )
                        {
                            break;
                        }
                    }
                    this.selectedFriendsVector.splice( index, 1 );//check this
                    this.selectAllItem.getChildByTag( TICK_IMAGE_TAG ).setVisible(false);
                }
                this.setLabelText();
                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;

            default:
                break;
        }
    },

    editBoxEditingDidBegin : function( editBox )
    {
        editBox.setFont( "HELVETICALTSTD-BOLD", 30 );
    },
    editBoxEditingDidEnd : function( editBox )
    {
        editBox.setFont( "HELVETICALTSTD-BOLD", 30 );
    },
    editBoxTextChanged : function( editBox, text )
    {
        editBox.setFont( "HELVETICALTSTD-BOLD", 30 );
        this.setScrollViewAccordingToWord( text );
    },
    editBoxReturn : function( editBox )
    {
    },

    setScrollViewAccordingToWord : function( text )
    {
        //cc.log( "text to search = " + text );
        var height = ( this.buttonsVector.length + 1.5 ) * 30 * 1.2;

        var scollFrameSize = cc.size( this.bgSprite.getContentSize().width, this.bgSprite.getContentSize().height * 0.5 );

        if( height < scollFrameSize.height )
        {
            height = scollFrameSize.height;
        }

        var elmCount = 0;

        var isLeft = true;

        for ( var i = 0; i < this.buttonsVector.length; i++ )
        {
            var item = this.buttonsVector[i];

            var label = item.getChildByTag( NAME_LABEL_TAG );
            var userNameTxt = label.getString().toLowerCase();
            //cc.log( "userNameTxt to search = " + userNameTxt );
            if(text.length && userNameTxt.indexOf(text.toLowerCase()) == -1)
            {
                item.setVisible( false );
            }
            else
            {
              //cc.log( "userNameTxt.indexOf(text.toLowerCase()) = " + userNameTxt.indexOf(text.toLowerCase()) );
                item.setVisible( true );
                elmCount++;
            }
        }

        height = ( elmCount / 2 + 1.5 ) * new cc.Sprite( "#inviteTab.png" ).getContentSize().height * 1.2;

        if( height < scollFrameSize.height )
        {
            height = scollFrameSize.height;
        }

        this.scroll.setInnerContainerSize( cc.size( scollFrameSize.width, height ) );

        var pos = height - new cc.Sprite( "#inviteTab.png" ).getContentSize().height * 0.5;

        for ( var i = 0; i < this.buttonsVector.length; i++ )
        {
            var button =  this.buttonsVector[ i ];

            if ( button.isVisible() )
            {
                if ( isLeft )
                {
                    button.setPosition( cc.p( this.bgSprite.getContentSize().width / 2 - button.getContentSize().width * 0.55, pos ) );
                }
                else
                {
                    button.setPosition( cc.p( this.bgSprite.getContentSize().width / 2 + button.getContentSize().width * 0.55, pos ) );
                    pos-=button.getContentSize().height * 1.2;
                }
                isLeft = !isLeft;
            }
        }
    },

    create : function( )
    {},

    hitForFriends : function( haveNet )
    {},
    selectAllItem : null,
    makeScrollVIew : function( data )
    {
        var name = new cc.EditBox( new cc.Sprite( "#search_bar.png" ).getContentSize() , new cc.Scale9Sprite( "#search_bar.png" ) );
        name.setPosition( cc.p( this.bgSprite.getContentSize().width * 0.5, this.bgSprite.getContentSize().height * 0.77 ) );

        name.setFont( "HELVETICALTSTD-BOLD", 30 );
        name.setPlaceholderFont( "HELVETICALTSTD-BOLD", 30 );
        name.setFontColor( cc.color( 255, 255, 255 ) );
        name.setMaxLength( 15 );
        name.setPlaceHolder( " Enter the Name" );
        name.setReturnType( cc.EditBox.KEYBOARD_RETURNTYPE_DONE );
        name.setInputMode( cc.EditBox.EDITBOX_INPUT_MODE_SINGLELINE );
        name.setDelegate( this );

        this.bgSprite.addChild( name );


        this.scroll = new ccui.ScrollView();

        var scollFrameSize = cc.size( this.bgSprite.getContentSize().width, this.bgSprite.getContentSize().height * 0.5 );
        this.scroll.setDirection( ccui.ScrollView.DIR_VERTICAL );
        this.scroll.setContentSize( scollFrameSize );
        this.scroll.setScrollBarAutoHideEnabled( false )
        this.scroll.setScrollBarOpacity(255);
        this.scroll.setScrollBarColor( cc.color( 255, 255, 255, 255 ) );
        this.scroll.setBounceEnabled( true );
        this.scroll.setPosition( cc.p( 0, this.bgSprite.getContentSize().height * 0.2 ) );

        var height = ( data.length/ 2 + 1.5 ) * new cc.Sprite( "#inviteTab.png" ).getContentSize().height * 1.2;

        if( height < scollFrameSize.height )
        {
            height = scollFrameSize.height;
        }
        this.scroll.setInnerContainerSize( cc.size( scollFrameSize.width, height ) );

        this.bgSprite.addChild( this.scroll );

        //var obj;
        var isLeft = true;
        var pos = height - new cc.Sprite( "#inviteTab.png" ).getContentSize().height * 0.5;
        var tag = 0;

        //for (var obj in data)
        //cc.log( "data.length = " + data.length );
        for( var ii = 0; ii < data.length; ii++)
        {
            var obj = data[ii];
            this.friendsVector.push( obj );
            if ( obj )
            {
                var sprite = new ccui.Button( "inviteTab.png", "inviteTab.png", "", ccui.Widget.PLIST_TEXTURE );

                sprite.setTag( tag );
                tag++;

                this.buttonsVector.push( sprite );
                if ( isLeft )
                {
                    sprite.setPosition( cc.p( this.bgSprite.getContentSize().width / 2 - sprite.getContentSize().width * 0.55, pos ) );
                }
                else
                {
                    sprite.setPosition( cc.p( this.bgSprite.getContentSize().width / 2 + sprite.getContentSize().width * 0.55, pos ) );
                    pos-=sprite.getContentSize().height * 1.2;
                }
                isLeft = !isLeft;

                var first_name = obj[ "name" ];
                var heading = new CustomLabel( first_name, "GEORGIAB", 20, cc.size( 200, 20 ) );
                heading.setPosition( sprite.getContentSize().width * 0.5, sprite.getContentSize().height * 0.5 );
                heading.setColor( cc.color( 255, 255, 0, 255 ) );
                heading.setTag( NAME_LABEL_TAG );
                sprite.addChild( heading, 2 );

                var urlDict = obj["picture"]["data"];

                var url = urlDict["url"];
                this.addImageOnParent( url, sprite );
                /*RemoteSprite * remoteSprite = RemoteSprite::createWithURL( url.getCString(), "lobby/defProfile.png", false );
                remoteSprite.setPosition( cc.p( sprite.getContentSize().width * 0.11, sprite.getContentSize().height * 0.5 ) );
                sprite.addChild( remoteSprite );*/

                var tickSprite = new cc.Sprite( "#check.png" );
                tickSprite.setPosition( cc.p( sprite.getContentSize().width * 0.9, sprite.getContentSize().height / 2 ) );
                sprite.addChild( tickSprite, 0, TICK_IMAGE_TAG );

                sprite.addTouchEventListener( this.touchEvent, this );
                this.scroll.addChild( sprite );
            }
        }

        this.selectedFriendsCountLabel = new cc.LabelTTF( this.selectedFriendsVector.length + "/" + data.length, "GEORGIAB", 40 );
        this.selectedFriendsCountLabel.setPosition( cc.p( this.bgSprite.getContentSize().width * 0.5, this.bgSprite.getContentSize().height * 0.7 ) );
        this.bgSprite.addChild( this.selectedFriendsCountLabel );

        this.selectedFriendsCountLabel.setVisible( false );

        this.selectAllItem = new cc.MenuItemSprite( new cc.Sprite( "#selectAll.png" ), new cc.Sprite( "#selectAll.png" ), this.menuCB, this );

        this.selectAllItem.setPosition( this.bgSprite.getContentSize().width * 0.25, this.bgSprite.getContentSize().height * 0.12 );
        this.selectAllItem.setTag( SELECT_ALL_BTN );

        this.selectAllItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );

        var tickSprite = new cc.Sprite( "#check.png" );
        tickSprite.setPosition( cc.p( this.selectAllItem.getContentSize().width * 0.125, this.selectAllItem.getContentSize().height / 2 ) );
        tickSprite.setTag( TICK_IMAGE_TAG );
        this.selectAllItem.addChild( tickSprite );

        this.selectAllButtons( tickSprite.isVisible() );


        var inviteItem = new cc.MenuItemSprite( new cc.Sprite( "#inviteBtn.png" ), new cc.Sprite( "#inviteBtn.png" ), this.menuCB, this );

        inviteItem.setPosition( this.bgSprite.getContentSize().width * 0.75, this.selectAllItem.getPositionY() );
        inviteItem.setTag( INVITE_BTN_TAG );

        inviteItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );

        var menu = new cc.Menu( this.selectAllItem, inviteItem, null );

        menu.setPosition( 0, 0 );
        this.bgSprite.addChild( menu );

        this.bgSprite.removeChildByTag( LOADER_TAG );

        var termText = new cc.LabelTTF( "* You will receive " + FB_INVITATION_REWARDS + " credits for every friend who successfully joins Double Jackpot Slots!", "GEORGIAB", 15 );
        termText.setPosition( cc.p( this.bgSprite.getContentSize().width / 2, cc.rectGetMinY(this.selectAllItem.getBoundingBox()) - termText.getContentSize().height * 0.75 ) );
        this.bgSprite.addChild( termText );
    },

    addImageOnParent: function (appIconURL,parentSprite){
        cc.loader.loadImg(appIconURL, {isCrossOrigin: true},function (err, texture) {
            var sprite = new cc.Sprite(texture);
            sprite.setPosition( cc.p( parentSprite.getContentSize().width * 0.11, parentSprite.getContentSize().height * 0.5 ) );
            parentSprite.addChild(sprite,10);
        },parentSprite);
    },

    sendInvitations : function()
    {
        var valueMapOfFriendIds = new Map();
        var ids = "";
        if ( this.selectedFriendsVector.length )
        {
            var a = 0;
            for ( var i = 0; i < this.selectedFriendsVector.length; i++ )
            {
                var sdata = this.selectedFriendsVector[i];
                var idString = sdata["id"];

                valueMapOfFriendIds.set( ("" + a ), idString );
                ids = ids + idString + ",";
                this.selectedFriendsVector.splice( i , 1 );
                i--;
                a++;
                if( a >= FB_MAX_REQUESTS - 1 )
                {
                    //cc.log( "FB_MAX_REQUESTS = " + a );
                    break;
                }
            }

            if( ServerData.getInstance().current_time_seconds >= CSUserdefauts.getInstance().getValueForKey( FB_REWARDS_ASSIGNMENT_TIME, 0 ) )
            {
                CSUserdefauts.getInstance().setValueForKey( FB_REWARDS_ASSIGNMENT_TIME, 0 );
            }

            //if (valueMapOfFriendIds.length > 0) {
                FacebookObj.sendRequestToFewUsers(ids);
            //cc.log( "ids.length = " + ids.length );
            if( ids.length == 0 )
            {
                this.removeFromParent(true);
            }
            //}

        }
        else
        {
            //cc.log( "in here" );
            FacebookObj.requestedOnce = false;
            this.removeFromParent( true );
           // ServerCommunicator.getInstance().SCUpdateData();
        }
//cc.log( 'in sendInvitations ids.length = ' + ids.length + ", this.giveRewardsForInviting = " + this.giveRewardsForInviting );
        if( ids.length == 0 && this.giveRewardsForInviting )
        {
            if( !CSUserdefauts.getInstance().getValueForKey( FB_REWARDS_ASSIGNMENT_TIME, 0 ) )
            {
                //cc.log( 'in sendInvitations assign rewards' );
                AppDelegate.getInstance().assignRewards( INSTANT_REWARDS ,true);
                CSUserdefauts.getInstance().setValueForKey( FB_REWARDS_ASSIGNMENT_TIME, ServerData.getInstance().current_time_seconds + SECONDS_IN_A_DAY );
            }
            this.removeFromParent( true );
            FacebookObj.requestedOnce = false;
        }
    },

    ctor:function ( code ) {
        this._super( );

        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        this.addChild( darkBG );

        var vSize = cc.winSize;

        this.bgSprite = new cc.Sprite( "#invitePop.png" );
        this.bgSprite.setPosition( vSize.width / 2, vSize.height / 2 );
        this.addChild( this.bgSprite );

        var delegate = AppDelegate.getInstance();

        if( delegate.invitableFriendsList )
        {
            this.makeScrollVIew( delegate.invitableFriendsList );
        }

        /*delegate.haveNetwork( 3 );

        Sprite * loaderSprite = Sprite::create( "loader.png" );
        loaderSprite.setPosition( bgSprite.getContentSize() * 0.5 );
        loaderSprite.runAction( RepeatForever::create( RotateBy::create( 0.75, 360 ) ) );
        bgSprite.addChild( loaderSprite, -1, LOADER_TAG );*/


        var buyMenu = new cc.Menu();

        buyMenu.setPosition( cc.p( 0, 0 ) );
        this.bgSprite.addChild( buyMenu );
     //res.Close_Buttpn_png
        var closeItem = new cc.MenuItemImage( res.Close_Buttpn_png, res.Close_Buttpn_png, this.menuCB, this );

        closeItem.setPosition( this.bgSprite.getContentSize().width - closeItem.getContentSize().width / 4, this.bgSprite.getContentSize().height - closeItem.getContentSize().height / 4 );
        closeItem.setTag( CLOSE_BTN_TAG );

       // closeItem.getSelectedImage().setColor( cc.color( 169, 169, 169 ) );

        buyMenu.addChild( closeItem );

        //DJSResizer * resizer = DJSResizer::getInstance();
        //resizer.setFinalScaling( bgSprite );
        

        return true;
    },

    handleScroll : function( event )
    {
        DPUtils.getInstance().scrolledAlready = true;

        var self = event.getCurrentTarget();

        if( !self.scroll )
        {
            return;
        }

        var nMoveY = event.getScrollY() * 0.5;
        //if( !isMAC )//Change 17 May
        {
            nMoveY = -nMoveY;
        }
        //cc.log( str );

        var inner = self.scroll.getInnerContainer();
        var curPos  = inner.getPosition();
        var nextPos = cc.p(curPos.x, curPos.y + nMoveY);

        // prevent scrolling past beginning
        if (nextPos.y > 0)
        {
            //inner.setPosition(cc.p(0, nextPos.y));
            self.scroll._startAutoScrollToDestination(cc.p(nextPos.x, 0), 0.5, true);
            return;
        }

        //auto ws = Director::getInstance()->getWinSize();
        var size = self.scroll.getContentSize();
        var innerSize = inner.getContentSize();
        var topMostY = size.height - innerSize.height;

        // prevent scroll past end
        if (nextPos.y < topMostY)
        {
            self.scroll._startAutoScrollToDestination(cc.p(nextPos.x, topMostY), 0.5, true);
            return;
        }

        self.scroll._startAutoScrollToDestination(nextPos, 0.5, true);
    }

});