


var reelsRandomizationArrayDJSGoldenSlot =
    [
        [// 85% payout
            40, 20, 40, 70, 40, 25,
            40, 55, 40, 36, 40, 40,
            40, 41, 40, 20, 40, 70,
            40, 25, 40, 55, 40, 35,
            40, 40, 40, 41, 40, 70,
            40, 55, 40, 40, 40, 41,
        ],
        [//92 %
            40, 20, 40, 70, 40, 25,
            40, 55, 38, 39, 40, 40,
            40, 41, 40, 20, 40, 70,
            40, 25, 40, 55, 40, 39,
            40, 40, 40, 41, 40, 70,
            40, 55, 40, 40, 40, 41,
        ],
        [//97 %
            40, 20, 40, 70, 40, 25,
            40, 55, 40, 40, 40, 40,
            40, 41, 40, 20, 40, 70,
            40, 25, 40, 55, 40, 40,
            40, 40, 40, 41, 40, 70,
            40, 55, 40, 40, 40, 41,
        ],
        [//140 %
            40, 30, 40, 70, 40, 25,
            40, 55, 40, 55, 40, 40,
            40, 41, 40, 30, 40, 70,
            40, 25, 40, 55, 40, 55,
            40, 40, 40, 41, 40, 70,
            40, 55, 40, 40, 40, 41,
        ],
        [//300 %
            40, 50, 40, 70, 40, 50,
            40, 55, 40, 70, 40, 40,
            40, 41, 40, 50, 40, 70,
            40, 50, 40, 55, 40, 70,
            40, 40, 40, 41, 40, 70,
            40, 55, 40, 40, 40, 41,
        ],
    ];

var DJSGoldenSlot = GameScene.extend({
    reelsArrayDJSGoldenSlot:[],
    ctor:function ( level_index ) {
        this.reelsArrayDJSGoldenSlot = [
            /*1*/ELEMENT_EMPTY,/*2*/ELEMENT_7_RED,/*3*/ELEMENT_EMPTY,/*4*/ELEMENT_BAR_1,/*5*/ELEMENT_EMPTY,
            /*6*/ELEMENT_7_PINK,/*7*/ELEMENT_EMPTY,/*8*/ELEMENT_BAR_2,/*9*/ELEMENT_EMPTY,/*10*/ELEMENT_X,
            /*11*/ELEMENT_EMPTY,/*12*/ELEMENT_BAR_3,/*13*/ELEMENT_EMPTY,/*14*/ELEMENT_7_BLUE,/*15*/ELEMENT_EMPTY,
            /*16*/ELEMENT_7_RED,/*17*/ELEMENT_EMPTY,/*18*/ELEMENT_BAR_1,/*19*/ELEMENT_EMPTY,/*20*/ELEMENT_7_PINK,
            /*21*/ELEMENT_EMPTY,/*22*/ELEMENT_BAR_2,/*23*/ELEMENT_EMPTY,/*24*/ELEMENT_X,/*25*/ELEMENT_EMPTY,
            /*26*/ELEMENT_BAR_3,/*27*/ELEMENT_EMPTY,/*28*/ELEMENT_7_BLUE,/*29*/ELEMENT_EMPTY,/*30*/ELEMENT_BAR_1,
            /*31*/ELEMENT_EMPTY,/*32*/ELEMENT_BAR_2,/*33*/ELEMENT_EMPTY,/*34*/ELEMENT_BAR_3,/*35*/ELEMENT_EMPTY,
            /*36*/ELEMENT_7_BLUE,
        ];
        this.probabilityArraryCheck = reelsRandomizationArrayDJSGoldenSlot;
        this.physicalArrayCheck= this.reelsArrayDJSGoldenSlot;
        this._super( level_index );

        this.startLoadingMechanism( level_index );

        this.totalProbalities = 0;

        this.totalLines = 1;

        this.fadeInOutChked = false;

        this.changeTotalProbabilities();

        return true;
    },


    calculatePayment : function( doAnimate )
    {
        var payMentArray = [
        /*0*/    1000, // triple wild

        /*1*/     75, // 7 7 7 Red

        /*2*/     50, // 7 7 7 Pink

        /*3*/     25,  //7 7 7 Blue

        /*4*/     20,  // 3Bar 3Bar 3Bar

        /*5*/     15,  // 2Bar 2Bar 2Bar

        /*6*/     10,  // Bar Bar Bar

        /*7*/     10,// Any 7 Combo

        /*8*/     5,// Any Bar Combo

        /*9*/     2,//2 wild

        /*10*/    1,//single wild
    ];

        this.lineVector = [];
        var tmpVector;
        var tmpVector2 = new Array();

        for ( var i = 0; i < this.slots[0].resultReel.length; i++ )
        {
            tmpVector = this.slots[0].resultReel[i];
            tmpVector2.push( tmpVector[1] );
        }

        this.lineVector.push( tmpVector2 );

        var pay = 0;
        var lineLength = 3;
        var countWild = 0;
        var countGeneral = 0;
        var pivotElement = -1;
        var pivotElement2 = -1;
        var i, j, k;
        var e = null;
        var pivotElm = null;
        var pivotElm2 = null;
        var tmpVec;
    

        for ( i = 0; i < this.totalLines; i++ )
        {
            tmpVec = this.lineVector[i];
            for ( k = 0; k < 11; k++ )
            {
                var breakLoop = false;
                countGeneral = 0;
                switch ( k )
                {
                    case 0://for all combination with wild
                        for ( j = 0; j < lineLength; j++ )
                        {
                            if ( this.resultArray[0][j] != -1 )
                            {
                                switch ( this.reelsArrayDJSGoldenSlot[this.resultArray[i][j]] )
                                {
                                    case ELEMENT_X:
                                        //tmpPay*=10;
                                        countWild++;
                                        if ( doAnimate ) {
                                            e = tmpVec[j];
                                        }
                                        if ( e )
                                        {
                                            e.aCode = SCALE_TAG;
                                        }
                                        break;


                                    default:
                                        switch ( pivotElement )
                                        {
                                            case -1:
                                                pivotElement = this.reelsArrayDJSGoldenSlot[this.resultArray[i][j]];
                                                countGeneral++;
                                                if ( doAnimate ) {
                                                    e = tmpVec[j];
                                                }
                                                if ( e )
                                                {
                                                    pivotElm = e;
                                                }
                                                break;

                                            default:
                                                pivotElement2 = this.reelsArrayDJSGoldenSlot[this.resultArray[i][j]];
                                                countGeneral++;

                                                if ( doAnimate ) {
                                                    e = tmpVec[j];
                                                }
                                                if ( e )
                                                {
                                                    pivotElm2 = e;
                                                }
                                                break;
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }

                        if ( j == 3 )
                        {
                            if ( countWild == 3 )
                            {
                                pay = payMentArray[0];
                                breakLoop = true;
                            }
                            else if( countWild >= 2 )
                            {
                                breakLoop = true;

                                switch ( pivotElement )
                                {
                                    case ELEMENT_7_RED:
                                        pay = payMentArray[9] * payMentArray[1];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }

                                        break;

                                    case ELEMENT_7_PINK:
                                        pay = payMentArray[9] * payMentArray[2];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_7_BLUE:
                                        pay = payMentArray[9] * payMentArray[3];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_3:
                                        pay = payMentArray[9] * payMentArray[4];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_2:
                                        pay = payMentArray[9] * payMentArray[5];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    case ELEMENT_BAR_1:
                                        pay = payMentArray[9] * payMentArray[6];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                        }
                                        break;

                                    default:
                                        pay = payMentArray[9];
                                        /*if ( pivotElm )
                                        {
                                            pivotElm.aCode = SCALE_TAG;
                                        }*/
                                        break;
                                }
                            }
                            else if( countWild == 1 )
                            {
                                breakLoop = true;

                                if ( pivotElement == pivotElement2 )
                                {
                                    switch ( pivotElement )
                                    {
                                        case ELEMENT_7_RED:
                                            pay = payMentArray[10] * payMentArray[1];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_7_PINK:
                                            pay = payMentArray[10] * payMentArray[2];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_7_BLUE:
                                            pay = payMentArray[10] * payMentArray[3];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_3:
                                            pay = payMentArray[10] * payMentArray[4];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_2:
                                            pay = payMentArray[10] * payMentArray[5];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        case ELEMENT_BAR_1:
                                            pay = payMentArray[10] * payMentArray[6];
                                            if ( doAnimate ) {
                                                pivotElm.aCode = SCALE_TAG;
                                                pivotElm2.aCode = SCALE_TAG;
                                            }
                                            break;

                                        default:
                                            pay = payMentArray[10];
                                            break;
                                    }
                                }
                                else
                                {
                                    if ( pivotElement >= ELEMENT_7_BAR && pivotElement <= ELEMENT_7_RED &&
                                        pivotElement2 >= ELEMENT_7_BAR && pivotElement2 <= ELEMENT_7_RED )
                                    {
                                        pay = payMentArray[10] * payMentArray[7];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else if ( ( pivotElement >= ELEMENT_BAR_1 && pivotElement <= ELEMENT_BAR_3 ) &&
                                        ( pivotElement2 >= ELEMENT_BAR_1 && pivotElement2 <= ELEMENT_BAR_3 ) )
                                    {
                                        pay = payMentArray[10] * payMentArray[8];
                                        if ( doAnimate ) {
                                            pivotElm.aCode = SCALE_TAG;
                                            pivotElm2.aCode = SCALE_TAG;
                                        }
                                    }
                                    else
                                    {
                                        pay = payMentArray[10];
                                    }
                                }
                            }
                        }
                        break;

                    case 1: // 7 7 7 red
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSGoldenSlot[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[1];
                            breakLoop = true;
                            

                            for ( var l = 0; l < countGeneral; l++ ) {

                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 2: // 7 7 7 Pink
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSGoldenSlot[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_PINK:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[2];
                            breakLoop = true;
                            for ( var l = 0; l < countGeneral; l++ ) {

                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 3: // 7 7 7 Blue
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSGoldenSlot[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_BLUE:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[3];
                            breakLoop = true;

                            for ( var l = 0; l < countGeneral; l++ ) {

                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 4: // 3Bar 3Bar 3Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSGoldenSlot[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[4];
                            breakLoop = true;

                            for ( var l = 0; l < countGeneral; l++ ) {

                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 5: // 2Bar 2Bar 2Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSGoldenSlot[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_2:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[5];
                            breakLoop = true;

                            for ( var l = 0; l < countGeneral; l++ ) {

                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 6: // Bar Bar Bar
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSGoldenSlot[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[6];
                            breakLoop = true;

                            for ( var l = 0; l < countGeneral; l++ ) {

                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 7: // any 7 combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSGoldenSlot[this.resultArray[i][j]] )
                            {
                                case ELEMENT_7_BLUE:
                                case ELEMENT_7_PINK:
                                case ELEMENT_7_RED:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[7];
                            breakLoop = true;

                            for ( var l = 0; l < countGeneral; l++ ) {
                            if ( doAnimate ) {
                                e = tmpVec[l];
                            }
                            if ( e )
                            {
                                e.aCode = SCALE_TAG;
                            }
                        }
                        }
                        break;

                    case 8: // any Bar combo
                        for ( j = 0; j < lineLength; j++ )
                        {
                            switch ( this.reelsArrayDJSGoldenSlot[this.resultArray[i][j]] )
                            {
                                case ELEMENT_BAR_1:
                                case ELEMENT_BAR_2:
                                case ELEMENT_BAR_3:
                                    countGeneral++;
                                    break;

                                default:
                                    break;
                            }
                        }
                        if ( countGeneral == 3 )
                        {
                            pay = payMentArray[8];
                            breakLoop = true;

                            for ( var l = 0; l < countGeneral; l++ )
                            {
                                if ( doAnimate ) {
                                    e = tmpVec[l];
                                }
                                if ( e )
                                {
                                    e.aCode = SCALE_TAG;
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

                if ( breakLoop )
                {
                    break;
                }
            }
        }

        if( doAnimate )
        {
            this.currentWin = this.currentBet * pay;
            if ( this.currentWin > 0 )
            {
                this.addWin = this.currentWin / 10;
                this.setCurrentWinAnimation();
            }

            this.checkAndSetAPopUp();
        }
        else
        {
            this.currentWinForcast = this.currentBet * pay;
        }

    },
    updateServerDataOnMachine : function() {
        if (this.physicalReelElements && this.physicalReelElements.length>=36) {
            var i=0;
            for(var idx in this.physicalReelElements){
                this.reelsArrayDJSGoldenSlot[i]=this.physicalReelElements[idx];
                i++;
            }
        }
        this.probabilityArray = this.probabilityArray[this.easyModeCode];

    },
    changeTotalProbabilities : function()
    {
        this.totalProbalities = 0;
        if (  ServerData.getInstance().TRICKY_MACHINE_CODE == this.mCode )
        {
            this.probabilityArray = this.probabilityArrayServer[this.easyModeCode];

            for ( var i = 0; i < 36; i++ )
            {
                this.totalProbalities+=this.probabilityArray[i];
            }
        }
        else
        {
            for ( var i = 0; i < 36; i++ )
            {
                this.probabilityArray[i] = reelsRandomizationArrayDJSGoldenSlot[this.easyModeCode][i];
                this.totalProbalities+=reelsRandomizationArrayDJSGoldenSlot[this.easyModeCode][i];
            }
        }
    },

    checkExcitement : function()
    {
        this.excitementChecked = true;
        if( ( this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] == ELEMENT_7_RED && this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] == ELEMENT_7_RED )  ||
            ( this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] == ELEMENT_7_BLUE && this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] == ELEMENT_7_BLUE ) ||
            ( this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] == ELEMENT_7_PINK && this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] == ELEMENT_7_PINK ) ||
            ( ( this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] <= ELEMENT_10X ) &&
                ( this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] <= ELEMENT_10X ) ) ||
            ( this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] == ELEMENT_7_RED && ((this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] == ELEMENT_7_RED && ((this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] == ELEMENT_7_BLUE && ((this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] == ELEMENT_7_BLUE && ((this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] == ELEMENT_7_PINK && ((this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] == ELEMENT_7_PINK && ((this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] == ELEMENT_7_BAR && ((this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] == ELEMENT_7_BAR && ((this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] == ELEMENT_BAR_3 && ((this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] >= ELEMENT_2X && this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] <= ELEMENT_10X ) ) ) ||
            ( this.reelsArrayDJSGoldenSlot[this.resultArray[0][1]] == ELEMENT_BAR_3 && ((this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] >= ELEMENT_2X && this.reelsArrayDJSGoldenSlot[this.resultArray[0][0]] <= ELEMENT_10X ) ) ) )
        {
            if ( !this.slots[0].isForcedStop )
            {
                this.schedule( this.reelStopper, 4.8 );

                this.delegate.jukeBox( S_EXCITEMENT );

                var glowSprite = new cc.Sprite( glowStrings[0] );

                if ( glowSprite )
                {
                    var r = this.slots[0].physicalReels[( this.slots[0].physicalReels.length - 1 )];

                    glowSprite.setPosition( r.getParent().getPosition().x + this.slots[0].getPosition().x, r.getParent().getPosition().y + this.slots[0].getPosition().y );

                    //this.scaleAccordingToSlotScreen( glowSprite );

                    this.addChild( glowSprite, 5 );
                    this.animNodeVec.push( glowSprite );

                    glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ) ) );
                }
            }
        }
    },

    checkFadeInOut : function( laneIndex)
    {
        this.fadeInOutChked = true;
        var tmp = this.slots[0].resultReel[laneIndex];

        var e = tmp[1];

        switch (e.eCode)
        {
            case ELEMENT_7_RED:
                if (laneIndex != 0)
                {
                    break;
                }
            case ELEMENT_2X:
            case ELEMENT_3X:
            case ELEMENT_4X:
            case ELEMENT_5X:
                e.runAction( new cc.Repeat( new cc.Sequence( new cc.FadeTo( 0.3, 100 ), new cc.FadeTo( 0.3, 255 ) ), 2 ) );
                this.delegate.jukeBox(S_FLASH);
                break;

            default:
                break;
        }
    },

    generateResult : function( scrIndx )
    {
        for ( var i = 0; i < this.slots[scrIndx].displayedReels.length; i++ )
        {
            //generate:
            var num = parseInt(this.prevReelIndex[i]);

            var tmpnum = 0;

            while (num == parseInt(this.prevReelIndex[i]))
            {
                num = Math.floor( Math.random() * this.totalProbalities );
            }
            this.prevReelIndex[i] = num;
            this.resultArray[0][i] = num;

            for (var j = 0; j < 36; j++)
            {
                tmpnum+=this.probabilityArray[j];

                if (tmpnum >= this.resultArray[0][i])
                {
                    this.resultArray[0][i] = j;
                    break;
                }
            }
        }

    },

    rollUpdator : function(dt)
    {
        var i;
        for (i = 0; i < this.totalSlots; i++)
        {
            this.slots[i].updateSlot(dt);
        }

        for (i = 0; i < this.totalSlots; i++)
        {
            if ( this.slots[i].isSlotRunning() )
            {
                break;
            }
        }

        if (i >= this.totalSlots)
        {
            this.isRolling = false;
            this.unschedule( this.rollUpdator );
            this.calculatePayment( true );

            if (this.isAutoSpinning)
            {
                if (this.currentWin > 0)
                {
                    this.scheduleOnce( this.autoSpinCB, 1.5);
                }
                else
                {
                    this.scheduleOnce( this.autoSpinCB, 1.0);
                }
            }
        }
    },

    setFalseWin : function()
    {
        var highWinIndices = [ 3, 5, 7, ];
        var missIndices = [ 3, 5, 7, 15, 17, 19, 21, ];

        var missedIndx = Math.floor( Math.random() * 3 );

        if( missedIndx == 2 )
        {
            if( Math.floor( Math.random() * 10 ) == 3 )
            {
            }
            else
            {
                missedIndx = Math.floor( Math.random() * 2 );
            }
        }

        var sameIndex = Math.floor( Math.random() * highWinIndices.length );

        for ( var i = 0; i < 3; i++ )
        {
            if ( i == missedIndx )
            {
                this.resultArray[0][i] = this.getRandomIndex( missIndices[( Math.floor( Math.random() * missIndices.length ) )], true );
            }
            else
            {
                this.resultArray[0][i] = this.getRandomIndex( highWinIndices[sameIndex], false );
            }
        }
    },

    setResult : function( indx, lane, sSlot )
    {
        var gap = 387.0;

        var j = 0;
        var animOriginY;

        var r = this.slots[0].physicalReels[lane];
        var arrSize = r.elementsVec.length;
        var spr;

        var spriteVec = [];

        animOriginY = gap * r.direction;
        spr = r.elementsVec[indx];
        if ( spr.eCode == ELEMENT_EMPTY )
        {
            gap = 285 * spr.getScale();
        }

        if ( indx == 0 )
        {
            spr = r.elementsVec[r.elementsVec.length - 1];
            spriteVec.push(spr);

            spr.setPositionY( animOriginY );

            for (var i = 0; i < 2; i++)
            {
                spr = r.elementsVec[i];
                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (i + 1) * r.direction);
            }
        }
        else if (indx == arrSize - 1)
        {
            spr = r.elementsVec[0];
            spriteVec.push(spr);

            spr.setPositionY(animOriginY);
            for (var i = r.elementsVec.length - 1; i > r.elementsVec.length - 3; i--)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction );
                j++;
            }
        }
        else
        {
            spr = r.elementsVec[indx - 1];

            spriteVec.push(spr);
            spr.setPositionY(animOriginY);

            for (var i = indx; i < indx + 2; i++)
            {
                spr = r.elementsVec[i];

                spriteVec.push(spr);
                spr.setPositionY(animOriginY + gap * (j + 1) * r.direction);
                j++;
            }
        }

        for (var i = 0; i < spriteVec.length; i++)
        {
            var e = spriteVec[i];
            e.runAction( new cc.FadeIn( 0.2 ) );
            gap = -773.5;

            var eee;
            if (i < 2)
            {
                eee = spriteVec[i + 1];
            }
            if (i == 0)
            {
                var ee = spriteVec[i + 1];
                if ( Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) ) != 387)
                {
                    gap = -Math.abs( parseInt( e.getPositionY() - ee.getPositionY() ) * 2 * ee.getScale() );
                }
            }

            gap*=r.direction;
            var ease;

            if ( this.slots[sSlot].isForcedStop )
            {
                ease =  new cc.MoveTo( 0.15, cc.p( 0, e.getPositionY() + gap) ).easing( cc.easeBackOut() );
            }
            else
            {
                ease = new cc.MoveTo( 0.3, cc.p( 0, e.getPositionY() + gap ) ).easing( cc.easeBackOut() );
            }

            ease.setTag( MOVING_TAG );
            e.runAction(ease);
        }

        return spriteVec;
    },

    setWinAnimation : function( animCode, screen )
    {
        var tI = 0.3;

        var fire;
        for (var i = 0; i < this.animNodeVec.length; i++)
        {
            fire = this.animNodeVec[i];
            fire.removeFromParent(true);
        }
        this.animNodeVec = [];


        switch ( animCode )
        {
            case WIN_TAG:

                for (var i = 0; i < this.slots[0].blurReels.length; i++)
                {
                    var r = this.slots[0].blurReels[i];

                    var node = r.getParent();

                    var moveW = this.slots[0].reelDimentions.x * this.slots[0].getScale();
                    var moveH = this.slots[0].reelDimentions.y * this.slots[0].getScale();

                    for (var j = 0; j < 4; j++)
                    {

                        var fire = new cc.ParticleSun();
                        fire.texture = cc.textureCache.addImage( res.FireStar_png );
                        var pos;

                        this.addChild(fire, 5);
                        this.animNodeVec.push(fire);

                        var moveBy = null;

                        var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                        diff*=( 2 - i );

                        switch (j % 4)
                        {
                            case 0:
                                pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY() );
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(moveW, 0));
                                break;

                            case 1:
                                pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() - moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(0, moveH));
                                break;

                            case 2:
                                pos = cc.p(node.getPositionX() + moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(-moveW, 0));
                                break;

                            case 3:
                                pos = cc.p(node.getPositionX() - moveW / 2 + this.slots[0].getPositionX() - diff, node.getPositionY() + moveH / 2 + this.slots[0].getPositionY());
                                fire.setPosition(pos);
                                moveBy = new cc.MoveBy(tI, cc.p(0, -moveH));
                                break;

                            default:
                                break;
                        }
                        fire.runAction( new cc.RepeatForever( new cc.Sequence( moveBy, moveBy.reverse() ) ) );
                    }
                }

                break;

            case BIG_WIN_TAG:
                for(var i = 0; i < this.slots[0].blurReels.length; i++)
                {
                    var r = this.slots[0].blurReels[i];

                    var node = r.getParent();

                    var walkAnimFrames = [];
                    var walkAnim = null;

                    var diff = this.slots[0].reelDimentions.x * this.slots[0].getScale() - this.slots[0].reelDimentions.x ;
                    diff*=( 2 - i );

                    var glowSprite = new cc.Sprite( glowStrings[1] );
                    {
                        var tmp = new cc.SpriteFrame( glowStrings[1], cc.rect(0, 0, glowSprite.getContentSize().width, glowSprite.getContentSize().height));
                        walkAnimFrames.push(tmp);
                    }
                    {
                        var tmp = new cc.SpriteFrame( glowStrings[3], cc.rect(0, 0, glowSprite.getContentSize().width, glowSprite.getContentSize().height));
                        walkAnimFrames.push(tmp);
                    }
                    {
                        var tmp = new cc.SpriteFrame( glowStrings[5], cc.rect(0, 0, glowSprite.getContentSize().width, glowSprite.getContentSize().height));
                        walkAnimFrames.push(tmp);
                    }

                    walkAnim = new cc.Animation( walkAnimFrames, 0.75 );

                    var tmpAnimate = new cc.Animate(walkAnim);
                    glowSprite.runAction( tmpAnimate.repeatForever() );

                    if (glowSprite)
                    {
                        glowSprite.setPosition( node.getPositionX() + this.slots[0].getPositionX() - diff, node.getPositionY() + this.slots[0].getPositionY() );


                        this.addChild(glowSprite, 5);
                        this.animNodeVec.push(glowSprite);

                        this.scaleAccordingToSlotScreen( glowSprite );

                        glowSprite.runAction( new cc.RepeatForever( new cc.Sequence( new cc.FadeTo(0.25, 100), new cc.FadeTo(0.25, 255) ) ) );
                    }
                }
                break;

            default:
                break;
        }
    },

    setSlotScrns : function( row)
    {
        var tmp = this.reelsArrayDJSGoldenSlot;

        if (!this.slots[0])
        {
            this.slots[0] = new SlotScreen(tmp, this.mCode);
            this.addChild(this.slots[0]);
        }
    },

    spin : function()
    {
        this.unschedule(this.updateWinCoin);

        this.deductBet( this.currentBet );
        if (this.displayedScore != this.totalScore)
        {
            this.displayedScore = this.totalScore;
            this.setScoreLabel();
        }

        this.isRolling = true;
        this.excitementChecked = false;
        this.fadeInOutChked = false;
        this.currentWin = 0;
        this.currentWinForcast = 0;
        this.startRolling();

        this.displayedWin = this.currentWin;
        this.addWin = 0;
        this.setWinLabel();
    }

});