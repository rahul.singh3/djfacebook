
       var   C_PLAY_1000_SPINS = 1;
       var   C_WIN_200_MORE_SPIN =2;
       var   C_BIG_WIN_TIMES=3;
       var   C_WIN_M_IN_TOTAL_WIN =4;
       var   C_WIN_1M_IN_TOTAL_WIN = 5;
       var   C_10X_WIN_TIMES =6 ;
       var   DC_TASK_1 = 1;
       var   DC_TASK_2 = 2;
       var   DC_TASK_3= 3;
       var   DC_TASK_4=4;
       var   DC_TASK_5= 5;
       var   DC_TASK_6= 6;
       var   user_Type_1 = 1; // < 10k
       var   user_Type_2 = 2; // >= 10k && coins < 5*10K
       var   user_Type_3 = 3; // >= 5*10K && coins < 50*10K
       var   user_Type_4 = 4; // >= 50*10K && coins < 500*10K
       var   user_Type_5 = 5; // >= 500*10K && coins < 1000*10K
       var   user_Type_6 = 6; // >= 1000*10K
       var   DC_TASK_Y_FACTOR = 100 ;// inital bet of machine
       var   DC_TASK_Z_FACTOR = 10000; // 10K
       DailyChallengeTask = (function () {
             return{
                 _id:0,
                 task_id:0,
                 isActive:0,
                 userType:1,
                 collected:0,
                 minimumBet:50,
                 initalWaitage:0,
                 progress:0,
                 reward:0,
                 taskHeading:"",
                 init:function (valueMap) {
                    // var valueMap1 = JSON.stringify(obj);
                    // var    v = ServerData.getInstance().encodedJsonData();
                     if(valueMap){
                         this._id = valueMap["_id"];
                         this.task_id = valueMap["task_id"];
                         this.isActive = valueMap["isActive"];
                         this.userType =  valueMap["userType"];
                         this.collected =  valueMap["collected"];
                         this.minimumBet =  valueMap["minimumBet"];
                         this.initalWaitage = valueMap["initalWaitage"];
                         this.progress =  valueMap["progress"];
                         this.reward =  valueMap["reward"];
                         this.taskHeading =  valueMap["taskHeading"];
                     }
                     return this;
                 },
                 getDailyChallengeTaskData:function () {
                      var valueMap;
                     valueMap={
                         '_id': this._id,
                         'task_id' : this.task_id,
                         'isActive':this.isActive,
                         'userType' :this.userType,
                         'collected':this.collected,
                         'minimumBet':this.minimumBet,
                         'initalWaitage':this.initalWaitage,
                         'progress':this.progress,
                         'reward':this.reward,
                         'taskHeading':this.taskHeading,
                     };
                     return valueMap;
                   /*   valueMap["_id"] =  this._id;
                      valueMap["task_id"] =  this.task_id;
                      valueMap["isActive"] =  this.isActive ;
                      valueMap["userType"] =  this.userType;
                      valueMap["collected"] =  this.collected;
                      valueMap["minimumBet"] =  this.minimumBet;
                      valueMap["initalWaitage"] =  this.initalWaitage;
                      valueMap["progress"] =  this.progress;
                      valueMap["reward"] =  this.reward;
                      valueMap["taskHeading"] =  this.taskHeading;
                      return valueMap;
                    */
                 }
             };
       });
VSDailyChallenge = (function () {
    // Instance stores a reference to the Singleton
    var instance;
    function init() {
        return{
            _dailyChallengeTasks:null,

            getUserType:function () {
             var currentCoins = userDataInfo.getInstance().totalChips;
             var  TenK = 10000;
              if (hackBuild){
                  return user_Type_1;
              }
                if (currentCoins < TenK) {
                    return user_Type_1;
                }else if (currentCoins >= TenK && currentCoins < 5*TenK){
                    return user_Type_2;
                }else if (currentCoins >= 5*TenK && currentCoins < 50*TenK){
                    return user_Type_3;
                }else if (currentCoins >= 50*TenK && currentCoins < 500*TenK){
                    return user_Type_4;
                }else if (currentCoins >= 500*TenK && currentCoins < 1000*TenK){
                    return user_Type_5;
                }else if (currentCoins >= 1000*TenK ){
                    return user_Type_6;
                }
                return user_Type_6;
            },
            myrandomV:function(idx){
                Math.floor( Math.random() * idx );
            },
            getTaskXfactor:function(task_id){
                var value =1;

                switch (task_id) {
                    case user_Type_1:
                        value =1;
                        break;
                    case user_Type_2:
                        value =2;
                        break;
                    case user_Type_3:
                        value =5;
                        break;
                    case user_Type_4:
                        value =20;
                        break;
                    case user_Type_5:
                        value =50;
                        break;
                    case user_Type_6:
                        value =50;
                        break;
                    default:
                        value =1;
                        break;


                }
                return value;
            },
            getInitialValue:function(task_id){

                var value =100;

                 switch (task_id) {
                     case DC_TASK_1:
                         value =100;
                         break;
                     case DC_TASK_2:
                         value =50;
                         break;
                     case DC_TASK_3:
                         value =2;
                         break;
                      default:
                         value =1;
                         break;


                 }
                 return value;
            },
            getTaskAFactor:function(task_id){

                var value =1;
                switch (task_id) {
                    case DC_TASK_1:
                        value =1;
                        break;
                    case DC_TASK_2:
                        value =2;
                        break;
                    case DC_TASK_3:
                        value =3;
                        break;
                    case DC_TASK_4:
                        value =4;
                        break;
                    case DC_TASK_5:
                    case DC_TASK_6:
                        value =5;
                        break;

                    default:
                        value =1;
                        break;


                }
                return value;
            },
            getTaskReward:function(task_id){
                var value =1;

                switch (task_id) {
                    case DC_TASK_1:
                        value =500;
                        break;
                    case DC_TASK_2:
                        value =1000;
                        break;
                    case DC_TASK_3:
                        value =2000;
                        break;
                    case DC_TASK_4:
                        value =5000;
                        break;
                    case DC_TASK_5:
                        value =10000;
                        break;
                    case DC_TASK_6:
                        value =20000;
                        break;

                    default:
                        value =1;
                        break;


                }
                return value;
            },
            getTaskHeading:function(task_id){
                var value ="Play %s spins with minimum bet of %s";
                switch (task_id) {
                    case DC_TASK_1:
                        value ="Play %s spins with minimum bet of %s";
                        break;
                    case DC_TASK_2:
                        value ="Win %s times with minimum bet of %s";
                        break;
                    case DC_TASK_3:
                        value ="Score Big win %s times with minimum bet of %s";
                        break;
                    case DC_TASK_4:
                        value ="Score %s in total wins";
                        break;
                    case DC_TASK_5:
                        value ="Score %s in total wins";
                        break;
                    case DC_TASK_6:
                        value ="Score 10X win %s times with minimum bet of %s";
                        break;

                    default:
                        value ="Play %s spins with minimum bet of %s";
                        break;


                }
                return value;
            },
            refreshDailyChallengesRecord:function (isNew,dailyChallMap) {
               var milestoneRandom = [DC_TASK_1,DC_TASK_2,DC_TASK_3,DC_TASK_4,DC_TASK_5,DC_TASK_6];
                this._dailyChallengeTasks = [];
                if (isNew) {
                    milestoneRandom = DPUtils.getInstance().shuffle(milestoneRandom,0,milestoneRandom.length);
                    var userType = this.getUserType();
                    var valueVector =[];
                    var dataAdded = false;
                    for (var idx = 0; idx < milestoneRandom.length; idx++) {
                        var dailyTask = new DailyChallengeTask();
                        var task_id = milestoneRandom[idx];

                        dailyTask._id = idx;
                        dailyTask.task_id = task_id;
                        dailyTask.userType = userType;
                        dailyTask.isActive = idx == 0; // active state first task

                        //task sequence order number (1,2,3,4,5,6)
                        var currentIndexTask = idx+1;
                        var initalWaitage = this.getInitialValue(task_id) * this.getTaskAFactor(currentIndexTask);
                        var minimumBet = DC_TASK_Y_FACTOR*this.getTaskXfactor(userType);
                        if (hackBuild){
                            initalWaitage =1;
                            minimumBet =1;
                        }
                        if(task_id == DC_TASK_5){
                            initalWaitage *=DC_TASK_Z_FACTOR*this.getTaskXfactor(userType);
                             minimumBet = 0;
                            if (hackBuild){
                                initalWaitage =1;
                            }
                             var data =  this.getTaskHeading(task_id);
                             var heading = data.replace("%s", DPUtils.getInstance().abbreviateNumber(initalWaitage));
                             dailyTask.taskHeading = heading;
                        }else  if(task_id == DC_TASK_4){

                            initalWaitage *=DC_TASK_Z_FACTOR*this.getTaskXfactor(userType)*this.getTaskAFactor(userType);
                            minimumBet = 0;
                            if (hackBuild){
                                initalWaitage =1;
                            }
                            var data =  this.getTaskHeading(task_id);
                            var heading = data.replace("%s", DPUtils.getInstance().abbreviateNumber(initalWaitage));
                            dailyTask.taskHeading = heading;

                        }else{
                             var value;
                            if(task_id == DC_TASK_1){
                                value ="Play "+ DPUtils.getInstance().abbreviateNumber(initalWaitage) +" spins with minimum bet of "+DPUtils.getInstance().abbreviateNumber(minimumBet);

                            }else if(task_id == DC_TASK_2){
                                value ="Win "+ DPUtils.getInstance().abbreviateNumber(initalWaitage) +" times with minimum bet of "+DPUtils.getInstance().abbreviateNumber(minimumBet);

                            }else if(task_id == DC_TASK_3){
                                value ="Score Big win "+ DPUtils.getInstance().abbreviateNumber(initalWaitage) +" times with minimum bet of "+DPUtils.getInstance().abbreviateNumber(minimumBet);

                            }else{
                                value ="Score 10X win "+ DPUtils.getInstance().abbreviateNumber(initalWaitage) +" times with minimum bet of "+DPUtils.getInstance().abbreviateNumber(minimumBet);
                            }
                            dailyTask.taskHeading = value;

                        }
                        dailyTask.initalWaitage = initalWaitage;
                        dailyTask.minimumBet = minimumBet;
                        dailyTask.reward = this.getTaskReward(currentIndexTask)*this.getTaskXfactor(userType);
                        valueVector.push(dailyTask.getDailyChallengeTaskData());
                        this._dailyChallengeTasks.push(dailyTask);
                        dataAdded = true;

                    }
                    if (dataAdded){
                        ServerData.getInstance().updateChallengesStr( valueVector ,true);

                    }
                }else{
                    if (dailyChallMap && dailyChallMap.length>0){
                        for (var idx = 0; idx < dailyChallMap.length; idx++) {
                            var dailyTask = new DailyChallengeTask().init(dailyChallMap[idx]);
                            this._dailyChallengeTasks.push(dailyTask);
                        }

                        if(this._dailyChallengeTasks.length){
                            var i, j;
                            var n= this._dailyChallengeTasks.length;
                            for (i = 0; i < n-1; i++){
                                // Last i elements are already in place
                                for (j = 0; j < n-i-1; j++){
                                    var task1 = this._dailyChallengeTasks[j];
                                    var task2 = this._dailyChallengeTasks[j+1];

                                    if (task1._id > task2._id){

                                        var b = this._dailyChallengeTasks[j+1];
                                        this._dailyChallengeTasks[j+1] = this._dailyChallengeTasks[j];
                                        this._dailyChallengeTasks[j] = b;

                                    }
                                }


                              }
                            //this._dailyChallengeTasks.sort();
                        }
                    }else{
                        this.refreshDailyChallengesRecord(true,null);
                    }
                }

            },
            updateUserDailyChallgesRecord:function (task_index,winningAmt,betAmt) {
                var isUpdate = false;
                for (var idx = 0; idx < this._dailyChallengeTasks.length; idx++) {
                    var dailyTask =  this._dailyChallengeTasks[idx];
                    if(dailyTask.isActive && dailyTask.task_id == task_index){
                        if(betAmt >= dailyTask.minimumBet && dailyTask.initalWaitage > dailyTask.progress){
                            isUpdate = this.isCurrentTaskUpdated(dailyTask,task_index,winningAmt);
                        }
                        break;
                    }
                }

                if(isUpdate){
                    this.updateDailyChanllengeRecord();
                }
            },
            isCurrentTaskUpdated:function (dailyTask,task_id,winningAmt) {
                switch (task_id) {
                    case C_PLAY_1000_SPINS:
                    case C_BIG_WIN_TIMES:
                    case C_10X_WIN_TIMES:
                    case C_WIN_200_MORE_SPIN:
                        dailyTask.progress = dailyTask.progress+1;
                        if(dailyTask.progress >= dailyTask.initalWaitage){
                          dailyTask.progress = dailyTask.initalWaitage;
                            var scene = cc.director.getRunningScene();
                            if ( scene && scene.getTag() == sceneTags.GAME_SCENE_TAG )
                            {
                                var game = scene;
                                game.showDailyTaskCompleteBarStatus(dailyTask.taskHeading);
                            }
                       // NDKManager::getInstance()->showNotificationWithCompleteTask(dailyTask->taskHeading);

                    }
                        break;
                    case C_WIN_1M_IN_TOTAL_WIN:
                    case C_WIN_M_IN_TOTAL_WIN:
                        dailyTask.progress = dailyTask.progress + winningAmt;
                        if(dailyTask.progress >= dailyTask.initalWaitage){
                        dailyTask.progress = dailyTask.initalWaitage;
                        var scene = cc.director.getRunningScene();
                            if ( scene && scene.getTag() == sceneTags.GAME_SCENE_TAG )
                            {
                                var game = scene;
                                game.showDailyTaskCompleteBarStatus(dailyTask.taskHeading);
                            }
                        //NDKManager::getInstance()->showNotificationWithCompleteTask(dailyTask->taskHeading);
                    }
                        break;
                    default:
                        break;
                }
                return true;

            },
            updateDailyChanllengeRecord:function () {
               var valueVector = [];
                for (var idx = 0; idx < this._dailyChallengeTasks.length; idx++) {
                    valueVector.push(this._dailyChallengeTasks[idx].getDailyChallengeTaskData());
                }
                ServerData.getInstance().updateChallengesStr( valueVector,false);

            },
            isMileStoneAchived:function () {
                for (var idx = 0; idx < this._dailyChallengeTasks.length; idx++) {
                    var dailyTask =  this._dailyChallengeTasks[idx];
                    if(dailyTask.isActive){
                        if(dailyTask.initalWaitage <= dailyTask.progress){
                            return true;
                        }
                        return false;
                    }
                }
                return false;
            },
            getNumberOfTaskCompletionDailyChallenge:function () {
                var  status = 1;
                for (var idx = 0; idx < this._dailyChallengeTasks.length; idx++) {
                    var dailyTask =  this._dailyChallengeTasks[idx];
                    if(dailyTask.progress >= dailyTask.initalWaitage && dailyTask.collected ){
                        status++;
                    }
                }
                return status;
            },
            getStatusOfCurrentDailyChallenge:function () {
                var status =0.0;
                for (var idx = 0; idx < this._dailyChallengeTasks.length; idx++) {
                    var dailyTask =  this._dailyChallengeTasks[idx];
                    if(dailyTask.isActive){
                        status = dailyTask.progress*100/dailyTask.initalWaitage;
                        break;
                    }
                }
                return status;
            }
        };
    };
    return {
        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function () {
            if ( !instance ) {
                instance = init();
            }
            return instance;
        }
    };
})();