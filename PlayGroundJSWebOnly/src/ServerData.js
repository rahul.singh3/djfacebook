

var hackBuild = false;
var APP_ID = "1085075762";
var APP_NAME = "Double Jackpot Slots";
var APP_VERSION = "1.19";
var BUILD_VERSION = "20";
var DEVELOPMENT_CODE = 2;//todo
var DEVICE_TYPE = 3;
var LOGGED_AS = "facebook";
var SECURITY_TOKEN = "phonato$12345$";
var session_token="";
var SERVER_UNDER_MAINTENANCE = 100;
var INVALID_USER_NAME = 101;
var INVALID_SESSION_TOKEN = 209;
var INVALID_USER = 706;
var SPIN_SERVICE_FAILURE = 1001;
var TIME_OUT = 1002;
var INTERNET_CONNECTION_LOST = 1003;
var SERVICE_LOGIN = 0;
var SERVICE_APP_CONFIG = 1;
var SERVICE_PING = 2;
var SERVICE_SPIN = 3;
var UPDATE_CUSTOM_DATA = 4;
var VERIFY_RECEIPT = 5;
var CHECK_OFFER = 6;
var CHECK_FBID = 7;

var timeOutArray = [ 15, 15, 5, 5, 5, 15, 2 ,60];
var ServerData = (function () {
    // Instance stores a reference to the Singleton
    var instance;

    function init() {
        // Singleton
        // Private methods and variables
        function privateMethod() {
            //console.log( "I am private" );
        }

        var privateVariable = "Im also private";
        var privateRandomNumber = Math.random();
        return {
            offer_id:"",
            offer_check:"",
            offer_message:"",
            offer_coins:0,
            device_model:"",
            device_name:"",
            device_version:"",
            configCode:0,
            maintenance:0,
            machineData:0,
            control:0,
            TRICKY_MACHINE_CODE:0,
            BROWSER_TYPE:0,
            Machine_Code:0,
            maintenance_duration:0,
            SERVER_UNDER_MAINTENANCE_DATA:"",
            FB_APP_PAGE_ID:"",
            server_buy_page : 3,
            server_country : "",
            server_real_money_url : "",
            server_real_money_show : 0,
            //ShowHappyHoursPop : false,
            server_happy_hours : 0,
            server_buypopup_frequency : 0,
            server_false_win : 15,
            server_make_him_win : 0,
            server_strategy : 0,
            server_deal_buyer_flag : "1",
            server_deal_buyer_image : "",
            server_deal_buyer_coins : "",
            server_deal_buyer_product : "",
            server_deal_nonbuyer_flag : "1",
            server_deal_nonbuyer_image : "",
            server_deal_nonbuyer_coins : "",
            server_deal_nonbuyer_product : "",
            server_featured_machine_order : "",
            server_progressive_jackpot : 0,
            server_purchase_frequency : 0,
            purchase_strategy : 1,
            currentTime : 0,
            progressive_jackpot_winning_count : 0,
            server_after_xgames_show_specialpopup : 50,
            server_show_special_popup : false,
            server_frequency_of_ads : 0,
            server_cdn_url : "",//https://dl.dropboxusercontent.com/u/29590393/vintageslots/profiles/",
            //"https://821a81b6dcb072ef8ea8-65c2fa4d1e4a51c8af8aef4a980ad9f6.ssl.cf2.rackcdn.com/Public/vintageslots/profiles/",
            server_machine_tunning_level : DEFAULT_EASY_MODE_BET_LIMIT,
            server_machine_tunning_buyer : 1,
            server_machine_tunning_loyal_user: 1,
            server_buyer_strategy : 3,
            server_occasion : UserDef.getInstance().getValueForKey( SERVER_OCCASION, 0 ),
            isShareEnabled : [0,0,0,0],//change 12 May
            server_win_animation_control : [0, 0, 0],
            server_level_array : [0, 0, 0, 2, 5, 8, 10, 12, 15, 22, 30, 40, 50, 55, 70, 80, 90, 110, 135, 160, 190, 220, 260, 300, 350,410,480,560],
            server_machine_order:[
                DJS_QUINTUPLE_5X,
                DJS_SPIN_TILL_YOU_WIN,
                DJS_FIRE,
                DJS_FREEZE,
                DJS_LIGHTNING_REWIND,
                DJS_MAGNET_SLIDE,
                DJS_WILD_X,
                DJS_DOUBLE_DIAMOND,
                DJS_TRIPLE_PAY,
                DJS_BONUS_MACHINE,
                DJS_FREE_SPINNER,
                DJS_RNF_STAR,
                DJS_HALLOWEEN_SLOT,
                DJS_X_MULTIPLIERS,
                DJS_DIAMOND_3X,
                DJS_NEON_SLOT,
                DJS_RESPIN,
                DJS_SPIN_TILL_YOU_WIN_TWIK,
                DJS_FREE_SPIN,
                DJS_DIAMONDS_FOREVER,
                DJS_CRAZY_CHERRY,
                DJS_LIGHTNING_REWIND_TWIK,
                DJS_DIAMOND,
                DJS_CRYSTAL_SLOT,
                DJS_GOLDEN_SLOT],
            server_current_version:0.0,
            server_show_version_check_popup:false,
            server_later_mandatory_update_status:0,
            server_mandatory_update_coins:0,
            biasing_status:0,
            server_hot_machine_order :"",
            server_new_machine_order :"",

            server_machine_unavailable:"",
            server_notify_control:0,
            server_bribe_popup:false,
            server_after_xgames_show_bribepopup:0,
            server_paid_wheel_control:0,
            server_inner_wheel_control:0,
            server_salepopup_frequency:0,
            server_win_control:0,
            big_win_control:0,
            special_notification_coins:0,
            server_bet_factor_3reel:20,
            ping_time:0,
            isAppratingShown:false,
            daily_bonus_coins:0,
            hourly_bonus_coins:0,
            reward_videos_see_count:0,
            bonus_max_consecutive_days:0,
            email_id:"",
            fbId:"",
            userLink:"",
            name:"",
            userLocale:"",
            userTimeZone:"",
            current_time_seconds:0,
            serviceFailureCountArray : [],
            serviceFailureReAttemptsArray : [ 10, 3, 4, 2, 2, 0, 0 ],
            timeTicker : function()
            {
                //console.log("timeTicker");
                instance.current_time_seconds++;
                if (instance.configCode === 1 && instance.current_time_seconds % instance.ping_time === 0) {
                        instance.pingServer();
                }
            },
            updateUserInfoData:function(nm, eId, gen, db, fId, link, locale, timezone){
                this.name = nm;
                this.email_id = eId;
                this.gender = gen;
                UserDef.getInstance().setValueForKey("userEmail",eId);
                this.fbId = fId;
                this.userLink = link;
                this.userLocale = locale;
                this.userTimeZone = timezone;
            },
            loadConfigData:function () {
                console.log("loadConfigData  request");

                DPUtils.getInstance().detectClient(window);
                var xhr = cc.loader.getXMLHttpRequest();
                xhr.open("GET", APP_CONFIG_URL, true);
                xhr.setRequestHeader('Content-type', 'application/json');
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 300)) {
                        instance.serviceFailureCountArray[ SERVICE_APP_CONFIG ] = 0;
                        var response = JSON.parse(xhr.responseText);
                        var servericeUrl = response["SERVER_BASE_URL"];
                        if (!servericeUrl.includes("https", 0)){
                            instance.SERVER_BASE_URL = servericeUrl.replace("http", "https");
                        }else{
                            instance.SERVER_BASE_URL = servericeUrl;
                        }
                        instance.SERVER_UNDER_MAINTENANCE_DATA = response["SERVER_UNDER_MAINTENANCE_DATA"];
                        instance.APPLE_APP_ID = response["APPLE_APP_ID"];
                        instance.ASSET_URL = response["ASSET_URL"];
                        instance.FB_APP_PAGE_ID = response["FB_APP_PAGE_ID"];
                        var nameList = instance.SERVER_UNDER_MAINTENANCE_DATA.split(':',2);
                        if (nameList.length>1){
                            instance.maintenance  = parseInt(nameList[0]);
                            instance.maintenance_duration = parseInt(nameList[1]);
                        }
                        AppDelegate.getInstance().configInTicker =1;
                        instance.serviceFailureCountArray[ SERVICE_APP_CONFIG ] =0;
                        instance.loginRequest();

                    }else if (xhr.readyState === 4){
                        console.log("loadConfigData  request failed");
                        instance.loadConfigData();
                        instance.handleSpinFailure(SERVICE_APP_CONFIG);
                    }
                };
                xhr.send();
            },
            encodedJsonData:function(data){
                var str = '';
                for (var i in data) {
                    if (data.hasOwnProperty(i)) {
                        if (str.length) str += '&';
                        str += encodeURIComponent(i) + '=' + encodeURIComponent(data[i]);
                    }
                }

                return str;
                },

            callService:function(dataJson,strUrl,callback){
                if (instance.gameStopped ){
                    return;
                }

                axios.post(strUrl,this.encodedJsonData(dataJson), {headers: {'Accept': 'application/json'}}).then(function (response) {
                    var valueMap = response.data;
                    if (typeof callback == "function"){
                        callback(valueMap);
                    }

                 }).catch(function (response) {
                    console.log(response);
                    });
            },

            handleLoginservice:function(valueMap){
                if (valueMap && valueMap["result"] == 1) {
                    if (instance.internetErrorOnLoginOccured===1){
                        instance.internetErrorOnLoginOccured =0;
                        if( cc.director.getRunningScene().getChildByTag( NO_CONN_LAYER ) )
                        {
                            cc.director.getRunningScene().removeChildByTag(NO_CONN_LAYER);
                        }
                    }
                    instance.serviceFailureCountArray[ SERVICE_LOGIN ]=0;

                    console.log(valueMap["result"]);
                    userDataInfo.getInstance().setUserDefaultConfigureData(valueMap["user_info"]);
                    instance.current_time_seconds = userDataInfo.getInstance().seconds_since_user_created;
                    session_token = valueMap["session_token"];
                    var app_config = valueMap["app_config"];
                    if(app_config!=null){
                        instance.setAppConfigureData(app_config);
                    }
                    //userEmail
                    var mail = UserDef.getInstance().getValueForKey("userEmail");
                    support_url = SUPPORT_BASE_URL + "email=" + mail + "&app_id=" + APP_ID + "&user_id=" + userDataInfo.getInstance().user_id;
                    UserDef.getInstance().setValueForKey("showSaleViewCount",0);
                    ServerData.getInstance().configCode=1;
                    instance.checkAndHandleOffer(false);

                }else{
                   // ServerData.getInstance().configCode =1;
                    console.log("loginRequest  failed");


                }
            },
            loginFbChecker:function(){
                if (instance.gameStopped)return;
                instance.internetErrorOnLoginOccured =1;
                instance.handleSpinFailure(CHECK_FBID);
                instance.loginRequest();

            },
            loginRequest:function(){
                console.log("loginRequest  request");
                if (this.fbId === undefined || !this.fbId || this.fbId.length===0){
                    AppDelegate.getInstance().logInTicker=1;
                    return;
                }
                instance.serviceFailureCountArray[ CHECK_FBID ] =0;

                AppDelegate.getInstance().logInTicker=2;

                var source = "app_page";
                if( DPUtils.getInstance().getAllUrlParams().fb_source )
                {
                    source = DPUtils.getInstance().getAllUrlParams().fb_source;
                }
                else if( DPUtils.getInstance().getAllUrlParams().offer_id )
                {
                    source = "offer";
                }
                var userdef = UserDef.getInstance();
                var memorycheck = window.performance.memory;
                if (memorycheck){
                    if (memorycheck.totalJSHeapSize){
                        memorycheck = parseInt( memorycheck.totalJSHeapSize/(1024*1024));
                    }else{
                        memorycheck = '';
                    }
                }else{
                    memorycheck = '';

                }

                var dataJson = {
                    'app_id': APP_ID,
                    'app_name': APP_NAME,
                    'security_token': SECURITY_TOKEN,
                    'device_type': DEVICE_TYPE,
                    'logged_as': LOGGED_AS,
                    'development': DEVELOPMENT_CODE,
                    'facebook_id': hackBuild ? '852642308217879' : this.fbId,
                    'email_id': hackBuild ? 'iosrahul64@gmail.com' : this.email_id,
                    'user_id': userdef.getValueForKey("user_id",""),
                    'device_version': this.device_model?this.device_model:'',//device_version
                    'device_model':this.device_version?this.device_version:'',
                     'device_name':this.device_name?this.device_name:'',
                    'app_version' :APP_VERSION,
                    'name':this.name?this.name:'',
                    'gender':this.gender?this.gender:'',
                    'media_source':source,
                    'device_ram':memorycheck?memorycheck:'0',

                };
                this.callServiceSpinWithAllRes(dataJson,instance.SERVER_BASE_URL+'login',this.handleLoginservice);
            },
            setAppConfigureData:function(app_config){
                var userdef = UserDef.getInstance();
                this.server_buy_page = parseInt(app_config["buy_page"]);
                this.server_buyer_strategy = parseInt(app_config["strategy"]); //----
                this.server_purchase_frequency = parseInt(app_config["purchase_frequency"]);
                this.buy_page_offer_new_user = parseInt(app_config["buy_page_offer_new_user"]);
                this.server_machine_tunning_level = parseInt(app_config["machine_tuning_level"]);
                this.server_machine_tunning_buyer = parseInt(app_config["machine_tuning_buyer"]);
                this.server_machine_tunning_loyal_user = parseInt(app_config["machine_tuning_loyal_user"]);
                userdef.setValueForKey(SERVER_OCCASION , parseInt(app_config["occasion"] ));
                this.server_current_version = app_config["current_version"];
                this.server_show_version_check_popup = app_config["current_version_check"];
                this.server_later_mandatory_update_status = app_config["later_mandatory_update_status"];
                this.server_mandatory_update_coins = parseInt(app_config["mandatory_update_coins"]);
                this.biasing_status = app_config["biasing_status"]?1:0;
                var server_level_array1 =app_config["level_array"];
                var doShareStr = app_config["enable_sharing"];//change 12 May
                for ( var i = 0; i < server_level_array1.length; i++ )
                {
                    var curChar = server_level_array1[i];
                    this.server_level_array[i] = parseInt( curChar );
                }

                for ( var i = 0; i < doShareStr.length; i++ )
                {
                    var curChar = doShareStr[i];
                    this.isShareEnabled[i] = parseInt( curChar );
                }
                var server_win_animation_control_str = app_config[ "win_animation_control" ];
                for ( var i = 0; i < server_win_animation_control_str.length; i++ )
                {
                    var curChar = server_win_animation_control_str[i];
                    this.server_win_animation_control[i] = parseInt( curChar );
                }
                this.server_hot_machine_order = app_config["featured_machine_order"];
                this.server_new_machine_order = app_config["new_machine_order"];
                this.server_machine_unavailable = app_config["machine_unavailable"];
                userdef.setValueForKey(SERVER_MACHINE_UNAVAILABLE, this.server_machine_unavailable );
                this.server_notify_control = app_config["notify_control"];
                var tmpVar = app_config["show_special_popup"];
                this.server_bribe_popup = tmpVar == "1" ? true : false;
                this.server_after_xgames_show_bribepopup = parseInt(app_config["after_xgames_show_specialpopup"]);
                this.server_paid_wheel_control = parseInt(app_config["paid_wheel"]);
                this.server_inner_wheel_control = parseInt(app_config["wheel_control"]);
                this.server_happy_hours = parseInt(app_config["happy_hour"]);
                this.server_strategy = parseInt( app_config["strategy"] );

                this.server_salepopup_frequency = parseInt(app_config["buypopup_frequency"]);
                this.server_buyer_strategy = parseInt(app_config["buyer_strategy"]);
                // Real Money Variables
                this.server_real_money_url = app_config["real_money_url"];
                this.server_real_money_show = parseInt(app_config["real_money_show"]);
                this.big_win_control = parseInt(app_config["big_win_control"]);
                //this.server_win_animation_control = app_config["win_animation_control"];
                this.server_win_control = parseInt(app_config["win_control"]);
                this.server_false_win = parseInt(app_config["false_win"]);
                var bet_factorStr = app_config["bet_control"];
                userdef.setValueForKey( everUsedSpecialPopup, false );
                if(bet_factorStr!=null){
                    this.server_bet_factor_3reel = parseInt(bet_factorStr);
                    if (!this.server_bet_factor_3reel){
                        var valuet = bet_factorStr["reel_3"];
                        if (valuet){
                            this.server_bet_factor_3reel = parseInt(valuet);
                        }
                    }
                }// Bonus amoumts
                this.special_notification_coins = app_config["special_notification_coins"];
                this.daily_bonus_coins = parseInt(app_config["daily_bonus_coins"]);
                this.hourly_bonus_coins = parseInt(app_config["hourly_bonus_coins"]);
                this.reward_videos_see_count = app_config["reward_videos_see_count"];
                this.bonus_max_consecutive_days = parseInt(app_config["bonus_max_consecutive_days"]);
                //delete
                var instant_deals = app_config["instant_deals"];
                if (instant_deals){
                    InstantDealModel.getInstance().refreshdataModel(instant_deals);
                    if(app_config["instant_deal_params"]){
                        InstantDealModel.getInstance().setinstant_deal_paramsData(app_config["instant_deal_params"]);

                    }
                }
                BonusTimer.getInstance();
                userDataInfo.getInstance().updateWheelControlData();
                instance.setHappyHoursStrategy();
                // deal Non_buyer Relative Variables
                this.server_deal_nonbuyer_flag = parseInt(app_config["deal_non_buyer_flag"]);
                this.server_deal_nonbuyer_image = app_config["deal_non_buyer_image"];
                this.server_deal_nonbuyer_coins = parseInt(app_config["deal_non_buyer_coins"]);
                this.server_deal_nonbuyer_product = parseInt(app_config["deal_non_buyer_product"]);

                // deal Buyer Relative Variables
                this.server_deal_buyer_flag = parseInt(app_config["deal_buyer_flag"]);
                this.server_deal_buyer_image = app_config["deal_buyer_image"];
                this.server_deal_buyer_coins = parseInt(app_config["deal_buyer_coins"]);
                this.server_deal_buyer_product = parseInt(app_config["deal_buyer_product"]);
                var deal = AppDelegate.getInstance().getDealInstance();
                deal.setDealData(0);
                this.current_time_seconds =0;
                this.ping_time = parseInt(app_config["ping_time"]);
                this.ping_time /= 1000;
                this.setLoyalUserStrategyAndFirstTimeuser();
                this.setMachineFactorAndWinStrategy();
                var delegate = AppDelegate.getInstance();
                if ( delegate.getTime( HAPPY_HOURS, -1 ) == -1 )
                {
                    var t = ServerData.getInstance().current_time_seconds;

                    if (  delegate.getTime( NEXT_HAPPY_HOUR_IN ) - t <= 0)
                    {
                        if ( this.server_happy_hours > 0 )
                        {
                            delegate.showHappyHoursPop = true;
                        }
                    }
                }
            },
            setMachineFactorAndWinStrategy:function(){
                if (this.server_win_control>0 && this.server_win_control<100) {
                    this.server_win_control = 100;
                }

                if ( this.server_false_win != 0 && this.server_false_win <= 2 ) {
                    this.server_false_win = 2;
                }

                // 28/05/2018 -- server_bet_factor_three_reel (min 25 spin per user) only used in 3 reel machine
                if( this.server_bet_factor_3reel < 20){
                    this.server_bet_factor_3reel = 20;
                }else if (!this.server_bet_factor_3reel){
                    this.server_bet_factor_3reel = 20;
                }

                this.setNewBuyPageStrategy();



                //server_buy_page
            },
            setNewBuyPageStrategy:function(){
                var d = new Date();
                var day_no = d.getDay();
                var  hrs = d.getHours();
                if(this.server_buy_page == 4 && ( ( day_no == 5 && hrs >= 18 ) || day_no == 6 || day_no == 0 || day_no == 1 ) )
                    this.server_buy_page = 4;
                else if( this.server_buy_page == 4 )
                    this.server_buy_page = 3;

                if (this.buy_page_offer_new_user && !UserDef.getInstance().getValueForKey( IS_BUYER,false) && this.server_buy_page <= 3)
                    this.server_buy_page = 2;




            },
            setLoyalUserStrategyAndFirstTimeuser:function(){
                var userdef = UserDef.getInstance();

                if (this.server_machine_tunning_loyal_user >=1 && !userdef.getValueForKey(IS_BUYER,false) && userDataInfo.getInstance().session_count%this.server_machine_tunning_loyal_user==0){
                    if ( this.server_machine_tunning_level && CSUserdefauts.getInstance().getValueForKey(EASY_MODE_BET,-1)<=0){
                        if ( this.server_machine_tunning_level < DEFAULT_EASY_MODE_BET_LIMIT ) {
                            this.server_machine_tunning_level = DEFAULT_EASY_MODE_BET_LIMIT;
                            CSUserdefauts.getInstance().setValueForKey(EASY_MODE_BET,this.server_machine_tunning_level);
                        }
                    }
                  }else if(!CSUserdefauts.getInstance().getValueForKey(EASY_MODE_ASSIGNED,false)){
                    CSUserdefauts.getInstance().setValueForKey(EASY_MODE_ASSIGNED,true);
                    if ( this.server_machine_tunning_level && this.server_machine_tunning_level < DEFAULT_EASY_MODE_BET_LIMIT ) {
                        this.server_machine_tunning_level = DEFAULT_EASY_MODE_BET_LIMIT;
                    }
                    CSUserdefauts.getInstance().setValueForKey(MINIMUM_EASY_MODE_BET_LIMIT,this.server_machine_tunning_level );

                }
            },
            map_to_object : function(map)
            {
                var out = Object.create(null)

                map.forEach( function ( value, key ) {
                    if (value instanceof Map)
                    {
                        out[key] = instance.map_to_object(value)
                    }
                    else
                    {
                        out[key] = value
                    }
                })
                return out;
            },
            machineData:null,
            loadSlotData : function(machineId)
            {
                var dataJson = {
                    'user_id': userDataInfo.getInstance().user_id,
                    'session_token': session_token,
                    'machine_id': machineId,
                };
                instance.Machine_Code = machineId;
                instance.callService(dataJson,instance.SERVER_BASE_URL+"game_user/machine/get",this.slotDataCallBack);

            },
            slotDataCallBack:function(valueMap) {
                if (valueMap && valueMap["result"] === 1  && valueMap["machine"]) {
                    ServerData.getInstance()._pingFailCount = 0;
                    var slot_config = valueMap["machine"];
                    var scene = cc.director.getRunningScene();

                    if (parseInt(slot_config["machine_id"]) == ServerData.getInstance().Machine_Code){
                        ServerData.getInstance().machineData = slot_config;
                        if ( scene && scene.getTag() == sceneTags.GAME_SCENE_TAG )
                        {
                            var game = scene;
                            game.updateServerData();
                        }
                      //  console.log("Machine Loaded"+slot_config);

                    }


                }
            },

            _pingFailCount:0,
            gameStopped :false,
            internetErrorOccured:0,
            internetErrorOnLoginOccured:0,
            pingServer:function(){
                ServerData.getInstance()._pingFailCount++;
                if (userDataInfo.getInstance().user_id.length>0) {
                    var dataJson = {
                        'user_id': userDataInfo.getInstance().user_id,
                        'session_token': session_token,
                    }
                    axios.post(instance.SERVER_BASE_URL+"game_user/ping",this.encodedJsonData(dataJson), {headers: {'Accept': 'application/json'}}).then(function (response) {
                        var pingData = response.data;
                        if (pingData["result"]==1){
                            console.log("ping request Success");
                            ServerData.getInstance()._pingFailCount=0
                            instance.gameStopped = false;
                            userDataInfo.getInstance().isInAppVerified = pingData["is_inapp_verified"]==1?true:false;
                            if (instance.internetErrorOccured===1){
                                instance.internetErrorOccured = 0;
                                if( cc.director.getRunningScene().getChildByTag( NO_CONN_LAYER ) )
                                {
                                    cc.director.getRunningScene().removeChildByTag(NO_CONN_LAYER);
                                }
                            }
                        }else if (pingData["error_code"]==INVALID_USER){
                            instance.stopGame(INVALID_USER);
                        }else{
                            console.log("ping request failed");

                        }

                    }).catch(function (response) {
                         if (response.isAxiosError){
                             instance.internetErrorOccured = 1;
                             instance.stopGame(INTERNET_CONNECTION_LOST);

                         }
                        //console.log(response);
                       // console.log("ping request Exception");

                    });
                }

            },
            updateChallengesStr:function(valueVector,isFreshdata){
                var data = JSON.stringify(valueVector);
                var dataJson = {
                    'user_id': userDataInfo.getInstance().user_id,
                    'session_token': session_token,
                    'daily_challenges_string':data,
                    'is_to_update':isFreshdata?'True':'False',
                }
                this.callServiceSpinWithAllRes(dataJson,instance.SERVER_BASE_URL+"game_user/daily_challenge",this.handleDcResponse);

            },
            instantDealViewAndBuy:function(dealid , isView){
                var dataJson = {
                    'user_id': userDataInfo.getInstance().user_id,
                    'session_token': session_token,
                    'instant_deal_id':dealid,
                    'field':isView?"view_count" : "buy_count",
                }
                this.callServiceSpinWithAllRes(dataJson,instance.SERVER_BASE_URL+"game_user/instant_deal",this.handleDcResponse);

            },
            handleDcResponse:function(response){
                if (response && response["result"]==1){
                   // console.log(response["handleDcResponse result"]);
                } else{
                   // console.log(response);
                }
            },
            updateReelSpin:function( LastWinAmt , currentPayoutMode , isSpinWin10x , _slotCallBackFn,  lastBetAmount , extra_round)
             {
                 var userdef = UserDef.getInstance();
                 var jsonVar = ServerData.getInstance().map_to_object(CSUserdefauts.getInstance().storageContainer);


                 var dataJson = {

                     'user_id': userDataInfo.getInstance().user_id,
                     'session_token': session_token,
                     'app_id': APP_ID,
                     'app_name': APP_NAME,
                     'max_bet_placed': userdef.getValueForKey(MAX_BET_PLACED),
                     'maximum_coins_reached': userdef.getValueForKey("maximumcoinsreached"),
                     'total_coins': userDataInfo.getInstance().totalChips,
                     'bet_amount': lastBetAmount?lastBetAmount:0,
                     'win_amount': LastWinAmt,
                     'last_played_theme': userdef.getValueForKey("last_played_theme","DJS_QUINTUPLE"),
                     'win_10x': isSpinWin10x?"1":"0",
                     'extra_round':extra_round,
                     'level':userdef.getValueForKey(LEVEL,"0"),
                     'vip_points':userdef.getValueForKey(VIP_POINTS,"0"),
                     'progressive_jackpot_winning_count':userDataInfo.getInstance().progressive_jackpot_winning_count,
                     'custom_data':JSON.stringify(jsonVar),
                     'machine_current_running_array':"payout_array_"+currentPayoutMode,

                 };
                 this.callServiceSpinWithAllRes(dataJson,instance.SERVER_BASE_URL+"game_user/spin",this.handleReelSpinData);
              },
            callServiceSpinWithAllRes:function(dataJson,strUrl,callback){
                if (instance.gameStopped ){
                    return;
                }
                axios.post(strUrl,this.encodedJsonData(dataJson), {headers: {'Accept': 'application/json'}}).then(function (response) {
                    var valueMap = response.data;
                    if (typeof callback == "function"){
                        callback(valueMap);
                    }

                }).catch(function (response) {
                    var valueMap = response.data;
                    if (typeof callback == "function"){
                        callback(valueMap);
                    }
                });
            },
            handleReelSpinData:function(response){
                    if (response && response["result"]==1){
                        instance.serviceFailureCountArray[ SERVICE_SPIN ] = 0;
                        var machine_tuning = response["machine_tuning"];
                        UserDef.getInstance().setValueForKey(MACHINE_TUNNING , machine_tuning );
                    } else{
                     instance.handleSpinFailure(SERVICE_SPIN);
                    }
            },
            handleSpinFailure : function( serviceCode )
            {
                instance.serviceFailureCountArray[ serviceCode ]++;
                if( instance.serviceFailureCountArray[ serviceCode ] > instance.serviceFailureReAttemptsArray[ serviceCode ] )
                {
                    instance.stopGame( INTERNET_CONNECTION_LOST, undefined, serviceCode );
                }else
                {
                    var sprite = new cc.Sprite( "res/noConnection/wifi_1.png" );
                    var walkAnimFrames = new Array();
                    var walkAnim = null;

                    for( var i = 1; i < 5; i++ )
                    {
                        var tmp = new cc.SpriteFrame( "res/noConnection/wifi_" + i + ".png" , cc.rect(0, 0, sprite.getContentSize().width, sprite.getContentSize().height));
                        walkAnimFrames.push( tmp );
                    }

                    for( var i = 4; i > 0; i-- )
                    {
                        var tmp = new cc.SpriteFrame( "res/noConnection/wifi_" + i + ".png" , cc.rect(0, 0, sprite.getContentSize().width, sprite.getContentSize().height));
                        walkAnimFrames.push( tmp );
                    }

                    walkAnim = new cc.Animation(walkAnimFrames, 0.1);
                    //walkAnim.setLoops(-1);
                    var tmpAnimate = cc.animate(walkAnim);
                    sprite.setOpacity( 0 );
                    sprite.runAction( new cc.Sequence( new cc.FadeIn( 0.1 ), new cc.Repeat( tmpAnimate, 2 ),
                        new cc.FadeOut( 0.1 ), cc.callFunc( function ( sprite ){
                            sprite.removeFromParent( true );
                        }, this ) ) );

                    sprite.setPosition( cc.winSize.width - sprite.getContentSize().width * 0.5, cc.winSize.height - sprite.getContentSize().height );
                    cc.director.getRunningScene().addChild( sprite, 50 );
                }
            },
            updatefavouriteMachinesOnServer:function(currentMachine,addOrRemove){
                var dataJson = {
                    'user_id': userDataInfo.getInstance().user_id,
                    'session_token': session_token,
                    'machine_id':currentMachine,
                    'action':addOrRemove,
                }
                this.callServiceSpinWithAllRes(dataJson,instance.SERVER_BASE_URL+"game/user/machines/favourite",this.handleDcResponse);
            },
            updateActiveMultipleReplaceOnServer:function(dataJson){
                this.updateData("", "", JSON.stringify(dataJson) ,"multiplereplace");
            },
            updateActiveGameMachineOnServer:function(currentActiveGame,currentMachine){
                var dataJson = {
                    'active_game': currentActiveGame,
                    'active_machine': currentMachine,
                };
                //this.callService(dataJson, instance.SERVER_BASE_URL + "game_user/update_data", this.serverdataCallBack);

                this.updateData("", "", JSON.stringify(dataJson) ,"multiplereplace");

            },
            updateCustomDataOnServer:function(){
                if (userDataInfo.getInstance().user_id && userDataInfo.getInstance().user_id.length){
                    var jsonVar = ServerData.getInstance().map_to_object(CSUserdefauts.getInstance().storageContainer);
                    this.updateData("custom_data", JSON.stringify(jsonVar),""  ,"singlereplace");
                }

            },
            updateData:function( columnName,  columnValFinal,  data,  action ){
               if (userDataInfo.getInstance().user_id.length>0) {
                   var dataJson = {
                       'user_id': userDataInfo.getInstance().user_id,
                       'session_token': session_token,
                       'action': action?action:"singlereplace",
                       'column': columnName?columnName:"",
                       'data': data?data:"",
                       'column_value': columnValFinal?columnValFinal:0,
                       'app_version' :APP_VERSION,

                   };
                   this.callService(dataJson, instance.SERVER_BASE_URL + "game_user/update_data", this.serverdataCallBack);
               }
            },
            inAppJson:null,
            updateInAppOnEmergencyServer:function(dataJsonV){
                this.callServiceSpinWithAllRes(dataJsonV,"http://rnfiphone.com/inappservice/index.php/v2/slots/emergency_inapp/verify_receipt",this.inAppCallBackEmergency);
            },
            inAppCallBackEmergency:function(valueMap) {
                if (valueMap && valueMap["result"] == 1) {
                    IAPHelper.getInstance().setVariablesFromJSON(1);
                    cc.log("inAppCallBackEmergency");
                }else{
                    cc.director.getRunningScene().removeChildByTag( LOADER_TAG, true );
                }

            },
            updateInAppOnServer:function(productId,productPrice,chips,dealNo,transId,IapVipPts){
                var dataJson = {
                    'user_id': userDataInfo.getInstance().user_id,
                    'session_token': session_token,
                    'app_id': APP_ID,
                    'app_name': APP_NAME,
                    'device_type': DEVICE_TYPE,
                    'product_id': productId,
                    'product_price': productPrice,
                    'chips': chips,
                    'app_version': APP_VERSION,
                    'is_deal': dealNo,
                    'development': DEVELOPMENT_CODE,
                    'is_inapp_verified': userDataInfo.getInstance().isInAppVerified? "1" : "0",
                    'transaction_id':transId,
                    'vip_points':IapVipPts,
                };
                if (userDataInfo.getInstance().isInAppVerified){
                    IAPHelper.getInstance().setVariablesFromJSON(1);
                }
                instance.inAppJson = dataJson;

                this.callServiceSpinWithAllRes(dataJson,instance.SERVER_BASE_URL+"game_user/inapp/verify_receipt",this.inAppCallBack);
            },
            inAppCallBack:function(valueMap) {
                if (valueMap && valueMap["result"] == 1 && !userDataInfo.getInstance().isInAppVerified) {
                    IAPHelper.getInstance().setVariablesFromJSON(1);
                }else{
                    cc.director.getRunningScene().removeChildByTag( LOADER_TAG, true );

                    //instance.updateInAppOnEmergencyServer(instance.inAppJson);
                }

            },
            setHappyHoursStrategy:function(){

            },
            updateDealActive:function( dealSecs,  productId,  chips, requestType){
                var dataJson = {
                    'user_id': userDataInfo.getInstance().user_id,
                    'session_token': session_token,
                    'request_type': requestType,
                    'deal_active_seconds': dealSecs,
                    'deal_product_id':productId,
                    'deal_reward_chips':chips,
                };
                this.callService(dataJson,instance.SERVER_BASE_URL+"game_user/deal",this.serverdataCallBack);

            },
            collectBonus:function(bonusType,coinsToIncrease){
                var dataJson = {
                    'user_id': userDataInfo.getInstance().user_id,
                    'session_token': session_token,
                    'bonus_type': bonusType,
                    'coins': coinsToIncrease,
                };
                this.callService(dataJson,instance.SERVER_BASE_URL+"game_user/bonus/collect",this.serverdataCallBack);

            },

            serverdataCallBack:function(valueMap){
                if (valueMap && valueMap["result"] == 1) {


                }
            },
            hitServerWithCodeAndCallBack:function (_offerCallBackFn,offerid,offermode) {
                var dataJson = {
                    'user_id': userDataInfo.getInstance().user_id,
                    'session_token': session_token,
                    'app_id': APP_ID,
                    'offer_id': offerid,
                };
                var strAppendUrl ="game/user/offer/collect";
                if (offermode == VERIFICATION) {
                    strAppendUrl ="game/user/offer/verify";
                }
             this.callService(dataJson,instance.SERVER_BASE_URL+strAppendUrl,_offerCallBackFn);
            },
            checkAndHandleOffer:function (comeFromLobby) {
                if( !DPUtils.getInstance().getAllUrlParams().offer_id )
                {
                    instance.configCode = 1;
                    return;
                }
                ServerData.getInstance().offer_check = DPUtils.getInstance().getAllUrlParams().offer_id.toUpperCase();
                ServerData.getInstance().hitServerWithCodeAndCallBack(this.offerCallBackFromServer, ServerData.getInstance().offer_check, VERIFICATION)

            },
            offerCallBackFromServer:function(response){
                if (response && response["result"] == 1) {
                    ServerData.getInstance().offer_id = ServerData.getInstance().offer_check;
                    var coins = response["coins"];
                    if (coins>0){
                        ServerData.getInstance().offer_coins =   coins;
                        PopUps.getInstance().addPopUpIndex(PopUpTags.STAY_TOUCH_ONE_LINK_POP);
                    }
                }else if (response && response["result"] == 2){
                    ServerData.getInstance().offer_id ="";
                    ServerData.getInstance().offer_message = response["message"];
                    PopUps.getInstance().addPopUpIndex(PopUpTags.STAY_TOUCH_ONE_LINK_POP);
                }
                else{
                    ServerData.getInstance().offer_id ="";
                }
            },
            stopGame : function( errCode, time, serviceCode )
            {
                instance.gameStopped = true;
                AppDelegate.getInstance().jukeBox( S_STOP_EFFECTS );
                AppDelegate.getInstance().jukeBox( S_STOP_MUSIC );
                showFacebookLikeBtn( false );
                if( cc.director.getRunningScene().getChildByTag( NO_CONN_LAYER ) )
                {
                    return;
                }
                var layer = new cc.Layer();//Color( cc.color( 0, 0, 0, 166 ) );
                layer.setTag( NO_CONN_LAYER );
                layer.setPosition( cc.p( 0, 0 ) );
                DPUtils.getInstance().setTouchSwallowing( this, layer );

                var darkBG = new cc.Sprite( res.Dark_BG_png );
                darkBG.setScale( 2 );
                darkBG.setOpacity( 169 );
                darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
                layer.addChild( darkBG );

                var visibleSize = cc.winSize;

                var bg;
                switch( errCode )
                {
                    case INTERNET_CONNECTION_LOST :
                        bg = new cc.Sprite( "res/noConnection/noConnectionBg.png" );
                        break;

                    default:
                        bg = new cc.Sprite( "res/noConnection/serverNonResponsive.png" );
                        break;
                }
                bg.setPosition( visibleSize.width * 0.5, visibleSize.height * 0.5 );
                layer.addChild( bg );
                if (errCode == INVALID_USER){
                    var refreshItem = new cc.MenuItemImage( "res/noConnection/retry_button.png", "res/noConnection/retry_button.png", function ( psenfer ) {
                        window.location.reload(true);
                    }, cc.director.getRunningScene() );
                    refreshItem.setPosition( visibleSize.width * 0.5, cc.rectGetMinY( bg.getBoundingBox() ) + refreshItem.getContentSize().height * 0.75 );

                    var menu = new cc.Menu( refreshItem );
                    menu.setPosition( cc.p( 0, 0 ) );
                    layer.addChild( menu );
                }

                DPUtils.getInstance().setTouchSwallowing( null, layer );
                cc.director.getRunningScene().addChild( layer, 50 );
                if (errCode == INVALID_USER) {
                    cc.director.pause();
                }
            }

        };

    };
    return {
        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function () {
            if ( !instance ) {
                instance = init();
                instance.serviceFailureCountArray = [ 0, 0, 0, 0, 0, 0, 0,0 ];
                instance.loadConfigData();
            }
            return instance;
        }
    };
})();
