/**
 * Created by rnftech-sunny on 3/23/17.
 */
var tournamentWin = cc.Layer.extend({

    winBase:null,
    collectButton:null,
    rays:null,
    visibleSize:null,
    call2:null,
    listen2:null,
    appdel:null,
    doShare : true,
    ctor:function () {
        this._super();
        this.init();
        return true;
    },

    init:function()
    {
        this.visibleSize =cc.winSize;
        this.appdel=AppDelegate.getInstance();
        this.doShare = ServerData.getInstance().isShareEnabled[SHARE_TOURNAMENT_WIN];
        this.createPop();
    },
    createPop:function()
    {
        var  bg=new cc.Layer();//Color(cc.color(0, 0, 0, 210));

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function(sender,event) {
                return true;
            },
            onTouchEnded: function(sender,event) {
            }
        }, bg);


        // auto bg=new cc.Sprite("cst_bet_bg.png");
        // bg.setPosition(origin.x+this.visibleSize.width*0.5, origin.y+this.visibleSize.height*0.5));
        this.winBase=new cc.Sprite("res/tournamentWinBg.png");
        this.winBase.setPosition(this.visibleSize.width*0.5, +this.visibleSize.height*0.5);
        this.collectButton=new ccui.Button("res/collect_btn.png");
        this.collectButton.setPosition( this.winBase.getPositionX(), cc.rectGetMinY(this.winBase.getBoundingBox()) + this.collectButton.getContentSize().height * 0.4 );
        this.collectButton.loadTextures("res/collect_btn.png", "res/collect_btn.png");
        // star=new cc.Sprite("level/cst_level_star.png");
        //star.setPosition(levelBase.getPositionX(),  levelBase.getBoundingBox().getMaxY()*1.2));

        //this.rays=new cc.Sprite("common/level/cst_gold_rays.png");
        //this.rays.setPosition(this.winBase.getPositionX(), this.winBase.getPositionY());

        //var action =cc.rotateBy(1.5, 360);
        //this.rays.runAction(cc.repeatForever(action));

        this.collectButton.addTouchEventListener(this.touchButon, this);
        this.addChild(bg);

        var darkBG = new cc.Sprite( res.Dark_BG_png );
        darkBG.setScale( 2 );
        darkBG.setOpacity( 169 );
        darkBG.setPosition( cc.p( cc.winSize.width * 0.5, cc.winSize.height * 0.5 ) );
        bg.addChild( darkBG );

        //this.addChild(this.rays);
        this.addChild(this.winBase);
        //this.addChild(star);
        this.addChild(this.collectButton);
        this.createEffects();

        if( ServerData.getInstance().isShareEnabled[SHARE_TOURNAMENT_WIN] )
        {
            var shareItem = new cc.MenuItemImage( res.share_bg_png, res.share_bg_png, this.menuCB, this );
            //this.resizer.setFinalScaling( shareItem );
            shareItem.setPosition( this.winBase.getPositionX(), this.collectButton.getPositionY() + this.collectButton.getContentSize().height * 0.5 + shareItem.getContentSize().height * 0.1 );

            var tick = new cc.Sprite( res.tick_png );
            tick.setPosition( cc.p( tick.getContentSize().width * 0.75, tick.getContentSize().height * 0.75 ) );
            tick.setTag( 1 );
            shareItem.addChild( tick );

            var menu = new cc.Menu();
            menu.setPosition( cc.p( 0, 0 ) );
            menu.addChild( shareItem );
            this.addChild( menu );
        }
        //shareItem.setTag( SETTING_BTN_TAG );

        DPUtils.getInstance().setTouchSwallowing( null, bg );
    },
menuCB : function( ref )
{
    this.doShare = !this.doShare;
    ref.getChildByTag( 1 ).setVisible( this.doShare );

},
    createEffects:function()
    {
        var sun1 = new cc.ParticleSmoke();
        var  sun2 = new cc.ParticleSmoke();

        sun1.setPosition(cc.rectGetMaxX(this.winBase.getBoundingBox())-300, cc.rectGetMinY(this.winBase.getBoundingBox()) );
        sun2.setPosition(  cc.rectGetMaxX(this.winBase.getBoundingBox())+300, cc.rectGetMinY(this.winBase.getBoundingBox()));
        //sun1.setScale(0.6);
        //sun2.setScale(0.6);
        sun1.setTexture( cc.textureCache.addImage("res/tournament/cst_purpleShine.png") );
        sun2.setTexture( cc.textureCache.addImage("res/tournament/cst_yellowShine.png") );

        sun2.setVisible(false);

        //this.appdel.jukeBox(MachineId.TOURNAMENT);
        var mTo1 = cc.moveTo( 0.5, cc.rectGetMinX( this.winBase.getBoundingBox())-100, cc.rectGetMaxY(this.winBase.getBoundingBox())-200);
        var mTo2 = cc.moveTo(0.5, cc.rectGetMinX( this.winBase.getBoundingBox())+100,cc.rectGetMaxY(this.winBase.getBoundingBox())-200);
        sun1.runAction( cc.sequence( mTo1, cc.callFunc(function(){
            var exp = new cc.ParticleExplosion();

            exp.setTexture( cc.textureCache.addImage("res/tournament/cst_purpleShine.png") );
            exp.setPosition( sun1.getPosition() );
            this.addChild( exp );
            sun1.removeFromParent( true );


        },this)));

        sun2.runAction(  cc.sequence( cc.delayTime( 0.7 ),cc.callFunc(function(){
            //this.appdel.jukeBox(MachineId.TOURNAMENT);
            sun2.setVisible(true);

        },this), mTo2, cc.callFunc(function(){

            var exp1 = new cc.ParticleExplosion();
            exp1.setTexture( cc.textureCache.addImage("res/tournament/cst_yellowShine.png") );
            exp1.setPosition( sun2.getPosition() );
            this.addChild( exp1 );
            sun2.removeFromParent( true );
        },this)));

        this.addChild( sun1);
        this.addChild( sun2);
    },
    touchButon:function( sender,  ev)
    {
        if(ev== ccui.Widget.TOUCH_ENDED)

        {

            // UserDefault::getInstance().setIntegerForKey(VIP_POINTS,(int)appdel.VIPpoints);
            //appdel.VIPUpdate();

            this.removeFromParent(true);
            this.call2.call(this.call2,this.listen2);

            if( this.doShare )
            {
                //AppDelegate.getInstance().shareOnFB( " Won Tournament!! " );
                FacebookObj.shareOnTimeLine( FacebookObj.userFbName + " Won Slots Tournament!","https://apps.facebook.com/doublejackpotslots/?fb_source=feed",
                    "https://7starslots.com/facebook_inapp_verification/doublejackpot/share_folder/tournament_trophy.jpg", "Double Jackpot Slots!", "Play Double Jackpot Slots and win huge jackpot! Join me at the Casino Now!"  );//#Jitu
            }


        }

    },

    setCallBack2:function(target, func)
    {
        this.listen2=target;
        this.call2=func;

    }
});
