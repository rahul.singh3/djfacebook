/**
 * Created by RNF-Mac11 on 2/27/17.
 */

var Tournament_1 = 1;
var Tournament_2 = 2;
var Tournament_3 = 3;

CSTournament = cc.Node.extend({

    m : 5,
    s : 0,
    prizeAmt : 0,
    win : 0,
    TournamentId : 0,
    finalPlayerRank : 0,
    _finalPrizePool : 0,
    remainSec : 0,
    _currentCount : 0,
    appdel : null,
    minBet : 0,
    maxBet : 0,
    _minSpinInAMin : 96 / 5,
    _maxSpinInAMin : 270 / 5,
    _totalLines : 1,
    _slotsWinPerc : 0.97,
    _tourTimeInSec : 300,
    _tourDelayTimeInSec : 0,

    _botsUpdateTimeInSec : 5,
    _poolUpdateTimeInSec : 10,
    _totalPlayers : 0,

    _minBetMod : 0,
    _targetWin : 0,

    _currPrizePool : 0,

    imageMapping : new Array(),

    _isNewRoundStarted : 0,


    _userClickCounter: 0,

    _playersView : null,
    labelmin : null,
    labelsec : null,
    labelcolon : null,
    prizePool : null,
    labelPrizeAmt : null,
    betRangeStatic : null,
    betRangeVar : null,
    _players : null,
    rankingBase : null,

    myrandom : function(i)
    {
        return Math.floor( Math.random() * i );
    },

    Timer : function( dt )
    {
        this._currentCount++;
        this.remainSec = this._tourTimeInSec - this._currentCount;

        if (this.remainSec)
        {
            if ((this.remainSec % this._poolUpdateTimeInSec) == 0)
                this.updateCurrentPrizePool(this.remainSec / this._poolUpdateTimeInSec);
            if ((this.remainSec % this._botsUpdateTimeInSec) == 0)
                this.updateBotsPoints(this.remainSec / this._botsUpdateTimeInSec);
        }

        this.s = this.s - 1;
        if( this.s == -1 )
        {
            if( this.m )
            {
                this.m = this.m - 1;

                this.s = 59;
            }
            else
            {
                this.m = 0;
                this.s = 0;
            }
        }

        var mm = "0" + this.m;

        if ( this.m < 1 && this.s < 11 ) {
            this.labelcolon.setVisible(false);
            this.labelmin.setVisible(false);
            this.labelsec.setPosition(this.labelcolon.getPosition());
            this.labelsec.setColor( cc.color( 255, 0, 0 ) );
            this.labelsec.setString( "" + this.s );
        }
        else
        {
            var secStr = ( this.s < 10 ) ? "0" + this.s : "" + this.s;

            this.labelmin.setString( mm );
            this.labelsec.setString( secStr );

        }

        if( this.m == 0 && this.s == 0)
        {
            this.unschedule( this.Timer );
            this.appdel.showNewTournament = true;

            var game = cc.director.getRunningScene()/*.getChildByTag( sceneTags.GAME_SCENE_TAG )*/;
            if (game.getTag() == sceneTags.GAME_SCENE_TAG )
            {
                game.setTournamentOver();
            }
        }
    },
    tournamentView : function()
    {
        this.rankingBase = new cc.Sprite("#cst_ranking_base.png");
        this.rankingBase.setPosition( cc.p( this.rankingBase.getContentSize().width * 0.5 + 5.0, 123 + this.rankingBase.getContentSize().height * 0.5 + 1.75 ) );


        this.labelmin = new cc.LabelTTF( "0" + this.m, "DS-DIGIT", 30 );
        this.labelmin.setPosition( cc.p( this.rankingBase.getContentSize().width * 0.5 - 25, this.rankingBase.getContentSize().height * 0.9 ) );
        this.labelmin.setColor( cc.color( 255, 255, 255 ) );
        this.addChild( this.labelmin );


        this.labelsec = new cc.LabelTTF( "0" + this.s, "DS-DIGIT", 30);
        this.labelsec.setPosition( cc.p( this.rankingBase.getContentSize().width * 0.5 + 25, this.labelmin.getPositionY() ) );
        this.labelsec.setColor( cc.color( 255, 255, 255 ) );

        this.addChild( this.labelsec );


        this.labelcolon = new cc.LabelTTF( ":", "DS-DIGIT", 30 );
        this.labelcolon.setPosition( cc.p( this.rankingBase.getContentSize().width * 0.5, this.labelmin.getPositionY() ) );
        this.labelcolon.setColor( cc.color( 255, 255, 255 ) );
        this.addChild(this.labelcolon);

        var fontSize = 25;

        switch (this.getTournamentId())
        {
            case 1:
                this.prizePool = new cc.Sprite("#cst_prize_pool_red.png");
                this.prizePool.setPosition( cc.p( this.prizePool.getContentSize().width * 0.5, cc.rectGetMinY( this.labelmin.getBoundingBox() ) - this.prizePool.getContentSize().height * 0.5 ) );
                this.addChild(this.prizePool);

                this.labelPrizeAmt = new cc.LabelTTF( DPUtils.getInstance().getNumString( this._currPrizePool ), "TT0504M_", fontSize);
                this.labelPrizeAmt.setPosition(cc.p( this.rankingBase.getContentSize().width * 0.5, cc.rectGetMinY( this.prizePool.getBoundingBox() ) + fontSize ));
                this.labelPrizeAmt.setColor( cc.color( 255, 255, 0 ) );
                this.addChild( this.labelPrizeAmt );

                this.betRangeVar = new cc.LabelTTF( "Up to 500 Bet", "TT0504M_", 20);
                this.betRangeVar.setPosition(cc.p( this.rankingBase.getContentSize().width * 0.5, cc.rectGetMinY( this.prizePool.getBoundingBox() ) - this.betRangeVar.getContentSize().height*1.75));
                this.addChild(this.betRangeVar);
                break;

            case 2:
                this.prizePool = new cc.Sprite("#cst_prize_pool_blue.png");
                this.prizePool.setPosition( cc.p( this.prizePool.getContentSize().width * 0.5, cc.rectGetMinY( this.labelmin.getBoundingBox() ) - this.prizePool.getContentSize().height*0.5));
                this.addChild(this.prizePool);
                this.labelPrizeAmt = new cc.LabelTTF( DPUtils.getInstance().getNumString(this._currPrizePool), "TT0504M_", fontSize);
                this.labelPrizeAmt.setPosition(cc.p(this.rankingBase.getContentSize().width*0.5, cc.rectGetMinY(this.prizePool.getBoundingBox())+ fontSize ));
                this.labelPrizeAmt.setColor( cc.color( 255, 255, 0 ) );


                this.addChild(this.labelPrizeAmt);
                this.betRangeVar = new cc.LabelTTF("1000 - 10000 Bet", "TT0504M_", 20);
                //betRangeVar=Sprite::create("tournament/cst_1000-5000_bet.png");
                this.betRangeVar.setPosition(cc.p(this.rankingBase.getContentSize().width*0.5, cc.rectGetMinY(this.prizePool.getBoundingBox())-this.betRangeVar.getContentSize().height*1.75));
                this.addChild(this.betRangeVar);

                break;

            case 3:
                this.prizePool = new cc.Sprite("#cst_prize_pool_green.png");
                this.prizePool.setPosition(cc.p(this.prizePool.getContentSize().width * 0.5, cc.rectGetMinY(this.labelmin.getBoundingBox())-this.prizePool.getContentSize().height*0.5));
                this.addChild(this.prizePool);
                this.labelPrizeAmt = new cc.LabelTTF( DPUtils.getInstance().getNumString(this._currPrizePool), "TT0504M_", fontSize);
                this.labelPrizeAmt.setPosition(cc.p(this.rankingBase.getContentSize().width*0.5, cc.rectGetMinY(this.prizePool.getBoundingBox())+ fontSize));
                this.labelPrizeAmt.setColor(cc.color( 255, 255, 0 ));


                this.addChild(this.labelPrizeAmt);
                this.betRangeVar = new cc.LabelTTF("Over 50000 Bet", "TT0504M_", 20);
                //betRangeVar=Sprite::create("tournament/cst_over_5000_bet.png");

                this.betRangeVar.setPosition(cc.p(this.rankingBase.getContentSize().width*0.5, cc.rectGetMinY(this.prizePool.getBoundingBox())-this.betRangeVar.getContentSize().height*1.75));
                this.addChild(this.betRangeVar);

                break;

            default:
                break;
        }

        this.betRangeStatic=new cc.LabelTTF("BET RANGE", "TT0504M_", 15);
        this.betRangeStatic.setPosition(cc.p(this.rankingBase.getContentSize().width*0.5, cc.rectGetMinY(this.prizePool.getBoundingBox())-this.betRangeStatic.getContentSize().height));
        this.betRangeStatic.setColor( cc.color( 169, 169, 169 ));
        this.addChild(this.betRangeStatic);
    },
    updateTournamentResults : function( dt )
    {
        var data = ServerData.getInstance();
        //var url = data.server_cdn_url;

        for( var a = 0; a < this._players.length - 1; a++ )
        {
            var it = this._players[a]
            var imgIndx;
            //cc.log( "it._id = " + it._id + ", it._rank = " + it._rank );
            if( it._id == 100 )
            {
                if( it._rank == 100 )
                {
                    //cc.log( "a = " + a );
                    var u = this._playersView[2];
                    var img = u._img;
                    imgIndx = this._players[a - 2]._imageIndex;
                    u._rank = this._players[a - 2]._rank;
                    u._points = this._players[a - 2]._points;

                    u.points.setString( DPUtils.getInstance().getNumString( u._points ) );
                    u.rank.setString( "" + u._rank );
                    u.rank.setColor( cc.color( 255, 255, 255 ) );
                    /*TTFConfig config = u.rank.getTTFConfig();
                    config.fontSize = 27.5;
                    u.rank.setTTFConfig( config );*/

                    img.removeFromParent(true);
                    var rsp2 = AppDelegate.getInstance().remoteSprite( DPUtils.getInstance().TournamentURLgalaxy(imgIndx), u ,"res/tournament/cst_loading_person.png");
                    u.addChild(rsp2);
                    rsp2.setPosition( cc.rectGetMinX( this.getChildByTag( 1002 ).getBoundingBox() ) + rsp2.getContentSize().width * 0.63, cc.rectGetMaxY( this.getChildByTag( 1002 ).getBoundingBox() ) - rsp2.getContentSize().height * 0.75 );

                    u._img = rsp2;

                    u.circle.setVisible(false);

                    u = this._playersView[1];
                    u._rank = this._players[a - 1]._rank;
                    u._points = this._players[a - 1]._points;
                    u.points.setString( DPUtils.getInstance().getNumString( u._points ) );
                    u.rank.setString( "" + u._rank );
                    u.rank.setColor( cc.color( 255, 255, 255 ) );
                    /*config = u.rank.getTTFConfig();
                    config.fontSize = 27.5;
                    u.rank.setTTFConfig( config );*/

                    u.circle.setVisible(false);

                    //updating image
                    img = u._img;
                    imgIndx = this._players[a - 1]._imageIndex;

                    img.removeFromParent(true); //removing already existing image of person from its parent (playerView)
                    var rsp1 = AppDelegate.getInstance().remoteSprite( DPUtils.getInstance().TournamentURLgalaxy(imgIndx), u, "res/tournament/cst_loading_person.png" );
                    u.addChild(rsp1);
                    rsp1.setPosition(cc.rectGetMinX( this.getChildByTag(1001).getBoundingBox() ) + rsp1.getContentSize().width * 0.63, cc.rectGetMaxY( this.getChildByTag( 1001 ).getBoundingBox() ) - rsp1.getContentSize().height * 0.75 );
                    u._img = rsp1;

                    //myPlayer
                    u = this._playersView[0];

                    u._points = it._points;

                    u.points.setString( DPUtils.getInstance().getNumString( u._points ) );
                    if ( u._points == 0 )
                    {

                        var str = "Win\n To\nRank";

                        u.rank.setString( str );
                    }
                    else
                    {
                        if ( u._rank != it._rank )
                        {
                            u.rank.runAction( new cc.Sequence( new cc.ScaleTo( 0.5, 1.5 ), new cc.ScaleTo( 0.5, 1.0 ) ) );
                        }
                        u._rank = it._rank;

                        u.rank.setString( "" + u._rank );
                        u.rank.setColor( cc.color( 255, 255, 0 ) );
                        /*TTFConfig config = u.rank.getTTFConfig();
                        config.fontSize = 37.5;
                        u.rank.setTTFConfig( config );*/
                    }

                    this.finalPlayerRank = it._rank;  // setting up final rank of our player

                    img = u._img;


                    var texture = this.setUserFbImage();
                    if ( texture ) {
                        img.setTexture( texture );
                        //texture.release();
                    }
                    else
                    {
                        img.setTexture( "res/tournament/cst_person_male.png" );
                    }

                    u._img = img;
                    u.circle.setVisible(false);
                }
            else  if( it._rank == 1 )
                {
                    var u = this._playersView[2];
                    var img = u._img;

                    //myplayer

                    if (u._rank != it._rank ) {
                    u.rank.runAction( new cc.Sequence( new cc.ScaleTo( 0.5, 1.5 ), new cc.ScaleTo( 0.5, 1.0 ) ) );
                }

                    u._rank = it._rank;
                    u.rank.setString( "" + u._rank );
                    u.rank.setColor( cc.color( 255, 255, 0 ) );
                    /*TTFConfig config = u.rank.getTTFConfig();
                    config.fontSize = 37.5;
                    u.rank.setTTFConfig( config );*/

                    u._points = it._points;
                    u.points.setString( DPUtils.getInstance().getNumString( u._points ) );

                    this.finalPlayerRank = it._rank;  // setting up final rank of our player
                    var texture = this.setUserFbImage();
                    if (texture) {
                        img.setTexture(texture);
                        //texture.release();
                    }
                    else
                    {
                        img.setTexture( "res/tournament/cst_person_male.png" );
                    }

                    u.circle.setVisible(true);
                    u.circle.setSpriteFrame("cst_greenCircle.png");



                    u = this._playersView[1];
                    u._rank = this._players[a + 1]._rank;
                    u._points = this._players[a + 1]._points;
                    u.points.setString( DPUtils.getInstance().getNumString( u._points ) );
                    u.rank.setString( "" + u._rank );
                    u.rank.setColor( cc.color( 255, 255, 255 ) );
                    /*config = u.rank.getTTFConfig();
                    config.fontSize = 27.5;
                    u.rank.setTTFConfig( config );*/

                    img = u._img;
                    imgIndx = this._players[a + 1]._imageIndex;

                    img.removeFromParent(true);
                    var rsp1 = AppDelegate.getInstance().remoteSprite( DPUtils.getInstance().TournamentURLgalaxy(imgIndx), u, "res/tournament/cst_loading_person.png" ); //rsp denotes remote sprite at playerview(2)
                    u.addChild(rsp1);
                    rsp1.setPosition( cc.rectGetMinX( this.getChildByTag( 1001 ).getBoundingBox() ) + rsp1.getContentSize().width * 0.63, cc.rectGetMaxY( this.getChildByTag( 1001 ).getBoundingBox() ) - rsp1.getContentSize().height * 0.75 );

                    u._img = rsp1;
                    u.circle.setVisible(true);
                    u.circle.setSpriteFrame("cst_greenCircle.png");


                    u = this._playersView[0];
                    u._rank = this._players[a + 2]._rank;
                    u._points = this._players[a + 2]._points;
                    u.points.setString( DPUtils.getInstance().getNumString( u._points ) );
                    u.rank.setString( "" + u._rank );
                    u.rank.setColor( cc.color( 255, 255, 255 ) );
                    /*config = u.rank.getTTFConfig();
                    config.fontSize = 27.5;
                    u.rank.setTTFConfig( config );*/

                    img=u._img;
                    imgIndx = this._players[a + 2]._imageIndex;


                    img.removeFromParent(true);
                    var rsp0 = AppDelegate.getInstance().remoteSprite( DPUtils.getInstance().TournamentURLgalaxy(imgIndx), u, "res/tournament/cst_loading_person.png"); //rsp denotes remote sprite at playerview(2)
                    u.addChild(rsp0);
                    rsp0.setPosition(cc.rectGetMinX(this.getChildByTag( 1000 ).getBoundingBox() ) + rsp0.getContentSize().width * 0.63, cc.rectGetMaxY(this.getChildByTag( 1000 ).getBoundingBox() ) - rsp0.getContentSize().height * 0.75 );

                    u._img = rsp0;

                    u.circle.setVisible(true);
                    u.circle.setSpriteFrame("cst_greenCircle.png");
                }
            else
                {
                    var u = this._playersView[2];
                    var img = u._img;
                    u._rank = this._players[a - 1]._rank;
                    u._points = this._players[a - 1]._points;
                    u.points.setString( DPUtils.getInstance().getNumString( u._points ) );
                    u.rank.setString( "" + u._rank );
                    u.rank.setColor( cc.color( 255, 255, 255 ) );
                    /*TTFConfig config = u.rank.getTTFConfig();
                    config.fontSize = 27.5;
                    u.rank.setTTFConfig( config );*/

                    imgIndx = this._players[a - 1]._imageIndex;

                    img.removeFromParent(true);
                    var rsp2 = AppDelegate.getInstance().remoteSprite( DPUtils.getInstance().TournamentURLgalaxy(imgIndx), u, "res/tournament/cst_loading_person.png" ); //rsp denotes remote sprite at playerview(2)
                    u.addChild(rsp2);
                    rsp2.setPosition(cc.rectGetMinX( this.getChildByTag( 1002 ).getBoundingBox() ) + rsp2.getContentSize().width * 0.63, cc.rectGetMaxY( this.getChildByTag( 1002 ).getBoundingBox() ) - rsp2.getContentSize().height * 0.75 );

                    u._img = rsp2;


                    if ( u._rank <= 10 ) {
                        u.circle.setVisible(true);
                        u.circle.setSpriteFrame("cst_greenCircle.png");
                    }
                    else if( u._rank > 10 && u._rank <= 30 ){

                        u.circle.setVisible(true);
                        u.circle.setSpriteFrame("cst_blueCircle.png");

                    }
                    else if(u._rank > 30 && u._rank <= 50){

                        u.circle.setVisible(true);
                        u.circle.setSpriteFrame("cst_redCircle.png");

                    }
                    else
                    {
                        u.circle.setVisible(false);
                    }


                    //myplayer
                    u = this._playersView[1];

                    if ( u._rank != it._rank ) {
                    u.rank.runAction( new cc.Sequence( new cc.ScaleTo( 0.5, 1.5 ), new cc.ScaleTo( 0.5, 1.0 ) ) );
                }

                    u._rank = it._rank;
                    u.rank.setString( "" + u._rank );
                    u.rank.setColor( cc.color( 255, 255, 0 ) );
                    /*config = u.rank.getTTFConfig();
                    config.fontSize = 37.5;
                    u.rank.setTTFConfig( config );*/

                    u._points = it._points;
                    u.points.setString( DPUtils.getInstance().getNumString( u._points ) );

                    this.finalPlayerRank = it._rank;  // setting up final rank of our player
                    img = u._img;

                    var texture = this.setUserFbImage();
                    if (texture) {
                        img.setTexture(texture);
                        //texture.release();
                    }
                    else
                    {
                        img.setTexture( "res/tournament/cst_person_male.png" );// Change 12 Dec

                    }

                    u._img = img;
                    if ( u._rank <= 10 ) {
                        u.circle.setVisible(true);
                        u.circle.setSpriteFrame("cst_greenCircle.png");
                    }
                    else if( u._rank > 10 && u._rank <= 30 ){
                        u.circle.setVisible(true);
                        u.circle.setSpriteFrame("cst_blueCircle.png");
                    }
                    else if( u._rank > 30 && u._rank <= 50 ){

                        u.circle.setVisible(true);
                        u.circle.setSpriteFrame("cst_redCircle.png");
                    }
                    else
                    {
                        u.circle.setVisible(false);
                    }



                    u = this._playersView[0];
                    u._rank = this._players[a + 1]._rank;
                    u._points = this._players[a + 1]._points;
                    u.points.setString( DPUtils.getInstance().getNumString( u._points ) );
                    u.rank.setString( "" + u._rank );
                    u.rank.setColor( cc.color( 255, 255, 255 ) );
                    /*config = u.rank.getTTFConfig();
                    config.fontSize = 27.5;
                    u.rank.setTTFConfig( config );*/

                    //image updation
                    img = u._img;
                    imgIndx = this._players[a + 1]._imageIndex;

                    img.removeFromParent(true);
                    var rsp0 = AppDelegate.getInstance().remoteSprite( DPUtils.getInstance().TournamentURLgalaxy(imgIndx), u, "res/tournament/cst_loading_person.png" ); //rsp denotes remote sprite at playerview(2)
                    u.addChild(rsp0);
                    rsp0.setPosition(cc.rectGetMinX( this.getChildByTag( 1000 ).getBoundingBox() ) + rsp0.getContentSize().width * 0.63, cc.rectGetMaxY( this.getChildByTag( 1000 ).getBoundingBox() ) - rsp0.getContentSize().height * 0.75 );

                    u._img = rsp0;


                    if (u._rank <= 10) {
                        u.circle.setVisible(true);
                        u.circle.setSpriteFrame("cst_greenCircle.png");
                    }
                    else if(u._rank > 10 && u._rank <= 30){

                        u.circle.setVisible(true);
                        u.circle.setSpriteFrame("cst_blueCircle.png");

                    }
                    else if(u._rank > 30 && u._rank <= 50){

                        u.circle.setVisible(true);
                        u.circle.setSpriteFrame("cst_redCircle.png");

                    }
                    else
                    {
                        u.circle.setVisible(false);

                    }
                }
            }
        }
    },
    setSch : function()
    {
        this.unschedule( this.Timer );

        this.schedule( this.Timer, 1 );
    },
    setPlayers : function()
    {
        var maxTargetWinForBot = this._targetWin - this._minBetMod;
        var picGrp = Math.floor( Math.random() * 20 );

        var bandRange = 20;
        var targetWinForBot;
        for ( var i = 0; i < 100; i++ )
        {
            this.imageMapping[i] = picGrp*100+i+1;
        }

        var randm = Math.floor( Math.random() * 100 ) + 1;

        this.imageMapping = DPUtils.getInstance().shuffle(this.imageMapping, 0, randm);

        //std::random_shuffle(&imageMapping[randm], &imageMapping[100],myrandom);

        for( var idx = 0; idx < 100; idx++ )
        {
            var p = new CSPlayer();
            p.retain();

            p.set( idx + 1 );
            if( idx == 0 )
                targetWinForBot = Math.floor( ( 100 / 100.0 ) * maxTargetWinForBot );
            else if( idx == 1 )
                targetWinForBot = Math.floor( ( 98 / 100.0 ) * maxTargetWinForBot );
            else  if( idx == 2 )
                targetWinForBot = Math.floor( ( 95 / 100.0 ) * maxTargetWinForBot );
            else if ( idx < bandRange )
                targetWinForBot = Math.floor( ( ( 90 + Math.floor(Math.random() * 5 ) ) / 100.0 ) * maxTargetWinForBot );
            else if (idx < 2 * bandRange )
                targetWinForBot = Math.floor( ( ( 70 + Math.floor(Math.random() * 20 ) ) / 100.0 ) * maxTargetWinForBot );
            else if ( idx < 3 * bandRange )
                targetWinForBot = Math.floor( ( ( 30 + Math.floor(Math.random() * 40 ) ) / 100.0 ) * maxTargetWinForBot );
            else if ( idx < 4 * bandRange )
                targetWinForBot = Math.floor( ( ( 5 + Math.floor(Math.random() * 25 ) ) / 100.0 ) * maxTargetWinForBot );
            else if ( idx < 5 * bandRange )
                targetWinForBot = Math.floor( ( ( 2 + Math.floor(Math.random() * 5 ) ) / 100.0 ) * maxTargetWinForBot );

            if ( this.getTournamentId() == 1 )  //special case for roundingOff
            {
                // log("targetWinBefore for %d---%lld",idx,targetWinForBot);
                if ( targetWinForBot % 100 <= 25 )
                {
                    targetWinForBot = Math.floor( targetWinForBot / this._minBetMod ) * this._minBetMod;
                }
                else if( targetWinForBot % 100 >= 75 )
                {
                    targetWinForBot=( Math.floor( targetWinForBot / this._minBetMod ) * this._minBetMod ) + 100;
                }
                else if ( targetWinForBot % 100 > 25 && targetWinForBot % 100 < 75 )
                {
                    targetWinForBot = ( Math.floor( targetWinForBot / this._minBetMod ) * this._minBetMod ) + 50;
                }
                else
                {
                    targetWinForBot = Math.floor(targetWinForBot / this._minBetMod) * this._minBetMod;
                }
            }
            else
            {
                targetWinForBot = Math.floor(targetWinForBot / this._minBetMod) * this._minBetMod;
            }

            if ( idx == 99 )
            {
                p._imageIndex = 0;
                p._targetWin = this._targetWin;
            }
            else
            {
                p._imageIndex = this.imageMapping[idx];
                p._targetWin = targetWinForBot;
            }
            //log("TargetWinbot %d--%lld",p.getplayerId(),p.getplayerTargetWin());
            this._players.push( p );
        }
    },
    setPlayersView : function()
    {
        for (var i = 0; i < 3; i++) {
            var str;
            // if(appdel.device==IS_IPHONE)
            var c = i + 1;
            str = "#cst_player_box" + c + ".png";


            var sp = new cc.Sprite(str);
            sp.setPosition(cc.rectGetMinX(this.rankingBase.getBoundingBox()) + this.rankingBase.getContentSize().width * 0.5, 35 * 0.5 + sp.getContentSize().height * 0.5 + i * (sp.getContentSize().height + 0.2));
            sp.setTag(/*tournamentPlayerBox.PLAYER_VIEW_1*/1000 + i);
            this.addChild(sp);
            var pv = new CSPlayerView();
            // pv.retain();
            this.addChild(pv);
            var pl = this._players[0];
            var imgIndx = this._players[99 - i]._imageIndex;
            pv.setView(i, imgIndx, sp);
            this._playersView.push(pv);
        }
    },
    setTournamentId : function( id )
    {
        this.TournamentId = id;
    },
    getTournamentId : function()
    {
        return this.TournamentId;
    },


    setFinalData : function(maxBet)
    {
        var tourTimeInMin = this._tourTimeInSec / 60;
        var l_wins = (tourTimeInMin * this._minSpinInAMin * this.maxBet * this._totalLines * this._slotsWinPerc);
        var h_wins = (tourTimeInMin * this._maxSpinInAMin * this.maxBet * this._totalLines * this._slotsWinPerc);

        this._targetWin = l_wins + Math.ceil( cc.rand() % (h_wins - l_wins));
        this._targetWin = Math.floor(this._targetWin / this._minBetMod) * this._minBetMod;

        var randPer = 6 + Math.ceil( cc.rand() % 4);
        this._finalPrizePool = Math.floor((randPer * this._targetWin) / 100.0);
    },

    setCurrentPrizePool : function()
    {
        var randPer = Math.ceil( Math.random() * 5 );
        this._currPrizePool = Math.floor( ( randPer * this._finalPrizePool ) / 100.0);
        this.labelPrizeAmt.setString( DPUtils.getInstance().getNumString( this._currPrizePool ) );
    },
    updateCurrentPrizePool : function( remainingCounts )
    {
        if (remainingCounts > 1)
        {
            var remainingPrizePool = this._finalPrizePool - this._currPrizePool;
            var dPoolVal = Math.floor(remainingPrizePool / remainingCounts);
            var linearPoolVal = Math.floor(this._currPrizePool + dPoolVal);

            var nonLinearFracVal;
            if (remainingCounts > 2)
                nonLinearFracVal = Math.floor( -(dPoolVal / 2.0) + Math.floor(Math.random() * dPoolVal));
            else
                nonLinearFracVal = 0;
            this._currPrizePool = Math.min(this._finalPrizePool, (linearPoolVal + nonLinearFracVal));
        }
        else
            this._currPrizePool = this._finalPrizePool;

        this.labelPrizeAmt.setString( DPUtils.getInstance().getNumString( this._currPrizePool ));
    },
    updateBotsPoints : function( remainingCounts)
    {
        var  remainingPts;
        var  linearChange;
        var  nonLinearFact;
        var  finalPts;

        for(var a = 0; a < this._players.length; a++ )
        {
            var p = this._players[a];
            if(p._id!=100)
            {
                if (remainingCounts > 1)
                {
                    if ( Math.ceil( Math.random() * 4 ) != 1 )
                    {
                        remainingPts = p._targetWin - p._points;
                        linearChange = Math.floor(remainingPts / remainingCounts*1.0);

                        if ( remainingCounts > 2 && linearChange != 0 )
                            nonLinearFact = Math.floor( -linearChange / 2.0 + Math.floor( Math.random() * linearChange));
                        else
                            nonLinearFact = 0;

                        finalPts = p._points + linearChange + nonLinearFact;

                        if (this.getTournamentId()==1)  //special case for roundingOff
                        {
                            if (finalPts % 100 <= 25)
                            {
                                finalPts=Math.floor(finalPts/this._minBetMod) * this._minBetMod;
                            }else if(finalPts % 100 >= 75){
                                finalPts=(Math.floor(finalPts/this._minBetMod) * this._minBetMod)+100;
                            }else if (finalPts % 100 > 25 && finalPts % 100 < 75)
                            {
                                finalPts=(Math.floor(finalPts/this._minBetMod)*this._minBetMod)+50;
                            }
                            else
                            {
                                finalPts = Math.floor(finalPts / this._minBetMod) * this._minBetMod;
                            }
                        }
                        else
                        {
                            finalPts = Math.floor(finalPts / this._minBetMod) * this._minBetMod;
                        }

                        p._points = ( Math.min( p._targetWin, finalPts ) );
                    }
                }
                else
                    p._points = (p._targetWin);

            }
        }

        this.updateRank();
    },
    updateMyPts : function( amt)
    {
        for( var a = 0; a < this._players.length; a++ )
        {
            var p = this._players[a];
            if (p._id==100) {
                p._points = p._points + amt;
            }
        }

        this.updateRank();
        this._userClickCounter++;
    },
    updateRank : function()
    {
        var sel=this;
        this._players.sort(function (lhs, rhs) {
            var diff = rhs._points - lhs._points;
            var v= sel._players.indexOf(lhs);

            if (diff == 0)
                return sel._players.indexOf(lhs) - sel._players.indexOf(rhs);
            return diff;

        });

        var i = 1;
        for (var a = 0; a < this._players.length; a++) {

            this._players[a]._rank = i;
            i++;
        }

        if (this.s >= 0) {
            if (this.m >= 0) {
                this.updateTournamentResults(0);
            }
        }
    },

    setUserFbImage : function()
    {
        if( !AppDelegate.getInstance().fbTexture )
        {
            var url = "https://graph.facebook.com/" + FacebookObj.userFbID + "/picture?width=135&height=135";
            if( hackBuild )
            {
                url = "https://graph.facebook.com/" + 852642308217879 + "/picture?width=135&height=135";
            }

            AppDelegate.getInstance().remoteSprite( url, this ,"res/tournament/cst_person_male.png", true);
        }
        return AppDelegate.getInstance().fbTexture;
    },

    ctor:function ( id ) {
        this._super();
        this.appdel = AppDelegate.getInstance();
        this.setTournamentId(id);
        this._players = new Array();
        this._playersView = new Array();

        this.rankingBase = new cc.Sprite("#cst_ranking_base.png");
        this.rankingBase.setPosition(cc.p(this.rankingBase.getContentSize().width * 0.5 + 2.5, 123 + this.rankingBase.getContentSize().height * 0.5 + 1.75));

        var s;
        var c;
        var num;


        switch ( this.getTournamentId() ) {
            case 1:
                this.minBet = 50;
                this.maxBet = 500;
                break;
            case 2:
                this.minBet = 1000;
                this.maxBet = 10000;
                break;
            case 3:
                this.minBet = 50000;
                this.maxBet = 500000;
                break;

            default:
                break;
        }
        s = "" + this.minBet;
        c = s[0];
        num = parseInt( c );
        if ( this.getTournamentId() == 1 )
        {
            this._minBetMod = 100;
        }
        else
            this._minBetMod = Math.floor( this.minBet / num );
        this.setFinalData(this.maxBet);
        this.tournamentView();
        this.setCurrentPrizePool();

        this._currentCount = 0;
        this.setSch();
        this.setPlayers();
        this.setPlayersView();
        return true;
    }

});