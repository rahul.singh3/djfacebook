/**
 * A brief explanation for "project.json":
 * Here is the content of project.json file, this is the global configuration for your game, you can modify it to customize some behavior.
 * The detail of each field is under it.
 {
    "project_type": "javascript",
    // "project_type" indicate the program language of your project, you can ignore this field

    "debugMode"     : 1,
    // "debugMode" possible values :
    //      0 - No message will be printed.
    //      1 - cc.error, cc.assert, cc.warn, cc.log will print in console.
    //      2 - cc.error, cc.assert, cc.warn will print in console.
    //      3 - cc.error, cc.assert will print in console.
    //      4 - cc.error, cc.assert, cc.warn, cc.log will print on canvas, available only on web.
    //      5 - cc.error, cc.assert, cc.warn will print on canvas, available only on web.
    //      6 - cc.error, cc.assert will print on canvas, available only on web.

    "showFPS"       : true,
    // Left bottom corner fps information will show when "showFPS" equals true, otherwise it will be hide.

    "frameRate"     : 60,
    // "frameRate" set the wanted frame rate for your game, but the real fps depends on your game implementation and the running environment.

    "noCache"       : false,
    // "noCache" set whether your resources will be loaded with a timestamp suffix in the url.
    // In this way, your resources will be force updated even if the browser holds a cache of it.
    // It's very useful for mobile browser debugging.

    "id"            : "gameCanvas",
    // "gameCanvas" sets the id of your canvas element on the web page, it's useful only on web.

    "renderMode"    : 0,
    // "renderMode" sets the renderer type, only useful on web :
    //      0 - Automatically chosen by engine
    //      1 - Forced to use canvas renderer
    //      2 - Forced to use WebGL renderer, but this will be ignored on mobile browsers

    "engineDir"     : "frameworks/cocos2d-html5/",
    // In debug mode, if you use the whole engine to develop your game, you should specify its relative path with "engineDir",
    // but if you are using a single engine file, you can ignore it.

    "modules"       : ["cocos2d"],
    // "modules" defines which modules you will need in your game, it's useful only on web,
    // using this can greatly reduce your game's resource size, and the cocos console tool can package your game with only the modules you set.
    // For details about modules definitions, you can refer to "../../frameworks/cocos2d-html5/modulesConfig.json".

    "jsList"        : [
    ]
    // "jsList" sets the list of js files in your game.
 }
 *
 */

var appLaunchWatcher = false;
var FacebookObj = null;//change 13 June
var BrowserviewportHeight;
var BrowserviewportWidth;
var InitialBrowserviewportWidth;

cc.game.onStart = function(){

    //var v;
    //v.setPosition( 0, 0 );
    //trackJs.track('ahoy trackjs!');
    /*/ for Testing
     FacebookObj.getAppFriendsList();
     FacebookObj.getNonAppFriendsList();//*/

    /*var refreshIntervalId = setInterval(function(){//change 16 May
     /// call your function here

     if( !appLaunchWatcher )
     {
     window.location.reload(true);
     }
     clearInterval(refreshIntervalId);

     }, 60000, refreshIntervalId);*/

    /*var d = new Date(); // for now
     console.log( "launched at " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds() );*/

    //cc.log( "FB = " + typeof(FB) );

    window.fbAsyncInit = function () {//change 24 May
        var app_id ="";

        if( hackBuild )
        {
            app_id = "911544245891110";
        }else {
            app_id = "911544245891110";
        }

        FB.init({
            appId: app_id,
            xfbml: true,
            status: true,
            cookie: true,
            frictionlessRequests: true,
            version: 'v6.0'//'v2.8'#Change Nov 30 2017
        });

        FB.Canvas.getPageInfo(function(info){
            var viewportHeight = info.clientHeight - info.offsetTop;
            BrowserviewportHeight=viewportHeight;
            InitialBrowserviewportWidth = viewportHeight;
            // BrowserviewportWidth=info.clientWidth;
        });

        FacebookObj = new FacebookAppV2_3();
        FacebookObj.init(app_id);
        FacebookObj.getLoginStatus('public_profile,email');

        if( hackBuild )
        {
            FacebookObj.getAppFriendsList();
            FacebookObj.getNonAppFriendsList();
        }

        /*var d = new Date(); // for now
         console.log( "launched at2 " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds() );*/

        if( appLaunchWatcher )
        {
            AppDelegate.getInstance().LaunchApp();
        }
    };

    (function (d, s, id) {//change 24 May
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    AppDelegate.getInstance().changeBackgroundImage( res.Background_Lobby_html_png  );
    //cc.log( "2" );
    cc.director.getNotificationNode().runAction( new cc.Sequence( new cc.DelayTime( 180.0 ), cc.callFunc( function() {//change 16 May
        if( !appLaunchWatcher )
        {
            window.location.reload(true);
        }
    }, this ) ) );

    /* later */

    //cc.log( "3" );
    //ServerCommunicator.getInstance();
    if(!cc.sys.isNative && document.getElementById("cocosLoading")) //If referenced loading.js, please remove it
        document.body.removeChild(document.getElementById("cocosLoading"));
    //cc.log( "4" );
    // Pass true to enable retina display, on Android disabled by default to improve performance
    cc.view.enableRetina(cc.sys.os === cc.sys.OS_IOS ? true : false);

    // Adjust viewport meta
    cc.view.adjustViewPort(true);
    //cc.log( "5" );
    // Uncomment the following line to set a fixed orientation for your game
    // cc.view.setOrientation(cc.ORIENTATION_PORTRAIT);

    // Setup the resolution policy and design resolution size
    cc.view.setDesignResolutionSize(1024, 768, cc.ResolutionPolicy.SHOW_ALL);

    // The game will be resized when browser size change
    cc.view.resizeWithBrowserSize(true);

    cc.view.setResizeCallback(function() {
        //console.log('Resized!');
        //console.log( "Width = " + cc.view.getFrameSize().width + " Height = " + cc.view.getFrameSize().height );
        //DJSResizer.getInstance().reScaleNodes();
    });

    function OnHideApp() {
       // console.log('OnHideApp!');
        if(!AppDelegate.getInstance().appJustLaunched && AppDelegate.getInstance().onForGround)
        {
            ServerData.getInstance().updateCustomDataOnServer();

        }
        AppDelegate.getInstance().onForGround = false;
        AppDelegate.getInstance().tournamentToBG();
    }
    function OnShowApp() {
      //  console.log('OnShowApp!');
        AppDelegate.getInstance().onForGround = true;
        AppDelegate.getInstance().tournamentToFG();
    }


    cc.eventManager.addCustomListener( cc.game.EVENT_HIDE, OnHideApp );
    cc.eventManager.addCustomListener( cc.game.EVENT_SHOW, OnShowApp );

    cc.LoaderScene.preload(g_resources, function () {
        appLaunchWatcher = true;
        if( ( typeof ( FacebookObj ) != undefined && FacebookObj != null ) || hackBuild )//change 12 June

            AppDelegate.getInstance().LaunchApp();

    }, this);

//"modules" : ["cocos2d","extensions","external"],
};
cc.game.run();